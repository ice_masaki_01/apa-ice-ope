/*-------------------------------------------------------------------------------
 ヘッダー スムーズスクロール
-------------------------------------------------------------------------------*/
$(function(){
	$('.anc a[href^=#] , #page_top a[href^=#]').click(function() {
		var winWidth = window.innerWidth;

			var speed = 400;
			var href= $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = Math.ceil(target.offset().top);
			$('body,html').animate({scrollTop:position}, speed, 'swing');
			return false;

	});
});


/* ------------------------------------------------------------------
アコーディオン
------------------------------------------------------------------ */
$ = jQuery;
$(function(){

  var scroll = {
    click : function(){
      $('a[href^=#]').click(function() {
        var speed = 400;
        var href = $(this).attr("href");
        var target = $(href === "#" || href === "" ? 'html' : href);
        if(target.offset() == undefined){
          var  position = '0px';
        }else{
          var position = target.offset().top;
        }
        $('body,html').animate({
          scrollTop: position
        }, speed, 'swing');
        return false;
      });
    }
  };
  scroll.click();

  var _photo = {
    change_image : function(){
      $('._photo-bottom .thumbnail li').hover(function(){
        var img = $(this).find('img');
        var main = $(this).closest('._photo-bottom').find('.main');
        main.find('img').attr('src', img.attr('src'));
      })
    },
    call : function(){
      _photo.change_image();
    },
  };
  _photo.call();
  
  navigation = {};
  navigation.initialize = (function(){
			$(function() {
				var $header = $('.sp-header');

				// Nav Toggle Button
				$('#nav-toggle').click(function(){
					$header.toggleClass('open');
				});
			});
		});
  navigation.initialize();
  
});


/*
$(function() {
	var w = $(window).width();
	var x = 750;
	if (w <= x) {
		$("header .open").each(function(){
		$("header .other_box").hide();
		$("header .open").click(function(){
			$(this).toggleClass("active");
			$("header .other_box").slideToggle(0);
		});
		$("header .other_box ul li a").click(function(){
			$(this).parent().parent().parent().parent().prev().removeClass("active");
			$("header .other_box").slideToggle(0);
		});
		return false;
		});
	}
});

$(function(){
	$(window).on("resize" , function() {

		var w = $(window).width();
		var x = 750;
		if (w <= x) {
			$("header .open").click(function(){
				$(this).toggleClass("active");
				$("header .other_box").slideToggle(0);
			});
			$("header .other_box ul li a").click(function(){
				$(this).parent().parent().parent().parent().prev().removeClass("active");
				$("header .other_box").slideToggle(0);
			});
			return false;
		}

	});
});
*/

/*-------------------------------------------------------------------------------
 スクロール　アニメ コンテンツ
-------------------------------------------------------------------------------*/

$(function () {
	$('#anc01').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('#anc02').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('#anc03').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('.relax').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('.banquet').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('.other').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('.sight').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
});


/* ------------------------------------------------------------------
	PC フッターアコーディオン
------------------------------------------------------------------ */

$(function(){
$(".network_area ").each(function(){
$(".network_area .over").click(function(){
$(this).toggleClass("active");
$(this).parents().find("#hotel_list_box").slideToggle(500);
});
return false;
});
});


$(function () {
	$('#page_top').hide();
	
	//page-top button
	var topBtn = $('.page-top');    
    topBtn.hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });

    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

});