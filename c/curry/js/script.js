
	//ページ内リンク

$(function(){
	$('.anc a[href^=#] , .pagetop a[href^=#]').click(function() {
		var winWidth = window.innerWidth;

			var speed = 400;
			var href= $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = Math.ceil(target.offset().top);
			$('body,html').animate({scrollTop:position}, speed, 'swing');
			return false;

	});
	

	//SP アコーディオン

	$(function(){
		var wid = $(window).innerWidth();
		if (wid < 771){
			$(".menu , .nav ul li a").click(function(){
				$('.menu').toggleClass("active");
				$('.nav').slideToggle(500);
			});
			return false;
		}
	});

});

// bxslider

$(window).load(function() {

	$('.bxslider').bxSlider({
		auto:true,
		controls:true,
		pager:true,
		speed:800,
		moveSlides:1,
	});

});
