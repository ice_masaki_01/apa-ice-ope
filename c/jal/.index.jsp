<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page import="java.util.*"%>
<%@ page import="java.util.stream.*"%>
<%@ page import="java.util.Map.Entry"%>
<%@ page import="HardCore.Databases,HardCore.Data,HardCore.Page"%>
<%@ page import="com.apahotel.controller.MyHttpRequest"%>
<%@ page import="com.apahotel.controller.PlanJSONConverter"%>
<%@ include file="/webadmin.jsp"%>
<%@ include file="/apacustom/taglib.jsp"%>
<%@ include file="/apacustom/apa_params.jsp"%>
<%@ page import="com.apahotel.model.TargetJalHotel"%>

<%

String lang_code = request.getParameter("lang_code"); 
if ( lang_code==null || lang_code.isEmpty() ) {
	lang_code = "ja_jp";
} else {
	switch (lang_code) {
		case "en_us" :
		lang_code = "en_us";
		break;
		case "ko_kr" :
		lang_code = "ko_kr";
		break;
		case "zh_cn" :
		lang_code = "zh_cn";
		break;
		case "zh_tw" :
		lang_code = "zh_tw";
		break;
		default:
		lang_code = "ja_jp";
		break;
	}
}

	// pref_idを元に各エリアごとのJAL対象ホテルをDBからループで取得する。
ArrayList<ArrayList<HashMap<String,String>>> hotelsAll = new ArrayList<>();
for(int area_id = 0; area_id < 10 ; area_id++){

	// エリアID（HTMLのエリアの配置順に対応）とPref_idを紐付け
	List<String> pref_ids = new ArrayList<>();
	switch (area_id) {
	  case 0: // 北海道エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("1"));
	    break;
	  case 1: // 東北エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("2","3","4","5","6","7"));
	    break;
	  case 2: // 首都圏エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("8","9","10","11","12","13","14"));
	    break;
	  case 3: // 甲信越エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("15","19","20"));
	    break;
	  case 4: // 北陸エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("16","17","18"));
	    break;
	  case 5: // 東海エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("21","22","23","24"));
	    break;
	  case 6: // 関西エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("25","26","27","28","29","30"));
	    break;
	  case 7: // 中国エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("31","32","33","34","35"));
	    break;
	  case 8: // 四国エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("36","37","38","39"));
	    break;
	  case 9: // 九州・沖縄エリアのホテルを取得
	    pref_ids.addAll(Arrays.asList("40","41","42","43","44","45","46","47"));
	    break;
	  default:
	    return;
	}

  // DBからデータを取得する。
	String getset = myconfig.get(db,"getset");
	Databases databases = new Databases(mytext);
	databases.readTitle(db, myconfig, "hotel");
	String pDbName = "data" + databases.getId();

	String[] colNames = {"hotel_id", "hotel_name", "hotel_type", "pref_id", "address_cat", "tel", "hotel_no", "econ_hotel_id", "top_img","ci","co","url","url_alias","external_url","emap_code","top_img","top_thumb_img","airport","jal_hotel_code","jal_area_code","lang_code"};
	HashMap<String, HashMap<String,String>> dbColumns = new HashMap<>();
	for(String colName: colNames){
	  HashMap<String, String> colProp = new HashMap<>();
	  colProp.put("pName", "col" + databases.namedcolumns.get(colName).get("id"));
	  colProp.put("type", "col" + databases.namedcolumns.get(colName).get("type"));
	  dbColumns.put(colName, colProp);
	}

	StringBuilder sb = new StringBuilder();
	sb.append("select ");
	{
	  ArrayList<String> keys = new ArrayList<>();
	  for(Entry<String, HashMap<String, String>> entry: dbColumns.entrySet()){
	    keys.add(entry.getValue().get("pName") + " " + entry.getKey());
	  }
	  sb.append(String.join(",", keys));
	}
	sb.append(" from " + pDbName);
	
	sb.append(" where ");
	// 該当するlang_codeのみのものを取得する。
	sb.append("" + dbColumns.get("lang_code").get("pName") + " = ? AND (" );
	// 条件として該当するpref_idのものを取得する。
	for (Integer pref_count = 0; pref_count < pref_ids.size() ; pref_count++) {
	  if (pref_count != 0){
	    sb.append(" OR ");
	  }
	  sb.append(dbColumns.get("pref_id").get("pName") + " = ?" );
	}
	sb.append(" )");
	
	ArrayList<HashMap<String,String>> hotels = new ArrayList<>();
	try{

	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  ps = db.prepareStatement(sb.toString());
	  int p = 1;
		
		ps.setString(p++, lang_code);
	  for (String pref_id : pref_ids) {
	    ps.setInt(p++, Integer.parseInt(pref_id));
	  }
		
	  rs = ps.executeQuery();
	  while(rs.next()){
	    HashMap<String,String> data = new HashMap<>();
	    for(String colName: colNames){
				String colValue = rs.getString(colName);
				if (colName == "pref_id") {
					// JALで利用するpref_idはXX0000のフォーマットのため変換する。
					int prefId = (int)Double.parseDouble(colValue);
					String prefString = String.format("%02d0000", prefId);
					data.put(colName, prefString);
					
				} else{
					data.put(colName, colValue);
				}
	    }
			// JAL対象ホテルのみ追加する。
			if ( data.get("jal_area_code") != null && data.get("airport") != null && data.get("jal_area_code") != null ){
				if ( ( !data.get("jal_area_code").isEmpty() ) && ( !data.get("airport").isEmpty() ) && ( !data.get("jal_hotel_code").isEmpty() )  ){
					hotels.add(data);
				}
			}
	    
	  }
	}catch(Exception e){
	  System.out.println(e);
	}

	if (db != null) db.close(); if (logdb != null) logdb.close();

	hotelsAll.add(hotels);

}
request.setAttribute("hotelsAll", hotelsAll);

// エリアセット
String[] areas = {"北海道エリア","東北エリア","首都圏エリア","甲信越エリア","北陸エリア","東海エリア","関西エリア","中国エリア","四国エリア","九州・沖縄エリア"};
request.setAttribute("areas", areas);


%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="">
<meta name="keywords" content="">
<title>JAL ダイナミックパッケージ|【公式】アパホテル｜ビジネスホテル予約サイト</title>
<link href="./css/vendor/bootstrap-grid.min.css" rel="stylesheet">
<link href="./css/base.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet">
<link href="./css/dp_940_new.css" rel="stylesheet">
<link href="./css/dp_940_new-rwd.css" rel="stylesheet">
<!-- [JAL JS] -->
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/common.js"></script>
<script type="text/javascript" src="./js/rollover.js"></script>
<script type="text/javascript" src="./js/login.js"></script>
<script type="text/javascript" src="./js/insiteSearchInput.js"></script>
<script type="text/javascript" src="./js/formControl.js"></script>
<script type="text/javascript" src="./js/cookieManager.js"></script>
<script type="text/javascript" src="./js/calendarControl.js"></script>
<script type="text/javascript" src="./js/relationalList.js"></script>
<script type="text/javascript" src="https://www.jal.co.jp/common_rn/js/domtourDPConf.js" charset="shift_jis"></script>
<script type="text/javascript" src="./js/domtourDPControl.js"></script>
<script type="text/javascript" src="./js/jquery.matchHeight.js"></script>
<script type="text/javascript">
// affliateフラグ
var isAffliate = false;

JLJS_DTDPCtrl.initValue ={
	adult_num: "2"
	,senior_num: "0"
	,child_num: "0"
	,baby_num: "0"
	,domain: "APA_DP"
	,specific_group_cd: "GRP10117"
	/*
	,customDate:{
		endDate: "2015/07/19",
		constDate: "2015/07/20"
	}
	*/
};
</script>
<!-- /[JAL JS] -->
<script type="text/javascript" src="./js/cmn-vars.js"></script>
<script type="text/javascript" src="./js/script.js"></script>
</head>
<body>
<div id="wrapper">
<header id="g-header">
    <div class="container">
        <div class="g-header--row-1 row no-gutters justify-content-between align-items-center">
            <p class="col-auto g-header--col-1"><img src="./img/logo-1.png" alt="APA HOTELS&amp;RESORTS"></p>
            <p class="col-auto g-header--col-2"><a href="http://www.jal.co.jp/domtour/jaldp/?_ga=2.171161568.1313465673.1532423611-597992466.1532423611" target="_blank"><img src="./img/logo-2.png" alt="JALPAK"></a></p>
        </div>
    </div>
</header>

<div id="main-container" class="contents">
    <div class="container">
        <div id="content-main" class="row no-gutters justify-content-end">
            <h1 class="col-12 content-main--col-1">
                <img src="./img/banner-1.jpg" alt="JALダイナミックパッケージ" class="only-pc">
                <img src="./img/sp--banner-1.jpg" alt="" class="only-sp">
            </h1>
            <p class="col-auto content-main--col-2"><a href="http://www.jal.co.jp/domtour/jaldp/about/" target="_blank" class="cmn-icon-1">JALダイナミックパッケージとは</a></p>
        </div>
        
        <nav id="content-nav">
            <ul class="tabMenu content-nav--items row no-gutters d-none d-md-flex justify-content-center">
	            <c:forEach var="area" items="${areas}" varStatus="status">
		           	<c:if test="${status.index==0}">
		 	         	<li class="item col-auto current"><a href="javascript:void(0);"><c:out value="${area}" /></a></li>
		            </c:if>
		            <c:if test="${status.index!=0}">
		                <li class="item col-auto"><a href="javascript:void(0);"><c:out value="${area}" /></a></li>
		            </c:if>
	            </c:forEach>
            </ul>
            <div class="content-nav--select row no-gutters d-flex d-md-none justify-content-between align-items-center">
                <p class="select-head col-auto">エリア選択</p>
                <div class="select-body col">
                    <select id="tabMenuSmartPhoneSize" class="select-1">
                    	<c:forEach var="area" items="${areas}" varStatus="status">
                    	<c:if test="${status.index==0}">
                        	<option selected><c:out value="${area}" /></option>
                        </c:if>
                        <c:if test="${status.index!=0}">
                       		 <option><c:out value="${area}" /></option>
                        </c:if>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </nav>
        
        <div class="tabList" id="content-list">
       		<c:forEach var="hotels" items="${hotelsAll}" varStatus="status">
       		<div class="tabArea" >
            <h2 class="cmn-hl-1"><c:out value="${areas[status.index]}" /></h2>
            <div class="cmn-list-1">
	            <ul class="cmn-list-1--items row">
	              	<c:forEach var="hotel" items="${hotels}">
		                <li class="cmn-list-1--item col-12 col-md-4">
		        	        <h3 class="item-head"><c:out value="${hotel.hotel_name}" /></h3>
		                    <div class="item-body row no-gutters flex-nowrap">
		                        <div class="item-body--col-1 col-auto"><img src="/image.jsp?id=${hotel.top_thumb_img}" alt="" class="item-body--thumb"></div>
		             	              <div class="item-body--col-2 col">
		                	                <p class="item-body--address"><span class="zip"><c:out value="${hotel.address_cat}" /></span></p>
		                    	            <p class="item-body--tel"><c:out value="${hotel.tel}" /></p>
		                                <div class="item-body--front row no-gutters">
		                                    <p class="col-auto check in"><c:out value="${hotel.ci}" /></p>
		                                    <p class="col-auto check out"><c:out value="${hotel.co}" /></p>
		                                </div>
		                                <div class="item-body--links row no-gutters flex-nowrap justify-content-start justify-content-md-between align-items-center">
		                                    <p class="col-auto link-detail"><a href="https://domdp.jal.co.jp/jaldp/cpHotelDetailPlanDetail/detail?hotel_id=${hotel.jal_hotel_code}&_ga=2.133808078.1313465673.1532423611-597992466.1532423611" target="_blank" class="cmn-icon-1 size-s">ホテル詳細</a></p>
		                                    <p class="col-auto link-select"><a href="javascript:void(0);" onclick="JLJS_DTDPModal.open('${hotel.airport}', '${hotel.pref_id}', '${hotel.jal_hotel_code}', '${hotel.hotel_name}', '${hotel.jal_area_code}');" class="cmn-btn-1 hotelSelect">ホテル選択</a></p>
		                                </div>
		                            </div>
		                        </div>
		                    </li>
	                    </c:forEach>
	                </ul>
	            </div>
	            </div>
            </c:forEach>
        </div>
        
        <div id="content-notice">
            <h2 class="cmn-hl-2">注意事項</h2>
            <div class="content-notice--body">
                <p class="indent-1">※JALダイナミックパッケージは、（株）ジャルパックが企画・実施しています。<br>JALダイナミックパッケージの旅行契約は、（株）ジャルパックとご利用者との間で成立します。事前にご確認のうえ、お申し込みください。</p>
                <p class="indent-1">※お支払は下記のいずれかとなります。</p>
                <div class="pl-1">
                    <p>【JALマイレージバンク会員の方】クレジットカード一括払い、コンビニ決済、e JALポイント、JMB旅プラスご利用マイルのいずれかでのお支払をお願いいたします。</p>
                    <p>【JALマイレージバンク未入会の方】クレジットカード一括払いでのお支払をお願いいたします。</p>
                    <p class="indent-1">・お支払い後、購入内容画面を印刷するか、ご自身のパソコンに必ず保存して予約内容をご確認ください。</p>
                    <p class="indent-1">・クレジットカードをご利用の場合、決済日がカードのご利用日となり、お引き落とし日は各カード会社で定められた日となります。（ご出発日と関係ありません）</p>
                    <p class="indent-1">・JMB旅プラスご利用マイルは、決済後の取消の場合、取消料の有無に関わらず払い戻しできません。(ただしツアー催行中止の場合は除きます。）<br>2017年11月16日以降に引き落としされたe JALポイントは払い戻しいたします。（払い戻しの操作後1カ月程度）</p>
                </div>
                <p class="indent-1">※時価販売商品のため、変更はお受けできません。変更をご希望の場合には、一旦取り消しの後、新規にご予約となります。<br>（取消しの際は、取消料の適用対象となる場合や、ご希望の航空便や宿泊施設が予約できない場合があります。）</p>
                <p class="indent-1">※ご旅行代金は、お見積もり時点での最新旅行代金を都度算出し、ご提供していますが、同一内容でのお見積もりのタイミングにより旅行代金が異なる場合があります。<br>ご予約後に旅行代金に変動があった場合、差額の返金および追徴はおこないませんのであらかじめご了承ください。<br>また、ご搭乗日の前日から起算して7日前までと6日前～前日まで利用する航空券の運賃が異なりますので、ご予約日により旅行代金が変更となります。</p>
                <div class="pl-1">
                    <p class="indent-1">・ご搭乗日7日前までのご予約便：個人包括旅行割引運賃</p>
                    <p class="indent-1">・ご搭乗日6日前～前日までのご予約便：個人包括旅行特定便割引運賃</p>
                </div>
                <p class="indent-1">※出発日の前日23時59分（購入完了）まで申し込み可能です。</p>
                <p class="indent-1">※キャンセル待ちは承っておりません。</p>
                <p class="indent-1">※予約内容の確認・キャンセルは、JAL eトラベルプラザ（外部リンク）にて行ってください。</p>
                <p class="indent-1">※よくあるご質問は、JAL eトラベルプラザ（外部リンク）にてご確認ください。</p>
                <p class="indent-1">※JAL eトラベルプラザ「JALダイナミックパッケージ」予約ページの推奨ブラウザは以下のとおりです。</p>
                <p>Windows Microsoft Internet Explorer 11</p>
            </div>
        </div>
    </div>
</div>

<footer id="g-footer">
    <div class="container">
        <p class="g-footer--text-1">旅行企画・実施　株式会社ジャルパック</p>
        <p class="g-footer--text-2">観光庁長官登録旅行業第705号　〒140-8658　東京都品川区東品川2-4-11　野村不動産天王洲ビル</p>
    </div>
</footer>

<!-- ========================================= -->
<!-- モーダル                                  -->
<!-- ========================================= -->
<div id="JS_dtdp_modal" class="dpModalBox disNon">
<div class="inBox">
<div class="domtour940 modal hotel" id="JS_dtdp_hotel">
<div class="domtourTit row no-gutters align-items-center justify-content-end">
<h3 class="col">航空券＋宿泊＋オプション（JALダイナミックパッケージ）で探す</h3>
<div class="col-auto"><a href="javascript:void(0);" class="modalClose JS_dtdp_modal_close"></a></div>
</div>
<div class="dtr940">
<form name="dp_search" action="https://www121.jal.co.jp/JmbWeb/JR/Dispatcher_ja.do" method="post" target="_blank">
<input type="hidden" name="memberBackUrl" value="https://domdp.jal.co.jp/jaldp/cpFlightListHotelList/list">
<input type="hidden" name="memberPattern" value="5,9">
<input type="hidden" name="guestBackUrl" value="https://domdp.jal.co.jp/jaldp/cpFlightListHotelList/list">
<input type="hidden" name="guestPattern" value="5">
<input type="hidden" name="domain" value="EJALTOURS">
<input type="hidden" name="TranID" value="DOM_JDPTOUR">

<div class="seaBox step1">
<h4><span class="step">STEP1</span>参加人数選択</h4>
<div class="iBox" id="JS_dtdp_bl_member">
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<dl class="sta">
<dt>おとな<span>（12歳以上）</span></dt>
<dd><select name="adult_num" class="per"><option value="1" selected>1名</option><option value="2">2名</option><option value="3">3名</option><option value="4">4名</option><option value="5">5名</option><option value="6">6名</option></select></dd>
<dt>こども<span>（12歳未満）</span></dt>
<dd class="txtChi"><span id="JS_dtdp_total_child">0</span>名</dd>
<dd class="btnChi childOpen" id="JS_dtdp_btn_child"><span>こども人数を入力</span></dd>
</dl>
<div id="JS_dtdp_ipt_child" style="display:none;">
<dl class="chi clearfix">
<dt>こども<span>（6～11歳）</span></dt>
<dd><select name="senior_num" class="per"><option value="0">0名</option><option value="1">1名</option><option value="2">2名</option><option value="3">3名</option><option value="4">4名</option><option value="5">5名</option></select></dd>
<dt>こども<span>（3～5歳）</span></dt>
<dd><select name="child_num" class="per"><option value="0">0名</option><option value="1">1名</option><option value="2">2名</option><option value="3">3名</option><option value="4">4名</option><option value="5">5名</option></select></dd>
<dt>同伴幼児<span>（0～2歳）</span></dt>
<dd><select name="baby_num" class="per"><option value="0">0名</option><option value="1">1名</option><option value="2">2名</option></select></dd>
</dl>
</div><!-- /JS_dtdp_ipt_child -->
</dd>
</dl>
</div><!-- /iBox -->
</div><!-- /step1 -->

<div class="seaBox step2">
<h4><span class="step">STEP2</span>フライト条件<br class="only-pc">選択</h4>
<div class="iBox" id="JS_dtdp_bl_flight">
<div class="JS_dtdp_adult_more2 disNon">
<p class="lab first"><label><input type="checkbox" name="flight_condition" value="1">異なるフライト条件（出発地、出発日、出発便）の方がいる</label></p>
<p class="stt">1グループ目のフライト選択</p>
</div><!-- /JS_dtdp_adult_more2 -->

<dl class="clm2 JS_dtdp_rel_wrap">
<dt class="ttl icon departure">往路</dt>
<dd class="con JS_dtdp_wrap_dep">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="dep_boardMonth1" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="dep_boardDay1" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="dep_boardAirport1" class="fli JS_dtdp_airport JS_dtdp_tri_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><select name="dep_arrivalAirport1" class="fli JS_dtdp_airport JS_dtdp_tri_arr"><option value="TYO">東京(羽田)</option></select></dd>
</dl>
</dd>
<dt class="ttl icon arrival">復路</dt>
<dd class="con JS_dtdp_wrap_arr">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="arr_boardMonth1" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="arr_boardDay1" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="arr_boardAirport1" class="fli JS_dtdp_airport JS_dtdp_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><strong><span class="JS_dtdp_lb_arr">東京(羽田)</span></strong><input type="hidden" name="arr_arrivalAirport1" value="TYO" class="JS_dtdp_arr"></dd>
</dl>
</dd>
</dl>
<div class="JS_dtdp_adult_more2 disNon">
<div id="JS_dtdp_opBox_fli2" style="display:none;">
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="flight_adult_num1" class="per JS_dtdp_adult"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="flight_senior_num1" class="per JS_dtdp_senior"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="flight_child_num1" class="per JS_dtdp_child"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="flight_baby_num1" class="per JS_dtdp_baby"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>
<p class="stt">2グループ目のフライト選択</p>
<dl class="clm2 JS_dtdp_rel_wrap">
<dt class="ttl icon departure">往路</dt>
<dd class="con JS_dtdp_wrap_dep">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="dep_boardMonth2" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="dep_boardDay2" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="dep_boardAirport2" class="fli JS_dtdp_airport JS_dtdp_tri_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><select name="dep_arrivalAirport2" class="fli JS_dtdp_airport JS_dtdp_tri_arr"><option value="TYO">東京(羽田)</option></select></dd>
</dl>
</dd>
<dt class="ttl icon arrival">復路</dt>
<dd class="con JS_dtdp_wrap_arr">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="arr_boardMonth2" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="arr_boardDay2" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="arr_boardAirport2" class="fli JS_dtdp_airport JS_dtdp_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><strong><span class="JS_dtdp_lb_arr">東京(羽田)</span></strong><input type="hidden" name="arr_arrivalAirport2" value="TYO" class="JS_dtdp_arr"></dd>
</dl>
</dd>
</dl>
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="flight_adult_num2" class="per JS_dtdp_adult"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="flight_senior_num2" class="per JS_dtdp_senior"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="flight_child_num2" class="per JS_dtdp_child"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="flight_baby_num2" class="per JS_dtdp_baby"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>

<div class="disNon" id="JS_dtdp_adult_more3">
<p class="lab"><label><input type="checkbox" name="flight_condition_flg2" value="1">3つ目のグループ・個人のフライトを追加する</label></p>

<div id="JS_dtdp_opBox_fli3" style="display:none;">
<p class="stt">3グループ目のフライト選択</p>
<dl class="clm2 JS_dtdp_rel_wrap">
<dt class="ttl icon departure">往路</dt>
<dd class="con JS_dtdp_wrap_dep">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="dep_boardMonth3" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="dep_boardDay3" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="dep_boardAirport3" class="fli JS_dtdp_airport JS_dtdp_tri_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><select name="dep_arrivalAirport3" class="fli JS_dtdp_airport JS_dtdp_tri_arr"><option value="TYO">東京(羽田)</option></select></dd>
</dl>
</dd>
<dt class="ttl icon arrival">復路</dt>
<dd class="con JS_dtdp_wrap_arr">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="arr_boardMonth3" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="arr_boardDay3" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="arr_boardAirport3" class="fli JS_dtdp_airport JS_dtdp_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><strong><span class="JS_dtdp_lb_arr">東京(羽田)</span></strong><input type="hidden" name="arr_arrivalAirport3" value="TYO" class="JS_dtdp_arr"></dd>
</dl>
</dd>
</dl>
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="flight_adult_num3" class="per JS_dtdp_adult"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="flight_senior_num3" class="per JS_dtdp_senior"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="flight_child_num3" class="per JS_dtdp_child"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="flight_baby_num3" class="per JS_dtdp_baby"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>

<div class="disNon" id="JS_dtdp_adult_more4">
<p class="lab"><label><input type="checkbox" name="flight_condition_flg3" value="1">4つ目のグループ・個人のフライトを追加する</label></p>

<div id="JS_dtdp_opBox_fli4" style="display:none;">
<p class="stt">4グループ目のフライト選択</p>
<dl class="clm2 JS_dtdp_rel_wrap">
<dt class="ttl icon departure">往路</dt>
<dd class="con JS_dtdp_wrap_dep">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="dep_boardMonth4" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="dep_boardDay4" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="dep_boardAirport4" class="fli JS_dtdp_airport JS_dtdp_tri_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><select name="dep_arrivalAirport4" class="fli JS_dtdp_airport JS_dtdp_tri_arr"><option value="TYO">東京(羽田)</option></select></dd>
</dl>
</dd>
<dt class="ttl icon arrival">復路</dt>
<dd class="con JS_dtdp_wrap_arr">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="arr_boardMonth4" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="arr_boardDay4" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="arr_boardAirport4" class="fli JS_dtdp_airport JS_dtdp_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><strong><span class="JS_dtdp_lb_arr">東京(羽田)</span></strong><input type="hidden" name="arr_arrivalAirport4" value="TYO" class="JS_dtdp_arr"></dd>
</dl>
</dd>
</dl>
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="flight_adult_num4" class="per JS_dtdp_adult"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="flight_senior_num4" class="per JS_dtdp_senior"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="flight_child_num4" class="per JS_dtdp_child"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="flight_baby_num4" class="per JS_dtdp_baby"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>

<div class="disNon" id="JS_dtdp_adult_more5">
<p class="lab"><label><input type="checkbox" name="flight_condition_flg4" value="1">5つ目のグループ・個人のフライトを追加する</label></p>

<div id="JS_dtdp_opBox_fli5" style="display:none;">
<p class="stt">5グループ目のフライト選択</p>
<dl class="clm2 JS_dtdp_rel_wrap">
<dt class="ttl icon departure">往路</dt>
<dd class="con JS_dtdp_wrap_dep">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="dep_boardMonth5" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="dep_boardDay5" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="dep_boardAirport5" class="fli JS_dtdp_airport JS_dtdp_tri_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><select name="dep_arrivalAirport5" class="fli JS_dtdp_airport JS_dtdp_tri_arr"><option value="TYO">東京(羽田)</option></select></dd>
</dl>
</dd>
<dt class="ttl icon arrival">復路</dt>
<dd class="con JS_dtdp_wrap_arr">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="arr_boardMonth5" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="arr_boardDay5" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="arr_boardAirport5" class="fli JS_dtdp_airport JS_dtdp_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><strong><span class="JS_dtdp_lb_arr">東京(羽田)</span></strong><input type="hidden" name="arr_arrivalAirport5" value="TYO" class="JS_dtdp_arr"></dd>
</dl>
</dd>
</dl>
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="flight_adult_num5" class="per JS_dtdp_adult"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="flight_senior_num5" class="per JS_dtdp_senior"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="flight_child_num5" class="per JS_dtdp_child"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="flight_baby_num5" class="per JS_dtdp_baby"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>

<div class="disNon" id="JS_dtdp_adult_more6">
<p class="lab"><label><input type="checkbox" name="flight_condition_flg5" value="1">6つ目のグループ・個人のフライトを追加する</label></p>

<div id="JS_dtdp_opBox_fli6" style="display:none;">
<p class="stt">6グループ目のフライト選択</p>
<dl class="clm2 JS_dtdp_rel_wrap">
<dt class="ttl icon departure">往路</dt>
<dd class="con JS_dtdp_wrap_dep">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="dep_boardMonth6" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="dep_boardDay6" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="dep_boardAirport6" class="fli JS_dtdp_airport JS_dtdp_tri_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><select name="dep_arrivalAirport6" class="fli JS_dtdp_airport JS_dtdp_tri_arr"><option value="TYO">東京(羽田)</option></select></dd>
</dl>
</dd>
<dt class="ttl icon arrival">復路</dt>
<dd class="con JS_dtdp_wrap_arr">
<dl class="air">
<dt>出発日</dt>
<dd class="dat"><select name="arr_boardMonth6" class="mon JS_dtdp_mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="arr_boardDay6" class="day JS_dtdp_day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a></dd>
<dt>出発地</dt>
<dd><select name="arr_boardAirport6" class="fli JS_dtdp_airport JS_dtdp_dep"><option value="TYO">東京(羽田)</option></select></dd>
<dt>到着地</dt>
<dd><strong><span class="JS_dtdp_lb_arr">東京(羽田)</span></strong><input type="hidden" name="arr_arrivalAirport6" value="TYO" class="JS_dtdp_arr"></dd>
</dl>
</dd>
</dl>
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="flight_adult_num6" class="per JS_dtdp_adult"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="flight_senior_num6" class="per JS_dtdp_senior"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="flight_child_num6" class="per JS_dtdp_child"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="flight_baby_num6" class="per JS_dtdp_baby"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>

</div><!-- /JS_dtdp_opBox_fli6 -->
</div><!-- /JS_dtdp_adult_more6 -->

</div><!-- /JS_dtdp_opBox_fli5 -->
</div><!-- /JS_dtdp_adult_more5 -->

</div><!-- /JS_dtdp_opBox_fli4 -->
</div><!-- /JS_dtdp_adult_more4 -->

</div><!-- /JS_dtdp_opBox_fli3 -->
</div><!-- /JS_dtdp_adult_more3 -->

</div><!-- /JS_dtdp_opBox_fli2 -->
</div><!-- /JS_dtdp_adult_more2 -->

</div><!-- /iBox -->
</div><!-- /step2 -->

<div class="seaBox step3">
<h4><span class="step">STEP3</span>宿泊施設条件<br class="only-pc">選択</h4>
<div class="iBox" id="JS_dtdp_bl_stayDate">
<div class="notice"><p class="JS_dtdp_stay_notice">1泊ごとに宿泊施設を変更する場合、最初の宿泊条件を入力してください。<br>※2泊目以降は選択内容のご確認(カート)から追加いただけます。</p>
<p class="JS_dtdp_stay_notice disNon">1泊ごとに宿泊施設や宿泊人数を変更する場合、最初の宿泊条件を入力してください。<br>※2泊目以降は選択内容のご確認(カート)から追加いただけます。</p></div>
<dl class="clm1 JS_dtdp_rel_wrap">
<dt class="ttl icon schedule">日程</dt>
<dd class="con">
<dl class="sta">
<dt class="mW54">宿泊日</dt>
<dd><select name="stay_month" class="mon"><option value="1">1月</option><option value="2">2月</option><option value="3">3月</option><option value="4">4月</option><option value="5">5月</option><option value="6">6月</option><option value="7">7月</option><option value="8">8月</option><option value="9">9月</option><option value="10">10月</option><option value="11">11月</option><option value="12">12月</option></select><select name="stay_day" class="day"><option value="1">1日</option><option value="2">2日</option><option value="3">3日</option><option value="4">4日</option><option value="5">5日</option><option value="6">6日</option><option value="7">7日</option><option value="8">8日</option><option value="9">9日</option><option value="10">10日</option><option value="11">11日</option><option value="12">12日</option><option value="13">13日</option><option value="14">14日</option><option value="15">15日</option><option value="16">16日</option><option value="17">17日</option><option value="18">18日</option><option value="19">19日</option><option value="20">20日</option><option value="21">21日</option><option value="22">22日</option><option value="23">23日</option><option value="24">24日</option><option value="25">25日</option><option value="26">26日</option><option value="27">27日</option><option value="28">28日</option><option value="29">29日</option><option value="30">30日</option><option value="31">31日</option></select><a href="javascript: void(0);" onclick="return false;" class="cal floatCalIcon"><img src="./img/icon_calendar_001_n.gif" alt=""></a><span class="mLR1">から</span><select name="nights" class="per"><option value="1">1泊</option><option value="2">2泊</option><option value="3">3泊</option><option value="4">4泊</option><option value="5">5泊</option><option value="6">6泊</option><option value="7">7泊</option><option value="8">8泊</option><option value="9">9泊</option><option value="10">10泊</option><option value="11">11泊</option><option value="12">12泊</option><option value="13">13泊</option></select></dd>
</dl>
</dd>
</dl>
<div class="JS_dtdp_adult_more2 disNon">
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="stay_adult_num" class="per"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="stay_senior_num" class="per"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="stay_child_num" class="per"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="stay_baby_num" class="per"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>
</div><!-- /JS_dtdp_adult_more2 -->
</div><!-- /iBox -->

<div class="iBox" id="JS_dtdp_bl_stayFaci">
<dl class="clm1 line">
<dt class="ttl icon boarding">宿泊施設</dt>
<dd class="con">
<dl class="sta hOf">
<dt class="mW54">都道府県</dt>
<dd><select name="stay_prefecture_cd" class="wid80"><option value="010000">北海道</option>
<option value="020000">青森県</option>
<option value="030000">岩手県</option>
<option value="040000">宮城県</option>
<option value="050000">秋田県</option>
<option value="060000">山形県</option>
<option value="070000">福島県</option>
<option value="080000">栃木県</option>
<option value="090000">群馬県</option>
<option value="100000">茨城県</option>
<option value="110000">埼玉県</option>
<option value="120000">千葉県</option>
<option value="130000">東京都</option>
<option value="140000">神奈川県</option>
<option value="150000">山梨県</option>
<option value="160000">長野県</option>
<option value="170000">新潟県</option>
<option value="180000">富山県</option>
<option value="190000">石川県</option>
<option value="200000">福井県</option>
<option value="210000">静岡県</option>
<option value="220000">岐阜県</option>
<option value="230000">愛知県</option>
<option value="240000">三重県</option>
<option value="250000">滋賀県</option>
<option value="260000">京都府</option>
<option value="270000">大阪府</option>
<option value="280000">兵庫県</option>
<option value="290000">奈良県</option>
<option value="300000">和歌山県</option>
<option value="310000">鳥取県</option>
<option value="320000">島根県</option>
<option value="330000">岡山県</option>
<option value="340000">広島県</option>
<option value="350000">山口県</option>
<option value="360000">徳島県</option>
<option value="370000">香川県</option>
<option value="380000">愛媛県</option>
<option value="390000">高知県</option>
<option value="400000">福岡県</option>
<option value="410000">佐賀県</option>
<option value="420000">長崎県</option>
<option value="430000">熊本県</option>
<option value="440000">大分県</option>
<option value="450000">宮崎県</option>
<option value="460000">鹿児島県</option>
<option value="470000">沖縄県</option></select></dd>
<dt>地域</dt>
<dd><select name="stay_area_cd" class="wid280"><option value="">指定なし</option></select></dd>
</dl>
<dl class="sta hOf">
<dt class="mW54">キーワード名</dt>
<dd><input type="text" name="stay_keyword" value="宿泊施設名またはプラン名" class="keyW pfOn"></dd>
</dl>
<p class="nam hOn"><strong id="JS_dtdp_hotel_name"></strong><input type="hidden" name="hotel_id" value="">
<span class="btnHot disNon" id="JS_dtdp_hotel_filter_btn"><span>ホテルを絞り込む</span></span></p>
<dl class="sta">
<dt class="mW54">部屋数</dt>
<dd><select name="room_num" class="wid80"><option value="1">1部屋</option></select></dd>
</dl>
</dd>
</dl>

<div class="disNon" id="JS_dtdp_room_more2">
<p class="lab"><label><input type="checkbox" name="every_room_differ_flg" value="1">部屋ごとに利用人数、宿泊プラン（禁煙、食事あり・なし、など）が異なる</label></p>
<div id="JS_dtdp_opBox_room_diff" style="display:none;">
<p class="stt">1部屋目の人数を選択してください。<span class="smaller01">※2部屋以降は、1部屋目の宿泊プランを選択後に選択いただけます。</span></p>
<dl class="clm1">
<dt class="ttl icon member">人数</dt>
<dd class="con">
<ul class="mem">
<li><span class="txt">おとな<span>（12歳以上）</span></span><span class="sel"><select name="room1_adult_num" class="per"><option value="1">1名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_adult"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（6～11歳）</span></span><span class="sel"><select name="room1_senior_num" class="per"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_senior"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">こども<span>（3～5歳）</span></span><span class="sel"><select name="room1_child_num" class="per"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_child"></span>名<span class="per">（参加人数）</span></span></span></li>
<li><span class="txt">同伴幼児<span>（0～2歳）</span></span><span class="sel"><select name="room1_baby_num" class="per"><option value="0">0名</option></select><span class="smaller01">／<span class="JS_dtdp_lb_baby"></span>名<span class="per">（参加人数）</span></span></span></li>
</ul>
</dd>
</dl>
</div><!-- /JS_dtdp_opBox_room_diff -->
</div><!-- /JS_dtdp_room_more2 -->
</div><!-- /iBox -->

<div class="iBox" id="JS_dtdp_bl_stayCond">
<dl class="clm1 line">
<dt class="ttl icon room">部屋条件</dt>
<dd class="con">
<dl class="sta">
<dt>部屋タイプ</dt>
<dd><select name="room_type" class="opt"><option value="">指定なし</option><option value="SNGL">シングル</option><option value="TWIN">ツイン</option><option value="SEMI">セミダブル</option><option value="DBLE">ダブル</option><option value="TRPL">トリプル</option><option value="FBED">4ベッド</option><option value="JPNR">和室</option><option value="JFRR">和洋室</option></select></dd>
<dt>禁煙ルーム</dt>
<dd><select name="room_smokingType" class="opt"><option value="">指定なし</option><option value="1">禁煙ルーム</option></select></dd>
<dt>食事</dt>
<dd><select name="room_mealType" class="opt"><option value="">指定なし</option><option value="1">朝食付き</option><option value="4">食事なし</option><option value="3">朝夕食付き</option><option value="2">夕食付き</option></select></dd>
</dl>
<div class="disNon" id="JS_dtdp_child_stayCond">
<div class="selChi">
<p>こども（3～5歳）の宿泊条件を選択してください。</p>
<ul>
<li><strong>寝具あり</strong><span>食事あり<select name="room_child_num1" class="per JS_dtdp_child"><option value="0">0名</option></select></span><span>食事なし<select name="room_child_num2" class="per JS_dtdp_child"><option value="0">0名</option></select></span></li>
<li><strong>寝具なし（添い寝）</strong><span>食事あり<select name="room_child_num3" class="per JS_dtdp_child"><option value="0">0名</option></select></span><span>食事なし<select name="room_child_num4" class="per JS_dtdp_child"><option value="0">0名</option></select></span></li>
</ul>
</div>
</div><!-- /JS_dtdp_child_stayCond -->
</dd>
</dl>
</div><!-- /iBox -->
</div><!-- /step3 -->

<div class="dtrBtm row no-gutters">
<div class="col-md-6 col-12">
<p class="smaller01">ご出発の前日（23:59）まで予約OK!<br>オプションは7日前（レンタカーは3日前）まで予約OK!（※23:59まで。一部オプションはお申し込み期限が異なります。）</p>
</div><!-- /floatL -->
<div class="col-md-6 col-12">
<p class="condition alR row no-gutters justify-content-center justify-content-md-end align-items-center">
<span class="col-auto condition--text-1">この条件で</span>
<span class="col-auto condition--btn-1"><button type="button" id="JS_dtdp_btn_submit">検索する</button></span>
</p>
<p class="condition condition--link-1 alR mgt5 row no-gutters justify-content-center justify-content-md-end">
<a href="http://www.jal.co.jp/tours/guide/information/domtour.html" target="otherwindow" class="iconOtherW col-auto">ご利用案内・お問い合わせ先</a>
</p>
</div><!-- /floatR -->
</div><!-- /dtrBtm -->

<div id="JS_dtdp_bl_other">
<input type="hidden" name="specific_group_cd" value="GRP10117">
<input type="hidden" name="site_disp_cntl" value="">
<input type="hidden" name="discount_coupon_cd" value="">
<input type="hidden" name="no_release_disp" value="">
<input type="hidden" name="top_link_disp" value="">
<input type="hidden" name="param1" value="">
<input type="hidden" name="param2" value="">
<input type="hidden" name="param3" value="">
<input type="hidden" name="param4" value="">
<input type="hidden" name="param5" value="">
</div>
</form>

<form name="dp_search_opt" action="https://www121.jal.co.jp/JmbWeb/JR/Dispatcher_ja.do" method="post" target="_blank">
<input type="hidden" name="memberBackUrl" value="https://domdp.jal.co.jp/jaldp/cpCartForm/disp">
<input type="hidden" name="memberPattern" value="5,9">
<input type="hidden" name="guestBackUrl" value="https://domdp.jal.co.jp/jaldp/cpCartForm/disp">
<input type="hidden" name="guestPattern" value="5">
<input type="hidden" name="domain" value="EJALTOURS" class="JS_dtdp_opt_node">
<input type="hidden" name="discount_coupon_cd" value="" class="JS_dtdp_opt_node">
<input type="hidden" name="option_area_jump" value="1">
<input type="hidden" name="top_link_disp" value="1" class="JS_dtdp_opt_node">
<input type="hidden" name="param1" value="" class="JS_dtdp_opt_node">
<input type="hidden" name="param2" value="" class="JS_dtdp_opt_node">
<input type="hidden" name="param3" value="" class="JS_dtdp_opt_node">
<input type="hidden" name="param4" value="" class="JS_dtdp_opt_node">
<input type="hidden" name="param5" value="" class="JS_dtdp_opt_node">
<input type="hidden" name="TranID" value="DOM_JDPTOUR">
</form>
</div><!-- /dtr940 -->
</div><!-- /domtour940 -->
</div>
</div>
<div id="JS_dtdp_modal_overlay" class="dpModalBg disNon"></div>
<!-- ========================================= -->
<!-- モーダル [END]                                  -->
<!-- ========================================= -->
</div><!--/ #wrapper -->
</body>
</html>