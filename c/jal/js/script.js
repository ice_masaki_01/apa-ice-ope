/* ===================================================================
javascript information

File Name  : script.js
Author     :
script Info : 
=================================================================== */

var APP = APP || {};

// viewportSetting
APP.viewportSetting = (function() {
  var viewport = document.createElement('meta');
  var content = !CHECK.mobile ? 'width=1040' : 'width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no';
  viewport.setAttribute('name', 'viewport');
  viewport.setAttribute('content', content);
  document.getElementsByTagName('head')[0].appendChild(viewport);
}());

// uaClass
APP.uaClass = function() {
  this.$el = $('html');
};
APP.uaClass.prototype = {
  addClass: function() {
    var deviceClass = CHECK.mobile ? 'device-mobile' : CHECK.tablet ? 'device-tablet' : 'device-pc';
    if (typeof DISP.browser.name !== 'undefined') {
      var browserName = DISP.browser.name.toLowerCase().replace(/\s+/g, '');
      var browserVer = DISP.browser.major_version;
      var browserClass = CHECK.ie ? 'bn-ie bv-' + browserVer : 'bn-' + browserName + ' bv-' + browserVer;
      this.$el.addClass(browserClass);
    }
    if (typeof DISP.os.name !== 'undefined') {
      var osGroup = DISP.os.group.toLowerCase().replace(/\s+/g, '');
      var osVer = DISP.os.major_version;
      var osClass = 'og-' + osGroup + ' ov-' + osVer;
      this.$el.addClass(osClass);
    }
    this.$el.addClass(deviceClass);
  },
  init: function() {
    this.addClass();
  }
};

// modalCustom
APP.modalCustom = function() {
  this.bg = '#JS_dtdp_modal_overlay';
};
APP.modalCustom.prototype = {
  addBpChangeEvent: function() {
    var bp = true; // PC = true
    var bpFlagChange = function() {
      if ((DISP.viewport.width >= 768 && bp == false) || (DISP.viewport.width < 768 && bp == true)) {
        $(window).trigger('bpChange');
        bp = !bp;
      }
    };
    $(window).on('load resize', function() {
      bpFlagChange();
    });
  },
  init: function() {
    var self = this;
    self.addBpChangeEvent();
    $(document).on('click', self.bg, function() {
      JLJS_DTDPModal.close();
    });
    $(window).on('bpChange', function() {
      JLJS_DTDPModal.close();
    });
  }
};

// calendarCustom
APP.calendarCustom = function() {
  this.dispWidth = 0;
};
APP.calendarCustom.prototype = {
  init: function() {
    var self = this;
    self.dispWidth = DISP.width;
    $(window).on('resize', function() {
      if (self.dispWidth != DISP.width) document.getElementById(JLJS_CalNav.displaySpace).style.display = 'none';
      self.dispWidth = DISP.width;
      return false;
    });
  }
};

// tellink
APP.tellink = function() {
  this.el = '.tellink-item';
  this.$el = $(this.el);
};
APP.tellink.prototype = {
  setLink: function($el) {
    var tel = $el.attr('data-tel-no');
    if (CHECK.mobile && typeof tel !== 'undefined') {
      var innerHtml = $el.html();
      var href = tel.replace(/-/g, '');
      var html = '<a href="tel:' + href + '" class="cmn-icon-1 tel">' + innerHtml + '</a>';
      $el.replaceWith(html);
    }
  },
  init: function() {
    var self = this;
    self.$el.each(function() {
      self.setLink($(this));
    });
  }
};

$(function() {
  // Instance ---->
  APP.uaClass = new APP.uaClass();
  APP.modalCustom = new APP.modalCustom();
  APP.tellink = new APP.tellink();
  APP.calendarCustom = new APP.calendarCustom();

  // Init -------->
  APP.uaClass.init();
  APP.modalCustom.init();
  APP.tellink.init();
  APP.calendarCustom.init();
});

// タブクリック(PC)
$(function() {
  $('.tabMenu > li').click(function() {
    var index = $(this).parent().children('li').index(this);
    $('.tabMenu').each(function() {
      $('>li', this).removeClass('current').eq(index).addClass('current');
    });
    
    $('#tabMenuSmartPhoneSize').each(function() {
       $('>option', this).prop('selected', false).eq(index).prop('selected', true);
    });
    
    $('.tabList > .tabArea').hide().eq(index).show();
  }).first().click();

});

// タブ選択（スマホ）
$(function() {
	$('#tabMenuSmartPhoneSize').change(function() {	    
	    var index = $('#tabMenuSmartPhoneSize').prop("selectedIndex");
	    
	    $('.tabMenu').each(function() {
	        $('>li', this).removeClass('current').eq(index).addClass('current');
	     });
	    
	    $('.tabList > .tabArea').hide().eq(index).show();
	}).first().click();
});