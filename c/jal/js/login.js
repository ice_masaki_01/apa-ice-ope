var JLJS_LoginStatus ={
	hasCookie: function(key,value){
		if(!document.cookie){return false;}
		var data = document.cookie.split(';');
		for(var i=0; i<data.length; i++){
			var splitArray = data[i].split('=');
			if(splitArray[0].replace(/\s/g, '') == key){
				if(unescape(splitArray[1].replace(/\s/g, '')) == value){return true;}
			}
		}
		return false;
	},

	hasAutoLoginCookie: function(){
		return this.hasCookie("ACUCG09", "1");
	},

	isLogin:  function(){
		return this.hasCookie("LOGIN", "YES");
	},

	isMember: function(){
		return (this.isLogin() || this.hasAutoLoginCookie());
	},
	
	isAutoLogin: function(){
		return (!this.isLogin() && this.hasAutoLoginCookie());
	},
	
	setLoginNO: function(){
		document.cookie = 'LOGIN=NO;domain=.jal.co.jp;path=/;';
	}
};

function Hantei() {
	return JLJS_LoginStatus.isMember();
}

function setAction() {
	var v_frm, v_URL, v_value, v_element, f, v_id, v_html;
	if ( arguments.length > 2 ) {
		v_frm = arguments[0];
		v_URL = arguments[1];
		v_value = arguments[2];
		v_element = "flg_fun_no";
		f = document.forms[v_frm];
		if ( Hantei()) {
			f.action = v_URL;
			f.elements[v_element].value = v_value;
		}
	}
	else {
		v_id = arguments[0];
		v_html = arguments[1];
		if ( document.getElementById(v_id) ) {
			if ( Hantei()) {
				document.getElementById( v_id ).innerHTML = v_html;
				document.getElementById( v_id ).style.display = "";
			}
		}
	}
}

function setDispatcherAction(formName, memUrl, memPattern, gstUrl, gstPattern) {
	if(Hantei()) {
		var frm = document.forms[formName];
		frm.action = "https://www121.jal.co.jp/JmbWeb/JR/Dispatcher_ja.do";
		frm.elements["memberBackUrl"].value = memUrl;
		frm.elements["memberPattern"].value = memPattern;
		frm.elements["guestBackUrl"].value = gstUrl;
		frm.elements["guestPattern"].value = gstPattern;
	}
}

function confirm_logout() {
	if ( confirm(
		"\u30ED\u30B0\u30A2\u30A6\u30C8\u3057\u307E\u3059\u304B\uFF1F"
	)) return true;
	else return false;
}

function JLJS_ChangeProtocol(anc, option) {
	
	var href = anc.href;
	
	if(Hantei()) {
		href = href.replace(/^http:/, "https:");
	}
	
	var target = anc.target;

	if(target && target != "_self") {
		if(option) {
			window.open(href, target, option);
		} else {
			window.open(href, target);
		}
	} else {
		location.href = href;
	}
}

function JLJS_ChangeProtocolString(url) {
	var obj = new Object();
	obj.href = url;
	
	JLJS_ChangeProtocol(obj);
}

function JLJS_ChangeLoginHref(id, url) {
	if(Hantei()) {
		var ele = document.getElementById(id);
		
		if(ele) {
			ele.href = url;
		}
	}
}

/*
function JLJS_LinkToHome(){
	if( Hantei()) {
		if( confirm_logout())
			document.location.href = "https://www121.jal.co.jp/JmbWeb/JR/LogOut_ja.do";
	}
	else
		document.location.href = "/";
}
*/