function JLJS_domtourDPControl(){
	this.CLS_REL_WRAP = ".JS_dtdp_rel_wrap";
	this.CLS_WRAP_DEP = ".JS_dtdp_wrap_dep";
	this.CLS_WRAP_ARR = ".JS_dtdp_wrap_arr";
	this.CLS_DEP = ".JS_dtdp_dep";
	this.CLS_ARR = ".JS_dtdp_arr";
	this.CLS_MON = ".JS_dtdp_mon";
	this.CLS_DAY = ".JS_dtdp_day";
	this.CLS_CAL = ".floatCalIcon";	
	this.CLS_LB_ARR = ".JS_dtdp_lb_arr";
	this.CLS_DISNON = "disNon";
	this.KBN_SEN = "sen";
	this.KBN_CHI = "chi";
	this.KBN_BAB = "bab";
	
	this.$frm;
	this.$blMem;
	this.$blFli;
	this.$blStayDate;
	this.$blStayFaci;
	this.$blStayCond;
	this.$blStayOthe;
	this.$bl1st;
	this.$bl2nd;
	
	this.$nodeMemAdu;
	this.$nodeMemSen;
	this.$nodeMemChi;
	this.$nodeMemBab;
	this.$nodeTtlChi;
	this.$nodeBtnChi;
	this.$nodeIptChi;
	this.$nodeAir;
	this.$nodeTriDep;
	this.$nodeTriArr;
	this.$nodeFlg1DepMon;
	this.$nodeFlg1ArrMon;
	this.$nodeFlg1TriArr;
	this.$nodeFliAdu;
	this.$nodeFliSen;
	this.$nodeFliChi;
	this.$nodeFliBab;
	this.$nodeAduMoreList = [];
	this.$nodeStayNotice;
	this.$nodeStayMon;
	this.$nodeStayDay;
	this.$nodeStayNights;
	this.$nodeStayDateAdu;
	this.$nodeStayDateSen;
	this.$nodeStayDateChi;
	this.$nodeStayDateBab;
	this.$nodeStayPref;
	this.$nodeStayArea;
	this.$nodeKeyword;
	this.$nodeHotelId;
	this.$nodeHotelName;
	this.$nodeStayRoom;
	this.$nodeRoomMore2;
	this.$nodeStayFaciAdu;
	this.$nodeStayFaciSen;
	this.$nodeStayFaciChi;
	this.$nodeStayFaciBab;
	this.$nodeChildRoomCond;
	this.$nodeStayCondChi;
	this.$nodeFliLbAdu;
	this.$nodeFliLbSen;
	this.$nodeFliLbChi;
	this.$nodeFliLbBab;
	this.$nodeStayDateLbAdu;
	this.$nodeStayDateLbSen;
	this.$nodeStayDateLbChi;
	this.$nodeStayDateLbBab;
	this.$nodeStayFaciLbAdu;
	this.$nodeStayFaciLbSen;
	this.$nodeStayFaciLbChi;
	this.$nodeStayFaciLbBab;
	this.$nodeDomain;
	
	this.$chkDiff;
	this.$chkFlg3;
	this.$chkFlg4;
	this.$chkFlg5;
	this.$chkFlg6;
	this.$chkDiffRoom;
	this.$opBoxDiff;
	this.$opBoxFlg3;
	this.$opBoxFlg4;
	this.$opBoxFlg5;
	this.$opBoxFlg6;
		
	this.setupTime = $.now();
	this.dsF1 = {};
	this.dsStay;
	this.offset = 7;
	this.offsetCal = 1;
	this.interval = 1;
	this.topPriorityAirport = "";
	this.defaultDepAirport = "";
	this.defaultArrAirport = "";
	this.defaultKeyword = "宿泊施設名またはプラン名";
	this.defaultPrefCode = "130000";
	this.defaultAreaCode = "137100";
	this.defaultDomain = "EJALTOURS";
	this.domain = "";
	this.initValue = {};
	this.isInitValue = false;
	this.isAutoSelAirport = true;
	this.isDomainSetting = true;
	this.isLink = true;
	this.isSimple = false;
	this.isHotel = false;
	this.isExec = true;
	this.ckKey = "dtDpTMSearch";
	this.ckVal = {};
	this.rtDomainList = ["JALHOME", "JAL.CO.JP", "DOMTOUR"];
	this.rtDomainSuffix_7_27 = "_7_27";
	this.toggleSpeed = 300;
}

JLJS_domtourDPControl.prototype = {
	init: function(){
		this.cleanModule();
		this.setNode();
		this.setInitValue();
		this.setOffSet();
		this.setAirport();
		this.isSimple = this.$chkDiff[0]? false: true;
		this.isHotel = $("#JS_dtdp_hotel")[0]? true: false;
		// JLJS.preloadImage("/common_rn/img/btn_childtoggle_003_o.gif");
	},
	
	beforeSetup: function(){},
	setup: function(){
		this.$frm = $("form[name=dp_search]");
		if(!this.$frm[0]){return;}
		
		this.init();
		this.beforeSetup();
		this.setupMember();
		this.setupFlight();	
		this.setupStay();
		this.setupOther();
		this.setupSubmit();
		this.afterSetup();
		
		JLJS_DTDPOpt.setup();
	},
	afterSetup: function(){},
	
	setupMember: function(){
		var self = this;
		this.setValuePickNode(this.$blMem);
		this.$nodeMemAdu.on("change", function(){
			self.changeMemberAdult();
			self.changeStayDateAdult();
			self.changeStayRoom();
			self.displayStayCondition.fromMemAdult();
		});
		this.$nodeMemSen.on("change", function(){
			self.changeMemberChildren(self.KBN_SEN);
			self.changeStayDateChildren(self.KBN_SEN);
		});
		this.$nodeMemChi.on("change", function(){
			self.changeMemberChildren(self.KBN_CHI);
			self.changeStayDateChildren(self.KBN_CHI);
			self.displayStayCondition.fromMemChild();
		});
		this.$nodeMemBab.on("change", function(){
			self.changeMemberChildren(self.KBN_BAB);
			self.changeStayDateChildren(self.KBN_BAB);
		});
		this.$nodeBtnChi.on("click", function(){
			self.toggleInputChild();
		});
		this.changeMemberAdult();
		this.changeMemberChildren(this.KBN_SEN);
		this.changeMemberChildren(this.KBN_CHI);
		this.changeMemberChildren(this.KBN_BAB);
		this.setTotalChild();
		if("childrenInputArea" in this.initValue){
			this.$nodeBtnChi.removeClass("childOpen").addClass("childClose");
			this.$nodeIptChi.css({"display": "block"});
		}
	},
	
	setupFlight: function(){
		this.setValuePickNode(this.$blFli);
		this.setValueAirport();
		this.setupFlightGroup();
		this.setupFlightDate();
		this.setupFlightAirport();
	},
	
	setupFlightGroup: function(){
		var self = this;
		this.$chkDiff.on("change", function(){
			self.$opBoxDiff.slideToggle(self.toggleSpeed);
		});
		this.$chkFlg3.on("change", function(){
			self.$opBoxFlg3.slideToggle(self.toggleSpeed);
		});
		this.$chkFlg4.on("change", function(){
			self.$opBoxFlg4.slideToggle(self.toggleSpeed);
		});
		this.$chkFlg5.on("change", function(){
			self.$opBoxFlg5.slideToggle(self.toggleSpeed);
		});
		this.$chkFlg6.on("change", function(){
			self.$opBoxFlg6.slideToggle(self.toggleSpeed);
		});
		var disBlo = {"display": "block"};
		if(this.$chkDiff.prop("checked")){this.$opBoxDiff.css(disBlo);}
		if(this.$chkFlg3.prop("checked")){this.$opBoxFlg3.css(disBlo);}
		if(this.$chkFlg4.prop("checked")){this.$opBoxFlg4.css(disBlo);}
		if(this.$chkFlg5.prop("checked")){this.$opBoxFlg5.css(disBlo);}
		if(this.$chkFlg6.prop("checked")){this.$opBoxFlg6.css(disBlo);}
	},
	
	setupFlightDate: function(){
		var self = this;
		this.$blFli.find(this.CLS_REL_WRAP).each(function(i){
			var $wrap = $(this);
			var $wrapDep = $wrap.find(self.CLS_WRAP_DEP);
			var $nodeDepMon = $wrapDep.find(self.CLS_MON);
			var $nodeDepDay = $wrapDep.find(self.CLS_DAY);
			var $nodeDepCal = $wrapDep.find(self.CLS_CAL);
			var $wrapArr = $wrap.find(self.CLS_WRAP_ARR);
			var $nodeArrMon = $wrapArr.find(self.CLS_MON);
			var $nodeArrDay = $wrapArr.find(self.CLS_DAY);
			var $nodeArrCal = $wrapArr.find(self.CLS_CAL);
			var dsDep = self.setDefaultDate($nodeDepMon, $nodeDepDay);
			var dsArr = self.setDefaultDate($nodeArrMon, $nodeArrDay, self.isLink);
			self.setupCalendar($nodeDepCal, {board: {month: $nodeDepMon[0], day: $nodeDepDay[0]}, arrival: {month: $nodeArrMon[0], day: $nodeArrDay[0]}});
			self.setupCalendar($nodeArrCal, {board: {month: $nodeArrMon[0], day: $nodeArrDay[0]}}, self.isLink);
			if(i==0){
				self.$nodeFlg1DepMon = $nodeDepMon;
				self.$nodeFlg1ArrMon = $nodeArrMon;
				self.dsF1 = {baseDs: dsDep, linkDs: dsArr};
				$nodeDepMon.on("change", {baseDs: dsDep, linkDs: dsArr}, function(e){
					self.changeMonth(e.data);
					self.setStayDate();
					self.setStayNights();
				});
				$nodeDepDay.on("change", {baseDs: dsDep, linkDs: dsArr}, function(e){
					self.changeDay(e.data);
					self.setStayDate();
					self.setStayNights();
				});
				$nodeArrMon.on("change", {baseDs: dsArr}, function(e){
					self.changeMonth(e.data);
					self.setStayNights();
				});
				$nodeArrDay.on("change", {baseDs: dsArr}, function(e){
					self.changeDay(e.data);
					self.setStayNights();
				});
			}else{
				$nodeDepMon.on("change", {baseDs: dsDep, linkDs: dsArr}, function(e){
					self.changeMonth(e.data);
				});
				$nodeDepDay.on("change", {baseDs: dsDep, linkDs: dsArr}, function(e){
					self.changeDay(e.data);
				});
				$nodeArrMon.on("change", {baseDs: dsArr}, function(e){
					self.changeMonth(e.data);
				});
			}
		});
	},
	
	setupFlightAirport: function(){
		var self = this;
		this.$nodeTriDep.on("change", function(){
			self.changeDep($(this));
		});
		this.$nodeTriArr.on("change", function(){
			self.changeArr($(this));
		});
		this.$nodeFlg1TriArr.on("change", function(){
			self.changeF1gArr($(this));
		});
		this.$nodeTriDep.change();
		this.$nodeTriArr.each(function(){
			var $node = $(this);
			var name = $node.closest(self.CLS_REL_WRAP).find(self.CLS_DEP).attr("name");
			if(name in self.initValue){return true;}
			$node.change();
		});
	},
	
	setupStay: function(){
		this.setValuePickNode(this.$blStayDate);
		this.setupStayDate();
		this.setupStayDateMember();
		
		this.setValuePickNode(this.$blStayFaci);
		this.setupStayPrefArea();
		this.setupStayKeyword();
		this.setupStayHotel();
		this.setupStayRoom();
		this.setupStayFaciMember();
		
		this.displayStayCondition.fromMemAdult();
		this.setValuePickNode(this.$blStayCond);
	},
	
	setupStayDate: function(){
		var self = this;
		this.dsStay = this.setDefaultDate(this.$nodeStayMon, this.$nodeStayDay);
		var $wrap = this.$blStayDate.find(this.CLS_REL_WRAP);
		var $nodeStayCal = $wrap.find(this.CLS_CAL);
		this.setupCalendar($nodeStayCal, {board: {month: this.$nodeStayMon[0], day: this.$nodeStayDay[0]}});
		this.$nodeStayMon.on("change", function(e){
			self.changeMonth({baseDs: self.dsStay});
		});
		if("nights" in this.initValue){
		}else{
			this.setStayNights();
		}
		if("stay_month" in this.initValue && "stay_day" in this.initValue){
		}else{
			this.setStayDate();
		}
	},
	
	setupStayDateMember: function(){
		var self = this;
		this.$nodeStayDateAdu.on("change", function(){
			self.changeStayDateAdult();
			self.changeStayRoom();
		});
		this.$nodeStayDateSen.on("change", function(){
			self.changeStayDateChildren(self.KBN_SEN);
		});
		this.$nodeStayDateChi.on("change", function(){
			self.changeStayDateChildren(self.KBN_CHI);
			self.displayStayCondition.fromStayDateChi();
		});
		this.$nodeStayDateBab.on("change", function(){
			self.changeStayDateChildren(self.KBN_BAB);
		});
		this.changeStayDateAdult();
		this.changeStayDateChildren(this.KBN_SEN);
		this.changeStayDateChildren(this.KBN_CHI);
		this.changeStayDateChildren(this.KBN_BAB);
	},
	
	setupStayPrefArea: function(){
		var self = this;
		this.$nodeStayPref.on("change", function(){
			self.changePref($(this).val());
		});
		if("stay_prefecture_cd" in this.initValue){
			var prefCode = this.initValue.stay_prefecture_cd;
			if(this.$nodeStayPref.children("[value=" + prefCode + "]")[0]){
				var areaCode = "";
				if("stay_area_cd" in this.initValue){
					areaCode = this.initValue.stay_area_cd;
					if(this.$nodeStayArea.children("[value=" + areaCode + "]")[0]){
						this.changePref(prefCode, areaCode);
						return;
					}
				}
				this.changePref(prefCode, areaCode);
				return;
			}
		}
		this.changeF1gArr();
	},
	
	setupStayKeyword: function(){
		var self = this;
		var pfOn = "pfOn";
		this.$nodeKeyword.on({
			"focus": function(){
				if(self.$nodeKeyword.val()==self.defaultKeyword){
					self.$nodeKeyword.val("");
					self.$nodeKeyword.removeClass(pfOn);
				}
			},
			"blur" : function(){
				if(self.$nodeKeyword.val()=="" || self.$nodeKeyword.val()==self.defaultKeyword){
					self.$nodeKeyword.addClass(pfOn);
					self.$nodeKeyword.val(self.defaultKeyword);
				}
			}
		});
		if(this.$nodeKeyword.val()=="" || this.$nodeKeyword.val()==this.defaultKeyword){
			this.$nodeKeyword.val(this.defaultKeyword);
		}else{
			this.$nodeKeyword.removeClass(pfOn);
		}
	},
	
	setupStayHotel: function(){
		if(!this.isHotel){
			this.$nodeHotelId.val("");
			return;
		}
		var self = this;
		var isSetup = true;
		var checkList = ["stay_prefecture_cd", "hotel_id", "hotel_name"];
		$.each(checkList, function(i, val){
			if(val in self.initValue){return true;}
			isSetup = false;
			return false;
		});
		if(!isSetup){
			this.$nodeHotelId.val("");
			return;
		}
		this.$nodeHotelName.html(this.initValue["hotel_name"]);
		JLJS_DTDPHtlFilt.setup();
	},
	
	setupStayRoom: function(){
		var self = this;
		this.$nodeStayRoom.on("change", function(){
			self.changeStayRoom();
			self.displayStayCondition.fromStayRoom();
		});
		this.$chkDiffRoom.on("change", function(){
			self.$opBoxDiffRoom.slideToggle(self.toggleSpeed);
			self.displayStayCondition.fromStayDiffRoom();
		});
		this.changeStayRoom();
		if(this.$chkDiffRoom.prop("checked")){
			this.$opBoxDiffRoom.css({"display": "block"});
		}
	},
	
	setupStayFaciMember: function(){
		var self = this;
		this.$nodeStayFaciChi.on("change", function(){
			self.displayStayCondition.fromStayFaciChi();
		});
	},
	
	setupOther: function(){
		this.setValuePickNode(this.$blStayOthe);
	},
	
	setupCalendar: function($nodeCal, nodes, isLink){
		if(!JLJS_CalNav.used){
			$nodeCal.addClass("hidden");
			return;
		}
		var self = this;
		$nodeCal.on("click", function(){
			var start = self.offsetCal;
			if(isLink){start = start + self.interval;}
			var opt = {start: start, end: 365, startNoReserveMonthDisplay: false, dayOfWeekFlag: true, dayRange: self.interval, nextMonthNum: 1};
			var prm = {target: this, option: opt, frm: nodes};
			JLJS_CalNav.calDisplay(prm);
		});
	},
	
	setupSubmit: function(){
		var self = this;
		$("#JS_dtdp_btn_next").on("click", function(e){
			self.$bl1st.addClass(self.CLS_DISNON);
			self.$bl2nd.removeClass(self.CLS_DISNON);
			e.preventDefault();
		})
		$("#JS_dtdp_btn_return").on("click", function(e){
			self.$bl2nd.addClass(self.CLS_DISNON);
			self.$bl1st.removeClass(self.CLS_DISNON);
			e.preventDefault();
		})
		$("#JS_dtdp_btn_submit").on("click", function(e){
			self.submitForm();
			e.preventDefault();
		})
	},
	
	setNode: function(){
		var clsAdu = ".JS_dtdp_adult";
		var clsSen = ".JS_dtdp_senior";
		var clsChi = ".JS_dtdp_child";
		var clsBab = ".JS_dtdp_baby";
		var clsLbAdu = ".JS_dtdp_lb_adult";
		var clsLbSen = ".JS_dtdp_lb_senior";
		var clsLbChi = ".JS_dtdp_lb_child";
		var clsLbBab = ".JS_dtdp_lb_baby";
		
		this.$blMem = $("#JS_dtdp_bl_member");
		this.$blFli = $("#JS_dtdp_bl_flight");
		this.$blStayDate = $("#JS_dtdp_bl_stayDate");
		this.$blStayFaci = $("#JS_dtdp_bl_stayFaci");
		this.$blStayCond = $("#JS_dtdp_bl_stayCond");
		this.$blStayOthe = $("#JS_dtdp_bl_other");
		this.$bl1st = $("#JS_dtdp_bl_first");
		this.$bl2nd = $("#JS_dtdp_bl_second");
		
		this.$nodeMemAdu = this.getJqObjByName("adult_num");
		this.$nodeMemSen = this.getJqObjByName("senior_num");
		this.$nodeMemChi = this.getJqObjByName("child_num");
		this.$nodeMemBab = this.getJqObjByName("baby_num");
		this.$nodeTtlChi = $("#JS_dtdp_total_child");
		this.$nodeBtnChi = $("#JS_dtdp_btn_child");
		this.$nodeIptChi = $("#JS_dtdp_ipt_child");
		this.$nodeAir = this.$blFli.find(".JS_dtdp_airport");
		this.$nodeTriDep = this.$blFli.find(".JS_dtdp_tri_dep");
		this.$nodeTriArr = this.$blFli.find(".JS_dtdp_tri_arr");
		this.$nodeFlg1TriArr = this.getJqObjByName("dep_arrivalAirport1");
		this.$nodeFliAdu = this.$blFli.find(clsAdu);
		this.$nodeFliSen = this.$blFli.find(clsSen);
		this.$nodeFliChi = this.$blFli.find(clsChi);
		this.$nodeFliBab = this.$blFli.find(clsBab);
		this.$nodeAduMoreList = [
			this.$frm.find(".JS_dtdp_adult_more2"),
			$("#JS_dtdp_adult_more3"),
			$("#JS_dtdp_adult_more4"),
			$("#JS_dtdp_adult_more5"),
			$("#JS_dtdp_adult_more6")
		];
		this.$nodeStayNotice = this.$frm.find(".JS_dtdp_stay_notice");
		this.$nodeStayMon = this.getJqObjByName("stay_month");
		this.$nodeStayDay = this.getJqObjByName("stay_day");
		this.$nodeStayNights = this.getJqObjByName("nights");
		this.$nodeStayDateAdu = this.getJqObjByName("stay_adult_num");
		this.$nodeStayDateSen = this.getJqObjByName("stay_senior_num");
		this.$nodeStayDateChi = this.getJqObjByName("stay_child_num");
		this.$nodeStayDateBab = this.getJqObjByName("stay_baby_num");
		this.$nodeStayPref = this.getJqObjByName("stay_prefecture_cd");
		this.$nodeStayArea = this.getJqObjByName("stay_area_cd");
		this.$nodeKeyword = this.getJqObjByName("stay_keyword");
		this.$nodeHotelId = this.getJqObjByName("hotel_id");
		this.$nodeHotelName = $("#JS_dtdp_hotel_name");
		this.$nodeStayRoom = this.getJqObjByName("room_num");
		this.$nodeRoomMore2 = $("#JS_dtdp_room_more2");
		this.$nodeStayFaciAdu = this.getJqObjByName("room1_adult_num");
		this.$nodeStayFaciSen = this.getJqObjByName("room1_senior_num");
		this.$nodeStayFaciChi = this.getJqObjByName("room1_child_num");
		this.$nodeStayFaciBab = this.getJqObjByName("room1_baby_num");
		this.$nodeChildRoomCond = $("#JS_dtdp_child_stayCond");
		this.$nodeStayCondChi = this.$blStayCond.find(clsChi);
		this.$nodeFliLbAdu = this.$blFli.find(clsLbAdu);
		this.$nodeFliLbSen = this.$blFli.find(clsLbSen);
		this.$nodeFliLbChi = this.$blFli.find(clsLbChi);
		this.$nodeFliLbBab = this.$blFli.find(clsLbBab);
		this.$nodeStayDateLbAdu = this.$blStayDate.find(clsLbAdu);
		this.$nodeStayDateLbSen = this.$blStayDate.find(clsLbSen);
		this.$nodeStayDateLbChi = this.$blStayDate.find(clsLbChi);
		this.$nodeStayDateLbBab = this.$blStayDate.find(clsLbBab);
		this.$nodeStayFaciLbAdu = this.$blStayFaci.find(clsLbAdu);
		this.$nodeStayFaciLbSen = this.$blStayFaci.find(clsLbSen);
		this.$nodeStayFaciLbChi = this.$blStayFaci.find(clsLbChi);
		this.$nodeStayFaciLbBab = this.$blStayFaci.find(clsLbBab);
		this.$nodeDomain = this.getJqObjByName("domain");
		
		this.$chkDiff = this.getJqObjByName("flight_condition");
		this.$chkFlg3 = this.getJqObjByName("flight_condition_flg2");
		this.$chkFlg4 = this.getJqObjByName("flight_condition_flg3");
		this.$chkFlg5 = this.getJqObjByName("flight_condition_flg4");
		this.$chkFlg6 = this.getJqObjByName("flight_condition_flg5");
		this.$chkDiffRoom = this.getJqObjByName("every_room_differ_flg");
		this.$opBoxDiff = $("#JS_dtdp_opBox_fli2");
		this.$opBoxFlg3 = $("#JS_dtdp_opBox_fli3");
		this.$opBoxFlg4 = $("#JS_dtdp_opBox_fli4");
		this.$opBoxFlg5 = $("#JS_dtdp_opBox_fli5");
		this.$opBoxFlg6 = $("#JS_dtdp_opBox_fli6");
		this.$opBoxDiffRoom = $("#JS_dtdp_opBox_room_diff");
	},
		
	setInitValue: function(){
		var self = this;
		var presetList = {
			DOMAIN: "domain",
			domain: "",
			topPriorityAirport: "",
			defaultDepAirport: "",
			defaultArrAirport: "",
			isDomainSetting: "",
			isAutoSelAirport: ""
		};
		$.each(presetList, function(key, val){
			if(key in self.initValue){
				if(!val){val = key;}
				self[val] = self.initValue[key];
				delete self.initValue[key];
			}
		});
		this.setDefaultAirport();
		
		var initValueCk = JLJS_CookieMgr.getMultiCookie(this.ckKey);
		if(initValueCk){
			$.each(initValueCk, function(key){
				self.initValue[key] = initValueCk[key];
			});
		}
		$.each(self.initValue, function(key){
			self.isInitValue = true;
			return false;
		});
	},
	
	setOffSet: function(){
		var offsetCustom = this.getInitValueOffset();
		if(offsetCustom){this.offset = offsetCustom;}
	},
	
	setAirport: function(val){
		var airportList = this.getAirport();
		this.createOption(this.$nodeAir, airportList, val);
	},
	
	setDefaultDate: function($monNode, $dayNode, isLink){
		var ds = new JLJS_DateSelector($monNode[0], $dayNode[0]);
		if($monNode.attr("name") in this.initValue && $dayNode.attr("name") in this.initValue){
			ds.setDateBySelecter();
		}else{
			var offset = this.offset;
			if(isLink){offset = offset + this.interval;}
			ds.applyOffset("+" + offset + "d");
		}
		ds.dayOfWeekFlag = true;
		ds.setDay();
		return ds;
	},
	
	setDefaultAirport: function(){
		if(this.defaultDepAirport){return;}
		var areaAirport = JLJS_CookieMgr.get("areaAirport");
		if(areaAirport){
			this.defaultDepAirport = areaAirport;
			return;
		}
		if(!this.isAutoSelAirport){return;}
		var sessionAirport = JLJS_CookieMgr.getMultiCookie("sessionAirport");
		if(sessionAirport){
			var autoSelAirport = sessionAirport.autoSelAirport;
			if(autoSelAirport){
				this.defaultDepAirport = JLJS_RltnlLst.airToAir_dp[autoSelAirport] || autoSelAirport;
			}
		}
	},
	
	setValuePickNode: function($bl){
		if(!this.isInitValue){return;}
		var self = this;
		var $node = $bl.find("[name]");
		$node.each(function(){
			self.setValueDefault($(this));
		});
	},
	
	setValueDefault: function($node){
		var name = $node.attr("name");
		var type = $node.attr("type");
		var tagName = $node[0].tagName.toLowerCase();
		if(name in this.initValue){
			if(tagName=="select"){
				$node.children("[value=" + this.initValue[name] + "]").eq(0).prop("selected", true);
				return;
			}
			if(type=="checkbox" && this.initValue[name]==1){
				$node.prop("checked", true);
				return;
			}
			$node.val(this.initValue[name]);
		}
	},
	
	setValueAirport: function(){
		if(this.topPriorityAirport){
			this.$nodeTriDep.val(this.topPriorityAirport);
			return;
		}
		if(!this.defaultDepAirport){return;}
		var self = this;
		this.$nodeTriDep.each(function(){
			var $node = $(this);
			var name = $node.attr("name");
			if(name in self.initValue){return true;}
			$node.children("[value=" + self.defaultDepAirport + "]").eq(0).prop("selected", true);
		});
		if(!this.defaultArrAirport){return;}
		this.$nodeTriArr.each(function(){
			var $node = $(this);
			var name = $node.attr("name");
			if(name in self.initValue){return true;}
			$node.children("[value=" + self.defaultArrAirport + "]").eq(0).prop("selected", true);
		});
	},
	
	setValue: function(selector, val){
		$(selector).val(val);
		return;
	},
	
	setTotalChild: function(){
		var sen = +this.$nodeMemSen.val();
		var chi = +this.$nodeMemChi.val();
		var bab = +this.$nodeMemBab.val();
		var total = sen + chi + bab;
		this.$nodeTtlChi.html(total);
	},
	
	setStayDate: function(){
		var d = {baseDs: this.dsF1.baseDs, linkDs: this.dsStay}
		this.adjustToDate(d);
	},
	
	setStayNights: function(){
		var d = this.dsF1;
		var depTime = (new Date((d.baseDs.Date).getTime())).setHours(0,0,0,0);
		var arrTime = (new Date((d.linkDs.Date).getTime())).setHours(0,0,0,0);
		var per = (arrTime-depTime)/86400000;
		if(per<1){
			per = 1;
		}else if(per>13){
			per = 13;
		}
		this.$nodeStayNights.val(per);
	},
	
	getJqObjByName: function(name){
		return this.$frm.find("[name=" + name + "]");
	},
	
	getInitValueOffset: function(){
		var offset;
		var offsetDate = this.initValue.offsetDate;
		if(offsetDate){offset = offsetDate;}
		var customDate = this.initValue.customDate;
		if(!customDate || !customDate.endDate || !customDate.constDate){return offset;}
		
		var end= customDate.endDate.split("/");
		var con= customDate.constDate.split("/");
		if(end.length!=3 || con.length!=3){return offset;}
		
		var setupTime = (new Date(this.setupTime)).setHours(0,0,0,0);
		var endTime = (new Date(end[0], end[1]-1, end[2])).getTime();
		var conTime = (new Date(con[0], con[1]-1, con[2])).getTime();
		if(endTime<setupTime || conTime<setupTime){return offset;}
		
		offset = (conTime-setupTime)/86400000;
		return offset;
	},
	
	getAirport: function(){
		var airportMajor = [];
		var airport = JLJS_DTDPConf.airport;
		$.each(airport, function(i, valHash){
			if("order" in valHash){airportMajor.push(valHash);}
		});
		airportMajor.sort(function(a, b){return (a.order<b.order)? -1: 1;});
		var mergeAirport = $.merge(airportMajor, airport);
		return mergeAirport;
	},
	
	changeMemberAdult: function(){
		var val = this.$nodeMemAdu.val();
		var optInfo = {sta: 1, end: val};
		this.createOptionWithFor(this.$nodeFliAdu, optInfo);
		this.createOptionWithFor(this.$nodeStayDateAdu, optInfo, val);
		this.$nodeFliLbAdu.html(val);
		this.$nodeStayDateLbAdu.html(val);
		if(this.isSimple){return;}
		
		var self = this;
		if(val==1){
			this.$nodeStayNotice.eq(0).removeClass(this.CLS_DISNON);
			this.$nodeStayNotice.eq(1).addClass(this.CLS_DISNON);
		}else{
			this.$nodeStayNotice.eq(1).removeClass(this.CLS_DISNON);
			this.$nodeStayNotice.eq(0).addClass(this.CLS_DISNON);
		}
		$.each(this.$nodeAduMoreList, function(i, $node){
			$node.addClass(self.CLS_DISNON);
			if(i+1<val){$node.removeClass(self.CLS_DISNON);}
		});
	},
	
	changeMemberChildren: function(kbn){
		var $baseNode, $linkNode1, $linkNode2, $linkNodeLb1, $linkNodeLb2;
		if(kbn==this.KBN_SEN){
			$baseNode = this.$nodeMemSen;
			$linkNode1 = this.$nodeFliSen;
			$linkNode2 = this.$nodeStayDateSen;
			$linkNodeLb1 = this.$nodeFliLbSen;
			$linkNodeLb2 = this.$nodeStayDateLbSen;
		}else if(kbn==this.KBN_CHI){
			$baseNode = this.$nodeMemChi;
			$linkNode1 = this.$nodeFliChi;
			$linkNode2 = this.$nodeStayDateChi;
			$linkNodeLb1 = this.$nodeFliLbChi;
			$linkNodeLb2 = this.$nodeStayDateLbChi;
		}else if(kbn==this.KBN_BAB){
			$baseNode = this.$nodeMemBab;
			$linkNode1 = this.$nodeFliBab;
			$linkNode2 = this.$nodeStayDateBab;
			$linkNodeLb1 = this.$nodeFliLbBab;
			$linkNodeLb2 = this.$nodeStayDateLbBab;
		}
		var val = $baseNode.val();
		var optInfo = {sta: 0, end: val};
		this.createOptionWithFor($linkNode1, optInfo);
		this.createOptionWithFor($linkNode2, optInfo, val);
		$linkNodeLb1.html(val);
		$linkNodeLb2.html(val);
		if(val==0){
			$linkNode1.closest("li").addClass(this.CLS_DISNON);
			$linkNode2.closest("li").addClass(this.CLS_DISNON);
		}else{
			$linkNode1.closest("li").removeClass(this.CLS_DISNON);
			$linkNode2.closest("li").removeClass(this.CLS_DISNON);
		}
		this.setTotalChild();
	},
	
	changeStayDateAdult: function(){
		var val = this.$nodeStayDateAdu.val();
		var optInfo = {sta: 1, end: val};
		this.createOptionWithFor(this.$nodeStayFaciAdu, optInfo);
		this.$nodeStayFaciLbAdu.html(val);
		optInfo.suffix = "部屋";
		this.createOptionWithFor(this.$nodeStayRoom, optInfo);
	},
	
	changeStayDateChildren: function(kbn){
		var $baseNode, $linkNode1, $linkNodeLb1;
		if(kbn==this.KBN_SEN){
			$baseNode = this.$nodeStayDateSen;
			$linkNode1 = this.$nodeStayFaciSen;
			$linkNodeLb1 = this.$nodeStayFaciLbSen;
		}else if(kbn==this.KBN_CHI){
			$baseNode = this.$nodeStayDateChi;
			$linkNode1 = this.$nodeStayFaciChi;
			$linkNodeLb1 = this.$nodeStayFaciLbChi;
		}else if(kbn==this.KBN_BAB){
			$baseNode = this.$nodeStayDateBab;
			$linkNode1 = this.$nodeStayFaciBab;
			$linkNodeLb1 = this.$nodeStayFaciLbBab;
		}
		var val = $baseNode.val();
		var optInfo = {sta: 0, end: val};
		this.createOptionWithFor($linkNode1, optInfo);
		$linkNodeLb1.html(val);
		if(val==0){
			$linkNode1.closest("li").addClass(this.CLS_DISNON);
		}else{
			$linkNode1.closest("li").removeClass(this.CLS_DISNON);
		}
	},
	
	changeStayRoom: function(){
		var val = this.$nodeStayRoom.val();
		if(val==1){
			this.$nodeRoomMore2.addClass(this.CLS_DISNON);
		}else{
			this.$nodeRoomMore2.removeClass(this.CLS_DISNON);
		}
	},
	
	changeMonth: function(d){
		var ds = d.baseDs;
		ds.setDateBySelecter();
		ds.setDay();
		if(!d.linkDs){return;}
		this.adjustToDate(d, this.interval);
	},
	
	changeDay: function(d){
		var ds = d.baseDs;
		ds.setDateBySelecter();
		if(!d.linkDs){return;}
		this.adjustToDate(d, this.interval);
	},
	
	changeDateFromCalendar: function(kbn){
		if(kbn=="base"){
			this.dsF1.baseDs.setDateBySelecter();
			this.dsF1.linkDs.setDateBySelecter();
			this.setStayDate();
		}else if(kbn=="link"){
			this.dsF1.linkDs.setDateBySelecter();
		}
		this.setStayNights();
	},
	
	adjustToDate: function(d, interval){
		if(!interval){interval = 0;}
		var date = new Date(d.baseDs.Date.getFullYear(), d.baseDs.Date.getMonth(), d.baseDs.Date.getDate() + interval);
		d.linkDs.adjustToDate(date);
		d.linkDs.setDay();
	},
	
	changeDep: function($node){
		var val = $node.val();
		var text = $node.children(":selected").text();
		var $relWrap = $node.closest(this.CLS_REL_WRAP);
		$relWrap.find(this.CLS_ARR).val(val);
		$relWrap.find(this.CLS_LB_ARR).html(text);
	},
	
	changeArr: function($node){
		var val = $node.val();
		var $relWrap = $node.closest(this.CLS_REL_WRAP);
		$relWrap.find(this.CLS_DEP).children("[value=" + val + "]").eq(0).prop("selected", true);
	},
	
	changeF1gArr: function(){
		if(this.isHotel){return;}
		var val = this.$nodeFlg1TriArr.val();
		var stayPref = JLJS_DTDPConf.stayPref[val];
		if(!stayPref.code){return;}
		this.$nodeStayPref.val(stayPref.code);
		this.changePref(stayPref.code, stayPref.defaultAreaCode);
	},
	
	changePref: function(prefCode, defAreaCode){
		if(!prefCode){prefCode = this.defaultPrefCode;}
		var areaList = JLJS_DTDPConf.stayArea[prefCode];
		this.createOption(this.$nodeStayArea, areaList, defAreaCode);
		if(defAreaCode || defAreaCode==""){return;}
		this.$nodeStayArea.children("option").eq(1).prop("selected", true);
	},
	
	toggleInputChild: function(){
		if(this.$nodeIptChi.is(":animated")){return;}
		if(this.$nodeBtnChi.hasClass("childOpen")){
			this.$nodeBtnChi.addClass("childClose").removeClass("childOpen");
		}else{
			this.$nodeBtnChi.addClass("childOpen").removeClass("childClose");
		}
		this.$nodeIptChi.slideToggle(this.toggleSpeed);
	},
	
	displayStayCondition: {
		fromMemAdult: function(){
			this.fromMemChild();
		},
		
		fromMemChild: function(){
			var inst = JLJS_DTDPCtrl;
			var aduVal = inst.$nodeMemAdu.val();
			var chiVal = inst.$nodeMemChi.val();
			if(chiVal==0 || aduVal==1 && chiVal>0){
				this.display(chiVal);
				return;
			}
			this.fromStayDateChi();
		},
		
		fromStayDateChi: function(){
			var inst = JLJS_DTDPCtrl;
			var val = inst.$nodeStayDateChi.val();
			if(val==0){
				this.display(val);
				return;
			}
			this.fromStayRoom();
		},
		
		fromStayRoom: function(){
			var inst = JLJS_DTDPCtrl;
			var roomVal = inst.$nodeStayRoom.val();
			if(roomVal==1){
				var val = inst.$nodeStayDateChi.val();
				this.display(val);
				return;
			}
			this.fromStayDiffRoom();
		},
		
		fromStayDiffRoom: function(){
			var inst = JLJS_DTDPCtrl;
			if(inst.$chkDiffRoom.prop("checked")){
				this.fromStayFaciChi();
				return;
			}
			var val = inst.$nodeStayDateChi.val();
			this.display(val);
		},
		
		fromStayFaciChi: function(){
			var inst = JLJS_DTDPCtrl;
			var val = inst.$nodeStayFaciChi.val();
			this.display(val);
		},
		
		display: function(val){
			var inst = JLJS_DTDPCtrl;
			if(val==0){
				inst.$nodeStayCondChi.val(0);
				inst.$nodeChildRoomCond.addClass(inst.CLS_DISNON);
			}else{
				var optInfo = {sta: 0, end: val};
				inst.createOptionWithFor(inst.$nodeStayCondChi, optInfo);
				inst.$nodeChildRoomCond.removeClass(inst.CLS_DISNON);
			}
		}
	},
	
	createOption: function($node, list, val){
		if(!$node || !$node[0] || !list){return;}
		this.removeOption($node);
		$.each(list, function(i, valHash){
			$node.append($("<option>").html(valHash.name).val(valHash.code));
		});
		if(val){$node.children("[value=" + val + "]").eq(0).prop("selected", true);}
	},
	
	createOptionWithFor: function($node, optInfo, val){
		if(!$node || !$node[0] || !optInfo){return;}
		var sta = optInfo.sta;
		var end = optInfo.end;
		var sfx = optInfo.suffix || "名";
		this.removeOption($node);
		for(var i=sta; i<=end; i++){
			$node.append($("<option>").html(i+sfx).val(i));
		}
		if(val){$node.val(val);}
	},
	
	removeOption: function($node){
		$node.children("option").remove();
	},
	
	checkHotelInfo: function(){
		if(!this.isHotel){return true;}
		if(JLJS_DTDPHtlFilt.$filtBox && JLJS_DTDPHtlFilt.$filtBox[0]){
			var valList = [];
			var $chkNode = JLJS_DTDPHtlFilt.$filtBox.find(JLJS_DTDPHtlFilt.CLS_HOTEL_ID_CHK);
			$chkNode.each(function(){
				var $node = $(this);
				if($node.prop("checked")){valList.push($node.val());}
			});
			this.$nodeHotelId.val(valList.join(","));
		}
		if(this.$nodeHotelId.val()){return true;}
		
		alert("宿泊施設を選択してください。");
		return false;
	},
	
	cleanModule: function(){
		var $selectNode = this.$frm.find("select");
		$selectNode.each(function(){
			$(this).children("option").eq(0).prop("selected", true);
		});
		var $textNode = this.$frm.find("input[type=text]");
		$textNode.each(function(){
			$(this).val("");
		});
		var $checkboxNode = this.$frm.find("input[type=checkbox]");
		$checkboxNode.each(function(){
			$(this).prop("checked", false);
		});
		this.getJqObjByName("domain").val(this.defaultDomain);
	},
	
	cleanCheckbox: function(){
		var valRoom = this.$nodeStayRoom.val();
		if(valRoom==1){
			this.$chkDiffRoom.prop("checked", false);
			this.$opBoxDiffRoom.css({"display": "none"});
		}
		if(this.isSimple){return;}
		
		var clrList = [this.$chkDiff, this.$chkFlg3, this.$chkFlg4, this.$chkFlg5, this.$chkFlg6];
		var clrOpBoxList = [this.$opBoxDiff, this.$opBoxFlg3, this.$opBoxFlg4, this.$opBoxFlg5, this.$opBoxFlg6];
		var clrIdx = this.$nodeMemAdu.val()-1;
		for(var i=0; i<clrIdx; i++){
			if(!clrList[i].prop("checked")){
				clrIdx = i + 1;
				break;
			}
		}
		$.each(clrList, function(i){
			if(i<clrIdx){return true;}
			clrList[i].prop("checked", false);
			clrOpBoxList[i].css({"display": "none"});
		});
	},
	
	cleanKeyword: function(){
		if(this.$nodeKeyword.val()!=this.defaultKeyword){return;}
		this.$nodeKeyword.val("");
	},
	
	saveCookie: function(){
		this.ckVal = {};
		this.saveCookieMember();
		this.saveCookieFlight();
		this.saveCookieStay();
		JLJS_CookieMgr.setMultiCookie(this.ckKey, this.ckVal);
	},
	
	saveCookieMember: function(){
		var self = this;
		var $node = this.$blMem.find("[name]");
		$node.each(function(){
			self.setCookie($(this));
		});
		if(this.$nodeBtnChi.hasClass("childClose")){this.setCookie("childrenInputArea", 1);}
	},
	
	saveCookieFlight: function(){
		var self = this;
		var baseFliName = [
			"dep_boardMonth", "dep_boardDay", "dep_boardAirport", "dep_arrivalAirport",
			"arr_boardMonth", "arr_boardDay", "arr_boardAirport"];
		var baseMemName = [
			"flight_adult_num", "flight_senior_num", "flight_child_num", "flight_baby_num"];
		var baseName = $.merge($.merge([], baseFliName), baseMemName);
		$.each(baseFliName, function(i){self.setCookie(self.getJqObjByName(baseFliName[i]+"1"));});
		if(this.isSimple){return;}
		
		var memAdu = this.$nodeMemAdu.val();
		if(memAdu==1){return;}
		if(!this.$chkDiff.prop("checked")){return;}
		this.setCookie(this.$chkDiff);
		$.each(baseMemName, function(i){self.setCookie(self.getJqObjByName(baseMemName[i]+"1"));});
		$.each(baseName, function(i){self.setCookie(self.getJqObjByName(baseName[i]+"2"));});
		if(memAdu==2){return;}
		if(!this.$chkFlg3.prop("checked")){return;}
		this.setCookie(this.$chkFlg3);
		$.each(baseName, function(i){self.setCookie(self.getJqObjByName(baseName[i]+"3"));});
		if(memAdu==3){return;}
		if(!this.$chkFlg4.prop("checked")){return;}
		this.setCookie(this.$chkFlg4);
		$.each(baseName, function(i){self.setCookie(self.getJqObjByName(baseName[i]+"4"));});
		if(memAdu==4){return;}
		if(!this.$chkFlg5.prop("checked")){return;}
		this.setCookie(this.$chkFlg5);
		$.each(baseName, function(i){self.setCookie(self.getJqObjByName(baseName[i]+"5"));});
		if(memAdu==5){return;}
		if(!this.$chkFlg6.prop("checked")){return;}
		this.setCookie(this.$chkFlg6);
		$.each(baseName, function(i){self.setCookie(self.getJqObjByName(baseName[i]+"6"));});
	},
	
	saveCookieStay: function(){
		var self = this;
		var memAdu = this.$nodeMemAdu.val();
		this.setCookie(this.$nodeStayMon);
		this.setCookie(this.$nodeStayDay);
		this.setCookie(this.$nodeStayNights);
		if(!this.isSimple && memAdu>1){
			this.setCookie(this.$nodeStayDateAdu);
			this.setCookie(this.$nodeStayDateSen);
			this.setCookie(this.$nodeStayDateChi);
			this.setCookie(this.$nodeStayDateBab);
		}
		var stayFaciRoom = this.$nodeStayRoom.val();
		this.setCookie(this.$nodeStayPref);
		this.setCookie(this.$nodeStayArea);
		this.setCookie(this.$nodeKeyword);
		if(this.isHotel){
			this.setCookie("hotel_name", this.$nodeHotelName.html());
			this.setCookie(this.$nodeHotelId);
		}
		this.setCookie(this.$nodeStayRoom);
		if(stayFaciRoom>1 && this.$chkDiffRoom.prop("checked")){
			this.setCookie(this.$chkDiffRoom);
			this.setCookie(this.$nodeStayFaciAdu);
			this.setCookie(this.$nodeStayFaciSen);
			this.setCookie(this.$nodeStayFaciChi);
			this.setCookie(this.$nodeStayFaciBab);
		}
		this.setCookie(this.getJqObjByName("room_type"));
		this.setCookie(this.getJqObjByName("room_smokingType"));
		this.setCookie(this.getJqObjByName("room_mealType"));
		if(this.$nodeChildRoomCond.is(":visible")){
			this.$nodeStayCondChi.each(function(){self.setCookie($(this));});
		}
	},
	
	setCookie: function(obj, val){
		var name;
		if($.type(obj)=="string"){
			name = obj;
		}else{
			name = obj.attr("name");
			val = obj.val();
		}
		this.ckVal[name] = val;
	},
	
	setDomain: function(){
		var rewriteDomian = "";
		if(this.isDomainSetting){
			if(typeof(isAffliate)!="undefined" && isAffliate){
				rewriteDomian = JLJS_CookieMgr.get("jalt-jep");
				if(rewriteDomian){
					this.$nodeDomain.val(rewriteDomian);
					return;
				}
			}
		}
		var self = this;
		if(this.domain){this.$nodeDomain.val(this.domain);}
		$.each(this.rtDomainList, function(i, val){
			var val_7_27 = val + self.rtDomainSuffix_7_27;
			if(self.$nodeDomain.val()==val || self.$nodeDomain.val()==val_7_27){
				var setupTime = (new Date(self.setupTime)).setHours(0,0,0,0);
				var depTime = (new Date((self.dsF1.baseDs.Date).getTime())).setHours(0,0,0,0);
				var per = (depTime-setupTime)/86400000
				rewriteDomian = (7<=per && per<=27)? val_7_27: val;
				self.$nodeDomain.val(rewriteDomian);
				return false;
			}
		});
	},
	
	privateSubmitCheck: function(){return true;},
	
	submitForm: function(){
		if(!this.privateSubmitCheck){return;}
		if(!this.checkHotelInfo()){return;}
		this.cleanCheckbox();
		this.cleanKeyword();
		this.setDomain();
		this.saveCookie();
		this.$frm.submit();
	},
	
	setHotelInfo: function(arr, pref, id, name, area){
		if(!arr || !pref || !id || !name){return;}
		this.$nodeTriArr.val(arr).change();
		this.$nodeStayPref.val(pref);
		if(!area){area = "";}
		this.changePref(pref, area);
		this.$nodeHotelId.val(id);
		this.$nodeHotelName.html(name);
	},
	
	setupFromDomTM: {
		setup: function(domInitValue){
			var inst = JLJS_DTDPCtrl;
			inst.initValue = domInitValue;
			inst.setup();
		},
		
		setPriorityData: function(priorityData){
			var inst = JLJS_DTDPCtrl;
			inst.$nodeTriDep.val(priorityData);
			inst.$nodeTriDep.change();
		}
	}
};
var JLJS_DTDPCtrl = new JLJS_domtourDPControl();

function JLJS_domtourDPOption(){
	this.$frm;
}
JLJS_domtourDPOption.prototype = {
	setup: function(){
		this.$frm = $("form[name=dp_search_opt]");
		if(!this.$frm[0]){return;}
		
		this.$nodeOptLink = $("#JS_dtdp_opt_link");
		if(!this.$nodeOptLink[0]){return;}
		
		var self = this;
		this.$nodeOptLink.on("click", function(){self.submitForm();});
	},
	
	setPrm: function(){
		JLJS_DTDPCtrl.setDomain();
		$(".JS_dtdp_opt_node").each(function(){
			var $node = $(this);
			var name = $node.attr("name");
			var $tmNode = JLJS_DTDPCtrl.$frm.find("[name=" + name + "]");
			if(!$tmNode[0]){return true;}
			$node.val($tmNode.val());
		});
	},
	
	privateSubmitCheck: function(){return true;},
	
	submitForm: function(){
		if(!this.privateSubmitCheck){return;}
		this.setPrm();
		this.$frm.submit();
	}
};
var JLJS_DTDPOpt = new JLJS_domtourDPOption();

function JLJS_domtourDPModal(){
	this.idModal = "#JS_dtdp_modal";
	this.idOverlay = "#JS_dtdp_modal_overlay";
	this.clsClose = ".JS_dtdp_modal_close";
	this.$doc = $(document);
	this.$nodeModal;
	this.$nodeOverlay;
	this.$nodeClose;
	this.topPos = 100;
	this.CLS_DISNON = JLJS_DTDPCtrl.CLS_DISNON;
}

JLJS_domtourDPModal.prototype = {
	setup: function(){
		this.$nodeModal = $(this.idModal);
		this.$nodeOverlay = $(this.idOverlay);
		this.$nodeClose = $(this.clsClose);
		if(!this.$nodeModal[0] || !this.$nodeOverlay[0] || !this.$nodeClose[0]){return;}
		
		var self = this;
		this.$nodeClose.on("click", function(){self.close();});
	},
	
	open: function(arr, pref, id, name, area){
		JLJS_DTDPCtrl.setHotelInfo(arr, pref, id, name, area);
		var y = this.$doc.scrollTop() + this.topPos;
		this.$nodeModal.removeClass(this.CLS_DISNON).css({top: y});
		this.$nodeOverlay.removeClass(this.CLS_DISNON);
	},
	
	close: function(){
		this.$nodeOverlay.addClass(this.CLS_DISNON);
		this.$nodeModal.addClass(this.CLS_DISNON);
	}
};
var JLJS_DTDPModal = new JLJS_domtourDPModal();

function JLJS_domtourDPHotelFilter(){
	this.ID_FILTER_BOX = "#JS_dtdp_hotel_filter_box";
	this.ID_FILTER_BTN = "#JS_dtdp_hotel_filter_btn";
	this.CLS_HOTEL_ID_CHK = ".JS_dtdp_hotel_id_chk";
	this.$filBox;
	this.$filBtn;
	
	this.closeFlag = true;
}
JLJS_domtourDPHotelFilter.prototype = {
	setup: function(){
		this.$filtBtn = $(this.ID_FILTER_BTN);
		this.$filtBox = $(this.ID_FILTER_BOX);
		if(!this.$filtBtn[0] || !this.$filtBox[0]){return;}
		
		this.setInitValue();
		this.setEvent();
		this.$filtBtn.removeClass("disNon");
	},
	
	setInitValue: function(){
		var hotelIdCk = JLJS_DTDPCtrl.initValue.hotel_id;
		if(!hotelIdCk){return;}
		
		var self = this;
		var valList = hotelIdCk.split(",");
		this.$filtBox.find(this.CLS_HOTEL_ID_CHK).each(function(){
			$(this).prop("checked", false);
		});
		$.each(valList, function(i, val){
			var $node = self.$filtBox.find("[value=" + val +"]");
			if($node[0]){$node.prop("checked", true);}
		});
	},
	
	setEvent: function(){
		var self = this;
		this.$filtBtn.on("click", function(){self.toggle();});
		this.$filtBox.on("click", function(){self.closeFlag = false;});
		$(document).on("click", function(){
			self.close();
			self.closeFlag = true;
		});
	},
	
	toggle: function(){
		if(this.$filtBox.is(":visible")){
			this.close();
		}else{
			this.open();
			this.closeFlag = false;
		}
	},
	
	open: function(){
		var offset = this.$filtBtn.offset();
		this.$filtBox.css({"position": "absolute"});
		this.$filtBox.css({"top" : offset.top + 26});
		this.$filtBox.css({"left": offset.left});
		this.$filtBox.removeClass("disNon");
	},
	
	close: function(){
		if(this.$filtBox.is(":hidden")){return;}
		if(!this.closeFlag){return;}
		this.$filtBox.addClass("disNon");
	}
};
var JLJS_DTDPHtlFilt = new JLJS_domtourDPHotelFilter();

$(function(){
	if(JLJS_DTDPCtrl.isExec){
		JLJS_DTDPCtrl.setup();
		JLJS_DTDPModal.setup();
	}
});

JLJS_CalendarNavigation.prototype.returnDateAndCloseAfter = function(year, month, day){
	if(JLJS_CalNav.selectObj[0].month==JLJS_DTDPCtrl.$nodeFlg1DepMon[0]){
		JLJS_DTDPCtrl.changeDateFromCalendar("base");
	}else if(JLJS_CalNav.selectObj[0].month==JLJS_DTDPCtrl.$nodeFlg1ArrMon[0]){
		JLJS_DTDPCtrl.changeDateFromCalendar("link");
	}
};

$(window).on("unload", function(){});