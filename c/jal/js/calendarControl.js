
function JLJS_CalendarNavigation() {
	this.isEN = false;
	this.baseDate;
	this.start = 0;
	this.end;
	this.todayDateObj;
	this.todayYear;
	this.todayMonth;
	this.todayDay;
	this.todayYearMonth;
	this.dayOfWeekFlag = false;
	this.dayZeroFlag = false;
	this.dayRange = 4;
	this.paneNum = 3;
	this.festivalDay = true;
	this.successionDate = false;
	this.startNoReserveMonthDisplay = true;
	this.popupFlag = false;
	this.displaySpace = "dispCalendarSpace";
	this.displaySpaceWrite = "dispCalendarSpaceWrite";
	this.iframeSpace = "iframeCalendarSpace";
	this.selectObj = [];
	
	this.browser = {msid: false, firefox: false, safari: false, netscape: false, version: -1};
	this.used = this.isUsed();
	this.calIconElm;
	this.floatCloseFlg;
	this.nextMonthNum = 3;
	this.NEXT_MONTH_NUM_DEFAULT = 3;
	
	//this.ICON_W_SIZE = JLJS.env.isIE ? 23 : 20;
	this.ICON_W_SIZE = 20;
	this.ICON_H_SIZE = JLJS.env.isIE ? 18 : 20;
	this.WIN_W_SIZE = 940;
	this.CAL_W_SIZE = JLJS.env.isIE ? 583 : 580;
	this.DISP_CLASS = "floatCalDisp";
	this.ICON_CLASS = "floatCalIcon";
	
	this.OPT_DOM_TM = {start:0, end:365, startNoReserveMonthDisplay:false};
	this.OPT_INT_TM = {start:0, end:330, startNoReserveMonthDisplay:false};
	this.OPT_TOP_DOM_TM = {start:0, end:365, startNoReserveMonthDisplay:false, dayOfWeekFlag: true};
	this.OPT_TOP_INT_TM = {start:0, end:330, startNoReserveMonthDisplay:false, dayOfWeekFlag: true};

	this.OPT_TOP_INT_HM = {start:0, end:365, startNoReserveMonthDisplay:false, dayOfWeekFlag: true, dayZeroFlag: true};
	
	this.DEFAULT_OPTION = {
		isEN:false, baseDate:'', start:'0', end:'', dayOfWeekFlag:false, dayZeroFlag:false, dayRange:'4', paneNum:'3', festivalDay:true, successionDate:false, startNoReserveMonthDisplay:true, popupFlag:false, depLabel:null, arrLabel:null, yearLabelFlag:false};
}

JLJS_CalendarNavigation.prototype = {
	setup : function(options) {
		
		this.nextMonthNum = this.NEXT_MONTH_NUM_DEFAULT;
		
		for(var key in JLJS_CalNav.DEFAULT_OPTION) {
			JLJS_CalNav[key] = JLJS_CalNav.DEFAULT_OPTION[key];
		}
		
		for(var key in options) {
			if(options[key] != "" || typeof options[key] == "boolean"){
				JLJS_CalNav[key] = options[key];
			}
		}
		
		JLJS_CalNav.todayYear = JLJS_CalNav.todayDateObj.getFullYear();
		JLJS_CalNav.todayMonth = JLJS_CalNav.todayDateObj.getMonth();
		JLJS_CalNav.todayDay = JLJS_CalNav.todayDateObj.getDate();
		JLJS_CalNav.todayYearMonth = JLJS_CalNav.todayYear * 100 + JLJS_CalNav.todayMonth;
		
		if(JLJS_CalNav.baseDate){
			var baseDateArry = JLJS_CalNav.baseDate.match(/\d+/g);
			JLJS_CalWin.baseYear = parseInt(baseDateArry[0], 10);
			JLJS_CalWin.baseMonth = parseInt(baseDateArry[1], 10) - 1;
			JLJS_CalWin.baseDay = parseInt(baseDateArry[2], 10);
		}else{
			JLJS_CalWin.baseYear = JLJS_CalNav.todayDateObj.getFullYear();
			JLJS_CalWin.baseMonth = JLJS_CalNav.todayDateObj.getMonth();
			JLJS_CalWin.baseDay = JLJS_CalNav.todayDateObj.getDate();
			if(JLJS_CalNav.start < 0) {
				JLJS_CalWin.baseDay = 1;
				JLJS_CalNav.baseDate = 
								JLJS_CalWin.baseYear + "/" +
								(JLJS_CalWin.baseMonth + 1) + "/" +
								JLJS_CalWin.baseDay;
								
				JLJS_CalNav.start = 0;
			}
		}
		
		if(!JLJS_CalNav.end || JLJS_CalNav.end && parseInt(JLJS_CalNav.start) > parseInt(JLJS_CalNav.end)){
			var nowDateObj = JLJS_CalWin.initDate();
			var nowTime = nowDateObj.getTime();
			
			var end_dateStr = (nowDateObj.getFullYear()+1) + "/12/31";
			var end_dateArry = end_dateStr.match(/\d+/g);
			nowDateObj.setDate(1);
			nowDateObj.setFullYear(end_dateArry[0]);
			nowDateObj.setMonth(end_dateArry[1] - 1);
			nowDateObj.setDate(end_dateArry[2]);
			var endTime = nowDateObj.getTime();
			
			JLJS_CalNav.end = (endTime - nowTime) / (24*3600*1000);
			
			if(JLJS_CalNav.baseDate){
				nowDateObj.setDate(1);
				nowDateObj.setFullYear(JLJS_CalWin.baseYear);
				nowDateObj.setMonth(JLJS_CalWin.baseMonth);
				nowDateObj.setDate(JLJS_CalWin.baseDay);
				
				var tmp = (nowTime - nowDateObj.getTime()) / (24*3600*1000);
				JLJS_CalNav.end = JLJS_CalNav.end + tmp;
			}
		}
	},
	
	calWin : function() {
		
		JLJS_CalNav.todayDateObj = JLJS_CalWin.initDate();
		
		JLJS_CalNav.setup(arguments[0]);
		
		JLJS_CalNav.selectObj = [];
		for(var i=1; i<arguments.length; i++){
			JLJS_CalNav.selectObj[JLJS_CalNav.selectObj.length] = arguments[i];
		}
		
		if(JLJS_CalNav.successionDate && JLJS_CalNav.selectObj.length){
			var yearVal = "";
			if(JLJS_CalNav.selectObj[0].year){
				var yearType = JLJS_CalNav.selectObj[0].year.type;
				if(yearType == "select-one"){
					var index = JLJS_CalNav.selectObj[0].year.selectedIndex;
					yearVal = parseInt(JLJS_CalNav.selectObj[0].year.options[index].value);
				}else if(yearType == "text" || yearType == "hidden"){
					yearVal = parseInt(JLJS_CalNav.selectObj[0].year.value);
				}
			}
			
			if(yearVal){
				JLJS_CalWin.year = yearVal;
				JLJS_CalWin.month = JLJS_CalNav.selectObj[0].month.selectedIndex;
			}else{
				if(JLJS_CalNav.selectObj[0].month.selectedIndex < JLJS_CalNav.todayMonth){
					JLJS_CalWin.year = JLJS_CalNav.todayYear + 1;
				}else{
					JLJS_CalWin.year = JLJS_CalNav.todayYear;
				}
				JLJS_CalWin.month = JLJS_CalNav.selectObj[0].month.selectedIndex;
			}
		}else{
			JLJS_CalWin.setWorkDateObject(JLJS_CalNav.todayYear, JLJS_CalNav.todayMonth, JLJS_CalNav.todayDay);
			if(!JLJS_CalNav.startNoReserveMonthDisplay){
				var baseDateObj = JLJS_CalWin.getDateObject(JLJS_CalWin.baseYear, JLJS_CalWin.baseMonth, JLJS_CalWin.baseDay);
				var baseDateTime = baseDateObj.getTime();
				var todayDateTime = JLJS_CalNav.todayDateObj.getTime();
				var num = 0;
				if(baseDateTime > todayDateTime){
					num = (baseDateTime - todayDateTime) / (24*3600*1000);
					JLJS_CalWin.workDateObj.setDate(JLJS_CalWin.workDateObj.getDate() + parseInt(JLJS_CalNav.start) + num);
				}else{
					JLJS_CalWin.workDateObj.setDate(baseDateObj.getDate() + parseInt(JLJS_CalNav.start));
				}
			}
			JLJS_CalWin.year = JLJS_CalWin.workDateObj.getFullYear();
			JLJS_CalWin.month = JLJS_CalWin.workDateObj.getMonth();
		}
		
		if(JLJS_CalNav.writeFuncName){
			JLJS_CalNav.writeFuncName();
		}else{
			JLJS_CalWin.write();
		}
		
		var divObj = document.getElementById(JLJS_CalNav.displaySpace);
		if(divObj){
			divObj.style.display = "block";
		}
	},
	
	getDayOptions : function(year, month, dayOfWeekFlag) {
		
		var dateList = [];
		
		if(!JLJS_CalNav.selectObj[0].year){
			if(month >= JLJS_CalNav.todayMonth){
				year = JLJS_CalNav.todayYear;
			}
		}
		
		JLJS_CalWin.setWorkDateObject(year, month);
		var dayOfWeekNum = JLJS_CalWin.workDateObj.getDay();
		var monAmount = JLJS_CalWin.getMonthAmount(year, month);
		
		var dayOfWeekName;
		if(JLJS_CalNav.isEN ){
			dayOfWeekName = JLJS_CalWin.dayOfWeekNames[2];
		}else{
			dayOfWeekName = JLJS_CalWin.dayOfWeekNames[0];
		}
		
		if(dayOfWeekFlag) {
			for(var i=1; i<=monAmount; i++){
				if(JLJS_CalNav.isEN){
					dateList[dateList.length] = i + "(" + dayOfWeekName[dayOfWeekNum] + ")";
				}else{
					dateList[dateList.length] = i + "\u65E5(" + dayOfWeekName[dayOfWeekNum] + ")";
				}
				dayOfWeekNum++;
				if(dayOfWeekNum == 7){
					dayOfWeekNum = 0;
				}
			}
		}else{
			for(var i=1; i<=monAmount; i++){
				if(JLJS_CalNav.isEN){
					dateList[dateList.length] = i;
				}else{
					dateList[dateList.length] = i + "\u65E5";
				}
				dayOfWeekNum++;
			}
		}
		return dateList;
	},
	
	setDayOptions : function(dateList, selectObj, selectDay) {
		
		if(JLJS_CalNav.dayOfWeekFlag) {
			selectObj.length = dateList.length;		
			for (var i=0; i<dateList.length; i++) {
				var optionValue = i+1;
				if(JLJS_CalNav.dayZeroFlag && i<9){
					optionValue = '0' + optionValue;
				}
				if(this.popupFlag) {
					selectObj.options[i] = window.opener.JLJS_CalPopup.newOption(dateList[i], optionValue);
				} else {
					selectObj.options[i] = new Option(dateList[i], optionValue);
				}
				
				if(selectDay && selectObj.options[i].value == selectDay){
					selectObj.options[i].selected = true;
				}
			}
		} else {
			
			for(var i=0; i<selectObj.options.length; i++) {
				if(selectObj.options[i].value == selectDay) {
					selectObj.options[i].selected = true;
					break;	
				}
			}
			
		}
	},
	
	returnDateAndClose : function(year, month, day) {
		
		var dateList = [];
		
		dateList = JLJS_CalNav.getDayOptions(year, month, JLJS_CalNav.dayOfWeekFlag);
		JLJS_CalNav.setDayOptions(dateList, JLJS_CalNav.selectObj[0].day, day);
		
		if(JLJS_CalNav.successionDate && JLJS_CalNav.selectObj[0].year){
			var yearType = JLJS_CalNav.selectObj[0].year.type;
			if(yearType == "select-one"){
				for(var i=0; i<JLJS_CalNav.selectObj[0].year.length; i++){
					if(JLJS_CalNav.selectObj[0].year.options[i].value == year){
						JLJS_CalNav.selectObj[0].year.options[i].selected = true;
					}
				}
			}else if(yearType == "text" || yearType == "hidden"){
				JLJS_CalNav.selectObj[0].year.value = year;
			}
		}
		JLJS_CalNav.selectObj[0].month.selectedIndex = month;
		if(JLJS_CalNav.depLabel) {
			JLJS_CalNav.setDateLabel(JLJS_CalNav.depLabel, year, month+1, day);
		}
				
		if(JLJS_CalNav.selectObj.length == 2){
			JLJS_CalWin.setWorkDateObject(year, month, day + parseInt(JLJS_CalNav.dayRange, 10));
			
			var workDateTime = JLJS_CalWin.workDateObj.getTime();
			var endDayTime = JLJS_CalWin.reserveEndDay.getTime();
			
			if(workDateTime > endDayTime) {
				JLJS_CalWin.setWorkDateObject(JLJS_CalWin.reserveEndDay.getFullYear(), JLJS_CalWin.reserveEndDay.getMonth(), JLJS_CalWin.reserveEndDay.getDate());
			}

			if(JLJS_CalWin.workDateObj.getFullYear() != year || JLJS_CalWin.workDateObj.getMonth() != month){
				var selectDay = JLJS_CalWin.workDateObj.getDate();
				dateList = JLJS_CalNav.getDayOptions(JLJS_CalWin.workDateObj.getFullYear(), JLJS_CalWin.workDateObj.getMonth(), JLJS_CalNav.dayOfWeekFlag);
				JLJS_CalWin.workDateObj.setDate(selectDay);
			}
			JLJS_CalNav.setDayOptions(dateList, JLJS_CalNav.selectObj[1].day);
			
			if(JLJS_CalNav.successionDate && JLJS_CalNav.selectObj[1].year){
				var yearType = JLJS_CalNav.selectObj[1].year.type;
				if(yearType == "select-one"){
					for(var i=0; i<JLJS_CalNav.selectObj[1].year.length; i++){
						if(JLJS_CalNav.selectObj[1].year.options[i].value == JLJS_CalWin.workDateObj.getFullYear()){
							JLJS_CalNav.selectObj[1].year.options[i].selected = true;
						}
					}
				}else if(yearType == "text" || yearType == "hidden"){
					JLJS_CalNav.selectObj[1].year.value = JLJS_CalWin.workDateObj.getFullYear();
				}
			}
			JLJS_CalNav.selectObj[1].month.selectedIndex = JLJS_CalWin.workDateObj.getMonth();
			JLJS_CalNav.selectObj[1].day.selectedIndex = JLJS_CalWin.workDateObj.getDate() - 1;		
			if(JLJS_CalNav.arrLabel) {
				JLJS_CalNav.setDateLabel(JLJS_CalNav.arrLabel, JLJS_CalWin.workDateObj.getFullYear(), JLJS_CalWin.workDateObj.getMonth()+1, JLJS_CalWin.workDateObj.getDate());
			}		
		}
		if(JLJS_CalNav.popupFlag) {
			window.close();	
		} else {
			var divObj = document.getElementById(JLJS_CalNav.displaySpace);
			if(divObj){
				divObj.style.display = "none";
			}
		}
		
		JLJS_CalNav.returnDateAndCloseAfter();
	},
	
	returnDateAndCloseAfter : function() {
	},
	
	setDateLabel : function(obj, year, month, day){
		var param = {"year":year, "month":month, "day":day, dayOfWeekFlag:JLJS_CalNav.dayOfWeekFlag, yearLabelFlag:JLJS_CalNav.yearLabelFlag, enFlag:JLJS_CalNav.isEN};
		obj.innerHTML = this.createDateText(param);
	},
	
	createDateText : function(param) {
		var year=0;
		var month = param.month-1;
		var day = param.day;
		var dayOfWeekFlag = param.dayOfWeekFlag;
		var enFlag = param.enFlag;
		var yearLabelFlag = param.yearLabelFlag;
		var dayOfWeek = "";
		if(param.year){
			year = param.year;
		}else{
			var today = new Date();
			var tYear = today.getFullYear();
			var tMonth = today.getMonth();
			var tDay = today.getDate();
			
			if(month < tMonth){
				year = tYear+1;	
			}else{
				year = tYear;	
			}
		}
		
		if(dayOfWeekFlag){
			var createDate = new Date(year, month, day);
			if(enFlag) {
				dayOfWeek = JLJS_CalWin.dayOfWeekNames[2][createDate.getDay()] + ", ";
			} else {
				dayOfWeek = "(" + JLJS_CalWin.dayOfWeekNames[0][createDate.getDay()] + ")";
			}
		}
		if(enFlag) {
			return dayOfWeek + JLJS_CalWin.monNames[2][month] + " " + day + (yearLabelFlag ? ", " + year : "");
		} else {
			return (yearLabelFlag ? year + "\u5e74" : "") + (month+1) + "\u6708" + day + "\u65e5" + dayOfWeek;
		}
	},
	
	init : function() {
		if(JLJS_CalNav.used) {
			
			var displaySpace = document.createElement("div");
			JLJS.addEvent(displaySpace, "click", function(e){
				JLJS_CalNav.floatCloseFlg = false;
			});
			JLJS.setAttr(displaySpace, "id", JLJS_CalNav.displaySpace);
			var displaySpaceWrite = document.createElement("div");
			JLJS.setAttr(displaySpaceWrite, "id", JLJS_CalNav.displaySpaceWrite);
			JLJS.classAttr.add(displaySpaceWrite, "calendar01");
			displaySpace.appendChild(displaySpaceWrite);
			if(document.all && JLJS_CalNav.browser.msie && JLJS_CalNav.browser.version < 7) {
				var iframeSpace = document.createElement("iframe");
				JLJS.setAttr(iframeSpace, "id", JLJS_CalNav.iframeSpace);
				JLJS.setAttr(iframeSpace, "src", "javascript:false;");
				displaySpace.appendChild(iframeSpace);
			}
			document.body.appendChild(displaySpace);
		}
	},

	getElementStyle : function(targetElm,IEStyleProp,CSSStyleProp) {
		var elem = targetElm;
		if (elem.currentStyle) {
			return elem.currentStyle[IEStyleProp];
		} else if (window.getComputedStyle) {
			var compStyle = window.getComputedStyle(elem,"");
			return compStyle.getPropertyValue(CSSStyleProp);
		}
	},
	
	getPosition : function(that) {
		var targetEle = that;
		var pos = new function(){ this.left = 0; this.top = 0; }
		while( targetEle ){
			pos.left += targetEle.offsetLeft; 
			pos.top += targetEle.offsetTop; 
			targetEle = targetEle.offsetParent;
			if ((targetEle) && (JLJS.env.isIE)) {
				pos.left += (parseInt(JLJS_CalNav.getElementStyle(targetEle,"borderLeftWidth","border-left-width")) || 0);
				pos.top += (parseInt(JLJS_CalNav.getElementStyle(targetEle,"borderTopWidth","border-top-width")) || 0);
			}
		}
		if (JLJS.env.isGecko) {
			var bd = document.getElementsByTagName("BODY")[0];
			pos.left += 2*(parseInt(JLJS_CalNav.getElementStyle(bd,"borderLeftWidth","border-left-width")) || 0);
			pos.top += 2*(parseInt(JLJS_CalNav.getElementStyle(bd,"borderTopWidth","border-top-width")) || 0);
		}
		return pos;
	},

	calDisplay : function(param) {
		var iconElm = param.target;
		var iconImg = iconElm.firstChild;
		var calElm = document.getElementById(JLJS_CalNav.displaySpace);
		this.floatCloseFlg = false;
		
		if(JLJS_CalNav.calIconElm) {
			if(JLJS.classAttr.check(iconElm, JLJS_CalNav.DISP_CLASS) && 
									JLJS_CalNav.getElementStyle(calElm, "display", "display") == "block") {
				calElm.style.display = "none";
				JLJS.classAttr.remove(iconElm, JLJS_CalNav.DISP_CLASS);
				return;
			}
			JLJS.classAttr.remove(JLJS_CalNav.calIconElm, JLJS_CalNav.DISP_CLASS);
		}
		JLJS_CalNav.calIconElm = iconElm;
		JLJS.classAttr.add(JLJS_CalNav.calIconElm, JLJS_CalNav.DISP_CLASS);
		
		var browserWidth = document.documentElement.clientWidth ||	document.body.clientWidth;
		var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
	
		var iconOffset = JLJS_CalNav.getPosition(iconImg);
		var top = iconOffset.top + JLJS_CalNav.ICON_H_SIZE;
		var left = iconOffset.left;
		var wrapper = document.getElementById("wrapper");
		var leftPlusWIN_W_SIZE = JLJS_CalNav.WIN_W_SIZE;
		var minWX = left;
		
		if(wrapper) {
			var contents = JLJS.getElementsByClassName("contents", "div", wrapper);
			if(contents.length) {
				leftPlusWIN_W_SIZE = JLJS_CalNav.WIN_W_SIZE + contents[0].offsetLeft;
				minWX = contents[0].offsetLeft;
			}else {
				leftPlusWIN_W_SIZE = JLJS_CalNav.WIN_W_SIZE + wrapper.offsetLeft + 10;
				minWX = wrapper.offsetLeft;
			}
		}
		if(left + JLJS_CalNav.CAL_W_SIZE - scrollX > Math.min(browserWidth, leftPlusWIN_W_SIZE)) {
			left = Math.max(scrollX, left - JLJS_CalNav.CAL_W_SIZE + JLJS_CalNav.ICON_W_SIZE, minWX);
		}
		calElm.style.position = "absolute";
		calElm.style.top = top + "px";
		calElm.style.left = left + "px";
		
		var board = {};
		var arrival = {};
		
		if(typeof param.frm.board.month == "string") {
		
			var frms = [];
			var frmElm = document[param.frm.name];

			if(frmElm.tagName) {
				frms[0] = frmElm;
			} else {
				frms = frmElm;
			}
			var index = param.frm.index;
			if(!index) {
				index = 0;	
			}
			if(index > frms.length) {
				return;
			}
			var frm = frms[index];
			
			board = {month: frm[param.frm.board.month], day: frm[param.frm.board.day]};
			if(param.frm.board.year) {
				board.year = frm[param.frm.board.year];
			}
			
			if(param.frm.arrival) {
				arrival = {month: frm[param.frm.arrival.month], day: frm[param.frm.arrival.day]};
				if(param.frm.arrival.year) {
					arrival.year = frm[param.frm.arrival.year];
				}
			}

		} else {
			board = param.frm.board;
			if(param.frm.arrival) {
				arrival = param.frm.arrival;
			}
		}
		
		if(param.frm.arrival) {
			JLJS_CalNav.calWin(param.option, board, arrival);
		} else {
			JLJS_CalNav.calWin(param.option, board);
		}
		
		return false;
	
	},
	
	isUsed : function() {
		var result = true;

		if(JLJS.env.isIE) {
			this.browser.msie = true; 
			var version = navigator.userAgent.toLowerCase().match(/msie [0-9.]+/);
			if(version) {
				this.browser.version = version[0].replace(/msie /g, "");
			}
		} else if(JLJS.env.isNN) {
			this.browser.netscape = true;
			var version = navigator.userAgent.toLowerCase().match(/netscape\/[0-9.]+/);
			if(version) {
				this.browser.version = version[0].replace(/netscape\//g, "")
			}
		} else if(JLJS.env.isGecko) {
			this.browser.firefox = true;
			var version = navigator.userAgent.toLowerCase().match(/firefox\/[0-9.]+/);
			if(version) {
				this.browser.version = version[0].replace(/firefox\//g, "");
			}
		} else if(JLJS.env.isSafari) {
			this.browser.safari = true;
			var version = navigator.userAgent.toLowerCase().match(/safari\/[0-9.]+/);
			if(version) {
				this.browser.version = version[0].replace(/safari\//g, "");
			}
		}		
		
		return result;
	}
}

function JLJS_CalendarWindow(){
	this.monNames = [
		[ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
		[ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
		[ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
	];
	this.dayOfWeekNames = [
		[ "\u65E5", "\u6708", "\u706B", "\u6C34", "\u6728", "\u91D1", "\u571F" ],
		[ "S", "M", "T", "W", "T", "F", "S" ],
		[ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ]
	];
	
	this.imgPath;

	this.baseYear;
	this.baseMonth;
	this.baseDay;

	this.year;
	this.month;
	this.day;
	
	this.yearMonth_S;
	this.yearMonth_E;
	this.startYearMonth;
	this.endYearMonth;
	
	this.previousPaneYear;
	this.previousPaneMonth;
	this.nextPaneYear;
	this.nextPaneMonth;

	this.reserveStartDay;
	this.reserveEndDay;
	
	this.firstPreviousHolioday = 1;

	this.holidayArry = [];
	this.previousHolidayArry = [];
	this.nextHolidayArry = [];

	this.previousCarryOverArry = [];
	this.carryOverTransferFlag= 0;
	this.carryOverTransferDay;
	this.carryOverFlag_people = 0;
	
	this.workDateObj = new Date();
	
	this.setFestivalDay();
}

JLJS_CalendarWindow.prototype = { 
	setFestivalDay : function(){
		
		this.FESTIVALDAY_LIST = {
			"0" : [
					{"day":"1", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1948", "endYear":""},
					{"day":"", "happyWeek":"2", "happyDayOfWeek":"1", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"2000", "endYear":""}
				],
			"1" : [
   					{"day":"11", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1967", "endYear":""}
				],
			"2" : [
					{"day":"", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"1", "autumnalEquinox":"", "startYear":"1948", "endYear":""}	
				],
			"3" : [
					{"day":"29", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"2007", "endYear":""}
				],
			"4" : [
					{"day":"3", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1948", "endYear":""},
					{"day":"4", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"2007", "endYear":""},
					{"day":"5", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1948", "endYear":""}
				],
			"5" : [],
			"6" : [
					{"day":"", "happyWeek":"3", "happyDayOfWeek":"1", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"2003", "endYear":""}
				],
			"7" : [],
			"8" : [
					{"day":"", "happyWeek":"3", "happyDayOfWeek":"1", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"2003", "endYear":""},
					{"day":"", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"1", "startYear":"1948", "endYear":""}
				],
			"9" : [
					{"day":"", "happyWeek":"2", "happyDayOfWeek":"1", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"2000", "endYear":""}
				],
			"10" : [
					{"day":"3", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1948", "endYear":""},
					{"day":"23", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1948", "endYear":""}
				],
			"11" : [
					{"day":"23", "happyWeek":"", "happyDayOfWeek":"", "vernalEquinox":"", "autumnalEquinox":"", "startYear":"1989", "endYear":""}
				]
		};
	},
	
	initDate : function() {
		var date = new Date();
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		date.setMilliseconds(0);
		return date;
	},
	
	getDateObject : function(year, month, day) {
		var dateObj = JLJS_CalWin.initDate();
		dateObj.setDate(1);

		dateObj.setFullYear(year);
		dateObj.setMonth(month);
		if(day){
			dateObj.setDate(day);
		}
		return dateObj;
	},
	
	setWorkDateObject : function(year, month, day) {
		JLJS_CalWin.workDateObj.setDate(1);
		JLJS_CalWin.workDateObj.setFullYear(year);
		JLJS_CalWin.workDateObj.setMonth(month);
		if(day){
			JLJS_CalWin.workDateObj.setDate(day);
		}
	},
	
	setVariables : function(year, month) {
		var paneNum = JLJS_CalNav.nextMonthNum;
		
		JLJS_CalWin.setWorkDateObject(year, month - parseInt(paneNum, 10));
		JLJS_CalWin.previousPaneYear = JLJS_CalWin.workDateObj.getFullYear();
		JLJS_CalWin.previousPaneMonth = JLJS_CalWin.workDateObj.getMonth();

		JLJS_CalWin.setWorkDateObject(year, month + parseInt(paneNum, 10));
		JLJS_CalWin.nextPaneYear = JLJS_CalWin.workDateObj.getFullYear();
		JLJS_CalWin.nextPaneMonth = JLJS_CalWin.workDateObj.getMonth();
		
		JLJS_CalWin.yearMonth_S = year * 100 + month;
		JLJS_CalWin.yearMonth_E = JLJS_CalWin.nextPaneYear * 100 + JLJS_CalWin.nextPaneMonth;
		
		var pn = [];
		JLJS_CalWin.firstPreviousHolioday = 1;

		for(var i=0; i<JLJS_CalNav.paneNum; i++) {
			pn[i] = {year:"", month:"", previousYear:"", previousMonth:"", yearMonth:"", monAmount:"", startIdx:"", previousMonDays:"", festivals:"", holidays:""};
			
			JLJS_CalWin.setWorkDateObject(year, month + i);
			
			pn[i].year = JLJS_CalWin.workDateObj.getFullYear();
			pn[i].month = JLJS_CalWin.workDateObj.getMonth();
			
			pn[i].yearMonth = pn[i].year * 100 + pn[i].month;
			
			pn[i].startIdx = JLJS_CalWin.workDateObj.getDay();
			pn[i].monAmount = JLJS_CalWin.getMonthAmount(pn[i].year, pn[i].month);
			
			JLJS_CalWin.workDateObj.setMonth(JLJS_CalWin.workDateObj.getMonth() - 1);
			pn[i].previousYear = JLJS_CalWin.workDateObj.getFullYear();
			pn[i].previousMonth = JLJS_CalWin.workDateObj.getMonth();	
			
			if(JLJS_CalNav.festivalDay){
				if(JLJS_CalWin.previousHolidayArry.length == 0 && JLJS_CalWin.firstPreviousHolioday == 1){
					var previousFestivalArry = JLJS_CalWin.getFestivalDayList(pn[i].previousYear, pn[i].previousMonth);
					if(previousFestivalArry.length > 0){
						var previousDayOffArry = JLJS_CalWin.getDayOffList(
														pn[i].previousYear, pn[i].previousMonth, previousFestivalArry);
						if(previousDayOffArry.length > 0){
							var previousHolidayArry = previousFestivalArry.concat(previousDayOffArry);
							previousHolidayArry.sort(function(a, b) {return (parseInt(a) > parseInt(b)) ? 1 : -1;});
						}else{
							previousHolidayArry = previousFestivalArry;
						}
					}
				}else{
					var previousHolidayArry = JLJS_CalWin.previousHolidayArry;
				}
				
				JLJS_CalWin.firstPreviousHolioday = 0;
				var holidayArry = [];
				var festivalArry = JLJS_CalWin.getFestivalDayList(pn[i].year, pn[i].month);
				if(festivalArry.length > 0){
					var dayOffArry = JLJS_CalWin.getDayOffList(pn[i].year, pn[i].month, festivalArry);
					if(dayOffArry.length > 0){
						var holidayArry = festivalArry.concat(dayOffArry);
						holidayArry.sort(function(a, b) {return (parseInt(a) > parseInt(b)) ? 1 : -1;});
					}else{
						holidayArry = festivalArry;
					}
				}else if(JLJS_CalWin.previousCarryOverArry.length > 0){
					var holidayArry = JLJS_CalWin.getTransferDayOffList(
														pn[i].year, pn[i].month, JLJS_CalWin.previousCarryOverArry);
					JLJS_CalWin.previousCarryOverArry = [];
				}
				JLJS_CalWin.previousHolidayArry = holidayArry;
				pn[i].holidays = holidayArry;
			}
		}
		
		return pn;
	},
	
	getMonthAmount : function(year, month) {
		var dateObj = JLJS_CalWin.getDateObject(year, month + 1);
		dateObj.setDate(0);
		var monthAmount = dateObj.getDate();
		return monthAmount;
	},
	
	getFestivalDayList : function(year, month) {
		var festivalDayList = [];
		var festivalArry = JLJS_CalWin.FESTIVALDAY_LIST[month];
		
		JLJS_CalWin.setWorkDateObject(year, month);
		
		for(var i=0; i<festivalArry.length; i++){
			var festivalDay = JLJS_CalWin.calculationOfDay(festivalArry[i], year, JLJS_CalWin.workDateObj.getDay());
			if(festivalDay){
				festivalDayList[festivalDayList.length] = parseInt(festivalDay, 10);
			}
		}
		
		if(festivalDayList.length > 1){
			festivalDayList.sort(function(a, b) {return (parseInt(a) > parseInt(b)) ? 1 : -1;});
		}
		return festivalDayList;
	},
	
	getDayOffList : function(year, month, festivalArry) {
		
		var dayOffList = [];
		var transferDayOffArry = [];
		var peopleDayOffArry = [];
		var carryOverArry = [];
		
		transferDayOffArry = JLJS_CalWin.getTransferDayOffList(year, month, festivalArry);
		peopleDayOffArry = JLJS_CalWin.getPeopleDayOffList(year, month, festivalArry);
		
		if(JLJS_CalWin.carryOverTransferFlag == 1){
			carryOverArry[carryOverArry.length] = JLJS_CalWin.carryOverTransferDay;
		}
		if(JLJS_CalWin.carryOverPeopleFlag == 1){
			if(carryOverArry[0] != 1){
				carryOverArry[carryOverArry.length] = 1;
			}
		}

		if(transferDayOffArry.length > 0 && peopleDayOffArry.length > 0){
			dayOffList = JLJS_CalWin.arryOrder(transferDayOffArry, peopleDayOffArry);
		}else if(transferDayOffArry.length > 0){
			dayOffList = transferDayOffArry;
		}else if(peopleDayOffArry.length > 0){
			dayOffList = peopleDayOffArry;
		}
		
		if(dayOffList > 0 && JLJS_CalWin.previousCarryOverArry.length > 0){
			dayOffList = JLJS_CalWin.arryOrder(dayOffList, JLJS_CalWin.previousCarryOverArry);
		}else if(JLJS_CalWin.previousCarryOverArry.length > 0){
			dayOffList[dayOffList.length] = JLJS_CalWin.previousCarryOverArry;
		}
		
		JLJS_CalWin.previousCarryOverArry = carryOverArry;
		
		if(dayOffList.length > 1){
			dayOffList.sort(function(a, b) {return (parseInt(a) > parseInt(b)) ? 1 : -1;});
		}
		return dayOffList;
	},

	arryOrder : function(arry1, arry2) {
		var tmpArry = [];
		var flag;
		for(var i=0; i<arry2.length; i++){
			flag = 0;
			for(var j=0; j<arry1.length; j++){
				if(arry2[i] == arry1[j]){
					flag = 1;
					break;
				}
			}
			if(flag == 0){
				tmpArry[tmpArry.length] = arry2[i];
			}
		}
		
		if(tmpArry.length > 0){
			tmpArry = arry1.concat(tmpArry);
		}else{
			tmpArry = arry1;
		}
		return tmpArry;
	},
	
	calculationOfDay : function(fesArry, year, startInd) {

		var day;
		var startYear = fesArry.startYear;
		var endYear = fesArry.endYear;
		
		if(year >= startYear) {
			if(!endYear || year <= endYear){
				if(fesArry.day) {
					day = fesArry.day;
				}else if(fesArry.happyWeek) {
					var happyDayOfWeekVal = 6 - parseInt(fesArry.happyDayOfWeek);
					day = parseInt(fesArry.happyWeek, 10) * 7 - (startInd + happyDayOfWeekVal) % 7;
				}else if(fesArry.vernalEquinox) {
					day = parseInt(0.24242 * year - parseInt(year / 4) + 35.84, 10);
				}else if(fesArry.autumnalEquinox) {
					day = parseInt(0.24204 * year - parseInt(year / 4) + 39.01, 10);
				}
				return day;
			}
		}
	},
	
	getTransferDayOffList : function(year, month, fesArry) {
		
		var transferDayOffList = [];
		for(var i=0; i<fesArry.length; i++){
			var cnt = 0;
			JLJS_CalWin.setWorkDateObject(year, month, fesArry[i]);
			var dayOfWeek = JLJS_CalWin.workDateObj.getDay();
			if(dayOfWeek == 0) {
				cnt++;
				for(var j=i+1; j<fesArry.length; j++) {
					if(fesArry[i] + cnt == fesArry[j] || cnt % 7 == 0) {
						cnt++;
					}else{
						break;
					}
				}
				if(transferDayOffList.length == 0 || transferDayOffList[transferDayOffList.length - 1] != fesArry[i] + cnt){
					transferDayOffList[transferDayOffList.length] = fesArry[i] + cnt;
				}
			}
		}
		
		JLJS_CalWin.carryOverTransferFlag = 0;
		JLJS_CalWin.carryOverTransferDay = "";
		
		if(transferDayOffList.length > 0){
			var monAmount = JLJS_CalWin.getMonthAmount(year, month);
			if(transferDayOffList[transferDayOffList.length - 1] > monAmount){
				JLJS_CalWin.carryOverTransferDay = transferDayOffList[transferDayOffList.length - 1] - monAmount;
				JLJS_CalWin.carryOverTransferFlag = 1;
				
				var tmpArry = new Array();
				for(var i=0; i<transferDayOffList.length-1; i++){
					tmpArry[i] = transferDayOffList[i];
				}
				transferDayOffList = tmpArry;
			}
			
			if(JLJS_CalWin.carryOverTransferFlag == 1){
				JLJS_CalWin.setWorkDateObject(year, month + 1);
				var nextFestivalDayList = JLJS_CalWin.getFestivalDayList(
													JLJS_CalWin.workDateObj.getFullYear(), JLJS_CalWin.workDateObj.getMonth());
				if(nextFestivalDayList.length > 0){
					var cnt = 0;
					for(var i=0; i<nextFestivalDayList.length; i++){
						if(JLJS_CalWin.carryOverTransferDay + i == nextFestivalDayList[i]){
							cnt++;
						}else{
							break;
						}
					}
					JLJS_CalWin.carryOverTransferDay = JLJS_CalWin.carryOverTransferDay + cnt;
				}
			}
		}
		
		if(transferDayOffList.length > 1){
			transferDayOffList.sort(function(a, b) {return (parseInt(a) > parseInt(b)) ? 1 : -1;	});	
		}
		return transferDayOffList;
	},
	
	getPeopleDayOffList : function(year, month, fesArry) {
		
		var peopleDayOffList = [];
		JLJS_CalWin.carryOverPeopleFlag = 0;
		
		JLJS_CalWin.setWorkDateObject(year, month + 1);
		
		var nextFestivalDayList = JLJS_CalWin.getFestivalDayList(
										JLJS_CalWin.workDateObj.getFullYear(), JLJS_CalWin.workDateObj.getMonth());
		
		JLJS_CalWin.setWorkDateObject(year, month);
		
		var monAmount = JLJS_CalWin.getMonthAmount(year, month);
		
		for(var i=0; i<fesArry.length; i++){
			JLJS_CalWin.workDateObj.setDate(fesArry[i]);
			var dayOfWeek = JLJS_CalWin.workDateObj.getDay();
			if(dayOfWeek != 6){
				if(fesArry[i] > monAmount - 2 && nextFestivalDayList.length > 0){
					if((i + 1) == fesArry.length){
						if((fesArry[i] + 2) - monAmount == nextFestivalDayList[0]){
							if(nextFestivalDayList[0] == 1){
								peopleDayOffList[peopleDayOffList.length] = fesArry[i] + 1;
							}else{
								JLJS_CalWin.carryOverPeopleFlag = 1;
							}
						}
					}
				}else{
					if((i + 1) < fesArry.length && fesArry[i] + 1 != fesArry[i + 1]){
						if(fesArry[i] + 2 == fesArry[i + 1]){
							peopleDayOffList[peopleDayOffList.length] = fesArry[i] + 1;
						}
					}
					
				}
			}
		}
		
		if(peopleDayOffList.length > 1){
			peopleDayOffList.sort(function(a, b) {return (parseInt(a) > parseInt(b)) ? 1 : -1;	});	
		}
		
		return peopleDayOffList;
	},
	
	createMonthCalendarHtml : function(pn, i) {
		
		var firstDate = JLJS_CalWin.getDateObject(pn[i].year, pn[i].month);		
		var currentMonth = firstDate.getMonth();
		var oneDayOfWeek = firstDate.getDay();
		firstDate.setDate(1 - 1*oneDayOfWeek);
		
		var lastDate = JLJS_CalWin.getDateObject(pn[i].year, pn[i].month);
		lastDate.setMonth(pn[i].month + 1);
		lastDate.setDate(0);
		if(lastDate.getDay() != 6){
			lastDate.setDate(lastDate.getDate()+6 - lastDate.getDay());
		}
		
		var html = '';
		var column = 0;
		var lineCnt = 1;
		var k = 0;
		while(firstDate.getTime() <= lastDate.getTime()) {
			if(column == 7) {
				html += '</tr><tr>';
				lineCnt++;
				column = 0;
			}
			
			if(column == 0){
				var classVal = "days sun";
			}else if(column == 6){
				var classVal = "days sat";
			}else{
				var classVal = "days"
			}

			if(currentMonth != firstDate.getMonth()){
				//html += '<td class="days unselectable">' + firstDate.getDate() + '</td>';
				html += '<td class="days unselectable">&nbsp;</td>';
			}else{
				if(pn[i].holidays[k] == firstDate.getDate()){
					classVal = "holiday";
					k++;
				}
				if(pn[i].yearMonth == JLJS_CalNav.todayYearMonth && firstDate.getDate() == JLJS_CalNav.todayDay){
					classVal += " today";
				}
				
				if(JLJS_CalWin.reserveStartDay && firstDate.getTime() < JLJS_CalWin.reserveStartDay.getTime() || 
											JLJS_CalWin.reserveEndDay && firstDate.getTime() > JLJS_CalWin.reserveEndDay.getTime()){
					classVal += " unselectable";
					html += '<td class="' + classVal + '">' + firstDate.getDate() + '</td>';
				}else{
					html += '<td class="' + classVal + '"><a href="javascript:JLJS_CalNav.returnDateAndClose('
											+ pn[i].year+ ',' + pn[i].month + ',' + firstDate.getDate() + ')">'
											+ firstDate.getDate() + '</a></td>';
				}
			}
			
			firstDate.setDate(firstDate.getDate() + 1);
			column++;
		}
		if(lineCnt <= 5){
			html += '</tr><tr>';
			var cnt = 1 + 5-lineCnt;
			for(var j=0; j<7*cnt; j++){
				html += '<td class="days unselectable">&nbsp;</td>';
				if(j==6 && 7<7*cnt){
					html += '</tr><tr>';
				}
			}
		}
		
		return html;
	},
	
	write_E : function() {
		JLJS_CalNav.isEN = true;
		JLJS_CalWin.write();
	},
	
	write : function() {
		var pn = [];
		pn = JLJS_CalWin.setVariables(parseInt(JLJS_CalWin.year, 10), parseInt(JLJS_CalWin.month, 10));
		
		JLJS_CalWin.reserveStartDay = JLJS_CalWin.getDateObject(
														JLJS_CalWin.baseYear, JLJS_CalWin.baseMonth, JLJS_CalWin.baseDay);
		JLJS_CalWin.reserveStartDay.setDate(JLJS_CalWin.reserveStartDay.getDate() + parseInt(JLJS_CalNav.start,10));
		JLJS_CalWin.startYearMonth = JLJS_CalWin.reserveStartDay.getFullYear() * 100 + JLJS_CalWin.reserveStartDay.getMonth();
		
		JLJS_CalWin.reserveEndDay = JLJS_CalWin.getDateObject(
														JLJS_CalWin.baseYear, JLJS_CalWin.baseMonth, JLJS_CalWin.baseDay);
		JLJS_CalWin.reserveEndDay.setDate(JLJS_CalWin.reserveEndDay.getDate() + parseInt(JLJS_CalNav.end, 10));
		var displayEndDate = new Date(JLJS_CalWin.reserveEndDay.getFullYear(), JLJS_CalWin.reserveEndDay.getMonth()-(JLJS_CalNav.paneNum - JLJS_CalNav.nextMonthNum), JLJS_CalWin.reserveEndDay.getDate());
		JLJS_CalWin.endYearMonth = displayEndDate.getFullYear() * 100 + displayEndDate.getMonth();
		
		var html = '';
		
		html += '<div class="calendar01H clearfix">';
		
		if(JLJS_CalNav.isEN){
			var text = 'Select date from Calendar';
		}else{
			var text = '\u30AB\u30EC\u30F3\u30C0\u30FC\u304B\u3089\u9078\u629E';
		}
		html += '<h2>' + text + '</h2>';
		
		html += '<div class="close"><a href="javascript:void(0)" onclick="';
		if(JLJS_CalNav.popupFlag) {
			html += 'window.close();';
		} else {
			html += 'document.getElementById(JLJS_CalNav.displaySpace).style.display=\'none\';';
		}
		html += ' return false;" class="roImg">';
		if(JLJS_CalNav.isEN){
			var text = '<img src="../img/parts_module_close_n.gif" width="22" height="22" alt="close">';
		}else{
			var text = '<img src="../img/parts_module_close_n.gif" width="22" height="22" alt="\u9589\u3058\u308B">';
		}
		html += text + '</a></div>';
		
		html += '</div>'
		
		html += '<div class="calendar01C clearfix">';

		for(var i=0; i<JLJS_CalNav.paneNum; i++) {
			html += '<div class="month01">';
			
			var text = "";
			if(pn[i].month == JLJS_CalNav.todayMonth && pn[i].year == JLJS_CalNav.todayYear) {
				if(JLJS_CalNav.isEN){
					text = '<span>this month</span>'
				}else{
					text = '<span>\u4ECA\u6708</span>';
				}
			}
			
			if(JLJS_CalNav.isEN){
				html += '<h3 class="month01H">' + '<strong>' + JLJS_CalWin.monNames[2][pn[i].month] + '&nbsp;</strong>'
												+ pn[i].year + '&nbsp;' + text + '</h3>';
			}else{
				html += '<h3 class="month01H">' + pn[i].year + '\u5E74 <strong>' + JLJS_CalWin.monNames[0][pn[i].month] 
												+ '</strong>\u6708&nbsp;' + text + '</h3>';
			}
			
			html += '<table cellspacing="0" summary="layout">';
			html += '<thead>';
			html += '<tr>';

			if(JLJS_CalNav.isEN){
				var dayOfWeekNames = JLJS_CalWin.dayOfWeekNames[1];
			}else{
				var dayOfWeekNames = JLJS_CalWin.dayOfWeekNames[0];
			}
			for(var j=0; j<dayOfWeekNames.length; j++) {
				var attr = 'week';
				if(j == 0){
					attr +=	' sun';
				}
				html += '<td class="' + attr + '">' + dayOfWeekNames[j] +'</td>';
			}

			html += '</tr>';
			html += '</thead>';
			
			html += '<tbody>';
			html += '<tr>';
			html += JLJS_CalWin.createMonthCalendarHtml(pn, i);
			html += '</tr>';
			html += '</tbody>'
			
			html += '</table>';
			html += '</div>';
		}
		html += '</div>';
			
		html += '<div class="calendarLi clearfix">';
		
		var paneNum = JLJS_CalNav.nextMonthNum
		
		if(JLJS_CalNav.isEN){
			var text = 'Previous ' + paneNum + ' Months';
		}else{
			var text = '\u524D\u306E' + paneNum + '\u30ab\u6708';
		}
		
		if(JLJS_CalWin.startYearMonth < JLJS_CalWin.yearMonth_S){
			html += '<p class="prevMonth"><a href="javascript:void(0)" onclick="JLJS_CalWin.goPrevious(); return false;">' + text + '</a></p>';
		}
		
		if(JLJS_CalNav.isEN){
			var text = 'Next ' + paneNum + ' Months';
		}else{
			var text = '\u6B21\u306E' + paneNum + '\u30ab\u6708';
		}
		
		if(JLJS_CalWin.yearMonth_E <= JLJS_CalWin.endYearMonth){
			html += '<p class="nextMonth">' + '<a href="javascript:void(0)" onclick="JLJS_CalWin.goNext(); return false;">' + text + '</a></p>';
		}
		
		html += '</div>';
		
		var divObj = document.getElementById(JLJS_CalNav.displaySpaceWrite);
		if(divObj){
			divObj.innerHTML = html;	
		}
	},

	goPrevious : function() {
		JLJS_CalWin.year = JLJS_CalWin.previousPaneYear;
		JLJS_CalWin.month = JLJS_CalWin.previousPaneMonth;
		JLJS_CalWin.write();
	},

	goNext : function() {
		JLJS_CalWin.year = JLJS_CalWin.nextPaneYear;
		JLJS_CalWin.month = JLJS_CalWin.nextPaneMonth;
		JLJS_CalWin.write();
	},

	changeYear : function(obj) {
		JLJS_CalWin.year = obj.options[obj.selectedIndex].value;
		JLJS_CalWin.write();
	},

	changeMonth : function(obj) {
		JLJS_CalWin.month = obj.options[obj.selectedIndex].value;
		JLJS_CalWin.write();
	}
};

function JLJS_CalendarPopup() {
	this.param = {};
}

JLJS_CalendarPopup.prototype = {
	calPopup : function(param) {
		this.param = param;
		this.param.option.popupFlag = true;
		var url = this.param.option && this.param.option.isEN ? "/en/common_rn/html/calendar_en.html" : "/common_rn/html/calendar.html";
		JLJS.openWindow(url, "popup", 662, 336);
	},
	newOption : function(text, value) {
		return new Option(text, value);	
	}
};

var JLJS_CalNav = new JLJS_CalendarNavigation;
var JLJS_CalWin = new JLJS_CalendarWindow;
var JLJS_CalPopup = new JLJS_CalendarPopup;
JLJS.addOnload(JLJS_CalNav.init);
JLJS.addEvent(document, "click", function(e) {
		if(!JLJS_CalNav.popupFlag) {
			if(JLJS_CalNav.floatCloseFlg) {
				var divObj = document.getElementById(JLJS_CalNav.displaySpace);
				if(divObj){
					divObj.style.display = "none";
				}
			}
			JLJS_CalNav.floatCloseFlg = true;
		}
});