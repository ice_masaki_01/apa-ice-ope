/*__________________________________________________________

   form control systems (rev001_JAL_BRANCH_2)

_____________________________________________________________*/

var MSG_ENTER_MEMNO     = '\u304A\u5F97\u610F\u69D8\u756A\u53F7\u3092\u5165\u529B\u3057\u3066\u304F\u3060\u3055\u3044\u3002';
var MSG_ENTER_PASSWD    = '\u30D1\u30B9\u30EF\u30FC\u30C9\u3092\u5165\u529B\u3057\u3066\u304F\u3060\u3055\u3044\u3002';
var MSG_CONFIRM_AIRPORT = '\u767A\u7740\u7A7A\u6E2F\u540D\u3092\u3054\u78BA\u8A8D\u304F\u3060\u3055\u3044\u3002';

/* ------ JLJS_InteractiveTextField ------ */

function JLJS_InteractiveTextField (node) {
	if (!node || !node.nodeName || !node.nodeName.match(/^(input|textarea)$/i)) return;

	this.node         = node;
	this.searchAttr   = 'title';
	this.status       = '';
	this.prepopulated = false;
	this.focused      = false;
	this.classNames = {
		'default'  : 'pseudo-default',
		'focus'    : 'pseudo-focus',
		'disabled' : 'pseudo-disabled'
	};

	if (!JLJS.getAttr(this.node, this.searchAttr)) return null;

	this.node._ITF_instance_ = this;
	this.prepopulateInfoText();
	this.setStatus((this.prepopulated) ? 'disabled' : 'default');
}

JLJS_InteractiveTextField.prototype = {
	prepopulateInfoText : function () {
		var value0 = this.node.value;
		var value1 = JLJS.getAttr(this.node, this.searchAttr);
		var value2 = JLJS.getAttr(this.node, JLJS.prfx.bAattrs + this.searchAttr);
		if (value0 && value0 != value1) {
			this.prepopulated = false;
			return;
		}
		if (value1) {
			this.node.value = value1;
			JLJS.setAttr(this.node, this.searchAttr, '');
			JLJS.setAttr(this.node, JLJS.prfx.bAattrs + this.searchAttr, value1);
		} else if (value2) {
			this.node.value = value2;
		}
		this.prepopulated = true;
	},
	
	removeInfoText : function () {
		var value = JLJS.getAttr(this.node, JLJS.prfx.bAattrs + this.searchAttr);
		if (this.node.value == value) {
			this.node.value = '';
		}
	},
	
	setStatus : function (status) {
		if (!status || typeof status != 'string' || !this.classNames[status]) return;
		this.status = status;
		for (var i in this.classNames) {
			JLJS.classAttr.remove(this.node, this.classNames[i]);
		}
		if (this.classNames[this.status]) {
			JLJS.classAttr.add(this.node, this.classNames[this.status]);
		}
	}
};

function JLJS_InteractiveTextField_Setup () {
	var nodes1 = JLJS.getElementsByTagName('input');
	var nodes2 = JLJS.getElementsByTagName('textarea');
	var nodes  = JLJS.concatNodeList(nodes1, nodes2);
	for (var i = 0; i < nodes.length; i++) {
		var type = (nodes[i].nodeName.match(/^input$/i)) ? JLJS.getAttr(nodes[i], 'type') : 'textarea';
		if (!type || !type.match(/^(text|password|textarea)$/i)) continue;

		var ITF = new JLJS_InteractiveTextField(nodes[i]);
		if (ITF && ITF.node && ITF.node._ITF_instance_) {
			JLJS.addEvent(ITF.node, 'focus', function (e) {
				var obj = e.currentTarget._ITF_instance_;
				obj.focused = true;
				obj.removeInfoText();
				obj.setStatus('focus');
			});
			JLJS.addEvent(ITF.node, 'blur' , function (e) {
				var obj = e.currentTarget._ITF_instance_;
				obj.focused = false;
				obj.prepopulateInfoText();
				obj.setStatus((obj.prepopulated) ? 'disabled' : 'default');
			});
		}
	}
}

JLJS.addOnload(JLJS_InteractiveTextField_Setup);

/* ------ JLJS_Selector ----- */

function JLJS_Selector (selectNode) {
	this.setNode(selectNode);
}

JLJS_Selector.prototype = {
	setNode : function(node) {
		if (typeof node == 'object' && node.nodeName && node.nodeName.match(/^select$/i)) {
			this.node = node;
		}
	},

	adjust : function(target, arg) {
		if (this.node && typeof target == 'string' && arg) {
			this.node.options[0].selected = true;
			for (var i = 0; i < this.node.options.length; i++) {
				var value = this.node.options[i][target];
				if (value && (typeof arg == 'number' && parseInt(value, 10) == arg || typeof arg == 'string' && value == arg)) {
					this.node.options[i].selected = true;
					break;
				}
			}
		}
	},
	
	adjustByLabel : function (arg) {
		this.adjust('text', arg);
	},

	adjustByValue : function (arg) {
		this.adjust('value', arg);
	}
}

/* ------ JLJS_DateSelector ----- */

function JLJS_DateSelector() {
	this.Date = new Date();
	this.today = new Date();
	this.days = new Array("\u65e5","\u6708","\u706b","\u6c34","\u6728","\u91d1","\u571f");
	this.dayOfWeekFlag = false;
	this.end = new Date();
	this.endDay = null;
	
	if (arguments.length == 3) {
		this.year   = new JLJS_Selector(arguments[0]);
		this.month  = new JLJS_Selector(arguments[1]);
		this.day    = new JLJS_Selector(arguments[2]);
		this.format = (this.year.node && this.month.node && this.day.node) ? 3 : 0;
	} else if (arguments.length == 2) {
		this.month  = new JLJS_Selector(arguments[0]);
		this.day    = new JLJS_Selector(arguments[1]);
		this.format = (this.month.node && this.day.node) ? 2 : 0;
	}
}

JLJS_DateSelector.prototype = {
	adjust : function() {
		if (this.year)  this.year.adjustByLabel(this.Date.getFullYear());
		if (this.month) this.month.adjustByLabel(this.Date.getMonth() + 1);		
		if (this.day)   this.day.adjustByLabel(this.Date.getDate());
	},

	applyOffset : function () {
		var offset = { y : 0, m : 0, d : 0 };
		var ptn    = /^((\+|\-)\d+)(y|m|d)$/;
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == 'string' && arguments[i].match(ptn)) {
				offset[RegExp.$3] = eval(RegExp.$1);
			}
		}
		this.Date.setFullYear(this.Date.getFullYear() + offset.y);
		this.Date.setMonth(this.Date.getMonth() + offset.m);
		this.Date.setDate(this.Date.getDate() + offset.d);
		if (!this.dayOfWeekFlag) this.adjust();
	},

	adjustToToday : function () {
		this.Date = new Date();
		if (!this.dayOfWeekFlag) this.adjust();
	},

	adjustToDate : function (arg) {
		if (arg.constructor == Date) {
			this.Date = arg
		} else if (typeof arg == 'string' && arg.match(/^[\d\-\/]*$/)) {
			var date = (arg.match(/\-/)) ? arg.split('-') : arg.split('/');
			if (date.length == 3) {
				this.Date.setDate(1);
				this.Date.setFullYear(parseInt(date[0], 10));
				this.Date.setMonth(parseInt(date[1], 10) - 1);
				this.Date.setDate(parseInt(date[2], 10));
			} else if (date.length == 2) {
				this.Date.setDate(1);
				this.Date.setMonth(parseInt(date[0], 10) - 1);
				this.Date.setDate(parseInt(date[1], 10));
			} else {
				return;
			}
		}
		if (!this.dayOfWeekFlag) this.adjust();
	},
	
	setDateBySelecter : function () {
		this.Date.setDate(1);
		if (this.month) this.Date.setMonth(parseInt(this.month.node.value, 10) - 1);
		if (this.year) {
			this.Date.setFullYear(this.year.node.value);	
		}else{
			this.Date.setFullYear(this.getFullYear());
		}
		if (this.day) {
			if (this.dayOfWeekFlag){
				var endDate = new Date(this.Date.getFullYear(), this.Date.getMonth() + 1, 1)
				endDate.setDate(0);
				if (endDate.getDate() >= this.day.node.value) {
					this.Date.setDate(this.day.node.value);
				}
			}else{
				this.Date.setDate(this.day.node.value);
			}
		}
	},
	
	setDay : function (zeroPadFlg) {
		
		var day = this.Date.getDate();
		
		this.day.node.options.length = 0;

		var fullYear = this.getFullYear();
		
		this.end.setFullYear(fullYear);
		this.end.setDate(1);
		this.end.setMonth(this.Date.getMonth() + 1);
		this.end.setDate(0);
		this.endDay = parseInt(this.end.getDate());
		for (i=0;i<this.endDay;i++) {
			this.end.setDate(i+1);
			var newOPT = document.createElement( 'option' );
			newOPT.value = (zeroPadFlg) ? this.padZero(i+1, 2) : i+1;
			newOPT.text = parseInt(i+1) + "\u65e5(" + this.days[this.end.getDay()] + ")";
			if( JLJS.env.isIE ) {
				this.day.node.add( newOPT, i )
			} else {
				this.day.node.appendChild( newOPT );
			}
			
		}
		this.adjust();
	},
	
	padZero : function (val, argLength) {
		val = "" + val;
		var result = val;
		for (var i = 1 ; i <= (argLength - val.length); i++) {
			result = "0" + result;
		}
		return result;		
	},
	
	getFullYear : function () {
		var fullYear = 0;
		if (this.year) {
			fullYear = parseInt(this.year.node.value, 10);
		}else if (this.Date.getMonth()< this.today.getMonth()) {
			fullYear = this.today.getFullYear() + 1;
		} else {
			fullYear = this.today.getFullYear();
		}
		return fullYear;
	}
};



/* ------ JLJS_JMBMemberLoginForm ------ */

function JLJS_JMBMemberLoginForm (formNode) {
	if (!formNode) return;
	this.formNode         = formNode;
	this.formNode._super_ = this;
	this.cookieKey        = 'member_no';
	this.AUTO_LOGIN_COOKIE_KEY = 'autoLoginGuideUnDisp';
	this.AUTO_LOGIN_GUIDE_PATH = "https://www.jal.co.jp/cgi-bin/jal/auto_login_guide/index.cgi";
	this.PARAM_ACTION = "paramAction";
	this.cookieExpireYear = 10;
	this.memberNo         = null;
	this.init();
	this.getValueFromCookie();
	this.reflectStatusToField();
}

JLJS_JMBMemberLoginForm.prototype = {
	init : function () {
		var nodes = JLJS.getElementsByTagName('input', this.formNode);
		for (var i = 0; i < nodes.length; i++) {
			var node = nodes[i];
			var type = JLJS.getAttr(node, 'type');
			if (!type) continue;
			if (!this.memberNoField && type.toLowerCase() == 'text') {
				this.memberNoField = node;
			}
			if (!this.memberPwField && type.toLowerCase() == 'password') {
				this.memberPwField = node;
				this.memberPwField.value = '';
			}
			if (!this.AutoCompCBox && type.toLowerCase() == 'checkbox') {
				this.AutoCompCBox = node;
			}
		}
		this.setupMemberNoEvent();
		this.setupPasswordEvent();
	},
	
	setupMemberNoEvent : function(){
		if(!this.memberNoField.value){
			this.memberNoField.style.backgroundImage = "url(../img/txt_jmb_side_003.gif)";
		}
		var self = this;
		JLJS.addEvent(this.memberNoField, 'focus', function (e) {
			self.memberNoField.style.backgroundImage = "";
		} );
		
		JLJS.addEvent(this.memberNoField, 'blur', function (e) {
			if(self.memberNoField.value) return;
			self.memberNoField.style.backgroundImage = "url(../img/txt_jmb_side_003.gif)";
		} );
	},
	
	setupPasswordEvent : function(){
		if(!this.memberPwField.style.value){
			this.memberPwField.style.backgroundImage = "url(../img/txt_jmb_side_004.gif)";
		}
		var self = this;
		JLJS.addEvent(this.memberPwField, 'focus', function (e) {
			self.memberPwField.style.backgroundImage = "";
		} );
		
		JLJS.addEvent(this.memberPwField, 'blur', function (e) {
			if(self.memberPwField.value) return;
			self.memberPwField.style.backgroundImage = "url(../img/txt_jmb_side_004.gif)";
		} );
	},
	getValueFromCookie : function () {
		var data = (document.cookie) ? document.cookie.split(';') : [];
		for (var i=0; i<data.length; i++) {
			if (data[i].split('=')[0].replace(/\s/g, '') == this.cookieKey) {
				this.memberNo = unescape(data[i].split('=')[1].replace(/\s/g, ''));
				break;
			}
		}
	},
	reflectStatusToField : function() {
		if (this.memberNo > 0) {
			if (this.memberNoField) {
				this.memberNoField.style.backgroundImage = '';
				this.memberNoField.value = this.memberNo;
			}
		}
	},

	checkInputData : function () {
		if (!this.memberNoField || !this.memberPwField || this.submitLock) return;
		if (this.memberNoField.value.length != 9 && this.memberNoField.value.length != 7) {
			alert(MSG_ENTER_MEMNO);
			this.memberNoField.focus();
			return;
		}
		if (this.memberPwField.value.length != 4 && this.memberPwField.value.length != 6) {
			alert (MSG_ENTER_PASSWD);
			this.memberPwField.focus();
			return;
		}
		this.submitLock = true;
		this.memberNoField.value = this.memberNoField.value.replace(/[\uff10-\uff19]/g, function(str){return String.fromCharCode(str.charCodeAt(0)-65248);});
		this.setFormAction();
		this.formNode.submit();
	},
	
	setFormAction : function () {
		this.formNode[this.PARAM_ACTION].value = "";
		if(this.AutoCompCBox.checked){
			var autoLoginCookie = JLJS_CookieMgr.getCookie(this.AUTO_LOGIN_COOKIE_KEY);
			if(!autoLoginCookie){
				this.setAction();
				return;
			}
			var memberCookie = autoLoginCookie.split(",");
			for (var i=0;i<memberCookie.length;i++){
				if(memberCookie[i] == this.memberNoField.value) {
					return;
				}
			}
			this.setAction();
		}
	},
	
	setAction : function (){
		this.formNode[this.PARAM_ACTION].value = this.formNode.action;
		this.formNode.action = this.AUTO_LOGIN_GUIDE_PATH;
		
	}
	
};

function JLJS_JMBMemberLoginFormSetup () {
	window.JLJSJMBLoginForm = null;
	var form  = document.getElementById("memberLogin");
	if (form) {
		JLJSJMBLoginForm = new JLJS_JMBMemberLoginForm(form);
		JLJS.addEvent(JLJSJMBLoginForm.formNode, 'submit', function (e) {
			e.preventDefault();
			JLJSJMBLoginForm.checkInputData();
		} );
	}
}

JLJS.addOnload(JLJS_JMBMemberLoginFormSetup);



/* ------ JLJS_ReserveSidePaneForm ------ */

function JLJS_ReserveSidePaneForm (formNode) {
	if (!formNode) return;
	this.formNode           = formNode;
	this.formNode._super_   = this;
	this.dateSelectors      = [];
	this.airportSelector    = {};
	this.narrowDownSelector = {};
}

JLJS_ReserveSidePaneForm.prototype = {
	setDateSelector : function (name1, name2) {
		if (!name1 || !name2) return;
		var ptn  = /^select$/i;
		var sel1 = this.formNode[name1]
		var sel2 = this.formNode[name2]
		if (sel1 && sel1.nodeName.match(ptn) && sel2 && sel2.nodeName.match(ptn)) {
			this.dateSelectors[this.dateSelectors.length] = new JLJS_DateSelector(sel1, sel2);
		}
	},

	adjustDateSelector : function () {
		for(var i = 0; i < this.dateSelectors.length; i++) {
			this.dateSelectors[0].adjustToToday();
		}
		if (this.dateSelectors[1]) {
			this.dateSelectors[1].applyOffset('+3d');
		}
	},

	setAirportSelector : function (name1, name2) {
		if (!name1 || !name2) return;
		var ptn   = /^select$/i;
		var sels  = {
			depart  : this.formNode[name1],
			arrival : this.formNode[name2]
		};
		if (sels.depart && sels.depart.nodeName.match(ptn) && sels.arrival && sels.arrival.nodeName.match(ptn)) {
			this.airportSelector = sels;
		}
	},

	setNarrowDownSelector : function (name1, name2, groupTable) {
		if (!name1 || !name2 || !groupTable) return;
		var ptn = /^select$/i;
		var nd  = {
			area : this.formNode[name1],
			disp : this.formNode[name2],
			buff : {}
		};
		if (nd.area && nd.area.nodeName.match(ptn) && nd.disp && nd.disp.nodeName.match(ptn)) {
			if (!this.narrowDownSelector.buff) {
				for (var i = 0; i < nd.disp.options.length; i++) {
					nd.buff[nd.disp.options[i].value] = nd.disp.options[i].text;
				}
			}
			this.narrowDownSelector              = nd;
			this.narrowDownSelector.area._super_ = this;
			this.narrowDownSelector.groupTable   = groupTable;
		}
	},

	airportCheck : function () {
		var sel1 = this.airportSelector.depart;
		var sel2 = this.airportSelector.arrival;
		if (!sel1 || !sel2) return;
		var sel1value = sel1.options[sel1.selectedIndex].value;
		var sel2value = sel2.options[sel2.selectedIndex].value;
		
		if (sel1value == sel2value || sel1value == '123' || sel2value == '123') {
			alert(MSG_CONFIRM_AIRPORT);
			sel1.focus();
			return false;
		} else {
			return true;
		}
	},
	
	airportNarrowDown : function (group) {
		var sel1 = this.narrowDownSelector.area;
		var sel2 = this.narrowDownSelector.disp;
		var buff = this.narrowDownSelector.buff;
		var gtbl = this.narrowDownSelector.groupTable;
		if (!sel1 || !sel2 || !buff || !gtbl) return;
		if (JLJS.env.isIE) {
			while (sel2.options[0]) {
				sel2.options.remove(0);
			}
		} else {
			while (sel2.firstChild) {
				sel2.removeChild(sel2.firstChild);
			}
		}
		var i = 0, ptn = (group && gtbl[group]) ? new RegExp('^(' + gtbl[group].join('|') + ')$') : /.*/;
		for (var value in buff) {
			if (value.match(ptn)) {
				var cOPT   = document.createElement('option');
				cOPT.value = value;
				cOPT.text  = buff[value];
				if (JLJS.env.isIE) {
					sel2.add(cOPT, i);
					i++;
				} else {
					sel2.appendChild(cOPT);
				}
			}
		}
		sel2.options[0].selected = true;
	}
};



