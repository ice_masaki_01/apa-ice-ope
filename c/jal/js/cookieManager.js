function JLJS_CookieManager() {
	this.DELIMITER = "|";
	this.PATH = "/";
	this.C_PATH = "cpath";
	this.pathname = window.location.pathname;
}

JLJS_CookieManager.prototype = {
	
	setCookie : function(name, value, options) {
		var mValue = {};
		mValue[name] = value;
		mValue[JLJS_CookieMgr.C_PATH] = JLJS_CookieMgr.pathname;
		
		JLJS_CookieMgr.setMulti(name, mValue, options);
	},
	
	getCookie : function(name) {
		var mValue = JLJS_CookieMgr.getMulti(name);
		var value;
		
		if(mValue) {
			if(!mValue[JLJS_CookieMgr.C_PATH]) {
				value = mValue[""];
			} else if(mValue[JLJS_CookieMgr.C_PATH] == JLJS_CookieMgr.pathname) {
				value = mValue[name];
			}
		}
		
		return value;
	},
	
	removeCookie : function(name) {
		var mValue = JLJS_CookieMgr.getCookie(name);
		if(mValue) {
			JLJS_CookieMgr.remove(name);
		}
	},
	
	setMultiCookie : function(mName, valueHash, options) {
		var mValue = {};
		for(var key in valueHash) {
			mValue[key] = valueHash[key];
		}
		mValue[JLJS_CookieMgr.C_PATH] = JLJS_CookieMgr.pathname;
		JLJS_CookieMgr.setMulti(mName, mValue, options);
	},
	
	addMultiCookie : function(mName, name, value, options) {
		var mValue = JLJS_CookieMgr.getMultiCookie(mName);
		if(!mValue) {
			mValue = {};
		}
		mValue[name] = value;
		JLJS_CookieMgr.setMultiCookie(mName, mValue, options);
	},
	
	getMultiCookie : function(mName) {
		var mValue = JLJS_CookieMgr.getMulti(mName);
		var valueHash;
		
		if(mValue) {
			if(!mValue[JLJS_CookieMgr.C_PATH]) {
				valueHash = mValue;
			} else if(mValue[JLJS_CookieMgr.C_PATH] == JLJS_CookieMgr.pathname) {
				delete mValue[JLJS_CookieMgr.C_PATH];
				valueHash = mValue;
			}
		}
		return valueHash;
	},
	
	removeMultiCookie : function(mName, name, options) {
		var mValue = JLJS_CookieMgr.getMultiCookie(mName);
		if(mValue) {
			JLJS_CookieMgr.removeMulti(mName, name, options);
		}
		
	},
	
// private function	
	
	trim : function(str) {
		return 	("" + str).replace(/^\s+|\s+$/g, "");
	},
	
	set : function(name, value, options) {
		name = JLJS_CookieMgr.trim(name);
		value = JLJS_CookieMgr.trim(value);

		var pathFlg = false;
		var option = ""
		for(var optKey in options) {
			option = option + optKey + "="  + options[optKey] + ";";
			if(optKey.toLowerCase() == "path") {
				pathFlg = true;
			}
		}
		if(!pathFlg) {
			option = option + "path=" + JLJS_CookieMgr.PATH + ";";
		}
		
		document.cookie = escape(name) + "=" + escape(value) + ";" + option;
	},
	
	get : function(name) {
		name = JLJS_CookieMgr.trim(name);

		var value;
		if(document.cookie) {
			var array = document.cookie.split(";");
			for(var i=0; i<array.length; i++) {
				array[i] = JLJS_CookieMgr.trim(array[i]);
				var index = array[i].indexOf("=");
				var cName = array[i];
				if(index != -1) {
					cName = array[i].substring(0, index);
				}
				cName = unescape(cName);
				if(name == cName) {
					if(index == -1) {
						value = cName;
					} else {
						value = unescape(array[i].substring(index+1));
					}
					break;
				}
			}
			
		}
		return value;
	},
	
	remove : function(name) {
		name = JLJS_CookieMgr.trim(name);

		var date = new Date();
		date.setFullYear(date.getFullYear() - 10);
		var expires = date.toGMTString();
		document.cookie = escape(name) + "=;path=" + JLJS_CookieMgr.PATH + ";expires=" + expires + ";";
	},
	
	setMulti : function(mName, valueHash, options) {
		mName = JLJS_CookieMgr.trim(mName);

		var mValue = "";
		var cnt = 0;
		for(var key in valueHash) {
			if(mValue) {
				mValue = mValue + JLJS_CookieMgr.DELIMITER;	
			}
			mValue = mValue + JLJS_CookieMgr.trim(key) + "=" + JLJS_CookieMgr.trim(valueHash[key]);
			cnt++;
		}
		if(!mValue || (mValue && cnt == 1 && valueHash[JLJS_CookieMgr.C_PATH])) {
			JLJS_CookieMgr.remove(mName);
		} else {
			JLJS_CookieMgr.set(mName, mValue, options);
		}
	},
	
	addMulti : function(mName, name, value, options) {
		mName = JLJS_CookieMgr.trim(mName);
		name = JLJS_CookieMgr.trim(name);
		value = JLJS_CookieMgr.trim(value);

		var mValue = JLJS_CookieMgr.getMulti(mName);
		if(!mValue) {
			mValue = {};
		}
		mValue[name] = value;
		JLJS_CookieMgr.setMulti(mName, mValue, options);
	},
	
	getMulti : function(mName) {
		mName = JLJS_CookieMgr.trim(mName);

		var mValue;
		var cookie = JLJS_CookieMgr.get(mName);
		if(cookie) {
			var array = unescape(cookie).split(JLJS_CookieMgr.DELIMITER);
			mValue = {};
			for(var i=0; i<array.length; i++) {
				var index = array[i].indexOf("=");
				var name = "";
				var value = "";
				if(index == -1) {
					value = array[i];
				} else {
					name = array[i].substring(0, index);
					value = array[i].substring(index+1);
				}
				mValue[name] = value;
			}
		}
		return mValue;
	},
	
	removeMulti : function(mName, name, options) {
		mName = JLJS_CookieMgr.trim(mName);
		name = JLJS_CookieMgr.trim(name);
		
		var mValue = JLJS_CookieMgr.getMulti(mName);
		if(mValue) {
			delete mValue[name];
			JLJS_CookieMgr.setMulti(mName, mValue, options);
		}
	}
	
}

var JLJS_CookieMgr = new JLJS_CookieManager();
