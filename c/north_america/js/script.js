
	//ページ内リンク

$(function(){
	$('.anc a[href^=#] , .pagetop a[href^=#]').click(function() {
		var winWidth = window.innerWidth;

			var speed = 400;
			var href= $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = Math.ceil(target.offset().top);
			$('body,html').animate({scrollTop:position}, speed, 'swing');
			return false;

	});

	$('.anc02 a[href^=#]').click(function() {
		var winWidth = window.innerWidth;

			var speed = 400;
			var href= $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = Math.ceil(target.offset().top)-20;
			$('body,html').animate({scrollTop:position}, speed, 'swing');
			return false;

	});
	
	$('.city ul li a').hover(function() {
		$(this).children('.ov').fadeToggle();
	});
	
	$('.list ul li a').hover(function() {
		$(this).children('.ov').fadeToggle();
	});

	//SP アコーディオン

	$(function(){
		var wid = $(window).innerWidth();
		if (wid < 771){
			$(".menu , .nav ul li a").click(function(){
				$('.menu').toggleClass("active");
				$('.nav ul').slideToggle(500);
			});
			return false;
		}
	});

});

// bxslider

$(window).load(function() {

	$('.bxslider').bxSlider({
		auto:true,
		controls:true,
		pager:false,
		speed:800,
		moveSlides:1,
	});

});
