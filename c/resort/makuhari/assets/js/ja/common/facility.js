$ = jQuery;
$(function(){
  var prefecture = {
    change : function(){
      $('select.select_prefecture').change(function(){
        var searchbox = $(this).closest('.search_box');
        var val = $(this).val();
        var areas_id = prefecture.get_area(val);
        searchbox.find('select.select_city option').hide().attr('selected',false);
        searchbox.find('select.select_city option').each(function(){
          if($.inArray($(this).attr('value'), areas_id) != '-1' || areas_id.length == 0){
            $(this).show();
          };
        });
        searchbox.find('select.select_city option').first().show();
        /*
        var set_val = $('select.select_city option:visible').first().attr('value');
        if(set_val){
          $('select.select_city').val(set_val);
        }else{
          $('select.select_city').val('');
          $('select.select_city option').show().attr('selected',false);
        }*/
      });
    },
    get_area : function(prefecture){
      var areas_id = [];
      switch(prefecture){
        case '0': 
          areas_id = ['0000','0001','0002','0003'];
        break;
        case '7': 
          areas_id = ['0700','0701','0702'];
        break;
        case '10': 
          areas_id = ['1000','1001','1002','1003','1004','1005','1006','1007','1008'];
        break;
        case '11': 
          areas_id = ['1100','1101'];
        break;
        case '17': 
          areas_id = ['1700','1701','1702'];
        break;
        case '19': 
          areas_id = ['1900','1901'];
        break;
        case '21': 
          areas_id = ['2100','2101'];
        break;
        case '24': 
          areas_id = ['2400','2401','2402','2403','2404','2405'];
        break;
        case '25': 
          areas_id = ['2500','2501'];
        break;
        case '27': 
          areas_id = ['2700','2701'];
        break;
        case '39': 
          areas_id = ['3900','3901'];
        break;
      };
      return areas_id;
    },
    call : function(){
      prefecture.change();
    },
  };
  prefecture.call();

  var area = {
    change : function(){
      $('select.select_city').change(function(){
        var searchbox = $(this).closest('.search_box');        
        var val = $(this).val();
        var area_id = area.get_area(val);
        if(area_id){
          searchbox.find('select.select_prefecture option').attr('selected',false);
        }
          searchbox.find('select.select_prefecture option').each(function(){
          if($(this).attr('value') == area_id && area_id != false || area_id == 0){
            searchbox.find('select.select_prefecture').val(area_id);
          };
        });
      });
    },
    get_area : function(prefecture){
      var area_id = false;
      switch(prefecture){
        case '0000':
        case '0001':
        case '0002':
        case '0003': 
          area_id = '0';
        break;
        case '0700':
        case '0701':
        case '0702':
          area_id = '7';
        break;
        case '1000':
        case '1001':
        case '1002':
        case '1003':
        case '1004':
        case '1005':
        case '1006':
        case '1007':
        case '1008':
          area_id = '10';
        break;
        case '1100':
        case '1101':
          area_id = '11';
        break;
        case '1700': 
        case '1701': 
        case '1702': 
          area_id = '17';
        break;
        case '1900': 
        case '1901': 
          area_id = '19';
        break;
        case '2100': 
        case '2101': 
          area_id = '21';
        break;
        case '2400': 
        case '2401': 
        case '2402': 
        case '2403': 
        case '2404': 
        case '2405': 
          area_id = '24';
        break;
        case '2500': 
        case '2501': 
          area_id = '25';
        break;
        case '2700': 
        case '2701': 
          area_id = '27';
        break;
        case '3900': 
        case '3901':
          area_id = '39';
        break;
      };
      return area_id;
    },
    call : function(){
      area.change();
    },    
  }
  area.call();

  var load = {
    click : function(){
      $(document).on('click',".sention-branch .more", function(){
        var list = $(this).parent().next();
        $(this).parent().remove();
        load.display(list);
      });
    },
    display : function(list){
      var delay = 50;
      list.find('li').css({'opacity':0}).each(function(){
        $(this).delay(delay).animate({'opacity':1}).removeClass('hide');
        delay = delay + 150;
      });
      list.next('.loadbtn').removeClass('hide').fadeIn('fast');
    },
    call : function(){
      load.click();
    }
  };
  load.call();

  var ajax = {
    click : function(){
      $(document).on('click',".search_btn", function(){
        var searchbox = $(this).closest('.search_box');
        var prefecture_id = searchbox.find('.select_prefecture').val();
        var area_id = searchbox.find('.select_city').val();
        var type = $(this).closest('.section-area-search').data('type');
        ajax.ajax_load(prefecture_id, area_id, type);
      });
    },
    ajax_load : function(prefecture_id, area_id, type){
      var lang = $('body').data('lang');
      ajax.loading.show();
      $.ajax({
        type: "POST",
        url: "/ja_"+lang+"/facility/-ajax_list.php",
        data: {
          "prefecture": prefecture_id,
          "area" : area_id,
          "p" : type
        },
        success: function(data){
          ajax.loading.hide();
          $('.sention-branch:visible').html(data);
          var delay = 50;
          $('.sention-branch:visible ul:first li').css({'opacity':0}).each(function(){
            $(this).delay(delay).animate({'opacity':1}).removeClass('hide');
            delay = delay + 150;
          });
        }
      });
    },
    loading : {
      show : function(){
        $('.sention-branch').addClass('loading'); 
      },
      hide : function(){
        $('.sention-branch').removeClass('loading');         
      },
    },
    call : function(){
      ajax.click();
    }
  };
  ajax.call();

})