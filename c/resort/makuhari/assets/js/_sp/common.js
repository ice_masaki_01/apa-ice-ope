$ = jQuery;
$(function(){

  var scroll = {
    click : function(){
      $('a[href^=#]').click(function() {
        var speed = 400;
        var href = $(this).attr("href");
        var target = $(href === "#" || href === "" ? 'html' : href);
        if(target.offset() == undefined){
          var  position = '0px';
        }else{
          var position = target.offset().top;
        }
        $('body,html').animate({
          scrollTop: position
        }, speed, 'swing');
        return false;
      });
    }
  };
  scroll.click();

  var _photo = {
    change_image : function(){
      $('._photo-bottom .thumbnail li').hover(function(){
        var img = $(this).find('img');
        var main = $(this).closest('._photo-bottom').find('.main');
        main.find('img').attr('src', img.attr('src'));
      })
    },
    call : function(){
      _photo.change_image();
    },
  };
  _photo.call();
  
  navigation = {};
  navigation.initialize = (function(){
			$(function() {
				var $header = $('.header');

				// Nav Toggle Button
				$('#nav-toggle').click(function(){
					$header.toggleClass('open');
				});
			});
		});
  navigation.initialize();
  
});
