$ = jQuery;
$(function() {
  //バリデーション
  //ページトップに戻るを追加
  $("body").prepend('<a name="page_top" id="page_top"></a>');
  $("body").append('<div id="go_top"><a href="#page_top"><img src="/global/assets/images/admin/btn_pagetop.png" alt="" /></a></div>');
  $('#go_top').css("position", "fixed").css("bottom", "10px").css("right", "10px").css("zIndex", "9999");
  $('#go_top a').click(function() {
    // スクロールの速度
    var speed = 400; // ミリ秒
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('body,html').animate({
      scrollTop: position
    }, speed, 'swing');
    return false;
  })

  var prefecture = {
    change : function(){
      $('select.prefecture').change(function(){
        var val = $(this).val();
        var areas_id = prefecture.get_area(val);
        $('select.area option').hide().attr('selected',false);
        var set_val = 'undefined';
        var index = 0
        $('select.area option').each(function(){
          if($.inArray($(this).attr('value'), areas_id) != '-1'){
            if(index == 0){
              set_val = $(this).attr('value');
            }
            $(this).show();
            index = index + 1;
          };          
        });
        $('select.area').closest('.inside2').show();
        if(set_val != 'undefined'){
          $('select.area').val(set_val);
        }else{
          $('select.area').closest('.inside2').hide();
        }
      });
    },
    get_area : function(prefecture){
      var areas_id = [];
      switch(prefecture){
        case '0': 
          areas_id = ['0000','0001','0002','0003'];
        break;
        case '7': 
          areas_id = ['0700','0701','0702'];
        break;
        case '10': 
          areas_id = ['1000','1001','1002','1003','1004','1005','1006','1007','1008'];
        break;
        case '11': 
          areas_id = ['1100','1101'];
        break;
        case '16': 
          areas_id = ['1600','1601','1602'];
        break;
        case '19': 
          areas_id = ['1900','1901'];
        break;
        case '21': 
          areas_id = ['2100','2101'];
        break;
        case '24': 
          areas_id = ['2400','2401','2402','2403','2404','2405'];
        break;
        case '25': 
          areas_id = ['2500','2501'];
        break;
        case '27': 
          areas_id = ['2700','2701'];
        break;
        case '39': 
          areas_id = ['3900','3901'];
        break;
      };
      return areas_id;
    },
    call : function(){
      prefecture.change();
    },
  };
  prefecture.call();

  var table = {
    hide: function() {
      $('.inside2 table thead').live('click', function() {
        $(this).parent().find('tbody').toggle();
      });
    },
    add : function(){
      $('.add_table_row').live('click', function() {
        var new_tr = $(this).closest('tr').prev('.row').clone();
        var length = new_tr.data('length') + 1;
        new_tr.find('.button.delete_table_row.hide').show();
        table.reset(new_tr);
        var group_index = $(this).closest('.box-sub_field').find('.wrapper-inside').data('length');
        var ajax_add = $(this).closest('.wrapper-inside').data('ajax_add');
        if(ajax_add === 1){//cloneされたtrでgroup_indexが元の値になってしまうため追加
          ++group_index;
        }
        table.key.set(new_tr, length, group_index);
        $(this).closest('tr').before(new_tr);
      })
    },
    remove: function() {
      $('.delete_table_row').live('click', function() {
        var tr = $(this).closest('tr');
        var target_table = $(this).closest('table')
        tr.remove();
        table.create_remove_hidden_tag(target_table);
        table.key.reset(target_table);
      })
    },
    create_remove_hidden_tag: function(table) {
      var tr_index = table.find('tr.row').length;
      var type = table.find('tr.row').data('type');
      var group_index = table.closest('.wrapper-inside').data('length');
      table.find('select,textarea,input[type="text"]').each(function() {
          if(type === 'group_table'){
            //グループの場合
            if($(this).data('name_saved_3') == null || $(this).data('name_saved_3') === '' || Number.isNaN($(this).data('name_saved_3'))){
              //select,note
              var name_1 = $(this).data('name_saved_1');
              var name_2 = $(this).data('name_saved_2');
              var code = '<input type="hidden" name=delete[] value="' + name_1 + '_' + group_index + '_' + name_2 + '"/>';
              }else{
                //tr
                var name_1 = $(this).data('name_saved_1');
                var name_2 = $(this).data('name_saved_2');
                var name_3 = $(this).data('name_saved_3');
                var code = '<input type="hidden" name=delete[] value="' + name_1 + '_' + group_index + '_' + name_2 + '_' + tr_index + '_' + name_3 + '"/>';
              }
          }else{
            //グループではない場合
            if($(this).data('name_saved_1') === '' || $(this).data('name_saved_2') === ''){
              //select,note
              var name = $(this).data('name_saved');
              var code = '<input type="hidden" name=delete[] value="' + name + '"/>';
            }else{
              //tr
              var name_1 = $(this).data('name_saved_1');
              var name_2 = $(this).data('name_saved_2');
              var code = '<input type="hidden" name=delete[] value="' + name_1 + '_' + tr_index + '_' + name_2 + '"/>';
            }
          }
        $('form#post').prepend(code);
      });
    },
    reset : function(tr){
      tr.find('textarea,input[type="text"]').val('');
    },
    key : {
      set: function(tr, tr_index, group_index) {
        tr.attr('data-length', tr_index);
        tr.find('textarea,input[type="text"],input[type="hidden"]').each(function(){
          var name = table.key.get_name($(this), tr_index, group_index);
          $(this).attr('name', name);
          $(this).attr('data-name', name);
        });
      },
      get_name : function(form_tag, tr_index, group_index){
        if(group_index == null || Number.isNaN(group_index)){
          var name_1 = form_tag.data('name_1');
          var name_2 = form_tag.data('name_2');
          var name = 'values' + name_1 + '[' + tr_index + ']' + name_2;
        }else{
          var name_1 = form_tag.data('name_1');
          var name_2 = form_tag.data('name_2');
          var name_3 = form_tag.data('name_3');
          var name = 'values' + name_1 + '[' + group_index + ']' + name_2 + '[' + tr_index + ']' + name_3;
        }
        return name;
      },
      reset: function(target_table) {
//        var length = 0;
//        target_table.find('tr.row').each(function() {
//          table.key.set($(this), length);
//          ++length;
//        })
      }
    },
    call : function(){
      table.hide();
      table.add();
      table.remove();
    }
  };
  table.call();



  var sub_field = {
    group: {
      hide: function() {
        $('.box-sub_field_h').live('click', function() {
          $(this).parent().find('.wrapper-inside').toggle();
        });
      },
      add: function() {
        $('.add_group').live('click', function() {
          var new_sub_field = $(this).closest('.box-sub_field').clone();
          var length = new_sub_field.find('.wrapper-inside').data('length') + 1;
          new_sub_field.find('.wrapper-inside').attr('data-ajax_add','1');
          new_sub_field.find('.button.delete_group.hide').show();
          sub_field.group.reset(new_sub_field);
          sub_field.group.key.set(new_sub_field, length);
          $(this).closest('.box-sub_field').after(new_sub_field);
          $(this).remove();
        })
      },
      remove: function() {
        $('.delete_group').live('click', function() {
          var field = $(this).closest('.box-sub_field');
          var inside = field.closest('.box-sub_field').find('.wrapper-inside');
          field.remove();
          sub_field.group.create_remove_hidden_tag(inside);
          sub_field.group.key.reset(inside);
        })
      },
      create_remove_hidden_tag: function(inside) {
        var group_index = inside.data('length');
        inside.find('.inside2 input[type="text"],.inside2 input[type="checkbox"], .inside2 input[type="radio"], .inside2 select, .inside2 textarea, .inside2 .btn-uploader').each(function() {
          if($(this).data('name_saved_3') == null || $(this).data('name_saved_3') === ''){
            var name_1 = $(this).data('name_saved_1');
            var name_2 = $(this).data('name_saved_2');
            var code = '<input type="hidden" name=delete[] value="' + name_1 + '_' + group_index + '_' + name_2 + '"/>';
          }else{
            var tr_index = $(this).closest('tr.row').data('length');
            var name_1 = $(this).data('name_saved_1');
            var name_2 = $(this).data('name_saved_2');
            var name_3 = $(this).data('name_saved_3');
            var code = '<input type="hidden" name=delete[] value="' + name_1 + '_' + group_index + '_' + name_2 + '_' + tr_index + '_' + name_3 + '"/>';            
          }
          $('form#post').prepend(code);
        });
      },
      reset: function(field) {
        field.find('input[type="text"]').val('');
        field.find('textarea').val('');
        field.find('select option:selected').attr('selected', false);
        field.find('input[type="checkbox"]').prop('checked', false);
        field.find('input[type="radio"]').prop('checked', false);
        field.find('input[type="radio"]').eq(0).prop('checked', true);
        field.find('.upload_images li').remove();
        field.find('tr.row:not(:eq(0))').remove();
      },
      key: {
        set: function(field, group_index) {
          field.find('.wrapper-inside').attr('data-length', group_index);
          field.find('.inside2 input[type="text"],.inside2 input[type="checkbox"], .inside2 input[type="radio"], .inside2 select, .inside2 textarea ,.inside2 input[type="hidden"], .inside2 .btn-uploader').each(function() {
            var tr_index = $(this).closest('tr.row').data('length');
            var name = sub_field.group.key.get_name($(this), group_index, tr_index);
            $(this).attr('name', name);
            $(this).attr('data-name', name);
          })
        },
        get_name: function(form_tag, group_index, tr_index) {
          if(tr_index == null){
            var name_1 = form_tag.data('name_1');
            var name_2 = form_tag.data('name_2');
            var name = 'values' + name_1 + '[' + group_index + ']' + name_2;
          }else{
            var name_1 = form_tag.data('name_1');
            var name_2 = form_tag.data('name_2');
            var name_3 = form_tag.data('name_3');
            var name = 'values' + name_1 + '[' + group_index + ']' + name_2 + '[' + tr_index + ']' + name_3;
          }
          return name;
        },
        reset: function(inside) {
          inside.find('.box-sub_field').each(function(i) {
            //sub_field.group.key.set($(this), i);
          });
        }
      }
    },
    call : function(){
      sub_field.group.hide();
      sub_field.group.add();
      sub_field.group.remove();
    }
  };
  sub_field.call();

  var uploader = {
    wp_media : wp.media({
      library: {
        type: 'image'
        },
        multiple: true,
        frame: "post",
        state: "insert"
      }),
    open : function(){
      $('.btn-uploader').live('click', function(e) {
        e.preventDefault();
        uploader.wp_media.open();
        uploader.selected($(this));
      })
    },
    //投稿画面のスタイル設定
    set_style : function(){
      uploader.wp_media.on('ready', function() {
        $('select.attachment-filters [value="uploaded"]').attr( 'selected', true ).parent().trigger('change'); //画像が投稿されている場合に、selectboxの初期選択を投稿中の画像にする
        $('.media-menu,.media-frame-menu').remove();
        $( '.media-frame-title, .media-frame-router, .media-frame-content, .media-frame-toolbar' ).css( 'left', 0 );
      });
    },
    //画像を選択した後に投稿画面に反映する
    selected : function(btn_upload){
      uploader.wp_media.on('insert', function(eo){
        var name = btn_upload.data('name');
        var ul = btn_upload.parent().find('.upload_images');
        var images = uploader.wp_media.state().get('selection');
        var id = 0;
        var ids = [];
        //登録されている画像を配列に格納する
        ul.children('li').each( function( ){
          var id = Number($(this).attr( 'id' ).slice(4));
          ids.push(id);
        });

        //投稿画面に反映する
        images.each(function(file){
          var new_id = file.toJSON().id;
          if ($.inArray(new_id, ids) > -1 ){ //投稿編集画面のリストに重複している場合、削除
            ul.find('li#img_'+ new_id).remove();
          }
          var src = '';
          if(file.attributes.sizes.thumbnail !== undefined){
            src = file.attributes.sizes.thumbnail.url;
          }else{
            src = file.attributes.sizes.full.url;
          }
          ul.append('<li class="image" id="img_'+ new_id +'"></li>').find('li:last').append(
            '<span class="box-image">' +
            '<a href="#" class="remove" title="画像を削除する"></a>' +
            '<img src="'+src+'" />' +
            '<input type="hidden" name="'+name+'[]" value="'+ new_id +'" />' +
            '</span>'
          );
        });
        uploader.wp_media.off('insert');
      });
    },
    remove : function(){
      $( ".upload_images .remove").live( 'click', function( e ) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('li.image').remove();
      });
    },
    sort : function(){
      $( ".upload_images" ).sortable({
        cursor : "move",
        tolerance : "pointer",
        opacity: 0.6
      });
    },
    call : function(){
      uploader.open();
      uploader.set_style();
      uploader.remove();
      uploader.sort();
    }
  }
  uploader.call();
})