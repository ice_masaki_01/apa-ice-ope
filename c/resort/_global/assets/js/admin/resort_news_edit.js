$ = jQuery;
$(function() {
  var preview = {
    init : function(){
      var val = $('input.language:checked').val();
      if(typeof val == "undefined"){
        val = 'ja';
      }
      preview.change_url(val);
    },
    change : function(){
      $('input.language').change(function(){
        var val = $(this).val();  
        preview.change_url(val);
      });
    },
    change_url : function(val){
    
      preview.set_url($('#wp-admin-bar-live a'), val);
      preview.set_url($('#wp-admin-bar-preview a'), val);
      $('.box-preview a').each(function(){
        preview.set_url($(this), val);
      });
    },
    set_url : function(target, val){
      if(target[0]){
        var author_id = $('#post_author_override').val();
        var urls = $(target).attr('href').split('/');
        var url = '/ja_'+val +'/' + urls[2] + '/' + urls[3] + '/' + urls[4];
        if(author_id == '111'){
          url = '/ja_'+val +'/myoko/' + urls[3] + '/' + urls[4];
        }else{
          url = '/ja_'+val +'/resort_hotels/' + urls[3] + '/' + urls[4];
        }
        var ids_urls = url.split('&');
        if(author_id == '111'){
          ids_urls[1] = 'hotel_id=32726';
        };
        if(ids_urls[2]){
          var url = ids_urls[0] + '&' + ids_urls[1] + '&' + ids_urls[2];
        }else{
          var url = ids_urls[0] + '&' + ids_urls[1];
        }
        target.attr('href',url);
      }
    },
    call : function(){
      preview.init();
      preview.change();
    },
  };
  preview.call();
})