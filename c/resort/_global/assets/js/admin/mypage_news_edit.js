$ = jQuery;
$(function() {

  var preview = {
    init : function(){
      var val = $('input.language:checked').val();
      if(typeof val == "undefined"){
        val = 'en';
      }
      preview.change_url(val);
    },
    change : function(){
      $('input.language').change(function(){
        var val = $(this).val();  
        preview.change_url(val);
      });
    },
    change_url : function(val){
      preview.set_url($('#wp-admin-bar-live a'), val);
      preview.set_url($('#wp-admin-bar-preview a'), val);
      $('.box-preview a').each(function(){
        preview.set_url($(this), val);
      });
    },
    set_url : function(target, val){
      if(target[0]){
        var urls = $(target).attr('href').split('/');
        var url = '/ja_'+val +'/' + urls[2] + '/' + urls[3];
        target.attr('href',url);
      }
    },
    call : function(){
      preview.init();
      preview.change();
    },
  };
  preview.call();
})