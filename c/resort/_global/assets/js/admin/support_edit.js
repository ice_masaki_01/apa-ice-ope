$ = jQuery;
$(function() {
  //バリデーション
  $("#post").validate({});
  //ページトップに戻るを追加
  $("body").prepend('<a name="page_top" id="page_top"></a>');
  $("body").append('<div id="go_top"><a href="#page_top"><img src="/global/assets/images/admin/btn_pagetop.png" alt="" /></a></div>');
  $('#go_top').css("position", "fixed").css("bottom", "10px").css("right", "10px").css("zIndex", "9999");
  $('#go_top a').click(function() {
    // スクロールの速度
    var speed = 400; // ミリ秒
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('body,html').animate({
      scrollTop: position
    }, speed, 'swing');
    return false;
  });
  
  var preview = {
    init : function(){
      var val = $('input.language:checked').val();
      if(typeof val == "undefined"){
        val = 'en';
      }
      preview.change_url(val);
    },
    change : function(){
      $('input.language').change(function(){
        var val = $(this).val();  
        preview.change_url(val);
      });
    },
    change_url : function(val){
      preview.set_url($('#wp-admin-bar-live a'), val);
      preview.set_url($('#wp-admin-bar-preview a'), val);
      $('.box-preview a').each(function(){
        preview.set_url($(this), val);
      });
    },
    set_url : function(target, val){
      if(target[0]){
        var urls = $(target).attr('href').split('/');
        var url = '/ja_'+val +'/' + urls[2] + '/' + urls[3];
        target.attr('href',url);
      }
    },
    call : function(){
      preview.init();
      preview.change();
    },
  };
  preview.call();
})