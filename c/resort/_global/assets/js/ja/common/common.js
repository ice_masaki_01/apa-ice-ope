$ = jQuery;

bind = function(fn, th){ return function(){ return fn.apply(th, arguments); }; };

getQueryParams = function(){
  tokens = location.search.slice(1).split('&');
  params = {};
  for(var i=0; i<tokens.length; i++){
    data = tokens[i].split('=');
    params[data[0]] = data[1];
  }

  return params;
}

var PrefectureCities = {
  '0': ['0000','0001','0002','0003'],
  '7': ['0700','0701','0702'],
  '10': ['1000','1001','1002','1003','1004','1005','1006','1007','1008'],
  '11': ['1100','1101'],
  '17': ['1700','1701','1702'],
  '19': ['1900','1901'],
  '21': ['2100','2101'],
  '24': ['2400','2401','2402','2403','2404','2405'],
  '25': ['2500','2501'],
  '27': ['2700','2701'],
  '39': ['3900','3901']
};

var RegionSelector = function(el){
  this.$el = $(el);
  this.$city = this.$el.find('.city select');
  this.$prefecture = this.$el.find('.prefecture select');
  this.$searchButton = this.$el.find('.js-search-button');

  this.$city.on('change', bind(this.syncPrefectureWithCity, this));
  this.$prefecture.on('change', bind(this.syncCityWithPrefecture, this));

  this.updateLink();
}
RegionSelector.prototype = {
  syncPrefectureWithCity: function(){
    var prefectureId = this.getPrefectureIdByCityId(this.$city.val());

    if(prefectureId) {
      this.$prefecture.val(prefectureId);
    }
    this.updateLink();
  },
  syncCityWithPrefecture: function(){
    var cityId = this.getFirstCityIdByPrefectureId(this.$prefecture.val());
    this.$city.val(cityId);
    this.updateLink();
  },
  updateLink: function(){
    var lang = $('.js-region-selector').data('lang');
    
    var url = "/ja_"+lang+"/hotels/list.php"
    var params = $.param(this.linkParams());
    if(params) {
      url += '?' + params;
    }

    this.$searchButton.attr('href', url);
  },
  linkParams: function(){
    var params = {};

    var cityId = this.$city.val()
    if(cityId !== undefined && cityId !== null && cityId !== ''){
      params.area = '' + cityId;
    }

    var prefectureId = this.$prefecture.val()
    if(prefectureId !== undefined && prefectureId !== null && prefectureId !== ''){
      params.prefecture = '' + prefectureId;
    }

    return params;
  },
  getPrefectureIdByCityId: function(currentCityId) {
    for(var prefectureId in PrefectureCities) {
      var cities = PrefectureCities[prefectureId];
      for(var i=0; i<cities.length; i++){
        var cityId = cities[i];
        if(currentCityId == cityId){
          return prefectureId;
        }
      }
    }

    return null;
  },
  getFirstCityIdByPrefectureId: function(prefectureId) {
    var cities = PrefectureCities['' + prefectureId];
    if(!cities) {
      return null;
    }

    return cities[0];
  }
};

var SearchPlanForm = function(el) {
  var _this = this;
  this.$el = $(el);
  this.$areas = this.$el.find('.js-plan-search-area-code');
  this.$prefectures = this.$el.find('.js-plan-search-prefecture-code');
  this.saveAreas(this.$areas);

  this.$areas.on('change', function(){ _this.selectPrefecture() });
  this.selectPrefecture();

  this.$prefectures.on('change', function(){ _this.filterAreas() });
  this.$prefectures.val('10');
  this.filterAreas(true);
}
SearchPlanForm.prototype = {
  saveAreas: function($areas) {
    var _this = this;
    this.areas = [];
    $areas.find('option').each(function(i, option){
      var $option = $(option);

      var parentPrefectureCode = null;
      for(var prefectureCode in PrefectureCities) {
        var areaCodes = PrefectureCities[prefectureCode];
        for(var i=0; i<areaCodes.length; i++) {
          var areaCode = areaCodes[i];
          if(areaCode == $option.val()){
            parentPrefectureCode = prefectureCode;
            break;
          }
        }
        if(_this.isPresent(parentPrefectureCode)) {
          break;
        }
      }
      _this.areas.push({text: $option.text(), code: $option.val(), prefectureCode: parentPrefectureCode});
    });
  },

  selectPrefecture: function(){
    if(this.$areas.val() == ''){
      return;
    }

    var code = this.$areas.find('option:selected').data('prefecturecode');
    this.$prefectures.val(this.isPresent(code) ? code : '');
  },

  filterAreas: function(keepSelection) {
    var selectedAreaCode = this.$areas.val();
    var selectedPrefectureCode = this.$prefectures.val();
    if(this.isPresent(selectedPrefectureCode) && selectedPrefectureCode.length > 0){
      selectedPrefectureCode = +selectedPrefectureCode;
    }
    this.$areas.empty();

    var existsArea = false
    for(var i=0; i<this.areas.length; i++){
      var area = this.areas[i];
      if(area.prefectureCode == selectedPrefectureCode){
        existsArea = true;
        break;
      }
    }

    for(var i=0; i<this.areas.length; i++){
      var area = this.areas[i];
      if(existsArea && this.isPresent(selectedPrefectureCode) && this.isPresent(area.prefectureCode) && area.prefectureCode != selectedPrefectureCode){
        continue;
      }
      $area = $('<option>').text(area.text).data('prefecturecode', area.prefectureCode).attr('value', area.code);
      if(keepSelection && selectedAreaCode == area.code){
        $area.prop('selected', true)
      }

      this.$areas.append($area)
    }
  },

  isPresent: function(val) {
    return val !== null && val !== undefined;
  }
}

$(function(){
  $('.js-region-selector').each(function(){
    new RegionSelector(this);
  });

  $('.datepicker').datepicker({
    showOn: 'both',
    buttonImage: '/global/assets/images/ja/common/ico_calendar.png',
    minDate: 0
  });

  $('.js-search-plan-form').each(function(){
    new SearchPlanForm(this);
  });

  var login = {
    init : function(){
      var lang = login.get_lang();
      login.set(lang);
    },
    get_lang : function(){
      var lang = $('body').attr('class');
      var lang = lang.split(' ')[0];

      switch (lang){
        case 'ja_en' :
          lang = 'en';
        break;
        case 'ja_zhcn' :
          lang = 'zhcn';
        break;
        case 'ja_zhtw' :
          lang = 'zhtw';
        break;
        case 'ja_ko' :
          lang = 'ko';
        break;
      };
      return lang;
    },
    set : function(lang){
      $.getJSON('/booking/api/user.json?locale='+lang, function(json){
        //json.login
        //json.name
        var target = $('header.global .welcome');
        target.html('');
        if(json.login){
          target.append('<span class="name">Welcome<span>'+json.name+' ('+json.member_no+')</span></span>');
          target.append('<span class="login"><a rel="nofollow" data-method="delete" href="/booking/user_sessions/destroy?locale=' + lang + '">Log out</a></span>');
          target.append('<span class="check"><a href="/booking/mypage">Check Your Reservation Status</a></span>');
        }else{
          target.append('<span class="name">Welcome<span>Guest</span></span><span class="login"><a href="/booking/user_sessions/new?locale=' + lang + '">Login</a></span><span class="check"><a href="/booking/reservation_sessions/new">Check Your Reservation Status</a></span>')}
      }); 
    },
  }
  login.init();

});


