$ = jQuery;
$(function(){

  // Hotel Reservation Form
  var HotelReservationForm = function(id){
    this.id = id;
    this.$el = $('.js-hotel-reservation')
    if(this.$el.length <= 0) { return; }

    this.$continueButton = this.$el.find('.js-continue');
    this.$cidate = this.$el.find('.js-cidate');
    this.$codate = this.$el.find('.js-codate');
    this.$roomnum = this.$el.find('.js-roomnum');
    this.$hotels = this.$el.find('.js-hotels');

    this.$cidate.on('change', bind(this.updateParams, this));
    this.$codate.on('change', bind(this.updateParams, this));
    this.$roomnum.on('change', bind(this.updateParams, this));
    this.$hotels.on('change', bind(this.updateParams, this));

    this.renderHotels();
    this.updateParams();
  }
  HotelReservationForm.urlBase = 'https://www.japan-hotel-reserve.jp/inbound/booking/room'
  HotelReservationForm.defaultParams = {lang: 'en_US'}
  HotelReservationForm.dateFormat = /\d\d\/\d\d\/\d\d\d\d/

  HotelReservationForm.prototype = {
    setParams: function(params){
      this.$continueButton.attr('href', this.buildUrl(params));
    },
    renderHotels: function(){
      currentJhtId = $('body').data('jht_id');

      for(var i=0; i<Hotels.length; i++){
        var hotel = Hotels[i];
        var $option = $('<option>'+ hotel.hotel_name +'</option>').val(hotel.cooperation_jht_id)

        if(currentJhtId > 0 && currentJhtId == +hotel.cooperation_jht_id) {
          $option.prop('selected', true)
        }

        this.$hotels.append($option);
      }
    },
    updateParams: function(){
      params = {hotelid: this.$hotels.val(), roomnum: this.$roomnum.val()};

      cidate = this.$cidate.val();
      if(cidate && cidate.match(HotelReservationForm.dateFormat)){ params.cidate = cidate; }

      codate = this.$codate.val();
      if(codate && codate.match(HotelReservationForm.dateFormat)){ params.codate = codate; }

      this.setParams(params);
    },
    buildUrl: function(params){
      mergedParams = $.extend(HotelReservationForm.defaultParams, params);

      return HotelReservationForm.urlBase + '?' + $.param(mergedParams);
    }
  };

  new HotelReservationForm(getQueryParams().id);
});
