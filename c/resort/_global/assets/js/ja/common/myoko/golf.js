(function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) {
            return installedModules[moduleId].exports;
        }
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: false,
            exports: {}
        };
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
        module.l = true;
        return module.exports;
    }
    __webpack_require__.m = modules;
    __webpack_require__.c = installedModules;
    __webpack_require__.i = function(value) {
        return value;
    };
    __webpack_require__.d = function(exports, name, getter) {
        if (!__webpack_require__.o(exports, name)) {
            Object.defineProperty(exports, name, {
                configurable: false,
                enumerable: true,
                get: getter
            });
        }
    };
    __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module["default"];
        } : function getModuleExports() {
            return module;
        };
        __webpack_require__.d(getter, "a", getter);
        return getter;
    };
    __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    };
    __webpack_require__.p = "";
    return __webpack_require__(__webpack_require__.s = 4);
})([ function(module, exports, __webpack_require__) {
    "use strict";
    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var Settings = {
        apiEndpoint: "/calendar/api/golf",
        eventApiEndpoint: "/calendar/api/event",
        selectors: {
            description: ".js-description",
            competitions: ".js-competitions",
            golfCalendar: ".js-golf-calendar",
            eventCalendar: ".js-event-calendar"
        },
        templates: {
            openCompetition: function openCompetition(data) {
                return '\n    <h3 class="_h3">新潟県妙高市のリゾートホテル「アパリゾート上越妙高」</h3>\n    <p>' + data.title + '</p>\n    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="_table">\n      <tbody>\n      </tbody>\n    </table>\n    ';
            },
            openCompetitionItem: function openCompetitionItem(data) {
                return "\n    <tr>\n      <th>" + data.content1 + "</th>\n      <td>" + data.content2 + "<br>" + data.content3 + "</td>\n    </tr>\n    ";
            }
        },
        youbiLabels: [ "日", "月", "火", "水", "木", "金", "土" ]
    };
    exports.default = Settings;
}, function(module, exports, __webpack_require__) {
    "use strict";
    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var _createClass = function() {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }
        return function(Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();
    var _settings = __webpack_require__(0);
    var _settings2 = _interopRequireDefault(_settings);
    var _golf_calendar = __webpack_require__(5);
    var _golf_calendar2 = _interopRequireDefault(_golf_calendar);
    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }
    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }
    var GolfPage = function() {
        function GolfPage(data) {
            _classCallCheck(this, GolfPage);
            this.data = data;
            this.renderDescription();
            this.renderOpenCompetitions();
            this.renderGolfCalendar();
        }
        _createClass(GolfPage, [ {
            key: "renderDescription",
            value: function renderDescription() {
                $(_settings2.default.selectors.description).html(this.data.description);
            }
        }, {
            key: "renderOpenCompetitions",
            value: function renderOpenCompetitions() {
                var $openCompetitions = $(_settings2.default.selectors.competitions);
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;
                try {
                    for (var _iterator = this.data.open_competitions[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var openCompetition = _step.value;
                        var $openCompetition = $(_settings2.default.templates.openCompetition(openCompetition));
                        var _iteratorNormalCompletion2 = true;
                        var _didIteratorError2 = false;
                        var _iteratorError2 = undefined;
                        try {
                            for (var _iterator2 = openCompetition.items[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                var item = _step2.value;
                                $openCompetition.find("tbody").append(_settings2.default.templates.openCompetitionItem(item));
                            }
                        } catch (err) {
                            _didIteratorError2 = true;
                            _iteratorError2 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                    _iterator2.return();
                                }
                            } finally {
                                if (_didIteratorError2) {
                                    throw _iteratorError2;
                                }
                            }
                        }
                        $openCompetitions.append($openCompetition);
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }
        }, {
            key: "renderGolfCalendar",
            value: function renderGolfCalendar() {
                var $golfCalendar = $(_settings2.default.selectors.golfCalendar);
                new _golf_calendar2.default($golfCalendar, this.data);
            }
        } ]);
        return GolfPage;
    }();
    exports.default = GolfPage;
}, , , function(module, exports, __webpack_require__) {
    "use strict";
    var _settings = __webpack_require__(0);
    var _settings2 = _interopRequireDefault(_settings);
    var _golf_page = __webpack_require__(1);
    var _golf_page2 = _interopRequireDefault(_golf_page);
    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }
    $(function() {
        var date = new Date();
        $.ajax({
            type: "get",
            url: _settings2.default.apiEndpoint,
            data: {
                year: date.getFullYear(),
                month: date.getMonth() + 1
            }
        }).done(function(data) {
            new _golf_page2.default(data);
        });
    });
}, function(module, exports, __webpack_require__) {
    "use strict";
    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var _createClass = function() {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }
        return function(Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();
    var _settings = __webpack_require__(0);
    var _settings2 = _interopRequireDefault(_settings);
    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }
    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }
    var GolfCalendar = function() {
        function GolfCalendar(ele, data) {
            _classCallCheck(this, GolfCalendar);
            this.ele = ele;
            this.year = new Date().getFullYear();
            this.month = new Date().getMonth() + 1;
            this.calendars = data.calendar;
            this.renderCalendar();
        }
        _createClass(GolfCalendar, [ {
            key: "loadData",
            value: function loadData() {
                var _this = this;
                var queryParameters = "?year=" + this.year + "&month=" + this.month;
                $.ajax({
                    type: "GET",
                    url: _settings2.default.apiEndpoint + queryParameters,
                    dataType: "json",
                    async: false
                }).done(function(data) {
                    _this.calendars = data.calendar;
                    _this.renderCalendar();
                });
            }
        }, {
            key: "loadCalendar",
            value: function loadCalendar() {
                this.loadData();
            }
        }, {
            key: "renderCalendar",
            value: function renderCalendar() {
                this.createFrame();
                this.printType();
                this.setEvent();
            }
        }, {
            key: "renderDescription",
            value: function renderDescription() {
                $(_settings2.default.selectors.description).html(this.data.description);
            }
        }, {
            key: "createFrame",
            value: function createFrame() {
                this.ele.html('<div class="calendar-head"><p class="calendar-year-month"></p></div>');
                var outText = "";
                outText += '<p class="pager js-calendar-pager"><span class="prev"></span><span class="now"></span><span class="next"><a href="#"><span class="year"></span><span class="month"></span></a></span></p><table><thead><tr><th>日</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th></thead><tbody></tbody></table>';
                this.ele.find(".calendar-head").after(outText);
            }
        }, {
            key: "printType",
            value: function printType() {
                var _this2 = this;
                $(this.ele).find(".js-calendar-pager .now").text(this.year + "年" + this.month + "月");
                $(this.ele).find(".js-calendar-pager .prev").html('<a href="#" class="js-prev-month"><span class="year">' + this.prevMonth()[0] + '年</span><span class="month">' + this.prevMonth()[1] + "月</span></a>");
                $(this.ele).find(".js-calendar-pager .next").html('<a href="#" class="js-next-month"><span class="year">' + this.nextMonth()[0] + '年</span><span class="month">' + this.nextMonth()[1] + "月</span></a>");
                $(".js-prev-month").on("click", function(e) {
                    e.preventDefault();
                    _this2.year = _this2.prevMonth()[0];
                    _this2.month = _this2.prevMonth()[1];
                    _this2.loadCalendar();
                });
                $(".js-next-month").on("click", function(e) {
                    e.preventDefault();
                    _this2.year = _this2.nextMonth()[0];
                    _this2.month = _this2.nextMonth()[1];
                    _this2.loadCalendar();
                });
                var thisDate = new Date(this.year, this.month - 1, 1);
                var startWeek = thisDate.getDay();
                var lastday = new Date(this.year, this.month, 0).getDate();
                var rowMax = Math.ceil((lastday + startWeek) / 7);
                var outText = "<tr>";
                var countDate = 1;
                for (var i = 0; i < startWeek; i++) {
                    outText += '<td class="calendar-none">&nbsp;</td>';
                }
                for (var row = 0; row < rowMax; row++) {
                    if (row == 0) {
                        for (var col = startWeek; col < 7; col++) {
                            outText += printTD(new Date(this.year, this.month - 1, countDate), col);
                            countDate++;
                        }
                    } else {
                        outText += "<tr>";
                        for (var col = 0; col < 7; col++) {
                            if (lastday >= countDate) {
                                outText += printTD(new Date(this.year, this.month - 1, countDate), col);
                            }
                            countDate++;
                        }
                    }
                    outText += "</tr>";
                }
                $(this.ele).find("tbody").html(outText);
                function printTD(date, col) {
                    return '<td id="calender-id' + date.getDate() + '"><span class="days">' + date.getDate() + '<span class="sp">日(' + _settings2.default.youbiLabels[date.getDay()] + ')</span></span><span class="category"></span></td>';
                }
            }
        }, {
            key: "setEvent",
            value: function setEvent() {
                for (var dateKey in this.calendars) {
                    var dateID = "calender-id" + dateKey;
                    var events = this.calendars[dateKey].events;
                    for (var eventKey in events) {
                        var $title = $("<span>", {
                            class: "txt"
                        }).text(events[eventKey].name);
                        if (events[eventKey].color) {
                            $title.addClass(events[eventKey].color);
                        } else {
                            $title.css("color", "black");
                        }
                        $(this.ele).find("#" + dateID + " .category").append($title);
                        $(this.ele).find("#" + dateID + " .category").append('<span class="lunch"></span>');
                        if (events[eventKey].lunch) {
                            $(this.ele).find("#" + dateID + " .lunch").append('<span class="title">ランチ付</span>');
                        }
                        $(this.ele).find("#" + dateID + " .lunch").append('<span class="price">' + events[eventKey].price + "</span>");
                    }
                    if (this.calendars[dateKey].holiday) {
                        $(this.ele).find("#calender-id" + dateKey).find(".days").addClass("holiday");
                    }
                }
            }
        }, {
            key: "nextMonth",
            value: function nextMonth() {
                if (this.month == 12) {
                    return [ this.year + 1, 1 ];
                } else {
                    return [ this.year, this.month + 1 ];
                }
            }
        }, {
            key: "prevMonth",
            value: function prevMonth() {
                if (this.month == 1) {
                    return [ this.year - 1, 12 ];
                } else {
                    return [ this.year, this.month - 1 ];
                }
            }
        } ]);
        return GolfCalendar;
    }();
    exports.default = GolfCalendar;
} ]);
