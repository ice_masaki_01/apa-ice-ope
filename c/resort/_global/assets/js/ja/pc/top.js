$ = jQuery;
$(function(){
  var main = {
    call : function(){
      main.anime();
    },
    anime : function(){
      $('#section-map h1').delay(1000).animate({'opacity': '1','top':'100'},1000);
      $('#section-map h2').delay(2000).animate({'opacity': '1','top':'185px'},1000);
      $('#section-map .japan').delay(3000).animate({'opacity': '1','top':'250px'},1000);

      $('.area_map').delay(2500).animate({'opacity': '1'},1500);

    }
  };
  main.call();
  
  var popular = {
    'click' : function(){
      $('#section-popular .btn-more').click(function(){
        $(this).hide();
        $('.box-hide').show();
        $('.box-hide ul li').css({'opacity':0});
        var delay = 50;
        $('.box-hide ul li').each(function(){
          $(this).delay(delay).animate({'opacity':1});
          delay = delay + 150;
        });
      });
    },
    call : function(){
      popular.click();
    }
  };
  popular.call();

  // Map of Japan
  var AreaMore = function(id) {
    this.id = id;
    this.$el = $('.area_more.' + id);

    this.$el.on('click', bind(this.onClick, this));
  }
  AreaMore.prototype = {
    show: function(){
      this.$el.show();
    },
    hide: function(){
      this.$el.hide();
    },
    onClick: function(e){
      e.stopPropagation();
    }
  }

  var Area = function(id, options){
    this.id = id;
    this.options = options
    this.$map = $('.js-area.' + id);
    this.$box = $('.js-area-boxes .area_box.' + id);

    this.$map.on('mouseenter', bind(this.onEnter, this));
    this.$map.on('mouseleave', bind(this.onLeave, this));
    this.$box.on('mouseenter', bind(this.onBoxEnter, this));
    this.$box.on('mouseleave', bind(this.onBoxLeave, this));

    this.hideTimer = null
    this.showTimer = null

    if($('.area_more.' + id).length > 0) {
      this.areaMore = new AreaMore(id);

      this.$map.on('click', bind(this.onClick, this));
    }
  };
  Area.timeoutSeconds = 100;

  Area.prototype = {
    onEnter: function(){
      this.clearHideTimer()

      this.showTimer = setTimeout(bind(this.enable, this), Area.timeoutSeconds);
    },
    onLeave: function(){
      this.hideTimer = setTimeout(bind(this.disable, this), Area.timeoutSeconds);
    },
    onClick: function(e){
      e.stopPropagation();

      this.areaMore.show();
    },
    onBoxEnter: function(){
      this.onEnter();

      this.options.onBoxEnter();
    },
    onBoxLeave: function(){
      this.onLeave();
    },
    clearShowTimer: function(){
      if(this.showTimer){
        clearTimeout(this.showTimer);
        this.showTimer = null;
      }
    },
    clearHideTimer: function(){
      if(this.hideTimer){
        clearTimeout(this.hideTimer);
        this.hideTimer = null;
      }
    },
    hideAreaMore: function(){
      if(!this.areaMore) { return; }

      this.areaMore.hide();
    },
    enable: function(){
      this.$box.fadeIn(400);
      this.$map.addClass('is-enabled');
    },
    disable: function(){
      this.$box.hide();
      this.$map.removeClass('is-enabled');
    }
  };

  var Areas = function(){
    Areas.items = [];

    $('.js-area').each(function(){
      var item = new Area($(this).data('area'), {
        onBoxEnter: function(){
          Areas.clearAllShowTimer();
        }
      });

      Areas.items.push(item);
    });
  };
  Areas.clearAllShowTimer = function(){
    for(var i=0; i<this.items.length; i++){
      this.items[i].clearShowTimer();
    }
  };
  Areas.hideEnabledAreaMore = function(){
    for(var i=0; i<this.items.length; i++){
      this.items[i].hideAreaMore();
    }
  };

  new Areas();

  $('#section-map').on('click', function(){
    Areas.hideEnabledAreaMore();
  });
});
