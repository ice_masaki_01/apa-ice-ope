$ = jQuery;
$(function(){
  var side = {
    btn : {
      click : function(){
        $(document).on('click',"#side-menu .btn-open", function(){
          side.btn.open($(this));
        });
        $(document).on('click',"#side-menu .btn-close", function(){
          side.btn.close($(this));
        });
      },
      open : function(btn){
        btn.removeClass('btn-open').addClass('btn-close');;
        btn.parent().animate({'right':'0px'}, 'fast');        
      },
      close: function(btn){
        btn.removeClass('btn-close').addClass('btn-open');;
        btn.parent().animate({'right':'-304px'}, 'fast');        
      },
      call : function(){
        side.btn.click();
      },
    },
    menu : {
      click : function(){
        $("#side-menu .menu-list li b").click(function(){
          $(this).next().slideToggle('fast');
          if ($(this).parent().hasClass('close')){
            $(this).parent().removeClass('close');
            $(this).parent().addClass('open');
            $(this).next().removeClass('close');
            $(this).next().addClass('open');
          } else {
            $(this).parent().removeClass('open');
            $(this).parent().addClass('close');
            $(this).next().removeClass('open');
            $(this).next().addClass('close');
          }
        })
      },call : function(){
        side.menu.click();       
      },
    },
  }
  side.btn.call();
  side.menu.call();
});
