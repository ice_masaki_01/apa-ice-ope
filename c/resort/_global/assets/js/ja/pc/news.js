$ = jQuery;
$(function(){
  var load = {
    page : 1,
    call : function(){
      load.click();
    },
    click : function(){
      $(document).on('click',"#section-news-list .more", function(){
        load.page = load.page+1;
        $(this).parent().next('._hr').remove();
        $(this).parent().remove();
        load.ajax_load(load.page);
      });
    },
    ajax_load : function(){
      $.ajax({
        type: "POST",
        url: "/ja_en/news/-ajax_list.php",
        data: "page="+load.page,
        success: function(data){
          $('#section-news-list .inner .box-articles').append(data);
          var delay = 50;
          $('#section-news-list .inner .box-articles').find('.hide').css({'opacity':0}).each(function(){
            $(this).delay(delay).animate({'opacity':1}).removeClass('hide');
            delay = delay + 150;
          });
        }
      });
    }
  };
  load.call();

  var side = {
    btn : {
      click : function(){
        $(document).on('click',"#side-menu .btn-open", function(){
          side.btn.open($(this));
        });
        $(document).on('click',"#side-menu .btn-close", function(){
          side.btn.close($(this));
        });
      },
      open : function(btn){
        btn.removeClass('btn-open').addClass('btn-close');
        btn.parent().animate({'right':'0px'}, 'fast');
      },
      close: function(btn){
        btn.removeClass('btn-close').addClass('btn-open');
        btn.parent().animate({'right':'-304px'}, 'fast');
      },
      call : function(){
        side.btn.click();
      }
    },
    menu : {
      click : function(){
        $("#side-menu .menu-list li b").click(function(){
          $(this).next().slideToggle('fast');
          if ($(this).parent().hasClass('close')){
            $(this).parent().removeClass('close');
            $(this).parent().addClass('open');
            $(this).next().removeClass('close');
            $(this).next().addClass('open');
          } else {
            $(this).parent().removeClass('open');
            $(this).parent().addClass('close');
            $(this).next().removeClass('open');
            $(this).next().addClass('close');
          }
        });
      },call : function(){
        side.menu.click();
      }
    }
  };
  side.btn.call();
  side.menu.call();
});
