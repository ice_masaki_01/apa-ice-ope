$(function(){
  var main_visual = {
    slick : '',
    slide_count : 0,
    init : function(){
      main_visual.slick = $("#section-mainv .box-visual").slick({
        infinite: true,
        dots:true,
        slidesToShow: 1,
        arrows: false,
        autoplay: true,
        swipe : true,
      autoplaySpeed: 5000,
      })
//      main_visual.carousel_click();
    },
//    carousel_click : function(){
//      $('#section-carousel li').click(function(){
//        var index = $('#section-carousel li').index($(this));
//        main_visual.slick.slick('slickGoTo', parseInt(index));
//        $('#section-carousel li').removeClass('current');
//        $(this).addClass('current');        
//      });
//    },
  }
  main_visual.init();
  
  var movie = {
    animation : function(){
      $('.lightbox').colorbox({
        iframe: true,
        innerWidth: 800,
        innerHeight: 500,
        scrolling: false
      });
    }
  };
  movie.animation();  

  var scroll = {
    click : function(){
      $('a[href^=#]').click(function() {
        var speed = 400;
        var href = $(this).attr("href");
        var target = $(href === "#" || href === "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({
          scrollTop: position
        }, speed, 'swing');
        return false;
      });
    }
  };
  scroll.click();

})
