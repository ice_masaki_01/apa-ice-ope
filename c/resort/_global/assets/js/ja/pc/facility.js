$ = jQuery;
$(function(){
  var branch = {
    mouseon : function(){
      $(".branch-hotel-head h3").mouseenter(function(){
        $(this).next().slideDown('fast');
      });
    },
    mouseout : function(){
      $(".branch-hotel-head").mouseleave(function(){
        $(".branch-hotel-head .outline:not(:animated)").slideUp('fast');
      });
    },
    call : function(){
      branch.mouseon();
      branch.mouseout();
    }
  };
  branch.call();

});
