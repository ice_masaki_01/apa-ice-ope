$ = jQuery;
$(function(){
  var branch = {
    mouseon : function(){
      $(".branch-hotel-head h3").mouseover(function(){
        $(this).next().slideToggle('fast');
        if ($(this).hasClass('close')){
          $(this).removeClass('close');
        } else {
          $(this).addClass('close');
        }
      });
    }
  };
  branch.mouseon();
});
