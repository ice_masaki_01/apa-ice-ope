$ = jQuery;
$(function(){
  var popular = {
    'click' : function(){
      $('#section-popular .btn-more').click(function(){
        $(this).hide();
        $('.box-hide').show();
        $('.box-hide ul li').css({'opacity':0});
        var delay = 50;
        $('.box-hide ul li').each(function(){
          $(this).delay(delay).animate({'opacity':1});
          delay = delay + 150;
        });
      });
    },
    call : function(){
      popular.click();
    }
  };
  popular.call();
});
