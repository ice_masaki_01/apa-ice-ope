$ = jQuery;
$(function(){
  var modal ={
    set_positon : function(){
      var ww = $(window).width()/2;
      var cw = $('.modal-content').width()/2;
      $('.modal-content').css('left',ww-cw);
    },
    resize : function(){
      var timer = false;
      $(window).resize( function(){
        if( $(this).width() > 767 ){
          if (timer !== false) {
            clearTimeout(timer);
          }
          timer = setTimeout( function() {
            modal.set_positon();
          }, 300);
        };
      })
    },
    click_open : function(){
      $('.btn-modal').click(function(){
        var name = $(this).find('a').data('target');
        modal.open(name);
      });
    },
    click_close : function(){
      $('.btn-close').click(function(){
        modal.close();
      });
    },
    open : function(name){
      $('#'+name).fadeIn('fast').css('zIndex',9998);
    },
    close : function(name){
      $('.modal-content').fadeOut('fast').css('zIndex',-1);
    },
    call : function(){
      modal.click_open();
      modal.click_close();
      modal.set_positon();
      modal.resize();

    },
  };
  modal.call();

  var popup = {
    set_positon : function(){
      var ww = $(window).width()/2;
      var cw = $('.content-popup').width()/2;
      $('.content-popup').css('left',ww-cw);
    },
    resize : function(){
      var timer = false;
      $(window).resize( function(){
        if( $(this).width() > 767 ){
          if (timer !== false) {
            clearTimeout(timer);
          }
          timer = setTimeout( function() {
            popup.set_positon();
          }, 300);
        };
      })
    },
    close : function(){
      $('.content-popup').fadeOut();
    },
    open : function(){
      $('.content-popup').fadeIn();
    },
    bk_screen : {
      btn_click : function(){
        $('.btn-make_booking').click(function(){          
          popup.open();
          popup.bk_screen.open();            
        });
      },
      screen_click : function(){
        $('#bk_screen').click(function(){
          popup.close();
          popup.bk_screen.close();          
        });
      },
      open : function(){
        $('#bk_screen').fadeIn('fast').css('zIndex',9998);
      },
      close : function(){
        $('#bk_screen').fadeOut('fast').css('zIndex',-1);        
      }
    },
    call : function(){
      popup.set_positon();
      popup.resize();
      popup.bk_screen.btn_click();
      popup.bk_screen.screen_click();
    },
  };
  popup.call();
  
    var tab = {
    initialize : function() {
      $('.tab').each(function() {
        var self = $(this);
        var current = 0;
        self.find('.boxes .box.active').show();
        tab.change(self);
      });
    },
    change : function(tabObject) {
      tabObject.find('.btns li').click(function() {
        tab.change_image($(this));
        tab.reset_style($(this));
      });
       tabObject.find('select.btns').change(function() {
        var value = $(this).val();
        tab.reset_style($('select.btns option.'+value));
      });
    },
    change_image : function(tabBtnObject) {
      if (tabBtnObject.parent('ul').hasClass('image')) {
        var btns = tabBtnObject.closest('ul').find('img');
        btns.each(function(){
          var self = $(this);
          self.attr('src', self.attr('src').replace(/^(.+)(_on)(\.[a-z]+)$/, "$1_off$3"));
        });
        var btn = tabBtnObject.find('img');
        btn.attr('src', btn.attr('src').replace(/^(.+)_off(\.[a-z]+)$/, "$1_on$2"));
      }
    },
    reset_style : function(tabBtnObject) {
      var tab = tabBtnObject.closest('.tab');
      tab.find('.btns li').removeClass('active');
      tab.find('.btns option').removeClass('active');
      tabBtnObject.addClass('active');
      tab.find('.boxes .box').hide().removeClass('active');
      var target = tabBtnObject.data('tab');
      $('.boxes .box[data-tab_box="'+target+'"]').show().addClass('active').find('.boxes .box').first().show().addClass('active');
      $('.boxes .box[data-tab_box="'+target+'"]').show().addClass('active').find('nav .btns li').first().addClass('active');
      $('.box.active').find('nav .btns li[data-tab="'+target+'"]').first().addClass('active');
      $('.box.active').find('.btns option[data-tab="'+target+'"]').prop('selected', true);
    }
  };
  tab.initialize();
});
