$ = jQuery;
$(function(){
  var gnav = {
    scroll : function(){
      $(window).scroll(function(){
        gnav.set();
      });
    },
    set : function(){
        var postion = $(window).scrollTop();
        if(314 < postion){
          $('.section-mainvisual-head').addClass('fixed');
        }else{
          $('.section-mainvisual-head').removeClass('fixed');
        }
    },
    call : function(){
      gnav.scroll();
      gnav.set();
    }
  };
  gnav.call();

  var branch = {
    mouseon : function(){
      $(".branch-hotel-head h3").mouseenter(function(){
        $(this).next().slideDown('fast');
      });
    },
    mouseout : function(){
      $(".branch-hotel-head").mouseleave(function(){
        $(".branch-hotel-head .outline:not(:animated)").slideUp('fast');
      });
    },
    call : function(){
      branch.mouseon();
      branch.mouseout();
    }
  };
  branch.call();

  var pulldown = {
    click : function(){
      $('.hotel_menu').click(function(){
        $('.list-hotel-menu:not(:animated)').slideToggle('fast');
      });
    }
  };
  pulldown.click();

  var droplink = {
    initialize : function() {
      if($('.droplink')[0]){
        $('.droplink').change(function(){
          var redirect_url_path = $(this).val();
          //location.href = redirect_url_path
          var speed = 500;
          var href= redirect_url_path;
          var target = $(href == "#" || href == "" ? 'html' : href);
          var position = target.offset().top;
          $("html, body").animate({scrollTop:position}, speed, "swing");
          return false;
        })
      }
    }
  };
  droplink.initialize();

  var amenity = {
    click : function(){
      $('#section-amenities .more').click(function(){
        $(this).hide();
        $('.list-amenities li.hide').show();
        $('.list-amenities li.hide').css({'opacity':0});
        var delay = 50;
        $('.list-amenities li.hide').each(function(){
          $(this).delay(delay).animate({'opacity':1});
          delay = delay + 150;
        });
      });        
    },
  }
  amenity.click();

  var gallery = {
    click : function(){
      $('#section-gallery .more').click(function(){
        $(this).hide();
        $('.list-gallery li.hide').show();
        $('.list-gallery li.hide').css({'opacity':0});
        var delay = 50;
        $('.list-gallery li.hide').each(function(){
          $(this).delay(delay).animate({'opacity':1});
          delay = delay + 150;
        });
      });        
    },
  }
  gallery.click();

});
