$ = jQuery;
$(function(){
  var branch = {
    mouseon : function(){
      $(".branch-hotel-head h3").mouseenter(function(){
        $(this).next().slideDown('fast');
      });
    },
    mouseout : function(){
      $(".branch-hotel-head").mouseleave(function(){
        $(".branch-hotel-head .outline:not(:animated)").slideUp('fast');
      });
    },
    load_more : function(){
      $('.btn-load').click(function(){
        $(this).hide();
        $('.list-branch.hide').show();
        $('.list-branch.hide li').show().css({'opacity':0});
        var delay = 50;
        $('.list-branch.hide li').each(function(){
          $(this).delay(delay).animate({'opacity':1});
          delay = delay + 150;
        });
      });
    },
    set_height : function(){
      var height = 0;
      height = $('.branch-hotel-image').height();
      $('.branch-hotel-head .outline').css("height", height-65);      
    },
    call : function(){
//      branch.mouseon();
//      branch.mouseout();
      branch.load_more();
      branch.set_height();
    },
  };
  branch.call();
});

