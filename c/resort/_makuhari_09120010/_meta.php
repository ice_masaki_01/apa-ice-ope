<?php
  require_once( dirname( __FILE__ ) . '/../../global/system/class/User_Agent_Switcher.php' ); 
  $ua = new User_Agent_Switcher();
  $prefectures = $_hotel-> get_prefectures();
?>
<?php if(empty($hotel_id)){?>
<meta name="robots" content="noindex">
<?php } ?>
<link rel="stylesheet" type="text/css" href="/global/assets/css/ja/common/reset.css">
<link rel="stylesheet" type="text/css" href="/global/assets/css/ja/common/jquery-ui.css">
<script type="text/javascript" src="/global/assets/js/plugin/jquery.js"></script>

<?php if($ua->is_pc_and_tablet()){ ?>
  <link rel="stylesheet" type="text/css" href="/global/assets/css/ja/pc/resort_hotels/common.css?id=1">
  <link rel="stylesheet" type="text/css" href="/global/assets/css/ja/pc/resort_hotels/style.css">
  <script type="text/javascript" src="/global/assets/js/ja/pc/resort_hotels/common.js"></script>
<?php }if($ua->is_sphone()){ ?>
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
  <meta name="format-detection" content="telephone=no">
  <link rel="stylesheet" type="text/css" href="/global/assets/css/ja/sp/resort_hotels/common.css?id=1">
  <link rel="stylesheet" type="text/css" href="/global/assets/css/ja/sp/resort_hotels/style.css">
  <script type="text/javascript" src="/global/assets/js/ja/sp/resort_hotels/common.js"></script>
<?php } ?>  
<?php if($ua->is_tablet()){ ?>
  <meta name="viewport" content="width=1260">
  <link rel="stylesheet" type="text/css" href="/global/assets/css/ja/tab/resort_hotels/common/common.css">
<?php } ?>  
<meta name="format-detection" content="telephone=no">