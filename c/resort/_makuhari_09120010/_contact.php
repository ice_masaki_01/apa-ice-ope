<?php
  function get_subject( $subject_number ) {
    switch( $subject_number ) {
      case 1:
        return 'アパポイントについて';
        break;
      case 2:
        return 'ご宿泊・ホテルサービスについて';
        break;
      case 3:
        return 'ご予約・お支払いについて';
        break;
      case 4:
        return 'その他';
        break;
      default:
        return null;
    }
  }

  function is_empty( $params ) {
    foreach($params as $key => $value) {
      if(empty($value)) return $key;
    }
    return false;
  }

  function is_kana( $params ) {
    foreach($params as $key => $value) {
      if (!mb_ereg("^[ァ-ヶー]+$", $value)) {
        return $key;
      }
    }
    return false;
  }

  function is_equal( $email1, $email2 ) {
    return $email1 == $email2;
  }

  function is_email_addr( $email ) {
    if (preg_match("/^([a-zA-Z0-9\+])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email))
      return true;
    return false;
  }

  function send_form_mail( $form_data ) {
    mb_send_mail(
      $form_data['to'],
      'お問い合わせありがとうございました。',
      "
{$form_data['last_name']} {$form_data['first_name']} 様

この度はアパホテルホームページよりご意見・お問い合わせをいただき、
ありがとうございました。

下記の内容で受け付けました。ご確認ください。
尚、このメールは自動返信です。
 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■受付内容
──────────────────────────────────
■件名
{$form_data['subject']}

■名前
{$form_data['last_name']} {$form_data['first_name']}

■フリガナ
{$form_data['last_name_kana']} {$form_data['first_name_kana']}

■会員番号
{$form_data['member_number']}

■メールアドレス 
{$form_data['email']}

■お問い合わせ内容
{$form_data['description']}

──────────────────────────────────
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

内容を確認の上、各担当者より連絡させていただきます。
なお、お問い合わせ内容によっては、お返事に時間がかかる場合がございます。
ご了承ください。

◆━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━◆

　アパホテル株式会社東京本社
　http://www.apahotel.com/
　〒107-0052　東京都港区赤坂3-2-3

◆━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━◆
      ",
      "From: アパホテル株式会社<info@apahotel.com>"
    );
  }


  if($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('Location: https://www.apahotel.com/');
    exit;
  }

  $subject          = (int)$_POST['subject'];
  $first_name       = htmlspecialchars($_POST['first_name']);
  $last_name        = htmlspecialchars($_POST['last_name']);
  $first_name_kana  = htmlspecialchars($_POST['first_name_kana']);
  $last_name_kana   = htmlspecialchars($_POST['last_name_kana']);
  $member_number    = (int)$_POST['member_number'];
  $email            = htmlspecialchars($_POST['email']);
  $email_confirm    = htmlspecialchars($_POST['email_confirm']);
  $description      = htmlspecialchars($_POST['description']);
  $valid            = true;

  $empty_param    = is_empty( array(
    '件名'=>get_subject($subject),
    '氏名(姓)'=>$first_name,
    '氏名(名)'=>$last_name,
    'フリガナ(姓)'=>$first_name_kana,
    'フリガナ(名)'=>$last_name_kana,
    'メールアドレス'=>$email,
    'お問い合わせ内容'=>$description
  ));
  $not_kana_param = is_kana( array(
    'フリガナ(姓)'=>$first_name_kana,
    'フリガナ(名)'=>$last_name_kana
  ));

  $error_messages = array();
  if($empty_param)  $error_messages[] = "$empty_param が入力されていません";
  if($not_kana_param) $error_messages[] = "$not_kana_param はカタカナのみ使用可能です";
  if(!is_equal($email, $email_confirm)) $error_messages[] = "メールアドレスが合致しません";
  if(!is_email_addr($email)) $error_messages[] = "メールアドレスが不正です";

  if(count($error_messages)) $valid = false;
?>
