  <?php $date = new DateTime(); ?>
  <?php if($ua->is_pc_and_tablet()){ ?>
  <section id="search" class="plan clearfix">
    <form method="post" action="https://www.apahotel.com/hotel/shutoken/tokyobay-makuhari/accommodation.html" name="form_reserve" id="form_reserve">
    <div class="container clearfix">
      <h3 class="_h3">空室状況の確認</h3>
      <div class="nights">
        <p class="_p">ご宿泊・泊数</p>
        <script type="text/javascript" src="/global/assets/js/plugin/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/global/assets/js/plugin/datepicker-ja.js"></script>

        <script>
          $( function() {
            $( "#select_date" ).datepicker({dateFormat: "yy/mm/dd", minDate: 0});
            $('#calendar').on('click', function() {
              $('#select_date').datepicker('show', $('#select_date'));
            } );
          } );
        </script>
        <input type="text" name="select_date" id="select_date"><img src="/global/assets/images/ja/resort_hotels/common/ico_calendar.png" id="calendar">
        <label><input type="checkbox" name="date_no_select" id="date_no_select" value="1" />日付未定</label>
        <select name="select_stay" id="select_stay">
          <?php if(!$higaeri){ ?>
          <option value="1">1泊</option>
          <option value="2">2泊</option>
          <option value="3">3泊</option>
          <option value="4">4泊</option>
          <option value="5">5泊</option>
          <option value="6">6泊</option>
          <option value="7">7泊</option>
          <option value="8">8泊</option>
          <option value="9">9泊</option>
          <option value="10">10泊</option>
          <option value="11">11泊</option>
          <option value="12">12泊</option>
          <option value="13">13泊</option>
          <option value="14">14泊</option>
          <?php } ?>
          <option value="0">日帰り</option>
        </select>
      </div>
      <div class="people">
        <p class="_p">人数・部屋数</p>
        <select name="capacity" id="capacity">
          <option value="1">1人</option>
          <option value="2">2人</option>
          <option value="3">3人</option>
          <option value="4">4人</option>
          <option value="5">5人</option>
        </select>
        <select name="select_room" id="select_room">
          <option value="1">1室</option>
          <option value="2">2室</option>
          <option value="3">3室</option>
          <option value="4">4室</option>
          <option value="5">5室</option>
          <option value="6">6室</option>
          <option value="7">7室</option>
          <option value="8">8室</option>
          <option value="9">9室</option>
          <option value="10">10室</option>
        </select>
      </div>
      <div class="smoking">
        <p class="_p">禁煙・喫煙</p>
        <select name="tobaccokey" id="tobaccokey">
          <option value="1">禁煙</option>
          <option value="2">喫煙</option>
        </select>
      </div>
      <div class="price">
        <p class="_p">料金</p>
        <select name="lower_price">
	  <option value="0">下限なし</option>
          <option value="5000">5000</option>
          <option value="8000">8000</option>
	  <option value="10000">10000</option>
          <option value="15000">15000</option>
          <option value="20000">20000</option>
          <option value="30000">30000</option>
          <option value="40000">40000</option>
        </select>
        <span class="_p">円～</span>
        <select name="upper_price">
          <option value="0">上限なし</option>
          <option value="8000">8000</option>
          <option value="10000">10000</option>
          <option value="15000">15000</option>
          <option value="20000">20000</option>
          <option value="30000">30000</option>
          <option value="40000">40000</option>
          <option value="50000">50000</option>
        </select>
        <span class="_p">円</span> </div>
          <?php if(!$higaeri){ ?>
      <div class="plan">
        <p class="_p">プラン</p>
        <select name="category_id">
          <option value="0">全て</option>
          <option value="1" >Web割</option>
          <option value="2" >素泊まり</option>
          <option value="3" >朝食付</option>
          <option value="4" >二食付</option>
          <option value="5" >夕食付</option>
          <option value="6" >VODカード付</option>
          <option value="7" >QUOカード付</option>
        </select>
      </div>
      <?php } ?>
      <p class="btn"><a class="_btn-small" onclick="$('#form_reserve').submit();">空室を検索</a></p>
      <?php if(!$higaeri){ ?>
      <?php $time = date('Y/m/d', strtotime('+1 day'));  ?>
      <h4 class="_h4">日帰りプラン</h4>
      <p class="btn-plan"><a href="https://www.apahotel.com/hotel/shutoken/tokyobay-makuhari/accommodation.html?select_stay=0&select_date=<?php echo $time;?>" class="_btn-small">空室を検索</a></p>
    <?php } ?>
    </div>
    </form>
  </section>
  <?php }if($ua->is_sphone()){ ?>
  <section id="search" class="plan clearfix">
    <form method="post" action="https://www.apahotel.com/hotel/shutoken/tokyobay-makuhari/accommodation.html" name="form_reserve" id="form_reserve">
      <h3 class="_h3">空室状況の確認</h3>
    <div class="container clearfix">
      <div class="nights">
        <p class="_p">ご宿泊・泊数</p>
        <input type="date" name="select_date" id="select_date"><br>
        <label><input type="checkbox" name="date_no_select" id="date_no_select" value="1" />日付未定</label>
        <select name="select_stay" id="select_stay">
          <option value="1">1泊</option>
          <option value="2">2泊</option>
          <option value="3">3泊</option>
          <option value="4">4泊</option>
          <option value="5">5泊</option>
          <option value="6">6泊</option>
          <option value="7">7泊</option>
          <option value="8">8泊</option>
          <option value="9">9泊</option>
          <option value="10">10泊</option>
          <option value="11">11泊</option>
          <option value="12">12泊</option>
          <option value="13">13泊</option>
          <option value="14">14泊</option>
          <option value="0">日帰り</option>
        </select>
      </div>
      <div class="people">
        <p class="_p">人数・部屋数</p>
        <select name="capacity" id="capacity">
          <option value="1">1人</option>
          <option value="2">2人</option>
          <option value="3">3人</option>
          <option value="4">4人</option>
          <option value="5">5人</option>
        </select>
        <select name="select_room" id="select_room">
          <option value="1">1室</option>
          <option value="2">2室</option>
          <option value="3">3室</option>
          <option value="4">4室</option>
          <option value="5">5室</option>
          <option value="6">6室</option>
          <option value="7">7室</option>
          <option value="8">8室</option>
          <option value="9">9室</option>
          <option value="10">10室</option>
        </select>
      </div>
      <div class="smoking">
        <p class="_p">禁煙・喫煙</p>
        <select name="tobaccokey" id="tobaccokey">
          <option value="1">禁煙</option>
          <option value="2">喫煙</option>
        </select>
      </div>
      <div class="price">
        <p class="_p">料金</p>
        <select name="lower_price">
	  <option value="0">下限なし</option>
          <option value="5000">5000</option>
          <option value="8000">8000</option>
	  <option value="10000">10000</option>
          <option value="15000">15000</option>
          <option value="20000">20000</option>
          <option value="30000">30000</option>
          <option value="40000">40000</option>
        </select>
        <span class="_p">円～</span><br />
        <p class="_p"></p>
        <select name="upper_price">
          <option value="0">上限なし</option>
          <option value="8000">8000</option>
          <option value="10000">10000</option>
          <option value="15000">15000</option>
          <option value="20000">20000</option>
          <option value="30000">30000</option>
          <option value="40000">40000</option>
          <option value="50000">50000</option>
        </select>
        <span class="_p">円</span> </div>
      <div class="plan">
        <p class="_p">プラン</p>
        <select name="category_id">
          <option value="0">全て</option>
          <option value="1" >Web割</option>
          <option value="2" >素泊まり</option>
          <option value="3" >朝食付</option>
          <option value="4" >二食付</option>
          <option value="5" >夕食付</option>
          <option value="6" >VODカード付</option>
          <option value="7" >QUOカード付</option>
        </select>
      </div>
      <p class="btn"><a class="_btn-small" onclick="$('#form_reserve').submit();" href="javascript:void(0);">空室を検索</a></p>
      <?php if(!$higaeri){ ?>
      <?php $time = date('Y/m/d', strtotime('+1 day'));  ?>
      <h4 class="_h4">日帰りプラン</h4>
      <p class="btn-plan"><a href="https://www.apahotel.com/hotel/shutoken/tokyobay-makuhari/accommodation.html?select_stay=0&select_date=<?php echo $time;?>" class="_btn-small">空室を検索</a></p>
    <?php } ?>
    </div>
    </form>
  </section>
  <?php } ?>
