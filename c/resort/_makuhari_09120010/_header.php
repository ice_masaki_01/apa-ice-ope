<?php
  $header =  $_hotel->get_header_obj($lang, $hotel_id);
?>
<header class="clearfix">
  <?php if($ua->is_pc_and_tablet()){ ?>
  <div class="container">
    <p class="link"><a href="/"><img src="/global/assets/images/ja/resort_hotels/pc/header-hotel_resorts.png" alt="APA HOTELS & RESORTS" class="not-effect" /></a></p>
    <p class="logo">
      <?php
        if(!empty($header['header_logo'])){
          $imgs = array_slice($header['header_logo'], 0, 1);
          $src = $imgs[0]['src'];                    
        };
        $_hotel->link('/ja_ja/resort_hotels/', '<img src="'.esc_attr($src).'" alt="'.esc_attr($header['name']).'" class="not-effect" />');
      ?>
    </p>
    <nav class="language">
      <label for="language-nav"><span>LANGUAGE</span></label>
      <input type="checkbox" id="language-nav" class="on-off" />
      <ul>
        <?php          
          if(!empty($header['en_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['en_site_hotel_url']).'">ENGLISH</a></li>';
          };
          if(!empty($header['ko_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['ko_site_hotel_url']).'">KOREAN</a></li>';
          };
          if(!empty($header['zhcn_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['zhcn_site_hotel_url']).'">SIMPLIFIED&nbsp;CHINESE</a></li>';
          };
          if(!empty($header['zhtw_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['zhtw_site_hotel_url']).'">TRADITIONAL&nbsp;CHINESE</a></li>';
          };
          if(!empty($header['japan_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['japan_site_hotel_url']).'">JAPANESE</a></li>';
          };
        ?>
      </ul>
    </nav>
    <p class="best-rate"><a href="https://www.apahotel.com/net/index.html" target="_blank"><img src="/global/assets/images/ja/resort_hotels/common/btn-apachoku.png" alt="「アパ直」からなら比較なしで最安値" /></a></p>
    <p class="reserv"><a href="https://www.apahotel.com/hotel/shutoken/tokyobay-makuhari/accommodation.html" target="_blank"><i>RESERVATION</i><span>ご予約</span></a></p>
    <div class="box-sns">
      <ul>
        <li class="image facebook"><a href="https://www.facebook.com/apahotel.makuhari/" target="_blank"><img src="/global/assets/images/ja/resort_hotels/common/facebook.png"></a></li>
        <li class="image instagram"><a href="https://www.instagram.com/makuhari_aparesort/" target="_blank"><img src="/global/assets/images/ja/resort_hotels/common/instagram.png"></a></li>
      </ul>
    </div>
  </div>
  <nav class="menu">
    <div class="container">
      <ul>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/guest_room/', '<i>GUEST&nbsp;ROOM</i><span>客室</span>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/spa/', '<i>SPA</i><span>大浴場</span>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/restaurant/', '<i>RESTAURANTS</i><span>レストラン</span>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/shop/', '<i>SHOPS</i><span>ショップ</span>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/facility/', '<i>FACILITIES</i><span>館内施設</span>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/hall/', '<i>HALLS</i><span>ご宴会・会議・ご婚礼</span>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/access/', '<i>ACCESS</i><span>アクセス</span>'); ?></li>
      </ul>
    </div>
  </nav>
  <?php }if($ua->is_sphone()){ ?>
<div class="header clearfix">
  <div id="mobile-head">
    <p class="logo">
        <?php
          if(!empty($header['header_logo_sp'])){
            $imgs = array_slice($header['header_logo_sp'], 0, 1);
            $src = $imgs[0]['src'];                    
          };
          $_hotel->link('/ja_ja/resort_hotels/', '<img src="'.esc_attr($src).'" alt="'.esc_attr($header['name']).'" class="not-effect" />');
        ?>
      </p>
      <div class="box-sns">
        <ul>
          <li class="image facebook"><a href="https://www.facebook.com/apahotel.makuhari/" target="_blank"><img src="/global/assets/images/ja/resort_hotels/common/facebook.png"></a></li>
          <li class="image instagram"><a href="https://www.instagram.com/makuhari_aparesort/" target="_blank"><img src="/global/assets/images/ja/resort_hotels/common/instagram.png"></a></li>
        </ul>
      </div>
      <div id="nav-toggle">
        <div><span></span><span></span><span></span></div>
      </div>
  </div>
  <nav class="menu clearfix">
    <ul>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/guest_room/', '<span>客室</span><i>GUEST&nbsp;ROOM</i>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/spa/', '<span>大浴場</span><i>SPA</i>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/restaurant/', '<span>レストラン</span><i>RESTAURANTS</i>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/shop/', '<span>ショップ</span><i>SHOPS</i>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/facility/', '<span>館内施設</span><i>FACILITIES</i>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/hall/', '<span>ご宴会・会議・ご婚礼</span><i>HALLS</i>'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/access/', '<span>アクセス</span><i>ACCESS</i>'); ?></li>
    <li><a href="https://www.apahotel.com/net/index.html" target="_blank">「アパ直」からなら<span>比較なしで最安値</span></a></li>
    <li><a href="https://www.apahotel.com/hotel/shutoken/tokyobay-makuhari/accommodation.html"><span>ご予約</span><i>RESERVATION</i></a></li>
        <?php
          if(!empty($header['en_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['en_site_hotel_url']).'"><i>ENGLISH</i></a></li>';
          };
          if(!empty($header['ko_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['ko_site_hotel_url']).'"><i>KOREAN</i></a></li>';
          };
          if(!empty($header['zhcn_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['zhcn_site_hotel_url']).'"><i>SIMPLIFIED&nbsp;CHINESE</i></a></li>';
          };
          if(!empty($header['zhtw_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['zhtw_site_hotel_url']).'"><i>TRADITIONAL&nbsp;CHINESE</i></a></li>';
          };
          if(!empty($header['japan_site_hotel_url'])){
            echo '<li><a href="'.esc_attr($header['japan_site_hotel_url']).'"><i>JAPANESE</i></a></li>';
          };          
        ?>
    </ul>
  </nav>
</div>
  
  <?php } ?>
</header>
