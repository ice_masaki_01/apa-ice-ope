<?php
$event_json = 'events_12.json';
$weekday = array(0 => '日', 1 => '月', 2 => '火', 3 => '水', 4 => '木', 5 => '金', 6 => '土');
$limit = 8;
$index = 0;

  $event_json = file_get_contents($event_json);
  if ($event_json === false) {
    echo "JSON READ FAILED\n";
  }
  $eventobj = json_decode($event_json);

  foreach( $eventobj->event_data as $val ) {
    if($index >= $limit) break;
    $index++;
    $date       = '';
    $start_date = '';
    $end_date   = '';
    $days       = explode(':', $val->days);
    $date       = @date_create(chop($days[0]));
    if($date) {
      $start_date = date_format($date, 'Y年m月d日') . ' (' . $weekday[date_format($date, 'w')] . ')';
      if(sizeof($days) > 1) {
        $date       = @date_create(chop($days[sizeof($days)-1]));
        $start_date   = "$start_date ～ " . date_format($date, 'Y年m月d日') . ' (' . $weekday[date_format($date, 'w')] . ')';
      }
    }
?>
        <li class="clearfix">
          <p class="pic"><a href="https://www.apahotel.com/event_bank/detail/<?php echo $val->id; ?>.html"><img src="http://api.eventbank.jp/image/<?php echo $val->id; ?>.jpg" alt=""></a></p>
          <p class="txt"><span class="date"><?php echo $start_date; ?></span><a href="http://www.apahotel.com/event_bank/detail/<?php echo $val->id; ?>.html"><?php echo $val->name; ?></a><span class="lead"><?php echo mb_strimwidth($val->info_str, 0, 180, '...'); ?></span></p>
        </li>
<?php
  }

?>
