<?php
  $footer =  $_hotel->get_footer_object($lang, $hotel_id);
?>
<footer class="clearfix">
  <?php if($ua->is_pc_and_tablet()){ ?>
  <div class="container">
    <p class="pagetop"><a href="#top"><img src="/global/assets/images/ja/resort_hotels/pc/footer-pagetop.png" alt="PAGETOP" /></a></p>
    <p class="logo">
    <?php
      if(!empty($footer['footer_logo'])){
        $imgs = array_slice($footer['footer_logo'], 0, 1);
        $src = $imgs[0]['src'];                    
      };
      $_hotel->link('/ja_ja/resort_hotels/', '<img src="'.esc_attr($src).'" alt="'.esc_attr($footer['name']).'" class="not-effect" />');
    ?>
    <p class="address"><?php $_hotel->h_html($footer['name']);?>&nbsp;
      <span class="post"><?php $_hotel->h_html($footer['zip_code']);?></span>&nbsp;<?php $_hotel->h_html($prefectures[$footer['prefecture']].$footer['city'].$footer['town'].$footer['street'].$footer['building']); ?><!-- <br><span class="tel">043-296-1111</span> --><span class="mapcode">マップコード&nbsp;6&nbsp;199&nbsp;835*67</span></p>
    <p class="btn-brochure font-m"><?php $_hotel->link($footer['pamphlet_url'],'<span>WEB</span>パンフレット','','class', '_blank');?></p>
    <p class="btn-brochure-en font-m"><?php $_hotel->link($footer['pamphlet_url_en'],'<span>ENGLISH BROCHURE</span>','','class', '_blank');?></p>
    <p class="btn-pet-term font-m"><a href="/global/assets/pdf/ja/makuhari/pet_kiyaku.pdf" target="_blank">ペット同伴宿泊に関する利用規約</a></p>
    <p class="btn-pet-consent font-m"><a href="/global/assets/pdf/ja/makuhari/pet_doi.pdf" target="_blank">ペット同伴宿泊滞在同意書</a></p>
    <p class="btn-inquiry font-m"><?php $_hotel->link('/ja_ja/resort_hotels/contact/','お問い合わせ');?></p>
        
    <nav class="menu">
      <ul>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/gallery/','フォトギャラリー'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/faq/','よくあるご質問'); ?></li>
        <li><a href="http://www.apahotel.com/customer/index.html" target="_blank">アパホテルカスタマーサポート</a></li>
        <li><a href="http://www.apa.co.jp/privacy/privacy03" target="_blank">プライバシーポリシー</a></li>
        <li><a href="http://www.apa.co.jp/outline/outline02.html" target="_blank">会社概要</a></li>
        <li><a href="http://www.apa.co.jp/recruit/index.html" target="_blank">採用情報</a></li>
      </ul>
    </nav>
    <p class="credit">Copyright &copy; HOTEL TOKYO BAY MAKUHARI. All Rights Reserved.</p>
    <p class="link"><a href="/"><img src="/global/assets/images/ja/resort_hotels/sp/footer-hotel_resorts.png" alt="APA HOTELS & RESORTS" class="not-effect" /></a></p>
  </div>
  <?php }if($ua->is_sphone()){ ?>
  <div class="container">
    <p class="pagetop"><a href="#top"><img src="/global/assets/images/ja/resort_hotels/sp/footer-pagetop-sp.png" alt="PAGETOP" /></a></p>
    <p class="logo">
    <?php
      if(!empty($footer['footer_logo'])){
        $imgs = array_slice($footer['footer_logo'], 0, 1);
        $src = $imgs[0]['src'];                    
      };
      $_hotel->link('/ja_ja/resort_hotels/', '<img src="/global/assets/images/ja/resort_hotels/sp/footer-logo-sp.png" alt="'.esc_attr($footer['name']).'" class="not-effect" />');
    ?>
    <p class="address"><?php $_hotel->h_html($footer['name']);?><br>
      <span class="post"><?php $_hotel->h_html($footer['zip_code']);?></span>&nbsp;<?php $_hotel->h_html($prefectures[$footer['prefecture']].$footer['city'].$footer['town'].$footer['street'].$footer['building']); ?><!-- <br><span class="tel">043-296-1111</span> --><span class="mapcode">マップコード&nbsp;6&nbsp;199&nbsp;835*67</span></p>
    <p class="btn-brochure font-m"><?php $_hotel->link($footer['pamphlet_url'],'<span>WEB</span>パンフレット','','class', '_blank');?></p>
    <p class="btn-brochure-en font-m"><?php $_hotel->link($footer['pamphlet_url_en'],'<span>ENGLISH BROCHURE</span>','','class', '_blank');?></p>
    <p class="btn-pet-term font-m"><a href="/global/assets/pdf/ja/makuhari/pet_kiyaku.pdf" target="_blank">ペット同伴宿泊に関する利用規約</a></p>
    <p class="btn-pet-consent font-m"><a href="/global/assets/pdf/ja/makuhari/pet_doi.pdf" target="_blank">ペット同伴宿泊滞在同意書</a></p>
    <p class="btn-inquiry font-m"><?php $_hotel->link('/ja_ja/resort_hotels/contact/','お問い合わせ');?></p>
    <nav class="menu">
      <ul class="clearfix">
        <li><?php $_hotel->link('/ja_ja/resort_hotels/gallery/','フォトギャラリー'); ?></li>
        <li><?php $_hotel->link('/ja_ja/resort_hotels/faq/','よくあるご質問'); ?></li>
        <li><a href="http://www.apahotel.com/customer/index.html" target="_blank">アパホテルカスタマーサポート</a></li>
        <li><a href="http://www.apa.co.jp/privacy/privacy03" target="_blank">プライバシーポリシー</a></li>
        <li><a href="http://www.apa.co.jp/outline/outline02.html" target="_blank">会社概要</a></li>
        <li><a href="http://www.apa.co.jp/recruit/index.html" target="_blank">採用情報</a></li>
      </ul>
    </nav>
    <p class="link"><a href="/"><img src="/global/assets/images/ja/resort_hotels/sp/footer-hotel_resorts.png" alt="APA HOTELS & RESORTS" class="not-effect" /></a></p>
    <p class="credit">Copyright &copy; HOTEL TOKYO BAY MAKUHARI. All Rights Reserved.</p>
  </div>
  <?php } ?>
</footer>

<?php require_once(dirname(__FILE__).'/_ga.php');?>
