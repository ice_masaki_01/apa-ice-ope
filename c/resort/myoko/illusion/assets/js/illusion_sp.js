$(function(){
  var main_visual = {
    slick : '',
    slide_count : 0,
    init : function(){
      main_visual.slick = $("#section-mainv .box-visual").slick({
        infinite: true,
        dots:true,
        slidesToShow: 1,
        arrows: false,
        autoplay: true,
        swipe : true,
        autoplaySpeed: 5000,
      })
//      main_visual.carousel_click();
    },
//   carousel_click : function(){
//      $('#section-carousel li').click(function(){
//      var index = $('#section-carousel li').index($(this));
//      main_visual.slick.slick('slickGoTo', parseInt(index));
//      $('#section-carousel li').removeClass('current');
//      $(this).addClass('current');        
//    });
//  },
  }
  main_visual.init();
  
  var movie = {
    animation : function(){
      $('.lightbox').colorbox({
        iframe: true,
        innerWidth: '90%',
        innerHeight: '90%',
        scrolling: false
      });
    }
  };
  movie.animation();  

  var scroll = {
    click : function(){
      $('a[href^=#]').click(function() {
        var speed = 400;
        var href = $(this).attr("href");
        var target = $(href === "#" || href === "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({
          scrollTop: position
        }, speed, 'swing');
        return false;
      });

      $('area[href^="#"]').click(function(e){
      if(!$(this).hasClass('inline') && !$(this).hasClass('js-event-active')){
        var speed = 400; // ミリ秒
        var btn_id = $(this).closest('p').attr('id');
        if(btn_id === "btn-gotop"){
          var position = 0;
        }else{
          var href= $(this).attr("href");
          var target = $(href === "#" || href === "" ? 'html' : href);
          var position = target.offset().top;
        };
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        (e.preventDefault) ? e.preventDefault():e.returnValue = false;
        return false;
      }
    });  
    }
  };
  scroll.click();
  
  
  
// navigation


  var gnav = {};
  gnav.initialize = function() {
    $("header nav .menu").click(function(){
      $("header nav .pulldown").removeClass('open');
      $(this).next().stop().slideToggle('fast');
      if ($("header nav").hasClass('open')){
        $("header nav").removeClass('open');
      } else {
        $("header nav").addClass('open');
      }
    });
  };
  gnav.initialize();
  
})
