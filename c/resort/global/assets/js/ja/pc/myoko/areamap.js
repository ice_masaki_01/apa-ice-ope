$(function(){
  var hopup = {
    init : function(){
      hopup.hide();
      hopup.open_click();
      hopup.hide_click();
      hopup.image_change();
      hopup.set_window();
      hopup.resize();
    },
    hide : function(){
      $('#bk_screen').fadeOut('fast').css('zindex','-1');
      $('#box-hopup').fadeOut('fast').css('zindex','-1');      
    },
    open_click : function(){
      $('#js-icons li').click(function(){
        $('#box-hopup .inner').hide();
        var id = $(this).data('id');
        $('#bk_screen').fadeIn('fast').css('zindex','9999');
        $('#box-hopup').fadeIn('fast').css('zindex','10000');
        $('#box-hopup').find('.box-'+id).fadeIn('fast');
      });      
    },
    hide_click : function(){
      $('#box-hopup .close').click(function(){hopup.hide();});
      $('#bk_screen').click(function(){hopup.hide();});
    },  
    image_change : function(){
      $('.box-thumbnail img').click(function(){
        $('.box-thumbnail img').removeClass('current');
        var url = $(this).attr('src');
        $(this).closest('.inner').find('.box-main .image img').attr('src',url);
        $(this).addClass('current');
      });
    },
    set_window : function(){
      var wh = $(window).height()/2;
      var ch = $('#box-hopup').height()/2;  
      $('#box-hopup').css('top', wh-ch-0);            
    },
    resize : function(){
      var timer = false;
      $(window).resize(function() {
        if (timer !== false) {
          clearTimeout(timer);
        }
        timer = setTimeout(function() {
          hopup.set_window();          
        }, 200);
      });
    },
  };
  hopup.init();
});