

$(window).on('load resize', function(){
	$('.r-hd-bottom .pd-style select').prettyDropdown();
});
$(window).on('load resize', function(){
    var windowWidth = $(window).width();
    var areaWidth = Math.floor(windowWidth * 0.94);
    if(windowWidth > 800) {
        $(".resort-grid-1").css('width','auto');
        $(".resort-grid-2").css('width','auto');
        $(".resort-grid-3").css('width','auto');
        $(".resort-grid-4").css('width','auto');
        $(".resort-grid-5").css('width','auto');
        $(".resort-grid-6").css('width','auto');

        var grid1Width = $(".resort-grid-1").width();
        var grid2Width = $(".resort-grid-2").width();
        var grid3Width = $(".resort-grid-3").width();
        var grid4Width = $(".resort-grid-4").width();
        var grid5Width = $(".resort-grid-5").width();
        var grid6Width = $(".resort-grid-6").width();
        var row1Width = parseInt(grid1Width + grid2Width + grid3Width);
        var row2Width = parseInt(grid4Width + grid5Width + grid6Width);
        var isRow2 = true;
        if(row1Width >= areaWidth || row2Width >= areaWidth){
            isRow2 = false;
        }

        if(isRow2){
            if(grid1Width < grid4Width){
                $(".resort-grid-1").css('width',grid4Width + 'px');
            }else{
                $(".resort-grid-4").css('width',grid1Width + 'px');
            }
            if(grid2Width < grid5Width){
                $(".resort-grid-2").css('width',grid5Width + 'px');
            }else{
                $(".resort-grid-5").css('width',grid2Width + 'px');
            }
            if(grid3Width < grid6Width){
                $(".resort-grid-3").css('width',grid6Width + 'px');
            }else{
                $(".resort-grid-6").css('width',grid3Width + 'px');
            }
        }else{
            $(".resort-grid-1").css('width','50%');
            $(".resort-grid-2").css('width','50%');
            $(".resort-grid-3").css('width','50%');
            $(".resort-grid-4").css('width','50%');
            $(".resort-grid-5").css('width','50%');
            $(".resort-grid-6").css('width','50%');
        }
    }
});

$(function(){
    $('.rooms-toggle').on('click',function(){
        var target = $(this).attr('toggle-target');
        if(target === 'central-tower'){
            $('.central-tower-rooms').slideToggle();
        }
        else if(target === 'west-wing'){
            $('.west-wing-rooms').slideToggle();
        }else{
            $('.east-wing-rooms').slideToggle();
        }
    });

    $.each($('.r-img-slider'),function(){
        var target = $(this).attr('slider-target');
        var targetClass = '.'+target;
        var targetThumbClass = targetClass+'-thumb';
        $(targetClass).slick({
            asNavFor:targetThumbClass,
            arrows: false,
            slidesToShow:1,
            fade: true
        });
        $(targetThumbClass).slick({
            asNavFor:targetClass,
            focusOnSelect: true,
            arrows: true,
            prevArrow: '<div class="sa-arrow sa-arrow-prev"><i class="apaicon-chevron-left"></i></div>',
            nextArrow: '<div class="sa-arrow sa-arrow-next"><i class="apaicon-chevron-right"></i></div>',
            slidesToShow:6,
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }
            ]
        });
    });
});






