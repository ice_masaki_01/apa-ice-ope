//Footer固定
$(window).bind("load resize", function(){    
	var footerH= $(".footer").outerHeight();
	var windowH= $(window).outerHeight();
	$('.footer').css('position', 'absolute');

	$("body").css({
          "padding-bottom": footerH + "px",
          "min-height": windowH + "px"
      });
}); 

$(window).on('load resize', function(){
	var w = $(window).width();
	var x = 800;
	if (w > x) {
		$('.pd-style select').prettyDropdown();
	} 
	else {
		$('.pd-style select').prettyDropdown();
	} 
});


$(window).on('load',function(){

	$('body').addClass('hs-show');
	
	

	$('.menu-trigger').click(function() {

	    if($('body').hasClass("nav-show")){
	        $('body').removeClass('nav-show');

	    }else{
	        $('body').addClass('nav-show');
	        
	    }
	    return false;
	});

	$('.ic-circle').click(function() {

	    if($('body').hasClass("nav-show2")){
	        $('body').removeClass('nav-show2');

	    }else{
	        $('body').addClass('nav-show2');
	        
	    }
	    return false;
	});

	




	//Modal
	$('.modal-btn').click(function() {
		$(window).trigger('resize');
		var modalID = $(this).attr('data-modal');
		var modalCol =$("[id='" + modalID +"'].modal-col");
		var mdlScrTop = $(document).scrollTop();
	    var mdlScrTopPad = 50;
	    var mdlTop = parseInt(mdlScrTop) + parseInt(mdlScrTopPad);

	    if($('body').hasClass("modal-show")){
	        $('body').removeClass('modal-show');

	    }else{
	        $('body').addClass('modal-show');
	        $(".modal-col").not(modalCol).hide();
	        modalCol.show();
	        $('.modal-wrap').css("top",mdlTop+'px');      
	    }
	    return false;

	});


	$('.page-overlay').click(function() {
	    $('body').removeClass('nav-show');
	    $('body').removeClass('modal-show');
	});
	
	$('.modal-close').click(function() {
	    $('body').removeClass('nav-show');
	    $('body').removeClass('modal-show');
	});
	
	//検索画面List開閉
	$('.list-bottom-link').click(function() {
		var parentLC= $(this).parents('.list-col')
		var thisDetail= parentLC.children('.list-detail')
	    parentLC.addClass('list-show');
	    thisDetail.slideDown();
	    return false;
	});


	$('.li-link-close').click(function() {
		var parentLC= $(this).parents('.list-col')
		var thisDetail= parentLC.children('.list-detail')
	    parentLC.removeClass('list-show');
	    thisDetail.slideUp();
	    return false;
	});

	//List開閉
	$('.li-link').click(function() {
		var parentLC= $(this).parents('.list-col')
		var thisDetail= parentLC.children('.list-detail')
	    parentLC.addClass('list-show');
	    thisDetail.slideDown();
	    return false;
	});


	$('.li-link-close').click(function() {
		var parentLC= $(this).parents('.list-col')
		var thisDetail= parentLC.children('.list-detail')
	    parentLC.removeClass('list-show');
	    thisDetail.slideUp();
	    return false;
	});

	//Tile開閉
	$('.tile-body').hide();
	$('.tile-open .tile-body').show();
	$('.tile-btn').click(function() {
		var tilePar= $(this).parents('.tile');
		var tileBody= tilePar.children('.tile-body');

		if(tilePar.hasClass("tile-show") || tilePar.hasClass("tile-open")){
			tilePar.removeClass('tile-open');
	        tilePar.removeClass('tile-show');
	        tileBody.slideUp();

	    }else{
	        tilePar.addClass('tile-show');
	        tileBody.slideDown();
	        
	    }
	    return false;
	});
	
	//Tile開閉
	$('.list-body').hide();
	$('.list-open .tile-body').show();
	$('.list-btn').click(function() {
		var tilePar= $(this).parents('.list-wrap');
		var tileBody= tilePar.children('.list-body');

		if(tilePar.hasClass("list-show") || tilePar.hasClass("list-open")){
			tilePar.removeClass('list-open');
	        tilePar.removeClass('list-show');
	        tileBody.slideUp();

	    }else{
	        tilePar.addClass('list-show');
	        tileBody.slideDown();
	        
	    }
	    return false;
	});
	
	//Form detail開閉
	$('#wd-form-expand').click(function(){
		$(this).toggleClass('toggle-open').parents().children('.wd-item-bottom').slideToggle();
		 return false;
	});
	
	$('#hs-form-expand2').click(function(){
		$(this).toggleClass('toggle-open').parents().children('.hs-form-bottom').slideToggle();
		 return false;
	});





	//Masonry equalizing heights
	if($(window).width() > 601) {
		var equalizeHeights = function(selector) {
		  var maxHeight = 0;
		  $(selector).each(function() {
		    var height = $(this).height();
		    if (height > maxHeight) { maxHeight = height; }
		  });  
		  $(selector).height(maxHeight);
		};

		equalizeHeights('.masonry-text');
	 }

	//Form date
	//var now = new Date();
	//var day = ("0" + now.getDate()).slice(-2);
	//var month = ("0" + (now.getMonth() + 1)).slice(-2);
	//var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
	//$('.bc-date-picker').val(today).attr('min', today);

	//page-top button
	var topBtn = $('.page-top');    
    topBtn.hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });

    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });


    //Carousel
	$('.carousel-wrap').slick({
	focusOnSelect: true,
	arrows: true,
	prevArrow: '<div class="sa-arrow sa-arrow-circle sa-arrow-prev"><i class="apaicon-chevron-left"></i></div>',
	nextArrow: '<div class="sa-arrow sa-arrow-circle sa-arrow-next"><i class="apaicon-chevron-right"></i></div>',
	slidesToShow:3,
	responsive: [
	    {
	      breakpoint: 1000,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});


	//Default slider
	$('.img-slider').slick({
	   //asNavFor:'.img-slider-thumb',
	   arrows: true,
	   prevArrow: '<div class="sa-arrow sa-arrow-prev"><i class="apaicon-chevron-left"></i></div>',
	   nextArrow: '<div class="sa-arrow sa-arrow-next"><i class="apaicon-chevron-right"></i></div>',
	   slidesToShow:1,
	   fade: true,
	 });
	 $('.img-slider-thumb').slick({
	   asNavFor:'.img-slider',
	   focusOnSelect: true,
	   arrows: false,
	   slidesToShow:4,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    }
		  ]
		});

	 //Sticky box
	if($(window).width() < 801) {

	    var docHeight = $(document).height();		
		$(window).on('load scroll',function(){
		    var footerHeight= $(".footer").outerHeight();
			var stickyHeight= $(".sticky-box").outerHeight();
			var fromBottom= footerHeight+stickyHeight;
			var pos=docHeight-fromBottom;
			var scrTopD = $(document).scrollTop() + $(window).height()-100;
		    if(pos <= scrTopD){
		        $('.sticky-box').addClass('scrld');

		    }else if(pos > scrTopD){
		        $('.sticky-box').removeClass('scrld');
		    }

		    
		    
		});
	}
	
	 //search-bar
	if($(window).width() < 801) {
		// var menuHeight = $("#search-menu-wrap").height();
		// var startPos = 0;
		// $("#search-menu-wrap").css("top", "-" + 115 + "px");
		// $(window).scroll(function(){
  // 			var currentPos = $(this).scrollTop();
  // 			if (currentPos > startPos) {
  //   			if($(window).scrollTop() >= -0) {
  //     				$("#search-menu-wrap").css("top", "-" + 115 + "px");
		// 		}
		// 		else{
		// 			$("#search-menu-wrap").css("top", "-" + 0 + "px");
		// 		}
  // 			} else {
  //   			$("#search-menu-wrap").css("top", 0 + "px");
  // 			}
  // 			startPos = currentPos;
		// });
		$("#search-menu-wrap").css("top", "-" + 115 + "px");
		$(document).scroll(function() {
		  var y = $(this).scrollTop();
		  if (y > 1) {
		    $("#search-menu-wrap").css("top", "-" + 115 + "px");
		    $("#search-menu-wrap").fadeOut();
		  }
		  else{
		    $("#search-menu-wrap").css("top", "-" + 0 + "px");
		    $("#search-menu-wrap").fadeIn();
		  }
		});
	}

	function resizeLogic() {
	    if ($(window).width() < 800) {
			$('.smocking-box').removeClass('hs-sml');
			$('.smocking-box').addClass('hs-med');
	    } 
	    else {
	        $('.smocking-box').removeClass('hs-med');
			$('.smocking-box').addClass('hs-sml');
	    };
	}
	resizeLogic(); // on load
	$(window).resize(resizeLogic); // on window resize
});


// for resort-pages
$(window).on('load resize', function(){
    $('.r-hd-bottom .pd-style select').prettyDropdown();
});

$(window).on('load resize', function(){
	var windowWidth = $(window).width();
	var areaWidth = Math.floor(windowWidth * 0.94);
    if(windowWidth > 800) {
        $(".resort-grid-1").css('width','auto');
        $(".resort-grid-2").css('width','auto');
        $(".resort-grid-3").css('width','auto');
        $(".resort-grid-4").css('width','auto');
        $(".resort-grid-5").css('width','auto');
        $(".resort-grid-6").css('width','auto');

		var grid1Width = $(".resort-grid-1").width();
        var grid2Width = $(".resort-grid-2").width();
        var grid3Width = $(".resort-grid-3").width();
        var grid4Width = $(".resort-grid-4").width();
        var grid5Width = $(".resort-grid-5").width();
        var grid6Width = $(".resort-grid-6").width();
        var row1Width = parseInt(grid1Width + grid2Width + grid3Width);
        var row2Width = parseInt(grid4Width + grid5Width + grid6Width);
        var isRow2 = true;
        if(row1Width >= areaWidth || row2Width >= areaWidth){
            isRow2 = false;
		}

        if(isRow2){
        	if(grid1Width < grid4Width){
                $(".resort-grid-1").css('width',grid4Width + 'px');
			}else{
                $(".resort-grid-4").css('width',grid1Width + 'px');
			}
            if(grid2Width < grid5Width){
                $(".resort-grid-2").css('width',grid5Width + 'px');
            }else{
                $(".resort-grid-5").css('width',grid2Width + 'px');
            }
            if(grid3Width < grid6Width){
                $(".resort-grid-3").css('width',grid6Width + 'px');
            }else{
                $(".resort-grid-6").css('width',grid3Width + 'px');
            }
		}else{
            $(".resort-grid-1").css('width','50%');
            $(".resort-grid-2").css('width','50%');
            $(".resort-grid-3").css('width','50%');
            $(".resort-grid-4").css('width','50%');
            $(".resort-grid-5").css('width','50%');
            $(".resort-grid-6").css('width','50%');
		}
    }
});

$(function(){
    // スライダーを動作させる
	function resetSlider(setPosition){
        $.each($('.r-img-slider'),function(){
            var target = $(this).attr('slider-target');
            var hasThumbnail = $(this).attr('has-thumbnail');
            var targetClass = '.'+target;
            var targetThumbClass = targetClass+'-thumb';
            if(hasThumbnail === 'true'){
                if(setPosition){
                    // 初期表示がdisplay:noneの場合はsetPositionを実行しないと表示されない
                    $(targetClass).slick('setPosition');
                    $(targetThumbClass).slick('setPosition');
                }else{
                    $(targetClass).not('.slick-initialized').slick({
                        asNavFor:targetThumbClass,
                        arrows: false,
                        slidesToShow:1,
                        fade: true
                    });
                    $(targetThumbClass).not('.slick-initialized').slick({
                        asNavFor:targetClass,
                        focusOnSelect: true,
                        arrows: true,
                        prevArrow: '<div class="sa-arrow sa-arrow-prev"><i class="apaicon-chevron-left"></i></div>',
                        nextArrow: '<div class="sa-arrow sa-arrow-next"><i class="apaicon-chevron-right"></i></div>',
                        slidesToShow:6,
                        responsive: [
                            {
                                breakpoint: 800,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3
                                }
                            }
                        ]
                    });
                }
            } else {
                if(setPosition){
                    $(targetClass).slick('setPosition');
                }else{
                    $(targetClass).not('.slick-initialized').slick({
                        arrows: true,
                        prevArrow: '<div class="sa-arrow sa-arrow-prev"><i class="apaicon-chevron-left"></i></div>',
                        nextArrow: '<div class="sa-arrow sa-arrow-next"><i class="apaicon-chevron-right"></i></div>',
                        fade: true
                    });
                }
            }


        });
    }

    resetSlider(false);
    $(window).on('resize', function(){
        resetSlider(true);
    });


	// 客室ページの建物選択
    $('.rooms-toggle').on('click',function(){
        var target = $(this).attr('toggle-target');
        if(target === 'central-tower'){
            $('.central-tower-rooms').toggle();
        }
        else if(target === 'west-wing'){
            $('.west-wing-rooms').toggle();
        }else{
            $('.east-wing-rooms').toggle();
        }
        resetSlider(true);
    });

    $('.r-pickup-title').on('click',function(){
    	var targetClass = '.'+$(this).attr('toggle-target');
    	$(targetClass).slideToggle();
	});

    // スムーズスクロール
    $('.r-smooth-scroll').on('click',function() {
        var speed = 400; // ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });

    // ゴルフ チャレンジホール達成者一覧の開閉
    $('.r-golf-challenge .players-toggle').on('click',function(){
        var targetArea = $(this).closest('.r-obtain-players-wrapper').find('.r-obtain-players-list');
        targetArea.slideToggle();
        return false;
    });

    // 妙高：宴会・研修ページの選択
    $('.hall-toggle').on('click',function(){
        var target = $(this).attr('toggle-target');
        var isClose = false;
        if($('.'+target).outerHeight() === 0){
            isClose = true;
		}

        $('.tab-content').css('height','0');
        $('.tabs li').removeClass('active');
        // 現在閉じていれば開く
        if(isClose){
            $('.'+target).css('height','auto');
            $('.'+target+'-tab').addClass('active');
		}

        resetSlider(true);
    });

});




// -----------------------------------------------------
// 					APA Tabs Elements	
// -----------------------------------------------------
$(document).ready(function($) {
  $('.tab_content').hide();
  $('.tab_content:first').show();
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab_content').hide();
    var selectTab = $(this).find('a').attr("data-name");
    $(selectTab).show();
  });
});
// -----------------------------------------------------
// 		Resort APA Tabs Elements	
// -----------------------------------------------------
$(document).ready(function($) {
  $('.tab-content').css({"display":"block","height":0,"overflow":"hidden"});
  $('.tab-content:first').css({"display":"block","height":"auto",});
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab-content').css({"display":"block","height":0,"overflow":"hidden"});
    var selectTab = $(this).find('a').attr("data-name");
    $(selectTab).css({"display":"block","height":"auto",});
  });
});


