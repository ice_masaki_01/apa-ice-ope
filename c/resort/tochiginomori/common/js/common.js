/* ------------------------------------------------------------------

 common.js
	
------------------------------------------------------------------ */

//
//popup
//

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

$(function(){
	$('.link_area a').hover(function(){
		$(this).stop().animate({'opacity' : '0.5'}, 300);
	}, function(){
		$(this).stop().animate({'opacity' : '1'}, 300);
	});
});


//
//SmoothScroll
//

$(function(){
// #で始まるアンカーをクリックした場合に処理
$('a[href^=#]').click(function() {
	// スクロールの速度
	var speed = 1000;// ミリ秒
	// アンカーの値取得
	var href= $(this).attr("href");
	// 移動先を取得
	var target = $(href == "#" || href == "" ? 'html' : href);
	// 移動先を数値で取得
	var position = target.offset().top;
	// スムーススクロール
	$($.browser.safari ? 'body' : 'html').animate({scrollTop:position}, speed, 'easeOutExpo');
	return false;
});

// analytics 外部サイトへの引渡しaタグ
var _gaConf = {
	internalDomain : 
		//計測対象のドメインをココに記載
		[
		"cent.apahotel.co.jp",
		"www.jgo-os.com",
		"apa.co.jp"

		]
}

//target="_blank"だったらwindow.open()。そうでなかったら'_link'を利用。
var makeGaLink = function(atag){
	var targetAttri = $(atag).attr("target");
	if(targetAttri === "_blank"){
		window.open(_gat._getTrackers()[0]._getLinkerUrl($(atag).attr("href")));
	}else{
		_gaq.push(['_link', $(atag).attr("href")]);
	}
	return false;
}


//aタグにバインド
$("a").click(function(evt){
	var targetUrl, i;
	targetUrl = $(this).attr("href");
	for(i=0; i<_gaConf.internalDomain.length; i++){
		if(targetUrl.indexOf("mailto:") !== 0){
			if(targetUrl.indexOf(_gaConf.internalDomain[i]) !== -1 && targetUrl.indexOf(location.host) === -1){
				//パラメータを付けてリンク先へ移動
				makeGaLink($(this));
				//デフォルトの動作をストップ
				evt.preventDefault();
			}
		}
	}
});

// フォームタグ
$('form').bind('submit', function() {
	_gaq.push(['_linkByPost', this]);
});
});


/*
$(document).ready(function(){
$('p.pagetop a[href*=#]').click(function() {
if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
&& location.hostname == this.hostname) {
var $target = $(this.hash);
$target = $target.length && $target
|| $('[name=' + this.hash.slice(1) +']');
if ($target.length) {
var targetOffset = $target.offset().top;
$('html,body')
.animate({scrollTop: targetOffset}, 1000, 'easeOutExpo');
return false;
}
}
});
});
*/

//
//RollOver
//
;(function($){
$(document).ready(function(){
$("a img[src*='_ov']").addClass("current");
$("a img[class!='current'],:image").mouseover(function(){
if ($(this).attr("src").match(/_ot./)){
$(this).attr("src",$(this).attr("src").replace("_ot.", "_ov."));
return;
}
}).mouseout(function(){
if ($(this).attr("src").match(/_ov./)){
$(this).attr("src",$(this).attr("src").replace("_ov.", "_ot."));
return;
}
}).click(function(){
if ($(this).attr("src").match(/_ov./)){
$(this).attr("src",$(this).attr("src").replace("_ov.", "_ot."));
return;
}
});


//preload images
var images = [];
$("a img,:image").each(function(index){
if($(this).attr("src").match(/_ot./)){
images[index]= new Image();
images[index].src = $(this).attr("src").replace("_ov.", "_ot.");
}
});
});
})(jQuery);
