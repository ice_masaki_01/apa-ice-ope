$(document).ready(function() {

   $('a[href^="http://"]').attr("target", "_blank");  

   $("a[rel='external']").click(function(){
      this.target = "_blank";
   });
});

$(document).ready(function() {
    $('.boxlink').click(function() {
        //a要素からhref属性を探して中身を取得し、そのURLに飛ばす。別窓なら('href'), '_blank'とすればOK
        window.location = $(this).find('a').attr('href');
        return false;
    });
});

function initRollOverImages() {
  var image_cache = new Object();
  $("img.swap").each(function(i) {
    var imgsrc = this.src;
    var dot = this.src.lastIndexOf('.');
    var imgsrc_on = this.src.substr(0, dot) + '_on' + this.src.substr(dot, 4);
    image_cache[this.src] = new Image();
    image_cache[this.src].src = imgsrc_on;
    $(this).hover(
      function() { this.src = imgsrc_on; },
      function() { this.src = imgsrc; });
  });
}

$(document).ready(initRollOverImages);
$(function(){
  $('header a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});

$(document).ready(initRollOverImages);
$(function(){
  $('.boxlink a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});


$(function(){
$(document).snowfall({
flakeColor:"#ffffff", //色を指定
        round:true,   //丸くする、丸が嫌ならfalseを入れる
        shadow:false,  //影をつける、影を付けたければtrueを入れる
        minSize:1,     //好みで最小の大きさを指定
        maxSize:3,    //好みで最大の大きさを指定
        flakeCount:180, //表示数
        minspeed:2,    //最小落下速度
        maxSpeed:5});  //最大落下速度
  });



$(function () {
    $('.content_text').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeInDown');
        } else {
            $(this).removeClass('fadeInDown');
            $(this).css('opacity', 0);
        }
    });
    $('.content_title_read').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeInDown');
        } else {
            $(this).removeClass('fadeInDown');
            $(this).css('opacity', 0);
        }
    });
    $('.content_title').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeInDown');
        } else {
            $(this).removeClass('fadeInDown');
            $(this).css('opacity', 0);
        }
    });
    $('.content_a_floatbox_inner_a').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn');
        } else {
            $(this).removeClass('fadeIn');
            $(this).css('opacity', 0);
        }
    });
    $('.content_a_floatbox_inner_b').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn2');
        } else {
            $(this).removeClass('fadeIn2');
            $(this).css('opacity', 0);
        }
    });
    $('.content_a_floatbox_inner_c').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn3');
        } else {
            $(this).removeClass('fadeIn3');
            $(this).css('opacity', 0);
        }
    });
    $('.content_basebox').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn');
        } else {
        }
    });
    $('.content_e_inner_a').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn');
        } else {
            $(this).removeClass('fadeIn');
            $(this).css('opacity', 0);
        }
    });
    $('.content_e_inner_b').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn2');
        } else {
            $(this).removeClass('fadeIn2');
            $(this).css('opacity', 0);
        }
    });
    $('.content_e_inner_c').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn3');
        } else {
            $(this).removeClass('fadeIn3');
            $(this).css('opacity', 0);
        }
    });
    $('.content_e_inner_d').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('fadeIn4');
        } else {
            $(this).removeClass('fadeIn4');
            $(this).css('opacity', 0);
        }
    });

    $('.main_text_a').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_b').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_c').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_d').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_e').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_f').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_g').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_h').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_i').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_j').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_k').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });
    $('.main_text_l').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).addClass('rotateIn');
        } else {
            $(this).removeClass('rotateIn');
            $(this).css('opacity', 0);
        }
    });


});


$(function(){
 $('.main_text_01 img').show().animate({'top': '0','left': '0','width': '200px','height': '200px'}, 500,function(){
  $(this).animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500).dequeue();
 });
});$(function(){
    setTimeout(function(){
 $('.main_text_02 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },500);
});
$(function(){
    setTimeout(function(){
 $('.main_text_03 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },700);
});
$(function(){
    setTimeout(function(){
 $('.main_text_04 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },900);
});
$(function(){
    setTimeout(function(){
 $('.main_text_05 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },1100);
});
$(function(){
    setTimeout(function(){
 $('.main_text_06 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },1300);
});
$(function(){
    setTimeout(function(){
 $('.main_text_07 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },1500);
});
$(function(){
    setTimeout(function(){
 $('.main_text_08 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },1700);
});
$(function(){
    setTimeout(function(){
 $('.main_text_09 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },1900);
});
$(function(){
    setTimeout(function(){
 $('.main_text_10 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },2100);
});
$(function(){
    setTimeout(function(){
 $('.main_text_11 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },2300);
});
$(function(){
    setTimeout(function(){
 $('.main_text_12 img').show().animate({'top': '0','left': '0','width': '75px','height': '75px'}, 500);
    },2500);
});

$(function(){
    setTimeout(function(){
 $('.main_text_13 img').show().animate({'top': '0','left': '0','width': '668px','height': '46px'}, 500);
    },2700);
});
$(function(){
 $('.main_text_14 img').show().animate({'top': '0','left': '0','width': '668px','height': '94px'}, 800);
});
$(function(){
    setTimeout(function(){
 $('.main_text_15 img').show().animate({'top': '0','left': '0','width': '668px','height': '46px'}, 500);
    },500);
});




$(function(){
 $('.main_text_n img').show().animate({'top': '0','left': '0','width': '668px','height': '94px'}, 1000);
});
