
$(function(){
		var _width = $(window).width();//デバイス（ウィンドウ）幅を取得
		if(_width <= 640){
					$('.middle_A').removeAttr('style');
					$('.middle_B').niceMiddle({type: "padding"});
					$('.middle_C').niceMiddle({type: "padding"});
	}else{
					$('.middle_A').niceMiddle({type: "padding"});
					$('.middle_B').removeAttr('style');
					$('.middle_C').niceMiddle({type: "padding"});
	}
});


/*-------------------------------------------------------------------------------
 ヘッダー スムーズスクロール
-------------------------------------------------------------------------------*/
$(function() {
	var w = $(window).width();
	var x = 750;
	if (w <= x) {
   $('a[href^=#]').click(function() {
      var speed = 1000; // ミリ秒
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top - 54;
						$('html,body').animate({scrollTop:position}, speed, 'easeOutExpo');
      return false;
   });
			} else {
   $('a[href^=#]').click(function() {
      var speed = 1000; // ミリ秒
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top - 75;
						$('html,body').animate({scrollTop:position}, speed, 'easeOutExpo');
      return false;
   });
	}
});

$(function() {
  var $win = $(window),
      $header = $('header .inner'),
      animationClass = 'is_animation';

  $win.on('load scroll', function() {
    var value = $(this).scrollTop();
    if ( value > 100 ) {
      $header.addClass(animationClass);
    } else {
      $header.removeClass(animationClass);
    }
  });
});


/*-------------------------------------------------------------------------------
 div全体をリンクに
-------------------------------------------------------------------------------*/

$(function(){
	//通常リンク
		$(".link_area").on('click',function () {
						window.location=$(this).find("a").attr("href");
						return false;
	});
	//リンクがblankの時
	$(".link_blank").on('click',function () {
						window.open($(this).find("a").attr("href"), '_blank');
						return false;
	});
});

/*-------------------------------------------------------------------------------
 高さ揃え（autoHeight）
-------------------------------------------------------------------------------*/

$(function() {
    $('.ranking_area h2').autoHeight();
    $('.ranking_area .lead').autoHeight();
});

$(function() {
	var w = $(window).width();
	var x = 750;
	if (w <= x) {
    var wH = $(window).height(); 
    	$('.other_box').css('height',wH+'px'); 
	}
});

/*-------------------------------------------------------------------------------
 スライド（slick）
-------------------------------------------------------------------------------*/

$(function(){
	$('.mv_area ul').slick({
		// アクセシビリティ。左右ボタンで画像の切り替えをできるかどうか
		accessibility: true,
		// 自動再生。trueで自動再生される。
		autoplay: true,
		// 自動再生で切り替えをする時間
		autoplaySpeed: 6000,
		//スライダーの高さをカレントスライドに合わせるかどうか（初期値：false）
		adaptiveHeight: true,
		// 自動再生や左右の矢印でスライドするスピード
		speed: 10,
		// 自動再生時にスライドのエリアにマウスオンで一時停止するかどうか
		pauseOnHover: false,
		// 自動再生時にドットにマウスオンで一時停止するかどうか
		pauseOnDotsHover: false,
		// 切り替えのアニメーション。ease,linear,ease-in,ease-out,ease-in-out
		cssEase: 'linear',
		// 画像下のドット（ページ送り）を表示
		dots: false,
		// ドットのclass名をつける
		dotsClass: 'dot_class',
		// ドラッグができるかどうか
		draggable: true,
		// 切り替え時のフェードイン設定。trueでon
		fade: true,
		// 左右の次へ、前へボタンを表示するかどうか
		arrows: false,
		// 無限スクロールにするかどうか。最後の画像の次は最初の画像が表示される。
		infinite: true,
		// 最初のスライダーの位置
		initialSlide: 0,
		// 画像の遅延表示。'ondemand'or'progressive'
		lazyLoad: 'ondemand',
		// スライドのエリアにマウスオーバーしている間、自動再生を止めるかどうか。
		pauseOnHover: false,
		// スライドのエリアに画像がいくつ表示されるかを指定
		slidesToShow: 1,
		// 一度にスライドする数
		slidesToScroll: 1,
		// タッチスワイプに対応するかどうか
		swipe: true,
		// 縦方向へのスライド
		vertical: false,
		// 表示中の画像を中央へ
		centerMode: true,
		// 中央のpadding
		centerPadding: '0',
		//（初期値：<button type="button" class="slick-prev">Previous</button>）
		prevArrow: '<button type="button" class="slick-prev">←</button>',
		//次への矢印ボタンはここでソースを変更してね
		//（初期値：<button type="button" class="slick-next">Next</button>）
		nextArrow: '<button type="button" class="slick-next">→</button>',
	});
});

$(window).load(function() {
	$('.mv_area ul').addClass('st');
});


/*-------------------------------------------------------------------------------
 スクロール　アニメ コンテンツ
-------------------------------------------------------------------------------*/

$(function () {
	$('#anc01').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('#anc02').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
	$('#anc03').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('active');
		}
	});
});

/*-------------------------------------------------------------------------------
 不要なCSS削除
-------------------------------------------------------------------------------*/

$(function(){
	$('#hotel_list_box').attr('style','');
	$('.hd_area').remove();
	$('#hd_bnr').remove();
	$('.welcome_area').remove();
});

/* ------------------------------------------------------------------
アコーディオン
------------------------------------------------------------------ */
$(function() {
	var w = $(window).width();
	var x = 750;
	if (w <= x) {
		$("header .open").each(function(){
		$("header .other_box").hide();
		$("header .open").click(function(){
			$(this).toggleClass("active")
			$("header .other_box").slideToggle(0);
		});
		$("header .other_box ul li a").click(function(){
			$(this).parent().parent().parent().parent().prev().removeClass("active");
			$("header .other_box").slideToggle(0);
		});
		return false;
		});
	}
});


$(function(){
$("#kodawari").each(function(){
$("#kodawari .kodawari_area").hide();
$("#kodawari .btn01").click(function(){
	$(this).toggleClass("active")
	$("#kodawari .kodawari_area").slideToggle(500);
});
return false;
});
});
					
					$(function(){
					var _width = $(window).width();//デバイス（ウィンドウ）幅を取得
					$(".side_hotel_search").each(function(){
					if(_width <= 640){
						$(".side_hotel_search li ul").hide();
					}
					$(".side_hotel_search li ul").each(function(){
					if(_width > 640){
						var text = $(this).prev("a").text();
						$(this).prev("a").replaceWith('<span class="active">' + text + '</span>');
					}
					});
					$(".side_hotel_search li a").click(function(){
					$(this).toggleClass("active").next().slideToggle(0);
					});
					return false;
					});
					});




/* ------------------------------------------------------------------
	PC フッターアコーディオン
------------------------------------------------------------------ */

$(function(){
$(".network_area ").each(function(){
$(".network_area .over").click(function(){
$(this).toggleClass("active");
$(this).parents().find("#hotel_list_box").slideToggle(500);
});
return false;
});
});


/* ------------------------------------------------------------------
 カレンダー
------------------------------------------------------------------ */

jQuery( function() {
	var date = new Date();
	var hour = date.getHours();
	jQuery( '.calendar' ).datepicker( {
		showOn: "both",
		buttonImage: "/common/img/ico_calendar.png",
		buttonImageOnly: true,
		changeMonth: true,
		minDate: hour < 5 ? '-1d' : '0d',
		maxDate: '+11m',
	} );
} );

/* Japanese initialisation for the jQuery UI date picker plugin. */
/* Written by Kentaro SATO (kentaro@ranvis.com). */
jQuery(function($){
	$.datepicker.regional['ja'] = {
		closeText: '閉じる',
		prevText: '<前',
		nextText: '次>',
		currentText: '今日',
		monthNames: ['1月','2月','3月','4月','5月','6月',
			'7月','8月','9月','10月','11月','12月'],
		monthNamesShort: ['1月','2月','3月','4月','5月','6月',
				'7月','8月','9月','10月','11月','12月'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		weekHeader: '週',
		dateFormat: 'yy/mm/dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年'};
	$.datepicker.setDefaults($.datepicker.regional['ja']);
});


/**
 *  GCalendar Holidays - Googleカレンダーから日本の祝日を取得
 *  @see       http://0-oo.net/sbox/javascript/google-calendar-holidays
 *  @version   0.4.0x
 *  @copyright 2008-2011 dgbadmin@gmail.com
 *  @copyright 2015 Konfactory
 *  @license   http://0-oo.net/pryn/MIT_license.txt (The MIT license)
 *
 *  See also
 *  @see http://code.google.com/intl/ja/apis/calendar/data/2.0/reference.html
 *  @see http://code.google.com/intl/ja/apis/gdata/docs/json.html
 */
var GCalHolidays = {
	/** GoogleカレンダーのID(現在mozilla固定 設定不可) */
	userIds: [
		//"japanese__ja@holiday.calendar.google.com"  //Google公式版
		//"japanese@holiday.calendar.google.com"    //Google公式（英語）版
		"outid3el0qkcrsuf89fltf7a4qbacgt9@import.calendar.google.com" //mozilla.org版
	],

	// @see http://code.google.com/intl/ja/apis/calendar/data/2.0/reference.html#Visibility
	visibility: "public",

	// @see http://code.google.com/intl/ja/apis/calendar/data/2.0/reference.html#Projection
	projection: "full-noattendees",

		/** jQuery UI Datepicker用のstyle（Themeに合わせてお好みで上書きして使う） */
	datepickerStyles: {
		sunday:   "background-image: none; background-color: #fcc", //日曜日
		saturday: "background-image: none; background-color: #ccf", //土曜日
		holiday:  "background-image: none; background-color: #fcc"  //祝日
	}
};
/**
 *  祝日を取得する
 *  @param  Function    callback    データ取得時に呼び出されるfunction
 *  @param  Number      year        (optional) 年（指定しなければ今年）
 *  @param  Number      month       (optional) 月（1～12 指定しなければ1年の全て）
 */
GCalHolidays.get = function(callback, year, month) {
	//日付範囲
	var padZero = function(value) { return ("0" + value).slice(-2); };
	var y = year || new Date().getFullYear();
	var start = [y, padZero(month || 1), "01"].join("-");
	var m = month || 12;
	var end = [y, padZero(m), padZero(new Date(y, m, 0).getDate())].join("-");

	this._caches = (this._caches || {});
	this._userCallback = callback;

	for (var i = 0, len = this.userIds.length; i < len; i++) {
		var cache = this._caches[i + ":" + year + "-" + padZero(month)];

		if (cache) {    //取得済みの場合はそれを使う
			callback(cache, i);
			continue;
		}

		$.ajax({
			url: "/api/get_holidays/" + start + '/' + end,
			cache: true,
			dataType: "json",
			success: function(json) {
				GCalHolidays.decode(json)
			}
		});
	}
};
/**
 *  JSONPによりGoogle Calendar API経由で呼び出されるfunction
 *  @param  Object  gdata   カレンダーデータ
 */
GCalHolidays.decode = function(gdata) {
	var days = GCalHolidays._entries2days(gdata.items);

	var index = null;
	if (gdata.items.length) {
		var userId = gdata.items[0].creator.email;

		//どのカレンダーか特定する
		for (var i = 0, len = this.userIds.length; i < len; i++) {
			if (this.userIds[i] == userId) {
				index = i;
				break;
			}
		}

		//キャッシュする
		var ym = gdata.items[0].start.date.match(/\d{4}-\d{2}/);   //日付範囲の年月
		this._caches[index + ":" + ym] = days;
	}

	//コールバック
	this._userCallback(days, index);
};
/**
 *  JSONPで取得したデータから日付情報を取り出す
 *  @param  Array   entries スケジュール
 *  @return Array   日付情報（year, month, date, title）
 */
GCalHolidays._entries2days = function(entries) {
	var days = [];

	if (!entries) {
		return days;
	}

	//日付順にソート
	entries.sort(function(a, b) {
		return (a.start.date > b.start.date) ? 1 : -1;
	});

	//シンプルな器に移す
	for (var i = 0, len = entries.length; i < len; i++) {
		var ymd = entries[i].start.date.split("-");
		var title = entries[i].summary.split(" ")[0];
		//年月日は使いやすいように数値にする
		days[i] = { year: ymd[0] * 1, month: ymd[1] * 1, date: ymd[2] * 1, title: title };
	}

	return days;
};

/**
 *  jQuery UI Datepickerのカレンダーに祝日を表示する
 *  @param  Number  year    表示する年
 *  @param  Number  month   表示する月
 *  @param  Object  inst    Datepicker
 *  @see http://jqueryui.com/demos/datepicker/
 */
GCalHolidays.datepicker = function(year, month, inst) {
	setTimeout(function() { //処理後に対象のdivが再構築されるケースを回避
		GCalHolidays.get(function(holidays) {
			for (var i = 0, len = holidays.length; i < len; i++) {
				var holiday = holidays[i];

				if (holiday.year != year || holiday.month != month) {
					return; //既に別の月を表示している場合は何もしない
				}

				inst.dpDiv.find("a").each(function() {
					if ($(this).text() == holiday.date) {
						$(this).addClass("gcal-holiday").attr("title", holiday.title);
						return false;
					}
				});
			}
		}, year, month);
	}, 1);
};
/**
 *  jQuery UI Datepickerが有効な場合はイベントハンドラとstyleをセットする
 */
if (window.$ && $.datepicker && $.datepicker.setDefaults) {
	$.datepicker.setDefaults({
		beforeShow: function(input, inst) {
			var date = $(input).datepicker("getDate") || new Date();
			GCalHolidays.datepicker(date.getFullYear(), date.getMonth() + 1, inst);
		},
		beforeShowDay: function(date) { //土日のclass属性
			return [true, { 0: "gcal-sunday", 6: "gcal-saturday" }[date.getDay()] || ""];
		},
		onChangeMonthYear: GCalHolidays.datepicker
	});

	$(function() {  //ページ表示後に土日・祝日用のstyleをセット
		var styles = GCalHolidays.datepickerStyles;
		var css = "";
		css += ".gcal-sunday   .ui-state-default { " + styles.sunday + " } ";
		css += ".gcal-saturday .ui-state-default { " + styles.saturday + " } ";
		css += ".ui-widget-content .gcal-holiday { " + styles.holiday + " }";
		$("head").append($('<style type="text/css">' + css + "</style>"));
	});
}













