/*=====================================================
* Script Name: tour_module_popupCalendar.js
* Description: 空席照会モジュール カレンダー 共通処理
* Version: 1.1
* Last Up Date: 2017/10/25
=====================================================*/
var m_tourModuleScrollTop = 0;

// *** Browser
var useragent = navigator.userAgent.toLowerCase();
var appVersion = parseInt(navigator.appVersion);

// * IE
var isIe = ((useragent.indexOf('msie') != -1) && (useragent.indexOf('opera') == -1));
var isIe5down = false;
if(isIe){
	var isIe3to5 = ((appVersion < 4) || ((appVersion == 4) && ((useragent.indexOf('msie 4')!=-1) || (useragent.indexOf('msie 5.0')!=-1))));
	isIe5down =(isIe && isIe3to5);
}

// * NN & FF
var isNn  = ((useragent.indexOf('mozilla')!=-1) && (useragent.indexOf('spoofer')==-1) && (useragent.indexOf('compatible') == -1) && (useragent.indexOf('opera')==-1));
var isNn6down = false;
if(isNn){
	var isNn2 = (appVersion == 2);
	var isNn3 = (appVersion == 3);
	var isNn4 = (appVersion == 4);
	var isNn6 = (appVersion == 5) && (useragent.indexOf('netscape6') != -1);
	isNn6down = (isNn2 || isNn3 || isNn4 || isNn6);
}

// * OPERA
var isOpera = (useragent.indexOf('opera') != -1);
var isOpera7down = false;
if(isOpera){
	var isOpera2 = (useragent.indexOf('opera 2') != -1 || useragent.indexOf('opera/2') != -1);
	var isOpera3 = (useragent.indexOf('opera 3') != -1 || useragent.indexOf('opera/3') != -1);
	var isOpera4 = (useragent.indexOf('opera 4') != -1 || useragent.indexOf('opera/4') != -1);
	var isOpera5 = (useragent.indexOf('opera 5') != -1 || useragent.indexOf('opera/5') != -1);
	var isOpera6 = (useragent.indexOf('opera 6') != -1 || useragent.indexOf('opera/6') != -1);
	var isOpera7 = (useragent.indexOf('opera 7') != -1 || useragent.indexOf('opera/7') != -1);
	isOpera7down = (isOpera2 || isOpera3 || isOpera4 || isOpera5 || isOpera6 || isOpera7);
}
// * SAFARI
var isSafari = (useragent.indexOf('safari') != -1);
var isSafari13down = false;
if(isSafari) {
	var tmpAgt = useragent.substring(useragent.indexOf('applewebkit/') + 12);
	var version = tmpAgt.substring(0, tmpAgt.indexOf(' '));
	if(version.indexOf('.') != -1){
		version = version.substring(0, version.indexOf('.'));
	}
	version -= 0;
	if(version < 312){
		isSafari13down = true;
	}
}
var is_untarget_brow = (isIe5down || isNn6down || isOpera7down || isSafari13down);

/*******************************************************************************
 * <p>カレンダー入力部品 共通メソッド定義<br>
********************************************************************************/
m_tourModuleCalendarCommonFunc = new Object();
m_tourModuleCalendarCommonFunc = {
	/**
	 * <p>bindオブジェクトに対し、funcメソッドを定義する。</p>
	 * @param {Object} bind
	 * @param {Object} func
	 */
	applyFunc : function(bind, func) {
		return function() {
			return func.apply(bind, arguments);
		};
	},
	addMultiEventListener : function(target, self, type, func) {
		if(target instanceof $tour_module_jq){
			target.on(type, this.applyFunc(self, func));
		} else if (target.addEventListener) {
			target.addEventListener(type, this.applyFunc(self, func), false);
		} else if (target.attachEvent && func.apply) {
			target.attachEvent('on' + type, this.applyFunc(self, func));
		} else {
			target['on' + type] = func;
		}
	},
	cancelEvent : function(evtObj) {
		if (window.attachEvent) {
			window.event.cancelBubble = true;
		} else if (window.addEventListener) {
			evtObj.stopPropagation();
		} else {
			window.event.cancelBubble = true;
		}
	}
};
/*************************************************************
 * <p>Dateの拡張</p>
 *************************************************************/
/**
 * <p>自身の日を加算する。</p>
 * @param {Number} dd 加算する日数
 * @return {Date} 自身
 */
Date.prototype.addDateTour = function(dd){
   this.setTime(this.getTime() + (60 * 60 * 24 * dd * 1000));
   return this;
};

/**
 * <p>自身の月を加算する</p>
 * @param {Number} mm 加算する日数
 * @return {Date} 自身
 */
Date.prototype.addMonthTour = function(mm){
var afterDate = new Date(this.getFullYear(), this.getMonth() + mm, 1);
   return afterDate;
};

/**
 * <p>自身の年月日よりyyyymmdd形式の文字列をかえす。<br>
 * yyyy/mm[00-11]/dd[01-31]</p>
 * @return {String} yyyymmdd String yyyymmdd形式のカレンダー
 */
Date.prototype.zeroPaddingFormatTour = function(){
   return this.getFullYear() + ('0' + (this.getMonth() + 1)).slice(-2) + ('0' + this.getDate()).slice(-2);
};

/**
 * <p>自身の年月日よりyyyymm形式の文字列をかえす。<br>
 * yyyy/mm[00-11]</p>
 * @return {String} yyyymm String yyyymm形式のカレンダー
 */
Date.prototype.zeroPaddingFormatYMTour = function(){
   return this.getFullYear() + ('0' + (this.getMonth() + 1)).slice(-2);
};

/*******************************************************************************
 * <p>ポップアップカレンダーの初期設定を行います。<br>
 * @param {Number} maxLimitDays
 * @param {Number} maxLimitMonths
 * @param {Boolean} isDispHoliday
 * @param {String} locale(J or E)
 * @param {Number} calendarLength
 * @param {Boolean} weekDay
********************************************************************************/
m_tourModulePopupCalendarProperties = function(args){
	
	var defaults = {}
	defaults = {
		maxLimitDays : null,
		maxSelectableDays : null,
		maxLimitMonths : 11,
		isDispHoliday : true,
		locale : 'J',
		calendarLength : 3 ,
		weekDay : true,
		spWidth : 0
	}
	
	$tour_module_jq.extend(true,this,defaults)
	$tour_module_jq.extend(true,this,args);
	
	return this;
}

m_tourModulePopupCalendarProperties.prototype.width = function() {
    return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
}

m_tourModulePopupCalendarProperties.prototype.isSp = function() {
	return this.width() <= this.spWidth;
}

m_tourModulePopupCalendarProperties.prototype.getCalendarLength = function() {
	if (this.isSp()) {
		return this.maxLimitMonths + 1;
	} else {
		return this.calendarLength;
	}
}


/*************************************************************
 * <p>日付操作系ユーティリティ</p>
 *************************************************************/
m_tourModuleDateUtil = function(PCP, boundaryTime) {
	if(is_untarget_brow) {
		return false;
	}

	this.ownYear = [];
	this.dummyDate = this.calcCurrentDate(boundaryTime);
	this.OFFSET = 1980;
	this.MATH_WEIGHT = 0.242194;
	this.maxLimitDateTime = null;
	this.maxSelectableDateTime = null;
	this.systemDate = null;
	this.PCP = PCP;
	
	this.systemDate = this.calcCurrentDate(boundaryTime);

	var maxLimitDate;
	if(this.PCP.maxLimitDays) {
		maxLimitDate = new Date(this.systemDate.getFullYear(), this.systemDate.getMonth(), this.systemDate.getDate() + this.PCP.maxLimitDays);
	} else if (this.PCP.maxLimitMonths){
		maxLimitDate = new Date(this.systemDate.getFullYear(), this.systemDate.getMonth() + this.PCP.maxLimitMonths, 0);
	} else {
		maxLimitDate = new Date(this.systemDate.getFullYear(), this.systemDate.getMonth() + 6, this.systemDate.getDate());
	}
	this.maxLimitDateTime = maxLimitDate.getTime();

	var maxSelectableDate;
	if(this.PCP.maxSelectableDays) {
		maxSelectableDate = new Date(this.systemDate.getFullYear(), this.systemDate.getMonth(), this.systemDate.getDate() + this.PCP.maxSelectableDays);
	} else if(this.PCP.maxSelectableMonths) {
		maxSelectableDate = new Date(this.systemDate.getFullYear(), this.systemDate.getMonth() + this.PCP.maxSelectableMonths, 0);
	}else{
		maxSelectableDate = new Date(this.systemDate.getFullYear(), this.systemDate.getMonth() + 6, this.systemDate.getDate());
	}
	this.maxSelectableDateTime = maxSelectableDate.getTime();
	
	this.dayOfTheWeekClass = ['calendar_sun_label', 'calendar_wday_label', 'calendar_wday_label', 'calendar_wday_label', 'calendar_wday_label', 'calendar_wday_label', 'calendar_sat_label'	];
	
	return this;
}

/**
 * <p>システム日付の新しいインスタンスを作成します。<br>
 * @return {Date}
 */
m_tourModuleDateUtil.prototype.getSystemDate = function(){
	return new Date(this.systemDate.getFullYear(), this.systemDate.getMonth(), this.systemDate.getDate());
}

/**
 * <p>引数で与えられた日付がシステム的に有効かどうかを判定します。<br>
 * ( システム日付 <= date <= 最大有効日付)である場合有効と判断します。</p>
 * @param {Date} date
 * @return {Boolean}
 */
m_tourModuleDateUtil.prototype.isValidDate = function(date) {
	if (this.systemDate.getTime() <= date.getTime() && date.getTime() <= this.maxLimitDateTime) {
		return true;
	} else {
		return false;
	}
};

/**
 * <p>引数で与えられた日付がシステム的に選択可能かどうかを判定します。<br>
 * ( システム日付 <= date <= 最大有効日付)である場合選択可能と判断します。</p>
 * @param {Date} date
 * @return {Boolean}
 */
m_tourModuleDateUtil.prototype.isSelectableDate = function(date) {
	if (this.systemDate.getTime() <= date.getTime() && date.getTime() <= this.maxSelectableDateTime) {
		return true;
	} else {
		return false;
	}
};

/**
 * <p>引数で与えられた日付がシステム的に選択可能な月内かどうかを判定します。<br>
 * ( システム日付（年月） <= date <= 最大有効日付（年月）)である場合選択可能と判断します。</p>
 * @param {Date} date
 * @return {Boolean}
 */
m_tourModuleDateUtil.prototype.isSelectableMonth = function(options, date) {
	var checkMonth = new Date(date.getFullYear(), date.getMonth(), 1);
	var maxSelectableDate = new Date(this.maxSelectableDateTime);
	var checkMaxSelectableMonth = new Date(maxSelectableDate.getFullYear(), maxSelectableDate.getMonth(), 1);

	// 受付非活性後の受付開始日付（の前日）を取得
	var enableStartDate = this.calcEnableStartDate(options);
	if (!enableStartDate) {
		return false;
	}
	// 受付非活性後の受付開始日付（当日）を設定
	enableStartDate.setDate(enableStartDate.getDate() + 1);
	var checkEnableStartMonth = new Date(enableStartDate.getFullYear(), enableStartDate.getMonth(), 1);

	if (checkEnableStartMonth.getTime() <= checkMonth.getTime() && checkMonth.getTime() <= checkMaxSelectableMonth.getTime()) {
		return true;
	} else {
		return false;
	}
};

/**
 * <p>month, dayよりDateオブジェクトを作成します。<br>
 * 妥当でない日付が入力された場合はnullを返却します。</p>
 * @param {Number} month [0-11]
 * @param {Number} date [1-31]
 * @return {Date}
 */
m_tourModuleDateUtil.prototype.createDateWithMD = function(month, day){
	
	if (((month < 0) || (11 < month)) || ((day < 1) || (31 < day))) {
		return null;
	}
	
	var year;
	if ((month < this.systemDate.getMonth()) || ((month == this.systemDate.getMonth()) && (day < this.systemDate.getDate()))) {
		// システム日付より前である場合は翌年とみなす。
		year = this.systemDate.getFullYear() + 1;
	} else {
		year = this.systemDate.getFullYear();
	}
	
	return this.createDateWithYMD(year, month, day);
}

/**
 * <p>year, monthよりDateオブジェクトを作成します。<br>
 * 妥当でない日付が入力された場合はnullを返却します。</p>
 * @param {Number} year yyyy
 * @param {Number} month [0-11]
 * @return {Date}
 */
m_tourModuleDateUtil.prototype.createDateWithYM = function (year, month){
	
	if (((month < 0) || (11 < month)) || ((year < 2008) || 2999 < year)) {
		return null;
	}
	
	var tmpDate = new Date(year, month, 1);
	
	// 妥当性チェック
	if (tmpDate.getFullYear() != year || tmpDate.getMonth() != month) {
		return null;
	}

	return tmpDate;
}

/**
 * <p>year, month, dayよりDateオブジェクトを作成します。<br>
 * 妥当でない日付が入力された場合はnullを返却します。</p>
 * @param {Number} year yyyy
 * @param {Number} month [0-11]
 * @param {Number} date [1-31]
 * @return {Date}
 */
m_tourModuleDateUtil.prototype.createDateWithYMD = function (year, month, day){
	
	if (((month < 0) || (11 < month)) || ((day < 1) || (31 < day)) || ((year < 2008) || 2999 < year)) {
		return null;
	}
	
	var tmpDate = new Date(year, month, day);
	
	// 妥当性チェック
	if (tmpDate.getFullYear() != year || tmpDate.getMonth() != month || tmpDate.getDate() != day) {
		return null;
	}

	return tmpDate;
}

/**
　* <p>その日が祝日であるかどうかを判定する。</p>
　* @param {Date} date
　* @return {boolean}
　*/
m_tourModuleDateUtil.prototype.isHoliday = function(date){
	
	if(!this.PCP.isDispHoliday){
		return false;
	}
	
   var targetYear = date.getFullYear();
   if (!this.ownYear[targetYear]) {
       var map = this.makeHolidayMap(targetYear);
       this.ownYear[targetYear] = map;
   }
   
   var dateZeroPaddingFormat = date.zeroPaddingFormatTour();
   var holidayMap = this.ownYear[targetYear];
   
   if(holidayMap[dateZeroPaddingFormat]){
		return true;
   } else {
		return false;
   }
};

/**
　* <p>yyyymmdd形式の引数からカレンダーを作成する。</p>
　* @param {String} strDate yyyymmdd形式のカレンダー
　* @return {Date}
　*/
m_tourModuleDateUtil.prototype.createCalendarFromFormatVal = function(strDate){
	var year = strDate.substring(0, 4);
	var month = strDate.substring(4, 6);
	if (month.charAt(0) == '0') {
		month = month.slice(-1);
	}
	var date = strDate.substring(6, 8);
	if (date.length == 0) {
		date = '1';
	}
	if (date.charAt(0) == '0') {
		date = date.slice(-1);
	}
	return new Date(year, month - 1, date);
};

/**
 * <p>fullYear年の祝日カレンダーを作成します。</p>
 * @param {Number} fullYear 年[数字4桁]
 * @return {Array} holidayMap
 */
m_tourModuleDateUtil.prototype.makeHolidayMap = function(fullYear){
   var holidayMap = [];
   
   holidayMap[this.createFormatCalendarVal(fullYear, 0, 1)] = '元日';
   holidayMap[this.createFormatCalendarVal(fullYear, 0, this.getWeekDay(fullYear, 0, 2, 1))] = '成人の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 1, 11)] = '建国記念の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 2, this.getVernal(fullYear))] = '春分の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 3, 29)] = '昭和の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 4, 3)] = '憲法記念日';
   holidayMap[this.createFormatCalendarVal(fullYear, 4, 4)] = 'みどりの日';
   holidayMap[this.createFormatCalendarVal(fullYear, 4, 5)] = 'こどもの日';
   holidayMap[this.createFormatCalendarVal(fullYear, 6, this.getWeekDay(fullYear, 6, 3, 1))] = '海の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 7, 11)] = '山の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 8, this.getWeekDay(fullYear, 8, 3, 1))] = '敬老の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 8, this.getAutumnal(fullYear))] = '秋分の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 9, this.getWeekDay(fullYear, 9, 2, 1))] = '体育の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 10, 3)] = '文化の日';
   //	if(fullYear == 2009){
   //    holidayMap[this.createFormatCalendarVal(fullYear, 10, 12)] = '天皇陛下御即位二十年奉祝';
   //	}
   holidayMap[this.createFormatCalendarVal(fullYear, 10, 23)] = '勤労感謝の日';
   holidayMap[this.createFormatCalendarVal(fullYear, 11, 23)] = '天皇誕生日';
   
   var tempMap1 = [];
   for (var key1 in holidayMap) {
       var cal1 = this.createCalendarFromFormatVal(key1);
       if (cal1.getDay() == 0) {
           for (;;) {
               cal1.addDateTour(1);
               var targetFormatCal1 = cal1.zeroPaddingFormatTour();
               if (!holidayMap[targetFormatCal1]) {
                   tempMap1[targetFormatCal1] = targetFormatCal1;
                   break;
               }
           }
       }
   }
   
   for (var key2 in tempMap1) {
       holidayMap[key2] = '振替休日';
   }
   
   var tempMap2 = [];
   for (var key3 in holidayMap) {
       var cal2 = this.createCalendarFromFormatVal(key3);
       cal2.addDateTour(2);
       if (holidayMap[cal2.zeroPaddingFormatTour()]) {
           cal2.addDateTour(-1);
           var targetFormatCal2 = cal2.zeroPaddingFormatTour();
           if (!holidayMap[targetFormatCal2]) {
               tempMap2[targetFormatCal2] = targetFormatCal2;
           }
       }
   }
   
   for (var key4 in tempMap2) {
       holidayMap[key4] = '国民の休日';
   }
   
   return holidayMap;
};

/**
 * <p>指定月の第？？曜日の日付を求める。</p>
 * @param {Number} year 年
 * @param {Number} month 月
 * @param {Number} day 日
 */
m_tourModuleDateUtil.prototype.createFormatCalendarVal = function(year, month, day){
	this.dummyDate.setFullYear(year, month, day);
	return this.dummyDate.zeroPaddingFormatTour();
};

/**
 * <p>指定月の第？？曜日の日付を求める。</p>
 * @param {Number} year 年
 * @param {Number} month 月
 * @param {Number} day 日
 */
m_tourModuleDateUtil.prototype.createFormatCalendarYMVal = function(year, month){
	this.dummyDate.setFullYear(year, month, 1);
	return this.dummyDate.zeroPaddingFormatYMTour();
};

/**
 * <p>指定月の第？？曜日の日付を求める。</p>
 * @param {Number} yyyy 年
 * @param {Number} mm 月
 * @param {Number} week 第何週目かの指定[1-6]
 * @param {Number} wday 曜日を指定[0(日)-6(土)]
 * @return {Date} date 指定月の第？？曜日の日付
 */
m_tourModuleDateUtil.prototype.getWeekDay = function(yyyy, mm, week, wday){
   var fisrtDate = new Date(yyyy, mm, 1);
   var firstDay = fisrtDate.getDay();
   
   var lastDate = new Date(fisrtDate.getFullYear(), fisrtDate.getMonth() + 1, 0);
   var numOfLastDate = lastDate.getDate();
   
   if (wday >= firstDay) {
       week--;
   }
   var numOfDate = 7 * week + wday + 1 - firstDay;
   if ((numOfDate > numOfLastDate) || (numOfDate <= 0)) {
       return null;
   }
   return numOfDate;
};

/**
 * <p>春分の日を算出する。<br>
 * ※2099年まで有効</p>
 * @param {String} yyyy 現在年
 * @return {String} 指定された年の春分の日を返す
 */
m_tourModuleDateUtil.prototype.getVernal = function(yyyy) {
  var vernal = parseInt(20.8431 + this.MATH_WEIGHT * (yyyy - this.OFFSET) - ((yyyy - this.OFFSET) >> 2));
  return vernal;
};

/**
 * <p>秋分の日を算出する。<br>
 * ※2099年まで有効</p>
 * @param {Number} yyyy 現在年
 * @return {String} 指定された年の秋分の日を返す
 */
m_tourModuleDateUtil.prototype.getAutumnal = function(yyyy) {
  var autumnal = parseInt(23.2488 + this.MATH_WEIGHT * (yyyy - this.OFFSET) - ((yyyy - this.OFFSET) >> 2));
  return autumnal;
};

/**
 * <p>プルダウン(月)要素の取得</p>
 * @return {Object} プルダウン要素
 */
m_tourModuleDateUtil.prototype.getDateClassElement = function(options, className) {
	return $tour_module_jq('#' + options.textId).parents('form').find(className).get(0);
};

// 営業日付を取得
m_tourModuleDateUtil.prototype.calcCurrentDate = function(boundaryTime) {
	// 営業日付を取得
	var currentDate = new Date();
	// 指定した時刻（営業終了時刻）を超えていたら翌日を当日に設定しなおす
	var compareDate;
	if (boundaryTime) {
		compareDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(),
			boundaryTime.substring(0,2), boundaryTime.substring(3,5));
	} else {
		// デフォルトの設定時間は17:00とする
		compareDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 17, 0);
	}
	// 指定した時刻（営業終了時刻）を超えている場合
	if (currentDate.getTime() > compareDate.getTime()) {
		currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
	} else {
		currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
	}
	return currentDate;
};

// 開始日に対する終了日の取得
m_tourModuleDateUtil.prototype.calcRetDaysFromStartDate = function(options) {
	var retDaysFromStartDate;
	if (!options.returnDaysFromDepartureDate) {
		return 2;
	}

	// 指定されたオプション値がnumber型の場合は直接返す
	if (typeof options.returnDaysFromDepartureDate === 'number') {
		return options.returnDaysFromDepartureDate;
	}

	var areaClass = options.targetObject.parents('form').find('.' + options.areaClass);
	var countryClass = options.targetObject.parents('form').find('.' + options.countryClass);
	var apoClass = options.targetObject.parents('form').find('.' + options.apoClass);

	// 都市に紐付く日数情報を取得
	var apoClassValue = apoClass.val();
	retDaysFromStartDate = options.returnDaysFromDepartureDate['city'][apoClassValue];
	// 都市に紐付く日数情報を取得出来たらそれを返す
	if (retDaysFromStartDate) {
		return retDaysFromStartDate;
	}
	// 国に紐付く日数情報を取得
	var countryClassValue = countryClass.val();
	retDaysFromStartDate = options.returnDaysFromDepartureDate['country'][countryClassValue];
	// 国に紐付く日数情報を取得出来たらそれを返す
	if (retDaysFromStartDate) {
		return retDaysFromStartDate;
	}
	// 地域に紐付く日数情報を取得
	var areaClassValue = areaClass.val();
	retDaysFromStartDate = options.returnDaysFromDepartureDate['area'][areaClassValue];
	// 地域に紐付く日数情報を取得出来たらそれを返す
	if (retDaysFromStartDate) {
		return retDaysFromStartDate;
	}
	// デフォルトの日数情報を取得
	retDaysFromStartDate = options.returnDaysFromDepartureDate['default'];
	// デフォルトの日数情報を取得出来たらそれを返す
	if (retDaysFromStartDate) {
		return retDaysFromStartDate;
	}

	return 2;
};

// 受付終了日数の取得
m_tourModuleDateUtil.prototype.calcReceiptDisableDays = function(options) {
	var targetReceiptDisableDays;
	if (!options.receiptDisableDays && typeof options.receiptDisableDays !== 'number') {
		return 18;
	}

	// 指定されたオプション値がnumber型の場合は直接返す
	if (typeof options.receiptDisableDays === 'number') {
		return options.receiptDisableDays;
	}

	if (options.areaClass) {
		var areaClass = this.getDateClassElement(options, '.' + options.areaClass);
		if (areaClass) {
			var areaClassValue = areaClass.value;
			targetReceiptDisableDays = options.receiptDisableDays[areaClassValue];
			if (!targetReceiptDisableDays) {
				targetReceiptDisableDays = options.receiptDisableDays['default'];
			}
		}
	} else {
		targetReceiptDisableDays = options.receiptDisableDays['default'];
	}
	return targetReceiptDisableDays;
};

// 最大旅行日数の取得
//以下の条件でカレンダーの種類を判断
//startDateClass：設定済み＋middleDateClass：設定済み⇒帰りのカレンダー
//startDateClass：設定済み＋middleDateClass：設定なし⇒移動日のカレンダー
//startDateClass：設定なし⇒行きのカレンダー
m_tourModuleDateUtil.prototype.calcMaxTravelDays = function(options) {
	var maxTravelDays;
	if (!options.maxTravelDays && typeof options.maxTravelDays !== 'number') {
		return undefined;
	}

	// 指定されたオプション値がnumber型の場合は直接返す
	if (typeof options.maxTravelDays === 'number') {
		return options.maxTravelDays;
	}

	if (options.areaClass) {
		var areaClass = this.getDateClassElement(options, '.' + options.areaClass);
		if (areaClass) {
			// 移動日または帰りのカレンダーの設定
			var areaClassValue = areaClass.value;
			maxTravelDays = options.maxTravelDays[areaClassValue];
			if (!maxTravelDays) {
				maxTravelDays = options.maxTravelDays['default'];
			}

		} else {
			maxTravelDays = undefined;
		}
	} else {
		maxTravelDays = undefined;
	}

	return maxTravelDays;
};

// 受付開始日付(の前日)を取得
// 以下の条件でカレンダーの種類を判断
// startDateClass：設定済み＋middleDateClass：設定済み⇒帰りのカレンダー
// startDateClass：設定済み＋middleDateClass：設定なし⇒移動日のカレンダー
// startDateClass：設定なし⇒行きのカレンダー
m_tourModuleDateUtil.prototype.calcEnableStartDate = function(options) {
	var enableStartDate;
	var currentDate = m_tourModuleDateUtil.prototype.calcCurrentDate(options.boundaryTime);
	var receiptDisableDays;
	receiptDisableDays = m_tourModuleDateUtil.prototype.calcReceiptDisableDays(options);

	// 受付非活性後の受付開始日付を設定
	if (options.startDateClass) {
		var isExcludeMiddleDate = this.isExcludeMiddleDate(options.stayCityCount);
		var startDateClassElement = this.getDateClassElement(options, '.' + options.startDateClass);
		var startDate = this.createCalendarFromFormatVal(startDateClassElement.value);
		if(options.sameDayFlag){
			//行きと同日選択可能な場合
			startDate.setDate(startDate.getDate()-1);
		}
		if (options.middleDateClass) {
			// 帰りのカレンダーの設定
			var middleDateClassElement = this.getDateClassElement(options, '.' + options.middleDateClass);
			var middleDate = this.createCalendarFromFormatVal(middleDateClassElement.value);

			var retDays = this.calcRetDaysFromStartDate(options);
			var startDateYear = parseInt(startDateClassElement.value.substring(0, 4));
			var startDateMonth = parseInt(startDateClassElement.value.substring(4, 6));
			var startDateDay = parseInt(startDateClassElement.value.substring(6, 8));
			var retDaysFromStartDate = new Date(startDateYear, startDateMonth - 1, startDateDay + retDays - 1);

			if (isExcludeMiddleDate) {
				// 移動日の省略時
				enableStartDate = retDaysFromStartDate;
			} else {
				// 移動日を表示する場合の分岐
				if (middleDate.getTime() > retDaysFromStartDate.getTime()) {
					enableStartDate = middleDate;
				} else {
					enableStartDate = retDaysFromStartDate;
				}
			}

		} else {
			// 移動日のカレンダーの設定
			enableStartDate = startDate;
		}
	} else if(options.minTravelDateClass){
		//受付開始日付が他項目と連動する場合
		var startDateClassElement = this.getDateClassElement(options, '.' + options.minTravelDateClass);
		var startDateYear = parseInt(startDateClassElement.value.substring(0, 4));
		var startDateMonth = parseInt(startDateClassElement.value.substring(4, 6));
		var startDateDay = parseInt(startDateClassElement.value.substring(6, 8));
		var retDaysFromStartDate = new Date(startDateYear, startDateMonth - 1, startDateDay - 1);
		enableStartDate = retDaysFromStartDate;
		
	}else{
		// 行きのカレンダーの設定
		if (typeof receiptDisableDays === 'number') {
			enableStartDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + receiptDisableDays);
		} else {
			enableStartDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 18);
		}
	}

	return enableStartDate;
};

// 受付開始日に対する受付終了日付を取得
m_tourModuleDateUtil.prototype.calcEnableEndDate = function(options, enableStartDate) {
	var enableEndDate;
	var maxTravelDays = m_tourModuleDateUtil.prototype.calcMaxTravelDays(options);
	if (typeof maxTravelDays !== 'number') {
		enableEndDate = new Date(2999, 12, 31);
		return enableEndDate;
	}

	if(options.maxTravelDateClass){
		//チェックイン時の考慮
		var startDateClassElement = this.getDateClassElement(options, '.' + options.maxTravelDateClass);
		var startDate = this.createCalendarFromFormatVal(startDateClassElement.value);
		enableEndDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + maxTravelDays);
	}else if (options.startDateClass) {
		// 移動日または帰りのカレンダーの設定
		var startDateClassElement = this.getDateClassElement(options, '.' + options.startDateClass);
		var startDate = this.createCalendarFromFormatVal(startDateClassElement.value);
		enableEndDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + maxTravelDays);
	} else if(options.maxTravelDateClass){
		//チェックイン時の考慮
		var startDateClassElement = this.getDateClassElement(options, '.' + options.maxTravelDateClass);
		var startDate = this.createCalendarFromFormatVal(startDateClassElement.value);
		enableEndDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + maxTravelDays);
	}else{
		// 行きのカレンダーの設定
		enableEndDate = new Date(2999, 12, 31);
	}

	return enableEndDate;
};

//滞在都市数から中間日付の除外を判定
m_tourModuleDateUtil.prototype.isExcludeMiddleDate = function(stayCityCount) {
	// 中間日付除外フラグ
	var isExcludeMiddleDate = false;
	// 滞在都市数を取得
	if (stayCityCount) {
		var selectedStayCityCount = stayCityCount.find('input[name=m_stay]:checked');
		// 1都市滞在に設定されている場合
		if (selectedStayCityCount && selectedStayCityCount.val() === '0') {
			isExcludeMiddleDate = true;
		}
	}
	return isExcludeMiddleDate;
};

/*************************************************************
 * <p>表示するカレンダーのコントロールクラスです。</P>
 * 
 *************************************************************/
m_tourModuleCalendarContoroller = function() {

	if(is_untarget_brow) {
		return false;
	}
	
	this.displayingCalendarName = null;
	this.cals = new Array();
	for (var i = 0, len = arguments.length; i < len; i++) {
		this.cals.push(arguments[i]);
		this.cals[i].calendarContoroller = this;
	}

	return this;	
};

/**
 * <p>与えられたカレンダーの表示を行います。<br>
 * プルダウンで選択された日付を基にカレンダーの表示を行います。<br>
 * プルダウンで日付が選択されていない場合はプライオリティがひとつ前のカレンダーが保持するプルダウン日付を基にカレンダーを表示します。<br>
 * それでも日付が取得できない場合はシステム日付を基にカレンダーを表示します。</p>
 * @param {m_tourModulePopupCalendar} cal
 */
m_tourModuleCalendarContoroller.prototype.dispCalendar = function(cal){
	
	// close the displaying calendars
	this.closeAllCalendar();
	
	if(this.isDisplayingCalendar(cal)){
		// update
		this.displayingCalendarName = null;
		return true;
	}
	
    // get the calendar in pull down
    var tmpDate = cal.getPulldownDate();
   	if (cal.DU.PCP.isSp()) {
      tmpDate = cal.DU.getSystemDate();
    } else if ( !tmpDate ) {
      tmpDate = cal.DU.getSystemDate();
    }

    // describe popupCalendar
    cal.describeCalendar(tmpDate);
	
	// update
	this.displayingCalendarName = cal.name;
	
	return true;
};

/**
 * <p>与えられたプライオリティーよりもひとつ前のプライオリティーカレンダーを取得します。</p>
 * @param {m_tourModulePopupCalendar} cal
 * @return {m_tourModulePopupCalendar} tmpCal
 */
m_tourModuleCalendarContoroller.prototype.getPrevPriorityCalendar = function(cal){
	for (var i = 0, n = this.cals.length; i < n; i++) {
		var tmpCal = this.cals[i];
		if( tmpCal.priority == (cal.priority - 1)) {
			return tmpCal;
		}
	}
	return null;
};

/**
 * <p>表示されているカレンダーを全て非表示にします。</p>
 */
m_tourModuleCalendarContoroller.prototype.closeAllCalendar = function(){
	for (var i = 0, n = this.cals.length; i < n; i++) {
		this.cals[i].close();
	}
	return true;
};

/**
 * <p>カレンダーを設定します。</p>
 * @param {m_tourModulePopupCalendar} arguments
 */
m_tourModuleCalendarContoroller.prototype.addCalendar = function(cal) {
	if (!this.cals) {
		this.cals = new array();
	}
	this.cals.push(cal);
	return true;
};

/**
 * <p>カレンダーを閉じます。</p>
 */
m_tourModuleCalendarContoroller.prototype.close = function() {
	this.closeAllCalendar();
	
	// update
	this.displayingCalendarName = null;
	
	return true;
};

/**
 * <p>表示されているカレンダーであるかどうかを判定します。</p>
 * @param {m_tourModulePopupCalendar} cal 
 * @return {Boolean} true:表示中のカレンダー false:その他
 */
m_tourModuleCalendarContoroller.prototype.isDisplayingCalendar = function(cal) {
	return $tour_module_jq('#cal_balloon_parts_' + cal.uniqueKey + ':visible').length > 0;
};

/**
 * <p>次の月に進む</p>
 * @param {m_tourModulePopupCalendar} cal  
 */
m_tourModuleCalendarContoroller.prototype.goNextMonth = function(cal) {
	if(!this.isDisplayingCalendar(cal)){
		this.close();
	} else {
		cal.updateCalendar(cal.baseDate.addMonthTour(cal.prevNextInterval));
	}
	return true;
};

/**
 * <p>前の月に戻る</p>
 * @param {m_tourModulePopupCalendar} cal  
 */
m_tourModuleCalendarContoroller.prototype.goPreviousMonth = function(cal) {
	if(!this.isDisplayingCalendar(cal)){
		this.close();
	} else {
		cal.updateCalendar(cal.baseDate.addMonthTour(-cal.prevNextInterval));		
	}
	return true;
};

/**
 * <p>今月に進む</p>
 * @param {m_tourModulePopupCalendar} cal  
 */
m_tourModuleCalendarContoroller.prototype.goCurrentMonth = function(cal) {
	cal.updateCalendar(cal.DU.getSystemDate());
	return true;
};

/**
 * <p>プルダウンに年月を設定する。<br>
 * @param {m_tourModulePopupCalendar} cal
 * @param {Number} year
 * @param {Number} month
 */
m_tourModuleCalendarContoroller.prototype.setMonthToForm = function(cal, year, month) {

	if(!this.isDisplayingCalendar(cal)){
		this.close();
	}

	var tmpDate = cal.DU.createDateWithYM(year, month);

	cal.setDatePulldownYM(tmpDate);

	this.close();

	return true;
};

/**
 * <p>プルダウンに日付を設定する<br>
 * 不正な日付であった場合システム日付をプルダウンに設定する。</p>
 * @param {m_tourModulePopupCalendar} cal
 * @param {Number} year
 * @param {Number} month
 * @param {Number} day
 */
m_tourModuleCalendarContoroller.prototype.setDateToForm = function(cal, year, month, day) {
	
	if(!this.isDisplayingCalendar(cal)){
		this.close();
	}
	
	var tmpDate = cal.DU.createDateWithYMD(year, month, day);
	if(!tmpDate){
		tmpDate = cal.DU.getSystemDate();
	} 
	
	cal.setDatePulldown(tmpDate);
	
	this.close();
	
	return true;
};

/*************************************************************
 * <p>ポップアップカレンダー表示クラス用言語設定</p>
 * 
 * @param	{String[11]}	monthList			1月～１２月に対応する表記を格納した配列
 * @param	{String[6]}		weekList			日曜日から～土曜日に対応する表記を格納した配列
 * @param	{String}		prevMonthFormat		前の３か月ボタンの表記を設定した文字列
 * @param	{String}		nextMonthFormat		次の３か月ボタンの表記を設定した文字列
 * @param	{String}		closeButtonFormat	閉じるボタンの表記を設定した文字列
 * @param	{String}		DateFormatter		年月日を出力したい書式に変換する関数
 *************************************************************/
m_tourModulePopupCalendarLanguageProperties = function(args){
	
	var defaults = {
		HeadFormat : 'Select the date of departure.',
		MonthList : new Array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'),
		WeekList : new Array('SU','MO','TU','WE','TH','FR','ST'),
		PrevMonthFormat : '3 months earlier',
		NextMonthFormat : '3 months later',
		PresentMonthFormat : '',
		CloseButtonFormat : 'Close',
		FormatMonth : function(baseDate) {
			return this.MonthList[baseDate.getMonth()]; },
		FormatYearMonth : function(baseDate) {
			return this.FormatMonth(baseDate) + ' ' + baseDate.getFullYear(); },
		FormatDate : function(baseDate) {
			return parseInt(baseDate.getFullYear(), 10) + '/' + parseInt(baseDate.getMonth(), 10) + '/' + parseInt(baseDate.getDate(), 10); }
	}
	
	$tour_module_jq.extend(true,this,defaults)
	$tour_module_jq.extend(true,this,args);
	
	return this;
	
}

/**
 * <p>自身の年月日より各言語の年月表現を返す。<br>
 * 'J':yyyy年mm[1-12]月/'E':${英語文字月表現}, yyyy</p>
 * @see m_tourModuleDateUtil.PCP.locale
 * @return {String}
 */
//Date.prototype.getStringYearMonthFormat = function(PCLP){
// this.getFullYear() + '年&nbsp;' +  stringMonthFormat;

/**
 * <p>自身の年月日より各言語の年表現を返す。<br>
 * @see PCLP
 * @return {String}
 */
/*Date.prototype.getStringMonthFormat = function(PCLP){
	return PCLP.MonthList[this.getMonth()];
}
*/

/*************************************************************
 * <p>ポップアップカレンダー表示クラス</p>
 * 
 * @param {String} name		ポップアップカレンダー名
 * @param {String} id		ポップアップカレンダー表示ID
 * @param {String} monthId	月プルダウンID
 * @param {String} dayId	日プルダウンID
 * @param {String} imgId	カレンダー画像ID
 * @param {String} dateField	国際・WWS向け
 * @param {Number} priority	表示優先度
 * @param {String} form_number	
 * @param {String} zero_value
 * @param {String} connectionCal
 * @param {String} DU
 * @param {String} PCLP
 * @param {String} isReceiveTrigger トリガーイベント許容フラグ
 *************************************************************/
m_tourModulePopupCalendar = function(args) {

	var defaults = {
		cssPatternPc : 'd_patternPC d_calendarPosition',
		cssPatternTablet : 'd_patternPC d_calendarPosition',
		DU : undefined,
		PCLP : undefined,
		name : undefined,
		id : undefined,
		monthId : undefined,
		dayId : undefined,
		dateField : undefined,
		textId : undefined,
		priority : undefined,
		index : undefined,
		uniqueKey : undefined,
		form_number : undefined,
		zero_value : undefined,
		connectionCal : undefined,
		endDay : undefined,
		calendarContoroller : undefined,
		isModalModule : false,
		isReceiveTrigger : false
	};
	
	if(is_untarget_brow) {
		return false;
	}

	$tour_module_jq.extend(true,this,defaults)
	$tour_module_jq.extend(true,this,args);

	this.baseDate = null;
	m_tourModulePopupCalendar.cals[this.name] = this;
	var _this = this;
	var m_txt = document.getElementById(this.textId);

	if(this.isReceiveTrigger){
		if (m_txt) {
			m_txt = $tour_module_jq(m_txt); // jQueryオブジェクトへ変換
			m_tourModuleCalendarCommonFunc.addMultiEventListener(
				m_txt, this, 'click', function(event) {
					$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).remove();
					$tour_module_jq('.is-balloon_open').removeClass('is-balloon_open');
					_this.chgDsc();
				});
			m_tourModuleCalendarCommonFunc.addMultiEventListener(
				m_txt, this, 'keydown', function(event) {
					if(event.keyCode === 13){
						$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).remove();
						$tour_module_jq('.is-balloon_open').removeClass('is-balloon_open');
						_this.chgDsc();
					}
				});
		}
	}else{
		if (m_txt) {
			m_tourModuleCalendarCommonFunc.addMultiEventListener(
				m_txt, this, 'click', function(event) {
					$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).remove();
					$tour_module_jq('.is-balloon_open').removeClass('is-balloon_open');
					_this.chgDsc();
					m_tourModuleCalendarCommonFunc.cancelEvent(event || window.event);
				});
			m_tourModuleCalendarCommonFunc.addMultiEventListener(
				m_txt, this, 'keydown', function(event) {
					if(event.keyCode === 13){
						$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).remove();
						$tour_module_jq('.is-balloon_open').removeClass('is-balloon_open');
						_this.chgDsc();
						m_tourModuleCalendarCommonFunc.cancelEvent(event || window.event);
					}
				});
		}
	}

	this.calendarContoroller.addCalendar(this);
	return this;
};

/**
 * <p>（カレンダー）内部で使用しているカレンダーを保持しておく。</p>
 */
m_tourModulePopupCalendar.cals = new Array();

/**
 * <p>（カレンダー）表示の通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgDsc = function() {
	m_tourModuleScrollTop = $tour_module_jq(window).scrollTop();
	this.calendarContoroller.dispCalendar(this);
	return true;
};

/**
 * <p>（カレンダー）次の月に進む通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgNext = function() {
	this.calendarContoroller.goNextMonth(this);
	return true;
};

/**
 * <p>（カレンダー）前の月に戻る通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgPrev = function() {
	this.calendarContoroller.goPreviousMonth(this);
	return true;
};

/**
 * <p>（カレンダー）今下月に進む通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgCurrent = function() {
	this.calendarContoroller.goCurrentMonth(this);
	return true;
};

/**
 * <p>（カレンダー）
 通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgClose = function() {
	this.calendarContoroller.close();
	$tour_module_jq(window).scrollTop(m_tourModuleScrollTop);
	$tour_module_jq('#' + this.textId).focus();
	return true;
};

/**
 * <p>（カレンダー）日付が選択された通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgSet = function(year, month, day) {
	var startDate = '';
	var startDateVal = '';
	if (this.startDateClass) {
		startDate = $tour_module_jq('#' + this.textId).parents('form').find('.' + this.startDateClass);
		startDateVal = startDate.val();
	}
	var middleDate = '';
	if (this.middleDateClass) {
		middleDate = $tour_module_jq('#' + this.textId).parents('form').find('.' + this.middleDateClass);
	}
	var retDate = '';
	if (this.retDateClass) {
		retDate = $tour_module_jq('#' + this.textId).parents('form').find('.' + this.retDateClass);
	}
	this.calendarContoroller.setDateToForm(this, year, month, day);
	$tour_module_jq(window).scrollTop(m_tourModuleScrollTop);
	$tour_module_jq('#' + this.textId).focus();
	$tour_module_jq('#' + this.textId).blur();
	return true;
};

/**
 * <p>（カレンダー）XX月すべてが選択された通知を受け取る</p>
 */
m_tourModulePopupCalendar.prototype.chgSetYM = function(year, month) {
	this.calendarContoroller.setMonthToForm(this, year, month);
	$tour_module_jq(window).scrollTop(m_tourModuleScrollTop);
	return true;
};


/**
 * <p>カレンダーを閉じる</p>
 */
m_tourModulePopupCalendar.prototype.close = function() {
	var targetElement = document.getElementById(this.id);
	if (targetElement) {
		targetElement.style.display = 'none';
	}
	return true;
};

/**
 * <p>プルダウン日付の取得</p>
 * @return {Date} date プルダウン日付
 */
m_tourModulePopupCalendar.prototype.getPulldownDate = function() {
	var monthElement = this.getPullDownMonthElement();
	var dayElement = this.getPullDownDayElement();

  //要素チェック
  if (!monthElement) {
    //何もしない
  } else if (!dayElement) {
    //何もしない
  } else {
  	var CalMonth = monthElement.value;
  	if (typeof this.DU.PCP.CalYear != 'undefined') {
  		CalMonth = monthElement.value.substring(4,6);
  	}

  	if (String(CalMonth).charAt(0) == '0') {
  		mstr = String(CalMonth).charAt(1);
  	} else {
  		mstr = String(CalMonth);
  	}

  	var month = parseInt(mstr, 10) - 1;
  	if (dayElement.value == '') {
  		var day = 1;
  	} else {
  		var day = parseInt(dayElement.value, 10);
  	}
  	return this.DU.createDateWithMD(month, day);
  }

  //要素チェック
  var dateFieldElement = this.getDateFieldElement();
  if (!dateFieldElement) {
    //何もしない
  } else if (!dateFieldElement.value) {
    return this.DU.getSystemDate();
  } else {
    return this.DU.createCalendarFromFormatVal(dateFieldElement.value);
  }
};

/**
 * <p>プルダウン(月)要素の取得</p>
 * @return {Object} プルダウン要素
 */
m_tourModulePopupCalendar.prototype.getDateFieldElement = function() {
	if(this.form_number){
		return document.getElementsByTagName('form')[this.form_number][this.dateField];
	} else if (this.index) {
		return document.getElementsByName(this.monthId)[this.index];
	} else {
	  return $tour_module_jq('#' + this.textId).parents('form').find('.' + this.dateValueClass).get(0);
	}
};

/**
 * <p>プルダウン(月)要素の取得</p>
 * @return {Object} プルダウン要素
 */
m_tourModulePopupCalendar.prototype.getPullDownMonthElement = function() {
	if(this.form_number){
		return document.getElementsByTagName('form')[this.form_number][this.monthId];
	} else if (this.index) {
		return document.getElementsByName(this.monthId)[this.index];
	} else {
		return $tour_module_jq('#' + this.textId).parents('form').find('[name="' + this.monthId + '"]').get(0);
	}
};

/**
 * <p>プルダウン(日)要素の取得</p>
 * @return {Object} プルダウン要素
 */
m_tourModulePopupCalendar.prototype.getPullDownDayElement = function() {
	if(this.form_number){
		return document.getElementsByTagName('form')[this.form_number][this.dayId];
	} else if (this.index) {
		return document.getElementsByName(this.dayId)[this.index];
	} else {
		return $tour_module_jq('#' + this.textId).parents('form').find('[name="' + this.dayId + '"]').get(0);
	}
};

/**
 * <p>ポップアップカレンダーの外枠を作成します。<br>
 */
m_tourModulePopupCalendar.prototype.createCalendarBase = function() {

	var htmlArray = new Array();
	var htmlArrayIndex = 0;

	if (!_.isEmpty(this.additionalClasses)) {
		var additionalClassNames = '';
		if (typeof(this.additionalClasses) === 'string') {
			additionalClassNames = this.additionalClasses + ' ';
		} else if (typeof(this.additionalClasses) === 'object') {
			for(var key in this.additionalClasses) {
				additionalClassNames = additionalClassNames + this.additionalClasses[key] + ' ' ;
			}
		}
		htmlArray[htmlArrayIndex++] = '<div id="cal_balloon_parts_' + this.uniqueKey + '" class="' + additionalClassNames + 'modal-balloon module-travel_parts select-calendar cal_balloon_parts">\n';
	} else {
		htmlArray[htmlArrayIndex++] = '<div id="cal_balloon_parts_' + this.uniqueKey + '" class="modal-balloon module-travel_parts select-calendar cal_balloon_parts">\n';
	}

	htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_cover">\n';
	htmlArray[htmlArrayIndex++] = '<i class="modal-balloon_arrow"></i>\n';
	htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_inner">\n';
	htmlArray[htmlArrayIndex++] = '<!-- \n';
	htmlArray[htmlArrayIndex++] = '  *****************************************************\n';
	htmlArray[htmlArrayIndex++] = '  *************** select-calendar module **************\n';
	htmlArray[htmlArrayIndex++] = '  *****************************************************\n';
	htmlArray[htmlArrayIndex++] = '-->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .modal-balloon_inner -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .modal-balloon_cover -->\n';
	htmlArray[htmlArrayIndex++] = '</div>\n';// <!-- / .modal-balloon -->

	return htmlArray.join('');
};



/**
 * <p>ポップアップカレンダーの中身を作成します。<br>
 * 与えられたbaseDateの年月を基準にカレンダーを作成します。</p>
 * @param {Date} baseDate  カレンダー表示基準日付
 */
m_tourModulePopupCalendar.prototype.createCalendarBody = function(baseDate) {

	this.baseDate = baseDate;

	var htmlArray = new Array();
	var htmlArrayIndex = 0;

	htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_contents">\n';
	htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_title">\n';
	htmlArray[htmlArrayIndex++] = '    <h2 class="mod-balloon_ttl">' + this.balloonTitle + '</h2>\n';
	htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n';
	htmlArray[htmlArrayIndex++] = '  </div>\n';
	htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_cont_cover">\n';

	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-cont_inner">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-set">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-set_cover mod-balloon-set_single">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-set_box">\n';

	htmlArray[htmlArrayIndex++] = '\n';
	htmlArray[htmlArrayIndex++] = '<div class="module-calendar is-period">\n';
	htmlArray[htmlArrayIndex++] = '<div class="calendar-wrap">\n';

	// 営業日付を取得
	var currentDate = m_tourModuleDateUtil.prototype.calcCurrentDate(this.boundaryTime);
	var currentMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
	var baseMonthMin = new Date(baseDate.getFullYear(), baseDate.getMonth(), 1);
	var baseMonthMax = new Date(baseDate.getFullYear(), baseDate.getMonth() + 2, 1);

	if (this.DU.PCP.isSp()) {
		//次へ、前へ非表示
	} else {
		htmlArray[htmlArrayIndex++] = '<ul class="calendar-nav">\n';
		if (this.isEnableToMovePrevMonth(-(this.DU.PCP.getCalendarLength()))) {
			htmlArray[htmlArrayIndex++] = '    <li class="prev"><a tabindex="0" href="javascript:void(0);" class="calPrev" onclick="m_tourModulePopupCalendar.cals[\'' + this.name + '\'].chgPrev()" href="javascript:void(0);">\n';
			htmlArray[htmlArrayIndex++] = this.PCLP.PrevMonthFormat;
			htmlArray[htmlArrayIndex++] = '</a></li>\n';
		} else {
			htmlArray[htmlArrayIndex++] = '    <li class="prev"></li>\n';
		}
		if (this.isEnableToMoveNextMonth(this.DU.PCP.getCalendarLength())) {
			htmlArray[htmlArrayIndex++] = '    <li class="next"><a tabindex="0" href="javascript:void(0);" class="calNext" onclick="m_tourModulePopupCalendar.cals[\'' +  this.name + '\'].chgNext()" href="javascript:void(0);">\n';
			htmlArray[htmlArrayIndex++] = this.PCLP.NextMonthFormat;
			htmlArray[htmlArrayIndex++] = '</a></li>\n';
		}
		if (currentMonth.getTime() < baseMonthMin.getTime()
			|| currentMonth.getTime() > baseMonthMax.getTime()) {
			htmlArray[htmlArrayIndex++] = '<li class="current"><a tabindex="0" href="javascript:void(0);" class="calCurrent" onclick="m_tourModulePopupCalendar.cals[\'' +  this.name + '\'].chgCurrent()">今月</a></li>\n';
		}

		htmlArray[htmlArrayIndex++] = '</ul>\n';
	}

	// カレンダーの表示月数分ループ
	for (var i = 0; i < this.DU.PCP.getCalendarLength(); i++) {
		htmlArray[htmlArrayIndex++] = '  <div class="calendar-box">\n';
		var dummyBaseDate = new Date(this.baseDate.getFullYear(),
				this.baseDate.getMonth() + i, 1);
		if (!this.DU.PCP.isSp()) {
		  //中止しない
		} else if (i < 1) {
		  //中止しない
		} else if (!this.DU.isValidDate(dummyBaseDate)) {
			break;
		}

		// 設定日付の年月取得（初期表示位置設定用）
		var dateValueClassElement = this.getDateFieldElement();
		var defDate = this.DU.createCalendarFromFormatVal(dateValueClassElement.value);
		var defDataYM = defDate.getFullYear() + ( '0' + (defDate.getMonth() + 1) ).slice( -2 ); 

		// 初期表示位置判定フラグ（SPモードで初期月でなく、かつ表示月と設定月が同じ場合）
		var topFlg = this.DU.PCP.isSp() && (i !== 0) && (defDataYM === (dummyBaseDate.getFullYear() + ( '0' + (dummyBaseDate.getMonth() + 1) ).slice( -2 )));

		htmlArray[htmlArrayIndex++] = '    <div class="monthly' + (topFlg ? ' selectMonth':'') + '" id="' + this.name + '_month_' + dummyBaseDate.getFullYear() + ( '0' + (dummyBaseDate.getMonth() + 1) ).slice( -2 ) + '">\n';
		htmlArray[htmlArrayIndex++] = '    <p class="month">' + this.PCLP.FormatYearMonth(dummyBaseDate); + '</p>\n';
		if (this.isDispAllOfMonth) {
			var allOfMonthDate = new Date(this.baseDate.getFullYear(), this.baseDate.getMonth() + i, this.baseDate.getDate());

			if (this.DU.isSelectableMonth(this, allOfMonthDate)) {
				var allOfMonthDate = new Date(this.baseDate.getFullYear(), this.baseDate.getMonth() + i, this.baseDate.getDate());
				htmlArray[htmlArrayIndex++] = '    <p class="period" onclick="m_tourModulePopupCalendar.cals[\'' + this.name + '\'].chgSetYM(' 				+ allOfMonthDate.getFullYear() + ', ' + allOfMonthDate.getMonth() + ')">'
				+ '<a href="javascript:void(0);">' + this.PCLP.FormatMonth(dummyBaseDate) + 'すべて</a></p>\n';
			} else {
				var allOfMonthDate = new Date(this.baseDate.getFullYear(), this.baseDate.getMonth() + i, this.baseDate.getDate());
				htmlArray[htmlArrayIndex++] = '    <p class="period periodHideButton" onclick="m_tourModulePopupCalendar.cals[\'' + this.name + '\'].chgSetYM(' 				+ allOfMonthDate.getFullYear() + ', ' + allOfMonthDate.getMonth() + ')">'
				+ '<a href="javascript:void(0);">' + this.PCLP.FormatMonth(dummyBaseDate) + 'すべて</a></p>\n';
			}
		}

		htmlArray[htmlArrayIndex++] = '    </div><!-- / .monthly -->\n';

		htmlArray[htmlArrayIndex++] = '    <table class="tbl-calendar">\n';
		htmlArray[htmlArrayIndex++] = '      <tbody><tr>\n';
		htmlArray[htmlArrayIndex++] = '        <th class="sun">' + this.PCLP.WeekList[0] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[1] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[2] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[3] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[4] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[5] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th class="sat">' + this.PCLP.WeekList[6] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '      </tr>';

		var baseDateMonthLastDate = new Date(dummyBaseDate.getFullYear(), dummyBaseDate.getMonth() + 1, 0);

		var date;
		var lastSaturday = baseDateMonthLastDate.getDate()
				+ (6 - baseDateMonthLastDate.getDay());
		var numOfMaxDate = lastSaturday + (6 - Math.ceil(lastSaturday / 7)) * 7;

		// 受付非活性後の受付開始日付を取得
		var enableStartDate = m_tourModuleDateUtil.prototype.calcEnableStartDate(this);

		// 受付開始日に対する受付終了日付を取得
		var enableEndDate =  m_tourModuleDateUtil.prototype.calcEnableEndDate(this, enableStartDate);

		// 1ヶ月の日数分ループ
		for (var j = 1; j <= numOfMaxDate; j++) {
			date = new Date(dummyBaseDate.getFullYear(), dummyBaseDate.getMonth(), j);
			if (j > baseDateMonthLastDate.getDate() && date.getDay() == 0) {
				break;
			} else if (j == 1 || date.getDay() == 0) {
				htmlArray[htmlArrayIndex++] = '<tr>\n';
				for (var k = 0; k < date.getDay(); k++) {
					htmlArray[htmlArrayIndex++] = '<td class="d_calEmpty"></td>\n';
				}
			}

			if (j <= baseDateMonthLastDate.getDate()) {
				var validDateFunction = '';
				var validDateFunctionEnd = '';

				if (this.DU.isSelectableDate(date) &&
					date.getTime() > enableStartDate.getTime() &&
					date.getTime() <= enableEndDate.getTime()) {

					htmlArray[htmlArrayIndex++] = '<td class="' + this.getCalendarCellStyle(date, currentDate) + '"'
					+ ' onclick="m_tourModulePopupCalendar.cals[\'' +  this.name + '\']'
					+ '.chgSet(' + date.getFullYear() + ', '
					+ date.getMonth() + ', ' + date.getDate()
					+ ')">';
					htmlArray[htmlArrayIndex++] = '<a href="javascript:void(0);" tabindex="-1">'
					htmlArray[htmlArrayIndex++] = date.getDate();
					htmlArray[htmlArrayIndex++] = '</a>';
					htmlArray[htmlArrayIndex++] = '</td>\n';
				} else {
					htmlArray[htmlArrayIndex++] = '<td class="' + this.getCalendarCellStyle(date, currentDate) + '">'
					+ validDateFunction + date.getDate()
					+ validDateFunctionEnd + '</td>\n';
				}

				if (date.getDay() == 6) {
					htmlArray[htmlArrayIndex++] = '</tr>\n';
				}
			} else {
				htmlArray[htmlArrayIndex++] = '<td class="d_calEmpty"></td>\n';
				if (date.getDay() == 6) {
					htmlArray[htmlArrayIndex++] = '</tr>\n';
				}
			}

		}

		htmlArray[htmlArrayIndex++] = '    </tbody></table>\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / [.calendar-box] -->\n';
	}

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .calendar-wrap -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .module-calendar -->\n';

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_cover -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_box -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_inner -->\n';

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_cover -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .modal-balloon_contents -->\n';

	return htmlArray.join('');
};

/**
 * <p>ポップアップカレンダーの中身(TOP用)を作成します。<br>
 * 与えられたbaseDateの年月を基準にカレンダーを作成します。</p>
 * @param {Date} baseDate  カレンダー表示基準日付
 */
m_tourModulePopupCalendar.prototype.createCalendarBodyTop = function(baseDate) {

	this.baseDate = baseDate;

	var htmlArray = new Array();
	var htmlArrayIndex = 0;

	htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_contents">\n';
	htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_title">\n';
	htmlArray[htmlArrayIndex++] = '    <h2 class="mod-balloon_ttl">' + this.balloonTitle + '</h2>\n';
	htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n';
	htmlArray[htmlArrayIndex++] = '  </div>\n';
	htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_cont_cover">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-cont_inner">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-set">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-set_cover mod-balloon-set_single">\n';
	htmlArray[htmlArrayIndex++] = '<div class="mod-balloon-set_box">\n';
	htmlArray[htmlArrayIndex++] = '\n';
	if (this.isDispAllOfMonth) {
		htmlArray[htmlArrayIndex++] = '<div id="module-calendar" class="is-period">\n';
	} else {
		htmlArray[htmlArrayIndex++] = '<div id="module-calendar">\n';
	}
	htmlArray[htmlArrayIndex++] = '<div id="calendar-wrap">\n';


	// 営業日付を取得
	var currentDate = m_tourModuleDateUtil.prototype.calcCurrentDate(this.boundaryTime);
	var currentMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
	var baseMonthMin = new Date(baseDate.getFullYear(), baseDate.getMonth(), 1);
	var baseMonthMax = new Date(baseDate.getFullYear(), baseDate.getMonth() + 2, 1);

	htmlArray[htmlArrayIndex++] = '<ul class="calendar-nav">\n';
	// 前へボタンの追加
	if (this.isEnableToMovePrevMonth(-(this.DU.PCP.getCalendarLength()))) {
		htmlArray[htmlArrayIndex++] = '    <li class="prev"><a tabindex="0" href="javascript:void(0);" class="calPrev" onclick="m_tourModulePopupCalendar.cals[\'' + this.name + '\'].chgPrev()" href="javascript:void(0);">\n';
		htmlArray[htmlArrayIndex++] = this.PCLP.PrevMonthFormat;
		htmlArray[htmlArrayIndex++] = '</a></li>\n';
	}

	htmlArray[htmlArrayIndex++] = '</ul>\n';

	// カレンダーの表示月数分ループ
	for (var i = 0; i < this.DU.PCP.getCalendarLength(); i++) {
		htmlArray[htmlArrayIndex++] = '  <div class="calendar-box">\n';
		var dummyBaseDate = new Date(this.baseDate.getFullYear(),
				this.baseDate.getMonth() + i, 1);
		if (!this.DU.PCP.isSp()) {
		  //中止しない
		} else if (i < 1) {
		  //中止しない
		} else if (!this.DU.isValidDate(dummyBaseDate)) {
			break;
		}

		htmlArray[htmlArrayIndex++] = '    <div class="monthly" id="' + this.name + '_month_' + dummyBaseDate.getFullYear() + ( '0' + (dummyBaseDate.getMonth() + 1) ).slice( -2 ) + '">\n';
		htmlArray[htmlArrayIndex++] = '    <p class="month">' + this.PCLP.FormatYearMonth(dummyBaseDate); + '</p>\n';
		if (this.isDispAllOfMonth) {
			var allOfMonthDate = new Date(this.baseDate.getFullYear(), this.baseDate.getMonth() + i, this.baseDate.getDate());

			if (this.DU.isSelectableMonth(this, allOfMonthDate)) {
				htmlArray[htmlArrayIndex++] = '    <p class="period" onclick="m_tourModulePopupCalendar.cals[\'' + this.name + '\'].chgSetYM(' 				+ allOfMonthDate.getFullYear() + ', ' + allOfMonthDate.getMonth() + ')">'
				+ '<a href="javascript:void(0);">' + this.PCLP.FormatMonth(dummyBaseDate) + 'すべて</a></p>\n';
			} else {
				htmlArray[htmlArrayIndex++] = '    <p class="period periodHideButton" onclick="m_tourModulePopupCalendar.cals[\'' + this.name + '\'].chgSetYM(' 				+ allOfMonthDate.getFullYear() + ', ' + allOfMonthDate.getMonth() + ')">'
				+ '<a href="javascript:void(0);">' + this.PCLP.FormatMonth(dummyBaseDate) + 'すべて</a></p>\n';
			}
		}

		htmlArray[htmlArrayIndex++] = '    </div><!-- / .monthly -->\n';

		htmlArray[htmlArrayIndex++] = '    <table class="tbl-calendar">\n';
		htmlArray[htmlArrayIndex++] = '      <tbody><tr>\n';
		htmlArray[htmlArrayIndex++] = '        <th class="sun">' + this.PCLP.WeekList[0] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[1] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[2] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[3] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[4] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th>' + this.PCLP.WeekList[5] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '        <th class="sat">' + this.PCLP.WeekList[6] + '</th>\n';
		htmlArray[htmlArrayIndex++] = '      </tr>';

		var baseDateMonthLastDate = new Date(dummyBaseDate.getFullYear(), dummyBaseDate.getMonth() + 1, 0);

		var date;
		var lastSaturday = baseDateMonthLastDate.getDate()
				+ (6 - baseDateMonthLastDate.getDay());
		var numOfMaxDate = lastSaturday + (6 - Math.ceil(lastSaturday / 7)) * 7;

		// 受付非活性後の受付開始日付を取得
		var enableStartDate = m_tourModuleDateUtil.prototype.calcEnableStartDate(this);

		// 受付開始日に対する受付終了日付を取得
		var enableEndDate =  m_tourModuleDateUtil.prototype.calcEnableEndDate(this, enableStartDate);

		// 1ヶ月の日数分ループ
		for (var j = 1; j <= numOfMaxDate; j++) {
			date = new Date(dummyBaseDate.getFullYear(), dummyBaseDate.getMonth(), j);
			if (j > baseDateMonthLastDate.getDate() && date.getDay() == 0) {
				break;
			} else if (j == 1 || date.getDay() == 0) {
				htmlArray[htmlArrayIndex++] = '<tr>\n';
				for (var k = 0; k < date.getDay(); k++) {
					htmlArray[htmlArrayIndex++] = '<td class="d_calEmpty"></td>\n';
				}
			}

			if (j <= baseDateMonthLastDate.getDate()) {
				var validDateFunction = '';
				var validDateFunctionEnd = '';

				if (this.DU.isSelectableDate(date) &&
					date.getTime() > enableStartDate.getTime() &&
					date.getTime() <= enableEndDate.getTime()) {

					htmlArray[htmlArrayIndex++] = '<td class="' + this.getCalendarCellStyle(date, currentDate) + '"'
					+ ' onclick="m_tourModulePopupCalendar.cals[\'' +  this.name + '\']'
					+ '.chgSet(' + date.getFullYear() + ', '
					+ date.getMonth() + ', ' + date.getDate()
					+ ')">';
					htmlArray[htmlArrayIndex++] = '<a href="javascript:void(0);" tabindex="-1">'
					htmlArray[htmlArrayIndex++] = date.getDate();
					htmlArray[htmlArrayIndex++] = '</a>';
					htmlArray[htmlArrayIndex++] = '</td>\n';
				} else {
					htmlArray[htmlArrayIndex++] = '<td class="' + this.getCalendarCellStyle(date, currentDate) + '">'
					+ validDateFunction + date.getDate()
					+ validDateFunctionEnd + '</td>\n';
				}

				if (date.getDay() == 6) {
					htmlArray[htmlArrayIndex++] = '</tr>\n';
				}
			} else {
				htmlArray[htmlArrayIndex++] = '<td class="d_calEmpty"></td>\n';
				if (date.getDay() == 6) {
					htmlArray[htmlArrayIndex++] = '</tr>\n';
				}
			}

		}

		htmlArray[htmlArrayIndex++] = '    </tbody></table>\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / [.calendar-box] -->\n';
	}

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .calendar-wrap -->\n';
	htmlArray[htmlArrayIndex++] = '<ul class="calendar-nav">\n';
	// 次へボタンの追加
	if (this.isEnableToMoveNextMonth(this.DU.PCP.getCalendarLength())) {
		htmlArray[htmlArrayIndex++] = '    <li class="next"><a tabindex="0" href="javascript:void(0);" class="calNext" onclick="m_tourModulePopupCalendar.cals[\'' +  this.name + '\'].chgNext()" href="javascript:void(0);">\n';
		htmlArray[htmlArrayIndex++] = this.PCLP.NextMonthFormat;
		htmlArray[htmlArrayIndex++] = '</a></li>\n';
	}
	// 今月ボタンの追加
	if (currentMonth.getTime() < baseMonthMin.getTime()
			|| currentMonth.getTime() > baseMonthMax.getTime()) {
			htmlArray[htmlArrayIndex++] = '<li class="current"><a tabindex="0" href="javascript:void(0);" class="calCurrent" onclick="m_tourModulePopupCalendar.cals[\'' +  this.name + '\'].chgCurrent()">今月</a></li>\n';
	}

	htmlArray[htmlArrayIndex++] = '</ul>\n';

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .module-calendar -->\n';

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_cover -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_box -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_inner -->\n';

	htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_cover -->\n';
	htmlArray[htmlArrayIndex++] = '  </div><!-- / .modal-balloon_contents -->\n';

	return htmlArray.join('');
};

/**
 * <p>ポップアップカレンダーの中身を作成します。<br>
 * 与えられたbaseDateの年月を基準にカレンダーを作成します。</p>
 * @param {Date} baseDate  カレンダー表示基準日付
 */
m_tourModulePopupCalendar.prototype.describeCalendar = function(baseDate) {

	this.baseDate = baseDate;

	var targetElement = document.getElementById(this.id);
	targetElement.style.display = 'inline';

	var calendarBase = this.createCalendarBase();
	var calendarBody = undefined;
	if (!_.isEmpty(this.site) && (this.site === 'inttourtop' || this.site === 'domtourtop')) {
		calendarBody = this.createCalendarBodyTop(baseDate);
	}else{
		calendarBody = this.createCalendarBody(baseDate);
	}

	$tour_module_jq('body').append(calendarBase);
	$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find('.modal-balloon_inner').append(calendarBody);
	$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find('.modal-balloon_inner').find('.periodHideButton').css('visibility','hidden');

	if(!this.isReceiveTrigger){
		// クリックイベントを飛ばす処理
		var bal = $tour_module_jq('#' + this.textId);
		bal.trigger('click');
	}

	// 選択日付の座標が取得できた場合、スクロール位置移動
	var titleHeight = $tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find(".mod-balloon_title").outerHeight(true);
	var selectMonthPosition = $tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find(".selectMonth").position();
	if (titleHeight !== undefined && selectMonthPosition !== undefined) {
		$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find(".modal-balloon_cover").scrollTop(selectMonthPosition.top + titleHeight);
	}	
	
	return true;
};

/**
 * <p>ポップアップカレンダーの中身を更新します。<br>
 * 与えられたbaseDateの年月を基準にカレンダーに更新します。</p>
 * @param {Date} baseDate  カレンダー表示基準日付
 */
m_tourModulePopupCalendar.prototype.updateCalendar = function(baseDate) {

	this.baseDate = baseDate;

	var calendarBody = undefined;
	if (!_.isEmpty(this.site) && (this.site === 'inttourtop' || this.site === 'domtourtop')) {
		calendarBody = this.createCalendarBodyTop(baseDate);
	}else{
		calendarBody = this.createCalendarBody(baseDate);
	}

	$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find('.modal-balloon_contents').remove();
	$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find('.modal-balloon_inner').append(calendarBody);
	$tour_module_jq('#cal_balloon_parts_' + this.uniqueKey).find('.modal-balloon_inner').find('.periodHideButton').css('visibility','hidden');

	return true;
};

/**
 * <p>プルダウンにmonthの情報を設定します。</p>
 * @param {Date} date
 */
m_tourModulePopupCalendar.prototype.setDatePulldownYM = function(date) {

	n_date = this.DU.getSystemDate();
	n_year = n_date.getYear();
	n_month = n_date.getMonth() + 1;

	// 1桁月日付に0をつける
	week = this.PCLP.WeekList[date.getDay()];
	month = date.getMonth() + 1;
	day = date.getDate();
	year = date.getFullYear();

	if (typeof this.DU.PCP.CalYear != 'undefined') {
		if (parseInt(n_month) > parseInt(month)) {
			var month = String(parseInt(this.DU.PCP.CalYear) + 1) + String(month);
		} else if(parseInt(n_month) == parseInt(month)) {
			if (parseInt(n_day) > parseInt(day)) {
				var month = String(parseInt(this.DU.PCP.CalYear) + 1) + String(month);
			} else {
				var month = String(this.DU.PCP.CalYear) + String(month);
			}
		} else {
			var month = String(this.DU.PCP.CalYear) + String(month);
		}
	}

    var pulldownMonthElement = this.getPullDownMonthElement();
    if (pulldownMonthElement) {
      this.getPullDownMonthElement().value = month;
    }

    var pulldownDayElement = this.getPullDownDayElement();
	if (pulldownDayElement) {
		this.getPullDownDayElement().value = day;
	}

    var pulldownDateFieldElement = this.getDateFieldElement();
	if (pulldownDateFieldElement) {
		this.getDateFieldElement().value = this.DU.createFormatCalendarYMVal(year,month - 1,day);
	}

	var tempDate = this.PCLP.FormatMonthText(new Date(year,month - 1,day));
	if (document.getElementById(this.textId).nodeName == 'INPUT') {
		document.getElementById(this.textId).value = tempDate;
		$tour_module_jq('#' + this.textId).trigger('change');
	} else {
		if(0 !== $tour_module_jq('#' + this.textId).find("span." + this.inputClass).length){
			$tour_module_jq('#' + this.textId).find("span." + this.inputClass).text(tempDate);
			$tour_module_jq('#' + this.textId).find("span." + this.inputClass).trigger('change');
		}else{
			document.getElementById(this.textId).innerHTML = tempDate;	
			$tour_module_jq('#' + this.textId).trigger('change');
		}
	}

	return true;
};

/**
 * <p>プルダウンにdateの日付を設定します。</p>
 * @param {Date} date
 */
m_tourModulePopupCalendar.prototype.setDatePulldown = function(date) {

	n_date = this.DU.getSystemDate();
	n_year = n_date.getYear();
	n_month = n_date.getMonth() + 1;
	n_day = n_date.getDate();

	// 1桁月日付に0をつける
	week = this.PCLP.WeekList[date.getDay()];
	month = date.getMonth() + 1;
	day = date.getDate();
	year = date.getFullYear();

	if (typeof this.DU.PCP.CalYear != 'undefined') {
		if (parseInt(n_month) > parseInt(month)) {
			var month = String(parseInt(this.DU.PCP.CalYear) + 1) + String(month);
		} else if(parseInt(n_month) == parseInt(month)) {
			if (parseInt(n_day) > parseInt(day)) {
				var month = String(parseInt(this.DU.PCP.CalYear) + 1) + String(month);
			} else {
				var month = String(this.DU.PCP.CalYear) + String(month);
			}
		} else {
			var month = String(this.DU.PCP.CalYear) + String(month);
		}
	}

    var pulldownMonthElement = this.getPullDownMonthElement();
    if (pulldownMonthElement) {
      this.getPullDownMonthElement().value = month;
    }

    var pulldownDayElement = this.getPullDownDayElement();
	if (pulldownDayElement) {
		this.getPullDownDayElement().value = day;
	}

    var pulldownDateFieldElement = this.getDateFieldElement();
	if (pulldownDateFieldElement) {
		this.getDateFieldElement().value = this.DU.createFormatCalendarVal(year,month - 1,day);
	}

	var tempDate = this.PCLP.FormatDate(new Date(year,month - 1,day));
	if (document.getElementById(this.textId).nodeName == 'INPUT') {
		document.getElementById(this.textId).value = tempDate;
		$tour_module_jq('#' + this.textId).trigger('change');
	} else {
		if(0 !== $tour_module_jq('#' + this.textId).find("span." + this.inputClass).length){
			$tour_module_jq('#' + this.textId).find("span." + this.inputClass).text(tempDate);
			$tour_module_jq('#' + this.textId).find("span." + this.inputClass).trigger('change');
		}else{
			document.getElementById(this.textId).innerHTML = tempDate;	
			$tour_module_jq('#' + this.textId).trigger('change');
		}
	}

	return true;
};

/**
 * <p>カレンダー上の前の月に移動が可能かどうかを判断する。</p>
 * 
 * @param {Number} value 移動する月数
 * @return {boolean} true:可能 false:不可
 */
m_tourModulePopupCalendar.prototype.isEnableToMovePrevMonth = function(value) {
	var tempDate = new Date(this.baseDate.getFullYear(), this.baseDate.getMonth(), 0);
	return this.DU.isValidDate(tempDate);
};

/**
 * <p>カレンダー上の次の月に移動が可能かどうかを判断する。</p>
 * @param {Number} value 移動する月数
 * @return {boolean} true:可能 false:不可
 */
m_tourModulePopupCalendar.prototype.isEnableToMoveNextMonth = function(value) {
	var tempDate = new Date(this.baseDate.getFullYear(), (this.baseDate.getMonth() + value), 1);
	return this.DU.isValidDate(tempDate);
};

/**
 * <p>指定された日付のstyle classを取得します。</p>
 * @param {Date} date
 * @return {String} styleclass名
 */
m_tourModulePopupCalendar.prototype.getCalendarCellStyle = function(date, currentDate) {
	var cellStyle = '';
	var defDate;
	var startDate;

	// 当日から開始日付の前日、受付開始後の非活性の日付の背景色を灰色に設定
	var receiptDisableDays = this.DU.calcReceiptDisableDays(this);
	var receiptDisableLastDay = new Date(currentDate.getFullYear(), currentDate.getMonth(),
		currentDate.getDate() + receiptDisableDays);

	// 受付非活性後の受付開始日付を取得
	var enableStartDate = this.DU.calcEnableStartDate(this);
	// 受付開始日に対する受付終了日付を取得
	var enableEndDate = this.DU.calcEnableEndDate(this, enableStartDate);

	if (receiptDisableLastDay.getTime() >= date.getTime() ||
		enableStartDate .getTime() >= date.getTime() ||
		enableEndDate.getTime() < date.getTime() ||
		!this.DU.isSelectableDate(date)) {

		if (currentDate.getTime() <= date.getTime()) {
			cellStyle += ' disabled';
		}
	}

	// 開始日付の背景色を設定
	if (this.startDateClass) {
		var startDateClassElement = this.DU.getDateClassElement(this, '.' + this.startDateClass);
		startDate = this.DU.createCalendarFromFormatVal(startDateClassElement.value);
		if (startDate.getTime() === date.getTime()) {
			cellStyle = ' select';
		}
	}

	// 中間日付除外フラグ
	var isExcludeMiddleDate = this.DU.isExcludeMiddleDate(this.stayCityCount);

	// 中間日付の背景色を設定
	if (this.middleDateClass && !isExcludeMiddleDate) {
		var middleDateClassElement = this.DU.getDateClassElement(this, '.' + this.middleDateClass);
		var middleDate = this.DU.createCalendarFromFormatVal(middleDateClassElement.value);
		if (middleDate.getTime() === date.getTime()) {
			cellStyle = ' select';
		}
	}
	// 選択状態の背景色を設定
	if (this.defDate) {
		var dateValueClassElement = this.getDateFieldElement();
		defDate = this.DU.createCalendarFromFormatVal(dateValueClassElement.value);
		// xx月すべてを選択した場合
		if (dateValueClassElement.value.length < 7) {
			if (this.DU.isSelectableDate(date) &&
				receiptDisableLastDay.getTime() < date.getTime() &&
				defDate.getFullYear() === date.getFullYear() &&
				defDate.getMonth() === date.getMonth()) {
				cellStyle = ' select';
			}
		// 年月日を選択した場合
		} else {
			if (defDate.getTime() === date.getTime()) {
				cellStyle = ' cursor';
			}
		}
	} else {
		if (this.defSelectDay) {
			defDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + this.defSelectDay);
		} else {
			defDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 30);
		}

		if (defDate.getTime() === date.getTime()) {
			cellStyle = ' select cursor';
		}
	}
	// 開始日付から選択日付までの背景色を設定
	if (this.startDateClass) {
		var startDateClassElement = this.DU.getDateClassElement(this, '.' + this.startDateClass);
		startDate = this.DU.createCalendarFromFormatVal(startDateClassElement.value);
		if (startDate.getTime() < date.getTime() &&
			defDate.getTime() > date.getTime()) {
			if (this.middleDateClass && !isExcludeMiddleDate) {
				var middleDateClassElement = this.DU.getDateClassElement(this, '.' + this.middleDateClass);
				var middleDate = this.DU.createCalendarFromFormatVal(middleDateClassElement.value);
				if (startDate.getTime() !== date.getTime() && middleDate.getTime() !== date.getTime()) {
					cellStyle = ' duration';
				}
			} else {
				if (startDate.getTime() !== date.getTime()) {
					cellStyle = ' duration';
				}
			}
		}
	}

	// 土日祝日の赤色表示
	if (this.DU.isHoliday(date)) {
		cellStyle += ' sun';
	} else {
		if (date.getDay() == 0) {
			cellStyle += ' sun';
		} else if (date.getDay() == 6) {
			cellStyle += ' sat';
		} else {
			cellStyle += '';
		}
	}

	return cellStyle;
};
