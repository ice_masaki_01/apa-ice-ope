/*=====================================================
* Script Name: domtour2nd_module_bulk.js
* Description: 空席照会モジュール 国内ツアー2nd 「旅作」+ツアー一括検索
* Version: 1.00
* Last Up Date: 2017/10/11
=====================================================*/

var m_domtour2ndModuleBulk_siteType = { site : 'domtour2nd', module : 'bulk' };

//初期値設定
var m_domtour2ndModuleBulk_defaults = {
		m_defDateDep:'',			//出発日用
		m_defDepArea:'001',			//出発地
		m_defNumberOfDays:'',		//旅行日数用
		m_defNumberOfPeople:'',		//人数用
		m_defTabNoDispCls:''		//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleBulk_optionParam = $tour_module_jq.extend(m_domtour2ndModuleBulk_optionParam, m_domtour2ndModuleBulk_defaults);

/**
 * 国内ツアー「旅作」+ツアー一括検索2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModuleBulk_setParams(options){
	m_domtour2ndModuleBulk_optionParam = $tour_module_jq.extend(m_domtour2ndModuleBulk_optionParam, options);
};

/**
 * 国内ツアー「旅作」+ツアー一括検索2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleBulk_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアー「旅作」+ツアー一括検索の各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModuleBulk_incorrectValue(options, m_domtour2ndModuleBulk_siteType, m_domtour2ndModuleBulk_defaults));
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//出発日
	var depDateText = m_tourModuleCommon_getListValue(m_domtour2ndModulebulk_depDate, 'Text');
	//出発地
	var depApoText = m_tourModuleCommon_getListValue(m_domtour2ndModulebulk_depApo, 'Text');
	//旅行日数
	var travelOfDaysText = m_tourModuleCommon_getListValue(m_domtour2ndModulebulk_travelOfDays, 'Text');
	//人数
	var numberOfPeopleText = m_tourModuleCommon_getListValue(m_domtour2ndModulebulk_numberOfPeople, 'Text');
	//検索ボタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModuleBulk_fromAction, 'URL');
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');
	
	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defFormMethod');
	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<form class="form-domBulk" id="form-domBulk" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<div class="mod-content_wrap">\n';
	html += '<div class="mod-content_cover col3">\n';
	
	//1カラム目 出発日 出発地
	html += '<div class="mod-content_box">\n';
	
	//出発日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depDateText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depDate">\n';
	//出発日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//出発地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depApoArea">\n';
	//出発地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//1カラム目 END
	
	
	//2カラム目 旅行日数 人数
	html += '<div class="mod-content_box">\n';
	
	//旅行日数
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + travelOfDaysText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_travelOfDays">\n';
	//旅行日数部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//人数
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + numberOfPeopleText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_numberOfPeople">\n';
	//人数部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 END
	
	//3カラム目 検索ボタン
	html += '<div class="mod-content_box mod-btn_box">\n';
	
	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '<div class="btn btn-search" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//3カラム目 END
	
	
	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</div><!-- / .mod-content_wrap -->\n';
	html += '</form>\n';
	html += '\n';

	this.append(html);
	
	m_domtour2ndModuleBulk_setDefVal(this, options);
	
};

function m_domtour2ndModuleBulk_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defModulePosition');
	// 出発地(方面、地区、地域)設定内容の補正（どれか一つでも設定されていたらundefinedの項目は空文字を設定する）
	if (options.m_defDepDistrict !== undefined ||
		options.m_defDepRegion !== undefined ||
		options.m_defDepArea !== undefined) {

		if (options.m_defDepDistrict === undefined) {
			options.m_defDepDistrict = '';
		}
		if (options.m_defDepRegion === undefined) {
			options.m_defDepRegion = '';
		}
		if (options.m_defDepArea === undefined) {
			options.m_defDepArea = '';
		}
	}
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	//パラメータ設定を取得
	//出発日
	 var defDateDep = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defDateDep');
	//出発地
	var defDepArea = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defDepArea');
	//旅行日数
	 var defNumberOfDays = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defNumberOfDays');
	//人数
	var defNumberOfPeople = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defNumberOfPeople');
	//再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModuleBulk_siteType, options, 'm_defTabNoDispCls');

	 
	//出発日
	target.find('.m_depDate').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtour2ndModulebulk_depDate, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defDateDep,
		defSelectDay : m_domtourModuleBulk_defSelectDay,
		dateValueClass : m_domtourModuleBulk_dateValueClass,
		dateValueName : m_domtourModuleBulk_dateValueName,
		inputPlaceholder : m_domtourModuleBulk_inputPlaceholder,
		inputClass : m_domtourModuleBulk_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModuleBulk_isRequired,
		isDisabled : m_domtourModuleBulk_isDisabled,
		isDispAllOfMonth : m_domtourModuleBulk_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleBulk_prevNextInterval,
		receiptDisableDays : m_domtourModuleBulk_receiptDisableDays,
		dispMonths : m_domtourModuleCommon_defDispMonths,
		selectableMonths : m_domtourModuleCommon_defSelectableMonths,
		isDispHoliday : m_domtourModuleBulk_isDispHoliday,
		boundaryTime : m_domtourModuleBulk_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//出発地
	target.find('.m_depApoArea').tourCommonModule('createAirportInput', {
		parent : this,
		defAirportCode : defDepArea,
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleBulk_depApo['Text'],
		areaClass : 'm_hiddenBulkDepDistrict',
		countryClass : 'm_hiddenBulkDepRegion',
		apoCodeClass : 'm_hiddenBulkDepArea',
		apoCodeName : 'deptAreaCd',
		inputPlaceholder : m_domtour2ndModuleBulk_depApo['Placeholder'],
		inputClass : 'm_bulkDepApoText',
		isRequired : false,
		isDisabled : false,
		layoutType : '27',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType : '0'
	});
	
	//旅行日数
	target.find('.m_travelOfDays').tourCommonModule('createTravelOfDays', {
		site : m_domtour2ndModuleBulk_siteType.site,
		module : m_domtour2ndModuleBulk_siteType.module,
		defSelect : defNumberOfDays,
		inputClass:'',
		inputId:'dom_bulk_days',
		inputName:'stayDaysCnt'
	});
	
	//人数
	target.find('.m_numberOfPeople').tourCommonModule('createPeoplesPull', {
		site : m_domtour2ndModuleBulk_siteType.site,
		module : m_domtour2ndModuleBulk_siteType.module,
		defSelect : defNumberOfPeople,
		inputClass:'',
		inputId:'dom_bulk_member',
		inputName:'usePersonCnt'
	});
	
	// 検索ボタン押下イベント登録：サブミット実行
	target.find('.btn-search').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			$tour_module_jq(this).domtourCommonModule('submitSearchBulk', {
				targetModule : target
			});
		}
	});
	
	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}
	
	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});
	
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 1);
	m_tourCommonModuleParts_setPartsCount('calendar', 1);
	
};



$tour_module_jq.fn.domtour2ndModuleBulk = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleBulk_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleBulk_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleBulk');
	}
};
