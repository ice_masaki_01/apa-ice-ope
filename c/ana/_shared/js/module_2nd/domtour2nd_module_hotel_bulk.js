/*=====================================================
* Script Name: domtour2nd_module_hotel_bulk.js
* Description: 空席照会モジュール 国内ツアー2nd ホテル(3社一括)
* Version: 1.00
* Last Up Date: 2018/01/19
=====================================================*/

var m_domtour2ndModuleHotelBulk_siteType = { site : 'domtour2nd', module : 'hotel_bulk' };

var m_domtour2ndModuleHotelBulk_usePartsCount_PeoplePulldown = 1;			//人数、配置数カウンタ
var m_domtour2ndModuleHotelBulk_usePartsCount_AddConditionCheckBox = 1;		//こだわり条件部品、配置数カウンタ

//初期値設定
var m_domtour2ndModuleHotelBulk_defaults = {
	m_defCheckin:'',					//ご利用日用
	m_defStayDays:'1',					//泊数用
	m_defPlaceOfStayDistrict:'',		//宿泊地用(方面)
	m_defPlaceOfStayRegion:'',			//宿泊地用(地区)
	m_defPlaceOfStayArea:'',			//宿泊地用(地域)
	m_defNumberOfPeople:'',				//人数
	m_defAddConditionChecked:[''],		//こだわり条件用
	m_defTabNoDispCls:''				//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleHotelBulk_optionParam = $tour_module_jq.extend(m_domtour2ndModuleHotelBulk_optionParam, m_domtour2ndModuleHotelBulk_defaults);

/**
 * 国内ツアーホテル2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModuleHotelBulk_setParams(options){
	m_domtour2ndModuleHotelBulk_optionParam = $tour_module_jq.extend(m_domtour2ndModuleHotelBulk_optionParam, options);
};

/**
 * 国内ツアーホテル2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleHotelBulk_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアーホテルの各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModuleHotelBulk_incorrectValue(options, m_domtour2ndModuleHotelBulk_siteType, m_domtour2ndModuleHotelBulk_defaults));
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//ご利用日
	var checkText = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_useDays, 'Text');
	//泊数
	var stayDaysText = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_stayDays, 'Text');
	//宿泊地
	var placeOfStaText = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_placeOfStay, 'Text');
	//1部屋あたりの利用人数
	var roomOfPeopleLabelText = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_roomOfPeopleLabel, 'Text');
	//人数
	var peopleText = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_people, 'Text');
	//こだわり条件
	var addConditionText1 = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_addCondition, 'Text1');
	var addConditionText2 = m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_addCondition, 'Text2');
	//検索ホタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_fromAction, 'URL');
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');
	
	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defFormMethod');
	
	//国内ホテル三社一括検索
	var infoText = m_tourModuleCommon_getListValue(m_domtour2ndModuleHotel_infoText, 'Text');
	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<form  class="form-domHotelBulk" id="form-domHotelBulk" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<div class="mod-content_wrap">\n';
	html += '<div class="mod-content_cover col3">\n';
	
	//1カラム目 ご利用日 泊数
	html += '<div class="mod-content_box">\n';
	
	//ご利用日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + checkText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_checkin">\n';
	//ご利用日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//泊数
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + stayDaysText + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_checkout">\n';
	//泊数部品埋め込み
	html += '</div>\n';
	html += '<input type="hidden" class="m_checkout_send_param" value="">\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//1カラム目 END
	
	
	//2カラム目 宿泊地 1部屋あたりの利用人数
	html += '<div class="mod-content_box">\n';
	
	//宿泊地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + placeOfStaText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_placeOfStay">\n';
	//宿泊地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//人数
	html += '<fieldset class="mod-input_box dom_hotel_member_box">\n';
	html += '<legend><span class="mod-input_head bold">' + roomOfPeopleLabelText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_people">\n';
	//人数部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 END
	
	
	//3カラム目 こだわり条件 検索ボタン
	html += '<div class="mod-content_box mod-btn_box">\n';
	
	//こだわり条件
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head isSPvtop bold">' + addConditionText1 + '<span class="ib bold">' + addConditionText2 + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_addCondition">\n';
	//こだわり条件部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '    <div class="btn btn-search" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//3カラム目 END

	
	html += '</div><!-- / .mod-content_cover -->\n';
	
	html += '<p class="mod-dom-hotel_info">'+infoText+'</p>';
	html += '<img src="/_shared/images/root/common/ico_mod_anasales.png" alt="" class="poweredtxt">\n';
	html += '</div><!-- / .mod-content_wrap -->\n';
	html += '</form>\n';
	html += '\n';
	html += '\n';

	this.append(html);
	
	m_domtour2ndModuleHotelBulk_setDefVal(this, options);
	
};

function m_domtour2ndModuleHotelBulk_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defModulePosition');
	// 宿泊地(方面、地区、地域)設定内容の補正（どれか一つでも設定されていたらundefinedの項目は空文字を設定する）
	if (options.m_defPlaceOfStayDistrict !== undefined ||
		options.m_defPlaceOfStayRegion !== undefined ||
		options.m_defPlaceOfStayArea !== undefined) {

		if (options.m_defPlaceOfStayDistrict === undefined) {
			options.m_defPlaceOfStayDistrict = '';
		}
		if (options.m_defPlaceOfStayRegion === undefined) {
			options.m_defPlaceOfStayRegion = '';
		}
		if (options.m_defPlaceOfStayArea === undefined) {
			options.m_defPlaceOfStayArea = '';
		}
	}
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	//パラメータ設定を取得
	//ご利用日
	var defCheckin = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defCheckin');
	//泊数
	var defStayDays = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defStayDays');
	//宿泊地(方面)
	var defPlaceOfStayDistrict = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defPlaceOfStayDistrict');
	//宿泊地(地区)
	var defPlaceOfStayRegion = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defPlaceOfStayRegion');
	//宿泊地(地域)
	var defPlaceOfStayArea = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defPlaceOfStayArea');
	//人数
	var defNumberOfPeople = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defNumberOfPeople');
	//こだわり条件
	var defAddConditionChecked = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defAddConditionChecked');
	//再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModuleHotelBulk_siteType, options, 'm_defTabNoDispCls');
	
	//ご利用日
	target.find('.m_checkin').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleHotelBulk_useDays, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defCheckin,
		defSelectDay : m_domtourModuleHotelBulk_checkin_defSelectDay,
		dateValueClass : m_domtourModuleHotelBulk_checkin_dateValueClass,
		dateValueName : m_domtourModuleHotelBulk_checkin_dateValueName,
		inputPlaceholder : m_domtourModuleHotelBulk_checkin_inputPlaceholder,
		inputClass : m_domtourModuleHotelBulk_checkin_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModuleHotelBulk_checkin_isRequired,
		isDisabled : m_domtourModuleHotelBulk_checkin_isDisabled,
		isDispAllOfMonth : m_domtourModuleHotelBulk_checkin_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleHotelBulk_checkin_prevNextInterval,
		receiptDisableDays : m_domtourModuleHotelBulk_checkin_receiptDisableDays,
		dispMonths : m_domtourModuleHotelBulk_checkin_dispMonths,
		selectableMonths : m_domtourModuleHotelBulk_checkin_selectableMonths,
		isDispHoliday : m_domtourModuleHotelBulk_checkin_isDispHoliday,
		boundaryTime : m_domtourModuleHotelBulk_checkin_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//泊数
	target.find('.m_checkout').tourCommonModule('createTravelOfDays',{
		site:m_domtour2ndModuleHotelBulk_siteType.site,
		module : m_domtour2ndModuleHotelBulk_siteType.module,
		defSelect : defStayDays,
		inputId : 'dom_hotel_bulk_days',
		inputName : 'stayDaysCnt',
		inputClass : m_domtourModuleHotelBulk_checkout_inputClass,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH,
	});
	
	//宿泊地
	target.find('.m_placeOfStay').tourCommonModule('createAirportInput', {
		parent : this,
		defAreaCode : defPlaceOfStayDistrict,                   // 方面
		defCountryCode : defPlaceOfStayRegion,                  // 地区
		defAirportCode : defPlaceOfStayArea,                    // 地域
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleHotelBulk_stay['Text'],
		areaClass : 'm_hiddenHotelBulkDistrict',                // 方面
		areaName : 'districtCd',                                // 方面
		countryClass : 'm_hiddenHotelBulkRegion',               // 地区
		countryName : 'regionCd',                               // 地区
		apoCodeClass : 'm_hiddenHotelBulkArea',                 // 地域
		apoCodeName : 'areaCd',                                 // 地域
		inputPlaceholder : m_domtour2ndModuleHotelBulk_stay['Placeholder'],
		inputClass : 'm_domHotelBulkPlaceOfStayText',
		isRequired : false,
		isDisabled : false,
		layoutType : '28',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType : '2'
	});
	
	//人数
	target.find('.m_people').tourCommonModule('createPeoplesPull', {
		site : m_domtour2ndModuleHotelBulk_siteType.site,
		module : m_domtour2ndModuleHotelBulk_siteType.module,
		defSelect : defNumberOfPeople,
		inputClass:'m_dom_hotel_bulk_member',
		inputId:'dom_hotel_bulk_member' + m_tour2ndModuleHotelBulk_returnCount_AddPeoplePulldown(),
		inputName:'usePersonCnt'
	});
	
	//こだわり条件
	target.find('.m_addCondition').tourCommonModule('createCheckboxList', {
		site:m_domtour2ndModuleHotelBulk_siteType.site,
		module : m_domtour2ndModuleHotelBulk_siteType.module,
		defCheck : defAddConditionChecked,
		uniqueKey : m_tour2ndModuleHotelBulk_returnCount_AddConditionCheckBox() + '_bulk',
		inputName:'m_dom_hotel_bulk_option'
	});
	
	// 検索ボタン押下イベント登録：サブミット実行
	target.find('.btn-search').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			// 検索サブミット
			$tour_module_jq(this).domtourCommonModule('submitSearchHotelBulk', {
				targetModule : target
			});
		}
	});
	
	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}
	
	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 1);
	m_tourCommonModuleParts_setPartsCount('calendar', 2);
};

//こだわり条件チェックボックスリスト配置数カウント
function m_tour2ndModuleHotelBulk_returnCount_AddConditionCheckBox(){
	
	var returnCount = m_domtour2ndModuleHotelBulk_usePartsCount_AddConditionCheckBox;
	m_domtour2ndModuleHotelBulk_usePartsCount_AddConditionCheckBox++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

//大人配置数カウント
function m_tour2ndModuleHotelBulk_returnCount_AddPeoplePulldown(){
	
	var returnCount = m_domtour2ndModuleHotelBulk_usePartsCount_PeoplePulldown;
	m_domtour2ndModuleHotelBulk_usePartsCount_PeoplePulldown++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

$tour_module_jq.fn.domtour2ndModuleHotelBulk = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleHotelBulk_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleHotelBulk_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleHotelBulk');
	}
};
