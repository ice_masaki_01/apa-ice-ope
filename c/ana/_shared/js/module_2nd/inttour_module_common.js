/*=====================================================
* Script Name: inttour_module_common.js
* Description: 空席照会モジュール 海外ツアー共通処理
* Version: 1.2
* Last Up Date: 2017/11/10
=====================================================*/

//////////////////////////////
// 海外ツアー 共通処理 START

// 過渡期かどうかのチェック処理
// 過渡期の定義情報を読み込んで過渡期フラグがON（過渡期中）の場合はtrue、
// 過渡期フラグがOFF、または定義情報自体が無い（過渡期中でない）場合はfalseを返す。
function m_inttourModuleCommon_isTransitionPeriod() {
	if ('m_inttourTransitionPeriodOptions' in window && m_inttourTransitionPeriodOptions.isTransitionPeriod) {
		return true;
	}
	return false;
}

//指定文字列の配列内存在チェック
function m_inttourModuleCommon_getStrArrayMatchAll(targetStr, arrayStr){
	if($tour_module_jq.inArray(targetStr, arrayStr) != -1){
		return true;
	}
	return false;
};

// 海外ツアー 共通処理 END
//////////////////////////////

//////////////////////////////
// 海外旅作 共通処理 START

//初期値設定
var m_inttourModuleDp_siteType = { site : 'inttour', module : 'tabisaku' };
var m_inttourModuleDp_defaults = {
	isExist : {
		stay : true,							// 都市滞在ラジオ
		secondDestCode : true,					// 2都市目
		transferDeptDate : true,				// 移動日
		numberOfPeople : true					// 参加人数
	},
	targetModule : undefined,
	numberOfPeopleModuleNo : '999',
	numberOfPeopleTextId : m_tourModuleNumberOfPeople_defaultParams.textId,
	numberOfPeopleColumn : m_tourModuleNumberOfPeople_defaultParams.column,
	numberOfPeopleRow : m_tourModuleNumberOfPeople_defaultParams.row,
	numberOfRoomsInputClass : 'm_numberOfRooms',
	numberOfPeopleTextValue : ''
};

// 海外旅作 検索実行関数
// リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_inttourModuleDp_submitSearch(obj) {
	var target = obj.targetModule;
	var isExistDef =  m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, {}, 'isExist');
	var isExist = $tour_module_jq.extend(isExistDef, obj.isExist);
	var nopModuleNo = m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, obj, 'numberOfPeopleModuleNo');
	var nopTextId = m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, obj, 'numberOfPeopleTextId');
	var nopColumn = m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, obj, 'numberOfPeopleColumn');
	var nopRow = m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, obj, 'numberOfPeopleRow');
	var norInputClass = m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, obj, 'numberOfRoomsInputClass');
	var numberOfPeopleTextValue = m_tourModuleCommon_getParam(m_inttourModuleDp_siteType, obj, 'numberOfPeopleTextValue');
	
	var targetForm = $tour_module_jq(this).closest('.form-tabisaku');
	
	// 参加人数エラー再チェック
	if(isExist.numberOfPeople){
		m_inttourModuleDp_lastCheckValueNumberOfPeople(
			target,
			nopTextId+ nopModuleNo,
			nopColumn,
			nopRow,
			nopModuleNo
		);
	}
	// リクエストパラメータ調整
	m_inttourModuleDp_arrangeRequest({
		isExist : isExist,
		targetForm : targetForm,
		numberOfRoomsInputClass : norInputClass,
		numberOfPeopleModuleNo : nopModuleNo,
		numberOfPeopleTextId : nopTextId,
		numberOfPeopleColumn : nopColumn,
		numberOfPeopleRow : nopRow,
		numberOfPeopleTextValue : numberOfPeopleTextValue
	});
	// 検索実行
	targetForm.submit();
}

//検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_inttourModuleDp_arrangeRequest = function (option){
	var isExist = option.isExist;
	var targetForm = option.targetForm;
	
	//////////////////////
	// 都市滞在調整 START
	if(isExist.stay){
		var selectedRadioValue = targetForm.find('input[name="m_stay"]:checked').val();
		
		// 1都市滞在選択時、到着地(2都市目)と移動日のパラメータを除外
		if (selectedRadioValue === m_inttourModuleDp_stayCityCountList[0].value) {
			if(isExist.secondDestCode){
				targetForm.find('input[name="secondDestCode"]').prop('disabled', true);
			}
			if(isExist.transferDeptDate){
				targetForm.find('input[name="transferDeptDate"]').prop('disabled', true);
			}
		} else {
			targetForm.find('input[name="secondDestCode"]').prop('disabled', false);
			targetForm.find('input[name="transferDeptDate"]').prop('disabled', false);
		}
	}
	
	// 都市滞在調整 END
	//////////////////////

	//////////////////////
	// 参加人数 START
	if(isExist.numberOfPeople){
		var selectRoomsValue = targetForm.find('.' + option.numberOfRoomsInputClass).val();
		var pplModuleNo = option.numberOfPeopleModuleNo;
		var columnArray = option.numberOfPeopleColumn;
		var columnMax = columnArray.length;
		var rowArray = option.numberOfPeopleRow;
		var rowMax = rowArray.length;
		var numberOfPeopleTextValue = option.numberOfPeopleTextValue;
		// 参加人数リクエストパラメータの調整
		// ・参加人数未選択時、参加人数のパラメータを送信する
		// ・指定部屋数分のみリクエストパラメータを送信
		// ・部屋数毎のエラーチェック状態を参照し、エラー状態の部屋分のパラメータは除外
		var isErrorNumberOfPeopleList = targetForm.find('#' + option.numberOfPeopleTextId + pplModuleNo).data('isErrorRoomList');
		if(isErrorNumberOfPeopleList === undefined){
			// 画面で参加人数の操作をしなかった場合、エラーリストを初期化
			isErrorNumberOfPeopleList = new Array(rowMax);
			for(var ri = 0; ri < rowMax; ri++){
				isErrorNumberOfPeopleList[ri] = false;
			}
		}
		for(var ri = 0; ri < rowMax; ri++){
			if(selectRoomsValue < (ri + 1) || isErrorNumberOfPeopleList[ri]){
				targetForm.find('.'+ rowArray[ri].name + pplModuleNo).prop('disabled', true);
			} else {
				targetForm.find('.'+ rowArray[ri].name + pplModuleNo).prop('disabled', false);
			}
		}
	}
	// 参加人数 END
	//////////////////////
}

//参加人数入力値の最終チェック処理
function m_inttourModuleDp_lastCheckValueNumberOfPeople(target, nopInputTextId, nopColumn, nopRow, moduleNo){
	var nopRowMax = nopRow.length;
	var nopHiddenSelector = '';
	for(var ri = 0; ri < nopRowMax; ri++){
		if(ri != 0){
			nopHiddenSelector += ',';
		}
		nopHiddenSelector += '.' + nopRow[ri].name + moduleNo;
	}
	var nopHiddenObj = target.find(nopHiddenSelector);
	m_inttourModuleDp_checkValueNumberOfPeople( nopHiddenObj, nopInputTextId, nopColumn ,nopRow);
}

// 参加人数入力値のチェック処理
// エラーの場合はエラー番号を返却する。
// targetInput:参加人数吹き出し内のセレクト要素
// inputId:吹き出し呼び出し元のinput要素のid
function m_inttourModuleDp_checkValueNumberOfPeople(targetInput, inputId, column, row) {
	var errNo = undefined;

	var maxPeopleAll = m_inttourModuleDp_errorCheckNumberOfPeopleMax.all;         // 全部屋合計(大人+子供(ベッド有り))最大8名
	var maxPeople1Room = m_inttourModuleDp_errorCheckNumberOfPeopleMax.roomEach;  // 1部屋合計(大人+子供(ベッド有り))最大6名
	var maxPeopleNoBed = m_inttourModuleDp_errorCheckNumberOfPeopleMax.noBed;     // ベッドなし人数合計(子供(ベッドなし)+幼児)最大2名

	var cntPeopleAll = 0;
	var columnArray = column;
	var columnMax = columnArray.length;
	var rowArray = row;
	var rowMax = rowArray.length;
	var cntPeople1RoomAdultChild1 = new Array(rowMax);	// 各部屋毎の大人+子供(ベッド有り)の集計
	var cntPeople1RoomChild2infant = new Array(rowMax);	// 各部屋毎の子供(ベッドなし)+幼児の集計
	var isErrorRoomList = new Array(rowMax); 			// true:エラー検出、false:エラーなし
	for(var ri = 0; ri < rowMax; ri++){
		cntPeople1RoomAdultChild1[ri] = 0;
		cntPeople1RoomChild2infant[ri] = 0;
		isErrorRoomList[ri] = false;
	}
	
	var checkArea = undefined;

	$tour_module_jq(targetInput).each(function(idx) {
		var selectStr = $tour_module_jq(this).attr('id');		// 吹き出し表示時選択肢から取得
		if(selectStr === undefined){
			selectStr = $tour_module_jq(this).attr('name');	// 吹き出し非表示時または検索時、hiddenから取得
		}
		var inputValue = undefined;
		// 指定要素の値取得
		inputValue = parseInt($tour_module_jq(this).val());
		if(isNaN(inputValue)){
			inputValue = parseInt($tour_module_jq(this).find('li.mod-current').data('val')); // TOP
		}
		if(isNaN(inputValue)){
			errNo = 0;	// 参加人数入力値不正エラー
			return false;
		}
		// 1行毎の合計
		for(var ri = 0; ri < rowMax; ri++){
			if(selectStr.indexOf(rowArray[ri].name) >= 0){
				for(var ci = 0; ci < columnMax; ci++){
					if(selectStr.indexOf(columnArray[ci].name) >= 0){
						if (!columnArray[ci].sumColumn) {
							// 全体の合計をカウント
							cntPeopleAll += inputValue;
							if(columnArray[ci].sum){
								cntPeople1RoomAdultChild1[ri] += inputValue;
							} else {
								// 子供(ベッドなし)+幼児のカウント
								cntPeople1RoomChild2infant[ri] += inputValue;
							}
						}
						break;
					}
				}
			}
		}
	});
	// 正常値取得後エラーチェック開始
	if(errNo === undefined || !errNo === 0){
		// ８人より多い人数を選択
		if(cntPeopleAll > maxPeopleAll){
			errNo = 1;
		}
		// １部屋に６より多い人数を選択
		for(var ri = 0; ri < rowMax; ri++){
			if(cntPeople1RoomAdultChild1[ri] > maxPeople1Room){
				errNo = 1;
				isErrorRoomList[ri] = true;
			}
		}
		// 到着地のエリア間共通チェック
		// １部屋で子供(ベッドなし)+幼児の人数が２人より多い
		for (var ri = 0; ri < rowMax; ri++) {
			if (cntPeople1RoomChild2infant[ri] > maxPeopleNoBed) {
				errNo = 1;
				isErrorRoomList[ri] = true;
			}
		}
		// 到着地のエリア毎の参加人数チェック
		// エリア->(大人+子供(ベッドあり))->(子供(ベッドなし)+幼児)の順でリストからチェックフラグを取得
		checkArea = $tour_module_jq('#' + inputId).closest('.form-tabisaku').find('.m_arrArea1').val();
		if(checkArea !== undefined){
			// エリアで取得
			var checkAreaList1 = m_inttourModuleDp_errorCheckNumberOfPeopleArea[checkArea];
			if(checkAreaList1 !== undefined){
				for(var ri = 0; ri < rowMax; ri++){
					var isError = false;
					// 大人+子供(ベッドあり)で取得(選択している部屋数までチェック)
					var checkAreaList2 = checkAreaList1[cntPeople1RoomAdultChild1[ri]];
					if(checkAreaList2 !== undefined){
						// 子供(ベッドなし)+幼児で取得
						var checkAreaList3 = checkAreaList2[cntPeople1RoomChild2infant[ri]];
						if(checkAreaList3 !== undefined){
							isError = checkAreaList3;
						}else{
							isError = true;
						}
					}else{
						isError = true;
					}
					// エラー判定
					if(isError){
						errNo = 1;
						isErrorRoomList[ri] = true;
					}
				}
			}
		}
	}
	// エラーチェックリストを呼び出し元input要素に保存
	$tour_module_jq('#' + inputId).data().isErrorRoomList = isErrorRoomList;

	return errNo;
}

// 海外ツアー旅作の各項目不正値チェック処理
// options:チェック対象の各値
// siteType:各モジュール判定、setParam取得用
// defaults:各モジュールデフォルト値
// return:options(チェック後)
function m_inttourModuleDp_incorrectValue(options, siteType, defaults) {
	
	// 座席クラス
	var cfCheck = [];
	for(cf=0;cf<m_inttourModuleDp_classFareList['default'].length;cf++){
		cfCheck[cf] = m_inttourModuleDp_classFareList['default'][cf]['value'];
	}

	// 部屋数
	var norlCheck = [];
	for(no=0;no<m_inttourModuleDp_numberOfRoomsList.length;no++){
		norlCheck[no] = m_inttourModuleDp_numberOfRoomsList[no]['value'];
	}

	// 参加人数チェック
	var nplpCheck = [];
	var nplpList = m_inttourModuleNumberOfPeople_NPLP.List;
	var nplpStartValue = undefined;
	var nplpEndValue = undefined;

	for(np=0;np<nplpList.length-1;np++){
		// 合計値は含めずにループ
		nplpStartValue = parseInt(nplpList[np][0]['value']);
		nplpEndValue = parseInt(nplpList[np][nplpList[np].length-1]['value']);
		// チェック用の正規表現を配列に格納
		nplpCheck[np] = new RegExp("[" + nplpStartValue + "-" + nplpEndValue + "]");
	}

	// 不正値チェック設定
	var m_inttourModuleDp_checkedArray = {
			m_defStayCityCount:['0','1'],				// 都市滞在ラジオボタン
			m_defAirportDep:/[A-Z][A-Z][A-Z]/,			// 出発地
			m_defAirportArr1:/[A-Z][A-Z][A-Z]/,			// 到着地（1都市目）
			m_defAirportArr2:/[A-Z][A-Z][A-Z]/,			// 到着地（2都市目）
			m_defClassFare:cfCheck,						// 座席クラス
			m_defIsUpgradeAwardObject:['checked',''],	// アップグレード特典クラス検索チェック
			m_defNumberOfRooms:norlCheck,				// 部屋数
			m_defNumberOfPeopleInit:nplpCheck,			// 参加人数×８部屋 初回表示値
			m_defNumberOfPeopleClear:nplpCheck			// 参加人数×８部屋 吹き出し内入力項目初期化用
	}
	
	// 都市滞在ラジオボタン
	var checkStayCityCount = m_tourModuleCommon_getParam(siteType, options, 'm_defStayCityCount');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkStayCityCount, m_inttourModuleDp_checkedArray['m_defStayCityCount']) === false){
		options['m_defStayCityCount'] = defaults['m_defStayCityCount'];
	}
	// 出発地
	var checkAirportDep = m_tourModuleCommon_getParam(siteType, options, 'm_defAirportDep');
	if(!m_inttourModuleDp_checkedArray['m_defAirportDep'].test(checkAirportDep)){
		options['m_defAirportDep'] = defaults['m_defAirportDep'];
	}
	// 到着地(1都市目)
	var checkAirportArr1 = m_tourModuleCommon_getParam(siteType, options, 'm_defAirportArr1');
	if(!m_inttourModuleDp_checkedArray['m_defAirportArr1'].test(checkAirportArr1)){
		options['m_defAirportArr1'] = defaults['m_defAirportArr1'];
	}
	// 到着地(2都市目)
	var checkAirportArr2 = m_tourModuleCommon_getParam(siteType, options, 'm_defAirportArr2');
	if(!m_inttourModuleDp_checkedArray['m_defAirportArr2'].test(checkAirportArr2)){
		options['m_defAirportArr2'] = defaults['m_defAirportArr2'];
	}
	// 座席クラス
	var checkClassFare = m_tourModuleCommon_getParam(siteType, options, 'm_defClassFare');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkClassFare, m_inttourModuleDp_checkedArray['m_defClassFare']) === false){
		options['m_defClassFare'] = defaults['m_defClassFare'];
	}
	// アップグレード特典対象チェックボックス初期値
	var checkIsUpgradeAwardObjectChecked = m_tourModuleCommon_getParam(siteType, options, 'm_defIsUpgradeAwardObject');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkIsUpgradeAwardObjectChecked, m_inttourModuleDp_checkedArray['m_defIsUpgradeAwardObject']) === false){
		options['m_defIsUpgradeAwardObject'] = defaults['m_defIsUpgradeAwardObject'];
	}
	// 部屋数
	var checkNumberOfRooms = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfRooms');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkNumberOfRooms, m_inttourModuleDp_checkedArray['m_defNumberOfRooms']) === false){
		options['m_defNumberOfRooms'] = defaults['m_defNumberOfRooms'];
	}
	// 参加人数
	var checkNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfPeopleInit');
	options['m_defNumberOfPeopleInit'] = m_inttourModuleDp_incorrectValueNumberOfPeople(checkNumberOfPeopleSelectInit, m_inttourModuleDp_checkedArray['m_defNumberOfPeopleInit']);
	
	var checkNumberOfPeopleSelectClear = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfPeopleClear');
	options['m_defNumberOfPeopleClear'] = m_inttourModuleDp_incorrectValueNumberOfPeople(checkNumberOfPeopleSelectClear, m_inttourModuleDp_checkedArray['m_defNumberOfPeopleClear']);
	
	return options;
}

function m_inttourModuleDp_incorrectValueNumberOfPeople(numberOfPeople, checkedArray) {
	// 1部屋から8部屋まで
	if(numberOfPeople !== undefined){
		var checkNOP = checkedArray;
		for(i=0;i<numberOfPeople.length;i++){
			for(j=0;j<numberOfPeople[i].length;j++){
				var checkInitValue = numberOfPeople[i][j];
				if($tour_module_jq.isArray(checkNOP) && !checkNOP[j].test(checkInitValue)){
					// initは初期値なしのため常にclearNumを使用
					numberOfPeople[i][j] = m_tourModuleNumberOfPeople_defaultParams['clearNum'][i][j];
				}
			}
		}
	}
	return numberOfPeople;
}

// 海外旅作 共通処理 END
//////////////////////////////

//////////////////////////////////
//海外パッケージ 共通処理 START

//初期値設定
var m_inttourModulePkg_siteType = { site : 'inttour', module : 'pack' };
var m_inttourModulePkg_defaults = {
	isExist : {
		// 行き先から探す
		departureDateType : true,	//選択ラジオ
		depDate : true,				//出発日
		depDateFrom : false,		//出発期間From
		depDateTo : false,			//出発期間To
		isWithTourConductor : true,	//かんたん検索オプションラジオ
		// コースコードから探す
		courseCode : true,			//コースコード
		depCourseDate : true		//出発日
	},
	targetModule : undefined,
	depDateClass : 'm_depPackDate',
	depDateFromClass : 'm_depPackDateFrom',
	depDateToClass : 'm_depPackDateTo',
	isSpFlg : false,
	courseCodeObj : undefined
};

//海外パッケージ「行き先から探す」検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_inttourModulePkg_submitSearchDest(obj) {
	var target = obj.targetModule;
	var isExistDef =  m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, {}, 'isExist');
	var isExist = $tour_module_jq.extend(isExistDef, obj.isExist);
	var depDateClass = m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, obj, 'depDateClass');
	var depDateFromClass = m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, obj, 'depDateFromClass');
	var depDateToClass = m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, obj, 'depDateToClass');
	var isSpFlg = m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, obj, 'isSpFlg');
	
	var targetForm = $tour_module_jq(this).closest('.form-pack');
	
	// リクエストパラメータ調整
	m_inttourModulePkg_arrangeRequestDest({
		isExist : isExist,
		target : target,
		targetForm : targetForm,
		depDateClass : depDateClass,
		depDateFromClass : depDateFromClass,
		depDateToClass : depDateToClass,
		isSpFlg : isSpFlg
	});
	// 検索実行
	targetForm.submit();
};


//海外パッケージ「行き先から探す」検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_inttourModulePkg_arrangeRequestDest = function (option){
	var isExist = option.isExist;
	var target = option.target;
	var targetForm = option.targetForm;

	if(isExist.depDate){
		// 出発日の送信パラメータ(hidden)設定
		var depDateClass = option.depDateClass;
		var depValueYearMonthDay = targetForm.find('.' + depDateClass).val();
		var depValueYearMonth = '';
		var depValueDay = '';
		// 値分割
		if(!_.isEmpty(depValueYearMonthDay)){
			depValueYearMonth = depValueYearMonthDay.slice(0, 6);
			depValueDay = depValueYearMonthDay.slice(6);
		}
		// hiddenに値設定・出力
		target.tourCommonModule('createInputHidden', {
			intPkgDepDateYM : depValueYearMonth,
			intPkgDepDateD : depValueDay
		});
	}else if(isExist.depDateFrom && isExist.depDateTo){
		// 出発期間の送信パラメータ(hidden)設定
		var depDateClass = option.depDateFromClass;
		var depValueYearMonthDay = targetForm.find('.' + depDateClass).val();
		var depValueYearMonth = '';
		var depValueDay = '';
		// 値分割
		if(!_.isEmpty(depValueYearMonthDay)){
			depValueYearMonth = depValueYearMonthDay.slice(0, 6);
			depValueDay = depValueYearMonthDay.slice(6);
		}
		var depDateEndClass = option.depDateToClass;
		var depEndValueYearMonthDay = targetForm.find('.' + depDateEndClass).val();
		var depEndValueYearMonth = '';
		var depEndValueDay = '';
		// 値分割
		if(!_.isEmpty(depEndValueYearMonthDay)){
			depEndValueYearMonth = depEndValueYearMonthDay.slice(0, 6);
			depEndValueDay = depEndValueYearMonthDay.slice(6);
		}
		// hiddenに値設定・出力
		target.tourCommonModule('createInputHidden', {
			intPkgDepDateYM : depValueYearMonth,
			intPkgDepDateD : depValueDay,
			intPkgDepEndDateYM : depEndValueYearMonth,
			intPkgDepEndDateD : depEndValueDay
		});
	}
	if(isExist.isWithTourConductor){
		// かんたん検索の送信パラメータ設定
		var isSpFlg = option.isSpFlg;
		if(isSpFlg){
			// スマートフォン表示の場合はツアー名をクリアする。
			targetForm.find('input[name="courseName"]').val('');
		}
		var selectedRadioValue = targetForm.find('input[name="isWithTourConductor"]:checked').val();
		if (selectedRadioValue === m_inttourModulePkg_NameTypeList[0].value) {
			// フリーワード検索時はツアー名をクリア
			targetForm.find('input[name="courseName"]').val('');
		}else{
			// ツアー名検索時はフリーワード
			targetForm.find('input[name="freeword"]').val('');
		}
	}
}

//海外パッケージ「コースコードから探す」検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_inttourModulePkg_submitSearchCourse(obj) {
	var target = obj.targetModule;
	var isExistDef =  m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, {}, 'isExist');
	var isExist = $tour_module_jq.extend(isExistDef, obj.isExist);
	var courseCodeObj = m_tourModuleCommon_getParam(m_inttourModulePkg_siteType, obj, 'courseCodeObj');
	var depDateClass = obj.depDateClass;
	var destination = 'new';
	
	var targetForm = $tour_module_jq(this).closest('.form-pack');
	
	// コースコードチェック・座席クラス設定・遷移先設定
	if(isExist.courseCode){
		destination = m_inttourModulePkg_checkCourseCode(target, courseCodeObj, depDateClass);
	}
	// TOPからの遷移時はCookieを作成
	if(obj.createCookie!==undefined){
		obj.createCookie();
	}
	// リクエストパラメータ調整
	m_inttourModulePkg_arrangeRequestCourse({
		isExist : isExist,
		target : target,
		targetForm : targetForm,
		courseCodeObj : courseCodeObj,
		depDateClass : depDateClass,
		destination : destination
	});
	// 検索実行
	targetForm.submit();
};


//海外パッケージ「コースコードから探す」検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_inttourModulePkg_arrangeRequestCourse = function (option){
	var target = option.target;
	var isExist = option.isExist;
	var targetForm = option.targetForm;
	var destination = option.destination;

	// コースコード
	if(isExist.courseCode){
		// 旧BEの場合パラメータ名変更
		if(destination === 'old'){
			$tour_module_jq(option.courseCodeObj).attr('name', 'PC_01corsecd');
		} else {
			$tour_module_jq(option.courseCodeObj).attr('name', 'courseNo');
		}
	}
	
	// 追加HTML分の事前削除
	targetForm.find('input[name="PC_01fromyear"]').remove();
	targetForm.find('input[name="PC_01frommonth"]').remove();
	targetForm.find('input[name="PC_01fromday"]').remove();
	targetForm.find('input[name="SRC"]').remove();
	targetForm.find('input[name="DST"]').remove();
	
	// 出発日
	if(isExist.depCourseDate){
		var depDateClass = option.depDateClass;
		var depValueYearMonthDay = targetForm.find('.' + depDateClass).val();
		// 新・旧BEそれぞれ値設定
		if(destination === 'old'){
			var depValueYear = '';
			var depValueMonth = '';
			var depValueDay = '';
			// 値分割
			if(!_.isEmpty(depValueYearMonthDay)){
				depValueYear = depValueYearMonthDay.slice(0, 4);
				depValueMonth = depValueYearMonthDay.slice(4, 6);
				depValueDay = depValueYearMonthDay.slice(6);
			}
			//旧BE用カレンダーパラメータ設定
			var hiddenHtml = '';
			hiddenHtml += '<input type="hidden" name="PC_01fromyear" value="' + depValueYear + '">\n';
			hiddenHtml += '<input type="hidden" name="PC_01frommonth" value="' + depValueMonth + '">\n';
			hiddenHtml += '<input type="hidden" name="PC_01fromday" value="' + depValueDay + '">\n';
			targetForm.append(hiddenHtml);
		}else{
			var depValueYearMonth = '';
			var depValueDay = '';
			// 値分割
			if(!_.isEmpty(depValueYearMonthDay)){
				depValueYearMonth = depValueYearMonthDay.slice(0, 6);
				depValueDay = depValueYearMonthDay.slice(6);
			}
			// hiddenに値設定・出力
			target.tourCommonModule('createInputHidden', {
				intPkgDepDateYM : depValueYearMonth,
				intPkgDepDateD : depValueDay
			});
		}
	}
	
	// 旧BE用固定パラメータ追加、新BEパラメータ排除
	if(destination === 'old'){
		// 追加
		var hiddenHtml = '';
		hiddenHtml += '<input type="hidden" name="SRC" value="PC_010">\n';
		hiddenHtml += '<input type="hidden" name="DST" value="PC_010_02">\n';
		targetForm.append(hiddenHtml);
		// 排除
		targetForm.find('input[name="isSearchTransition"]').prop('disabled', true);
		targetForm.find('input[name="coopSiteCode"]').prop('disabled', true);
	} else {
		targetForm.find('input[name="isSearchTransition"]').prop('disabled', false);
		targetForm.find('input[name="coopSiteCode"]').prop('disabled', false);
	}

}

//コースコード入力値チェックと整形、座席クラス設定、遷移先BE決定
// return 'new':新BE、'old':'旧BE'
function m_inttourModulePkg_checkCourseCode(target, courseCode, depDateClass){
	var destination = 'new';
	var new_be = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_inttourModulePkg_fromAction_coursecode, 'URL');
	var old_be = m_tourModuleCommon_oldBE_DOMAIN + m_tourModuleCommon_getListValue(m_inttourModulePkg_fromAction_oldBE, 'URL');

	//form、コースコード入力値を取得
	var targetForm = courseCode.closest('form');
	var courseCodeValue = courseCode.val();
	
	//1桁目が数字の場合は新BEへ、それ以外は旧BEへ送信する
	if(/^[0-9０-９]/.test(courseCodeValue)){
		//新BE
		targetForm.attr('action', new_be);
		destination = 'new';
	}else{
		//旧BE
		targetForm.attr('action', old_be);
		destination = 'old';
		targetForm.attr('method', 'POST');
	}
	
	//お客様がハイフンを入れたらパラメータで除外
	if(/-/.test(courseCodeValue) || /ー/.test(courseCodeValue) || /－/.test(courseCodeValue)){
		courseCodeValue = courseCodeValue.replace(/-/g,'')
										.replace(/ー/g,'')
										.replace(/－/g,'');
	}
	
	//全角を半角に変換
	if(/[Ａ-Ｚａ-ｚ０-９]/.test(courseCodeValue)){
		courseCodeValue = courseCodeValue.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){
		return String.fromCharCode(s.charCodeAt(0)-0xFEE0);
		});
	}
	//小文字を大文字に変換
	courseCodeValue = courseCodeValue.toUpperCase();
	//コースコード欄に反映
	courseCode.val(courseCodeValue);
	
	//新BEの場合はコースコードから座席クラスを取得する
	if(targetForm.attr('action') === new_be){
		//座席クラス
		var cabinClass = '';
		//判定用文字
		var digit= '';
		
		//7桁目が存在する場合は7桁目で判定
		if(6 < courseCodeValue.length){
			digit = courseCodeValue.charAt(6);
			//7桁目リスト
			var inttourClassFareListCourseCode = m_inttourModulePkg_classFareListCourseCode;
			//リストから該当値を検索しセットする
			cabinClass = inttourClassFareListCourseCode[digit];
		
		//それ以外は3桁目を参照
		}else if(2 < courseCodeValue.length){
			digit = courseCodeValue.charAt(2);
			//3桁目リスト
			var inttourClassFareListCourseCodeBasic = m_inttourModulePkg_classFareListCourseCodeBasic;
			//リストから該当値を検索しセットする
			cabinClass = inttourClassFareListCourseCodeBasic[digit];
		}
		
		//値が取れなかった場合はブランク
		if(cabinClass === undefined){
			cabinClass = '';
		}
		
		//座席クラス格納用のhidden作成
		target.tourCommonModule('createInputHidden', {
			cabinClassCode : cabinClass
		});
	}
	
	return destination;
}

// 海外ツアーパッケージの各項目不正値チェック処理
// options:チェック対象の各値
// siteType:各モジュール判定、setParam取得用
// defaults:各モジュールデフォルト値
// return:options(チェック後)
function m_inttourModulePkg_incorrectValue(options, siteType, defaults) {
	
	//座席クラス
	var cfCheck = [];
	for(cf=0;cf<m_inttourPkg_classFareList_DEFAULT.length;cf++){
		cfCheck[cf] = m_inttourPkg_classFareList_DEFAULT[cf]['value'];
	}

	//不正値チェック設定
	var m_inttourModulePkg_checkedArray = {
			m_defAirportDep:/[A-Z][A-Z][A-Z]/,	// 国際線出発地
			m_defAreaArr:/[0-9][0-9]/,			// 目的地(地域)
			m_defCountryArr:/[A-Z][A-Z]/,		// 目的地(国)
			m_defAirportArr:/[A-Z][A-Z][A-Z]/,	// 目的地(都市)
			m_defDepartureDateType:['0','1'],	// 出発日・期間選択ラジオ
			m_defClassFare:cfCheck,				// 座席クラス
			m_defIsLandOnly:['checked',''],		// 現地ホテルから参加可能ツアーチェック
			m_defIsWithTourConductor:['0','1'],	// 簡単検索オプション選択
			m_defCourseCode:/^[a-zA-Z0-9]+$/,	// コースコード名
	}
	
	// 国際線出発地
	var checkPackAirportDep = m_tourModuleCommon_getParam(siteType, options, 'm_defAirportDep');
	if(!m_inttourModulePkg_checkedArray['m_defAirportDep'].test(checkPackAirportDep)){
		options['m_defAirportDep'] = defaults['m_defAirportDep'];
	}
	// 目的地(地域)
	var checkPackAreaArr = m_tourModuleCommon_getParam(siteType, options, 'm_defAreaArr');
	var defAreaArr = defaults['m_defAreaArr'];
	if(checkPackAreaArr !== undefined && checkPackAreaArr != defAreaArr && !m_inttourModulePkg_checkedArray['m_defAreaArr'].test(checkPackAreaArr)){
		options['m_defAreaArr'] = defaults['m_defAreaArr'];
	}
	// 目的地(国)
	var checkPackCountryArr = m_tourModuleCommon_getParam(siteType, options, 'm_defCountryArr');
	var defCountryArr = defaults['m_defCountryArr'];
	if(checkPackCountryArr !== undefined && checkPackCountryArr != defCountryArr && !m_inttourModulePkg_checkedArray['m_defCountryArr'].test(checkPackCountryArr)){
		options['m_defCountryArr'] = defaults['m_defCountryArr'];
	}
	// 目的地(都市)
	var checkPackAirportArr = m_tourModuleCommon_getParam(siteType, options, 'm_defAirportArr');
	var defAirportArr = defaults['m_defAirportArr'];
	if(checkPackAirportArr !== undefined && checkPackAirportArr != defAirportArr && !m_inttourModulePkg_checkedArray['m_defAirportArr'].test(checkPackAirportArr)){
		options['m_defAirportArr'] = defaults['m_defAirportArr'];
	}
	// 出発日・期間選択ラジオ
	var checkPackDepartureDateType = m_tourModuleCommon_getParam(siteType, options, 'm_defDepartureDateType');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkPackDepartureDateType, m_inttourModulePkg_checkedArray['m_defDepartureDateType']) === false){
		options['m_defDepartureDateType'] = defaults['m_defDepartureDateType'];
	}
	// 座席クラス
	var checkPackClassFare = m_tourModuleCommon_getParam(siteType, options, 'm_defClassFare');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkPackClassFare, m_inttourModulePkg_checkedArray['m_defClassFare']) === false){
		options['m_defClassFare'] = defaults['m_defClassFare'];
	}
	// 現地ホテルから参加プランを選択可能なツアー（航空券を利用しない）のチェックボックス
	var checkIsWithoutTicketsChecked = m_tourModuleCommon_getParam(siteType, options, 'm_defIsLandOnly');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkIsWithoutTicketsChecked, m_inttourModulePkg_checkedArray['m_defIsLandOnly']) === false){
		options['m_defIsLandOnly'] = defaults['m_defIsLandOnly'];
	}
	// かんたん検索オプション
	var checkPackTourNameType = m_tourModuleCommon_getParam(siteType, options, 'm_defIsWithTourConductor');
	if(m_inttourModuleCommon_getStrArrayMatchAll(checkPackTourNameType, m_inttourModulePkg_checkedArray['m_defIsWithTourConductor']) === false){
		options['m_defIsWithTourConductor'] = defaults['m_defIsWithTourConductor'];
	}
	// コースコード
	var checkPackCourseCode = m_tourModuleCommon_getParam(siteType, options, 'm_defCourseCode');
	if(!m_inttourModulePkg_checkedArray['m_defCourseCode'].test(checkPackCourseCode)){
		options['m_defCourseCode'] = defaults['m_defCourseCode'];
	}
	
	return options;
}

//海外パッケージ 共通処理 END
//////////////////////////////////

//////////////////////////////////
//海外オプション 共通処理 START

//海外オプションの各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_inttourModuleOption_incorrectValue(options, siteType, defaults) {
	//不正値チェック設定
	var m_inttourModuleOption_checkedArray = {
			m_defArea:/[0-9]/,			// エリア
			m_defTheme:/[0-9]/			// テーマ
	}
	
	// エリア
	var checkOptionArea = m_tourModuleCommon_getParam(siteType, options, 'm_defArea');
	var defArea = defaults['m_defArea'];
	if(checkOptionArea !== undefined && checkOptionArea != defArea && !m_inttourModuleOption_checkedArray['m_defArea'].test(checkOptionArea)){
		options['m_defArea'] = defaults['m_defArea'];
	}
	
	// テーマ
	var checkOptionTheme = m_tourModuleCommon_getParam(siteType, options, 'm_defTheme');
	var defTheme = defaults['m_defTheme'];
	if(checkOptionTheme !== undefined && checkOptionTheme != defTheme && !m_inttourModuleOption_checkedArray['m_defTheme'].test(checkOptionTheme)){
		options['m_defTheme'] = defaults['m_defTheme'];
	}
	
	return options;
}

//海外オプション 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_inttourModuleOption_submitSearch(obj) {
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-option');

	// リクエストパラメータ調整は特に無し

	// 検索実行
	targetForm.submit();
}

//海外オプション 共通処理 END
//////////////////////////////////

$tour_module_jq.fn.inttourCommonModule = function(method) {

	var methods = {
		'submitSearchDp' : m_inttourModuleDp_submitSearch,
		'submitSearchPkgDest' : m_inttourModulePkg_submitSearchDest,
		'submitSearchPkgCourse' : m_inttourModulePkg_submitSearchCourse,
		'submitSearchOption' : m_inttourModuleOption_submitSearch
	};

	// Dispatch the proper method
	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.inttourCommonModule');
	}
};
