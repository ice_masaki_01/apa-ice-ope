/*=====================================================
* Script Name: domtour2nd_module_traffic.js
* Description: 空席照会モジュール 国内ツアー2nd 観光
* Version: 1.00
* Last Up Date: 2017/10/11
=====================================================*/

var m_domtour2ndModuleTraffic_siteType = { site : 'domtour2nd', module : 'traffic' };

//初期値設定
var m_domtour2ndModuleTraffic_defaults = {
	m_defDateTraffic:'',		//利用日用
	m_defTrafficDistrict:'',	//目的地(方面)
	m_defTrafficRegion:'',		//目的地(地区)
	m_defPurposeTraffic:'',		//目的用
	m_defTabNoDispCls:''					//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleTraffic_optionParam = $tour_module_jq.extend(m_domtour2ndModuleTraffic_optionParam, m_domtour2ndModuleTraffic_defaults);

/**
 * 国内ツアー観光2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModuleTraffic_setParams(options){
	m_domtour2ndModuleTraffic_optionParam = $tour_module_jq.extend(m_domtour2ndModuleTraffic_optionParam, options);
};

/**
 * 国内ツアー観光2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleTraffic_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアー観光の各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModuleTraffic_incorrectValue(options, m_domtour2ndModuleTraffic_siteType, m_domtour2ndModuleTraffic_defaults));
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//利用日
	var trafficDateText = m_tourModuleCommon_getListValue(m_domtourModuleTraffic_trafficDate, 'Text');
	//目的地
	var trafficPlaceText = m_tourModuleCommon_getListValue(m_domtourModuleTraffic_trafficPlace, 'Text');
	//目的
	var trafficPurposeText = m_tourModuleCommon_getListValue(m_domtourModuleTraffic_trafficPurpose, 'Text');
	//検索ボタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModuleTraffic_fromAction, 1);
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');
	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defFormMethod');
	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<form class="form-domTraffic" id="form-domTraffic" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<div class="mod-content_wrap">\n';
	html += '<div class="mod-content_cover col3">\n';
	
	//1カラム目 利用日
	html += '<div class="mod-content_box">\n';
	
	//利用日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + trafficDateText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_trafficDate">\n';
	//利用日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//1カラム目 END
	
	
	//2カラム目 目的地 目的
	html += '<div class="mod-content_box">\n';
	
	//目的地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + trafficPlaceText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_trafficPlace">\n';
	//目的地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//目的
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + trafficPurposeText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_trafficPurpose">\n';
	//目的部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 END
	
	//3カラム目 検索ボタン
	html += '<div class="mod-content_box mod-btn_box">\n';
	
	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '<div class="btn btn-search" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//3カラム目 END
	
	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</div><!-- / .mod-content_wrap -->\n';
	html += '</form>\n';
	html += '\n';

	this.append(html);
	
	m_domtour2ndModuleTraffic_setDefVal(this, options);
	
};

function m_domtour2ndModuleTraffic_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defModulePosition');
	// 目的地(方面、地区)設定内容の補正（どれか一つでも設定されていたらundefinedの項目は空文字を設定する）
	if (options.m_defTrafficDistrict !== undefined ||
		options.m_defTrafficRegion !== undefined) {

		if (options.m_defTrafficDistrict === undefined) {
			options.m_defTrafficDistrict = '';
		}
		if (options.m_defTrafficRegion === undefined) {
			options.m_defTrafficRegion = '';
		}
	}
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	//パラメータ設定を取得
	//利用日
	var defDateTraffic = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defDateTraffic');
	//目的地(方面)
	var defDistrictTraffic = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defTrafficDistrict');
	//目的地(地区)
	var defRegionTraffic = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defTrafficRegion');
	//目的
	var defPurposeTraffic = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defPurposeTraffic');
	//再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModuleTraffic_siteType, options, 'm_defTabNoDispCls');

	//利用日
	target.find('.m_trafficDate').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleTraffic_trafficDate, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defDateTraffic,
		defSelectDay : m_domtourModuleTraffic_defSelectDay,
		dateValueClass : m_domtourModuleTraffic_dateValueClass,
		dateValueName : m_domtourModuleTraffic_dateValueName,
		inputPlaceholder : m_domtourModuleTraffic_inputPlaceholder,
		inputClass : m_domtourModuleTraffic_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModuleTraffic_isRequired,
		isDisabled : m_domtourModuleTraffic_isDisabled,
		isDispAllOfMonth : m_domtourModuleTraffic_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleTraffic_prevNextInterval,
		receiptDisableDays : m_domtourModuleTraffic_receiptDisableDays,
		dispMonths : m_domtourModuleCommon_defDispMonths,
		selectableMonths : m_domtourModuleCommon_defSelectableMonths,
		isDispHoliday : m_domtourModuleTraffic_isDispHoliday,
		boundaryTime : m_domtourModuleTraffic_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//目的地
	target.find('.m_trafficPlace').tourCommonModule('createAirportInput', {
		parent:this,
		defAreaCode : defDistrictTraffic,                         // 方面
		defAirportCode : defRegionTraffic,                        // 地区
		defPartsPosition:dataControlPointValue,
		balloonTitle:m_domtour2ndModuleTraffic_depApo['Text'],
		areaClass:'m_trafficPlaceArea',
		areaName : 'districtCd',                                  // 方面
		countryClass:'m_trafficPlaceCountry',
		apoCodeClass: 'm_trafficPlaceApo',
		apoCodeName: 'regionCd',                                  // 地区
		
		inputPlaceholder:m_domtour2ndModuleTraffic_depApo['Placeholder'],
		inputClass:'m_domTrafficPlaceText',
		isRequired:false,
		isDisabled:false,
		layoutType:'26',
		uniqueKey:m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType:'2'
	});
	
	//目的
	target.find('.m_trafficPurpose').tourCommonModule('createPurposePull', {
		site : m_domtour2ndModulePkg_siteType.site,
		defSelect : defPurposeTraffic,
		inputClass:'',
		inputId:'dom_traffic_purpose',
		inputName:'optCls'
	});
	
	// 検索ボタン押下イベント登録：サブミット実行
	target.find('.btn-search').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			$tour_module_jq(this).domtourCommonModule('submitSearchTraffic', {
				targetModule : target
			});
		}
	});
	
	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}
	
	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 1);
	m_tourCommonModuleParts_setPartsCount('calendar', 1);
};



$tour_module_jq.fn.domtour2ndModuleTraffic = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleTraffic_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleTraffic_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleTraffic');
	}
};
