/*=====================================================
* Script Name: domtour_module_common.js
* Description: 空席照会モジュール 国内ツアー共通処理
* Version: 1.01
* Last Up Date: 2017/10/19
=====================================================*/

//指定文字列の配列内存在チェック
function m_domtourModuleCommon_getStrArrayMatchAll(targetStr, arrayStr){
	if($tour_module_jq.inArray(targetStr, arrayStr) != -1){
		return true;
	}
	return false;
};

// 国内ツアー旅作の各項目不正値チェック処理
// options:チェック対象の各値
// siteType:各モジュール判定、setParam取得用
// defaults:各モジュールデフォルト値
// return:options(チェック後)
function m_domtourModuleDp_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//参加人数（大人）
	var adultCheck = [];
	for(var i in m_domtourModuleDp_PeoplesPulldownList){
		adultCheck[i] = m_domtourModuleDp_PeoplesPulldownList[i].value;
	}
	
	//参加人数（子供幼児）
	var nplpCheck = [];
	var nplpList = m_domtourModuleNumberOfPeopleDp_NPLP.List;
	var nplpStartValue = undefined;
	var nplpEndValue = undefined;
	for(np=0;np<nplpList.length;np++){
		// 合計値は含めずにループ
		nplpStartValue = parseInt(nplpList[np][0]['value']);
		nplpEndValue = parseInt(nplpList[np][nplpList[np].length-1]['value']);
		// チェック用の正規表現を配列に格納
		nplpCheck[np] = new RegExp("[" + nplpStartValue + "-" + nplpEndValue + "]");
	}
	
	//不正値チェック設定
	var m_domtourModuleDp_checkedArray = {
			m_defDepFlightAirportDep:/[A-Z]|[0-9]/,		//フライト行　出発地用
			m_defDepFlightAirportArr:/[A-Z]|[0-9]/,		//フライト行　到着地用
			m_defArrFlightAirportDep:/[A-Z]|[0-9]/,		//フライト帰り　出発地用
			m_defArrFlightAirportArr:/[A-Z]|[0-9]/,		//フライト帰り　到着地用 
			m_defNumberOfAdult:adultCheck,				//参加人数（大人）用
			m_defNumberOfChild:nplpCheck,				//参加人数（子供幼児）用
			m_defPlaceOfStayDistrict:/[0-9]/,			//宿泊地(方面)
			m_defPlaceOfStayRegion:/[0-9]/,		 		//宿泊地(地区)
			m_defPlaceOfStayArea:/[0-9]/,				//宿泊地(地域)
			m_defIsWithCar:['checked','']				//レンタカーも合わせて予約チェック用
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//フライト行　出発地
	var checkDepFlightAirportDep = m_tourModuleCommon_getParam(siteType, options, 'm_defDepFlightAirportDep');
	if(!m_domtourModuleDp_checkedArray['m_defDepFlightAirportDep'].test(checkDepFlightAirportDep)){
		options['m_defDepFlightAirportDep'] = defaults['m_defDepFlightAirportDep'];
	}
	
	//フライト行　到着地
	var checkDepFlightAirportArr = m_tourModuleCommon_getParam(siteType, options, 'm_defDepFlightAirportArr');
	if(!m_domtourModuleDp_checkedArray['m_defDepFlightAirportArr'].test(checkDepFlightAirportArr)){
		options['m_defDepFlightAirportArr'] = defaults['m_defDepFlightAirportArr'];
	}
	
	//フライト帰り　出発地
	var checkArrFlightAirportDep = m_tourModuleCommon_getParam(siteType, options, 'm_defArrFlightAirportDep');
	if(!m_domtourModuleDp_checkedArray['m_defArrFlightAirportDep'].test(checkArrFlightAirportDep)){
		options['m_defArrFlightAirportDep'] = defaults['m_defArrFlightAirportDep'];
	}
	
	//フライト帰り　到着地
	var checkArrFlightAirportArr = m_tourModuleCommon_getParam(siteType, options, 'm_defArrFlightAirportArr');
	if(!m_domtourModuleDp_checkedArray['m_defArrFlightAirportArr'].test(checkArrFlightAirportArr)){
		options['m_defArrFlightAirportArr'] = defaults['m_defArrFlightAirportArr'];
	}
	
	//参加人数（大人）
	var checkNumberOfAdult = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfAdult');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfAdult, m_domtourModuleDp_checkedArray['m_defNumberOfAdult']) === false){
		options['m_defNumberOfAdult'] = defaults['m_defNumberOfAdult'];
	}
	
	//参加人数（子供幼児）
	var checkNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfChild');
	options['m_defNumberOfChild'] = m_domtourModuleDp_incorrectValueNumberOfPeople(checkNumberOfPeopleSelectInit, m_domtourModuleDp_checkedArray['m_defNumberOfChild']);

	//宿泊地(方面)
	var checkPlaceOfStayDistrict = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayDistrict');
	var defDistrict = defaults['m_defPlaceOfStayDistrict'];
	if(checkPlaceOfStayDistrict !== undefined && checkPlaceOfStayDistrict != defDistrict && !m_domtourModuleDp_checkedArray['m_defPlaceOfStayDistrict'].test(checkPlaceOfStayDistrict)){
		options['m_defPlaceOfStayDistrict'] = defaults['m_defPlaceOfStayDistrict'];
	}
	//宿泊地(地区)
	var checkPlaceOfStayRegion = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayRegion');
	var defRegion = defaults['m_defPlaceOfStayRegion'];
	if(checkPlaceOfStayRegion !== undefined && checkPlaceOfStayRegion != defRegion && !m_domtourModuleDp_checkedArray['m_defPlaceOfStayRegion'].test(checkPlaceOfStayRegion)){
		options['m_defPlaceOfStayRegion'] = defaults['m_defPlaceOfStayRegion'];
	}
	//宿泊地(地域)
	var checkPlaceOfStayArea = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayArea');
	var defArea = defaults['m_defPlaceOfStayArea'];
	if(checkPlaceOfStayArea !== undefined && checkPlaceOfStayArea != defArea && !m_domtourModuleDp_checkedArray['m_defPlaceOfStayArea'].test(checkPlaceOfStayArea)){
		options['m_defPlaceOfStayArea'] = defaults['m_defPlaceOfStayArea'];
	}
	
	//レンタカーも合わせて予約チェックボックス
	var checkIsWithCar = m_tourModuleCommon_getParam(siteType, options, 'm_defIsWithCar');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkIsWithCar, m_domtourModuleDp_checkedArray['m_defIsWithCar']) === false){
		options['m_defIsWithCar'] = defaults['m_defIsWithCar'];
	}
	
	return options;
}

function m_domtourModuleDp_incorrectValueNumberOfPeople(numberOfPeople, checkedArray) {
	if(numberOfPeople !== undefined){
		var checkNOP = checkedArray;
		for(i=0;i<numberOfPeople.length;i++){
			for(j=0;j<numberOfPeople[i].length;j++){
				var checkInitValue = numberOfPeople[i][j];
				if($tour_module_jq.isArray(checkNOP) && !checkNOP[j].test(checkInitValue)){
					numberOfPeople[i][j] = m_domtourModuleNumberOfPeopleDp_optionParams['initNum'][i][j];
				}
			}
		}
	}
	return numberOfPeople;
}

//国内ツアーパッケージの各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_domtourModulePkg_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//旅行日数
	var daysCheck = [];
	for(var i in m_domtourModulePkg_travelOfDaysList){
		daysCheck[i] = m_domtourModulePkg_travelOfDaysList[i].value;
	}
	
	//大人
	var adultCheck = [];
	for(var i in m_domtourModulePkg_PeoplesPulldownList){
		adultCheck[i] = m_domtourModulePkg_PeoplesPulldownList[i].value;
	}
	
	//参加人数（子供幼児）
	var nplpCheck = [];
	var nplpList = m_domtourModuleNumberOfPeoplePkg_NPLP.List;
	var nplpStartValue = undefined;
	var nplpEndValue = undefined;
	for(np=0;np<nplpList.length;np++){
		// 合計値は含めずにループ
		nplpStartValue = parseInt(nplpList[np][0]['value']);
		nplpEndValue = parseInt(nplpList[np][nplpList[np].length-1]['value']);
		// チェック用の正規表現を配列に格納
		nplpCheck[np] = new RegExp("[" + nplpStartValue + "-" + nplpEndValue + "]");
	}
	
	//不正値チェック設定
	var m_domtourModulePkg_checkedArray = {
			m_defModuleMode:['1','0'],			//新旧モジュール切り替えラジオ
			m_defAirportDep:/[0-9]/,			//出発地用
			m_defAreaArr:/[0-9]/,				//目的地用
			m_defNumberOfDays:daysCheck,		//旅行日数用
			m_defNumberOfAdult:adultCheck,		//大人用
			m_defNumberOfChild:nplpCheck,		//参加人数（子供幼児）用
			m_defCourseAirportDep:/[0-9]/,		//出発地用（コースコード）
			m_defCourseCode:/^[a-zA-Z0-9]+$/	//コースコード用（コースコード）
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//新旧モジュール切り替えラジオ
	var checkModuleMode = m_tourModuleCommon_getParam(siteType, options, 'm_defModuleMode');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkModuleMode, m_domtourModulePkg_checkedArray['m_defModuleMode']) === false){
		options['m_defModuleMode'] = defaults['m_defModuleMode'];
	}
	
	//出発地
	var checkAirportDep = m_tourModuleCommon_getParam(siteType, options, 'm_defAirportDep');
	if(!m_domtourModulePkg_checkedArray['m_defAirportDep'].test(checkAirportDep)){
		options['m_defAirportDep'] = defaults['m_defAirportDep'];
	}
	
	//目的地
	var checkAreaArr = m_tourModuleCommon_getParam(siteType, options, 'm_defAreaArr');
	if(!m_domtourModulePkg_checkedArray['m_defAreaArr'].test(checkAreaArr)){
		options['m_defAreaArr'] = defaults['m_defAreaArr'];
	}
	
	//旅行日数
	var checkNumberOfDays = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfDays');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfDays, m_domtourModulePkg_checkedArray['m_defNumberOfDays']) === false){
		options['m_defNumberOfDays'] = defaults['m_defNumberOfDays'];
	}
	
	//大人
	var checkNumberOfAdult = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfAdult');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfAdult, m_domtourModulePkg_checkedArray['m_defNumberOfAdult']) === false){
		options['m_defNumberOfAdult'] = defaults['m_defNumberOfAdult'];
	}
	
	//参加人数（子供幼児）
	var checkNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfChild');
	options['m_defNumberOfChild'] = m_domtourModulePkg_incorrectValueNumberOfPeople(checkNumberOfPeopleSelectInit, m_domtourModulePkg_checkedArray['m_defNumberOfChild']);
	
	//出発地（コースコード）
	var checkCourseAirportDep = m_tourModuleCommon_getParam(siteType, options, 'm_defCourseAirportDep');
	if(!m_domtourModulePkg_checkedArray['m_defCourseAirportDep'].test(checkCourseAirportDep)){
		options['m_defCourseAirportDep'] = defaults['m_defCourseAirportDep'];
	}
	
	//コースコード（コースコード）
	var checkCourseCode = m_tourModuleCommon_getParam(siteType, options, 'm_defCourseCode');
	if(!m_domtourModulePkg_checkedArray['m_defCourseCode'].test(checkCourseCode)){
		options['m_defCourseCode'] = defaults['m_defCourseCode'];
	}
	
	return options;
}

function m_domtourModulePkg_incorrectValueNumberOfPeople(numberOfPeople, checkedArray) {
	if(numberOfPeople !== undefined){
		var checkNOP = checkedArray;
		for(i=0;i<numberOfPeople.length;i++){
			for(j=0;j<numberOfPeople[i].length;j++){
				var checkInitValue = numberOfPeople[i][j];
				if($tour_module_jq.isArray(checkNOP) && !checkNOP[j].test(checkInitValue)){
					numberOfPeople[i][j] = m_domtourModuleNumberOfPeoplePkg_optionParams['initNum'][i][j];
				}
			}
		}
	}
	return numberOfPeople;
}

//国内ツアーホテルの各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_domtourModuleHotel_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//旅行日数
	var adultCheck = [];
	for(var i in m_domtourModuleHotel_PeoplesPulldownList){
		adultCheck[i] = m_domtourModuleHotel_PeoplesPulldownList[i].value;
	}
	
	//こだわり条件
	var AddCondition = [];
	for(var i in m_domtourModule_HotelOptionList){
		AddCondition[i] = m_domtourModule_HotelOptionList[i].id;
	}
	
	//参加人数（子供幼児）
	var nplpCheck = [];
	var nplpList = m_domtourModuleNumberOfPeopleHotel_NPLP.List;
	var nplpStartValue = undefined;
	var nplpEndValue = undefined;
	for(np=0;np<nplpList.length;np++){
		// 合計値は含めずにループ
		nplpStartValue = parseInt(nplpList[np][0]['value']);
		nplpEndValue = parseInt(nplpList[np][nplpList[np].length-1]['value']);
		// チェック用の正規表現を配列に格納
		nplpCheck[np] = new RegExp("[" + nplpStartValue + "-" + nplpEndValue + "]");
	}
	
	//不正値チェック設定
	var m_domtourModuleHotel_checkedArray = {
		m_defPlaceOfStayDistrict:/[0-9]/,			//宿泊地(方面)
		m_defPlaceOfStayRegion:/[0-9]/,		 		//宿泊地(地区)
		m_defPlaceOfStayArea:/[0-9]/,				//宿泊地(地域)
		m_defNumberOfAdult:adultCheck,				//大人用
		m_defNumberOfChild:nplpCheck,				//参加人数（子供幼児）用
		m_defAddConditionChecked:AddCondition		//こだわり条件用
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//宿泊地(方面)
	var checkPlaceOfStayDistrict = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayDistrict');
	var defDistrict = defaults['m_defPlaceOfStayDistrict'];
	if(checkPlaceOfStayDistrict !== undefined && checkPlaceOfStayDistrict != defDistrict && !m_domtourModuleHotel_checkedArray['m_defPlaceOfStayDistrict'].test(checkPlaceOfStayDistrict)){
		options['m_defPlaceOfStayDistrict'] = defaults['m_defPlaceOfStayDistrict'];
	}
	//宿泊地(地区)
	var checkPlaceOfStayRegion = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayRegion');
	var defRegion = defaults['m_defPlaceOfStayRegion'];
	if(checkPlaceOfStayRegion !== undefined && checkPlaceOfStayRegion != defRegion && !m_domtourModuleHotel_checkedArray['m_defPlaceOfStayRegion'].test(checkPlaceOfStayRegion)){
		options['m_defPlaceOfStayRegion'] = defaults['m_defPlaceOfStayRegion'];
	}
	//宿泊地(地域)
	var checkPlaceOfStayArea = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayArea');
	var defArea = defaults['m_defPlaceOfStayArea'];
	if(checkPlaceOfStayArea !== undefined && checkPlaceOfStayArea != defArea && !m_domtourModuleHotel_checkedArray['m_defPlaceOfStayArea'].test(checkPlaceOfStayArea)){
		options['m_defPlaceOfStayArea'] = defaults['m_defPlaceOfStayArea'];
	}
	
	//大人
	var checkNumberOfAdult = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfAdult');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfAdult, m_domtourModuleHotel_checkedArray['m_defNumberOfAdult']) === false){
		options['m_defNumberOfAdult'] = defaults['m_defNumberOfAdult'];
	}
	
	//参加人数（子供幼児）
	var checkNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfChild');
	options['m_defNumberOfChild'] = m_domtourModuleHotel_incorrectValueNumberOfPeople(checkNumberOfPeopleSelectInit, m_domtourModuleHotel_checkedArray['m_defNumberOfChild']);
	
	//こだわり条件
	var checkAddCondition = m_tourModuleCommon_getParam(siteType, options, 'm_defAddConditionChecked');
	if(checkAddCondition instanceof Array){
		var addCondition =[];
		for(var i in checkAddCondition){
			if(m_domtourModuleCommon_getStrArrayMatchAll(checkAddCondition[i], m_domtourModuleHotel_checkedArray['m_defAddConditionChecked']) === false){
				addCondition[i] = '';
			}else{
				addCondition[i] = checkAddCondition[i];
			}
		}
		options['m_defAddConditionChecked'] = addCondition;
	}else{
		options['m_defAddConditionChecked'] = defaults['m_defAddConditionChecked'];
	}

	
	return options;
}

function m_domtourModuleHotel_incorrectValueNumberOfPeople(numberOfPeople, checkedArray) {
	if(numberOfPeople !== undefined){
		var checkNOP = checkedArray;
		for(i=0;i<numberOfPeople.length;i++){
			for(j=0;j<numberOfPeople[i].length;j++){
				var checkInitValue = numberOfPeople[i][j];
				if($tour_module_jq.isArray(checkNOP) && !checkNOP[j].test(checkInitValue)){
					numberOfPeople[i][j] = m_domtourModuleNumberOfPeopleHotel_optionParams['initNum'][i][j];
				}
			}
		}
	}
	return numberOfPeople;
}

//国内ツアーレンタカーの各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_domtourModuleCar_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//配車時間・返車時間
	var carTimeCheck = [];
	for(var i in m_domtourModule_TimePulldownList){
		carTimeCheck[i] = m_domtourModule_TimePulldownList[i].value;
	}
	
	//車両クラス
	var carClassCheck = [];
	for(var i in m_domtourModule_CarClassList){
		carClassCheck[i] = m_domtourModule_CarClassList[i].id;
	}
	
	//不正値チェック設定
	var m_domtourModuleCar_checkedArray = {
			m_defTimeFirstCar:carTimeCheck,                 //配車時間
			m_defTimeLastCar:carTimeCheck,                  //返車時間
			m_defRadioFirstCar:['1','2'],                   //配車場所（ラジオ）
			m_defFirstApoAreaApo:/[A-Z]|[0-9]/,             //配車場所（空港エリア）
			m_defFirstCityAreaDistrict:/[0-9]/,             //配車場所（市内エリア）（方面）
			m_defFirstCityAreaRegion:/[0-9]/,               //配車場所（市内エリア）（地区）
			m_defFirstCityAreaArea:/[0-9]/,                 //配車場所（市内エリア）（地域）
			m_defRadioLastCar:['1','2','3'],                //返車場所（ラジオ）
			m_defLastApoAreaApo:/[A-Z]|[0-9]/,              //返車場所（空港営業所-空港選択）
			m_defLastApoOfficeApoCoptCd:/[A-Z]|[0-9]/,      //返車場所（空港営業所-営業所選択）（取引先）
			m_defLastApoOfficeApoFaclCd:/[A-Z]|[0-9]/,      //返車場所（空港営業所-営業所選択）（施設）
			m_defLastApoOfficeApoAbdAreaCd:/[A-Z]|[0-9]/,   //返車場所（空港営業所-営業所選択）（乗捨エリア）
			m_defLastApoOfficeApoNo:/[A-Z]|[0-9]/,          //返車場所（空港営業所-営業所選択）（営業所番号）
			m_defLastCityAreaDistrict:/[0-9]/,              //返車場所（市内営業所-エリア選択）（方面）
			m_defLastCityAreaRegion:/[0-9]/,                //返車場所（市内営業所-エリア選択）（地区）
			m_defLastCityAreaArea:/[0-9]/,                  //返車場所（市内営業所-エリア選択）（地域）
			m_defLastCityOfficeApoCoptCd:/[A-Z]|[0-9]/,     //返車場所（市内営業所-営業所選択）（取引先）
			m_defLastCityOfficeApoFaclCd:/[A-Z]|[0-9]/,     //返車場所（市内営業所-営業所選択）（施設）
			m_defLastCityOfficeApoAbdAreaCd:/[A-Z]|[0-9]/,  //返車場所（市内営業所-営業所選択）（乗捨エリア）
			m_defLastCityOfficeApoNo:/[A-Z]|[0-9]/,         //返車場所（市内営業所-営業所選択）（営業所番号）
			m_defCarClassChecked:carClassCheck              //車両クラス
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//配車時間
	var checkTimeFirstCar = m_tourModuleCommon_getParam(siteType, options, 'm_defTimeFirstCar');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkTimeFirstCar, m_domtourModuleCar_checkedArray['m_defTimeFirstCar']) === false){
		options['m_defTimeFirstCar'] = defaults['m_defTimeFirstCar'];
	}
	
	//返車時間
	var checkTimeLastCar = m_tourModuleCommon_getParam(siteType, options, 'm_defTimeLastCar');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkTimeLastCar, m_domtourModuleCar_checkedArray['m_defTimeLastCar']) === false){
		options['m_defTimeLastCar'] = defaults['m_defTimeLastCar'];
	}
	
	//配車場所ラジオ
	var checkRadioFirstCar = m_tourModuleCommon_getParam(siteType, options, 'm_defRadioFirstCar');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkRadioFirstCar, m_domtourModuleCar_checkedArray['m_defRadioFirstCar']) === false){
		options['m_defRadioFirstCar'] = defaults['m_defRadioFirstCar'];
	}
	
	//配車場所（空港エリア）
	var checkFirstApoAreaApo = m_tourModuleCommon_getParam(siteType, options, 'm_defFirstApoAreaApo');
	if(!m_domtourModuleCar_checkedArray['m_defFirstApoAreaApo'].test(checkFirstApoAreaApo)){
		options['m_defFirstApoAreaApo'] = defaults['m_defFirstApoAreaApo'];
	}
	
	//配車場所（市内エリア）（方面）
	var checkFirstCityAreaDistrict = m_tourModuleCommon_getParam(siteType, options, 'm_defFirstCityAreaDistrict');
	var defFirstDistrict = defaults['m_defFirstCityAreaDistrict'];
	if(checkFirstCityAreaDistrict !== undefined && checkFirstCityAreaDistrict != defFirstDistrict && !m_domtourModuleCar_checkedArray['m_defFirstCityAreaDistrict'].test(checkFirstCityAreaDistrict)){
		options['m_defFirstCityAreaDistrict'] = defaults['m_defFirstCityAreaDistrict'];
	}
	
	//配車場所（市内エリア）（地区）
	var checkFirstCityAreaRegion = m_tourModuleCommon_getParam(siteType, options, 'm_defFirstCityAreaRegion');
	var defFirstRegion = defaults['m_defFirstCityAreaRegion'];
	if(checkFirstCityAreaRegion !== undefined && checkFirstCityAreaRegion != defFirstRegion && !m_domtourModuleCar_checkedArray['m_defFirstCityAreaRegion'].test(checkFirstCityAreaRegion)){
		options['m_defFirstCityAreaRegion'] = defaults['m_defFirstCityAreaRegion'];
	}
	
	//配車場所（市内エリア）（地域）
	var checkFirstCityAreaArea = m_tourModuleCommon_getParam(siteType, options, 'm_defFirstCityAreaArea');
	var defFirstArea = defaults['m_defFirstCityAreaArea'];
	if(checkFirstCityAreaArea !== undefined && checkFirstCityAreaArea != defFirstArea && !m_domtourModuleCar_checkedArray['m_defFirstCityAreaArea'].test(checkFirstCityAreaArea)){
		options['m_defFirstCityAreaArea'] = defaults['m_defFirstCityAreaArea'];
	}
	
	//返車場所ラジオ
	var checkRadioLastCar = m_tourModuleCommon_getParam(siteType, options, 'm_defRadioLastCar');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkRadioLastCar, m_domtourModuleCar_checkedArray['m_defRadioLastCar']) === false){
		options['m_defRadioLastCar'] = defaults['m_defRadioLastCar'];
	}
	
	//返車場所（空港営業所-空港選択）
	var checkLastApoAreaApo = m_tourModuleCommon_getParam(siteType, options, 'm_defLastApoAreaApo');
	if(!m_domtourModuleCar_checkedArray['m_defLastApoAreaApo'].test(checkLastApoAreaApo)){
		options['m_defLastApoAreaApo'] = defaults['m_defLastApoAreaApo'];
	}
	
	//返車場所（空港営業所-営業所選択）（取引先）／（施設）／（乗捨エリア）／（営業所番号）
	var checkLastApoOfficeApoCoptCd = m_tourModuleCommon_getParam(siteType, options, 'm_defLastApoOfficeApoCoptCd');
	var checkLastApoOfficeApoFaclCd = m_tourModuleCommon_getParam(siteType, options, 'm_defLastApoOfficeApoFaclCd');
	var checkLastApoOfficeApoAbdAreaCd = m_tourModuleCommon_getParam(siteType, options, 'm_defLastApoOfficeApoAbdAreaCd');
	var checkLastApoOfficeApoNo = m_tourModuleCommon_getParam(siteType, options, 'm_defLastApoOfficeApoNo');
	
	//ひとつでも不正値があったらデフォルト設定
	if(    (!m_domtourModuleCar_checkedArray['m_defLastApoOfficeApoCoptCd'].test(checkLastApoOfficeApoCoptCd))
		|| (!m_domtourModuleCar_checkedArray['m_defLastApoOfficeApoFaclCd'].test(checkLastApoOfficeApoFaclCd))
		|| (!m_domtourModuleCar_checkedArray['m_defLastApoOfficeApoAbdAreaCd'].test(checkLastApoOfficeApoAbdAreaCd))
		|| (!m_domtourModuleCar_checkedArray['m_defLastApoOfficeApoNo'].test(checkLastApoOfficeApoNo))){
		
		options['m_defLastApoOfficeApoCoptCd'] = defaults['m_defLastApoOfficeApoCoptCd'];
		options['m_defLastApoOfficeApoFaclCd'] = defaults['m_defLastApoOfficeApoFaclCd'];
		options['m_defLastApoOfficeApoAbdAreaCd'] = defaults['m_defLastApoOfficeApoAbdAreaCd'];
		options['m_defLastApoOfficeApoNo'] = defaults['m_defLastApoOfficeApoNo'];
	}
	
	//返車場所（市内営業所-エリア選択）（方面）
	var checkLastCityAreaDistrict = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityAreaDistrict');
	var defLastDistrict = defaults['m_defLastCityAreaDistrict'];
	if(checkLastCityAreaDistrict !== undefined && checkLastCityAreaDistrict != defLastDistrict && !m_domtourModuleCar_checkedArray['m_defLastCityAreaDistrict'].test(checkLastCityAreaDistrict)){
		options['m_defLastCityAreaDistrict'] = defaults['m_defLastCityAreaDistrict'];
	}
	
	//返車場所（市内営業所-エリア選択）（地区）
	var checkLastCityAreaRegion = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityAreaRegion');
	var defLastRegion = defaults['m_defLastCityAreaRegion'];
	if(checkLastCityAreaRegion !== undefined && checkLastCityAreaRegion != defLastRegion && !m_domtourModuleCar_checkedArray['m_defLastCityAreaRegion'].test(checkLastCityAreaRegion)){
		options['m_defLastCityAreaRegion'] = defaults['m_defLastCityAreaRegion'];
	}
	
	//返車場所（市内営業所-エリア選択）（地域）
	var checkLastCityAreaArea = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityAreaArea');
	var defLastArea = defaults['m_defLastCityAreaArea'];
	if(checkLastCityAreaArea !== undefined && checkLastCityAreaArea != defLastArea && !m_domtourModuleCar_checkedArray['m_defLastCityAreaArea'].test(checkLastCityAreaArea)){
		options['m_defLastCityAreaArea'] = defaults['m_defLastCityAreaArea'];
	}
	
	//返車場所（市内営業所-営業所選択）（取引先）／（施設）／（乗捨エリア）／（営業所番号）
	var checkLastCityOfficeApoCoptCd = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityOfficeApoCoptCd');
	var checkLastCityOfficeApoFaclCd = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityOfficeApoFaclCd');
	var checkLastCityOfficeApoAbdAreaCd = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityOfficeApoAbdAreaCd');
	var checkLastCityOfficeApoNo = m_tourModuleCommon_getParam(siteType, options, 'm_defLastCityOfficeApoNo');

	//ひとつでも不正値があったらデフォルト設定
	if((!m_domtourModuleCar_checkedArray['m_defLastCityOfficeApoCoptCd'].test(checkLastCityOfficeApoCoptCd))
		|| (!m_domtourModuleCar_checkedArray['m_defLastCityOfficeApoFaclCd'].test(checkLastCityOfficeApoFaclCd))
		|| (!m_domtourModuleCar_checkedArray['m_defLastCityOfficeApoAbdAreaCd'].test(checkLastCityOfficeApoAbdAreaCd))
		|| (!m_domtourModuleCar_checkedArray['m_defLastCityOfficeApoNo'].test(checkLastCityOfficeApoNo))){
		
		options['m_defLastCityOfficeApoCoptCd'] = defaults['m_defLastCityOfficeApoCoptCd'];
		options['m_defLastCityOfficeApoFaclCd'] = defaults['m_defLastCityOfficeApoFaclCd'];
		options['m_defLastCityOfficeApoAbdAreaCd'] = defaults['m_defLastCityOfficeApoAbdAreaCd'];
		options['m_defLastCityOfficeApoNo'] = defaults['m_defLastCityOfficeApoNo'];
	}
	
	//車両クラスから検索
	var checkCarClass = m_tourModuleCommon_getParam(siteType, options, 'm_defCarClassChecked');
	if(checkCarClass instanceof Array){
		var carClass =[];
		for(var i in checkCarClass){
			if(m_domtourModuleCommon_getStrArrayMatchAll(checkCarClass[i], m_domtourModuleCar_checkedArray['m_defCarClassChecked']) === false){
				carClass[i] = '';
			}else{
				carClass[i] = checkCarClass[i];
			}
		}
		options['m_defCarClassChecked'] = carClass;
	}else{
		options['m_defCarClassChecked'] = defaults['m_defCarClassChecked'];
	}
	
	
	return options;
}

//国内ツアー観光の各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_domtourModuleTraffic_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//目的
	var purposeCheck = [];
	for(var i in m_domtourModule_PurposePulldownList){
		purposeCheck[i] = m_domtourModule_PurposePulldownList[i].value;
	}
	
	//不正値チェック設定
	var m_domtourModuleTraffic_checkedArray = {
			m_defTrafficDistrict:/[0-9]/,			//目的地(方面)
			m_defTrafficRegion:/[0-9]/,				//目的地(地区)
			m_defPurposeTraffic:purposeCheck		//目的用
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//目的地地(方面)
	var checkTrafficDistrict = m_tourModuleCommon_getParam(siteType, options, 'm_defTrafficDistrict');
	var defDistrict = defaults['m_defTrafficDistrict'];
	if(checkTrafficDistrict !== undefined && checkTrafficDistrict != defDistrict && !m_domtourModuleTraffic_checkedArray['m_defTrafficDistrict'].test(checkTrafficDistrict)){
		options['m_defTrafficDistrict'] = defaults['m_defTrafficDistrict'];
	}
	//目的地(地区)
	var checkTrafficRegion = m_tourModuleCommon_getParam(siteType, options, 'm_defTrafficRegion');
	var defRegion = defaults['m_defTrafficRegion'];
	if(checkTrafficRegion !== undefined && checkTrafficRegion != defRegion && !m_domtourModuleTraffic_checkedArray['m_defTrafficRegion'].test(checkTrafficRegion)){
		options['m_defTrafficRegion'] = defaults['m_defTrafficRegion'];
	}
	
	//目的
	var checkPurpose = m_tourModuleCommon_getParam(siteType, options, 'm_defPurposeTraffic');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkPurpose, m_domtourModuleTraffic_checkedArray['m_defPurposeTraffic']) === false){
		options['m_defPurposeTraffic'] = defaults['m_defPurposeTraffic'];
	}
	
	return options;
}

//国内ツアーツアー一括の各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_domtourModuleBulk_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//旅行日数
	var numberOfDaysCheck = [];
	for(var i in m_domtourModuleBulk_travelOfDaysList){
		numberOfDaysCheck[i] = m_domtourModuleBulk_travelOfDaysList[i].value;
	}
	
	//人数
	var numberOfPeopleCheck = [];
	for(var i in m_domtourModuleBulk_PeoplesPulldownList){
		numberOfPeopleCheck[i] = m_domtourModuleBulk_PeoplesPulldownList[i].value;
	}
	
	//不正値チェック設定
	var m_domtourModuleBulk_checkedArray = {
			m_defDepArea:/[0-9]/,						//出発地用
			m_defNumberOfDays:numberOfDaysCheck,		//旅行日数用
			m_defNumberOfPeople:numberOfPeopleCheck		//人数用
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//出発地
	var checkDepArea = m_tourModuleCommon_getParam(siteType, options, 'm_defDepArea');
	if(!m_domtourModuleBulk_checkedArray['m_defDepArea'].test(checkDepArea)){
		options['m_defDepArea'] = defaults['m_defDepArea'];
	}
	
	//旅行日数
	var checkNumberOfDays = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfDays');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfDays, m_domtourModuleBulk_checkedArray['m_defNumberOfDays']) === false){
		options['m_defNumberOfDays'] = defaults['m_defNumberOfDays'];
	}
	
	//人数
	var checkNumberOfPeople = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfPeople');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfPeople, m_domtourModuleBulk_checkedArray['m_defNumberOfPeople']) === false){
		options['m_defNumberOfPeople'] = defaults['m_defNumberOfPeople'];
	}
	
	return options;
}

//国内ツアーホテル(3社一括)の各項目不正値チェック処理
//options:チェック対象の各値
//siteType:各モジュール判定、setParam取得用
//defaults:各モジュールデフォルト値
//return:options(チェック後)
function m_domtourModuleHotelBulk_incorrectValue(options, siteType, defaults) {
	
	//////////////////////
	//チェック設定初期化
	//////////////////////
	
	//人数
	var peopleCheck = [];
	for(var i in m_domModuleHotel_PeoplesPulldownList){
		peopleCheck[i] = m_domModuleHotel_PeoplesPulldownList[i].value;
	}
	
	//こだわり条件
	var AddCondition = [];
	for(var i in m_domtourModule_HotelBulkOptionList){
		AddCondition[i] = m_domtourModule_HotelBulkOptionList[i].id;
	}
	

	//不正値チェック設定
	var m_domtourModuleHotelBulk_checkedArray = {
		m_defPlaceOfStayDistrict:/[0-9]/,			//宿泊地(方面)
		m_defPlaceOfStayRegion:/[0-9]/,		 		//宿泊地(地区)
		m_defPlaceOfStayArea:/[0-9]/,				//宿泊地(地域)
		m_defNumberOfPeople:peopleCheck,			//人数用
		m_defAddConditionChecked:AddCondition		//こだわり条件用
	};
	
	////////////////////////
	//各項目不正値チェック
	////////////////////////
	
	//宿泊地(方面)
	var checkPlaceOfStayDistrict = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayDistrict');
	var defDistrict = defaults['m_defPlaceOfStayDistrict'];
	if(checkPlaceOfStayDistrict !== undefined && checkPlaceOfStayDistrict != defDistrict && !m_domtourModuleHotelBulk_checkedArray['m_defPlaceOfStayDistrict'].test(checkPlaceOfStayDistrict)){
		options['m_defPlaceOfStayDistrict'] = defaults['m_defPlaceOfStayDistrict'];
	}
	//宿泊地(地区)
	var checkPlaceOfStayRegion = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayRegion');
	var defRegion = defaults['m_defPlaceOfStayRegion'];
	if(checkPlaceOfStayRegion !== undefined && checkPlaceOfStayRegion != defRegion && !m_domtourModuleHotelBulk_checkedArray['m_defPlaceOfStayRegion'].test(checkPlaceOfStayRegion)){
		options['m_defPlaceOfStayRegion'] = defaults['m_defPlaceOfStayRegion'];
	}
	//宿泊地(地域)
	var checkPlaceOfStayArea = m_tourModuleCommon_getParam(siteType, options, 'm_defPlaceOfStayArea');
	var defArea = defaults['m_defPlaceOfStayArea'];
	if(checkPlaceOfStayArea !== undefined && checkPlaceOfStayArea != defArea && !m_domtourModuleHotelBulk_checkedArray['m_defPlaceOfStayArea'].test(checkPlaceOfStayArea)){
		options['m_defPlaceOfStayArea'] = defaults['m_defPlaceOfStayArea'];
	}
	
	//人数
	var checkNumberOfPeople = m_tourModuleCommon_getParam(siteType, options, 'm_defNumberOfPeople');
	if(m_domtourModuleCommon_getStrArrayMatchAll(checkNumberOfPeople, m_domtourModuleHotelBulk_checkedArray['m_defNumberOfPeople']) === false){
		options['m_defNumberOfPeople'] = defaults['m_defNumberOfPeople'];
	}

	//こだわり条件
	var checkAddCondition = m_tourModuleCommon_getParam(siteType, options, 'm_defAddConditionChecked');
	if(checkAddCondition instanceof Array){
		var addCondition =[];
		for(var i in checkAddCondition){
			if(m_domtourModuleCommon_getStrArrayMatchAll(checkAddCondition[i], m_domtourModuleHotelBulk_checkedArray['m_defAddConditionChecked']) === false){
				addCondition[i] = '';
			}else{
				addCondition[i] = checkAddCondition[i];
			}
		}
		options['m_defAddConditionChecked'] = addCondition;
	}else{
		options['m_defAddConditionChecked'] = defaults['m_defAddConditionChecked'];
	}

	
	return options;
}


//国内旅作 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModuleDp_submitSearch(obj) {
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-domDp');
	
	// リクエストパラメータ調整
	m_domtourModuleDp_arrangeRequest({
		target : target,
		targetForm : targetForm
	});
	
	// 検索実行
	targetForm.submit();
}

//国内旅作 検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_domtourModuleDp_arrangeRequest = function (option){

	var target = option.target;
	var targetForm = option.targetForm;

	/////////////////////////////////
	// 子供・幼児のパラメータ調整

	// パラメータ名変換
	var mealSumTrue = 0;
	var mealSumFalse = 0;
	for(var i=0;i<m_domtourModuleNumberOfPeopleDp_requestParamsMap.name.length;i++){
		var param = targetForm.find('input[name=' + m_domtourModuleNumberOfPeopleDp_requestParamsMap.name[i] + ']').val();
		
		if(param){
			target.tourCommonModule('createInputHidden', {
				site : 'domtour',
				inputName:m_domtourModuleNumberOfPeopleDp_requestParamsMap.param[i],
				inputValue:param
			});
			// 活性・非活性項目確認用集計
			var mealFlg = m_domtourModuleNumberOfPeopleDp_requestParamsMap.meal[i];
			if(mealFlg !== undefined && mealFlg === true){
				mealSumTrue += parseInt(param);
			}else if(mealFlg !== undefined && mealFlg === false){
				mealSumFalse += parseInt(param);
			}else{
				// 集計対象外
			}
		}
		targetForm.find('input[name=' + m_domtourModuleNumberOfPeopleDp_requestParamsMap.name[i] + ']').prop('disabled', true);
	}
	// 活性・非活性項目パラメータ調整
	var mealFlg = undefined;
	if(mealSumTrue > 0 && mealSumFalse <= 0){
		mealFlg = true;
	}else if(mealSumFalse > 0 && mealSumTrue <= 0){
		mealFlg = false;
	}else{
		// 調整対象外
	}
	if(mealFlg !== undefined){
		for(var i=0;i<m_domtourModuleNumberOfPeopleDp_requestParamsMap.param.length;i++){
			if(m_domtourModuleNumberOfPeopleDp_requestParamsMap.meal[i] !== undefined
				&& mealFlg !== m_domtourModuleNumberOfPeopleDp_requestParamsMap.meal[i])
			{
				// 非活性項目のパラメータを空に設定
				targetForm.find('input[name=' + m_domtourModuleNumberOfPeopleDp_requestParamsMap.param[i] + ']').val('');
			}
		}
	}

	/////////////////////////////////
	// ラジオボタンパラメータ調整
	var isChecked = target.find('[name=rcPlanDispFlg]:checked').val();
	if(isChecked === undefined){
		// 未選択の場合はパラメータ有・値空で送信
		targetForm.append('<input type="hidden" name="rcPlanDispFlg" value="">\n');
	}

}

//国内パッケージ(行き先から探す) 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModulePkg_submitSearchDest(obj) {
	
	//送信パラメータの調整、チェック等
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-domPkg');
	
	// リクエストパラメータ調整
	m_domtourModulePkg_arrangeRequest({
		target : target,
		targetForm : targetForm
	});
	
	// 検索実行
	targetForm.submit();
}

//国内旅作 検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_domtourModulePkg_arrangeRequest = function (option){
	
	var target = option.target;
	var targetForm = option.targetForm;
	
	
	//子供・幼児のパラメータ調整
	for(var i=0;i<m_domtourModuleNumberOfPeoplePkg_requestParamsMap.name.length;i++){
		var param = targetForm.find('input[name=' + m_domtourModuleNumberOfPeoplePkg_requestParamsMap.name[i] + ']').val();
		
		if(param){
			target.tourCommonModule('createInputHidden', {
				site : 'domtour',
				inputName:m_domtourModuleNumberOfPeoplePkg_requestParamsMap.param[i],
				inputValue:param
			});
		}
		
		targetForm.find('input[name=' + m_domtourModuleNumberOfPeoplePkg_requestParamsMap.name[i] + ']').prop('disabled', true);
	}
	
	//参加人数合計パラメータ付与(大人と子供（食事・寝具あり）の参加人数合計を指定)
	var personCntList = ['adultCnt','childAPkgCnt','childCPkgCnt'];
	var personCnt =0;
	for (var i in personCntList){
		var itemsParam = target.find("[name="+personCntList[i]+"]").val();
		if(itemsParam!==undefined && itemsParam!==""){
			personCnt = personCnt + parseInt(itemsParam);
		}
	}
	
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'usePersonCnt',
		inputValue:personCnt
	});
}

//国内パッケージ(商品コードから探す) 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModulePkg_submitSearchCourse(obj) {
	
	//送信パラメータの調整、チェック等
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-domPkgCourse');
	
	// リクエストパラメータ調整
	m_domtourModulePkg_arrangeRequestCourse({
		target : target,
		targetForm : targetForm
	});
	
	// 検索実行
	targetForm.submit();
}

//国内パッケージ(商品コードから探す)検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_domtourModulePkg_arrangeRequestCourse = function (option){
	
	var target = option.target;
	var targetForm = option.targetForm;
	
	//コースコード取得
	var courseCodeValue = targetForm.find('.m_courseCode').val();
	
	//お客様がハイフンを入れたらパラメータで除外
	if(/-/.test(courseCodeValue) || /ー/.test(courseCodeValue) || /－/.test(courseCodeValue)){
		courseCodeValue = courseCodeValue.replace(/-/g,'')
										.replace(/ー/g,'')
										.replace(/－/g,'');
	}
	
	//全角を半角に変換
	if(/[Ａ-Ｚａ-ｚ０-９]/.test(courseCodeValue)){
		courseCodeValue = courseCodeValue.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){
		return String.fromCharCode(s.charCodeAt(0)-0xFEE0);
		});
	}
	//小文字を大文字に変換
	courseCodeValue = courseCodeValue.toUpperCase();
	//コースコード欄に反映
	targetForm.find('.m_courseCode').val(courseCodeValue);
	
}

//国内ホテル 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModuleHotel_submitSearch(obj) {
	
	// 送信パラメータの調整、チェック等
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-domHotel');
	var isChildDisabled = obj.isChildDisabled;
	
	// リクエストパラメータ調整
	m_domtourModuleHotel_arrangeRequest({
		target : target,
		site : obj.site,
		targetForm : targetForm,
		isChildDisabled : isChildDisabled
	});
	
	// 検索実行
	targetForm.submit();
}

//国内ホテル検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_domtourModuleHotel_arrangeRequest = function (option){
	
	var target = option.target;
	var targetForm = option.targetForm;
	var criteriaCondParams='';
	
	//こだわり条件のパラメータ調整
	if('domtourtop' === option.site){
		//TOPの場合
		 var checkList = $tour_module_jq(':checkbox[name="m_dom_hotel_option"]')
		 
		 for(i=0;i<checkList.length;i++){
			 if(checkList[i].checked){
				 criteriaCondParams = criteriaCondParams + m_domtourModule_HotelOptionList[i].id+',';
			 }
		 }
		 criteriaCondParams=criteriaCondParams.slice(0, -1);
		
	}else{
		//2ndの場合
		var checkList = targetForm.find('input[name=m_dom_hotel_option]');
		
		
		for(var i=0;i<m_domtourModule_HotelOptionList.length;i++){
			if(checkList[i].checked){
				criteriaCondParams = criteriaCondParams + checkList[i].id.slice(0, -3) +',';
			}
		}
		criteriaCondParams=criteriaCondParams.slice(0, -1);
	}
	// hiddenに値設定・出力
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'criteriaCondId',
		inputValue:criteriaCondParams
	});
	
	
	//子供・幼児のパラメータ調整
	var isChildDisabled = option.isChildDisabled;
	for(var i=0;i<m_domtourModuleNumberOfPeopleHotel_requestParamsMap.name.length;i++){
		var param = targetForm.find('input[name=' + m_domtourModuleNumberOfPeopleHotel_requestParamsMap.name[i] + ']').val();
		
		if(param){
			if(isChildDisabled){
				param = ''; // 非活性時は空で送信
			}
			target.tourCommonModule('createInputHidden', {
				site : 'domtour',
				inputName:m_domtourModuleNumberOfPeopleHotel_requestParamsMap.param[i],
				inputValue:param
			});
		}
		
		targetForm.find('input[name=' + m_domtourModuleNumberOfPeopleHotel_requestParamsMap.name[i] + ']').prop('disabled', true);
	}
	
	
	//参加人数合計パラメータ付与(大人と子供・幼児（食事・寝具あり）の参加人数合計を指定)
	var personCntList = ['adultCnt','childAHtlCnt','childBHtlCnt','childCHtlCnt'];
	var personCnt =0;
	for (var i in personCntList){
		var itemsParam = target.find("[name="+personCntList[i]+"]").val();
		if(itemsParam!==undefined && itemsParam!==""){
			personCnt = personCnt + parseInt(itemsParam);
		}
	}
	
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'usePersonCnt',
		inputValue:personCnt
	});
}

//国内レンタカー 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModuleCar_submitSearch(obj) {
	
	//送信パラメータの調整、チェック等
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-domCar');
	
	// リクエストパラメータ調整
	m_domtourModuleCar_arrangeRequest({
		target : target,
		site : obj.site,
		targetForm : targetForm
	});
	
	// 検索実行
	targetForm.submit();
}

//国内レンタカー検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_domtourModuleCar_arrangeRequest = function (option){
	
	var target = option.target;
	var targetForm = option.targetForm;
	
	var carTypeClassParams='';
	
	//車両クラスのパラメータ調整
	if('domtourtop' === option.site){
		var checkList = $tour_module_jq(':checkbox[name="m_dom_car_class"]:checked').map(function(index, checkbox) {
			  return $tour_module_jq(this).val();
			});
		 
		 for(i=0;i<checkList.length;i++){
			 carTypeClassParams = carTypeClassParams + m_domtourModule_CarClassParamList[m_domtourModule_CarClassList[checkList[i]].id]+',';
		 }
		 carTypeClassParams=carTypeClassParams.slice(0, -1);
		
	}else{
	var checkList = targetForm.find('input[name=m_dom_car_class]');
	
	
	for(var i=0;i<m_domtourModule_CarClassList.length;i++){
		if(checkList[i].checked){
			carTypeClassParams = carTypeClassParams + m_domtourModule_CarClassParamList[checkList[i].id.slice(0, -3)]+',';
		}
	}
	carTypeClassParams=carTypeClassParams.slice(0, -1);
	
	}
	// hiddenに値設定・出力
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'carTypeClassId',
		inputValue:carTypeClassParams
	});
	
	//返車場所パラメータの調整
	var check = target.find('input[name="brbkPlaceCls"]:checked').val();
	
	if(check === "1"){
		//貸出店舗の場合 何もしない
		
	}else{
		//空港or市内営業所の場合
		var officeParam ='';
		
		if(check === "2"){
			//空港営業所の場合
			officeParam = targetForm.find('[name="brbkAirpOffice"]').val();
			
		}else if(check === "3"){
			//市内営業所の場合
			officeParam = targetForm.find('[name="brbkOffice"]').val();
		}
		
		var valueParams = ['','','',''];
		if(officeParam !== ""){
			valueParams = officeParam.split("_");
		}
		
		var sendParams = m_domtourModuleCar_officeParamsMap[check];
		
		//分割した値を送信パラメータに設定
		for(var i in sendParams){
			// hiddenに値設定・出力
			target.tourCommonModule('createInputHidden', {
				site : 'domtour',
				inputName:sendParams[i],
				inputValue:valueParams[i]
			});
		}
		
	} 

	//disabled事前解除
	var prm = m_domtourModuleCar_unnecessaryParamsMap['1'];
	for(var i in prm){
		targetForm.find('[name='+prm[i]+']').prop('disabled', false);
	}

	//不要パラメータは送信しない
	var unnecessaryParams = m_domtourModuleCar_unnecessaryParamsMap[check];
	for(var i in unnecessaryParams){
		targetForm.find('[name='+unnecessaryParams[i]+']').prop('disabled', true);
	}
	
}

//国内観光 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModuleTraffic_submitSearch(obj) {
	
	//送信パラメータの調整、チェック等
	var targetForm = $tour_module_jq(this).closest('.form-domTraffic');
	
	//actionURLの設定
	var optClsParam = targetForm.find('[name="optCls"]').val();
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_domtourModuleTraffic_fromAction[optClsParam];
	targetForm.attr('action', actionURL);
	
	// 検索実行
	targetForm.submit();
}

//国内ツアー一括 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModuleBulk_submitSearch(obj) {
	
	//送信パラメータの調整、チェック等
	var targetForm = $tour_module_jq(this).closest('.form-domBulk');
	
	// 検索実行
	targetForm.submit();
}

//国内ホテル(3社一括) 検索実行関数
//リクエストパラメータの調整(操作)を実施した後、フォームをsubmitする。
function m_domtourModuleHotelBulk_submitSearch(obj) {
	
	// 送信パラメータの調整、チェック等
	var target = obj.targetModule;
	var targetForm = $tour_module_jq(this).closest('.form-domHotelBulk');
	var isChildDisabled = obj.isChildDisabled;
	
	// リクエストパラメータ調整
	m_domtourModuleHotelBulk_arrangeRequest({
		target : target,
		site : obj.site,
		targetForm : targetForm,
		isChildDisabled : isChildDisabled
	});
	
	// 検索実行
	targetForm.submit();
}

//国内ホテル(3社一括)検索ボタン押下時のサブミット前リクエストパラメータ調整
var m_domtourModuleHotelBulk_arrangeRequest = function (option){
	
	var target = option.target;
	var targetForm = option.targetForm;
	var criteriaCondParams='';
	
	//こだわり条件のパラメータ調整
	if('dom' === option.site){
		//TOPの場合
		 var checkList = $tour_module_jq(':checkbox[name="m_dom_hotel_option"]')
		 
		 for(i=0;i<checkList.length;i++){
			 if(checkList[i].checked){
				 criteriaCondParams = criteriaCondParams + m_domtourModule_HotelBulkOptionList[i].id+',';
			 }
		 }
		 criteriaCondParams=criteriaCondParams.slice(0, -1);
		
	}else{
		//2ndの場合
		var checkList = targetForm.find('input[name=m_dom_hotel_bulk_option]');
		
		
		for(var i=0;i<m_domtourModule_HotelBulkOptionList.length;i++){
			if(checkList[i].checked){
				criteriaCondParams = criteriaCondParams + checkList[i].id.slice(0, -8) +',';
			}
		}
		criteriaCondParams=criteriaCondParams.slice(0, -1);
	}
	// hiddenに値設定・出力
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'criteriaCondId',
		inputValue:criteriaCondParams
	});
}

//////////////////////////////////


$tour_module_jq.fn.domtourCommonModule = function(method) {

var methods = {
'submitSearchDp' : m_domtourModuleDp_submitSearch,
'submitSearchPkgDest' : m_domtourModulePkg_submitSearchDest,
'submitSearchPkgCourse' : m_domtourModulePkg_submitSearchCourse,
'submitSearchHotel' : m_domtourModuleHotel_submitSearch,
'submitSearchCar' : m_domtourModuleCar_submitSearch,
'submitSearchTraffic' : m_domtourModuleTraffic_submitSearch,
'submitSearchBulk' : m_domtourModuleBulk_submitSearch,
'submitSearchHotelBulk' : m_domtourModuleHotelBulk_submitSearch

};

// Dispatch the proper method
if (methods[method]) {
return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
} else {
$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtourCommonModule');
}
};

