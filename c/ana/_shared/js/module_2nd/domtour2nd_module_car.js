/*=====================================================
* Script Name: domtour2nd_module_car.js
* Description: 空席照会モジュール 国内ツアー2nd レンタカー
* Version: 1.00
* Last Up Date: 2017/10/11
=====================================================*/

var m_domtour2ndModuleCar_siteType = { site : 'domtour2nd', module : 'car' };

var m_domtour2ndModuleCar_usePartsCount_FirstCarRadio = 1;		//配車場所部品、配置数カウンタ
var m_domtour2ndModuleCar_usePartsCount_LastCarRadio = 1;		//返車場所部品、配置数カウンタ
var m_domtour2ndModuleCar_usePartsCount_CarClassCheckBox = 1;		//車両クラス部品、配置数カウンタ

//初期値設定
var m_domtour2ndModuleCar_defaults = {
	m_defDateFirstCar:'',					//配車日用
	m_defTimeFirstCar:'0900',				//配車時間用
	m_defDateLastCar:'',					//返車日用
	m_defTimeLastCar:'1800',				//返車時間用
	m_defRadioFirstCar:'1',					//配車場所ラジオ用
	m_defFirstApoAreaApo:'',				//配車場所 空港エリア
	m_defFirstCityAreaDistrict:'',			//配車場所 市内エリア_方面
	m_defFirstCityAreaRegion:'',			//配車場所 市内エリア_地区
	m_defFirstCityAreaArea:'',				//配車場所 市内エリア_地域
	m_defRadioLastCar:'1',					//返車場所ラジオ用
	
	m_defLastApoAreaApo:'',					//返車場所 空港営業所
	m_defLastApoOfficeApoCoptCd:'',			//返車場所 空港営業所・営業所 取引先コード
	m_defLastApoOfficeApoFaclCd:'',			//返車場所 空港営業所・営業所 施設コード
	m_defLastApoOfficeApoAbdAreaCd:'',		//返車場所 空港営業所・営業所 乗捨エリアコード
	m_defLastApoOfficeApoNo:'',				//返車場所 空港営業所・営業所 営業所番号
	
	m_defLastCityAreaDistrict:'',			//返車場所 市内営業所_方面
	m_defLastCityAreaRegion:'',				//返車場所 市内営業所_地区
	m_defLastCityAreaArea:'',				//返車場所 市内営業所_地域
	m_defLastCityOfficeApoCoptCd:'',		//返車場所 市内営業所・営業所 取引先コード
	m_defLastCityOfficeApoFaclCd:'',		//返車場所 市内営業所・営業所 施設コード
	m_defLastCityOfficeApoAbdAreaCd:'',		//返車場所 市内営業所・営業所 乗捨エリアコード
	m_defLastCityOfficeApoNo:'',			//返車場所 市内営業所・営業所 営業所番号

	m_defCarClassChecked:[''],				//車両クラスから検索用
	m_defTabNoDispCls:''					//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleCar_optionParam = $tour_module_jq.extend(m_domtour2ndModuleCar_optionParam, m_domtour2ndModuleCar_defaults);

/**
 * 国内ツアーレンタカー2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModuleCar_setParams(options){
	m_domtour2ndModuleCar_optionParam = $tour_module_jq.extend(m_domtour2ndModuleCar_optionParam, options);
};

/**
 * 国内ツアーレンタカー2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleCar_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアーレンタカーの各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModuleCar_incorrectValue(options, m_domtour2ndModuleCar_siteType, m_domtour2ndModuleCar_defaults));
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//配車日時
	var firstDateCarText = m_tourModuleCommon_getListValue(m_domtourModuleCar_firstDateCar, 'Text');
	//返車日時
	var lastDateCarText = m_tourModuleCommon_getListValue(m_domtourModuleCar_lastDateCar, 'Text');
	//配車場所
	var firstPlaceCarText = m_tourModuleCommon_getListValue(m_domtourModuleCar_firstPlaceCar, 'Text');
	//返車場所
	var lastPlaceCarText = m_tourModuleCommon_getListValue(m_domtourModuleCar_lastPlaceCar, 'Text');
	//車両クラスから検索
	var carClassLabelText1 = m_tourModuleCommon_getListValue(m_domtourModuleCar_carClassLabel, 'Text1');
	var carClassLabelText2 = m_tourModuleCommon_getListValue(m_domtourModuleCar_carClassLabel, 'Text2');
	//検索ボタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModuleCar_fromAction, 'URL');
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');
	
	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defFormMethod');
	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<form class="form-domCar" id="form-domCar" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<div class="mod-content_wrap">\n';
	html += '<div class="mod-content_cover col3">\n';
	
	
	//1カラム目 配車日時 返車日時
	html += '<div class="mod-content_box">\n';
	
	//配車日時
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + firstDateCarText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_firstDayCar">\n';
	//配車日時（日）部品埋め込み
	html += '</div>\n';
	html += '<div class="mod-input_parts m_firstTimeCar">\n';
	//配車日時（時）部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//返車日時
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + lastDateCarText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_lastDayCar">\n';
	//返車日時（日）部品埋め込み
	html += '</div>\n';
	html += '<div class="mod-input_parts m_lastTimeCar">\n';
	//返車日時（時）部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//1カラム目 END
	
	
	//2カラム目 配車場所 返車場所
	html += '<div class="mod-content_box">\n';
	
	//配車場所
	html += '<fieldset class="mod-input_box not-column">\n';
	html += '<legend><span class="mod-input_head bold">' + firstPlaceCarText + '</span></legend>\n';
	html += '<div class="mod-input_parts js-mod-tab m_firstPlaceCar">\n';
	//配車場所部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//返車場所
	html += '<fieldset class="mod-input_box not-column">\n';
	html += '<legend><span class="mod-input_head bold">' + lastPlaceCarText + '</span></legend>\n';
	html += '<div class="mod-input_parts js-mod-tab m_lastPlaceCar">\n';
	//返車場所部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 END
	
	
	//3カラム目 車両クラス 検索ボタン
	html += '<div class="mod-content_box mod-btn_box">\n';
	
	//車両クラス
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head isSPvtop bold">' + carClassLabelText1 + '<span class="ib bold">' + carClassLabelText2 + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_carClass">\n';
	//車両クラス部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '<div class="btn btn-search" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//3カラム目 END
	
	
	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</div><!-- / .mod-content_wrap -->\n';
	html += '</form>\n';
	html += '\n';

	this.append(html);
	
	m_domtour2ndModuleCar_setDefVal(this, options);
	
};

function m_domtour2ndModuleCar_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defModulePosition');
	
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	//パラメータ設定を取得
	//配車日
	 var defDateFirstCar = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defDateFirstCar');
	//配車時間
	 var defTimeFirstCar = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defTimeFirstCar');
	//返車日
	 var defDateLastCar = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defDateLastCar');
	//配車時間
	 var defTimeLastCar = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defTimeLastCar');
	//車両クラス
	var defCarClassChecked = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defCarClassChecked');
	// 再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defTabNoDispCls');
	
	//配車日
	target.find('.m_firstDayCar').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleCar_firstDateCar, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defDateFirstCar,
		defSelectDay : m_domtourModuleCar_firstDay_defSelectDay,
		dateValueClass : m_domtourModuleCar_firstDay_dateValueClass,
		dateValueName : m_domtourModuleCar_firstDay_dateValueName,
		inputPlaceholder : m_domtourModuleCar_firstDay_inputPlaceholder,
		inputClass : m_domtourModuleCar_firstDay_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModuleCar_firstDay_isRequired,
		isDisabled : m_domtourModuleCar_firstDay_isDisabled,
		isDispAllOfMonth : m_domtourModuleCar_firstDay_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleCar_firstDay_prevNextInterval,
		receiptDisableDays : m_domtourModuleCar_firstDay_receiptDisableDays,
		dispMonths : m_domtourModuleCar_firstDay_dispMonths,
		selectableMonths : m_domtourModuleCar_firstDay_selectableMonths,
		isDispHoliday : m_domtourModuleCar_firstDay_isDispHoliday,
		boundaryTime : m_domtourModuleCar_firstDay_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//配車時
	target.find('.m_firstTimeCar').tourCommonModule('createTimePull', {
		site : m_domtour2ndModuleCar_siteType.site,
		inputClass:'',
		inputId:'dom_car_start_time',
		defSelect:defTimeFirstCar,
		inputName:'allocTm'
	});
	
	//返車日
	domCarLastDateOptions = {
			parent : target,
			balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleCar_lastDateCar, 'BalloonTitle'),
			defPartsPosition : dataControlPointValue,
			defDate : defDateLastCar,
			defSelectDay : m_domtourModuleCar_lastDay_defSelectDay,
			dateValueClass : m_domtourModuleCar_lastDay_dateValueClass,
			dateValueName : m_domtourModuleCar_lastDay_dateValueName,
			inputPlaceholder : m_domtourModuleCar_lastDay_inputPlaceholder,
			inputClass : m_domtourModuleCar_lastDay_inputClass,
			uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 1),
			isRequired : m_domtourModuleCar_lastDay_isRequired,
			isDisabled : m_domtourModuleCar_lastDay_isDisabled,
			isDispAllOfMonth : m_domtourModuleCar_lastDay_isDispAllOfMonth,
			prevNextInterval : m_domtourModuleCar_lastDay_prevNextInterval,
			receiptDisableDays : m_domtourModuleCar_lastDay_receiptDisableDays,
			dispMonths : m_domtourModuleCar_lastDay_dispMonths,
			selectableMonths : m_domtourModuleCar_lastDay_selectableMonths,
			isDispHoliday : m_domtourModuleCar_lastDay_isDispHoliday,
			boundaryTime : m_domtourModuleCar_lastDay_boundaryTime,
			startDateClass : m_domtourModuleCar_lastDay_startDateClass,
			daysFromStartDate : m_domtourModuleCar_lastDay_daysFromStartDate,
			spWidth : m_tour2ndModuleCommon_SP_WIDTH,
			maxTravelDays : m_domtourModuleCar_lastDay_maxTravelDays,
			returnDaysFromDepartureDate : m_domtourModuleCar_lastDay_returnDaysFromDepartureDate,
			sameDayFlag : m_domtourModuleCar_lastDay_sameDayFlag
		}
	target.find('.m_lastDayCar').tourCommonModule('createDateInput', domCarLastDateOptions);
	
	//返車時
	target.find('.m_lastTimeCar').tourCommonModule('createTimePull', {
		site : m_domtour2ndModuleCar_siteType.site,
		inputClass:'',
		inputId:'dom_car_return_time',
		defSelect:defTimeLastCar,
		inputName:'brbkTm'
	});
	
	//配車場所
	target.find('.m_firstPlaceCar').domtour2ndModuleCar('privateCreateFirstPlaceCar', options);
	
	//返車場所
	target.find('.m_lastPlaceCar').domtour2ndModuleCar('privateCreateLastPlaceCar', options);
	
	//車両クラス
	target.find('.m_carClass').tourCommonModule('createCheckboxList', {
		site:m_domtour2ndModuleCar_siteType.site,
		module : m_domtour2ndModuleCar_siteType.module,
		defCheck : defCarClassChecked,
		uniqueKey : m_tour2ndModuleCar_returnCount_CarClassCheckBox(),
		inputName:'m_dom_car_class'
	});
	
	//配車場所・返車場所ラジオボタンイベント
	$tour_module_jq(document).on('click keydown', '.m_js-mod-tab_btn_car', function (e) {
		var $modBox = $tour_module_jq(this).closest('.module-cont');
		var $modTabBox = $tour_module_jq(this).closest('.js-mod-tab');
		var $modTabBtn = $modTabBox.find('.js-mod-tab_btn');
		var $modTabContents = $modTabBox.children('.js-mod-tab_contents');
		var btnIndex = $modTabBtn.index(this);
		$modTabContents.removeClass('is-active');
		$modTabContents.eq(btnIndex).addClass('is-active');
	});
	
	
	
	//連動処理関連、初期値保持
	//配車日
	var domCarFirstDate_befor = target.find('.m_domCarFirstDate').val();
	
	//配車日項目変更イベント登録（配車日に連動する返車日）
	target.find('.m_domCarFirstDateText').on('change', function(e){
		
		var domCarFirstDate = target.find('.m_domCarFirstDate').val();
		
		//取得前と取得後に変更がない場合は、イベント終了
		if(domCarFirstDate_befor !== domCarFirstDate){
			//返車日の連動
			//取得
			var m_domCarLastDate = target.find('.m_domCarLastDate');
			var m_domCarLastDateText = target.find('.m_domCarLastDateText');
			//計算
			var carLastDaysFromCarFirstDate = m_tourModuleDateUtil.prototype.calcRetDaysFromStartDate(domCarLastDateOptions);
			var carLastDate = m_tourModuleCalender_PCLP.SubtractDate(domCarFirstDate, carLastDaysFromCarFirstDate);
			var carLastDateText = m_tourModuleCalender_PCLP.FormatDate(carLastDate);
			//更新
			m_domCarLastDate.val(m_tourModuleCalender_PCLP.GetDateText(carLastDate));
			m_domCarLastDateText.val(carLastDateText);
			
			//更新前項目情報の更新
			domCarFirstDate_befor = domCarFirstDate;
		}else{
			//イベント終了
		}
	});
	
	// 検索ボタン押下イベント登録：サブミット実行
	target.find('.btn-search').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			$tour_module_jq(this).domtourCommonModule('submitSearchCar', {
				targetModule : target
			});
		}
	});
	
	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}
	
	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});
	
	
	// 部品配置数カウントアップ
	
	m_tourCommonModuleParts_setPartsCount('calendar', 2);
};

/**
 * 配車場所モジュール処理
 */
function m_domtour2ndModuleCar_createFirstPlaceCar(options) {
	
	////////////////
	// 初期化設定
	////////////////
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//配車場所
	var firstPlaceCarText1 = m_tourModuleCommon_getListValue(m_domtourModuleCar_firstPlaceCar, 'SubText1');
	var firstPlaceCarText2 = m_tourModuleCommon_getListValue(m_domtourModuleCar_firstPlaceCar, 'SubText2');
	
	//モジュール配置数
	var moduleNum = m_tour2ndModuleCar_returnCount_FirstCarRadio();
	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';
	
	//ラジオボタン
	html += '<ul class="mod-radio-group_wrap hz-type">\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_car" type="radio" id="dom_car_ariport_' + moduleNum + '" value="1" name="allocPlaceCls">\n';
	html += '<label tabindex="0" for="dom_car_ariport_' + moduleNum + '">' + firstPlaceCarText1 + '</label>\n';
	html += '</li>\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_car" type="radio" id="dom_car_city_' + moduleNum + '" value="2" name="allocPlaceCls">\n';
	html += '<label tabindex="0" for="dom_car_city_' + moduleNum + '">' + firstPlaceCarText2 + '</label>\n';
	html += '</li>\n';
	html += '</ul>\n';
	
	
	html += '<div class="js-mod-tab_contents mod-tab_contents ariport m_firstPlaceCar_apoArea ">\n';
	//空港エリア一覧部品埋め込み
	html += '</div><!-- / . js-mod-tab_contents mod-tab_contents -->\n';
	
	html += '<div class="js-mod-tab_contents mod-tab_contents city m_firstPlaceCar_cityArea">\n';
	//市内エリア一覧部品埋め込み
	html += '</div><!-- / . js-mod-tab_contents mod-tab_contents -->\n';
	
	this.append(html);
	
	//初期選択制御
	var isRadioCheck = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defRadioFirstCar');
	if(isRadioCheck === '1'){
		this.find('#dom_car_ariport_' + moduleNum + '').attr('checked', 'checked');
		this.find('.m_firstPlaceCar_apoArea').addClass('is-active');
	}else if(isRadioCheck === '2'){
		this.find('#dom_car_city_' + moduleNum + '').attr('checked', 'checked');
		this.find('.m_firstPlaceCar_cityArea').addClass('is-active');
	}
	
	m_domtour2ndModuleFirstPlaceCar_setDefVal(this, options);

};

function m_domtour2ndModuleFirstPlaceCar_setDefVal(target, options){
	
	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defModulePosition');
	
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	// 配車場所 空港エリア
	var defFirstApoAreaApo       = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defFirstApoAreaApo');
	var defFirstCityAreaDistrict = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defFirstCityAreaDistrict');
	var defFirstCityAreaRegion   = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defFirstCityAreaRegion');
	var defFirstCityAreaArea     = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defFirstCityAreaArea');
	
	// 配車場所 空港エリア
	target.find('.m_firstPlaceCar_apoArea').tourCommonModule('createAirportInput', {
		parent           : this,
		defAirportCode   : defFirstApoAreaApo,
		defPartsPosition : dataControlPointValue,
		balloonTitle     : m_domtour2ndModuleCar_firstApoArea['Text'],
		areaClass        : 'm_hiddenFirstApoAreaArea',
		countryClass     : 'm_hiddenFirstApoAreaCountry',
		apoCodeClass     : 'm_hiddenFirstApoAreaApo',
		apoCodeName      : 'allocAirpCd',
		inputPlaceholder : m_domtour2ndModuleCar_firstApoArea['Placeholder'],
		inputClass       : 'm_domCarFirstCarApoAreaText',
		isRequired       : false,
		isDisabled       : false,
		layoutType       : '20',
		uniqueKey        : m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType          : '2'
	});
	
	//配車場所 市内エリア
	target.find('.m_firstPlaceCar_cityArea').tourCommonModule('createAirportInput', {
		parent           : this,
		defAreaCode      : defFirstCityAreaDistrict,// 方面
		defCountryCode   : defFirstCityAreaRegion,  // 地区
		defAirportCode   : defFirstCityAreaArea,    // 地域
		defPartsPosition : dataControlPointValue,
		balloonTitle     : m_domtour2ndModuleCar_firstCityArea['Text'],
		areaClass        : 'm_hiddenFirstCityAreaDistrict', // 方面
		countryClass     : 'm_hiddenFirstCityAreaRegion',   // 地区
		apoCodeClass     : 'm_hiddenFirstCityAreaArea',     // 地域
		areaName         : 'allocDistrictCd',               // 方面
		countryName      : 'allocRegionCd',                 // 地区
		apoCodeName      : 'allocAreaCd',                   // 地域
		inputPlaceholder : m_domtour2ndModuleCar_firstCityArea['Placeholder'],
		inputClass       : 'm_domCarFirstCarCityAreaText',
		isRequired       : false,
		isDisabled       : false,
		layoutType       : '22',
		uniqueKey        : m_tourCommonModuleParts_getPartsCount('airport', 1),
		apoType          : '2',
	});
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 2);
};

/**
 * 返車場所モジュール処理
 */
function m_domtour2ndModuleCar_createLastPlaceCar(options) {
	
	////////////////
	// 初期化設定
	////////////////
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//返車場所
	var lastPlaceCarText1 = m_tourModuleCommon_getListValue(m_domtourModuleCar_lastPlaceCar, 'SubText1');
	var lastPlaceCarText2 = m_tourModuleCommon_getListValue(m_domtourModuleCar_lastPlaceCar, 'SubText2');
	var lastPlaceCarText3 = m_tourModuleCommon_getListValue(m_domtourModuleCar_lastPlaceCar, 'SubText3');
	
	//モジュール配置数
	var moduleNum = m_tour2ndModuleCar_returnCount_LastCarRadio();

	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';
	
	//ラジオボタン
	html += '<ul class="mod-radio-group_wrap hz-type">\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_car" type="radio" id="dom_car_return01_' + moduleNum + '" value="1" name="brbkPlaceCls">\n';
	html += '<label tabindex="0" for="dom_car_return01_' + moduleNum + '">' + lastPlaceCarText1 + '</label>\n';
	html += '</li>\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_car" type="radio" id="dom_car_return02_' + moduleNum + '" value="2" name="brbkPlaceCls">\n';
	html += '<label tabindex="0" for="dom_car_return02_' + moduleNum + '">' + lastPlaceCarText2 + '</label>\n';
	html += '</li>\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_car" type="radio" id="dom_car_return03_' + moduleNum + '" value="3" name="brbkPlaceCls">\n';
	html += '<label tabindex="0" for="dom_car_return03_' + moduleNum + '">' + lastPlaceCarText3 + '</label>\n';
	html += '</li>\n';
	html += '</ul>\n';

	html += '<div class="js-mod-tab_contents mod-tab_contents return01 m_lastPlaceCar_sameFirst">\n';
	html += '</div><!-- / . js-mod-tab_contents mod-tab_contents -->\n';
	
	html += '<div class="js-mod-tab_contents mod-tab_contents return02 ">\n';
	html += '<div class="m_lastPlaceCar_apoArea1">\n';
	// 返車・空港営業所         部品埋め込み
	html += '</div>\n';
	html += '<div class="m_lastPlaceCar_apoArea2">\n';
	// 返車・空港営業所・営業所 部品埋め込み
	html += '</div>\n';
	html += '</div><!-- / . js-mod-tab_contents mod-tab_contents -->\n';
	
	html += '<div class="js-mod-tab_contents mod-tab_contents return03">\n';
	html += '<div class="m_lastPlaceCar_cityArea1">\n';
	// 返車・市内営業所         部品埋め込み
	html += '</div>\n';
	html += '<div class="m_lastPlaceCar_cityArea2">\n';
	// 返車・市内営業所・営業所 部品埋め込み
	html += '</div>\n';
	html += '</div><!-- / . js-mod-tab_contents mod-tab_contents -->\n';
	
	this.append(html);
	
	//初期選択制御
	var isRadioCheck = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defRadioLastCar');
	if(isRadioCheck === '1'){
		this.find('#dom_car_return01_' + moduleNum + '').attr('checked', 'checked');
		this.find('.m_lastPlaceCar_sameFirst').addClass('is-active');
	}else if(isRadioCheck === '2'){
		this.find('#dom_car_return02_' + moduleNum + '').attr('checked', 'checked');
		this.find('.return02').addClass('is-active');
	}else if(isRadioCheck === '3'){
		this.find('#dom_car_return03_' + moduleNum + '').attr('checked', 'checked');
		this.find('.return03').addClass('is-active');
	}
	
	m_domtour2ndModuleLastPlaceCar_setDefVal(this, options);

};

function m_domtour2ndModuleLastPlaceCar_setDefVal(target, options){
	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defModulePosition');
	
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	// 返車場所 空港営業所
	var defLastApoAreaApo             = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastApoAreaApo');
	var defLastApoOfficeApoCoptCd     = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastApoOfficeApoCoptCd');
	var defLastApoOfficeApoFaclCd     = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastApoOfficeApoFaclCd');
	var defLastApoOfficeApoAbdAreaCd  = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastApoOfficeApoAbdAreaCd');
	var defLastApoOfficeApoNo         = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastApoOfficeApoNo');
	var arrLastApoOfficeApo           = [defLastApoOfficeApoCoptCd, defLastApoOfficeApoFaclCd, defLastApoOfficeApoAbdAreaCd, defLastApoOfficeApoNo];
	var defLastApoOfficeApo           = arrLastApoOfficeApo.join('').length > 0 ? arrLastApoOfficeApo.join('_') : '';

	var defLastCityAreaDistrict = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityAreaDistrict');
	var defLastCityAreaRegion   = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityAreaRegion');
	var defLastCityAreaArea     = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityAreaArea');
	var defLastCityOfficeApoCoptCd    = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityOfficeApoCoptCd');
	var defLastCityOfficeApoFaclCd    = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityOfficeApoFaclCd');
	var defLastCityOfficeApoAbdAreaCd = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityOfficeApoAbdAreaCd');
	var defLastCityOfficeApoNo        = m_tourModuleCommon_getParam(m_domtour2ndModuleCar_siteType, options, 'm_defLastCityOfficeApoNo');
	var arrLastCityOfficeApo          = [defLastCityOfficeApoCoptCd, defLastCityOfficeApoFaclCd, defLastCityOfficeApoAbdAreaCd, defLastCityOfficeApoNo];
	var defLastCityOfficeApo          = arrLastCityOfficeApo.join('').length > 0 ? arrLastCityOfficeApo.join('_') : '';

	//返車場所 空港営業所
	target.find('.m_lastPlaceCar_apoArea1').tourCommonModule('createAirportInput', {
		parent              : this,
		defAirportCode      : defLastApoAreaApo,
		defPartsPosition    : dataControlPointValue,
		balloonTitle        : m_domtour2ndModuleCar_lastApoArea['Text'],
		areaClass           : 'm_hiddenLastApoAreaArea',
		countryClass        : 'm_hiddenLastApoAreaCountry',
		apoCodeClass        : 'm_hiddenLastApoAreaApo',
		apoCodeName         : 'brbkAirpCd',
		inputPlaceholder    : m_domtour2ndModuleCar_lastApoArea['Placeholder'],
		inputClass          : 'm_domCarLastCarApoAreaText',
		isRequired          : false,
		isDisabled          : false,
		layoutType          : '21',
		uniqueKey           : m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType             : '2'
	});

	//返車場所・空港営業所・営業所
	target.find('.m_lastPlaceCar_apoArea2').tourCommonModule('createAirportInput', {
		parent              : this,
		defAirportCode      : defLastApoOfficeApo,
		defPartsPosition    : dataControlPointValue,
		balloonTitle        : m_domtour2ndModuleCar_lastApoOffice['Text'],
		filterApoCodeClass  : 'm_hiddenLastApoAreaApo',
		areaClass           : 'm_hiddenLastApoOfficeArea',
		countryClass        : 'm_hiddenLastApoOfficeCountry',
		apoCodeClass        : 'm_hiddenLastApoOfficeApo',
		apoCodeName         : 'brbkAirpOffice',
		inputPlaceholder    : m_domtour2ndModuleCar_lastApoOffice['Placeholder'],
		inputClass          : 'm_domCarOfficeLastCarApoAreaText',
		isRequired          : false,
		isDisabled          : true,
		layoutType          : '24',
		uniqueKey           : m_tourCommonModuleParts_getPartsCount('airport', 1),
		apoType             : '2'
	});
	
	//返車場所・市内営業所
	target.find('.m_lastPlaceCar_cityArea1').tourCommonModule('createAirportInput', {
		parent              : this,
		defAreaCode         : defLastCityAreaDistrict,  // 方面
		defCountryCode      : defLastCityAreaRegion,    // 地区
		defAirportCode      : defLastCityAreaArea,      // 地域
		defPartsPosition    : dataControlPointValue,
		balloonTitle        : m_domtour2ndModuleCar_lastCityArea['Text'],
		areaClass           : 'm_hiddenLastCityAreaDistrict', 	// 方面
		countryClass        : 'm_hiddenLastCityAreaRegion',     // 地区
		apoCodeClass        : 'm_hiddenLastCityAreaArea',       // 地域
		areaName            : 'brbkDistrictCd',                 // 方面
		countryName         : 'brbkRegionCd',                   // 地区
		apoCodeName         : 'brbkAreaCd',                     // 地域
		inputPlaceholder    : m_domtour2ndModuleCar_lastCityArea['Placeholder'],
		inputClass          : 'm_domCarLastCarCityAreaText',
		isRequired          : false,
		isDisabled          : false,
		layoutType          : '23',
		uniqueKey           : m_tourCommonModuleParts_getPartsCount('airport', 2),
		apoType             : '2',
	});

	//返車場所・市内営業所・営業所
	target.find('.m_lastPlaceCar_cityArea2').tourCommonModule('createAirportInput', {
		parent              : this,
		defAirportCode      : defLastCityOfficeApo,
		defPartsPosition    : dataControlPointValue,
		balloonTitle        : m_domtour2ndModuleCar_lastCityOffice['Text'],
		filterApoCodeClass  : 'm_hiddenLastCityAreaArea',
		areaClass           : 'm_hiddenLastCityOfficeArea',
		countryClass        : 'm_hiddenLastCityOfficeCountry',
		apoCodeClass        : 'm_hiddenLastCityOfficeApo',
		apoCodeName         : 'brbkOffice',
		inputPlaceholder    : m_domtour2ndModuleCar_lastCityOffice['Placeholder'],
		inputClass          : 'm_domCarLastCarCityOfficeAreaText',
		isRequired          : false,
		isDisabled          : true,
		layoutType          : '25',
		uniqueKey           : m_tourCommonModuleParts_getPartsCount('airport', 3),
		apoType             : '2'
	});
	
	// 返車場所 空港営業所項目変更イベント登録
	target.find('.m_hiddenLastApoAreaApo').on('onChangeAirport', function(e){
		// 変更後の値取得
		var strArrArea = $tour_module_jq(this).val();
		// 変更前の値取得（デフォルト設定を考慮し、undefinedの場合は変更後を設定。）
		var strBefArea = e.beforeAirport === undefined ? strArrArea : e.beforeAirport;
		
		// 変更された場合
		if (strBefArea !== strArrArea) {
			// 子リスト初期化
			target.find('.m_hiddenLastApoOfficeArea').val("");
			target.find('.m_hiddenLastApoOfficeCountry').val("");
			target.find('.m_hiddenLastApoOfficeApo').val("");
			target.find('.m_domCarOfficeLastCarApoAreaText').val("");
		}
		
		// データがない場合（指定なし）、子リスト非活性化にする
		target.find('.m_domCarOfficeLastCarApoAreaText').prop('disabled', _.isEmpty(strArrArea));
	});
	target.find('.m_hiddenLastApoAreaApo').trigger('onChangeAirport'); // 初期化設定
	
	// 返車場所 市内営業所項目変更イベント登録
	target.find('.m_hiddenLastCityAreaArea').on('onChangeAirport', function(e){
		// 変更後の値取得
		var strArrArea = $tour_module_jq(this).val();
		// 変更前の値取得（デフォルト設定を考慮し、undefinedの場合は変更後を設定。）
		var strBefArea = e.beforeAirport === undefined ? strArrArea : e.beforeAirport;
		
		// 変更された場合
		if (strBefArea !== strArrArea) {
			// 子リスト初期化
			target.find('.m_hiddenLastCityOfficeArea').val("");
			target.find('.m_hiddenLastCityOfficeCountry').val("");
			target.find('.m_hiddenLastCityOfficeApo').val("");
			target.find('.m_domCarLastCarCityOfficeAreaText').val("");
		}
		
		// データがない場合（指定なし）、子リスト非活性化にする
		target.find('.m_domCarLastCarCityOfficeAreaText').prop('disabled', _.isEmpty(strArrArea));
	});
	target.find('.m_hiddenLastCityAreaArea').trigger('onChangeAirport'); // 初期化設定
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 4);
};

//車両クラスボックスリスト配置数カウント
function m_tour2ndModuleCar_returnCount_CarClassCheckBox(){
	
	var returnCount = m_domtour2ndModuleCar_usePartsCount_CarClassCheckBox;
	m_domtour2ndModuleCar_usePartsCount_CarClassCheckBox++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

//配車場所配置数カウント
function m_tour2ndModuleCar_returnCount_FirstCarRadio(){
	
	var returnCount = m_domtour2ndModuleCar_usePartsCount_FirstCarRadio;
	m_domtour2ndModuleCar_usePartsCount_FirstCarRadio++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

//返車場所配置数カウント
function m_tour2ndModuleCar_returnCount_LastCarRadio(){
	
	var returnCount = m_domtour2ndModuleCar_usePartsCount_LastCarRadio;
	m_domtour2ndModuleCar_usePartsCount_LastCarRadio++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

$tour_module_jq.fn.domtour2ndModuleCar = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleCar_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleCar_secondModuleDisplay,
		'privateCreateFirstPlaceCar' : m_domtour2ndModuleCar_createFirstPlaceCar,
		'privateCreateLastPlaceCar' : m_domtour2ndModuleCar_createLastPlaceCar
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleCar');
	}
};
