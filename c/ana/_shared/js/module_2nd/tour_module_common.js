/*=====================================================
* Script Name: tour_module_common.js
* Description: 空席照会モジュール ツアー共通処理
* Version: 1.1
* Last Up Date: 2017/10/31
=====================================================*/

// 定数定義
//--------------------------------------------------------------------各種ドメイン
var m_tourModuleCommon_ASW_DOMAIN = 'https://www.ana.co.jp';
//var m_tourModuleCommon_oldBE_DOMAIN = 'https://rps.atour.ana.co.jp';	// 本番のためテスト系では使用しないこと
var m_tourModuleCommon_oldBE_DOMAIN = 'https://www.ana.co.jp';	// 暫定対応
var m_tourModuleCommon_CAM_DOMAIN = 'https://cam.ana.co.jp';

/////////////////////////
// 各部品初期化
/////////////////////////

// カレンダー用定義
var m_tourModuleCalendar_commonContoroller = null;
if (typeof(m_tourModuleCalendarContoroller) === 'function') {
  m_tourModuleCalendar_commonContoroller = new m_tourModuleCalendarContoroller();
}
// 参加人数モジュールコントローラ
var m_tourModuleNumberOfPeople_commonContoroller = null;
if (typeof(m_tourModuleNumberOfPeopleContoroller) === 'function') {
  m_tourModuleNumberOfPeople_commonContoroller = new m_tourModuleNumberOfPeopleContoroller();
}

// 部品使用カウンタ
var m_tourModuleStayCityRadio_usePartsCount = 1;
var m_tourModuleNumberOfPeople_usePartsCount = 1;
var m_tourModuleAirport_usePartsCount = 1;
var m_tourModuleCalendar_usePartsCount = 1;

//AdobeTargetのパラメータ格納
var m_tourModuleCommon_setTargetValue_params;

//再検索時のパラメータ格納
var m_tourModuleCommon_setResearchValue_params;

//初期値設定
var m_tourModuleCommon_siteType = { site : 'common', module : 'common' };
var m_tourModuleCommon_defaults = {
	// 都市滞在ラジオボタン部品
	 m_stayCityCountList : m_inttourModuleDp_stayCityCountList
	,m_defStayCityCount : '0'
	// 座席クラス
	,m_defHtmlType : 'sec'
	,m_defClassFareList : m_inttourModuleDp_classFareList
	,m_defInputObject : ''
	,m_defClassFareValue : '0'
	,m_changeListEventName : ''
	,m_changeListEventObject : ''
	,m_selectListObject1 : ''
	,m_selectListObject2 : ''
	// 部屋数部品
	,m_numberOfRoomsList : m_inttourModuleDp_numberOfRoomsList
	,m_numberOfRoomsValue : '1'
	// 参加人数部品
	,moduleNo : '999'
};

//カレンダー部品デフォルト設定
var m_tourModuleCalender_siteType = { site : 'common', module : 'calendar' };
var m_tourModuleCalendar_defaults = {
	 name:'cal'												// ポップアップカレンダー名  + calendarCount
	,id:'calId'												// ポップアップカレンダー表示ID  + calendarCount
	,monthId:'segConditionForm.selectedEmbMonth'			// 月プルダウンID
	,dayId:'segConditionForm.selectedEmbDay'				// 日プルダウンID
	,textId:'calTextId'										// カレンダー画像ID  + calendarCount
	,priority:'1'											// 表示優先度
	,index:''
	,balloonTitle:''
	,uniqueKey:''
	,dateValueClass : ''
	,dateValueName : ''
	,inputPlaceholder : '日付を選択してください'
	,inputClass : ''
	,uniqueKey : 'dummy'
	,isRequired : false
	,isDisabled : false
	,isDispAllOfMonth : false
	,prevNextInterval : 1
	,isDispHoliday : true
	,defSelectDay : 30
	,boundaryTime : '17:00'
	,startDateClass : ''
	,middleDateClass : ''
	,retDateClass : ''
	,areaClass : ''
	,retDaysFromStartDate : 2
	,midDaysFromStartDate : 1
	,dispDays : 300
	,selectableDays : 300
	,stayCityCount : undefined
	,form_number:0
	,zero_value:0
	,connectionCal:''
	,endDay:0
	,DU:null
	,PCLP:null
	,isReceiveTrigger:false
	,additionalClasses:null
	,calendarContoroller:m_tourModuleCalendar_commonContoroller
	,sameDayFlag:false
}

// 参加人数部品各固定値
var m_tourModuleNumberOfPeople_FixedParamsDomId = {
	dp : 'num_dom_free_balloon',
	pkg : 'num_dom_pkg_balloon',
	hotel : 'num_dom_hotel_balloon'
};
var m_tourModuleNumberOfPeople_FixedParams = {
	domtour : {
		top : {
			id : m_tourModuleNumberOfPeople_FixedParamsDomId,
			htmlType : 'domTop'
		},
		sec : {
			id : m_tourModuleNumberOfPeople_FixedParamsDomId,
			htmlType : 'domSec'
		}
	},
	inttour : {
		top : {
			id : 'num_int_free_balloon',
			htmlType : 'intTop'
		},
		sec : {
			id : 'num_int_free_balloon',
			htmlType : 'intSec'
		}
	}
}

// 参加人数部品デフォルト設定
var m_tourModuleNumberOfPeople_siteType = { site : 'common', module : 'people' };
var m_tourModuleNumberOfPeople_defaultParams = {
	 name:'peopleCount'												// ポップアップ参加人数  + 部品使用カウント
	,id: m_tourModuleNumberOfPeople_FixedParams.inttour.sec.id					// ポップアップ参加人数表示ID  + 部品使用カウント
	,className : undefined											// ポップアップ参加人数吹き出しclass属性名
	,textId:'m_numberOfPeople_'										// 参加人数テキストボックス  + 部品使用カウント
	,htmlType : m_tourModuleNumberOfPeople_FixedParams.inttour.sec.htmlType		// ポップアップ参加人数表示ID  + 部品使用カウント
	,NPLP : m_inttourModuleNumberOfPeople_NPLP
//	,initNum:[										// 部品生成時初期表示値※デフォルト設定なし
//	  // Adult, Child, ChildNoBed, Infant
//	  [ 1, 0, 0, 0 ],	// 1部屋目
//	  [ 1, 0, 0, 0 ],	// 2部屋目
//	  [ 1, 0, 0, 0 ],	// 3部屋目
//	  [ 1, 0, 0, 0 ],	// 4部屋目
//	  [ 1, 0, 0, 0 ],	// 5部屋目
//	  [ 1, 0, 0, 0 ],	// 6部屋目
//	  [ 1, 0, 0, 0 ],	// 7部屋目
//	  [ 1, 0, 0, 0 ]	// 8部屋目
//	]
	,clearNum:[										// 他要素変更時の選択値再生成用初期値
	  // Adult, Child, ChildNoBed, Infant
	  [ 1, 0, 0, 0 ],	// 1部屋目
	  [ 1, 0, 0, 0 ],	// 2部屋目
	  [ 1, 0, 0, 0 ],	// 3部屋目
	  [ 1, 0, 0, 0 ],	// 4部屋目
	  [ 1, 0, 0, 0 ],	// 5部屋目
	  [ 1, 0, 0, 0 ],	// 6部屋目
	  [ 1, 0, 0, 0 ],	// 7部屋目
	  [ 1, 0, 0, 0 ]	// 8部屋目
	]
	,column : [
		{ name : 'adultNum', sum : true, dispSum : true },
		{ name : 'childNum', sum : true, dispSum : true },
		{ name : 'childNoBedNum', dispSum : true },
		{ name : 'infantNum', dispSum : true },
		{ name : 'personNum', sumColumn : true } //合計用
	]
	,row : [
		{ name : 'One' },
		{ name : 'Two' },
		{ name : 'Three' },
		{ name : 'Four' },
		{ name : 'Five' },
		{ name : 'Six' },
		{ name : 'Seven' },
		{ name : 'Eight' }
	]
	,dispMatrix : [
		[ true, true, true, true, false ],
		[ true, true, true, true, false ],
		[ true, true, true, true, false ],
		[ true, true, true, true, false ],
		[ true, true, true, true, false ],
		[ true, true, true, true, false ],
		[ true, true, true, true, false ],
		[ true, true, true, true, false ]
	]
	,setDipsRowsObj : undefined
	,changeDipsRowsEventName : ''
	,notChangeTextValue  : ''
	,checkValueFunc : undefined
	,ctrlDisabled : undefined
	,m_tourModuleNumberOfPeopleContoroller:m_tourModuleNumberOfPeople_commonContoroller
	,textBoxTemplate : '<%= value %>人'
}

function m_tourModuleCommon_setTargetValue(options){
	// Adobe設定
	m_tourModuleCommon_setTargetValue_params = $tour_module_jq.extend(m_tourModuleCommon_setTargetValue_params,options);
}

function m_tourModuleCommon_getParam(type, options, param){
	if(options != undefined && options[param] != undefined){
		return options[param];
	}else{
		if(type.site == 'dom'){
			if(undefined !== type.module && 'Main' === type.module){
				return m_domTopModuleMain_optionParam_common[param];
			}else if(undefined !== type.module && 'tabisaku' === type.module){
				return m_domTopModuleDp_optionParam[param];
			}else if(undefined !== type.module && 'hotel' === type.module){
				return m_domTopModuleHotel_optionParam[param];
			}else if(undefined !== type.module && 'car' === type.module){
				return m_domTopModuleCar_optionParam[param];
			}else if(undefined !== type.module && 'hotel_bulk' === type.module){
				return m_domTopModuleHotelBulk_optionParam[param];
			}
			return m_domModule_optionParam[param];
		} else if(type.site == 'inttour'){
			if(type.module  == 'tabisaku'){
				return m_inttourModuleDp_defaults[param];
			}else if(type.module  == 'pack'){
				return m_inttourModulePkg_defaults[param];
			}
		} else if(type.site == 'domtourtop'){
			if(undefined !== type.module && 'Main' === type.module){
				return m_domtourTopModuleMain_optionParam_common[param];
			}else if(undefined !== type.module && 'tabisaku' === type.module){
				return m_domtourTopModuleDp_optionParam[param];
			}else if(undefined !== type.module && 'pack' === type.module){
				return m_domtourTopModulePkg_optionParam[param];
			}else if(undefined !== type.module && 'hotel' === type.module){
				return m_domtourTopModuleHotel_optionParam[param];
			}else if(undefined !== type.module && 'car' === type.module){
				return m_domtourTopModuleCar_optionParam[param];
			}else if(undefined !== type.module && 'traffic' === type.module){
				return m_domtourTopModuleTraffic_optionParam[param];
			}
		} else if(undefined !== type.site && 'inttourtop' === type.site){
			if(undefined !== type.module && 'Main' === type.module){
				return m_tourTopModuleMain_optionParam_common[param];
			}else if(undefined !== type.module && 'tabisaku' === type.module){
				return m_inttourTopModuleDp_optionParam[param];
			}else if(undefined !== type.module && 'pack' === type.module){
				return m_inttourTopModulePkg_optionParam[param];
			}
		} else if(type.site == 'tour2nd'){
			if(type.module  == 'Main'){
				return m_tour2ndModuleMain_optionParam_common[param];
			}else if(type.module  == 'tab'){
				return m_tour2ndModuleTab_optionParam[param];
			}
		} else if(type.site == 'domtour2nd'){
			if(type.module  == 'tab'){
				return m_domtour2ndModuleTab_optionParam[param];
			}else if(type.module  == 'tabisaku'){
				return m_domtour2ndModuleDp_optionParam[param];
			}else if(type.module  == 'pack'){
				return m_domtour2ndModulePkg_optionParam[param];
			}else if(type.module  == 'hotel'){
				return m_domtour2ndModuleHotel_optionParam[param];
			}else if(type.module  == 'car'){
				return m_domtour2ndModuleCar_optionParam[param];
			}else if(type.module  == 'traffic'){
				return m_domtour2ndModuleTraffic_optionParam[param];
			}else if(type.module  == 'bulk'){
				return m_domtour2ndModuleBulk_optionParam[param];
			}else if(type.module  == 'hotel_bulk'){
				return m_domtour2ndModuleHotelBulk_optionParam[param];
			}
		} else if(type.site == 'inttour2nd'){
			if(type.module  == 'tab'){
				return m_inttour2ndModuleTab_optionParam[param];
			}else if(type.module  == 'tabisaku'){
				return m_inttour2ndModuleDp_optionParam[param];
			}else if(type.module  == 'pack'){
				return m_inttour2ndModulePkg_optionParam[param];
			}else if(type.module  == 'option'){
				return m_inttour2ndModuleOption_optionParam[param];
			}
		} else if(type.site == 'common'){
			if(type.module  == 'common'){
				return m_tourModuleCommon_defaults[param];
			}else if(type.module  == 'calendar'){
				return m_tourModuleCalendar_defaults[param];
			}else if(type.module  == 'people'){
				return m_tourModuleNumberOfPeople_defaultParams[param];
			}
		}
		return null;
	}
};

function m_tourModuleCommon_getDefVal(type, list, options, param, defParam){
	var optionsVal = '';
	if(options != undefined){
		optionsVal = options[param];
		var defVal = list.find('[value=' + optionsVal + ']');
		if(defVal.size() != 0){
			return optionsVal;
		}
	}

	if(type.site == 'dom'){
		optionsVal = m_domModule_optionParam[defParam];
	} else if(type.site == 'intwws'){
		optionsVal = m_intwwsModule_optionParam[defParam];
	} else if(type.site == 'inttop'){
		optionsVal = m_intTopModule_optionParam[defParam];
	} else if(type.site == 'wwstop'){
		optionsVal = m_wwsTopModule_optionParam[defParam];
	} else if(type.site == 'tour2nd'){
		if(type.module == 'Main'){
			optionsVal = m_tour2ndModuleMain_optionParam_common[defParam];
		}else if(type.module == 'tab'){
			optionsVal = m_tour2ndModuleTab_optionParam[defParam];
		}
	} else if(type.site == 'domtour2nd'){
		if(type.module == 'tab'){
			optionsVal = m_domtour2ndModuleTab_optionParam[defParam];
		}
	} else if(type.site == 'inttour2nd'){
		if(type.module == 'tab'){
			optionsVal = m_inttour2ndModuleTab_optionParam[defParam];
		}else if(type.module == 'tabisaku'){
			optionsVal = m_inttour2ndModuleDp_optionParam[defParam];
		} else if(type.module == 'pack'){
			optionsVal = m_inttour2ndModulePkg_optionParam[defParam];
		}
	} else if(undefined !== type.site && 'inttourtop' === type.site){
		if(undefined !== type.module && 'Main' === type.module){
			optionsVal = m_tourTopModuleMain_optionParam_common[defParam];
		}else if(undefined !== type.module && 'tabisaku' === type.module){
			optionsVal = m_inttourTopModuleDp_optionParam[defParam];
		} else if(undefined !== type.module && 'pack' === type.module){
			optionsVal = m_inttourTopModulePkg_optionParam[defParam];
		}
	}

	var defVal = list.find('[value=' + optionsVal + ']');
	if(defVal.size() != 0){
		return optionsVal;
	}

	if(type.site == 'dom'){
		optionsVal = m_domModule_defaults[defParam];
	} else if(type.site == 'intwws'){
		optionsVal = m_intwwsModule_defaults[defParam];
	} else if(type.site == 'inttop'){
		optionsVal = m_intTopModule_defaults[defParam];
	} else if(type.site == 'wwstop'){
		optionsVal = m_wwsTopModule_defaults[defParam];
	} else if(type.site == 'tour2nd'){
		if(type.module == 'Main'){
			optionsVal = m_tour2ndModuleMain_defaults_common[defParam];
		}else if(type.module == 'tabSelDomInt'){
			optionsVal = m_tour2ndModuleTab_defaults[defParam];
		}
	} else if(type.site == 'domtour2nd'){
		if(type.module == 'tab'){
			optionsVal = m_domtour2ndModuleTab_defaults[defParam];
		}
	} else if(type.site == 'inttour2nd'){
		if(type.module == 'tab'){
			optionsVal = m_inttour2ndModuleTab_defaults[defParam];
		}else if(type.module == 'tabisaku'){
			optionsVal = m_inttour2ndModuleDp_defaults[defParam];
		} else if(type.module == 'pack'){
			optionsVal = m_inttour2ndModulePkg_defaults[defParam];
		}
	} else if(undefined !== type.site && 'inttourtop' === type.site){
		if(undefined !== type.module &&  'Main' === type.module){
			optionsVal = m_tourTopModuleMain_defaults_common[defParam];
		}else if(undefined !== type.module && 'tabisaku' === type.module){
			optionsVal = m_inttourTopModuleDp_defaults[defParam];
		} else if(undefined !== type.module && 'pack' === type.module){
			optionsVal = m_inttourTopModulePkg_defaults[defParam];
		}
	}
	return optionsVal;
};

function m_tourModuleCommon_getListValue(listName, key){
	var listValue = listName[key];

	if(listValue == undefined){
		return '';
	}
	return listValue;
};

////////////////////////
// 部品共通 START
///////////////////////

// 0埋め
function m_tourModuleCommon_zeroPadding(num) {
	var zeroPadding = ('0' + (num)).slice(-2);
	return zeroPadding;
};


// 部品配置数カウント
function m_tourCommonModuleParts_getPartsCount(partsName, moduleNum){
	if(partsName === 'stayCityCountRadio'){
		return m_tourModuleStayCityRadio_usePartsCount + moduleNum;
	}else if(partsName === 'm_tourModuleNumberOfPeople'){
		return m_tourModuleNumberOfPeople_usePartsCount + moduleNum;
	}else if(partsName === 'airport'){
		return m_tourModuleAirport_usePartsCount + moduleNum;
	}else if(partsName === 'calendar'){
		return m_tourModuleCalendar_usePartsCount + moduleNum;
	}
};

function m_tourCommonModuleParts_setPartsCount(partsName, useCount){
	if(partsName === 'stayCityCountRadio'){
		m_tourModuleStayCityRadio_usePartsCount += useCount;
	}else if(partsName === 'm_tourModuleNumberOfPeople'){
		m_tourModuleNumberOfPeople_usePartsCount += useCount;
	}else if(partsName === 'airport'){
		m_tourModuleAirport_usePartsCount += useCount;
	}else if(partsName === 'calendar'){
		m_tourModuleCalendar_usePartsCount += useCount;
	}
};


////////////////////////
// 部品共通 END
///////////////////////

////////////////////////
// 参加人数処理 START
///////////////////////

//参加人数モジュールの生成
function m_tourModuleNumberOfPeople_createParts(options) {
	// 初期化
	var count = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'moduleNo');
	var column = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'column');
	var row = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'row');
	var dispMatrix = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'dispMatrix');
	
	// 送信パラメータ用HTML生成
	var htmlHidden = '';

	for(var pri = 0; pri < row.length; pri++){
		for(var pci = 0; pci < column.length; pci++){
			// 表示マスまたは合計列の場合はhidden出力
			if(dispMatrix[pri][pci] || column[pci].sumColumn){
				htmlHidden += '<input type="hidden" name="'+ column[pci].name + row[pri].name +'" value="" class="'+ row[pri].name + count +'">\n';
			}
		}
	}
	this.append(htmlHidden);

	// 参加人数モジュールにhidden要素を連携する準備。行単位で渡す。
	options.numberOfPeopleHidden = new Array(row.length);
	for(var i = 0; i < row.length; i++){
		options.numberOfPeopleHidden[i] = this.find('.'+ row[i].name + count);
	}

	// 参加人数モジュールに親要素を渡す
	options.numberOfPeopleParent = this;

	// 参加人数モジュール呼び出し
	m_tourModuleNumberOfPeople_setController(options);
	m_tourModuleNumberOfPeople_setData(this, options);
	// モジュール使用カウンタ
	m_tourCommonModuleParts_setPartsCount('m_tourModuleNumberOfPeople', 1);
};

//参加人数コントローラーの設定
function m_tourModuleNumberOfPeople_setController(options){
	var nopModalId = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'id');
	var nopModalClassName = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'className');
	var count = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'moduleNo');
	var clearNum = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'clearNum');
	var column = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'column');
	var row = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'row');
	var dispMatrix = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'dispMatrix');
	var setDipsRowsObj = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'setDipsRowsObj');
	var nplp = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'NPLP');
	var htmlType = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'htmlType');
	var changeDipsRowsEventName = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'changeDipsRowsEventName');
	var checkValueFunc = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'checkValueFunc');
	var ctrlDisabled = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'ctrlDisabled');
	var notChangeTextValue = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'notChangeTextValue');
	
	var numberOfPeopleTourParams = {};
	$tour_module_jq.extend(numberOfPeopleTourParams, m_tourModuleNumberOfPeople_defaultParams);

	numberOfPeopleTourParams.NPLP = nplp;
	numberOfPeopleTourParams.htmlType = htmlType;
	numberOfPeopleTourParams.numberOfPeopleCount = count;

	numberOfPeopleTourParams.id = nopModalId; //吹き出しID
	numberOfPeopleTourParams.id += count;

	numberOfPeopleTourParams.className = nopModalClassName; //吹き出しclass属性名

	numberOfPeopleTourParams.textId += count; //入力項目ID

	numberOfPeopleTourParams.name += count;

	numberOfPeopleTourParams.hidden = options.numberOfPeopleHidden;

	numberOfPeopleTourParams.column = column;
	numberOfPeopleTourParams.row = row;
	numberOfPeopleTourParams.dispMatrix = dispMatrix;
	numberOfPeopleTourParams.dispMatrixFixed = $tour_module_jq.extend(true, {}, dispMatrix);

	numberOfPeopleTourParams.setDipsRows = setDipsRowsObj;
	numberOfPeopleTourParams.changeDipsRowsEventName = changeDipsRowsEventName;
	numberOfPeopleTourParams.notChangeTextValue = notChangeTextValue;

	numberOfPeopleTourParams.clearValue = clearNum;

	numberOfPeopleTourParams.checkValueFunc = checkValueFunc;
	numberOfPeopleTourParams.ctrlDisabled = ctrlDisabled;
	
	// 参加人数モジュール生成
	var nop = new m_tourModuleNumberOfPeople(numberOfPeopleTourParams);
	m_tourModuleNumberOfPeople_commonContoroller.addNumberOfPeople(nop);
};

//参加人数データの設定
function m_tourModuleNumberOfPeople_setData(target, options){
	var initNum = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'initNum');
	var clearNum = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'clearNum');
	var count = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'moduleNo');
	var column = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'column');
	var row = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'row');
	var dispMatrix = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'dispMatrix');
	var setDipsRowsObj = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'setDipsRowsObj');
	var htmlType = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'htmlType');
	var textBoxTemplate = m_tourModuleCommon_getParam(m_tourModuleNumberOfPeople_siteType, options, 'textBoxTemplate');

	var inputParams = {
			parent : target,
			mumberOfPeopleInitNum : initNum,				// hiddenの初期値(優先1)
			mumberOfPeopleClearNum : clearNum,				// hiddenの初期値(優先2)
			numberOfPeopleCount : count,					// 入力フォームID/name番号割り振り
			numberOfPeopleColumn : column,					// hiddenのclass,name属性名生成用配列
			numberOfPeopleRow : row,						// hiddenのclass,name属性名生成用配列
			numberOfPeopleDispMatrixFixed : dispMatrix,		// hiddenのclass,name属性名生成用配列
			numberOfPeopleDispRooms : setDipsRowsObj,		// 部屋数object
			htmlType : htmlType,							// タイプ指定
			textBoxTemplate : textBoxTemplate				// 入力項目値テキストテンプレート
	};

	$tour_module_jq(options.numberOfPeopleHidden[0][0]).change(inputParams, m_tourModuleNumberOfPeople_updateText);
	$tour_module_jq(options.numberOfPeopleHidden[0][0]).trigger('change');
};

// 呼び元入力テキスト更新
// 初期表示の場合はhidden値設定
function m_tourModuleNumberOfPeople_updateText(options) {

	var data = options.data;
	var parent = data.parent;

	var sumAll = 0;

	// 参加人数hidden値よりフォームに出力するテキストの準備
	var dispArray = data.numberOfPeopleDispMatrixFixed;
	var col = data.numberOfPeopleColumn;
	var row = data.numberOfPeopleRow;
	var no = data.numberOfPeopleCount;
	var val = new Array(row.length);
	for(var ri = 0; ri < row.length; ri++){
		val[ri] = new Array(col.length);
		for(var ci = 0; ci < col.length; ci++){
			if(dispArray[ri][ci]){
				val[ri][ci] =  parent.find('input[name="'+ col[ci].name + row[ri].name +'"]').val();
			}else{
				val[ri][ci] = '0';
			}
		}
	}

	var isEmptyFlg = false;
	for(var ri = 0; ri < row.length; ri++){
		for(var ci = 0; ci < col.length; ci++){
			if(_.isEmpty(val[ri][ci])){
				isEmptyFlg = true;
				break;
			}else{
				// 表示かつ合計対象列は加算
				if(dispArray[ri][ci] && col[ci].dispSum){
					sumAll += parseInt(val[ri][ci]);
				}
			}
		}
	}
	if(isEmptyFlg){
		// 参加人数hidden値の初期設定
		sumAll = 0;
		dipsRooms = $tour_module_jq(data.numberOfPeopleDispRooms).val();
		if(dipsRooms){
			dipsRooms = parseInt(dipsRooms);
		}else{
			dipsRooms = row.length; // すべての列を表示する
		}
		// 初期値設定
		var initNumAarray = data.mumberOfPeopleInitNum;
		if(initNumAarray !== undefined){
			// 初回表示設定ありの場合はテキスト表示変更
			isEmptyFlg = false;
		}else{
			initNumAarray = data.mumberOfPeopleClearNum;
		}
		// hidden値設定
		for(var ri = 0; ri < row.length; ri++){
			var sumRowHidden = 0;
			for(var ci = 0; ci < col.length; ci++){
				if(dispArray[ri][ci]){
					var mumberOfPeopleNum = initNumAarray[ri][ci];
					if(dipsRooms === 0 || dipsRooms < (ri + 1)){
						mumberOfPeopleNum = 0;
					}
					parent.find('input[name="'+ col[ci].name + row[ri].name +'"]').val(mumberOfPeopleNum);
					if(col[ci].sum){
						sumRowHidden += parseInt(mumberOfPeopleNum);
					}
					if(col[ci].dispSum){
						sumAll += parseInt(mumberOfPeopleNum);
					}
				}else if(col[ci].sumColumn){
					parent.find('input[name="'+ col[ci].name + row[ri].name +'"]').val(sumRowHidden);
				}else{
					// 非表示分は0を設定
					parent.find('input[name="'+ col[ci].name + row[ri].name +'"]').val(0);
				}
			}
		}
	}

	// 入力フォームにテキスト反映
	if(!isEmptyFlg){
		var numberOfPeopleTextTemplate = _.template(data.textBoxTemplate)
		var numberOfPeopleTextValue = numberOfPeopleTextTemplate({value : sumAll});
		var textId = m_tourModuleNumberOfPeople_defaultParams.textId;
		var htmlType = data.htmlType;
		
		if ((undefined !== htmlType && m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType === htmlType)
			|| (undefined !== htmlType && m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType === htmlType)){
			var result = parent.find('.m_numberOfPeople_text').text(numberOfPeopleTextValue);
		}else{
			var result = parent.find('input[type=text]#'+ textId + no).val(numberOfPeopleTextValue);
		}
		if(!result){
			parent.find('#'+ textId + no).text(numberOfPeopleTextValue);
		}
	}
};

////////////////////////
// 参加人数 END
///////////////////////

////////////////////////
// 空港一覧部品 START
///////////////////////

// 空港一覧入力部品
function m_tourModuleAirportList_createParts(options){
	var html = '';

	if (undefined !== options.site && "inttourtop" === options.site){
		// 海外ツアーTOP
		if(undefined !== options.isChainStart){
			html += '<div class="item-input js-mod-balloon d_locationIcon" data-balloon="airport_balloon_parts_' + options.uniqueKey + '" data-control-point="left" data-balloon-chain="'+ options.isChain +'" data-balloon-chain-start="'+ options.isChainStart +'" tabindex="0">\n';
		}else{
			html += '<div class="item-input js-mod-balloon d_locationIcon" data-balloon="airport_balloon_parts_' + options.uniqueKey + '" data-control-point="left" data-balloon-chain="'+ options.isChain +'" tabindex="0">\n';
		}
		html += '<span class="item-val ' + options.inputClass + '">'+ options.inputPlaceholder +'</span>\n';
		html += '<!--/mod-modal--></div>\n';
	}else if(undefined !== options.site && "domtourtop" === options.site){
		// 国内ツアーTOP
		if(undefined !== options.isChainStart){
			html += '<div class="item-input js-mod-balloon d_locationIcon" data-balloon="airport_balloon_parts_' + options.uniqueKey + '" data-control-point="'+ options.defPartsPosition +'" data-balloon-chain="'+ options.isChain +'" data-balloon-chain-start="'+ options.isChainStart +'" tabindex="0">\n';
		}else if(undefined !== options.isChain){
			html += '<div class="item-input js-mod-balloon d_locationIcon" data-balloon="airport_balloon_parts_' + options.uniqueKey + '" data-control-point="'+ options.defPartsPosition +'" data-balloon-chain="'+ options.isChain +'" tabindex="0">\n';
		}else{
			html += '<div class="item-input js-mod-balloon d_locationIcon" data-balloon="airport_balloon_parts_' + options.uniqueKey + '" data-control-point="'+ options.defPartsPosition +'" tabindex="0">\n';
		}
		html += '<span class="item-val ' + options.inputClass + '">'+ options.inputPlaceholder +'</span>\n';
		html += '<!--/mod-modal--></div>\n';
	}else{
		// 国内ツアー2nd, 海外ツアー2nd
		html += '<input type="text" name="" value="' + options.defAirportCode + '" class="select-arr mod-input mod-icon js-mod-balloon d_locationIcon ' + options.inputClass + '" readonly data-balloon="airport_balloon_parts_' + options.uniqueKey + '" data-control-point="'+ options.defPartsPosition +'" placeholder="' + options.inputPlaceholder + '">\n';
	}
	if (options.areaName) {
		html += '<input type="hidden" name="' + options.areaName + '" value="" class="' + options.areaClass + '">\n';
	} else {
		html += '<input type="hidden" value="" class="' + options.areaClass + '">\n';
	}
	if (options.countryName) {
		html += '<input type="hidden" name="' + options.countryName + '" value="" class="' + options.countryClass + '">\n';
	} else {
		html += '<input type="hidden" value="" class="' + options.countryClass + '">\n';
	}
	if (options.apoCodeName) {
		html += '<input type="hidden" name="' + options.apoCodeName + '" value="" class="' + options.apoCodeClass + '">\n';
	} else {
		html += '<input type="hidden" value="" class="' + options.apoCodeClass + '">\n';
	}

	this.prepend(html);

	// 必須入力背景色（黄色）の設定
	if (options.isRequired) {
		this.find('.' + options.inputClass).addClass('required');
	}


	// 入力不可の設定
	if (options.isDisabled) {
		if (undefined !== options.site && "inttourtop" === options.site){
			this.find('.item-input').addClass('disabled');
		}else{
			this.find('.' + options.inputClass).prop('disabled', true);
		}
		
	}

	// 右端アイコンの設定
	if (options.apoType === '2') {
		// 三角
		this.find('.' + options.inputClass).removeClass('select-arr');
		this.find('.' + options.inputClass).removeClass('select-dep');
		if (undefined !== options.site && "domtourtop" === options.site) {
			this.find('.' + options.inputClass).addClass('select-wrap');
		} else {
			this.find('.' + options.inputClass).addClass('ico_select');
		}
	}else if (options.apoType === '0') {
		// 飛行機離陸
		this.find('.' + options.inputClass).removeClass('select-arr');
		this.find('.' + options.inputClass).addClass('select-dep');
	} else {
		// 飛行機着陸
		this.find('.' + options.inputClass).removeClass('select-dep');
		this.find('.' + options.inputClass).addClass('select-arr');
	}

	if (undefined !== options.site && "inttourtop" === options.site && undefined !== options.module && "pack" === options.module && undefined !== options.layoutType && "9" === options.layoutType){
		if ('m_inttourTransitionPeriodOptions' in window && m_inttourTransitionPeriodOptions.isTransitionPeriod){
			//TOPパッケージで且つ目的地で過渡期の場合は空港入力部品(一覧吹き出し)を初期化しない
			m_tourModuleAirportList_initComponent(this, options, false);
		}else{
			m_tourModuleAirportList_initComponent(this, options, true);
		}
	}else{
		m_tourModuleAirportList_initComponent(this, options, true);	
	}
	
};

/**
 * 空港入力部品の初期化
 *
 */
function m_tourModuleAirportList_initComponent(target, options,isTransitionPeriod){

	var defAreaCode = options.defAreaCode;
	var defCountryCode = options.defCountryCode;
	var defAirportCode = options.defAirportCode;

	if(isTransitionPeriod){
		// 空港一覧の設定
		target.m_tourModuleAirportList_AirportList({
			targetObject : target,
			balloonTitle : options.balloonTitle,
			filterAirportClass : '.' + options.filterApoCodeClass,
			targetAreaClass : '.' + options.areaClass,
			targetCountryClass : '.' + options.countryClass,
			targetAirportClass : '.' + options.apoCodeClass,
			targetInputClass : '.' + options.inputClass,
			m_tourModuleParts_airportListTableLanguageProperties : m_tourModuleParts_airportListTableLanguageProperties,
			uniqueKey : options.uniqueKey,
			isRequired : options.isRequired,
			isDisabled : options.isDisabled,
			layoutType : options.layoutType,
			stayCityCount : options.stayCityCount,
			inputPlaceholder : options.inputPlaceholder,
			intTopFlg : options.intTopFlg,
			isInitSelectOtherInputObj : options.isInitSelectOtherInputObj
		});
	}else{
		
	}

	var targetArea = target.find('.' + options.areaClass);
	var targetCountry = target.find('.' + options.countryClass);
	var targetApo = target.find('.' + options.apoCodeClass);

	if (!_.isEmpty(defAirportCode)) {
		var areaCountry = target.m_tourModuleAirportList_getAreaCountryFromAirport(defAirportCode, options.layoutType);
		targetArea.val(areaCountry['area']);
		targetCountry.val(areaCountry['country']);
		targetApo.val(defAirportCode);
	} else if (!_.isEmpty(defCountryCode)) {
		var area = target.m_tourModuleAirportList_getAreaFromCountry(defCountryCode, options.layoutType);
		targetArea.val(area);
		targetCountry.val(defCountryCode);
		targetApo.val('');
	} else if (!_.isEmpty(defAreaCode)) {
		targetArea.val(defAreaCode);
		targetCountry.val('');
		targetApo.val('');
	} else {
		targetArea.val('');
		targetCountry.val('');
		targetApo.val('');
	}

	var selectedName = target.m_tourModuleAirportList_getSelectedName(targetArea.val(), targetCountry.val(), targetApo.val(), options.layoutType);
	if(undefined !== options.inputClass && 0 !== target.find("span" + '.' + options.inputClass).length){
		if(0 !==selectedName.length){
			target.find("span" + '.' + options.inputClass).text(selectedName);
		}else{
			target.find("span" + '.' + options.inputClass).text(options.inputPlaceholder);
		}
		
	}else{
		target.find('.' + options.inputClass).val(selectedName);	
	}
	
};
////////////////////////
// 空港一覧部品 END
///////////////////////

////////////////////////
// カレンダー部品 START
///////////////////////

// 海外ツアー用カレンダー実装
function m_tourModuleCalendar_createPartsParams(options){

	var popupCalendarTourCommonModulePartsParams = {};
	$tour_module_jq.extend(popupCalendarTourCommonModulePartsParams, m_tourModuleCalendar_defaults);
	popupCalendarTourCommonModulePartsParams.PCLP = m_tourModuleCalender_PCLP;

	popupCalendarTourCommonModulePartsParams.balloonTitle = options.balloonTitle;
	popupCalendarTourCommonModulePartsParams.uniqueKey = options.uniqueKey;
	popupCalendarTourCommonModulePartsParams.name += '_tourcommonmoduleparts_' + options.uniqueKey;
	popupCalendarTourCommonModulePartsParams.textId = 'calTextId_tourcommonmoduleparts_' + options.uniqueKey;
	popupCalendarTourCommonModulePartsParams.id += '_tourcommonmoduleparts_' + options.uniqueKey;
	
	popupCalendarTourCommonModulePartsParams.inputClass = options.inputClass;
	
	return popupCalendarTourCommonModulePartsParams;
};

// 海外ツアー用カレンダー設定
m_tourModuleCalendar_propertiesTicket = function (options) {

	var PCP = new m_tourModulePopupCalendarProperties();

	// maximum limit of display day from system date
	PCP.maxLimitDays = options.dispDays;

	// maximum limit of selectable day from system date
	PCP.maxSelectableDays = options.selectableDays;

	// maximum limit of selectable month from system date
	if(options.dispMonths !== undefined){
		PCP.maxLimitMonths = options.dispMonths;
	} else {
		PCP.maxLimitMonths = 12;
	}
	
	// maximum limit of selectable month from system date
	PCP.maxSelectableMonths = options.selectableMonths;

	// shows holiday
	// true:shows false:not shows
	PCP.isDispHoliday = options.isDispHoliday;

	// locale of the popup calendar
	// 'J':Japanese 'E':English
	PCP.locale = 'J';

	// length of the popup calendar that shows up
	PCP.calendarLength = 3;

	// weekday
	PCP.weekDay = true;
	
	// SPモード判定幅
	if(options.spWidth !== undefined){
		PCP.spWidth = options.spWidth;
	}

	return PCP;
};

// 日付入力部品
function m_tourModuleCalendar_createParts(options) {

	var html = '';
	
	if (undefined !== options.site && ("inttourtop" === options.site || "domtourtop" === options.site)){
		if (undefined !== options.isChain){
			html += '<p id="calTextId_tourcommonmoduleparts_' + options.uniqueKey + '" class="item-input ico_calendar js-mod-balloon" data-balloon="cal_balloon_parts_' + options.uniqueKey + '" data-control-point="' + options.defPartsPosition + '" data-balloon-chain="'+ options.isChain +'" tabindex="0">\n';
		}else{
			html += '<p id="calTextId_tourcommonmoduleparts_' + options.uniqueKey + '" class="item-input ico_calendar js-mod-balloon" data-balloon="cal_balloon_parts_' + options.uniqueKey + '" data-control-point="' + options.defPartsPosition + '" tabindex="0">\n';
		}
		var depDeteId = '';
		if(undefined !== options.depDateId && '' !== options.depDateId){
			depDeteId = ' id="' + options.depDateId;
		}
		html += '		<span class="item-val ' + options.inputClass + '"' + depDeteId + '">' + options.defDate + '</span>\n';
		html += '<!--/mod-modal--></p>\n';
	}else{
		html += '<input autocomplete="off" type="text" value="" placeholder="' + options.inputPlaceholder + '" id="calTextId_tourcommonmoduleparts_' + options.uniqueKey + '" class="mod-pgtour-depdate mod-input ico_calendar period mod-icon js-mod-balloon ' + options.inputClass + '" data-balloon="cal_balloon_parts_' + options.uniqueKey + '" data-control-point="' + options.defPartsPosition + '" readonly="readonly">\n';
	}
	
	if (options.dateValueName) {
		html += '<input type="hidden" name="' + options.dateValueName + '" value="' + options.defDate + '" class="mod-input ' + options.dateValueClass + '">\n';
	} else {
		html += '<input type="hidden" value="' + options.defDate + '" class="mod-input ' + options.dateValueClass + '">\n';
	}
	this.append(html);

	// 必須入力背景色（黄色）の設定
	if (options.isRequired) {
		this.find('.' + options.inputClass).addClass('required');
	}

	// 入力不可の設定
	if (options.isDisabled) {
		this.find('.' + options.inputClass).prop('disabled', true);
	}

	// 入力項目AUTO機能開始の設定
	if ((undefined !== options.site && "domtourtop" === options.site) 
		&& (undefined !== options.isChainStart)) {
		
		this.find('#calTextId_tourcommonmoduleparts_' + options.uniqueKey).attr('data-balloon-chain-start', options.isChainStart);
	}

	var calHtml = '<div style="display:none;" id="calId_tourcommonmoduleparts_' + options.uniqueKey + '" class="calendar-module"></div>\n';
	this.after(calHtml);

	PCP_ticket = new m_tourModuleCalendar_propertiesTicket(options);
	var DU_ticket = new m_tourModuleDateUtil(PCP_ticket, options.boundaryTime);
	options.targetObject = this;

	m_tourModuleCalendar_setController(options, DU_ticket);
	m_tourModuleCalendar_setData(this, options, DU_ticket);
}

// カレンダーコントローラーの設定
function m_tourModuleCalendar_setController(options, DU_ticket){

	var popupCalendarTourCommonModulePartsParams = m_tourModuleCalendar_createPartsParams(options);
	popupCalendarTourCommonModulePartsParams.DU = DU_ticket;

	// 行き日付に対する移動日の差分日数を取得
	var midDaysFromStartDate = m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'midDaysFromStartDate');

	// 行き日付に対する帰りの差分日数を取得
	popupCalendarTourCommonModulePartsParams.midDaysFromStartDate = midDaysFromStartDate;
	// 帰り選択日の場合はエリアを参照し中南米とヨーロッパのみ行き+3日、それ以外は行き+2日を設定
	var retDaysFromStartDate = m_tourModuleDateUtil.prototype.calcRetDaysFromStartDate(options);
	popupCalendarTourCommonModulePartsParams.retDaysFromStartDate = retDaysFromStartDate;

	// カレンダー初期選択日の設定
	if (options.defDate) {
		popupCalendarTourCommonModulePartsParams.defDate = options.defDate;
	} else {
		var defDate = m_tourModuleCalender_PCLP.calcDefDate(options);
		defDateText = defDate.getFullYear() + '/' + ('0' + (defDate.getMonth() + 1)).slice(-2)
			+ '/' + ('0' + defDate.getDate()).slice(-2);
		popupCalendarTourCommonModulePartsParams.defDate = defDateText;
		options.defDate = defDateText;
	}

	popupCalendarTourCommonModulePartsParams.site =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'site');
	popupCalendarTourCommonModulePartsParams.targetObject =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'targetObject');
	popupCalendarTourCommonModulePartsParams.dateValueClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'dateValueClass');
	popupCalendarTourCommonModulePartsParams.dateField =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'dateValueName');
	popupCalendarTourCommonModulePartsParams.isModalModule =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'isModalModule');
	popupCalendarTourCommonModulePartsParams.isDispAllOfMonth =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'isDispAllOfMonth');
	popupCalendarTourCommonModulePartsParams.prevNextInterval =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'prevNextInterval');
	popupCalendarTourCommonModulePartsParams.boundaryTime =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'boundaryTime');
	popupCalendarTourCommonModulePartsParams.startDateClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'startDateClass');
	popupCalendarTourCommonModulePartsParams.middleDateClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'middleDateClass');
	popupCalendarTourCommonModulePartsParams.retDateClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'retDateClass');
	popupCalendarTourCommonModulePartsParams.areaClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'areaClass');
	popupCalendarTourCommonModulePartsParams.receiptDisableDays =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'receiptDisableDays');
	popupCalendarTourCommonModulePartsParams.maxTravelDays =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'maxTravelDays');
	popupCalendarTourCommonModulePartsParams.returnDaysFromDepartureDate =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'returnDaysFromDepartureDate');
	popupCalendarTourCommonModulePartsParams.dispDays =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'dispDays');
	popupCalendarTourCommonModulePartsParams.dispMonths =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'dispMonths');
	popupCalendarTourCommonModulePartsParams.selectableDays =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'selectableDays');
	popupCalendarTourCommonModulePartsParams.selectableMonths =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'selectableMonths');
	popupCalendarTourCommonModulePartsParams.stayCityCount =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'stayCityCount');
	popupCalendarTourCommonModulePartsParams.isReceiveTrigger =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'isReceiveTrigger');
	popupCalendarTourCommonModulePartsParams.maxTravelDateClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'maxTravelDateClass');
	popupCalendarTourCommonModulePartsParams.minTravelDateClass =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'minTravelDateClass');
	popupCalendarTourCommonModulePartsParams.additionalClasses =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'additionalClasses');
	popupCalendarTourCommonModulePartsParams.sameDayFlag =
		m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'sameDayFlag');

	var cal = new m_tourModulePopupCalendar(popupCalendarTourCommonModulePartsParams);
	m_tourModuleCalendar_commonContoroller.addCalendar(cal);
};

// カレンダーデータの設定
function m_tourModuleCalendar_setData(target, options, DU_ticket){
	var defDate = options.defDate;
	var defSelectDay = options.defSelectDay;

	var targetDate = null;

	// 日付の設定
	if (m_tourModuleCalendar_isQueryableDate(defDate, options, DU_ticket)) {
		// 有効なデフォルト日付が設定されている場合
		targetDate = new Date(defDate);
	} else {
		// デフォルト
		targetDate = m_tourModuleCalender_PCLP.calcDefDate(options)
	}
	var FormatDate;
	var fieldDate;
	if (defDate.length === 10) {
		FormatDate = m_tourModuleCalender_PCLP.FormatDate(targetDate);
		fieldDate = targetDate.getFullYear() + ('0'+(targetDate.getMonth()+1)).slice(-2) + ('0'+targetDate.getDate()).slice(-2);
	} else if (defDate.length === 7) {
		FormatDate = m_tourModuleCalender_PCLP.FormatMonthText(targetDate);
		fieldDate = targetDate.getFullYear() + ('0'+(targetDate.getMonth()+1)).slice(-2);
	} else {
		FormatDate = m_tourModuleCalender_PCLP.FormatDate(targetDate);
		fieldDate = targetDate.getFullYear() + ('0'+(targetDate.getMonth()+1)).slice(-2) + ('0'+targetDate.getDate()).slice(-2);
	}
	target.find('.' + options.dateValueClass).val(fieldDate);
	
	if ("inttourtop" === options.site || "domtourtop" === options.site){
		target.find('.' + options.inputClass).text(FormatDate);
	}else{
		target.find('.' + options.inputClass).val(FormatDate);	
	}
	target.find('.' + options.inputClass).val(FormatDate);
	target.find('.' + options.inputClass).trigger('change');
};

// 引数の日付が照会可能期間かどうか判定
function m_tourModuleCalendar_isQueryableDate(depDateSlash, options, DU_ticket){
	if (!m_tourModuleCalendar_isValidDate(depDateSlash)) {
		return false;
	}
	options.initCheck = options.parent; //初期化用オブジェクト
	var targetDate = new Date(depDateSlash);
	if(depDateSlash.length == 7){
	    // 年月の場合は月末を指定
	    targetDate = new Date(targetDate.getFullYear(), targetDate.getMonth() + 1, 0);
	}
	var referenceableStartDate = m_tourModuleDateUtil.prototype.calcCurrentDate(options.boundaryTime); // 営業日
	referenceableStartDate.setDate(referenceableStartDate.getDate() + m_tourModuleDateUtil.prototype.calcReceiptDisableDays(options) + 1); // 営業日+受付終了日+1日
	if (targetDate.getTime() < referenceableStartDate.getTime()) {
		//選択可能開始日より過去だったら照会可能期間外
		return false;
	}
	
	var referenceableLastDate = m_tourModuleDateUtil.prototype.calcCurrentDate(options.boundaryTime); // 営業日
	if(options.selectableDays){
		//選択可能範囲が日指定
		referenceableLastDate.setDate(referenceableLastDate.getDate() + options.selectableDays); // 営業日+選択可能制御日
	}else{
		//選択可能範囲が月指定
		referenceableLastDate.setMonth(referenceableLastDate.getMonth() + options.selectableMonths); // 営業日+選択可能制御月
		referenceableLastDate.setDate(1);
	}
	
	if (targetDate.getTime() >= referenceableLastDate.getTime()) {
		//選択可能日数後より未来だったら照会可能期間外
		return false;
	}
	return true;
};

// targetDateが'YYYY/MM/DD'または'YYYY/MM'形式か判定
function m_tourModuleCalendar_isValidDate(targetDate){
	var date = new Date(targetDate);
	if (targetDate.length === 10) {
		// targetDateが'YYYY/MM/DD'形式か判定
		if (targetDate == ( date.getFullYear() + '/' + ('0'+(date.getMonth()+1)).slice(-2) + '/' + ('0'+date.getDate()).slice(-2) ) ) {
			return true;
		}
	} else if (targetDate.length === 7) {
		// targetDateが'YYYY/MM'形式か判定
		if (targetDate == ( date.getFullYear() + '/' + ('0'+(date.getMonth()+1)).slice(-2) ) ) {
			return true;
		}
	} else {
		// targetDateが'YYYY/MM/DD'形式か判定
		if (targetDate == ( date.getFullYear() + '/' + ('0'+(date.getMonth()+1)).slice(-2) + '/' + ('0'+date.getDate()).slice(-2) ) ) {
			return true;
		}
	}
	return false;
};

////////////////////////
// カレンダー部品 END
///////////////////////

///////////////////////
// 部屋数部品 START
///////////////////////
	
// 部屋数プルダウン選択肢の設定
function m_tourModuleNumberOfRooms_createParts(options) {
	var type = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_defHtmlType');
	var listName = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_numberOfRoomsList');
	var selectValue = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_numberOfRoomsValue');
	var html = '';
	for(var key in listName) {
		var date = listName[key];
		var codeValue = date['value'];
		var textValue = date['text'];

		if(undefined !== type && "inttourtop" === type){
			html += '<li data-val="' + codeValue + '" tabindex="0">' + textValue + '</li>\n';
		}else{
			html += '<option value="' + codeValue + '">' + textValue + '</option>\n';
		}
	}
	this.append(html);

	if(undefined !== type && "inttourtop" === type){
		var targetObj = $tour_module_jq(this).parents('.mod-customSelect');
		var selDataObj = $tour_module_jq(this).find('li[data-val=' + selectValue + ']');
		targetObj.find('.m_numberOfRooms_text').text(selDataObj.text());
		selDataObj.addClass('mod-current');
		targetObj.find('.m_numberOfRooms_hidden').val(selectValue);
	}else{
		var option = '[value=' + selectValue + ']';
		this.find(option).prop('selected', true);
	}
};

///////////////////////
// 部屋数部品 END
///////////////////////

///////////////////////
// hidden部品 START
///////////////////////
	
// ツアー用のhidden要素生成
// 本関数をコールした要素のform要素内にhidden要素を追加する
function m_tourModuleHidden_createParts(data){
	// form要素をセレクト
	var target = $tour_module_jq(this).find('form');
	// 生成パラメータ用値一覧
	var d_csc = data.coopSiteCode; // 提携サイトコード
	var d_ccc = data.cabinClassCode; // 海外パッケージコースコード用座席クラス
	var d_ipddym = data.intPkgDepDateYM; // 海外パッケージ出発日or出発期間From(年・月)
	var d_ipddd = data.intPkgDepDateD;  // 海外パッケージ出発日or出発期間From(日)
	var d_ipdedym = data.intPkgDepEndDateYM; // 海外パッケージ出発期間To(年・月)
	var d_ipdedd = data.intPkgDepEndDateD;  // 海外パッケージ出発期間To(日)
	// 提携サイトコード生成
	if(!_.isEmpty(d_csc)){
		var html = '<input type="hidden" name="coopSiteCode" value="'+ d_csc +'">\n';
		target.append(html);
	}
	// 海外パッケージコースコード用座席クラス生成
	if(d_ccc !== undefined){
		// 追加HTML分の事前削除
		target.find('input[name="cabinClass"]').remove();
		
		var html = '<input type="hidden" name="cabinClass" value="'+ d_ccc +'">\n';
		target.append(html);
	}
	// 海外パッケージ出発日or出発期間From（年・月、日）
	if(d_ipddym !== undefined){
		// 追加HTML分の事前削除
		target.find('input[name="deptYearMonth"]').remove();
		target.find('input[name="deptDay"]').remove();
		target.find('input[name="deptEndYearMonth"]').remove();
		target.find('input[name="deptEndDay"]').remove();
		
		var html = '<input type="hidden" name="deptYearMonth" value="'+ d_ipddym +'">\n';
		html += '<input type="hidden" name="deptDay" value="'+ d_ipddd +'">\n';
		target.append(html);
	}
	// 海外パッケージ出発期間To（年・月、日）
	if(d_ipdedym !== undefined){
		// 追加HTML分の事前削除
		target.find('input[name="deptEndYearMonth"]').remove();
		target.find('input[name="deptEndDay"]').remove();
		
		var html = '<input type="hidden" name="deptEndYearMonth" value="'+ d_ipdedym +'">\n';
		html += '<input type="hidden" name="deptEndDay" value="'+ d_ipdedd +'">\n';
		target.append(html);
	}
	
	if('domtour' === data.site){
		// 追加HTML分の事前削除
		target.find('input[name="' + data.inputName + '"]').remove();
		
		var html = '<input type="hidden" name="' + data.inputName + '" value="'+ data.inputValue +'">\n';
		target.append(html);
	}
}

///////////////////////
// hidden部品 END
///////////////////////

/////////////////////////////////////
// 都市滞在ラジオボタン部品 START
/////////////////////////////////////

// 都市滞在ラジオボタンの設定
function m_tourModuleStayCityRadio_createParts(options){
	var inttourStayCityCountList = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_stayCityCountList');
	var inttourStayCityCountValue = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_defStayCityCount');
	var parent = options.parent;
	var html = '';
	for(var key in inttourStayCityCountList){
		var date = inttourStayCityCountList[key];
		var seatValue = date['value'];
		var seatText = date['text'];
		var cnt = m_tourModuleCommon_zeroPadding(m_tourCommonModuleParts_getPartsCount('stayCityCountRadio', 0));

		html += '<li class="ico-radio">\n';
		html += '<input class="item-radio js-mod-int-tabisaku-tab_btn m_stayCityCountRadio" type="radio" id="m_stayCityCountRadio' + cnt + '" value="' + seatValue + '" name="m_stay">\n';
		html += '<label tabindex="0" for="m_stayCityCountRadio' + cnt + '">' + seatText + '</label>\n';
		html += '</li>\n';

		// モジュール使用カウンタ
		m_tourCommonModuleParts_setPartsCount('stayCityCountRadio', 1);
	}
	this.prepend(html);

	// 都市滞在ラジオボタンクリックイベント
	if(parent){
		options.parent.find('.m_stayCityCountRadio').click(
			{
				parent : options.parent,
				list : inttourStayCityCountList,
				htmlType : options.htmlType
			},
			m_tourModuleStayCityRadio_changeSelect
		);
	}
	
	var option = '[value=' + inttourStayCityCountValue + ']';
	var selRadio = this.find(option);
	selRadio.prop('checked', true);
	selRadio.trigger('click');
};

// 都市滞在ラジオボタン選択値変更時の表示切替
function m_tourModuleStayCityRadio_changeSelect(options){

	var data = options.data;
	var parent = data.parent;
	var list = data.list;
	var htmlType = data.htmlType;

	var selectedRadio = this;
	var $box = $tour_module_jq(selectedRadio).closest('.mod-content_wrap');
	if ($tour_module_jq(selectedRadio).val() === list[0].value) {
		$box.find('.mod-city-stay_02').removeClass('is-select');
		$box.find('.mod-city-stay_01').addClass('is-select');
		$box.find('.m_city_stay_inline_block_02').css('display', 'none');
		if(htmlType === 'inttourtop'){
			$box.find('.m_city_stay_inline_block_01').css('display', 'block');
		}else{
			$box.find('.m_city_stay_inline_block_01').css('display', 'inline-block');
		}
	} else {
		$box.find('.mod-city-stay_01').removeClass('is-select');
		$box.find('.mod-city-stay_02').addClass('is-select');
		$box.find('.m_city_stay_inline_block_01').css('display', 'none');
		if(htmlType === 'inttourtop'){
			$box.find('.m_city_stay_inline_block_02').css('display', 'block');
		}else{
			$box.find('.m_city_stay_inline_block_02').css('display', 'inline-block');
		}
	}
};

/////////////////////////////////////
// 都市滞在ラジオボタン部品 END
/////////////////////////////////////

/////////////////////////////////////
// 座席クラスプルダウン部品 START
/////////////////////////////////////

// 座席クラスプルダウン選択肢の初期設定
function m_tourModuleClassFare_createParts(options) {
	var module = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'module');
	var type = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_defHtmlType');
	var lists = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_defClassFareList');
	var inputObject = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_defInputObject');
	var selectValue = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_defClassFareValue');
	var changeListEventName = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_changeListEventName');
	var changeListEventObject = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_changeListEventObject');
	var selectListObject1 = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_selectListObject1');
	var selectListObject2 = m_tourModuleCommon_getParam(m_tourModuleCommon_siteType, options, 'm_selectListObject2');
	var thisObj = this;

	// 指定の座席リストで再構築イベント登録
	if(!_.isEmpty(changeListEventName) && !_.isEmpty(changeListEventObject)){
		$tour_module_jq(changeListEventObject).on(changeListEventName, function(e){
			var listName = m_tourModuleClassFare_getPullList(lists, selectListObject1, selectListObject2, type, inputObject);
			if(undefined !== type && "inttourtop" === type){
				var targetObj = $tour_module_jq(thisObj).parents('.mod-customSelect').find('li[data-val]');
				if(undefined !== targetObj && 0 !== targetObj.length){
					//座席クラス ターゲットがliタグの場合は初期選択値としてリストの先頭値を引数として渡す
					m_tourModuleClassFare_setPull(type, listName, (undefined !== module && "tabisaku" === module) ? selectValue : listName[0].value, thisObj);
				}else{
					m_tourModuleClassFare_setPull(type, listName, (undefined !== module && "tabisaku" === module) ? selectValue : '', thisObj);
				}
			}else{
				m_tourModuleClassFare_setPull(type, listName, (undefined !== module && "tabisaku" === module) ? selectValue : '', thisObj);	
			}
		});
	}
	
	// 初回構築
	var listName = m_tourModuleClassFare_getPullList(lists ,selectListObject1, selectListObject2, type, inputObject);
	m_tourModuleClassFare_setPull(type, listName, selectValue, thisObj);
}

// 座席クラスプルダウン選択肢の取得
function m_tourModuleClassFare_getPullList(lists, selectListObject1, selectListObject2, type, inputObject) {
	var listName = undefined;
	// 優先選択1
	listName = lists[$tour_module_jq(selectListObject1).val()];
	
	// 指定しない判定（優先選択１に対応するリストが無く、かつ「指定しない」用定義が設定されている場合）
	if (!listName && !$tour_module_jq(selectListObject1).val()) {
		listName = lists[$tour_module_jq(selectListObject2).val() + m_inttourModuleCommon_FareList_Unspecified]
	}
	
	if(!listName){
		// 優先選択2
		listName = lists[$tour_module_jq(selectListObject2).val()];
	}
	if(listName === undefined){
		listName = lists['default'];
		// エリア未選択時は非活性化
		if(undefined !== type && "inttourtop" === type){
			$tour_module_jq(inputObject).parents('ul').addClass('disabled');
			$tour_module_jq(inputObject).parents('li').removeAttr('tabindex');
		}else{
			$tour_module_jq(inputObject).prop("disabled", true);
		}
	}else{
		// エリア取得時は活性化
		if(undefined !== type && "inttourtop" === type){
			$tour_module_jq(inputObject).parents('ul').removeClass('disabled');
			$tour_module_jq(inputObject).parents('li').attr('tabindex','0');
		}else{
			$tour_module_jq(inputObject).prop("disabled", false);
		}
	}
	return listName;
}
	
// 座席クラスプルダウン選択肢の生成
function m_tourModuleClassFare_setPull(type, listName , selectValue, thisObj) {
	$tour_module_jq(thisObj).empty();
	var html = '';
	for(var key in listName) {
		var date = listName[key];
		var codeValue = date['value'];
		var textValue = date['text'];

		if(undefined !== type && "inttourtop" === type){
			html += '<li data-val="' + codeValue + '" tabindex="0">' + textValue + '</li>\n';
		}else{
			html += '<option value="' + codeValue + '">' + textValue + '</option>\n';
		}
	}
	$tour_module_jq(thisObj).append(html);

	if(undefined !== selectValue){
		if(undefined !== type && "inttourtop" === type){
			var targetObj = $tour_module_jq(thisObj).parents('.mod-customSelect');
			var selDataObj = $tour_module_jq(thisObj).find('li[data-val="' + selectValue + '"]');
			if(selDataObj.length <= 0){
				// 指定値がリストに存在しない場合、リストの先頭を選択
				selDataObj = $tour_module_jq(thisObj).find('li:first');
				selectValue = selDataObj.data('val');
			}
			targetObj.find('.m_classFare_text').text(selDataObj.text());
			selDataObj.addClass('mod-current');
			targetObj.find('.m_classFare_hidden').val(selectValue);
		}else{
			var option = '[value="' + selectValue + '"]';
			$tour_module_jq(thisObj).find(option).prop('selected', true);
		}
	}
};

/////////////////////////////////////
// 座席クラスプルダウン部品 END
/////////////////////////////////////

/////////////////////////////////////
//再検索パラメータ設定 START
/////////////////////////////////////

//再検索パラメータ設定値反映
function m_tourModuleCommon_setResearchValue(listName){
	//URL取得
	var url = location.href;

	//URLパラメータ取得
	var split_param = url.split('?');

	//パラメータ格納処理
	if (1 < split_param.length) {
		var url_param = split_param[1].split("&");

		//パラメータ格納用
		var params = new Object;
		for (i = 0; i < url_param.length; i++) {
			var keyValue = url_param[i].split('=');
			var value = keyValue[1].replace(/\+/g,'%20');
			var decodeValue = decodeURIComponent(value);
			params[keyValue[0]] = decodeValue;
		}
		m_tourModuleCommon_setResearchValue_params = $tour_module_jq.extend(m_tourModuleCommon_setResearchValue_params, params);
	}

	// 無毒化処理
	m_tourModuleCommon_setResearchValue_params = m_tourModuleCommon_sanitizing(m_tourModuleCommon_setResearchValue_params);
	
	// パラメータがあれば処理
	if(m_tourModuleCommon_setResearchValue_params !== undefined){
		// 各項目値反映用
		var optionsParam = new Object;
		
		// キー値変換処理
		for(module in listName){
			// モジュール名でループ
			var moduleName = listName[module];

			var moduleParam = {};
			var getParams = new Object;

			// モジュールごとパラメータ存在フラグ
			var flag = 0;

			for(key in m_tourModuleCommon_setResearchValue_params){
				// リストにあればセット
				if(moduleName[key] !== undefined){
					//初期値変数名でパラメータを格納
					getParams[moduleName[key]] = m_tourModuleCommon_setResearchValue_params[key];
					flag = 1;
				}
			}
			// 旅作では参加人数判定を行う
			if(module == 'inttourDp'){
				var row_param = m_tourModuleNumberOfPeople_defaultParams.row;
				var column_param = m_tourModuleNumberOfPeople_defaultParams.column;
				// 参加人数が未選択の場合は設定しない
				if(m_tourModuleCommon_setResearchValue_params[column_param[0].name + row_param[0].name] !== undefined){
					// 参加人数格納用変数
					var numberOfPeopleParams = [];
					// 部屋数判定用フラグ
					var room_flag = true;
					
					// 参加人数のパラメータを整形
					for(i=0;row_param[i];i++){
						var numberOfPeopleInit = [];
						for(j=0;column_param[j+1];j++){
							var numKey = column_param[j].name + row_param[i].name;
							var number = m_tourModuleCommon_setResearchValue_params[numKey];
							if(number === undefined){
								numberOfPeopleInit[j] = 0;
								if(room_flag){
									room_flag = false;
									getParams['m_defNumberOfRooms'] = String(i);
								}
							}else{
								numberOfPeopleInit[j] = parseInt(m_tourModuleCommon_setResearchValue_params[numKey]);
								if(i==7 && room_flag === true){
								// 部屋数最大時
									getParams['m_defNumberOfRooms'] = "8";
								}
							}
						}
						numberOfPeopleParams[i] = numberOfPeopleInit;
					}
					// 参加人数を設定
					getParams['m_defNumberOfPeopleInit'] = numberOfPeopleParams;
				}
				// 旅作チェックボックス判定
				if(getParams['m_defIsUpgradeAwardObject'] !== undefined && getParams['m_defIsUpgradeAwardObject'] != 0){
					getParams['m_defIsUpgradeAwardObject'] = 'checked';
				}else{
					getParams['m_defIsUpgradeAwardObject'] = "";
				}
				
			}else if(module == 'inttourPkg'){
				// パッケージツアーチェックボックス判定
				if(getParams['m_defIsLandOnly'] !== undefined && getParams['m_defIsLandOnly'] != 0){
					getParams['m_defIsLandOnly'] = 'checked';
				}else{
					getParams['m_defIsLandOnly'] = "";
				}
			}else if (module == 'domtourDp'){
				// 旅作レンタカーも予約チェックボックス判定
				if(getParams['m_defIsWithCar'] !== undefined && getParams['m_defIsWithCar'] != 0){
					getParams['m_defIsWithCar'] = 'checked';
				}else{
					getParams['m_defIsWithCar'] = "";
				}
				
				
				//子供・幼児
				var childCnt =[
				               m_tourModuleCommon_setResearchValue_params['childADpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childIDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childCDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childJDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childEDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childFDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childGDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['childHDpCnt'],
				               m_tourModuleCommon_setResearchValue_params['infantCnt']
				               ];
				for(key in childCnt){
					if(!isNaN(Number(childCnt[key]))){
						childCnt[key] = Number(childCnt[key]);
					}else{
						childCnt[key] = Number('0');
					}
				}
				
				getParams['m_defNumberOfChild'] =[
				                                  [ childCnt[0], childCnt[1], 0, 0, 0, 0, 0 ],						// 9～11歳
				                                  [ childCnt[2], childCnt[3], 0, 0, 0, 0, 0 ],						// 6～8歳
				                                  [ 0, 0, childCnt[4], childCnt[5], childCnt[6], childCnt[7], 0 ],	// 3～5歳
				                                  [ 0, 0, 0, 0, 0, 0, childCnt[8] ]									// 0～2歳
				                                  ]; 
				
			}else if(module == 'domtourPkg'){
				//新モジュールに切り替え
				getParams['m_defModuleMode'] = '0';
				
				//行き先検索、商品コード検索判定
				if(getParams['m_defNumberOfDays'] !== undefined && getParams['m_defNumberOfDays'] !== ''){
					//行き先検索の場合 何もしない
				}else{
					//商品コード検索の場合 商品コード側パラメータに入れ替え
					getParams['m_defCourseDateDep'] = getParams['m_defDateDep'];
					getParams['m_defDateDep'] = "";
					getParams['m_defCourseAirportDep'] = getParams['m_defAirportDep'];
					getParams['m_defAirportDep'] = "";
				}
				
				//子供・幼児
				var childCnt =[
				               m_tourModuleCommon_setResearchValue_params['childAPkgCnt'],
				               m_tourModuleCommon_setResearchValue_params['childBPkgCnt'],
				               m_tourModuleCommon_setResearchValue_params['childCPkgCnt'],
				               m_tourModuleCommon_setResearchValue_params['childDPkgCnt'],
				               m_tourModuleCommon_setResearchValue_params['infantCnt']
				               ];
				
				for(key in childCnt){
					if(!isNaN(Number(childCnt[key]))){
						childCnt[key] = Number(childCnt[key]);
					}else{
						childCnt[key] = Number('0');
					}
				}
				
				getParams['m_defNumberOfChild']=[
				                                 [ childCnt[0], childCnt[1], 0 ],	// 6～11歳
				                                 [ childCnt[2], childCnt[3], 0 ],	// 3～5歳
				                                 [ 0, 0, childCnt[4] ]				// 0～2歳
				                                 ];
			}else if(module == 'domtourHotel'){
				//子供・幼児
				var childCnt =[
				               m_tourModuleCommon_setResearchValue_params['childAHtlCnt'],
				               m_tourModuleCommon_setResearchValue_params['childBHtlCnt'],
				               m_tourModuleCommon_setResearchValue_params['childCHtlCnt'],
				               m_tourModuleCommon_setResearchValue_params['childDHtlCnt'],
				               m_tourModuleCommon_setResearchValue_params['childEHtlCnt'],
				               m_tourModuleCommon_setResearchValue_params['childFHtlCnt']
				               ];
				
				for(key in childCnt){
					if(!isNaN(Number(childCnt[key]))){
						childCnt[key] = Number(childCnt[key]);
					}else{
						childCnt[key] = Number('0');
					}
				}
				
				getParams['m_defNumberOfChild']=[
				                                 [ 0, 0, 0, 0, childCnt[0] ],								// 9～11歳
				                                 [ 0, 0, 0, 0, childCnt[1] ],								// 6～8歳
				                                 [ childCnt[2], childCnt[3], childCnt[4], childCnt[5], 0 ]	// 0～5歳
				                                ];
				
			}
			
			// 各モジュール用の値をセット
			moduleParam[module] = getParams;
			if(flag != 0){
				optionsParam = $tour_module_jq.extend(optionsParam,moduleParam);
			}
		}
		return optionsParam;
	}
};

/////////////////////////////////////
//再検索パラメータ設定 END
/////////////////////////////////////

/////////////////////////////////////
//無毒化処理 START
/////////////////////////////////////

//無毒化処理
function m_tourModuleCommon_sanitizing(params) {
	if(params === null || params === undefined || params == ''){
		return params;
	}
	var escapeMap = {
		'&': '&amp;',
		"'": '&#x27;',
		'`': '&#x60;',
		'"': '&quot;',
		'<': '&lt;',
		'>': '&gt;'
	};
	var escapeReg = '[';
	for (var p in escapeMap) {
		if (escapeMap.hasOwnProperty(p)) {
			escapeReg += p;
		}
	}
	escapeReg += ']';
	var reg = new RegExp(escapeReg, 'g');
	
	for(key in params){
		if(params[key] !== undefined && params[key] != ''){
			params[key] = params[key].replace(reg, function(match){
				return escapeMap[match];
			});
		}
	}
	return params;
};

/////////////////////////////////////
//無毒化処理 END
/////////////////////////////////////

/////////////////////////////////////
//旅行日数処理 START
/////////////////////////////////////

//旅行日数モジュールの生成
function m_tourModuleTravelOfDays_createParts(options) {
	
	var inputDefSelect = options.defSelect;
	
	//HTML生成
	var html = '';
	var defSelect ='';
	var defValue ='';
	
	if(options.site === 'domtour2nd'){
		//国内2ndモジュールの場合
		var inputClass = options.inputClass;
		var inputId = options.inputId;
		var inputName = options.inputName;
		
		//各モジュールのプルダウンリストの取得
		var pullDownList;
		
		if (options.module === 'pack'){
			//パッケージ
			pullDownList = m_domtourModulePkg_travelOfDaysList;
		}else if (options.module === 'bulk'){
			//ツアー一括
			pullDownList = m_domtourModuleBulk_travelOfDaysList;
		}else if (options.module === 'hotel_bulk'){
			//ホテル（３社一括）
			pullDownList = m_domtourModuleHotelBulk_stayDaysList;
		}else{
			
		}
		
		html += '<select name="' + inputName + '" id="' + inputId + '" class="mod-select ' + inputClass + '">\n';
		for(var i in pullDownList){
			if(inputDefSelect === pullDownList[i].value){
				html += '<option value="' +pullDownList[i].value+ '" selected>' +pullDownList[i].text+ '</option>\n';
			}else{
				html += '<option value="' +pullDownList[i].value+ '">' +pullDownList[i].text+ '</option>\n';
			}
		}
		html += '</select>\n';
	}else if(options.site === 'domtourtop' || options.site === 'dom'){
		//国内TOPモジュールの場合
		var inputId = options.inputId;
		var inputName = options.inputName;
		if (options.module === 'pack'){
			//パッケージ
			pullDownList = m_domtourModulePkg_travelOfDaysList;
		}else if (options.module === 'bulk'){
			//ツアー一括
			pullDownList = m_domtourModuleBulk_travelOfDaysList;
		}else if (options.module === 'hotel_bulk'){
			pullDownList = m_domtourModuleHotelBulk_stayDaysList;
		}else{
			
		}
		
		html += '<ul class="item-num-set">\n';
		html += '<li class="item-num-down" tabindex="0">?</li>\n';
		html += '<li class="item-num-val">\n';
		html += '<ul class="mod-customSelect">\n';
		html += '<li tabindex="0"><span class="item-val"></span>\n';
		html += '<input type="hidden" id="' + inputId + '" name="' + inputName + '">\n';
		html += '<ul class="mod-option" data-comtomselect="' + inputId + '" style="display: none;">\n';
		
		for(var i in pullDownList){
			
			if(pullDownList[i].value === options.defSelect){
				defSelect = pullDownList[i].text;
				defValue = pullDownList[i].value;
				html += '<li data-val="' +pullDownList[i].value+ '" tabindex="0" class="mod-current">' +pullDownList[i].text+ '</li>\n';
			}else{
				html += '<li data-val="' +pullDownList[i].value+ '" tabindex="0">' +pullDownList[i].text+ '</li>\n';
			}
			
		}
		html += '</ul>\n';
		html += '</li>\n';
		html += '</ul>\n';
		html += '</li>\n';
		html += '<li class="item-num-up" tabindex="0">＋</li>\n';
		html += '</ul>\n';
	}
	

	this.append(html);
	
	if(options.site === 'domtourtop'|| options.site === 'dom'){
		if(0 !== defSelect.length){
			this.find("span" + '.item-val').text(defSelect);
			this.find("#" + inputId).val(defValue);
		}else{
			this.find("span" + '.item-val').text(pullDownList[0].text);
			this.find("#" + inputId).val(pullDownList[0].value);
		}
		
		
	}
	
};

/////////////////////////////////////
//旅行日数処理 END
/////////////////////////////////////

/////////////////////////////////////
//人数処理 START
/////////////////////////////////////

//人数（プルダウン）モジュールの生成
function m_tourModulePeoplesPull_createParts(options) {
	
	
	//HTML生成
	var html = '';
	var defSelect ='';
	var defValue ='';
	
	if(options.site === 'domtour2nd'){
		//国内2ndモジュールの場合
		var inputClass = options.inputClass;
		var inputId = options.inputId;
		var inputName = options.inputName;
		var inputDefSelect = options.defSelect;
		
		html += '<select name="' + inputName + '" id="' + inputId + '" class="mod-select ' + inputClass + '">\n';
		
		
		//各モジュールのプルダウンリストの取得
		var pullDownList;
		
		if(options.module === 'tabisaku'){
			//旅作
			pullDownList = m_domtourModuleDp_PeoplesPulldownList;
		}else if (options.module === 'pack'){
			//パッケージ
			pullDownList = m_domtourModulePkg_PeoplesPulldownList;
		}else if (options.module === 'hotel'){
			//ホテル
			pullDownList = m_domtourModuleHotel_PeoplesPulldownList;
		}else if (options.module === 'bulk'){
			//ツアー一括
			pullDownList = m_domtourModuleBulk_PeoplesPulldownList;
		}else if (options.module === 'hotel_bulk'){
			//ホテル(3社一括)
			pullDownList = m_domModuleHotel_PeoplesPulldownList;
		}else{
			
		}
		
		for(var i in pullDownList){
			
			if(inputDefSelect === pullDownList[i].value){
				html += '<option value="' +pullDownList[i].value+ '" selected>' +pullDownList[i].text+ '</option>\n';
			}else{
				html += '<option value="' +pullDownList[i].value+ '">' +pullDownList[i].text+ '</option>\n';
			}
			
		}
		html += '</select>\n';
	}else if(options.site === 'domtourtop'||options.site === 'dom'){
		//国内TOPモジュールの場合
		var inputId = options.inputId;
		var inputName = options.inputName;
		var downId = "";
		var valId = "";
		var upId = "";
		if(options.module === 'tabisaku'){
			//旅作
			pullDownList = m_domtourModuleDp_PeoplesPulldownList;
		}else if (options.module === 'pack'){
			//パッケージ
			pullDownList = m_domtourModulePkg_PeoplesPulldownList;
		}else if (options.module === 'hotel'|| options.module === 'hotel_bulk'){
			//ホテル
			if(options.site === 'dom'){
				pullDownList = m_domModuleHotel_PeoplesPulldownList;
			}else{
				pullDownList = m_domtourModuleHotel_PeoplesPulldownList;
				var downId = inputName+"_down";
				var valId = inputName+"_val";
				var upId = inputName+"_up";
			}
			
		}else if (options.module === 'bulk'){
			//ツアー一括
			pullDownList = m_domtourModuleBulk_PeoplesPulldownList;
		}else{
			
		}
		
		html += '<ul class="item-num-set">\n';
		html += '<li class="item-num-down" tabindex="0" id="'+downId+'">?</li>\n';
		html += '<li class="item-num-val">\n';
		html += '<ul class="mod-customSelect">\n';
		html += '<li tabindex="0"  id="'+valId+'"><span class="item-val"></span>\n';
		html += '<input type="hidden" id="' + inputId + '" name="' + inputName + '">\n';
		html += '<ul class="mod-option" data-comtomselect="' + inputId + '" style="display: none;">\n';
		
		for(var i in pullDownList){
			
			if(pullDownList[i].value === options.defSelect){
				defSelect = pullDownList[i].text;
				defValue = pullDownList[i].value;
				html += '<li data-val="' +pullDownList[i].value+ '" tabindex="0" class="mod-current">' +pullDownList[i].text+ '</li>\n';
			}else{
				html += '<li data-val="' +pullDownList[i].value+ '" tabindex="0">' +pullDownList[i].text+ '</li>\n';
			}
			
		}
		html += '</ul>\n';
		html += '</li>\n';
		html += '</ul>\n';
		html += '</li>\n';
		html += '<li class="item-num-up" tabindex="0"  id="'+downId+'">＋</li>\n';
		html += '</ul>\n';
	}
	

	this.append(html);
	
	if(options.site === 'domtourtop'||options.site === 'dom'){
		if(0 !== defSelect.length){
			this.find("span" + '.item-val').text(defSelect);
			this.find("#" + inputId).val(defValue);
		}else{
			this.find("span" + '.item-val').text(pullDownList[0].text);
			this.find("#" + inputId).val(pullDownList[0].value);
		}
		
		
	}
	
};

/////////////////////////////////////
//人数処理 END
/////////////////////////////////////

/////////////////////////////////////
//目的処理 START
/////////////////////////////////////

//目的（プルダウン）モジュールの生成
function m_tourModulePurposePull_createParts(options) {
	
	
	//HTML生成
	var html = '';
	var defSelect ='';
	var defValue ='';
	
	if(options.site === 'domtour2nd'){
		//国内2ndモジュールの場合
		var inputClass = options.inputClass;
		var inputId = options.inputId;
		var inputName = options.inputName;
		
		html += '<select name="' + inputName + '" id="' + inputId + '" class="mod-select ' + inputClass + '">\n';
		for(var i in m_domtourModule_PurposePulldownList){
			if(m_domtourModule_PurposePulldownList[i].value === options.defSelect){
				html += '<option value="' +m_domtourModule_PurposePulldownList[i].value+ '" selected>' +m_domtourModule_PurposePulldownList[i].text+ '</option>\n';
			}else{
				html += '<option value="' +m_domtourModule_PurposePulldownList[i].value+ '">' +m_domtourModule_PurposePulldownList[i].text+ '</option>\n';
			}
		}
		html += '</select>\n';
	}else if(options.site === 'domtourtop'){
		//国内TOPモジュールの場合
		var inputClass = options.inputClass;
		var inputId = options.inputId;
		var inputName = options.inputName;
		
		
		html += '<div class="select-wrap item-input">\n';
		html += '<ul class="mod-customSelect">\n';
		html += '<li tabindex="0"><span class="item-val-02"></span><input type="hidden" id="' + inputId + '" name="' + inputName + '">\n';
		html += '<ul class="mod-option" data-comtomselect="' + inputId + '" style="display: none;">\n';
		for(var i in m_domtourModule_PurposePulldownList){
		html += '<li data-val="' +m_domtourModule_PurposePulldownList[i].value+ '" tabindex="0">' +m_domtourModule_PurposePulldownList[i].text+ '</li>\n';
			if(m_domtourModule_PurposePulldownList[i].value === options.defSelect){
				defSelect = m_domtourModule_PurposePulldownList[i].text;
				defValue = m_domtourModule_PurposePulldownList[i].value;
			}
		}
		html += '</ul>\n';
		html += '</li>\n';
		html += '</ul>\n';
		html += '</div>\n';
		
	}
	

	this.append(html);
	
	if(options.site === 'domtourtop'){
		if(0 !== defSelect.length){
			this.find("span" + '.item-val-02').text(defSelect);
			this.find("#" + inputId).val(defValue);
		}else{
			this.find("span" + '.item-val-02').text(m_domtourModule_PurposePulldownList[0].text);
			this.find("#" + inputId).val(m_domtourModule_PurposePulldownList[0].value);
		}
		
		
	}
	
};

/////////////////////////////////////
//目的処理 END
/////////////////////////////////////

/////////////////////////////////////
//時間処理 START
/////////////////////////////////////

//時間（プルダウン）モジュールの生成
function m_tourModuleTimePull_createParts(options) {
	
	
	//HTML生成
	var html = '';
	var defSelect ='';
	var defValue ='';
	
	if(options.site === 'domtour2nd'){
		//国内2ndモジュールの場合
		var inputClass = options.inputClass;
		var inputId = options.inputId;
		var inputName = options.inputName;
		
		html += '<select name="' + inputName + '"" id="' + inputId + '" class="mod-select ' + inputClass + '">\n';
		for(var i in m_domtourModule_TimePulldownList){
			
			if(m_domtourModule_TimePulldownList[i].value === options.defSelect){
				html += '<option value="' +m_domtourModule_TimePulldownList[i].value+ '" selected="">' +m_domtourModule_TimePulldownList[i].text+ '</option>\n';
			}else{
				html += '<option value="' +m_domtourModule_TimePulldownList[i].value+ '">' +m_domtourModule_TimePulldownList[i].text+ '</option>\n';
			}
			
			
		}
		html += '</select>\n';
	}else if(options.site === 'domtourtop'){
		var inputId = options.inputId;
		var inputName = options.inputName;
		//国内TOPモジュールの場合
		html += '<div class="select-wrap item-input mt10">\n';
		html += '<ul class="mod-customSelect">\n';
		html += '<li tabindex="0"><span class="item-val-02"></span><input type="hidden" id="'+inputId+'" name="'+inputName+'">\n';
		html += '<ul class="mod-option" data-comtomselect="'+inputId+'" style="display: none;">\n';
		for(var i in m_domtourModule_TimePulldownList){
			
			if(m_domtourModule_TimePulldownList[i].value === options.defSelect){
				html += '<li data-val="' +m_domtourModule_TimePulldownList[i].value+ '" tabindex="0" class="mod-current">' +m_domtourModule_TimePulldownList[i].text+ '</li>\n';
				defSelect = m_domtourModule_TimePulldownList[i].text;
				defValue = m_domtourModule_TimePulldownList[i].value;
			}else{
				html += '<li data-val="' +m_domtourModule_TimePulldownList[i].value+ '" tabindex="0">' +m_domtourModule_TimePulldownList[i].text+ '</li>\n';
			}
			
		}
		html += '</ul>\n';
		html += '</li>\n';
		html += '</ul>\n';
		html += '</div>\n';
	}
	
	

	this.append(html);
	
	if(options.site === 'domtourtop'){
			this.find("span" + '.item-val-02').text(defSelect);
			this.find("#" + inputId).val(defValue);
	}
	
};

/////////////////////////////////////
//時間処理 END
/////////////////////////////////////

/////////////////////////////////////
//チェックボックスリスト処理 START
/////////////////////////////////////

//チェックボックスリストモジュールの生成
function m_tourModuleCheckboxList_createParts(options) {
	
	var inputdefCheck =[];
	
	if(options.defCheck instanceof Array){
		inputdefCheck = options.defCheck
	}else{
		
	}
	
	//HTML生成
	var html = '';
	
	if(options.site === 'domtour2nd'){
		//国内2ndモジュールの場合
		
		html += '<ul class="check-list">\n';
		
		var inputName = options.inputName;
		
		//各モジュールのプルダウンリストの取得
		var pullDownList;
		
		if(options.module === 'hotel'){
			//ホテル こだわり条件
			pullDownList = m_domtourModule_HotelOptionList;
		}else if(options.module === 'car'){
			//レンタカー 車両クラス
			pullDownList = m_domtourModule_CarClassList;
		}else if(options.module === 'hotel_bulk'){
			//ホテル(3社一括) こだわり条件
			pullDownList = m_domtourModule_HotelBulkOptionList;
		}else{
			
		}
		
		for(var i in pullDownList){
			
			if(inputdefCheck.indexOf(pullDownList[i].id) >= 0){
				html += '<li class="ico-checkbox vtop"><input name="' + inputName + '" value="' + i + '" id="' + pullDownList[i].id + '_' + options.uniqueKey + '" type="checkbox" checked>\n';
				html += '<label tabindex="0" for="' + pullDownList[i].id + '_' + options.uniqueKey + '" class=""><span>' + pullDownList[i].text + '</span></label></li>\n';
			}else{
				html += '<li class="ico-checkbox vtop"><input name="' + inputName + '" value="' + i + '" id="' + pullDownList[i].id + '_' + options.uniqueKey +  '" type="checkbox">\n';
				html += '<label tabindex="0" for="' + pullDownList[i].id + '_' + options.uniqueKey + '" class=""><span>' + pullDownList[i].text + '</span></label></li>\n';
			}
		}
		html += '</ul>\n';
	}else if(options.site === 'domtourtop' || options.site === 'dom'){
		//国内TOPモジュールの場合
		html += '<ul class="check-group multiple '+options.inputulClass+'">\n';
		
		var inputName = options.inputName;
		
		//各モジュールのプルダウンリストの取得
		var pullDownList;
		
		if(options.module === 'hotel'){
			//ホテル こだわり条件
			pullDownList = m_domtourModule_HotelOptionList;
		}else if(options.module === 'car'){
			//レンタカー 車両クラス
			pullDownList = m_domtourModule_CarClassList;
		}else if(options.module === 'hotel_bulk'){
			//ホテル(3社一括) こだわり条件
			pullDownList = m_domtourModule_HotelBulkOptionList;
		}else{
			
		}
		
		for(var i in pullDownList){
			html += '<li class="'+options.inputliClass+'">\n';
			html += '<span class="ico-checkbox">\n';
			if(inputdefCheck.indexOf(pullDownList[i].id) >= 0){
				html += '<input type="checkbox" checked id="' + pullDownList[i].id+'" value="' + i + '" name="' + inputName + '">\n';
				html += '<label tabindex="0" for="' + pullDownList[i].id+'" class=""><span>' + pullDownList[i].text + '</span></label>\n';
			}else{
				html += '<input type="checkbox" id="' + pullDownList[i].id+'" value="' + i + '" name="' + inputName + '">\n';
				html += '<label tabindex="0" for="' + pullDownList[i].id+'" class=""><span>' + pullDownList[i].text + '</span></label>\n';
			}
			html += '</span>\n';
			html += '</li>\n';
		}
	}
		html += '</ul>\n';
	this.append(html);
	
};

/////////////////////////////////////
//チェックボックスリスト処理 END
/////////////////////////////////////

// 外部公開メソッドの定義
$tour_module_jq.fn.tourCommonModule = function(method) {

	var methods = {
		'setTargetValue' : m_tourModuleCommon_setTargetValue,
		'createNumberOfPeople' : m_tourModuleNumberOfPeople_createParts,
		'createDateInput' : m_tourModuleCalendar_createParts,
		'createAirportInput' : m_tourModuleAirportList_createParts,
		'createStayCityCountRadio' : m_tourModuleStayCityRadio_createParts,
		'setClassFarePull' : m_tourModuleClassFare_createParts,
		'setNumberOfRooms' : m_tourModuleNumberOfRooms_createParts,
		'createInputHidden' : m_tourModuleHidden_createParts,
		'setResearchValue' : m_tourModuleCommon_setResearchValue,
		'createTravelOfDays' : m_tourModuleTravelOfDays_createParts,
		'createPeoplesPull' : m_tourModulePeoplesPull_createParts,
		'createPurposePull' : m_tourModulePurposePull_createParts,
		'createTimePull' : m_tourModuleTimePull_createParts,
		'createCheckboxList' : m_tourModuleCheckboxList_createParts
	};

	// Dispatch the proper method
	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.tourCommonModule');
	}
};
