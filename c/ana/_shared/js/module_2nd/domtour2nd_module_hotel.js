/*=====================================================
* Script Name: domtour2nd_module_hotel.js
* Description: 空席照会モジュール 国内ツアー2nd ホテル
* Version: 1.00
* Last Up Date: 2017/10/11
=====================================================*/

var m_domtour2ndModuleHotel_siteType = { site : 'domtour2nd', module : 'hotel' };

var m_domtour2ndModuleHotel_usePartsCount_AdultPulldown = 1;			//大人、配置数カウンタ
var m_domtour2ndModuleHotel_usePartsCount_AddConditionCheckBox = 1;		//こだわり条件部品、配置数カウンタ

//初期値設定
var m_domtour2ndModuleHotel_defaults = {
	m_defCheckin:'',					//チェックイン用
	m_defCheckout:'',					//チェックアウト用
	m_defPlaceOfStayDistrict:'',		//宿泊地用(方面)
	m_defPlaceOfStayRegion:'',			//宿泊地用(地区)
	m_defPlaceOfStayArea:'',			//宿泊地用(地域)
	m_defNumberOfAdult:'',				//大人用
	m_defNumberOfChild:m_domtourModuleNumberOfPeopleHotel_optionParams.initNum,   //参加人数（子供幼児）用
	m_defAddConditionChecked:[''],		//こだわり条件用
	m_defTabNoDispCls:''				//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleHotel_optionParam = $tour_module_jq.extend(m_domtour2ndModuleHotel_optionParam, m_domtour2ndModuleHotel_defaults);

/**
 * 国内ツアーホテル2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModuleHotel_setParams(options){
	m_domtour2ndModuleHotel_optionParam = $tour_module_jq.extend(m_domtour2ndModuleHotel_optionParam, options);
};

/**
 * 国内ツアーホテル2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleHotel_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアーホテルの各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModuleHotel_incorrectValue(options, m_domtour2ndModuleHotel_siteType, m_domtour2ndModuleHotel_defaults));
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	
	//チェックイン・チェックアウト
	var checkText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_check, 'Text1');
	var checkinText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_check, 'Text2');
	var checkoutText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_check, 'Text3');
	//宿泊地
	var placeOfStaText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_placeOfStay, 'Text');
	//1部屋あたりの利用人数
	var roomOfPeopleLabelText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_roomOfPeopleLabel, 'Text');
	//大人
	var adultText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_adult, 'Text');
	//子供・幼児
	var childText = m_tourModuleCommon_getListValue(m_domtourModuleHotel_child, 'Text');
	var childTextCaution = m_tourModuleCommon_getListValue(m_domtourModuleHotel_child, 'Caution');
	var numberOfPeoplePartsId1 = m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.hotel;
	var numberOfPeopleCount1 = m_tourModuleCommon_zeroPadding(m_tourModuleNumberOfPeople_usePartsCount); // 参加人数ID番号設定
	var numberOfPeopleTextId1 = m_tourModuleNumberOfPeople_defaultParams.textId;
	//こだわり条件
	var addConditionText1 = m_tourModuleCommon_getListValue(m_domtourModuleHotel_addCondition, 'Text1');
	var addConditionText2 = m_tourModuleCommon_getListValue(m_domtourModuleHotel_addCondition, 'Text2');
	//検索ホタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModuleHotel_fromAction, 'URL');
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');
	
	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defFormMethod');
	
	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<form  class="form-domHotel" id="form-domHotel" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<div class="mod-content_wrap">\n';
	html += '<div class="mod-content_cover col3">\n';
	
	//1カラム目 チェックイン チェックアウト 宿泊地
	html += '<div class="mod-content_box">\n';
	
	//チェックイン
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + checkText + '<span class="ib bold">' + checkinText + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_checkin">\n';
	//チェックイン部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//チェックアウト
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + checkText + '<span class="ib bold">' + checkoutText + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_checkout">\n';
	//チェックアウト部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//宿泊地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head bold">' + placeOfStaText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_placeOfStay">\n';
	//宿泊地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//1カラム目 END
	
	
	//2カラム目 1部屋あたりの利用人数(大人、子供・幼児)
	html += '<div class="mod-content_box">\n';
	html += '<p class="mod-title small">' + roomOfPeopleLabelText + '</p>\n';
	
	//大人
	html += '<fieldset class="mod-input_box column">\n';
	html += '<legend><span class="mod-input_head">' + adultText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_adult">\n';
	//大人部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//子供・幼児
	html += '<fieldset class="mod-input_box column">\n';
	html += '<legend><span class="mod-input_head">' + childText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_child">\n';
	html += '<input type="text" readonly name="" id="' + numberOfPeopleTextId1 + numberOfPeopleCount1 + '" value="" class="ico_select mod-input mod-icon js-mod-balloon m_dom_hotel_children m_numberOfPeople" data-balloon="'+ numberOfPeoplePartsId1 + numberOfPeopleCount1 +'" data-control-point="'+ dataControlPointValue +'" disabled>\n';
	html += '<p class="notes m_dispChildCaution">' + childTextCaution + '</p>\n';
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 END
	
	
	//3カラム目 こだわり条件 検索ボタン
	html += '<div class="mod-content_box mod-btn_box">\n';
	
	//こだわり条件
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head isSPvtop bold">' + addConditionText1 + '<span class="ib bold">' + addConditionText2 + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_addCondition">\n';
	//こだわり条件部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '    <div class="btn btn-search" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//3カラム目 END

	
	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</div><!-- / .mod-content_wrap -->\n';
	html += '</form>\n';
	html += '\n';

	this.append(html);
	
	m_domtour2ndModuleHotel_setDefVal(this, options);
	
};

function m_domtour2ndModuleHotel_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defModulePosition');
	// 宿泊地(方面、地区、地域)設定内容の補正（どれか一つでも設定されていたらundefinedの項目は空文字を設定する）
	if (options.m_defPlaceOfStayDistrict !== undefined ||
		options.m_defPlaceOfStayRegion !== undefined ||
		options.m_defPlaceOfStayArea !== undefined) {

		if (options.m_defPlaceOfStayDistrict === undefined) {
			options.m_defPlaceOfStayDistrict = '';
		}
		if (options.m_defPlaceOfStayRegion === undefined) {
			options.m_defPlaceOfStayRegion = '';
		}
		if (options.m_defPlaceOfStayArea === undefined) {
			options.m_defPlaceOfStayArea = '';
		}
	}
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	//パラメータ設定を取得
	//チェックイン
	var defCheckin = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defCheckin');
	//チェックアウト
	var defCheckout = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defCheckout');
	// 宿泊地(方面)
	var defPlaceOfStayDistrict = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defPlaceOfStayDistrict');
	// 宿泊地(地区)
	var defPlaceOfStayRegion = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defPlaceOfStayRegion');
	// 宿泊地(地域)
	var defPlaceOfStayArea = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defPlaceOfStayArea');
	//大人
	var defNumberOfAdult = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defNumberOfAdult');
	 // 子供(参加人数部品)
	var defNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defNumberOfChild');
	var numberOfPeopleCount1 = m_tourModuleCommon_zeroPadding(m_tourModuleNumberOfPeople_usePartsCount);
	//こだわり条件
	var defAddConditionChecked = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defAddConditionChecked');
	// 再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModuleHotel_siteType, options, 'm_defTabNoDispCls');
	
	//チェックイン
	target.find('.m_checkin').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleHotel_check, 'BalloonTitle1'),
		defPartsPosition : dataControlPointValue,
		defDate : defCheckin,
		defSelectDay : m_domtourModuleHotel_checkin_defSelectDay,
		dateValueClass : m_domtourModuleHotel_checkin_dateValueClass,
		dateValueName : m_domtourModuleHotel_checkin_dateValueName,
		inputPlaceholder : m_domtourModuleHotel_checkin_inputPlaceholder,
		inputClass : m_domtourModuleHotel_checkin_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModuleHotel_checkin_isRequired,
		isDisabled : m_domtourModuleHotel_checkin_isDisabled,
		isDispAllOfMonth : m_domtourModuleHotel_checkin_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleHotel_checkin_prevNextInterval,
		receiptDisableDays : m_domtourModuleHotel_checkin_receiptDisableDays,
		dispMonths : m_domtourModuleHotel_checkin_dispMonths,
		selectableMonths : m_domtourModuleHotel_checkin_selectableMonths,
		isDispHoliday : m_domtourModuleHotel_checkin_isDispHoliday,
		boundaryTime : m_domtourModuleHotel_checkin_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//チェックアウト
	defCheckoutOptions = {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleHotel_check, 'BalloonTitle2'),
		defPartsPosition : dataControlPointValue,
		defDate : defCheckout,
		defSelectDay : m_domtourModuleHotel_checkout_defSelectDay,
		dateValueClass : m_domtourModuleHotel_checkout_dateValueClass,
		dateValueName : m_domtourModuleHotel_checkout_dateValueName,
		inputPlaceholder : m_domtourModuleHotel_checkout_inputPlaceholder,
		inputClass : m_domtourModuleHotel_checkout_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 1),
		isRequired : m_domtourModuleHotel_checkout_isRequired,
		isDisabled : m_domtourModuleHotel_checkout_isDisabled,
		isDispAllOfMonth : m_domtourModuleHotel_checkout_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleHotel_checkout_prevNextInterval,
		receiptDisableDays : m_domtourModuleHotel_checkout_receiptDisableDays,
		dispMonths : m_domtourModuleHotel_checkout_dispMonths,
		selectableMonths : m_domtourModuleHotel_checkout_selectableMonths,
		isDispHoliday : m_domtourModuleHotel_checkout_isDispHoliday,
		boundaryTime : m_domtourModuleHotel_checkout_boundaryTime,
		startDateClass : m_domtourModuleHotel_checkout_startDateClass,
		daysFromStartDate : m_domtourModuleHotel_checkout_daysFromStartDate,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH,
		maxTravelDays : m_domtourModuleHotel_checkout_maxTravelDays,
		returnDaysFromDepartureDate : m_domtourModuleHotel_checkout_returnDaysFromDepartureDate
	}
	target.find('.m_checkout').tourCommonModule('createDateInput', defCheckoutOptions);
	
	//宿泊地
	target.find('.m_placeOfStay').tourCommonModule('createAirportInput', {
		parent : this,
		defAreaCode : defPlaceOfStayDistrict,                   // 方面
		defCountryCode : defPlaceOfStayRegion,                  // 地区
		defAirportCode : defPlaceOfStayArea,                    // 地域
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleHotel_stay['Text'],
		areaClass : 'm_hiddenHotelDistrict',                    // 方面
		areaName : 'districtCd',                                // 方面
		countryClass : 'm_hiddenHotelRegion',                   // 地区
		countryName : 'regionCd',                               // 地区
		apoCodeClass : 'm_hiddenHotelArea',                     // 地域
		apoCodeName : 'areaCd',                                 // 地域
		inputPlaceholder : m_domtour2ndModuleHotel_stay['Placeholder'],
		inputClass : 'm_domHotelPlaceOfStayText',
		isRequired : false,
		isDisabled : false,
		layoutType : '19',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType : '2'
	});
	
	//大人
	target.find('.m_adult').tourCommonModule('createPeoplesPull', {
		site : m_domtour2ndModuleHotel_siteType.site,
		module : m_domtour2ndModuleHotel_siteType.module,
		defSelect : defNumberOfAdult,
		inputClass:'m_dom_hotel_member',
		inputId:'dom_hotel_member' + m_tour2ndModuleHotel_returnCount_AddAdultPulldown(),
		inputName:'adultCnt'
	});
	
	//子供・幼児
	target.find('.m_child').tourCommonModule('createNumberOfPeople', {
		id: m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.hotel,
		className: m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.hotel,
		htmlType : m_tourModuleNumberOfPeople_FixedParams.domtour.sec.htmlType,
		moduleNo : numberOfPeopleCount1,
		NPLP : m_domtourModuleNumberOfPeopleHotel_NPLP,
		initNum : defNumberOfPeopleSelectInit,
		clearNum : defNumberOfPeopleSelectInit,
		column : m_domtourModuleNumberOfPeopleHotel_optionParams.column,
		row : m_domtourModuleNumberOfPeopleHotel_optionParams.row,
		dispMatrix : m_domtourModuleNumberOfPeopleHotel_optionParams.dispMatrix,
		textBoxTemplate : m_domtourModuleNumberOfPeopleHotel_optionParams.textBoxTemplate
	});
	
	//こだわり条件
	target.find('.m_addCondition').tourCommonModule('createCheckboxList', {
		site:m_domtour2ndModuleHotel_siteType.site,
		module : m_domtour2ndModuleHotel_siteType.module,
		defCheck : defAddConditionChecked,
		uniqueKey : m_tour2ndModuleHotel_returnCount_AddConditionCheckBox(),
		inputName:'m_dom_hotel_option'
	});
	
	//連動処理関連、初期値保持
	//チェックイン
	var domHotelCheInDate_befor = target.find('.m_domHotelCheInDate').val();
	
	//チェックイン項目変更イベント登録（チェックインに連動するチェックアウト）
	target.find('.m_domHotelCheInDateText').on('change', function(e){
		
		var domHotelCheInDate = target.find('.m_domHotelCheInDate').val();
		
		//取得前と取得後に変更がない場合は、イベント終了
		if(domHotelCheInDate_befor !== domHotelCheInDate){
			//チェックアウトの連動
			//取得
			var m_domHotelCheOutDate = target.find('.m_domHotelCheOutDate');
			var m_domHotelCheOutDateText = target.find('.m_domHotelCheOutDateText');
			//計算
			var cheOutDaysFromCheInDate = m_tourModuleDateUtil.prototype.calcRetDaysFromStartDate(defCheckoutOptions);
			var cheOutDate = m_tourModuleCalender_PCLP.SubtractDate(domHotelCheInDate, cheOutDaysFromCheInDate);
			var cheOutDateText = m_tourModuleCalender_PCLP.FormatDate(cheOutDate);
			//更新
			m_domHotelCheOutDate.val(m_tourModuleCalender_PCLP.GetDateText(cheOutDate));
			m_domHotelCheOutDateText.val(cheOutDateText);
			
			//更新前項目情報の更新
			domHotelCheInDate_befor = domHotelCheInDate;
		}else{
			//イベント終了
		}
	});
	
	// 大人人数変更イベント登録
	target.find('.m_dom_hotel_member').on('change', function(e){
		// 大人1人のとき注意文言表示、それ以外は非表示
		var adultValue = $tour_module_jq(this).val();
		if(adultValue === '1'){
			target.find('.m_dispChildCaution').css('display','block');
		}else{
			target.find('.m_dispChildCaution').css('display','none');
		}
		//※子供・幼児の活性・非活性制御はツアー2nd共通処理で実施
	});
	
	
	// 初期化実施
	target.find('.m_dom_hotel_member').trigger('change');
	
	// 検索ボタン押下イベント登録：サブミット実行
	target.find('.btn-search').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			// 子供・幼児 非活性判定
			var isChildDisabled = target.find('.m_dom_hotel_children').is(':disabled');
			// 検索サブミット
			$tour_module_jq(this).domtourCommonModule('submitSearchHotel', {
				targetModule : target,
				isChildDisabled : isChildDisabled
			});
		}
	});
	
	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}
	
	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});
	
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 1);
	m_tourCommonModuleParts_setPartsCount('calendar', 2);
};

//こだわり条件チェックボックスリスト配置数カウント
function m_tour2ndModuleHotel_returnCount_AddConditionCheckBox(){
	
	var returnCount = m_domtour2ndModuleHotel_usePartsCount_AddConditionCheckBox;
	m_domtour2ndModuleHotel_usePartsCount_AddConditionCheckBox++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

//大人配置数カウント
function m_tour2ndModuleHotel_returnCount_AddAdultPulldown(){
	
	var returnCount = m_domtour2ndModuleHotel_usePartsCount_AdultPulldown;
	m_domtour2ndModuleHotel_usePartsCount_AdultPulldown++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

$tour_module_jq.fn.domtour2ndModuleHotel = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleHotel_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleHotel_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleHotel');
	}
};
