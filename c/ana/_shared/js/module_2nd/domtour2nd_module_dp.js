/*=====================================================
* Script Name: domtour2nd_module_dp.js
* Description: 空席照会モジュール 国内ツアー2nd 旅作
* Version: 1.00
* Last Up Date: 2017/10/11
=====================================================*/

var m_domtour2ndModuleDp_siteType = { site : 'domtour2nd', module : 'tabisaku' };

var m_domtour2ndModuleDp_usePartsCount_rentalCarCheckBox = 1;		//レンタカーチェックボックス、配置数カウンタ
var m_domtour2ndModuleDp_usePartsCount_adultPullDown = 1;			//大人プルダウン、配置数カウンタ

//初期値設定
var m_domtour2ndModuleDp_defaults = {
	m_defDepFlightDateDep:'',			//フライト行　搭乗日用
	m_defDepFlightAirportDep:'HND',		//フライト行　出発地用
	m_defDepFlightAirportArr:'',		//フライト行　到着地用
	m_defArrFlightDateDep:'',			//フライト帰り　搭乗日用
	m_defArrFlightAirportDep:'',		//フライト帰り　出発地用
	m_defArrFlightAirportArr:'',		//フライト帰り　到着地用
	m_defNumberOfAdult:'',				//参加人数（大人）用
	m_defNumberOfChild:m_domtourModuleNumberOfPeopleDp_optionParams.initNum,   //参加人数（子供幼児）用
	m_defPlaceOfStayDistrict:'',		//宿泊地用(方面)
	m_defPlaceOfStayRegion:'',			//宿泊地用(地区)
	m_defPlaceOfStayArea:'',			//宿泊地用(地域)
	m_defCheckin:'',					//チェックイン用
	m_defCheckout:'',					//チェックアウト用
	m_defIsWithCar:'',					//レンタカーも合わせて予約チェック用
	m_defTabNoDispCls:''				//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleDp_optionParam = $tour_module_jq.extend(m_domtour2ndModuleDp_optionParam, m_domtour2ndModuleDp_defaults);

/**
 * 国内ツアー旅作2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModuleDp_setParams(options){
	m_domtour2ndModuleDp_optionParam = $tour_module_jq.extend(m_domtour2ndModuleDp_optionParam, options);
};

/**
 * 国内ツアー旅作2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleDp_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアー旅作の各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModuleDp_incorrectValue(options, m_domtour2ndModuleDp_siteType, m_domtour2ndModuleDp_defaults));

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defModulePosition');


	//各項目設定値

	//フライト 行き
	var depFlightText = m_tourModuleCommon_getListValue(m_domtourModuleDp_depFlight, 'Text');
	//搭乗日
	var depFlightDepDateText = m_tourModuleCommon_getListValue(m_domtourModuleDp_depFlightDepDate, 'Text');
	//出発地
	var depFlightDepApoText = m_tourModuleCommon_getListValue(m_domtourModuleDp_depFlightDepApo, 'Text');
	//到着地
	var depFlightArrApoText = m_tourModuleCommon_getListValue(m_domtourModuleDp_depFlightArrApo, 'Text');
	//フライト 帰り
	var arrFlightText = m_tourModuleCommon_getListValue(m_domtourModuleDp_arrFlight, 'Text');
	//搭乗日
	var arrFlightDepDateText = m_tourModuleCommon_getListValue(m_domtourModuleDp_arrFlightDepDate, 'Text');
	//出発地
	var arrFlightDepApoText = m_tourModuleCommon_getListValue(m_domtourModuleDp_arrFlightDepApo, 'Text');
	//到着地
	var arrFlightArrApoText = m_tourModuleCommon_getListValue(m_domtourModuleDp_arrFlightArrApo, 'Text');
	//参加人数
	var numberOfPeopleText = m_tourModuleCommon_getListValue(m_domtourModuleDp_numberOfPeople, 'Text');
	//大人
	var adultText = m_tourModuleCommon_getListValue(m_domtourModuleDp_adult, 'Text');
	//子供・幼児
	var childText = m_tourModuleCommon_getListValue(m_domtourModuleDp_child, 'Text');
	var numberOfPeoplePartsId1 = m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.dp;
	var numberOfPeopleCount1 = m_tourModuleCommon_zeroPadding(m_tourModuleNumberOfPeople_usePartsCount); // 参加人数ID番号設定
	var numberOfPeopleTextId1 = m_tourModuleNumberOfPeople_defaultParams.textId;
	//宿泊
	var stayText = m_tourModuleCommon_getListValue(m_domtourModuleDp_stay, 'Text');
	//宿泊地
	var placeOfStaText = m_tourModuleCommon_getListValue(m_domtourModuleDp_placeOfStay, 'Text');
	//チェックイン・チェックアウト
	var checkText = m_tourModuleCommon_getListValue(m_domtourModuleDp_check, 'Text1');
	var checkinText = m_tourModuleCommon_getListValue(m_domtourModuleDp_check, 'Text2');
	var checkoutText = m_tourModuleCommon_getListValue(m_domtourModuleDp_check, 'Text3');
	//レンタカー
	var carText1 = m_tourModuleCommon_getListValue(m_domtourModuleDp_car, 'Text1');
	var carText2 = m_tourModuleCommon_getListValue(m_domtourModuleDp_car, 'Text2');
	var carURL = m_tourModuleCommon_getListValue(m_domtourModuleDp_car, 'URL');
	var isCarChecked = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defIsWithCar');
	//検索ボタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModuleDp_fromAction, 'URL');
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');

	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defFormMethod');

	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<form class="form-domDp" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<div class="mod-content_wrap">\n';
	html += '<div class="mod-content_cover col4">\n';

	//1カラム目 フライト 行き（搭乗日、出発地、到着地）
	html += '<div class="mod-content_box">\n';
	html += '<p class="mod-title">' + depFlightText + '</p>\n';

	//搭乗日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depFlightDepDateText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depFlightDepDate">\n';
	//搭乗日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//出発地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depFlightDepApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depFlightDepApoArea">\n';
	//出発地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//到着地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depFlightArrApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depFlightDepArrArea">\n';
	//到着地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	html += '</div><!-- / .mod-content_box -->\n';
	//1カラム目 フライト 行き END


	//2カラム目 フライト 帰り（搭乗日、出発地、到着地）
	html += '<div class="mod-content_box mod-shutter_box">\n';
	html += '<p class="mod-title">' + arrFlightText + '</p>\n';

	//搭乗日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + arrFlightDepDateText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_arrFlightDepDate">\n';
	//搭乗日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//出発地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + arrFlightDepApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_arrFlightDepApoArea">\n';
	//出発地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//到着地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + arrFlightArrApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_arrFlightDepArrArea">\n';
	//到着地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 フライト 帰り END


	//3カラム目 参加人数（大人、子供・幼児）
	html += '<div class="mod-content_box mod-shutter_box">\n';
	html += '<p class="mod-title">' + numberOfPeopleText + '</p>\n';

	//大人
	html += '<fieldset class="mod-input_box mod-tabisaku-room">\n';
	html += '<legend><span class="mod-input_head">' + adultText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_adult">\n';
	//大人部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//子供・幼児
	html += '<fieldset class="mod-input_box mod-tabisaku-num">\n';
	html += '<legend><span class="mod-input_head">' + childText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_child">\n';
	html += '<input type="text" readonly name="" id="' + numberOfPeopleTextId1 + numberOfPeopleCount1 + '" value="" class="ico_select mod-input mod-icon js-mod-balloon m_numberOfPeople" data-balloon="'+ numberOfPeoplePartsId1 + numberOfPeopleCount1 +'" data-control-point="'+ dataControlPointValue +'" tabindex="0">\n';
	html += '</div>\n';
	html += '</fieldset>\n';

	html += '</div><!-- / .mod-content_box -->\n';
	//3カラム目 参加人数 END


	//4カラム目 宿泊（宿泊地、チェックイン、チェックアウト）レンタカー 検索ボタン
	html += '<div class="mod-content_box mod-btn_box mod-shutter_box">\n';
	html += '<p class="mod-title">' + stayText + '</p>\n';

	//宿泊地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + placeOfStaText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_placeOfStay">\n';
	//宿泊地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//チェックイン
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + checkText + '<span class="ib">' + checkinText + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_checkin">\n';
	//チェックイン部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//チェックアウト
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + checkText + '<span class="ib">' + checkoutText + '</span></span></legend>\n';
	html += '<div class="mod-input_parts m_checkout">\n';
	//チェックアウト部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';

	//レンタカー
	html += '<fieldset class="mod-input_box rental-car_box">\n';
	html += '<legend><span class="mod-input_head bold">' + carText1 + '</span></legend>\n';
	html += '<div class="mod-input_parts">\n';
	html += '<p class="ico-checkbox vtop inlabel"><input name="rcPlanDispFlg" value="1" id="rental-car'+ m_domtour2ndModuleDp_usePartsCount_rentalCarCheckBox +'" type="checkbox" ' + isCarChecked + '>\n';
	html += '<label tabindex="0" for="rental-car'+ m_domtour2ndModuleDp_usePartsCount_rentalCarCheckBox +'" class=""><span>' + carText2 + '</span></label>\n';
	html += '<a href="' + carURL + '" class="ico_mod_help"><span class="invisible">' + carText2 + '</span></a>\n';
	html += '</p>\n';
	html += '</div>\n';
	html += '</fieldset>\n';


	//特定サイトID
	html += '<input type="hidden" name="ptclSiteId" value="WWYQ6">\n';

	//CID
	html += '<input type="hidden" name="cid" value="">\n';



	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '<div class="btn btn-search" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';

	html += '</div><!-- / .mod-content_box -->\n';
	//4カラム目 END


	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</div"><!-- / .mod-content_wrap -->\n';
	html += '</form>\n';
	html += '\n';

	this.append(html);

	m_domtour2ndModuleDp_setDefVal(this, options);

};

function m_domtour2ndModuleDp_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defModulePosition');
	// 宿泊地(方面、地区、地域)設定内容の補正（どれか一つでも設定されていたらundefinedの項目は空文字を設定する）
	if (options.m_defPlaceOfStayDistrict !== undefined ||
		options.m_defPlaceOfStayRegion !== undefined ||
		options.m_defPlaceOfStayArea !== undefined) {

		if (options.m_defPlaceOfStayDistrict === undefined) {
			options.m_defPlaceOfStayDistrict = '';
		}
		if (options.m_defPlaceOfStayRegion === undefined) {
			options.m_defPlaceOfStayRegion = '';
		}
		if (options.m_defPlaceOfStayArea === undefined) {
			options.m_defPlaceOfStayArea = '';
		}
	}

	////////////////////
	// 各入力項目初期化
	////////////////////

	//パラメータ設定を取得
	//搭乗日（行）
	var defDepFlightDateDep = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defDepFlightDateDep');
	//搭乗日（帰）
	var defArrFlightDateDep = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defArrFlightDateDep');
	//フライト行き出発地
	var defDepFlightAirportDep = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defDepFlightAirportDep');
	//フライト行き到着地
	var defDepFlightAirportArr = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defDepFlightAirportArr');
	//フライト帰り出発地
	var defArrFlightAirportDep = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defArrFlightAirportDep');
	//フライト帰り到着地
	var defArrFlightAirportArr = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defArrFlightAirportArr');
	//大人
	var defNumberOfAdult = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defNumberOfAdult');
	// 子供(参加人数部品)
	var defNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defNumberOfChild');
	var numberOfPeopleCount1 = m_tourModuleCommon_zeroPadding(m_tourModuleNumberOfPeople_usePartsCount);
	// 宿泊地(方面)
	var defPlaceOfStayDistrict = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defPlaceOfStayDistrict');
	// 宿泊地(地区)
	var defPlaceOfStayRegion = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defPlaceOfStayRegion');
	// 宿泊地(地域)
	var defPlaceOfStayArea = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defPlaceOfStayArea');
	//チェックイン
	var defCheckin = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defCheckin');
	if(defDepFlightDateDep !== '' && defCheckin === ''){
		 defCheckin = defDepFlightDateDep;
	}
	//チェックアウト
	 var defCheckout = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defCheckout');
	//再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModuleDp_siteType, options, 'm_defTabNoDispCls');

	//搭乗日（行）
	target.find('.m_depFlightDepDate').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleDp_depFlightDepDate, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defDepFlightDateDep,
		defSelectDay : m_domtourModuleDp_DepDate_defSelectDay,
		dateValueClass : m_domtourModuleDp_DepDate_dateValueClass,
		dateValueName : m_domtourModuleDp_DepDate_dateValueName,
		inputPlaceholder : m_domtourModuleDp_DepDate_inputPlaceholder,
		inputClass : m_domtourModuleDp_DepDate_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModuleDp_DepDate_isRequired,
		isDisabled : m_domtourModuleDp_DepDate_isDisabled,
		isDispAllOfMonth : m_domtourModuleDp_DepDate_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleDp_DepDate_prevNextInterval,
		receiptDisableDays : m_domtourModuleDp_DepDate_receiptDisableDays,
		dispMonths : m_domtourModuleCommon_defDispMonths,
		selectableMonths : m_domtourModuleCommon_defSelectableMonths,
		isDispHoliday : m_domtourModuleDp_DepDate_isDispHoliday,
		boundaryTime : m_domtourModuleDp_DepDate_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});

	//出発地（行）
	target.find('.m_depFlightDepApoArea').tourCommonModule('createAirportInput', {
		parent : this,
		defAirportCode : defDepFlightAirportDep,
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleDp_depApo['Text'],
		areaClass : 'm_hiddenDepFlightDepArea',
		countryClass : 'm_hiddenDepFlightDepCountry',
		apoCodeClass : 'm_hiddenDepFlightDepApo',
		apoCodeName : 'goDeptAirpCd',
		inputPlaceholder : m_domtour2ndModuleDp_depApo['Placeholder'],
		inputClass : 'm_depFlightDepApoText',
		isRequired : false,
		isDisabled : false,
		layoutType : '11',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType : '0'
	});

	//到着地（行）
	target.find('.m_depFlightDepArrArea').tourCommonModule('createAirportInput', {
		parent : this,
		defAirportCode : defDepFlightAirportArr,
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleDp_arrApo['Text'],
		areaClass : 'm_hiddenDepFlightArrArea',
		countryClass : 'm_hiddenDepFlightArrCountry',
		apoCodeClass : 'm_hiddenDepFlightArrApo',
		apoCodeName : 'goArrAirpCd',
		inputPlaceholder : m_domtour2ndModuleDp_arrApo['Placeholder'],
		inputClass : 'm_depFlightArrApoText',
		isRequired : false,
		isDisabled : false,
		layoutType : '12',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 1),
		apoType : '1'
	});

	//搭乗日（帰）
	var arrFlightDepDateOptions ={
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleDp_arrFlightDepDate, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defArrFlightDateDep,
		defSelectDay : m_domtourModuleDp_arrDate_defSelectDay,
		dateValueClass : m_domtourModuleDp_arrDate_dateValueClass,
		dateValueName : m_domtourModuleDp_arrDate_dateValueName,
		inputPlaceholder : m_domtourModuleDp_arrDate_inputPlaceholder,
		inputClass : m_domtourModuleDp_arrDate_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 1),
		isRequired : m_domtourModuleDp_arrDate_isRequired,
		isDisabled : m_domtourModuleDp_arrDate_isDisabled,
		isDispAllOfMonth : m_domtourModuleDp_arrDate_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleDp_arrDate_prevNextInterval,
		receiptDisableDays : m_domtourModuleDp_arrDate_receiptDisableDays,
		dispMonths : m_domtourModuleDp_arrDate_dispMonths,
		selectableMonths : m_domtourModuleDp_arrDate_selectableMonths,
		isDispHoliday : m_domtourModuleDp_arrDate_isDispHoliday,
		boundaryTime : m_domtourModuleDp_arrDate_boundaryTime,
		startDateClass : m_domtourModuleDp_arrDate_startDateClass,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH,
		maxTravelDays : m_domtourModuleDp_arrDate_maxTravelDays,
		returnDaysFromDepartureDate:m_domtourModuleDp_arrDate_returnDaysFromDepartureDate
	};
	target.find('.m_arrFlightDepDate').tourCommonModule('createDateInput', arrFlightDepDateOptions);

	//出発地（帰）
	target.find('.m_arrFlightDepApoArea').tourCommonModule('createAirportInput', {
		parent : this,
		defAirportCode : defArrFlightAirportDep,
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleDp_depApo['Text'],
		areaClass : 'm_hiddenArrFlightDepArea',
		countryClass : 'm_hiddenArrFlightDepCountry',
		apoCodeClass : 'm_hiddenArrFlightDepApo',
		apoCodeName : 'rtnDeptAirpCd',
		inputPlaceholder :m_domtour2ndModuleDp_depApo['Placeholder'],
		inputClass : 'm_arrFlightDepApoText',
		isRequired : false,
		isDisabled : false,
		layoutType : '13',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 2),
		apoType : '0'
	});

	//到着地（帰）
	target.find('.m_arrFlightDepArrArea').tourCommonModule('createAirportInput', {
		parent : this,
		defAirportCode : defArrFlightAirportArr,
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleDp_arrApo['Text'],
		filterApoCodeClass : 'm_hiddenDepFlightDepApo',
		areaClass : 'm_hiddenArrFlightArrArea',
		countryClass : 'm_hiddenArrFlightArrCountry',
		apoCodeClass : 'm_hiddenArrFlightArrApo',
		apoCodeName : 'rtnArrAirpCd',
		inputPlaceholder : m_domtour2ndModuleDp_arrApo['Placeholder'],
		inputClass : 'm_arrFlightArrApoText',
		isRequired : false,
		isDisabled : true,
		layoutType : '14',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 3),
		apoType : '1'
	});

	//大人
	target.find('.m_adult').tourCommonModule('createPeoplesPull', {
		site : m_domtour2ndModuleDp_siteType.site,
		module : m_domtour2ndModuleDp_siteType.module,
		defSelect : defNumberOfAdult,
		inputClass:'',
		inputId:'dom_tabisaku_member' + m_domtour2ndModuleDp_usePartsCount_adultPullDown,
		inputName:'adultCnt'
	});

	//子供・幼児(参加人数部品)
	target.find('.m_child').tourCommonModule('createNumberOfPeople', {
		id: m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.dp,
		className : m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.dp,
		htmlType : m_tourModuleNumberOfPeople_FixedParams.domtour.sec.htmlType,
		moduleNo : numberOfPeopleCount1,
		NPLP : m_domtourModuleNumberOfPeopleDp_NPLP,
		initNum : defNumberOfPeopleSelectInit,
		clearNum : defNumberOfPeopleSelectInit,
		column : m_domtourModuleNumberOfPeopleDp_optionParams.column,
		row : m_domtourModuleNumberOfPeopleDp_optionParams.row,
		dispMatrix : m_domtourModuleNumberOfPeopleDp_optionParams.dispMatrix,
		textBoxTemplate : m_domtourModuleNumberOfPeopleDp_optionParams.textBoxTemplate,
		ctrlDisabled : m_domtourModuleNumberOfPeopleDp_optionParams.ctrlDisabled
	});

	//宿泊地
	target.find('.m_placeOfStay').tourCommonModule('createAirportInput', {
		parent : this,
		defAreaCode : defPlaceOfStayDistrict,                   // 方面
		defCountryCode : defPlaceOfStayRegion,                  // 地区
		defAirportCode : defPlaceOfStayArea,                    // 地域
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModuleDp_stay['Text'],
		areaClass : 'm_hiddenHotelDistrict',                    // 方面
		areaName : 'districtCd',                                // 方面
		countryClass : 'm_hiddenHotelRegion',                   // 地区
		countryName : 'regionCd',                               // 地区
		apoCodeClass : 'm_hiddenHotelArea',                     // 地域
		apoCodeName : 'areaCd',                                 // 地域
		inputPlaceholder : m_domtour2ndModuleDp_stay['Placeholder'],
		inputClass : 'm_domDpPlaceOfStayText',
		isRequired : false,
		isDisabled : false,
		layoutType : '15',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 4),
		apoType : '2'
	});

	//チェックイン
	target.find('.m_checkin').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleDp_check, 'BalloonTitle1'),
		defPartsPosition : dataControlPointValue,
		defDate : defCheckin,
		defSelectDay : m_domtourModuleDp_checkin_defSelectDay,
		dateValueClass : m_domtourModuleDp_checkin_dateValueClass,
		dateValueName : m_domtourModuleDp_checkin_dateValueName,
		inputPlaceholder : m_domtourModuleDp_checkin_inputPlaceholder,
		inputClass : m_domtourModuleDp_checkin_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 2),
		isRequired : m_domtourModuleDp_checkin_isRequired,
		isDisabled : m_domtourModuleDp_checkin_isDisabled,
		isDispAllOfMonth : m_domtourModuleDp_checkin_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleDp_checkin_prevNextInterval,
		receiptDisableDays : m_domtourModuleDp_checkin_receiptDisableDays,
		dispMonths : m_domtourModuleDp_checkin_dispMonths,
		selectableMonths : m_domtourModuleDp_checkin_selectableMonths,
		isDispHoliday : m_domtourModuleDp_checkin_isDispHoliday,
		boundaryTime : m_domtourModuleDp_checkin_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH,
		maxTravelDays : m_domtourModuleDp_checkin_maxTravelDays,
		maxTravelDateClass:m_domtourModuleDp_checkin_maxTravelDateClass,
		minTravelDateClass:m_domtourModuleDp_checkin_minTravelDateClass
	});

	//チェックアウト
	defCheckoutOptions = {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModuleDp_check, 'BalloonTitle2'),
		defPartsPosition : dataControlPointValue,
		defDate : defCheckout,
		defSelectDay : m_domtourModuleDp_checkout_defSelectDay,
		dateValueClass : m_domtourModuleDp_checkout_dateValueClass,
		dateValueName : m_domtourModuleDp_checkout_dateValueName,
		inputPlaceholder : m_domtourModuleDp_checkout_inputPlaceholder,
		inputClass : m_domtourModuleDp_checkout_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 3),
		isRequired : m_domtourModuleDp_checkout_isRequired,
		isDisabled : m_domtourModuleDp_checkout_isDisabled,
		isDispAllOfMonth : m_domtourModuleDp_checkout_isDispAllOfMonth,
		prevNextInterval : m_domtourModuleDp_checkout_prevNextInterval,
		selectableMonths : m_domtourModuleDp_checkout_selectableMonths,
		receiptDisableDays : m_domtourModuleDp_checkout_receiptDisableDays,
		dispMonths : m_domtourModuleDp_checkout_dispMonths,
		isDispHoliday : m_domtourModuleDp_checkout_isDispHoliday,
		boundaryTime : m_domtourModuleDp_checkout_boundaryTime,
		startDateClass : m_domtourModuleDp_checkout_startDateClass,
		returnDaysFromDepartureDate:m_domtourModuleDp_checkout_returnDaysFromDepartureDate,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH,
		maxTravelDays : m_domtourModuleDp_checkout_maxTravelDays,
		maxTravelDateClass:m_domtourModuleDp_checkout_maxTravelDateClass
	};
	target.find('.m_checkout').tourCommonModule('createDateInput', defCheckoutOptions);

	// フライト行き出発地項目変更イベント登録
	target.find('.m_hiddenDepFlightDepApo').on('onChangeAirport', function(e){
		// フライト帰り到着地にフライト行き出発地と同じ空港をセット
		var setApoCode = $tour_module_jq(this).val();
		if(!_.isEmpty(setApoCode)){
			var arrFlightArrApoTextObj = target.find('.m_arrFlightArrApoText');
			var arrFlightArrApoCode = target.find('.m_hiddenArrFlightArrApo').val();
			var multiValueArrApo = arrFlightArrApoTextObj.m_tourModuleAirportList_getMultiAirportValue(arrFlightArrApoCode, '14');
			var multiValueDepApo = arrFlightArrApoTextObj.m_tourModuleAirportList_getMultiAirportValue(setApoCode, '14');
			var depFlightDepApoObj = target.find('.m_depFlightDepApoArea');
			// フライト行き出発地とフライト帰り到着地のマルチ空港区分が同じ場合はフライト帰り等着地を書き換えない
			if(!_.isEmpty(multiValueArrApo) && !_.isEmpty(multiValueDepApo) && (multiValueArrApo === multiValueDepApo) ){
				// 書き換えない
			}else{
				var areaCountry = depFlightDepApoObj.m_tourModuleAirportList_getAreaCountryFromAirport(setApoCode, '14');
				target.find('.m_hiddenArrFlightArrArea').val(areaCountry['area']);
				target.find('.m_hiddenArrFlightArrCountry').val(areaCountry['country']);
				target.find('.m_hiddenArrFlightArrApo').val(setApoCode);
				var selectedName = depFlightDepApoObj.m_tourModuleAirportList_getSelectedName(areaCountry['area'], areaCountry['country'], setApoCode, '14');
				arrFlightArrApoTextObj.val(selectedName);
			}
			// マルチ空港が選択された場合はフライト帰り到着地を活性化、それ以外は非活性化
			if(!_.isEmpty(multiValueDepApo)){
				arrFlightArrApoTextObj.prop('disabled', false);
			}else{
				arrFlightArrApoTextObj.prop('disabled', true);
			}
		}else{
			// フライト帰り到着地はデフォルト非活性
			target.find('.m_arrFlightArrApoText').prop('disabled', true);
		}
	});
	target.find('.m_hiddenDepFlightDepApo').trigger('onChangeAirport'); // 初期化設定

	// フライト行き到着地項目変更イベント登録
	target.find('.m_hiddenDepFlightArrApo').on('onChangeAirport', function(e){
		// フライト行き到着地と同じ空港をセット
		var setApoCode = $tour_module_jq(this).val();
		var depFlightArrApoObj = target.find('.m_depFlightDepArrArea');
		var areaCountry = depFlightArrApoObj.m_tourModuleAirportList_getAreaCountryFromAirport(setApoCode, '13');
		target.find('.m_hiddenArrFlightDepArea').val(areaCountry['area']);
		target.find('.m_hiddenArrFlightDepCountry').val(areaCountry['country']);
		target.find('.m_hiddenArrFlightDepApo').val(setApoCode);
		var selectedName = depFlightArrApoObj.m_tourModuleAirportList_getSelectedName(areaCountry['area'], areaCountry['country'], setApoCode, '13');
		var arrFlightDepApoTextObj = target.find('.m_arrFlightDepApoText');
		arrFlightDepApoTextObj.val(selectedName);

		// フライト行き到着地に紐付く宿泊地情報取得
		var placeOfStay = target.find('.m_placeOfStay');																// 「宿泊地」オブジェクト取得
		var ApoCode_PlaceOfStay = m_tourModuleAirportList_domAirportList1_ApoKeyList[setApoCode]['lodging_Apo'];		// 「到着地」に対応する地域コード取得
		var Country_PlaceOfStay = m_tourModuleAirportList_domAirportList1_ApoKeyList[setApoCode]['lodging_Country'];	// 「到着地」に対応する地区コード取得
		var Area_PlaceOfStay = '';

		// 地域コードが設定されている場合
		if (!_.isEmpty(ApoCode_PlaceOfStay)) {
			var tempAreaCountry = placeOfStay.m_tourModuleAirportList_getAreaCountryFromAirport(ApoCode_PlaceOfStay, '15');	// 地域コードに対応する方面地区情報取得
			Area_PlaceOfStay = tempAreaCountry['area'];			// 方面コード取得
			Country_PlaceOfStay = tempAreaCountry['country'];	// 地域コード取得

		// 地区コードが設定されている場合
		} else if(!_.isEmpty(Country_PlaceOfStay)){
			ApoCode_PlaceOfStay = '';
			Area_PlaceOfStay = placeOfStay.m_tourModuleAirportList_getAreaFromCountry(Country_PlaceOfStay, '15');			// 方面コード取得

		// 設定がない場合
		} else {
			ApoCode_PlaceOfStay = '';
			Country_PlaceOfStay = '';
			Area_PlaceOfStay = '';
		}

		// 宿泊地名取得
		var Name_PlaceOfStay = placeOfStay.m_tourModuleAirportList_getSelectedName(Area_PlaceOfStay, Country_PlaceOfStay, ApoCode_PlaceOfStay, '15');

		// 宿泊地情報を再設定
		target.find('.m_hiddenHotelDistrict').val(Area_PlaceOfStay);	// 方面
		target.find('.m_hiddenHotelRegion').val(Country_PlaceOfStay);	// 地区
		target.find('.m_hiddenHotelArea').val(ApoCode_PlaceOfStay);		// 地域
		target.find('.m_domDpPlaceOfStayText').val(Name_PlaceOfStay);	// 名称
	});

	//連動処理関連、初期値保持
	//搭乗日（行）
	var domDpDepDate_befor = target.find('.m_domDpDepDate').val();
	//搭乗日（帰り）
	var domDpRetDate_befor = target.find('.m_domDpRetDate').val();

	// フライト 行き 搭乗日項目変更イベント登録（搭乗日（行）に連動する搭乗日（帰り）、チェックイン、チェックアウト）
	target.find('.m_domDpDepDateText').on('change', function(e){

		//更新後の搭乗日（行）
		var domDpDepDate = target.find('.m_domDpDepDate').val();

		//更新前と更新後に変更がない場合は、イベント終了
		if(domDpDepDate_befor !== domDpDepDate){

			//搭乗日（帰り）の連動(行き搭乗日の翌日をセット)
			//取得
			var m_domDpRetDate = target.find('.m_domDpRetDate');
			var m_domDpRetDateText = target.find('.m_domDpRetDateText');
			//計算
			var retDaysFromStartDate = m_tourModuleDateUtil.prototype.calcRetDaysFromStartDate(arrFlightDepDateOptions);
			var retDate = m_tourModuleCalender_PCLP.SubtractDate(domDpDepDate, retDaysFromStartDate);
			var retDateText = m_tourModuleCalender_PCLP.FormatDate(retDate);
			//更新
			m_domDpRetDate.val(m_tourModuleCalender_PCLP.GetDateText(retDate));
			m_domDpRetDateText.val(retDateText);

			//チェックインの連動(行き搭乗日と同日をセット)
			//取得
			var m_domDpCheInDate = target.find('.m_domDpCheInDate');
			var m_domDpCheInDateText = target.find('.m_domDpCheInDateText');
			//更新
			m_domDpCheInDate.val(domDpDepDate);
			m_domDpCheInDateText.val(target.find('.m_domDpDepDateText').val());

			//チェックアウトの連動(行き搭乗日の翌日をセット)
			//取得
			var m_domDpCheOutDate = target.find('.m_domDpCheOutDate');
			var m_domDpCheOutDateText = target.find('.m_domDpCheOutDateText');
			//更新
			m_domDpCheOutDate.val(m_tourModuleCalender_PCLP.GetDateText(retDate));
			m_domDpCheOutDateText.val(retDateText);

			//更新前項目情報の更新
			domDpDepDate_befor = domDpDepDate;
			domDpRetDate_befor = target.find('.m_domDpRetDate').val();

		}else{
			//イベント終了
		}
	});

	// フライト 帰り 搭乗日項目変更イベント登録（搭乗日（帰り）に連動するチェックイン、チェックアウト）
	target.find('.m_domDpRetDateText').on('change', function(e){

		//更新後の搭乗日（帰り）
		var domDpRetDate = target.find('.m_domDpRetDate').val();

		//更新前と更新後に変更がない場合は、イベント終了
		if(domDpRetDate_befor !== domDpRetDate){

			//チェックインの連動(制御なし)

			//チェックアウトの連動(帰り搭乗日と同日をセット)
			//取得
			var m_domDpCheOutDate = target.find('.m_domDpCheOutDate');
			var m_domDpCheOutDateText = target.find('.m_domDpCheOutDateText');
			//更新
			m_domDpCheOutDate.val(domDpRetDate);
			m_domDpCheOutDateText.val(target.find('.m_domDpRetDateText').val());

			//更新前項目情報の更新
			domDpRetDate_befor = domDpRetDate;

		}else{
			//イベント終了
		}
	});


	// 検索ボタン押下イベント登録：サブミット実行
	target.find('.btn-search').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			$tour_module_jq(this).domtourCommonModule('submitSearchDp', {
				targetModule : target
			});
		}
	});

	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}

	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});


	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 5);
	m_tourCommonModuleParts_setPartsCount('calendar', 4);
	m_domtour2ndModuleDp_usePartsCount_rentalCarCheckBox++;
	m_domtour2ndModuleDp_usePartsCount_adultPullDown++;

};



$tour_module_jq.fn.domtour2ndModuleDp = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleDp_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleDp_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleDp');
	}
};
