/*=====================================================
* Script Name: tour2nd_module_tab.js
* Description: 空席照会モジュール ツアー2nd 国内/海外選択タブ
* Version: 1.00
* Last Up Date: 2017/9/29
=====================================================*/

var m_tour2ndModuleTab_siteType = { site : 'tour2nd', module : 'tab' };

var m_tour2ndModuleTab_AREA_CODE_JP_tabSelDomInt = '01';

//初期値設定
var m_tour2ndModuleTab_defaults = {
	m_defInitTab:'1',			// 機能タブ '1'(国内ツアー) or '2'(海外ツアー)
};

// defaults値保持のため、shallow copy
var m_tour2ndModuleTab_optionParam = $tour_module_jq.extend(m_tour2ndModuleTab_optionParam, m_tour2ndModuleTab_defaults);

function m_tour2ndModuleTab_setParams(options){
	m_tour2ndModuleTab_optionParam = $tour_module_jq.extend(m_tour2ndModuleTab_optionParam, options);
};

// 指定文字列の配列内存在チェック
function m_tour2ndModuleTab_getStrArrayMatchAll(targetStr, arrayStr){
	if($tour_module_jq.inArray(targetStr, arrayStr) != -1){
		return true;
	}
	return false;
};

/**
 * 国際線2ndモジュールHTML埋め込み処理
 */
function m_tour2ndModuleTab_secondModuleDisplay(options) {

	// テキスト設定
	var tabDomTourText = m_tourModuleCommon_getListValue(m_tour2ndModuleTab_tabNameDomTour, 'Text');
	var tabIntTourText = m_tourModuleCommon_getListValue(m_tour2ndModuleTab_tabNameIntTour, 'Text');

	// 画面レイアウト
	var html = '';
	// タブの制御（「国内旅行」「海外旅行」）
	html += '<ul class="mod-travel_select_list">\n';
	html += '<li><a href="javascript:void(0);" class="js-mod-travel-tab_btn m_domtourTab" data-tab-target="mod_domtour">'+ tabDomTourText +'</a></li>\n';
	html += '<li class="is-current"><a href="javascript:void(0);" class="js-mod-travel-tab_btn m_inttourTab" data-tab-target="mod_inttour">'+ tabIntTourText +'</a></li>\n';
	html += '</ul>\n';

	this.prepend(html);

	var calHtml = '';
	this.after(calHtml);
	m_tour2ndModuleTab_setDefVal(this, options);
};

function m_tour2ndModuleTab_setDefVal(target, options){
	
	options =$tour_module_jq.extend(m_tour2ndModuleTab_optionParam, options);
	
	var defIsDispTabFuncDom = m_tourModuleCommon_getParam(m_tour2ndModuleTab_siteType, options, 'm_defIsDispTabFuncDom');
	var defIsDispTabFuncInt = m_tourModuleCommon_getParam(m_tour2ndModuleTab_siteType, options, 'm_defIsDispTabFuncInt');
	var defDispTabIndex = m_tourModuleCommon_getParam(m_tour2ndModuleTab_siteType, options, 'm_defInitTab');
	
	// 更新時カレントタブ制御(「旅作」「海外パッケージ」モジュールの操作）
	var $box = target;
	var currentTab = undefined;
	if (defDispTabIndex == '1' && defIsDispTabFuncDom) {
		currentTab = target.find('.m_domtourTab');
	} else if (defDispTabIndex == '2' && defIsDispTabFuncInt) {
		currentTab = target.find('.m_inttourTab');
	}
	
	var $targetTab = $box.find('.' + $tour_module_jq(currentTab).attr('data-tab-target'));
	
	var currentParentLi = currentTab.parent('li');
	currentParentLi.siblings().removeClass('is-current');
	currentParentLi.addClass('is-current');
	
	if ($targetTab[0]) {
		$box.find('.js-mod-travel-tab_contents').removeClass('is-active');
		$targetTab.addClass('is-active');
	}

	// 海外旅行、国内旅行選択タブ
	$tour_module_jq(document).on('click', '.js-mod-travel-tab_btn', function (e) {
		e.preventDefault();
		var $box = $tour_module_jq(this).closest('.js-mod-travel-tab');
		var $target = $box.find('.' + $tour_module_jq(this).attr('data-tab-target'));
		var $parentLi = $tour_module_jq(this).parent('li');
		$parentLi.siblings().removeClass('is-current');
		$parentLi.addClass('is-current');

		if ($target[0]) {
			$box.find('.js-mod-travel-tab_contents').removeClass('is-active');
			$target.addClass('is-active');
		}
	});

};

// 外部公開メソッドの定義
$tour_module_jq.fn.tour2ndModuleTab = function(method) {
	var methods = {
		'privateSetParams' : m_tour2ndModuleTab_setParams,
		'privateSecondModuleDisplay' : m_tour2ndModuleTab_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.tour2ndModuleTab');
	}
};
