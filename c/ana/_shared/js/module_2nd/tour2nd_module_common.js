/*=====================================================
* Script Name: tour2nd_module_common.js
* Description: 空席照会モジュール ツアー2nd共通処理
* Version: 1.00
* Last Up Date: 2017/09/29
=====================================================*/

var m_tour2ndModuleCommon_SP_WIDTH = 767;
var m_tour2ndModuleCommon = {};
m_tour2ndModuleCommon.modalBalloon = {};

(function () {
    m_tour2ndModuleCommon.checkWindow = {
    isResize: true,
    spWidth: m_tour2ndModuleCommon_SP_WIDTH,
    width: function () {
      return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    },
    height: function () {
      return window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
    },
    isSp: function () {
      return this.width() <= this.spWidth;
    },
    getTapEventType: function () {
      //touchendに対応してたらtouchend、してなければclick
      var mytap = window.ontouchend===null?"touchend":"click";
      return mytap;
    }
  };
  // click keydown[enter]はクリック判定
  m_tour2ndModuleCommon.clickEvent = function (e) {
    var through = false;
    if (e.type == 'keydown' && (e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
      through = true;
    } else if (e.type == 'touchend') {
      through = true;
    } else if (e.type == 'click') {
      through = true;
    }
    return through;
  };

  m_tour2ndModuleCommon.modalBalloon.balloonClose = function (isFade) {
    if ($tour_module_jq('.modal-balloon.is-active')[0]) {
      var time = (isFade) ? 300 : 0;
      var $balloon = $tour_module_jq('.modal-balloon.is-active');
      $tour_module_jq('body').css({height: '', overflow: ''});
      $tour_module_jq(window).scrollTop($balloon.attr('data-wst'));
      $balloon.removeAttr('data-wst').removeClass('is-active');
      $balloon.fadeOut(time, function () {
        $balloon.css({height: ''});
        $balloon.find('.mod-balloon_cont_cover').css({height: ''});
        $balloon.removeAttr('data-target-input');
        $balloon.removeAttr('data-target-input-year');
        $balloon.removeAttr('data-target-input-yearmonth');
        $balloon.removeAttr('data-target-input-day');
        $tour_module_jq('.is-balloon_open').removeClass('is-balloon_open');
      });

      $tour_module_jq(document).off('.modal-balloon-close');
      $tour_module_jq(window).off('.modal-balloon-resize');
      $tour_module_jq('.mod-balloon-set_box').off('.balloonScroll');
    }
  };

  m_tour2ndModuleCommon.modalBalloon.balloonChain = function (self, $originEl) {
    var isChain = $originEl.attr('data-balloon-chain');
    if (isChain && isChain == 'true') {
      setTimeout(function (){
        var $moduleBox = $originEl.closest('.js-module-wrap');
        var $balloonChainEl = $moduleBox.find('[data-balloon-chain="true"]:visible').not('.disabled');
        var chainLength = $balloonChainEl.length - 1;
        var thisIndex = $balloonChainEl.index($originEl);
        if (chainLength > thisIndex) {
          $balloonChainEl.eq(thisIndex + 1).trigger('click');
        }
      }, 50);
    }
  };

  // 吹き出し外クリックで吹き出し閉じる
  m_tour2ndModuleCommon.modalBalloon.setBalloonOutsideClose = function () {
    $tour_module_jq(document).on(m_tour2ndModuleCommon.checkWindow.getTapEventType() + '.modal-balloon-close', function (e) {
      if (!m_tour2ndModuleCommon.clickEvent(e)) return true;

      var $modBalloonBtn = $tour_module_jq(e.target).closest('.js-mod-balloon');
      var $openBalloon = $tour_module_jq('.modal-balloon:visible');
      if (!$tour_module_jq(e.target).hasClass('calPrev') && !$tour_module_jq(e.target).hasClass('calNext') && !$tour_module_jq(e.target).hasClass('calCurrent')) {
        if (!$modBalloonBtn[0] && $openBalloon[0]) {
          if (!$tour_module_jq.contains($openBalloon[0], e.target)) {
            m_tour2ndModuleCommon.modalBalloon.balloonClose(true);
          }
        }
      }
    });
  }

  // 吹き出し内スクロール、スクロールイベントでボタンの表示チェック
  m_tour2ndModuleCommon.modalBalloon.setBalloonScrollEvent = function () {
    var timer = 0;
    $tour_module_jq('.mod-balloon-set_box').on('scroll.balloonScroll', function (e) {
      var self = this;
      clearTimeout(timer);
      timer = setTimeout(function () {
        var $modalBalloon = $tour_module_jq(self).closest('.modal-balloon');
        var $listWrap = $tour_module_jq(self).closest('.mod-balloon-set_cover');
        var $list = $listWrap.find('.mod-balloon-set_box');
        var listH = $list.height();
        var isSingleScroll = ($listWrap.find('.mod-list-single')[0]);
        
        if (isSingleScroll) {
          if ($listWrap.find('.mod-list-single').height() <= listH + $list.scrollTop()) {
            $listWrap.addClass('is-scroll-disabled');
          } else {
            $listWrap.removeClass('is-scroll-disabled');
          }
        } else {
          var boxHeight = $listWrap.find('.mod-balloon-set_box_inner').outerHeight();
          if (boxHeight <= listH + $list.scrollTop()) {
            $listWrap.addClass('is-scroll-disabled');
          } else {
            $listWrap.removeClass('is-scroll-disabled');
          }
        }

      }, 10);
    });
  }

  // 吹き出し内で項目数が9個以上ある場合、スクロールボタンを表示する
  m_tour2ndModuleCommon.modalBalloon.setAreaSelectScroll = function () {
    var $modalBalloon = $tour_module_jq('.modal-balloon.is-active');
    var $listWrap = $modalBalloon.find('.mod-balloon-set_cover');
    var isSingleScroll = ($listWrap.find('.mod-list-single')[0]);

    if ($modalBalloon.hasClass('mod-scroll_column')) {
      if (isSingleScroll) {
        $listWrap.each(function () {
        	var mHeight = $tour_module_jq(this).height();
    		var cHeight = $tour_module_jq(this).find('.mod-balloon-set_box_inner').outerHeight();
        	if (($tour_module_jq(this).find('li').length - 1) >= 9) {
        		var rmFlag = false;
        		$tour_module_jq(this).addClass('is-scroll');
        		$tour_module_jq(this).find('.mod-balloon-scroll_over').show();
        		var $listCity = $tour_module_jq(this).find('.m_APlist')
        		if($listCity.length >= 2){
        			$listCity.each(function(){
        				if (($tour_module_jq(this).find('li').length - 1) < 9 && $tour_module_jq(this).css('display') != 'none') {
        					// 初期表示のエリア項目数が9未満の場合はis-scrollを削除
        					rmFlag = true;
        				}
        			});
        		}
        		if(rmFlag){
        			$tour_module_jq(this).removeClass('is-scroll');
        			$tour_module_jq(this).find('.mod-balloon-scroll_over').hide();
        		}
        	}
        	// 項目数が9個未満でも縦幅が表示幅を超えていたらスクロールボタン表示
        	if(cHeight - mHeight > 0){
        		if(!$tour_module_jq(this).hasClass('is-scroll')){
        			$tour_module_jq(this).addClass('is-scroll');
            		$tour_module_jq(this).find('.mod-balloon-scroll_over').show();
        		}
    		}else{
    			if($tour_module_jq(this).hasClass('is-scroll')){
    				$tour_module_jq(this).removeClass('is-scroll');
        			$tour_module_jq(this).find('.mod-balloon-scroll_over').hide();
    			}
    		}
        });
      } else {
        var listWrapHeight = $listWrap.height();
        var boxHeight = $modalBalloon.find('.mod-balloon-set_box_inner').outerHeight();
        if (listWrapHeight < boxHeight) {
          $tour_module_jq(this).addClass('is-scroll');
          $tour_module_jq(this).find('.mod-balloon-scroll_over').show();
        }
      }
    }
    m_tour2ndModuleCommon.modalBalloon.setBalloonScrollEvent();
  }

  // 吹き出しの位置調整　吹き出し開いたときのみ動作
  m_tour2ndModuleCommon.modalBalloon.setResizePosition = function () {
    $tour_module_jq(window).on('resize.modal-balloon-resize', function (e) {
      var $btn = $tour_module_jq('.is-balloon_open');
      var $balloon = $tour_module_jq('.modal-balloon.is-active');
      if ($btn[0] && $balloon[0]) {
        var elWidth = $btn.width();
        var elOffsetLeft = $btn.offset().left - $balloon.offset().left;
        var arrowCss = {};
        arrowCss.left = Math.round(elOffsetLeft + elWidth/2);
        $balloon.find('.modal-balloon_arrow').css(arrowCss);
      }
    });
  }

  m_tour2ndModuleCommon.modalBalloon.pcBalloonOpen = function () {
    var $target = $tour_module_jq('#' + $tour_module_jq(this).attr('data-balloon'));
    if ($target[0]) {
      m_tour2ndModuleCommon.modalBalloon.balloonClose(false);

      $tour_module_jq(this).addClass('is-balloon_open');
      $target.css({opacity: 0}).show();
      
      // 吹き出し内で選択した値をセットするinputのid
      // month, dayはカレンダーで使用
      var targetInput = $tour_module_jq(this).attr('data-target-input');
      var targetInputYear = $tour_module_jq(this).attr('data-target-input-year');
      var targetInputYearMonth = $tour_module_jq(this).attr('data-target-input-yearmonth');
      var targetInputDay = $tour_module_jq(this).attr('data-target-input-day');
      
      if (targetInput) {
        $target.attr('data-target-input', targetInput);
      }
      if (targetInputYear) {
        $target.attr('data-target-input-year', targetInputYear);
      }
      if (targetInputYearMonth && targetInputDay) {
        $target.attr('data-target-input-yearmonth', targetInputYearMonth);
        $target.attr('data-target-input-day', targetInputDay);
      }

      var arrowCss = {};
      var innerCss = {};
      var pointFlg;
      var elHeight = $tour_module_jq(this).height();
      var elWidth = $tour_module_jq(this).width();
      var elOffsetTop = $tour_module_jq(this).offset().top;
      var elOffsetLeft = $tour_module_jq(this).offset().left - $target.offset().left;

      var $moduleBox = $tour_module_jq(this).closest('.module-travel_cover');
      var isHorizon = $moduleBox.children('.module-travel_parts').hasClass('horizon');

      if (isHorizon) {
        var $selfContentBox = $tour_module_jq(this).closest('.mod-content_box');
        var $contentBox = $tour_module_jq(this).closest('.mod-content_cover').find('.mod-content_box');
        var contentBoxLength = $contentBox.length - 1;
        var contentBoxIndex = $contentBox.index($selfContentBox);

        if (contentBoxIndex === 0) {
          pointFlg = 'left';
        } else if (contentBoxLength === contentBoxIndex) {
          pointFlg = 'right';
        } else {
          pointFlg = 'center';
        }
      } else {
        pointFlg = $tour_module_jq(this).attr('data-control-point');
      }
      
      if (pointFlg == 'right') {
        innerCss['right'] = 0;
        innerCss['left'] = 'auto';
      } else if (pointFlg == 'left') {
        innerCss['right'] = 'auto';
        innerCss['left'] = 0;
      } else {
        innerCss['left'] = 0;
        innerCss['right'] = 0;
        innerCss['margin'] = '0 auto';
      }

      arrowCss.top = Math.round(elHeight + elOffsetTop - 15);
      arrowCss.left = Math.round(elOffsetLeft + (elWidth / 2));
      innerCss.top = Math.round(elHeight + elOffsetTop - 3);
      $target.find('.modal-balloon_arrow').css(arrowCss);
      $target.find('.modal-balloon_inner').removeAttr('style');
      $target.find('.modal-balloon_inner').css(innerCss);
      
      // クラス名「balloon-set_left, balloon-set_right」などが付与された状態であれば削除
      $target.removeClass(function(index, className) {
        return (className.match(/\bballoon-set_\S+/g) || []).join(' ');
      });
      
      $target.addClass('is-active');
      if (pointFlg) $target.addClass('balloon-set_' + pointFlg);
      $target.animate({opacity: 1}, 300);

      $target.find('[tabindex="0"]').eq(0).focus();

      // スクロールボタン
      m_tour2ndModuleCommon.modalBalloon.setAreaSelectScroll();

      // 矢印位置調整
      m_tour2ndModuleCommon.modalBalloon.setResizePosition();

      // 画面外クリックで閉じる
      m_tour2ndModuleCommon.modalBalloon.setBalloonOutsideClose();
    }
  };

  m_tour2ndModuleCommon.modalBalloon.spBalloonOpen = function () {
    var $target = $tour_module_jq('#' + $tour_module_jq(this).attr('data-balloon'));
    if ($target[0]) {
      var $modalTitle = $target.find('.mod-balloon_title');
      var $modalContent = $target.find('.mod-balloon_cont_cover');

      $target.attr('data-wst', window.pageYOffset);
      $target.css({opacity: 0}).show();
      $tour_module_jq(this).addClass('is-balloon_open');
      $target.addClass('is-active is-sp');
      $target.find('.mod-balloon-set_cover').eq(0).addClass('is-active');

      $target.css({
        height: m_tour2ndModuleCommon.checkWindow.height()
      });
      $modalContent.css({
        height: m_tour2ndModuleCommon.checkWindow.height() - $modalTitle.outerHeight()
      });

      $target.animate({opacity: 1}, 300, function () {
        $tour_module_jq('body').css({
          height: m_tour2ndModuleCommon.checkWindow.height(),
          overflow: 'hidden'
        });
        setTimeout(function() {
          $tour_module_jq(window).scrollTop(0);
        }, 150);
      });
      
      $tour_module_jq(window).on('resize.modal-balloon-resize orientationchange.modal-balloon-resize', function() {
        $tour_module_jq('body').css({
          height: m_tour2ndModuleCommon.checkWindow.height()
        });
        $target.css({
          height: m_tour2ndModuleCommon.checkWindow.height()
        });
        $modalContent.css({
          height: m_tour2ndModuleCommon.checkWindow.height() - $modalTitle.outerHeight()
        });
        setTimeout(function() {
          $tour_module_jq(window).scrollTop(0);
        }, 150);
      });
    }
  }


  $tour_module_jq(document).on('click keydown', '.js-mod-balloon', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();

    $tour_module_jq(this).blur();

    if (m_tour2ndModuleCommon.checkWindow.isSp()) {
      m_tour2ndModuleCommon.modalBalloon.spBalloonOpen.call(this);
    } else {
      m_tour2ndModuleCommon.modalBalloon.pcBalloonOpen.call(this);
    }
  });
//  },false);

  $tour_module_jq(document).on('click keydown', '.js-mod-balloon_close', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();
    m_tour2ndModuleCommon.modalBalloon.balloonClose(true);
  });


  // 吹き出し内スクロールボタン押下で1項目分スクロールする
  $tour_module_jq(document).on('click keydown', '.js-mod-balloon-scroll_over', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;

    e.preventDefault();
    var $box = $tour_module_jq(this).closest('.modal-balloon');
    var $listWrap = $tour_module_jq(this).closest('.mod-balloon-set_cover');
    var $list = $listWrap.find('.mod-balloon-set_box');
    var listH = $list.height();
    var defScrollTop = $list.scrollTop();
    var initH = listH + defScrollTop;
    var targetScrollTop = 0;
    var isScroll = false;
    var isSingleScroll = ($list.find('.mod-list-single')[0]);
    var isScrollDisabled = $listWrap.hasClass('is-scroll-disabled');

    if (isScrollDisabled) return;

    if (isSingleScroll) {
      // singleタイプ
      var scrollList = (function () {
        var arr = [];
        $list.find('.mod-list-single').children('div:visible').find('li').each(function () {
          arr.push($tour_module_jq(this).outerHeight());
        });
        return arr;
      })();

      for (var i = 0; i < scrollList.length; i++) {
        if (i === scrollList.length - 1) {
          break;
        }
        initH = initH - scrollList[i];
        targetScrollTop += scrollList[i];
        if (initH == 0) {
          targetScrollTop = targetScrollTop + scrollList[i+1] - listH;
          break;
        } else if (initH < 0) {
          targetScrollTop = targetScrollTop - listH;
          break;
        }
        isScroll = true;
      }
      if (isScroll) {
        $list.stop(true, true).animate({
          scrollTop: targetScrollTop
        }, 200, 'swing', function () {

        });
      }
    } else {
      // horizonタイプ
      var liHeight = $list.find('.mod-list-horizon:visible').find('li').eq(0).outerHeight(true);
      var boxHeight = 0;
      targetScrollTop = defScrollTop + (liHeight * 2);
      $list.children().each(function () {
        boxHeight += $tour_module_jq(this).outerHeight(true);
      });

      if (boxHeight > listH + defScrollTop) {
        $list.stop(true, true).animate({
          scrollTop: targetScrollTop
        }, 200, 'swing', function () {

        });
      }
    }

  });



  // 吹き出し内　決定ボタン押下
  $tour_module_jq(document).on('click keydown', '.modal-balloon .js-mod-submit_btn', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();
	m_tour2ndModuleCommon.modalBalloon.balloonClose(false);
  });

  // とりあえず閉じる
  $tour_module_jq(document).on('click keydown', '.select-calendar td a', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();
    m_tour2ndModuleCommon.modalBalloon.balloonClose(false);
  });

  // ｢XX月すべて｣ボタン押下時に閉じる
  $tour_module_jq(document).on('click keydown', '.select-calendar p.period a', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();
    m_tour2ndModuleCommon.modalBalloon.balloonClose(false);
  });

  // 吹き出し内の要素を選択したとき
  $tour_module_jq(document).on('click keydown', '.mod-balloon-set_cover li', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    if ($tour_module_jq(this).closest('.item-num-set')[0]) return true;
    if ($tour_module_jq(this).closest('.calendar-nav')[0]) return true;
    e.preventDefault();
    var $balloon = $tour_module_jq(this).closest('.modal-balloon');
    var $balloonWrap = $tour_module_jq(this).closest('.mod-balloon-set_cover');
    var $nextBalloonWrap = $balloonWrap.next('.mod-balloon-set_cover');
    var $balloonOuterWrap = $balloonWrap.closest('.mod-balloon_cont_cover');
    var columnIndex = $balloonOuterWrap.find('.mod-balloon-set_cover').index($balloonWrap);
    var columnLength = $balloonOuterWrap.find('.mod-balloon-set_cover').length;
    var submit = false;

    var dataVal = $tour_module_jq(this).attr('data-val');

    // 次のカラムが存在し、data-valに値がある場合
    if ((columnIndex < columnLength - 1) && dataVal && dataVal !== '') {
      if ($balloon.hasClass('is-sp')) {
        if ($nextBalloonWrap[0]) {
          $nextBalloonWrap.addClass('is-active');
          setTimeout(function () {
            $nextBalloonWrap.addClass('is-anim');
            
            setTimeout(function () {
              $balloonWrap.removeClass('is-active');
            }, 300);
          }, 10);
        }
      } else {
        $tour_module_jq(this).parent('ul').find('li').removeClass('current');
        $tour_module_jq(this).addClass('current');
      }

    } else {
      submit = true;
    }

    if (submit) {
      m_tour2ndModuleCommon.modalBalloon.balloonClose(false);
    }
  });

  // 吹き出し内の戻るボタンを押下したとき
  $tour_module_jq(document).on('click keydown', '.modal-balloon .js-mod-back_btn', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();
    var $balloonWrap = $tour_module_jq(this).closest('.mod-balloon-set_cover');
    var $prevBalloonWrap = $balloonWrap.prev('.mod-balloon-set_cover');
    $prevBalloonWrap.addClass('is-active');
    $balloonWrap.removeClass('is-anim');
    setTimeout(function () {
      $balloonWrap.removeClass('is-active');
    }, 300);
  });

  $tour_module_jq(document).on('click keydown', '.js-mod-err_close', function (e) {
    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
    e.preventDefault();
    var $box = $tour_module_jq(this).closest('.modal-balloon');
    $box.find('.mod-err_wrap').fadeOut(300, function () {
      $tour_module_jq(this).css('display','none');
    });
  });

  // SP時シャッター
  function moduleShutterOpen () {
    var $moduleBox = $tour_module_jq(this).closest('.module-travel_cover');
    $moduleBox.find('.mod-shutter_box').removeClass('mod-shutter_box');
    $moduleBox.find('.js-mod-shutter_btn').hide();
  }
  // シャッター開くボタン
  $tour_module_jq(document).on('click.shutterOpen', '.js-mod-shutter_btn', function (e) {
    moduleShutterOpen.call(this);
  });
  // 触ったらシャッターが開く要素
  $tour_module_jq(document).on('click.shutterOpen', '.module-travel_cover input, .module-travel_cover .mod-type_select a', function (e) {
    moduleShutterOpen.call(this);
  });

  // 国内ツアー　ホテル
  $tour_module_jq(document).on('change', '.m_dom_hotel_member', function (e) {
    var $box = $tour_module_jq(this).closest('.mod-content_wrap');
    if ($tour_module_jq(this).val() >= 2) {
      $box.find('.m_dom_hotel_children').prop('disabled', false);
    } else {
      $box.find('.m_dom_hotel_children').prop('disabled', true);
    }
  });


  // radioボタン　キーボード操作
  $tour_module_jq(document).on('keydown', '.mod-radio-group_wrap', function (e) {
    if (e.keyCode == 37 || e.keyCode == 38) { //VK_LEFT
      e.preventDefault();
      var $elm = $tour_module_jq(this).find('input:checked');
      var $parent = $elm.closest('.ico-radio');
      var $next = $parent.next('.ico-radio');
      if (!$next[0]) {
        $next = $parent.siblings('.ico-radio').eq(0);
      }
      if ($next[0]) {
        $next.find('input').click();
      }
    } else if (e.keyCode == 39 || e.keyCode == 40) { //VK_RIGHT
      e.preventDefault();
      var $elm = $tour_module_jq(this).find('input:checked');
      var $parent = $elm.closest('.ico-radio');
      var $prev = $parent.prev('.ico-radio');
      if (!$prev[0]) {
        var temp = $parent.siblings('.ico-radio');
        $prev = temp.eq(temp.length - 1);
      }
      if ($prev[0]) {
        $prev.find('input').click();
      }
    }
  });

  // radioボタン　キーボード操作
  $tour_module_jq(document).on('keydown', '.ico-checkbox', function (e) {
    if (e.keyCode === 13 || e.keyCode === 32) { //VK_RETURN or VK_SPACE
      e.preventDefault();
      var $elm = $tour_module_jq(this).find('input');
      if ($elm.prop('checked')) {
        $elm.prop('checked', false);
      } else {
        $elm.prop('checked', true);
      }
    }
  });

  // リサイズ PC ⇔ SPで吹き出しクローズ
  (function () {
    var windowType = m_tour2ndModuleCommon.checkWindow.isSp() ? 'sp' : 'pc';
    $tour_module_jq(window).on('resize', function (e) {
      if (m_tour2ndModuleCommon.checkWindow.isSp() && windowType !== 'sp') {
        windowType = 'sp';
        m_tour2ndModuleCommon.modalBalloon.balloonClose();
      } else if (!m_tour2ndModuleCommon.checkWindow.isSp() && windowType !== 'pc') {
        windowType = 'pc';
        m_tour2ndModuleCommon.modalBalloon.balloonClose();
      }
    });
  })();

})();

