/*=====================================================
* Script Name: module_domtour2nd_tab.js
* Description: 空席照会モジュール　海外ツアー・2nd用・国内旅行タブ
* Version: 0.01
* Last Up Date: 2017/7/19
=====================================================*/

var m_domtour2ndModuleTab_siteType = { site : 'domtour2nd', module : 'tab' };

var m_domtour2ndModuleTab_AREA_CODE_JP_tabSelFuncDom = '01';

//初期値設定
var m_domtour2ndModuleTab_defaults = {
	m_defInitTab:'1',				// 機能タブ '1'(国内旅作) or '2'(国内パッケージ) or '3'(国内ホテル) or '4'(国内レンタカー) or '5'(国内観光) or '6'(国内ツアー一括)
//	m_defIsDispDomDp:true,			// 国内旅作タブ表示スイッチ デフォルト値はmainに記述済
//	m_defIsDispDomPkg:true,			// 国内パッケージタブ表示スイッチ デフォルト値はmainに記述済
//	m_defIsDispDomHotel:true,		// 国内ホテルタブ表示スイッチ デフォルト値はmainに記述済
//  m_defIsDispDomHotelBUlk:true,	// 国内ホテル(３社一括)タブ表示スイッチ デフォルト値はmainに記述済
//	m_defIsDispDomCar:true,			// 国内レンタカータブ表示スイッチ デフォルト値はmainに記述済
//	m_defIsDispDomTraffic:true,		// 国内観光タブ表示スイッチ デフォルト値はmainに記述済
//	m_defIsDispDomBulk:true,		// 国内ツアー一括タブ表示スイッチ デフォルト値はmainに記述済
};

// defaults値保持のため、shallow copy
var m_domtour2ndModuleTab_optionParam = $tour_module_jq.extend(m_domtour2ndModuleTab_optionParam, m_domtour2ndModuleTab_defaults);

function m_domtour2ndModuleTab_setParams(options){
	m_domtour2ndModuleTab_optionParam = $tour_module_jq.extend(m_domtour2ndModuleTab_optionParam, options);
};

// 指定文字列の配列内存在チェック
function m_domtour2ndModuleTab_getStrArrayMatchAll(targetStr, arrayStr){
	if($tour_module_jq.inArray(targetStr, arrayStr) != -1){
		return true;
	}
	return false;
};

/**
 * 国際線2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModuleTab_secondModuleDisplay(options) {

	// テキスト設定
	var tabisakuLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModuleTab_tabisakuLabel, 'Text');
	var packageTourLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModuleTab_packageTourLabel, 'Text');
	var hotelLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModuleTab_hotelLabel, 'Text');
	var carLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModuleTab_carLabel, 'Text');
	var trafficLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModuleTab_trafficLabel, 'Text');
	var bulkText = m_tourModuleCommon_getListValue(m_domtour2ndModuleTab_bulkLabel, 'Text');

	// 画面レイアウト
	var html = '';
	html += '<ul class="mod-type_select mod-radio-group">\n';
	html += '<li class="is-current m_tabisakuTab-btnwrap"><a class="js-mod-list-tab_btn m_tabisakuTab" data-tab-target="module-dom-tabisaku" href="javascript:void(0);">' + tabisakuLabelText + '</a></li>\n';
	html += '<li class="m_packTourTab-btnwrap"><a class="js-mod-list-tab_btn m_packTourTab" data-tab-target="module-dom-pgtour" href="javascript:void(0);">' + packageTourLabelText + '</a></li>\n';
	html += '<li class="m_hotelTab-btnwrap"><a class="js-mod-list-tab_btn m_hotelTab" data-tab-target="module-dom-hotel" href="javascript:void(0);">' + hotelLabelText + '</a></li>\n';
	html += '<li class="m_hotelBulkTab-btnwrap"><a class="js-mod-list-tab_btn m_hotelBulkTab" data-tab-target="module-dom-hotel3" href="javascript:void(0);">' + hotelLabelText + '</a></li>\n';
	html += '<li class="m_carTab-btnwrap"><a class="js-mod-list-tab_btn m_carTab" data-tab-target="module-dom-car" href="javascript:void(0);">' + carLabelText + '</a></li>\n';
	html += '<li class="m_trafficTab-btnwrap"><a class="js-mod-list-tab_btn m_trafficTab" data-tab-target="module-dom-traffic" href="javascript:void(0);">' + trafficLabelText + '</a></li>\n';
	html += '<li class="m_bulkTab-btnwrap"><a class="js-mod-list-tab_btn m_bulkTab" data-tab-target="module-dom-bulk" href="javascript:void(0);">' + bulkText + '</a></li>\n';
	html += '</ul>\n';

	this.prepend(html);

	m_domtour2ndModuleTab_setDefVal(this, options);
};

function m_domtour2ndModuleTab_setDefVal(target, options){

	// 国内旅作タブ表示スイッチ
	var defIsDispTabisaku = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomDp');
	// 国内パッケージタブ表示スイッチ
	var defIsDispPackTour = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomPkg');
	// 国内ホテルタブ表示スイッチ
	var defIsDispHotel = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomHotel');
	// 国内ホテル(３社一括)タブ表示スイッチ
	var defIsDispHotelBulk = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomHotelBulk');
	// 国内レンタカータブ表示スイッチ
	var defIsDispCar = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomCar');
	// 国内観光タブ表示スイッチ
	var defIsDispTraffic = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomTraffic');
	// 国内ツアータブ表示スイッチ
	var defIsDispBulk = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defIsDispDomBulk');
	
	// 国内タブ初期選択
	var defDispTabIndex = m_tourModuleCommon_getParam(m_domtour2ndModuleTab_siteType, options, 'm_defInitTab');
	
	// 「旅作」タブ表示/非表示制御
	if (defIsDispTabisaku) {
		target.find('.m_tabisakuTab').css('display', null);
	} else {
		target.find('.m_tabisakuTab').css('display', 'none');
	}

	// 「パッケージツアー」タブ表示/非表示制御
	if (defIsDispPackTour) {
		target.find('.m_packTourTab').css('display', null);
	} else {
		target.find('.m_packTourTab').css('display', 'none');
	}
	
	// 「ホテル」タブ表示/非表示制御
	if (defIsDispHotel) {
		target.find('.m_hotelTab').css('display', null);
	} else {
		target.find('.m_hotelTab').css('display', 'none');
	}
	
	// 「ホテル」(３社一括)タブ表示/非表示制御
	if (defIsDispHotelBulk) {
		target.find('.m_hotelBulkTab').css('display', null);
	} else {
		target.find('.m_hotelBulkTab').css('display', 'none');
	}
	
	// 「レンタカー」タブ表示/非表示制御
	if (defIsDispCar) {
		target.find('.m_carTab').css('display', null);
	} else {
		target.find('.m_carTab').css('display', 'none');
	}
	
	// 「観光」タブ表示/非表示制御
	if (defIsDispTraffic) {
		target.find('.m_trafficTab').css('display', null);
	} else {
		target.find('.m_trafficTab').css('display', 'none');
	}
	
	// 「ツアー」タブ表示/非表示制御
	if (defIsDispBulk) {
		target.find('.m_bulkTab').css('display', null);
	} else {
		target.find('.m_bulkTab').css('display', 'none');
	}
	
	// 更新時カレントタブ制御(「旅作」「海外パッケージ」モジュールの操作）
	var $box = target;
	var currentTab = undefined;
	if (defDispTabIndex == '1' && defIsDispTabisaku) {
		currentTab = target.find('.m_tabisakuTab');
	} else if (defDispTabIndex == '2' && defIsDispPackTour) {
		currentTab = target.find('.m_packTourTab');
	}else if (defDispTabIndex == '3' && defIsDispHotel) {
		currentTab = target.find('.m_hotelTab');
	}else if (defDispTabIndex == '4' && defIsDispHotelBulk) {
		currentTab = target.find('.m_hotelBulkTab');
	}else if (defDispTabIndex == '5' && defIsDispCar) {
		currentTab = target.find('.m_carTab');
	}else if (defDispTabIndex == '6' && defIsDispTraffic) {
		currentTab = target.find('.m_trafficTab');
	}else if (defDispTabIndex == '7' && defIsDispBulk) {
		currentTab = target.find('.m_bulkTab');
	}

	if(!$tour_module_jq(currentTab)[0]){
		// 初期表示指定の要素が非表示の場合、表示される機能の先頭を表示する
		if(defIsDispTabisaku){
			currentTab = target.find('.m_tabisakuTab');
		}else if(defIsDispPackTour){
			currentTab = target.find('.m_packTourTab');
		}else if(defIsDispHotel){
			currentTab = target.find('.m_hotelTab');
		}else if(defIsDispHotelBulk){
			currentTab = target.find('.m_hotelBulkTab');
		}else if(defIsDispCar){
			currentTab = target.find('.m_carTab');
		}else if(defIsDispTraffic){
			currentTab = target.find('.m_trafficTab');
		}else if(defIsDispBulk){
			currentTab = target.find('.m_bulkTab');
		}
	}

	var $targetTab = $box.find('.' + $tour_module_jq(currentTab).attr('data-tab-target'));

	var currentParentLi = currentTab.parent('li');
	currentParentLi.siblings().removeClass('is-current');
	currentParentLi.addClass('is-current');

	if ($targetTab[0]) {
		$box.find('.js-mod-list-tab_contents').removeClass('is-active');
		$targetTab.addClass('is-active');
	}


	// 国内タブ内モジュール(旅作/パッケージ/ホテル/レンタカー/観光/ツアー)選択イベント
	target.find('.js-mod-list-tab_btn').on('click', function (e) {
	    if (!m_tour2ndModuleCommon.clickEvent(e)) return true;
	    e.preventDefault();
		var $box = $tour_module_jq(this).closest('.js-mod-list-tab');
		var $target = $box.find('.' + $tour_module_jq(this).attr('data-tab-target'));
		var $parentLi = $tour_module_jq(this).parent('li');
		$parentLi.siblings().removeClass('is-current');
		$parentLi.addClass('is-current');

		if ($target[0]) {
			$box.find('.js-mod-list-tab_contents').removeClass('is-active');
			$target.addClass('is-active');
		}
	});
};


// 外部公開メソッドの定義
$tour_module_jq.fn.domtour2ndModuleTab = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModuleTab_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModuleTab_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModuleTab');
	}
};
