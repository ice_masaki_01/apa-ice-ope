/*=====================================================
* Script Name: domtour2nd_module_list_JA.js
* Description: 空席照会モジュール 国内ツアー2nd 定義リスト（日本語）
* Version: 1.00
* Last Up Date: 2017/10/10
=====================================================*/

//--------------------------------------------------------------------出発地
var m_domtour2ndModuleDp_depApo = {
	'Text':'出発地',
	'Placeholder':'選択してください',
	'Img':'/module/air-booking/image/common/d_icon_depart.png',
	'Alt':'空港一覧',
	'SuggestImg':'/module/air-booking/image/common/btn_suggest_delete.png',
	'SuggestAlt':'closeAlt',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------到着地
var m_domtour2ndModuleDp_arrApo = {
	'Text':'到着地',
	'Placeholder':'選択してください',
	'Img':'/module/air-booking/image/common/d_icon_arrive.png',
	'Alt':'空港一覧',
	'SuggestImg':'/module/air-booking/image/common/btn_suggest_delete.png',
	'SuggestAlt':'closeAlt',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------宿泊地
var m_domtour2ndModuleDp_stay = {
	'Text':'宿泊地',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------出発地
var m_domtour2ndModulePkg_depApo = {
	'Text':'出発地',
	'Placeholder':'選択してください',
};
//--------------------------------------------------------------------目的地
var m_domtour2ndModulePkg_arrApo = {
	'Text':'目的地',
	'Placeholder':'選択してください',
};

//--------------------------------------------------------------------宿泊地
var m_domtour2ndModuleHotel_stay = {
	'Text':'宿泊地',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------配車場所-空港エリア
var m_domtour2ndModuleCar_firstApoArea = {
	'Text':'配車空港',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------配車場所-市内エリア
var m_domtour2ndModuleCar_firstCityArea = {
	'Text':'市内営業所',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------返車場所-空港営業所
var m_domtour2ndModuleCar_lastApoArea = {
	'Text':'返車空港',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------返車場所-空港営業所・営業所
var m_domtour2ndModuleCar_lastApoOffice = {
	'Text':'空港営業所',
	'Placeholder':'営業所選択',
	'InitMessage':'営業所選択'
};

//--------------------------------------------------------------------返車場所-市内営業所
var m_domtour2ndModuleCar_lastCityArea = {
	'Text':'市内営業所',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------返車場所-市内営業所・営業所
var m_domtour2ndModuleCar_lastCityOffice = {
	'Text':'市内営業所',
	'Placeholder':'営業所選択',
	'InitMessage':'営業所選択'
};

//--------------------------------------------------------------------目的地
var m_domtour2ndModuleTraffic_depApo = {
	'Text':'目的地',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------出発地
var m_domtour2ndModuleBulk_depApo = {
	'Text':'出発地',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------宿泊地
var m_domtour2ndModuleHotelBulk_stay = {
	'Text':'宿泊地',
	'Placeholder':'選択してください',
	'InitMessage':'選択してください'
};

//--------------------------------------------------------------------航空券+宿泊「旅作」
var m_domtour2ndModuleTab_tabisakuLabel = {
	'Text':'航空券+宿泊「旅作」'
};

//--------------------------------------------------------------------パッケージツアー
var m_domtour2ndModuleTab_packageTourLabel = {
	'Text':'パッケージツアー'
};

//--------------------------------------------------------------------ホテル
var m_domtour2ndModuleTab_hotelLabel = {
	'Text':'ホテル'
};

//--------------------------------------------------------------------レンタカー
var m_domtour2ndModuleTab_carLabel = {
	'Text':'レンタカー'
};

//--------------------------------------------------------------------観光・体験・現地交通
var m_domtour2ndModuleTab_trafficLabel = {
	'Text':'観光・体験・現地交通'
};

//--------------------------------------------------------------------ツアー
var m_domtour2ndModuleTab_bulkLabel = {
	'Text':'国内ツアー比較検索'
};

//--------------------------------------------------------------------「パッケージ」 行き先から探す
var m_domtour2ndModulePkg_tourConditionLabel = {
	'Text':'行き先から探す'
};

//--------------------------------------------------------------------「パッケージ」  商品コードから探す
var m_domtour2ndModulePkg_courseCodeConditionLabel = {
	'Text':'商品コードから探す'
};

//--------------------------------------------------------------------「パッケージ」  コースコード
var m_domtour2ndModulePkg_courseCode = {
	'Text1':'コース',
	'Text2':'コード',
	'placeholder':'（例：A001A11A21）'
};

//--------------------------------------------------------------------「パッケージ」  商品コードの確認方法
var m_domtour2ndModulePkg_courseCodeInfo = {
		'Text':'商品コードの確認方法',
		'Img':'/common-layout/images/icon_blank_BLU.png',
		'URL':'/dummy.html'
};

//--------------------------------------------------------------------「ツアー一括」 出発日
var m_domtour2ndModulebulk_depDate = {
	'Text':'出発日',
	'BalloonTitle':'出発日'
};

//--------------------------------------------------------------------「ツアー一括」 出発地
var m_domtour2ndModulebulk_depApo = {
	'Text':'出発地'
};

//--------------------------------------------------------------------「ツアー一括」 旅行日数
var m_domtour2ndModulebulk_travelOfDays = {
	'Text':'旅行日数'
};

//--------------------------------------------------------------------「ツアー一括」 人数
var m_domtour2ndModulebulk_numberOfPeople = {
	'Text':'人数'
};

//--------------------------------------------------------------------国内ホテル三社一括検索
var m_domtour2ndModuleHotel_infoText = {
	'Text':'@ホテル、楽天トラベル、じゃらんnetの宿泊プランを一括比較検索いただけます。',
};

//--------------------------------------------------------------------再検索キー値設定
var m_domtour2ndModuleCommon_setDefValue = {
		domtourDp : {	// 国内旅作
			'goDeptDt':'m_defDepFlightDateDep',				//フライト行　搭乗日用
			'goDeptAirpCd':'m_defDepFlightAirportDep',		//フライト行　出発地用
			'goArrAirpCd':'m_defDepFlightAirportArr',		//フライト行　到着地用
			'rtnDeptDt':'m_defArrFlightDateDep',			//フライト帰り　搭乗日用
			'rtnDeptAirpCd':'m_defArrFlightAirportDep',		//フライト帰り　出発地用
			'rtnArrAirpCd':'m_defArrFlightAirportArr',		//フライト帰り　到着地用
			'adultCnt':'m_defNumberOfAdult',				//参加人数（大人）用
			// 参加人数（子供・幼児）は外部公開メソッドsetResearchValueで実施
			'districtCd':'m_defPlaceOfStayDistrict',		//宿泊地用(方面)
			'regionCd':'m_defPlaceOfStayRegion',			//宿泊地用(地区)
			'areaCd':'m_defPlaceOfStayArea',				//宿泊地用(地域)
			'checkInDt':'m_defCheckin',						//チェックイン用
			'checkOutDt':'m_defCheckout',					//チェックアウト用
			'rcPlanDispFlg':'m_defIsWithCar',				//レンタカーも合わせて予約チェック用]
			'KartKeep':'m_defKartKeep'						//再検索用パラメータ
		},
		
		domtourPkg : {	// 国内パッケージ
			'moduleMode':'m_defModuleMode',					//新旧モジュール切り替えラジオ
			'deptDt':'m_defDateDep',						//出発日
			'deptAreaCd':'m_defAirportDep',					//出発地
			'districtCd':'m_defAreaArr',					//目的地
			'travelDaysCnt':'m_defNumberOfDays',			//旅行日数
			'adultCnt':'m_defNumberOfAdult',				//大人
			// 参加人数（子供・幼児）は外部公開メソッドsetResearchValueで実施
			'cmdtCd':'m_defCourseCode',						//コースコード
			'KartKeep':'m_defKartKeep'						//再検索用パラメータ
		},
		
		domtourHotel : {	// 国内ホテル
			'checkInDt':'m_defCheckin',						//チェックイン用
			'checkOutDt':'m_defCheckout',					//チェックアウト用
			'districtCd':'m_defPlaceOfStayDistrict',		//宿泊地用(方面)
			'regionCd':'m_defPlaceOfStayRegion',			//宿泊地用(地区)
			'areaCd':'m_defPlaceOfStayArea',				//宿泊地用(地域)
			'adultCnt':'m_defNumberOfAdult',				//大人用
			// 参加人数（子供・幼児）は外部公開メソッドsetResearchValueで実施
			'criteriaCondId':'m_defAddConditionChecked',	//こだわり条件用
			'KartKeep':'m_defKartKeep'						//再検索用パラメータ
		},
		
		domtourHotelBulk : {	// 国内ホテル(三社一括)
			'useDt':'m_defCheckin',							//ご利用日用
			'stayDaysCnt':'m_defStayDays',					//泊数用
			'districtCd':'m_defPlaceOfStayDistrict',		//宿泊地用(方面)
			'regionCd':'m_defPlaceOfStayRegion',			//宿泊地用(地区)
			'areaCd':'m_defPlaceOfStayArea',				//宿泊地用(地域)
			'usePersonCnt':'m_defNumberOfPeople',			//大人用
			'criteriaCondId':'m_defAddConditionChecked',	//こだわり条件用
			'KartKeep':'m_defKartKeep'						//再検索用パラメータ
		},
		
		domtourCar : {	// 国内レンタカー
			'allocDt'                   :'m_defDateFirstCar',               //配車日
			'allocTm'                   :'m_defTimeFirstCar',               //配車時間
			'brbkDt'                    :'m_defDateLastCar',                //返車日
			'brbkTm'                    :'m_defTimeLastCar',                //返車時間
			'allocPlaceCls'             :'m_defRadioFirstCar',              //配車場所(ラジオ)
			'allocAirpCd'               :'m_defFirstApoAreaApo',            //配車場所（空港エリア）
			'allocDistrictCd'           :'m_defFirstCityAreaDistrict',      //配車場所（市内エリア）
			'allocRegionCd'             :'m_defFirstCityAreaRegion',        //配車場所（市内エリア）
			'allocAreaCd'               :'m_defFirstCityAreaArea',          //配車場所（市内エリア）
			'brbkPlaceCls'              :'m_defRadioLastCar',               //返車場所（ラジオ）
			'brbkAirpCd'                :'m_defLastApoAreaApo',             //返車場所（空港営業所-空港選択）
			'brbkAirpOfficeCoptCd'      :'m_defLastApoOfficeApoCoptCd',     //返車場所（空港営業所-営業所選択）
			'brbkAirpOfficeFaclCd'      :'m_defLastApoOfficeApoFaclCd',     //返車場所（空港営業所-営業所選択）
			'brbkAirpOfficeAbdAreaCd'   :'m_defLastApoOfficeApoAbdAreaCd',  //返車場所（空港営業所-営業所選択）
			'brbkAirpOfficeNo'          :'m_defLastApoOfficeApoNo',         //返車場所（空港営業所-営業所選択）
			'brbkDistrictCd'            :'m_defLastCityAreaDistrict',       //返車場所（市内営業所-エリア選択）
			'brbkRegionCd'              :'m_defLastCityAreaRegion',         //返車場所（市内営業所-エリア選択）
			'brbkAreaCd'                :'m_defLastCityAreaArea',           //返車場所（市内営業所-エリア選択）
			'brbkOfficeCoptCd'          :'m_defLastCityOfficeApoCoptCd',    //返車場所（市内営業所-営業所選択）
			'brbkOfficeFaclCd'          :'m_defLastCityOfficeApoFaclCd',    //返車場所（市内営業所-営業所選択）
			'brbkOfficeAbdAreaCd'       :'m_defLastCityOfficeApoAbdAreaCd', //返車場所（市内営業所-営業所選択）
			'brbkOfficeNo'              :'m_defLastCityOfficeApoNo',        //返車場所（市内営業所-営業所選択）
			'carTypeClassId'            :'m_defCarClassChecked',            //車両クラス
			'KartKeep'                  :'m_defKartKeep'                    //再検索用パラメータ
		},
		
		domtourTraffic : {	// 国内観光
			'useDt':'m_defDateTraffic',					//利用日
			'districtCd':'m_defTrafficDistrict',		//目的地(方面)
			'regionCd':'m_defTrafficRegion',			//目的地(地区)
			'optCls':'m_defPurposeTraffic',				//目的
			'KartKeep':'m_defKartKeep'					//再検索用パラメータ
		},
		
		domtourBulk : {	// 国内ツアー一括
			'deptDt':'m_defDateDep',					//出発日用
			'deptAreaCd':'m_defDepArea',				//出発地
			'stayDaysCnt':'m_defNumberOfDays',			//旅行日数用
			'usePersonCnt':'m_defNumberOfPeople',		//人数用
			'KartKeep':'m_defKartKeep'					//再検索用パラメータ
		}
};

