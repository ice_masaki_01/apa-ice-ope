/*=====================================================
* Script Name: tour_module_numberOfPeopleParts.js
* Description: 空席照会モジュール 参加人数 共通処理
* Version: 1.1
* Last Up Date: 2017/10/25
=====================================================*/

// コンフリクト対応(海外ツアー2nd向け)
if(typeof($tour_module_jq) == 'undefined'){
	$tour_module_jq= $.noConflict(true);
}

// *** Browser
var useragent =navigator.userAgent.toLowerCase();
var appVersion = parseInt(navigator.appVersion);

// * IE
var isIe = ((useragent.indexOf('msie') != -1) && (useragent.indexOf('opera') == -1));
var isIe5down = false;
if (isIe) {
	var isIe3to5 = ((appVersion < 4) || ((appVersion == 4) && ((useragent.indexOf('msie 4')!=-1) || (useragent.indexOf('msie 5.0')!=-1))));
	isIe5down =(isIe && isIe3to5);
}

// * NN & FF
var isNn  = ((useragent.indexOf('mozilla')!=-1) && (useragent.indexOf('spoofer')==-1) && (useragent.indexOf('compatible') == -1) && (useragent.indexOf('opera')==-1));
var isNn6down = false;
if (isNn) {
	var isNn2 = (appVersion == 2);
	var isNn3 = (appVersion == 3);
	var isNn4 = (appVersion == 4);
	var isNn6 = (appVersion == 5) && (useragent.indexOf('netscape6') != -1);
	isNn6down = (isNn2 || isNn3 || isNn4 || isNn6);
}

// * OPERA
var isOpera = (useragent.indexOf('opera') != -1);
var isOpera7down = false;
if (isOpera) {
	var isOpera2 = (useragent.indexOf('opera 2') != -1 || useragent.indexOf('opera/2') != -1);
	var isOpera3 = (useragent.indexOf('opera 3') != -1 || useragent.indexOf('opera/3') != -1);
	var isOpera4 = (useragent.indexOf('opera 4') != -1 || useragent.indexOf('opera/4') != -1);
	var isOpera5 = (useragent.indexOf('opera 5') != -1 || useragent.indexOf('opera/5') != -1);
	var isOpera6 = (useragent.indexOf('opera 6') != -1 || useragent.indexOf('opera/6') != -1);
	var isOpera7 = (useragent.indexOf('opera 7') != -1 || useragent.indexOf('opera/7') != -1);
	isOpera7down = (isOpera2 || isOpera3 || isOpera4 || isOpera5 || isOpera6 || isOpera7);
}

// * SAFARI
var isSafari = (useragent.indexOf('safari') != -1);
var isSafari13down = false;
if (isSafari) {
	var tmpAgt = useragent.substring(useragent.indexOf('applewebkit/') + 12);
	var version = tmpAgt.substring(0, tmpAgt.indexOf(' '));
	if (version.indexOf('.') != -1) {
		version = version.substring(0, version.indexOf('.'));
	}
	version -= 0;
	if (version < 312) {
		isSafari13down = true;
	}
}

var is_untarget_brow = (isIe5down || isNn6down || isOpera7down || isSafari13down);

/*******************************************************************************
 * <p>参加人数入力部品 共通メソッド定義<br>
********************************************************************************/
m_tourModuleNumberOfPeopleCommonFunc = new Object();
m_tourModuleNumberOfPeopleCommonFunc = {
	/**
	 * <p>bindオブジェクトに対し、funcメソッドを定義する。</p>
	 * @param {Object} bind
	 * @param {Object} func
	 */
	applyFunc : function(bind, func) {
		return function() {
			return func.apply(bind, arguments);
		};
	},
	addMultiEventListener : function(target, self, type, func) {
		if(target instanceof $tour_module_jq){
			target.on(type, this.applyFunc(self, func));
		} else if (target.addEventListener) {
			target.addEventListener(type, this.applyFunc(self, func), false);
		} else if (target.attachEvent && func.apply) {
			target.attachEvent('on' + type, this.applyFunc(self, func));
		} else {
			target['on' + type] = func;
		}
	},
	cancelEvent : function(evtObj) {
		if (window.attachEvent) {
			window.event.cancelBubble = true;
		} else if (window.addEventListener) {
			evtObj.stopPropagation();
		} else {
			window.event.cancelBubble = true;
		}
	}
};

/*******************************************************************************
 * <p>参加人数入力部品の初期設定を行います。<br>
 * @param {String} locale
********************************************************************************/
m_tourModuleNumberOfPeopleProperties = function(args) {

	var defaults = {
		locale : 'J'
	};

	$tour_module_jq.extend(true,this,defaults);
	$tour_module_jq.extend(true,this,args);

	return this;
}

/*************************************************************
 * <p>表示する参加人数選択ダイアログのコントロールクラスです。</P>
 *
 *************************************************************/
m_tourModuleNumberOfPeopleContoroller = function() {

	if (is_untarget_brow) {
		return false;
	}

	this.displayingNumberOfPeopleName = null;
	this.nops = new Array();
	for (var i = 0, len = arguments.length; i < len; i++) {
		this.nops.push(arguments[i]);
		this.nops[i].m_tourModuleNumberOfPeopleContoroller = this;
	}

	var _this = this;

	m_tourModuleNumberOfPeopleCommonFunc.addMultiEventListener(document, this, 'click',
			function() {
				_this.close();
			});
	return this;
};

/**
 * <p>参加人数選択ダイアログの表示を行います。<br>
 * 内部のhidden項目に設定された参加人数を基にダイアログの表示を行います。<br>
 * hidden項目に値がすでに設定されている場合（入力済みの場合）は、その値を、<br>
 * hidden項目に値が一度も設定されていない場合は初期値に設定されているを表示します。</p>
 * @param {NumberOfPeople} nop
 */
m_tourModuleNumberOfPeopleContoroller.prototype.dispNumberOfPeople = function(nop) {

	// close the displaying numberOfPeople
	if (this.isDisplayingNumberOfPeople(nop)) {
		// update
		this.displayingNumberOfPeopleName = null;
		return true;
	}

	// select html type
	var htmlType = nop.htmlType;

	// describe NumberOfPeople
	nop.describeNumberOfPeople(htmlType);

	// update
	this.displayingNumberOfPeopleName = nop.name;

	return true;
};

/**
 * <p>参加人数選択ダイアログを生成後自部品内に追加します。</p>
 * @param {NumberOfPeople} nop
 */
m_tourModuleNumberOfPeopleContoroller.prototype.addNumberOfPeople = function(nop) {
	if (!this.nops) {
		this.nops = new array();
	}
	this.nops.push(nop);
	return true;
};

/**
 * <p>参加人数選択ダイアログを閉じます。</p>
 */
m_tourModuleNumberOfPeopleContoroller.prototype.close = function() {
	// update
	this.displayingNumberOfPeopleName = null;

	return true;
};

/**
 * <p>表示されているダイアログであるかどうかを判定します。</p>
 * @param {NumberOfPeople} nop
 * @return {Boolean} true:表示中のダイアログ false:その他
 */
m_tourModuleNumberOfPeopleContoroller.prototype.isDisplayingNumberOfPeople = function(nop) {
	return this.displayingNumberOfPeopleName == nop.name;
};

/*************************************************************
 * <p>参加人数入力部品クラス用言語設定</p>
 *
 * @param	{String}		SubmitButtonFormat	決定ボタンの表記を設定した文字列
 *************************************************************/
m_tourModuleNumberOfPeopleLanguageProperties = function(args) {
	var defaults = {
		SubmitButtonFormat : 'Submit'
	};

	$tour_module_jq.extend(true,this,defaults);
	$tour_module_jq.extend(true,this,args);

	return this;
}

/******************************************************************************************************************
 * <p>参加人数入力部品クラス</p>
 *
 * @param {String}   NPLP									言語設定
 * @param {String}   htmlType								HTMLタイプ
 * @param {String}   numberOfPeopleCount					モジュール配置番号
 * @param {String}   id										参加人数入力部品ID
 * @param {String}   className								参加人数入力部品class属性名
 * @param {String}   textId									呼び元入力項目id属性名
 * @param {String}   name									モジュール名
 * @param {Object}   hidden 								吹出設定値出力先hidden項目
 * @param {Object}   column									吹き出し選択項目名(列)
 * @param {Object}   row 									吹き出し選択項目名(行)
 * @param {Array}    dispMatrix								吹き出し選択項目表示/非表示状態保持
 * @param {Array}    dispMatrixFixed						吹き出し選択項目表示/非表示初期設定値
 * @param {Object}   setDipsRows							吹き出し選択項目表示行指定用DOMエレメント
 * @param {String}   changeDipsRowsEventName				吹き出し選択項目表示行指定用DOMエレメントイベント名
 * @param {String}   notChangeTextValue						吹き出し選択項目表示行指定用DOMエレメントイベント対象外値
 * @param {function} checkValueFunc							吹き出し選択項目変更時各値チェック関数
 * @param {function} ctrlDisabled							吹き出し選択項目変更時各選択項目活性・非活性化関数
 * @param {Object}   m_tourModuleNumberOfPeopleContoroller	部品内コントロールクラス
 ******************************************************************************************************************/
m_tourModuleNumberOfPeople = function(args) {

	var defaults = {
		NPLP : undefined,
		htmlType : undefined,
		numberOfPeopleCount : undefined,
		id : undefined,
		className : undefined,
		textId : undefined,
		name : undefined,
		hidden : undefined,
		column : undefined,
		row : undefined,
		dispMatrix : undefined,
		dispMatrixFixed : undefined,
		setDipsRows : undefined,
		changeDipsRowsEventName : undefined,
		notChangeTextValue : undefined,
		checkValueFunc :  undefined,
		ctrlDisabled : undefined,
		m_tourModuleNumberOfPeopleContoroller : undefined
	};

	if (is_untarget_brow) {
		return false;
	}

	$tour_module_jq.extend(true,this,defaults)
	$tour_module_jq.extend(true,this,args);

	this.baseDate = null;
	m_tourModuleNumberOfPeople.nops[this.name] = this;
	var _this = this;

	// 吹き出し出力イベント登録
	m_tourModuleNumberOfPeopleCommonFunc.addMultiEventListener(
		document.getElementById(this.textId), this, 'click', function(event) {
			_this.chgDsc(); 
		}
	);
	m_tourModuleNumberOfPeopleCommonFunc.addMultiEventListener(
		document.getElementById(this.textId), this, 'keydown', function(event) {
			if(event.keyCode === 13){ // エンターキーのみ実行
				_this.chgDsc();
			}
		}
	);
	// 行表示・非表示制御イベント登録
	if(this.setDipsRows){
		m_tourModuleNumberOfPeopleCommonFunc.addMultiEventListener(
				this.setDipsRows, this, this.changeDipsRowsEventName, function(event) {
					this.m_tourModuleNumberOfPeopleContoroller.chageDispRows(this); });
	}
	this.m_tourModuleNumberOfPeopleContoroller.addNumberOfPeople(this);
	return this;
};

/**
 * <p>部品内部で使用している参加人数選択ダイアログインスタンスを保持しておく。</p>
 */
m_tourModuleNumberOfPeople.nops = new Array();

/**
 * <p>参加人数選択ダイアログ表示の通知を受け取る</p>
 */
m_tourModuleNumberOfPeople.prototype.chgDsc = function() {
	// 行の表示非表示制御
	if(this.setDipsRows){
		this.m_tourModuleNumberOfPeopleContoroller.setDispRows(this.setDipsRows, this.dispMatrix, this.dispMatrixFixed);
	}
	// 吹き出しHTML出力
	this.m_tourModuleNumberOfPeopleContoroller.dispNumberOfPeople(this);
	return true;
};

/**
 * <p>行の表示・非表示設定を行う</p>
 * @param {Object} maxRow  最大表示数選択オブジェクト
 * @param {Array} dispMatrix  現在の列×行表示項目値保持配列
 * @param {Array} dispMatrixFinal 列×行表示可能項目配列
 */
m_tourModuleNumberOfPeopleContoroller.prototype.setDispRows = function(obj, dispMatrix, dispMatrixFinal){
	var updateDispMaxRow = parseInt($tour_module_jq(obj).val());
	var dispArray = dispMatrix;
	var maxColumn = dispArray[0].length;
	for(var ri = 0; ri < dispArray.length; ri++){
		for(var ci = 0; ci < maxColumn; ci++){
			if((ri + 1) <= updateDispMaxRow && dispMatrixFinal[ri][ci]){
				dispArray[ri][ci] = true;
			}else{
				dispArray[ri][ci] = false;
			}
		}
	}
}

/**
* <p>行表示数選択時のイベント処理</p>
* @param {Object} nop  部品オブジェクト
*/
m_tourModuleNumberOfPeopleContoroller.prototype.chageDispRows = function(nop){
	// 参加人数選択が初期値の場合は参加人数選択テキスト更新なし
	this.setDispRows(nop.setDipsRows, nop.dispMatrix, nop.dispMatrixFixed);
	var isChangeDispRows = true;
	var isChangeText = true;
	var textValue = '';
	if(nop.notChangeTextValue !== undefined){
		if((nop.htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType)
			|| (nop.htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType)){
			textValue = $tour_module_jq('#' + nop.textId + ' span').text();
		}else{
			textValue = $tour_module_jq('#' + nop.textId).val();
		}
		if(textValue == nop.notChangeTextValue){
			isChangeText = false;
		}
	}
	
	// hidden更新
	nop.chgSet({
		isChangeText : isChangeText,
		isChangeDispRows : isChangeDispRows
	});
}
/**
 * <p>参加人数選択ダイアログ<br>
 * 通知を受け取る</p>
* @param {boolean} isNotFocus フォーカス抑制フラグ
 */
m_tourModuleNumberOfPeople.prototype.chgClose = function(isNotFocus) {
	this.m_tourModuleNumberOfPeopleContoroller.close();
	if(!isNotFocus){
		$tour_module_jq('#' + this.textId).focus();
		$tour_module_jq('#' + this.textId).blur();
	}

	return true;
};

/**
 * <p>参加人数選択ダイアログにて決定ボタンが押下された通知を受け取る</p>
 */
m_tourModuleNumberOfPeople.prototype.chgSet = function(options) {
	var dispArray = this.dispMatrix;
	var rowArray = this.row;
	var columnArray = this.column;
	var no = this.numberOfPeopleCount;
	var isChangeText = options.isChangeText;
	var isChangeDispRows = options.isChangeDispRows;
	var htmlType = this.htmlType;
	
	// 部屋数は行数とイコール
	var rowMax = rowArray.length;
	var colMax = columnArray.length;

	// 選択された各参加人数設定値を取得
	var v_array = new Array(rowMax);
	for(var ri = 0; ri < rowMax; ri++){
		v_array[ri] = new Array(colMax);
		var sumRow = 0;
		for(var ci = 0; ci < colMax; ci++){
			// 表示項目は値取得
			if(dispArray[ri][ci] === true){
				var selectId = columnArray[ci].name + rowArray[ri].name + no + 'Select';
				if((htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType)
					|| (htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType)){
					v_array[ri][ci] = parseInt($tour_module_jq('#'+ selectId).find('li.mod-current').data('val'));
				}else{
					v_array[ri][ci] = parseInt($tour_module_jq('#'+ selectId).val());
				}
				// 入力項目から取得失敗時または部品非表示時は初期値を設定
				if(isNaN(v_array[ri][ci])){
					v_array[ri][ci] = this.clearValue[ri][ci];
				}
				// 合計対象列の場合は合計する
				if(columnArray[ci].sum){
					sumRow += v_array[ri][ci];
				}
			}else if(columnArray[ci].sumColumn){
				// 合計を設定
				v_array[ri][ci] = sumRow;
			}else{
				// 非表示項目は0を設定
				v_array[ri][ci] = 0;
			}
		}
	}

	// 選択された各参加人数を保持するhidden項目を取得
	var h_array = new Array(rowMax);
	for(var i = 0; i < rowMax; i++){
		h_array[i] = this.hidden[i];
	}

	// 選択された各参加人数をhidden項目に反映
	for(var ri = 0; ri < rowMax; ri++){
		h_array[ri].each(function() {
			for(var ci = 0; ci < colMax; ci++){
				if($tour_module_jq(this).attr('name') == columnArray[ci].name + rowArray[ri].name){
					$tour_module_jq(this).val(v_array[ri][ci]);
					break;
				}
			}
		});
	}

	// 参加人数選択ダイアログを閉じる
	this.chgClose(isChangeDispRows);

	// hidden項目のchangeイベントを発火
	// 表示更新対象外値がセットされている場合は発火しない
	if(isChangeText){
		h_array[0].eq(0).trigger('change');
	}

	return true;
};

/**
 * <p>ポップアップ参加人数の中身を作成します。<br>
 * 与えられたbaseDateの年月を基準にカレンダーを作成します。</p>
 * @param {String} type  中身のHTMLパターンを選択
 */
m_tourModuleNumberOfPeople.prototype.describeNumberOfPeople = function(type) {

	// 列行作成初期設定
	var columnMax = this.column.length;
	var rowMax = this.row.length;

	var htmlArray = new Array();
	var htmlArrayIndex = 0;

	// 参加人数HTML
	var targetElement = document.getElementById(this.id);
	
	if(type === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType){
		// 国内ツアーTop
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '<div id="'+ this.id +'" class="modal-balloon '+ this.className +' select-num w780 mod-column">\n';
		}
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_cover">\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_inner">\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_contents">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_title">\n';
		htmlArray[htmlArrayIndex++] = '    <h2 class="mod-balloon_ttl"><span class="small">'+ this.NPLP.HeadFormat +'</span></h2>\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">'+ this.NPLP.CloseText +'</a>\n';
		htmlArray[htmlArrayIndex++] = '  </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_cont_cover">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-cont_inner">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_cover mod-balloon-set_single">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_box">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_item">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="select-person_area sp10">\n';
		// 行×列のselect出力
		//行ごと表示・非表示判定の準備
		var isDispRowArray = new Array(rowMax);
		for(var ri = 0; ri < rowMax; ri++){
			var result = false;
			for(var ci = 0; ci < columnMax; ci++){
				result |= this.dispMatrix[ri][ci];
			}
			// 列の中に１つでも表示項目があれば指定行は表示チェック対象
			if(result){
				isDispRowArray[ri] = true;
			}else{
				isDispRowArray[ri] = false;
			}
		}
		for(var ri = 0; ri < rowMax; ri++){
			// 表示項目は出力,非表示項はスキップ
			if(isDispRowArray[ri]){
				htmlArray[htmlArrayIndex++] = '    <dl class="ttl_6em select-person_box">\n';
				htmlArray[htmlArrayIndex++] = '    <dt class="tleft bold">'+ this.NPLP.RowName[ri].text +'</dt>\n';
				var countColumnNewLine = 0;
				for(var ci = 0; ci < columnMax; ci++){
					if(this.dispMatrix[ri][ci]){
						var selectId = this.column[ci].name + this.row[ri].name + this.numberOfPeopleCount + 'Select';
						// 同じ種類の行について2個ずつ改行するための制御
						if(countColumnNewLine >= 2){
							htmlArray[htmlArrayIndex++] = '    </dl>\n';
							htmlArray[htmlArrayIndex++] = '\n';
							htmlArray[htmlArrayIndex++] = '    <dl class="ttl_6em select-person_box noborder">\n';
							htmlArray[htmlArrayIndex++] = '    <dt class="tleft bold">&nbsp;</dt>\n';
							countColumnNewLine = 0;
						}
						if(this.row[ri].nolegend){
							htmlArray[htmlArrayIndex++] = '    <dd class="ttl w4em nowrap">&nbsp;</dd>\n';
						}else{
							if(this.NPLP.ColumnName[ci].text2){
								htmlArray[htmlArrayIndex++] = '    <dd class="ttl w4em nowrap line2">'+ this.NPLP.ColumnName[ci].text +'<br>'+ this.NPLP.ColumnName[ci].text2 +'</dd>\n';
							}else{
								htmlArray[htmlArrayIndex++] = '    <dd class="ttl w4em nowrap">'+ this.NPLP.ColumnName[ci].text +'</dd>\n';
							}
						}
						htmlArray[htmlArrayIndex++] = '    <dd>\n';
						htmlArray[htmlArrayIndex++] = '    <ul class="item-num-set">\n';
						htmlArray[htmlArrayIndex++] = '    <li class="item-num-down m_nopTargetTabIndex" tabindex="0">&#x2014;</li>\n';
						htmlArray[htmlArrayIndex++] = '    <li class="item-num-val">\n';
						htmlArray[htmlArrayIndex++] = '    <ul class="mod-customSelect">\n';
						htmlArray[htmlArrayIndex++] = '    <li class="m_nopTargetTabIndex" tabindex="0"><span class="item-val m_numberOfPeople_text"></span>\n';
						if(this.column[ci].meal !== undefined){
							htmlArray[htmlArrayIndex++] = '    <ul class="mod-option" id="'+ selectId +'" data-switch_disabled="'+ this.column[ci].meal +'">\n';
						}else{
							htmlArray[htmlArrayIndex++] = '    <ul class="mod-option" id="'+ selectId +'">\n';
						}
						htmlArray[htmlArrayIndex++] = '    </ul>\n';
						htmlArray[htmlArrayIndex++] = '    </li>\n';
						htmlArray[htmlArrayIndex++] = '    </ul>\n';
						htmlArray[htmlArrayIndex++] = '    </li>\n';
						htmlArray[htmlArrayIndex++] = '    <li class="item-num-up m_nopTargetTabIndex" tabindex="0">＋</li>\n';
						htmlArray[htmlArrayIndex++] = '    </ul>\n';
						htmlArray[htmlArrayIndex++] = '    </dd>\n';
						if(this.NPLP.ColumnName[ci].text === '' || this.row[ri].nolegend){
							htmlArray[htmlArrayIndex++] = '    <dd class="ttl w4em nowrap"></dd>\n';
							htmlArray[htmlArrayIndex++] = '    <dd></dd>\n';
						}
						countColumnNewLine++;
					}
				}
				htmlArray[htmlArrayIndex++] = '    </dl>\n';
				htmlArray[htmlArrayIndex++] = '\n';
			}
		}
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .mod-column_box -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_submit">\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-submit_btn js-mod-submit_btn m_numberOfPeopleChgSet" onclick="m_tourModuleNumberOfPeople.nops[\'' + this.name + '\'].chgSet({isChangeText : true})">'+ this.NPLP.SubmitButtonFormat +'</a>\n';
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_box -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_inner -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  \n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_contents -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_inner -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_cover -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon -->\n';
		// 初回出力時は大枠divを出力
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_cover -->\n';
			$tour_module_jq('body').append(htmlArray.join(''));
			targetElement = document.getElementById(this.id);
		}else{
			// 部品HTML再構築
			targetElement.innerHTML = htmlArray.join('');
		}
		// 決定ボタンをエンターキー操作時、クリックイベントに変換
		$tour_module_jq(targetElement).find('.m_numberOfPeopleChgSet').on('keydown',function(event){
			if(event.keyCode === 13){ // エンターキー
				$tour_module_jq(this).trigger('click');
			}
		});
		
		// 各参加人数のプルダウンに対して選択肢を設定
		this.setNumberOfPeoplePull(targetElement);
		
		// 選択値変更時のイベント登録・初期設定
		var ctrlDisabled = this.ctrlDisabled;
		if(ctrlDisabled){
			$tour_module_jq(targetElement).find('.mod-option').change(
				{
					parent : $tour_module_jq(targetElement),
					callerId : this.textId,
					dispMatrix : this.dispMatrix,
					no : this.numberOfPeopleCount,
					column : this.column,
					row : this.row,
					htmlType : this.htmlType,
					switchValue : ctrlDisabled,
					inputSelector : '.mod-option'
				},
				this.ctrlDisabledNumberOfPeople
			);
			// 活性・非活性初期化
			$tour_module_jq(targetElement).find('.mod-option').each(function(){
				var selSwitchDisabled = $tour_module_jq(this).data('switch_disabled');
				if(selSwitchDisabled !== undefined){
					var inputValue = parseInt($tour_module_jq(this).find('.mod-current').data('val'));
					if(inputValue >=  ctrlDisabled){
						// 最初にヒットした活性・非活性制御対象選択項目でイベント発火
						$tour_module_jq(this).trigger('change');
						return false;
					}
				}else{
					return true;
				}
			});
		}
		
	}else if(type === m_tourModuleNumberOfPeople_FixedParams.domtour.sec.htmlType){
		// 国内ツアー2nd
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '<div id="'+ this.id +'" class="modal-balloon module-travel_parts '+ this.className +' select-num mod-column w780">\n';
		}
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_cover">\n';
		htmlArray[htmlArrayIndex++] = '<i class="modal-balloon_arrow"></i>\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_inner">\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_contents">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_title">\n';
		htmlArray[htmlArrayIndex++] = '    <h2 class="mod-balloon_ttl"><span class="small">'+ this.NPLP.HeadFormat +'</span></h2>\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n';
		htmlArray[htmlArrayIndex++] = '  </div>\n';
		htmlArray[htmlArrayIndex++] = '  \n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_cont_cover">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-cont_inner">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_cover mod-balloon-set_single">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_box">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_item">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="select-person_area">\n';
		// 行×列のselect出力
		//行ごと表示・非表示判定の準備
		var isDispRowArray = new Array(rowMax);
		for(var ri = 0; ri < rowMax; ri++){
			var result = false;
			for(var ci = 0; ci < columnMax; ci++){
				result |= this.dispMatrix[ri][ci];
			}
			// 列の中に１つでも表示項目があれば指定行は表示チェック対象
			if(result){
				isDispRowArray[ri] = true;
			}else{
				isDispRowArray[ri] = false;
			}
		}
		for(var ri = 0; ri < rowMax; ri++){
			// 表示項目は出力,非表示項はスキップ
			if(isDispRowArray[ri]){
				if(this.row[ri].nolegend){
					htmlArray[htmlArrayIndex++] = '<fieldset class="mod-input_box select-person_box nolegend">\n';
				}else{
					htmlArray[htmlArrayIndex++] = '<fieldset class="mod-input_box select-person_box">\n';
				}
				htmlArray[htmlArrayIndex++] = '<legend><span class="mod-input_head bold">'+ this.NPLP.RowName[ri].text +'</span></legend>\n';
				for(var ci = 0; ci < columnMax; ci++){
					if(this.dispMatrix[ri][ci]){
						var selectId = this.column[ci].name + this.row[ri].name + this.numberOfPeopleCount + 'Select';
						htmlArray[htmlArrayIndex++] = '    <div class="mod-input_parts">\n';
						// 行毎の列タイトル変更
						if($tour_module_jq.isArray(this.NPLP.ColumnName[ci].text)){
							if(this.NPLP.ColumnName[ci].text2){
								htmlArray[htmlArrayIndex++] = '    <label for="'+ selectId +'"><span class="mod-input_head">'+ this.NPLP.ColumnName[ci].text[ri].text +'<br class="isSPn">'+ this.NPLP.ColumnName[ci].text2[ri].text2 +'</span></label>\n';
							}else{
								htmlArray[htmlArrayIndex++] = '    <label for="'+ selectId +'"><span class="mod-input_head">'+ this.NPLP.ColumnName[ci].text[ri].text +'</span></label>\n';
							}
						}else{
							// 列タイトルは固定で出力
							if(this.NPLP.ColumnName[ci].text2){
								htmlArray[htmlArrayIndex++] = '    <label for="'+ selectId +'"><span class="mod-input_head">'+ this.NPLP.ColumnName[ci].text +'<br class="isSPn">'+ this.NPLP.ColumnName[ci].text2 +'</span></label>\n';
							}else{
								htmlArray[htmlArrayIndex++] = '    <label for="'+ selectId +'"><span class="mod-input_head">'+ this.NPLP.ColumnName[ci].text +'</span></label>\n';
							}
						}
						
						htmlArray[htmlArrayIndex++] = '    <div class="mod-select_wrap">\n';
						if(this.column[ci].meal !== undefined){
							htmlArray[htmlArrayIndex++] = '        <select name="" id="'+ selectId +'" class="mod-select" data-switch_disabled="'+ this.column[ci].meal +'">\n';
						}else{
							htmlArray[htmlArrayIndex++] = '        <select name="" id="'+ selectId +'" class="mod-select">\n';
						}
																// 値一覧が追加される箇所
						htmlArray[htmlArrayIndex++] = '        </select>\n';
						htmlArray[htmlArrayIndex++] = '    </div>\n';
						htmlArray[htmlArrayIndex++] = '    </div>\n';
					}
				}
				htmlArray[htmlArrayIndex++] = '    </fieldset>\n';
				htmlArray[htmlArrayIndex++] = '\n';
			}
		}
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .mod-column_box -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_submit">\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-submit_btn js-mod-submit_btn m_numberOfPeopleChgSet" onclick="m_tourModuleNumberOfPeople.nops[\'' + this.name + '\'].chgSet({isChangeText : true})">'+ this.NPLP.SubmitButtonFormat +'</a>\n';
		htmlArray[htmlArrayIndex++] = '    <!-- <div class="txt-btm">\n';
		htmlArray[htmlArrayIndex++] = '    <p class="ico_mod_help"><a class="ico-newwin03" target="_blank" href="#" onclick="SCClick("");return true;">利用可能人数について</a></p>\n';
		htmlArray[htmlArrayIndex++] = '    </div> -->\n';
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_box -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_inner -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  \n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_contents -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_inner -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_cover -->\n';
		// 初回出力時は大枠divを出力
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon -->\n';
			$tour_module_jq('body').append(htmlArray.join(''));
			targetElement = document.getElementById(this.id);
		}else{
			// 部品HTML再構築
			targetElement.innerHTML = htmlArray.join('');
		}

		// 決定ボタンをエンターキー操作時、クリックイベントに変換
		$tour_module_jq(targetElement).find('.m_numberOfPeopleChgSet').on('keydown',function(event){
			if(event.keyCode === 13){ // エンターキー
				$tour_module_jq(this).trigger('click');
			}
		});
		
		// 各参加人数のプルダウンに対して選択肢を設定
		this.setNumberOfPeoplePull(targetElement);
		
		// 選択値変更時のイベント登録・初期設定
		var ctrlDisabled = this.ctrlDisabled;
		if(ctrlDisabled){
			$tour_module_jq(targetElement).find('.mod-select').change(
				{
					parent : $tour_module_jq(targetElement),
					callerId : this.textId,
					dispMatrix : this.dispMatrix,
					no : this.numberOfPeopleCount,
					column : this.column,
					row : this.row,
					htmlType : this.htmlType,
					switchValue : ctrlDisabled,
					inputSelector : '.mod-select'
				},
				this.ctrlDisabledNumberOfPeople
			);
			// 活性・非活性初期化
			$tour_module_jq(targetElement).find('.mod-select').each(function(){
				var selSwitchDisabled = $tour_module_jq(this).data('switch_disabled');
				if(selSwitchDisabled !== undefined){
					var inputValue = parseInt($tour_module_jq(this).val());
					if(inputValue >=  ctrlDisabled){
						// 最初にヒットした活性・非活性制御対象選択項目でイベント発火
						$tour_module_jq(this).trigger('change');
						return false;
					}
				}else{
					return true;
				}
			});
		}
		
		
	}else if(type === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType){
		// 海外ツアーTOP
		// 初回出力時は大枠divを出力
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '<div id="'+ this.id +'" class="modal-balloon select-num mod-column">\n';
		}
		htmlArray[htmlArrayIndex++] = '<!-- \n';
		htmlArray[htmlArrayIndex++] = '  *****************************************************\n';
		htmlArray[htmlArrayIndex++] = '  ******************* select-num **********************\n';
		htmlArray[htmlArrayIndex++] = '  *****************************************************\n';
		htmlArray[htmlArrayIndex++] = '-->\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_cover">\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_inner">\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_contents">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_title">\n';
		htmlArray[htmlArrayIndex++] = '    <h2 class="mod-balloon_ttl"><span class="small">'+ this.NPLP.HeadFormat +'</span></h2>\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">'+ this.NPLP.CloseText +'</a>\n';
		htmlArray[htmlArrayIndex++] = '  </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_cont_cover">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-cont_inner">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_cover mod-balloon-set_single">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_box">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_item">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="ttl-aera">\n';
		htmlArray[htmlArrayIndex++] = '    <dl class="ttl_5em select-person_box">\n';
		htmlArray[htmlArrayIndex++] = '    <dt></dt>\n';
		// 行のタイトル出力
		for(var ci = 0; ci < columnMax; ci++){
			// 合計行はスキップ
			if(!this.column[ci].sumColumn){
				if(this.NPLP.ColumnName[ci].spanText){
					htmlArray[htmlArrayIndex++] = '    <dd>'+ this.NPLP.ColumnName[ci].text +'<span class="small">'+ this.NPLP.ColumnName[ci].spanText +'</span></dd>\n';
				}else{
					htmlArray[htmlArrayIndex++] = '    <dd>'+ this.NPLP.ColumnName[ci].text +'</dd>\n';
				}
			}
		}
		htmlArray[htmlArrayIndex++] = '    </dl>\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .ttl-aera -->\n';
		htmlArray[htmlArrayIndex++] = '    <div class="select-person_area">\n';
		// 行×列のselect出力
		//行ごと表示・非表示判定の準備
		var isDispRowArray = new Array(rowMax);
		for(var ri = 0; ri < rowMax; ri++){
			var result = false;
			for(var ci = 0; ci < columnMax; ci++){
				result |= this.dispMatrix[ri][ci];
			}
			// 列の中に１つでも表示項目があれば指定行は表示チェック対象
			if(result){
				isDispRowArray[ri] = true;
			}else{
				isDispRowArray[ri] = false;
			}
		}
		for(var ri = 0; ri < rowMax; ri++){
			// 表示項目は出力,非表示項はスキップ
			if(isDispRowArray[ri]){
				htmlArray[htmlArrayIndex++] = '    <dl class="ttl_5em select-person_box">\n';
				htmlArray[htmlArrayIndex++] = '    <dt>'+ this.NPLP.RowName[ri].text +'</dt>\n';
				for(var ci = 0; ci < columnMax; ci++){	// インデックス0は合計のため表示なし
					// 表示項目は出力,非表示項はスキップ
					if(this.dispMatrix[ri][ci]){
						var selectId = this.column[ci].name + this.row[ri].name + this.numberOfPeopleCount + 'Select';
						htmlArray[htmlArrayIndex++] = '    <dd>\n';
						htmlArray[htmlArrayIndex++] = '    <ul class="item-num-set">\n';
						htmlArray[htmlArrayIndex++] = '    <li class="item-num-down" tabindex="0">&#x2014;</li>\n';
						htmlArray[htmlArrayIndex++] = '    <li class="item-num-val">\n';
						htmlArray[htmlArrayIndex++] = '    <ul class="mod-customSelect">\n';
						htmlArray[htmlArrayIndex++] = '    <li tabindex="0"><span class="item-val m_numberOfPeople_text"></span>\n';
						htmlArray[htmlArrayIndex++] = '        <ul id="'+ selectId +'" class="mod-option">\n';
																// 値一覧が追加される箇所
						htmlArray[htmlArrayIndex++] = '        </ul>\n';
						htmlArray[htmlArrayIndex++] = '    </li>\n';
						htmlArray[htmlArrayIndex++] = '    </ul>\n';
						htmlArray[htmlArrayIndex++] = '    </li>\n';
						htmlArray[htmlArrayIndex++] = '    <li class="item-num-up" tabindex="0">＋</li>\n';
						htmlArray[htmlArrayIndex++] = '    </ul>\n';
						htmlArray[htmlArrayIndex++] = '    </dd>\n';
					}
				}
				htmlArray[htmlArrayIndex++] = '    </dl>\n';
				htmlArray[htmlArrayIndex++] = '\n';
			}
		}
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .mod-column_box -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_submit">\n';
		htmlArray[htmlArrayIndex++] = '    <p class="mod-err_txt"></p>\n';  // エラー時文言出力
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-submit_btn js-mod-submit_btn m_numberOfPeopleChgSet" onclick="m_tourModuleNumberOfPeople.nops[\'' + this.name + '\'].chgSet({isChangeText : true})">'+ this.NPLP.SubmitButtonFormat +'</a>\n';
		htmlArray[htmlArrayIndex++] = '    <div class="txt-btm">\n';
		htmlArray[htmlArrayIndex++] = '    <p class="ico_mod_help"><a class="ico-newwin03" target="_blank" href="'+ m_tourModuleCommon_ASW_DOMAIN + this.NPLP.Help.URL +'" onclick="SCClick(\''+ this.NPLP.Help.SiteCatalystId +'\');return true;">'+ this.NPLP.Help.Text +'</a></p>\n';
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .mod-column_box -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_box -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_inner -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  \n';
		htmlArray[htmlArrayIndex++] = '  \n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_contents -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_inner -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_cover -->\n';
		// 初回出力時は大枠divを出力
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon -->\n';
			$tour_module_jq('body').append(htmlArray.join(''));
			targetElement = document.getElementById(this.id);
		}else{
			// 部品HTML再構築
			targetElement.innerHTML = htmlArray.join('');
		}

		// 決定ボタンをエンターキー操作時、クリックイベントに変換
		$tour_module_jq(targetElement).find('.m_numberOfPeopleChgSet').on('keydown',function(event){
			if(event.keyCode === 13){ // エンターキー
				$tour_module_jq(this).trigger('click');
			}
		});

		// 各参加人数の入力項目に選択肢を設定
		this.setNumberOfPeoplePull(targetElement);

		// 選択値変更時のイベント登録
		var checkNumberOfPeopleFunc = this.checkNumberOfPeople;
		if(checkNumberOfPeopleFunc){
			$tour_module_jq(targetElement).find('.mod-option').change(
				{
					parent : $tour_module_jq(targetElement),
					callerId : this.textId,
					checkFunc : this.checkValueFunc,
					errorTextArray : this.NPLP.ErrorText,
					column : this.column,
					row : this.row,
					htmlType : this.htmlType
				},
				checkNumberOfPeopleFunc
			);
		}
		// 初期設定(任意のセレクト1つのイベント実行)
		$tour_module_jq(targetElement).find('.mod-option').eq(0).trigger('change');

	}else{
		// 海外ツアー2nd
		// 初回出力時は大枠divを出力
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '<div id="'+ this.id +'" class="modal-balloon module-travel_parts select-num mod-column">\n';
		}
		htmlArray[htmlArrayIndex++] = '<!-- \n';
		htmlArray[htmlArrayIndex++] = '  *****************************************************\n';
		htmlArray[htmlArrayIndex++] = '  ******************* select-num **********************\n';
		htmlArray[htmlArrayIndex++] = '  *****************************************************\n';
		htmlArray[htmlArrayIndex++] = '-->\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_cover">\n';
		htmlArray[htmlArrayIndex++] = '<i class="modal-balloon_arrow"></i>\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_inner">\n';
		htmlArray[htmlArrayIndex++] = '<div class="modal-balloon_contents">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_title">\n';
		htmlArray[htmlArrayIndex++] = '    <h2 class="mod-balloon_ttl"><span class="small">'+ this.NPLP.HeadFormat +'</span></h2>\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n';
		htmlArray[htmlArrayIndex++] = '  </div>\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon_cont_cover">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-cont_inner">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_cover mod-balloon-set_single">\n';
		htmlArray[htmlArrayIndex++] = '  <div class="mod-balloon-set_box">\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_item">\n';
		htmlArray[htmlArrayIndex++] = '		<div class="mod-text_right mod-information_topCHrb">\n';
		htmlArray[htmlArrayIndex++] = '    <p class="ico_mod_help"><a class="ico-newwin03" target="_blank" href="'+ m_tourModuleCommon_ASW_DOMAIN + this.NPLP.Help.URL +'" >'+ this.NPLP.Help.Text +'</a></p>\n';
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '    <div class="ttl-aera">\n';
		htmlArray[htmlArrayIndex++] = '    <dl class="ttl_5em select-person_box">\n';
		htmlArray[htmlArrayIndex++] = '    <dt></dt>\n';
		// 行のタイトル出力
		for(var ci = 0; ci < columnMax; ci++){
			// 合計行はスキップ
			if(!this.column[ci].sumColumn){
				if(this.NPLP.ColumnName[ci].spanText){
					htmlArray[htmlArrayIndex++] = '    <dd>'+ this.NPLP.ColumnName[ci].text +'<span class="small">'+ this.NPLP.ColumnName[ci].spanText +'</span></dd>\n';
				}else{
					htmlArray[htmlArrayIndex++] = '    <dd>'+ this.NPLP.ColumnName[ci].text +'</dd>\n';
				}
			}
		}
		htmlArray[htmlArrayIndex++] = '    </dl>\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .ttl-aera -->\n';
		htmlArray[htmlArrayIndex++] = '    <div class="select-person_area">\n';
		// 行×列のselect出力
		//行ごと表示・非表示判定の準備
		var isDispRowArray = new Array(rowMax);
		for(var ri = 0; ri < rowMax; ri++){
			var result = false;
			for(var ci = 0; ci < columnMax; ci++){
				result |= this.dispMatrix[ri][ci];
			}
			// 列の中に１つでも表示項目があれば指定行は表示チェック対象
			if(result){
				isDispRowArray[ri] = true;
			}else{
				isDispRowArray[ri] = false;
			}
		}
		for(var ri = 0; ri < rowMax; ri++){
			// 表示項目は出力,非表示項はスキップ
			if(isDispRowArray[ri]){
				htmlArray[htmlArrayIndex++] = '    <fieldset class="mod-input_box select-person_box">\n';
				htmlArray[htmlArrayIndex++] = '    <legend><span class="mod-input_head">'+ this.NPLP.RowName[ri].text +'</span></legend>\n';
				for(var ci = 0; ci < columnMax; ci++){	// インデックス0は合計のため表示なし
					htmlArray[htmlArrayIndex++] = '    <div class="mod-input_parts">\n';
					// 表示項目は出力,非表示項はスキップ
					if(this.dispMatrix[ri][ci]){
						var selectId = this.column[ci].name + this.row[ri].name + this.numberOfPeopleCount + 'Select';
						// 項目名出しわけ
						if(this.NPLP.ColumnName[ci].spanText){
							htmlArray[htmlArrayIndex++] = '    <label for="'+ selectId +'"><span class="mod-input_head">'+ this.NPLP.ColumnName[ci].text +'<span class="small">'+ this.NPLP.ColumnName[ci].spanText +'</span></span></label>\n';
						}
						else{
							htmlArray[htmlArrayIndex++] = '    <label for="'+ selectId +'"><span class="mod-input_head">'+ this.NPLP.ColumnName[ci].text +'</span></label>\n';
						}
						htmlArray[htmlArrayIndex++] = '    <div class="mod-select_wrap">\n';
						htmlArray[htmlArrayIndex++] = '		<select name="" id="'+ selectId +'" class="mod-select">\n';
															// 値一覧が追加される箇所
						htmlArray[htmlArrayIndex++] = '		</select>\n';
						htmlArray[htmlArrayIndex++] = '		</div>\n';
					}
					htmlArray[htmlArrayIndex++] = '    </div>\n';
				}
				htmlArray[htmlArrayIndex++] = '    </fieldset>\n';
				htmlArray[htmlArrayIndex++] = '\n';
			}
		}
		htmlArray[htmlArrayIndex++] = '		</div>';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .mod-column_box -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-column_box mod-select_submit">\n';
		htmlArray[htmlArrayIndex++] = '    <div class="mod-err_wrap">\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-err_close js-mod-err_close" tabindex="0">'+ this.NPLP.CloseText +'</a>\n';
		htmlArray[htmlArrayIndex++] = '    <p class="mod-err_txt"></p>\n'; // エラー時文言出力
		htmlArray[htmlArrayIndex++] = '    </div>\n';
		htmlArray[htmlArrayIndex++] = '    <a href="javascript:void(0);" class="mod-submit_btn js-mod-submit_btn m_numberOfPeopleChgSet" onclick="m_tourModuleNumberOfPeople.nops[\'' + this.name + '\'].chgSet({isChangeText : true})">'+ this.NPLP.SubmitButtonFormat +'</a>\n';
		htmlArray[htmlArrayIndex++] = '    </div><!-- / .mod-column_box -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_cover -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon-set_box -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_inner -->\n';
		htmlArray[htmlArrayIndex++] = '  </div><!-- / .mod-balloon_cont_cover -->\n';
		htmlArray[htmlArrayIndex++] = '\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_contents -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_inner -->\n';
		htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon_cover -->\n';
		// 初回出力時は大枠divを出力
		if(!targetElement){
			htmlArray[htmlArrayIndex++] = '</div><!-- / .modal-balloon -->\n';
			$tour_module_jq('body').append(htmlArray.join(''));
			targetElement = document.getElementById(this.id);
		}else{
			// 部品HTML再構築
			targetElement.innerHTML = htmlArray.join('');
		}

		// 決定ボタンをエンターキー操作時、クリックイベントに変換
		$tour_module_jq(targetElement).find('.m_numberOfPeopleChgSet').on('keydown',function(event){
			if(event.keyCode === 13){ // エンターキー
				$tour_module_jq(this).trigger('click');
			}
		});

		// 各参加人数のプルダウンに対して選択肢を設定
		this.setNumberOfPeoplePull(targetElement);

		// 選択値変更時のイベント登録
		var checkNumberOfPeopleFunc = this.checkNumberOfPeople;
		if(checkNumberOfPeopleFunc){
			$tour_module_jq(targetElement).find('.mod-select').change(
				{
					parent : $tour_module_jq(targetElement),
					callerId : this.textId,
					checkFunc : this.checkValueFunc,
					errorTextArray : this.NPLP.ErrorText,
					column : this.column,
					row : this.row,
					htmlType : this.htmlType
				},
				checkNumberOfPeopleFunc
			);
		}
		// 初期設定(任意のセレクト1つのイベント実行)
		$tour_module_jq(targetElement).find('.mod-select').eq(0).trigger('change');
	}

	return true;
};

// 参加人数プルダウン選択肢の設定
/**
 * <p>参加人数選択ダイアログの各参加人数設定プルダウンに選択肢と<br>
 * 選択状態を設定します。<br>
 * 選択状態が取得できない場合はプルダウンの先頭の選択肢が選択状態になります。</p>
 * @param {Object} target 参加人数選択ダイアログのインスタンス
 */
m_tourModuleNumberOfPeople.prototype.setNumberOfPeoplePull = function(target) {

	var listName = this.NPLP.List;
	var html = '';
	var targetSelect = undefined;
	var selectedValue = undefined;
	var columnArray = this.column;	
	var columnMax = columnArray.length;
	var rowArray = this.row;
	var rowMax = rowArray.length;
	var dispArray = this.dispMatrix;
	var no = this.numberOfPeopleCount;
	var type = this.htmlType;

	for(var ri = 0; ri < rowMax; ri++){
		for(var ci = 0; ci < columnMax; ci++){
			if(!dispArray[ri][ci]){
				continue;
			}
			html = '';
			var ln = listName[ci];
			// 非表示の場合はselect設定なし
			if(!_.isEmpty(ln)){
				for (var key in ln) {
					var date = ln[key];
					var codeValue = date['value'];
					var textValue = date['text'];
					if(type === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType
						|| type === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType){
						html += '<li data-val="' + codeValue + '" tabindex="0">' + textValue + '</li>\n';
					}else{
						html += '<option value="' + codeValue + '">' + textValue + '</option>\n';
					}
				}
				targetSelect = $tour_module_jq(target).find('#' + columnArray[ci].name + rowArray[ri].name + no + 'Select');
				targetSelect.append(html);
				$tour_module_jq($tour_module_jq(this.hidden[ri])).each(function(){
					if($tour_module_jq(this).attr('name') === columnArray[ci].name + rowArray[ri].name){
						selectedValue = $tour_module_jq(this).val();
						return false;
					}
				});
				if(type === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType
					|| type === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType){
					var targetObj = $tour_module_jq(targetSelect).parents('.mod-customSelect');
					var selDataObj = $tour_module_jq(targetSelect).find('li[data-val=' + selectedValue + ']');
					targetObj.find('.m_numberOfPeople_text').text(selDataObj.text());
					selDataObj.addClass('mod-current');
				}else{
					targetSelect.find('[value="' + selectedValue + '"]').prop('selected', true);
				}
			}
		}
	}
	
	// その他必要な入力項目制御設定
	this.setInputControl(target);
};

/**
 * <p>参加人数選択ダイアログの各参加人数入力項目の制御を行うイベントを登録します。
 * @param {Object} target 参加人数選択ダイアログのインスタンス
 */
m_tourModuleNumberOfPeople.prototype.setInputControl = function(target) {
	var type = this.htmlType;
	var thisInputObj = this;
	
	if(type === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType
		|| type === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType){
		//プラスボタン
		$tour_module_jq(target).find('.item-num-up').on({
			'click': function() {
				thisInputObj.setNumUp(this);
			},
			'keydown': function(e) {
				if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
					thisInputObj.setNumUp(this);
					return false;
				}
			}
		});
		//マイナスボタン
		$tour_module_jq(target).find('.item-num-down').on({
			'click': function() {
				thisInputObj.setNumDown(this);
			},
			'keydown': function(e) {
				if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
					thisInputObj.setNumDown(this);
					return false;
				}
			}
		});
		//擬似プルダウンを開く
		$tour_module_jq(target).find('.mod-customSelect').on({
			'click': function() {
				thisInputObj.customSelectOpen(this);
			},
			'keydown': function(e) {
				if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
					thisInputObj.customSelectOpen(this);
					$tour_module_jq('.members-module-list li').tile(3);
				}
			}
		});
		//矢印でオプション内移動　※キーボード操作時のみ
		$tour_module_jq(target).find('.mod-customSelect .mod-option').find('li').on('keydown', function(e) {
			e.preventDefault();
			//上
			if ((e.which && e.which === 38) || (e.keyCode && e.keyCode === 38)) {
				if ($tour_module_jq(this).prev('li')) {
					$tour_module_jq(this).prev('li').focus();
				} else {
					return false;
				}
			}
			//下
			if ((e.which && e.which === 40) || (e.keyCode && e.keyCode === 40)) {
				if ($tour_module_jq(this).next('li')) {
					$tour_module_jq(this).next('li').focus();
				} else {
					return false;
				}
			}
		});
		// 擬似プルダウン選択時※バブリングフェーズでイベント登録
		$tour_module_jq(target).find('.mod-option li').on({
			'click': function(e) {
				e.stopPropagation();	// TOP既存JS処理無効化
				thisInputObj.setNum(this);
			},
			'keydown': function(e) {
				if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
					e.stopPropagation();	// TOP既存JS処理無効化
					thisInputObj.setNum(this);
				}
			}
		});
	}else{
		// 特に無し
	}
}

/**
 * <p>参加人数選択ダイアログの各参加人数入力項目の＋ボタン押下時の処理を行います。
 * @param {Object} _this ＋ボタン
 */
m_tourModuleNumberOfPeople.prototype.setNumUp = function(_this) {
	var baseObj = $tour_module_jq(_this).prev('.item-num-val').find('.mod-option');
	var baseObjVal = baseObj.find('.mod-current').data('val');
	var baseObjOpMax = baseObj.find('li:last').data('val');

	var downBtn = $tour_module_jq(_this).siblings('.item-num-down');
	downBtn.removeClass('disabled');

	if (baseObjVal < baseObjOpMax) {
		baseObjVal = baseObj.find('.mod-current').next().data('val');
		$tour_module_jq(_this).removeClass('disabled');

		baseObj.find('li').removeClass('mod-current');
		baseObj.find('li[data-val=' + baseObjVal + ']').addClass('mod-current');
		baseObj.prev('[class*=item-val]').text(baseObj.find('li[data-val=' + baseObjVal + ']').text());
		baseObj.trigger('change');
	} else if (baseObjVal == baseObjOpMax) {
		$tour_module_jq(_this).addClass('disabled');
	} else {}
};

/**
 * <p>参加人数選択ダイアログの各参加人数入力項目の－ボタン押下時の処理を行います。
 * @param {Object} _this －ボタン
 */
m_tourModuleNumberOfPeople.prototype.setNumDown = function(_this) {
	var baseObj = $tour_module_jq(_this).next('.item-num-val').find('.mod-option');
	var baseObjVal = baseObj.find('.mod-current').data('val');
	var upBtn = $tour_module_jq(_this).siblings('.item-num-up');

	upBtn.removeClass('disabled');

	if (baseObjVal == baseObj.find('li:first').data('val')) {
		$tour_module_jq(_this).addClass('disabled');
	} else {
		$tour_module_jq(_this).removeClass('disabled');

		baseObjVal = baseObj.find('.mod-current').prev().data('val');

		baseObj.find('li').removeClass('mod-current');
		baseObj.find('li[data-val=' + baseObjVal + ']').addClass('mod-current');
		baseObj.prev('[class*=item-val]').text(baseObj.find('li[data-val=' + baseObjVal + ']').text());
		baseObj.trigger('change');
	}
};

/**
 * <p>参加人数選択ダイアログの各参加人数入力項目のテキスト部分押下時の擬似プルダウン表示処理を行います。
 * @param {Object} _this 入力項目テキスト部分
 */
m_tourModuleNumberOfPeople.prototype.customSelectOpen = function(_this) {
	var opListObj = $tour_module_jq(_this).find('.mod-option');

	if (opListObj.css('display') == 'none') {
		$tour_module_jq('.mod-option').hide();
		opListObj.show();
		opListObj.find('.mod-current').focus();
	} else {
		$tour_module_jq('.mod-option').hide();
	}
};

/**
 * <p>参加人数選択ダイアログの各参加人数入力項目の擬似プルダウン選択時の処理を行います。
 * @param {Object} _this 選択項目
 */
m_tourModuleNumberOfPeople.prototype.setNum = function(_this) {
	var opListObj = $tour_module_jq(_this).closest('.mod-option');
	var opListCurrent = $tour_module_jq(_this);
	var opVal = opListCurrent.data('val');
	var opLabel = opListCurrent.text();

	opListObj.children('li').removeClass('mod-current');
	$tour_module_jq(_this).addClass('mod-current');

	//値とラベルセット
	opListObj.trigger('change');
	opListObj.closest('li').find('[class*=item-val]').text(opLabel);

	opListObj.hide();
	opListObj.closest('li').focus();
};

/**
 * 選択値変更時のチェック処理
 * @param {object} options
 */
m_tourModuleNumberOfPeople.prototype.checkNumberOfPeople = function(options) {
	var data = options.data;
	var parent = data.parent;
	var callerId = data.callerId;
	var checkFunc = data.checkFunc;
	var errorTextArray = data.errorTextArray;
	var column = data.column;
	var row = data.row;
	var htmlType = data.htmlType;

	var errorNum = undefined;
	if(checkFunc !== 'undefined' && typeof checkFunc === 'function') {
		// 参加人数部品内の選択項目を渡す
		if(htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType
			|| htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType){
			errorNum = parseInt(checkFunc(parent.find('.mod-option'), callerId, column, row));
		}else{
			errorNum = parseInt(checkFunc(parent.find('.mod-select'), callerId, column, row));
		}
	}

	if(!isNaN(errorNum) && errorNum < errorTextArray.length){
		parent.find('.mod-err_wrap').css('display','block');
		parent.find('.mod-err_txt').text(errorTextArray[errorNum]);
	}else{
		parent.find('.mod-err_wrap').css('display','none');
		parent.find('.mod-err_txt').text('');
	}
};

/**
 * 選択値変更時の活性・非活性制御
 * @param {object} options
 */
m_tourModuleNumberOfPeople.prototype.ctrlDisabledNumberOfPeople = function(options) {
	var data = options.data;
	var parent = data.parent;
	var htmlType = data.htmlType;
	var callerId = data.callerId;
	var dispArray = data.dispMatrix;
	var inputSelector = data.inputSelector;
	var no = data.no;
	var rowArray = data.row;
	var columnArray = data.column;
	var htmlType = data.htmlType;
	var switchValue = data.switchValue;
	var switchDisabledPatern = $tour_module_jq(this).data('switch_disabled');
	var selValue = 0;
	
	// 選択変更値取得
	if((htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType)
			|| (htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType)){
		selValue = parseInt($tour_module_jq(this).find('li.mod-current').data('val')); // TOP
	}else{
		selValue = parseInt($tour_module_jq(this).val()); // 2nd
	}
	// 選択項目の変更値が活性・非活性制御対象値以上の場合操作実施
	if(switchDisabledPatern !== undefined && selValue >= switchValue){
		// 選択された各参加人数設定値を取得
		var rowMax = rowArray.length;
		var colMax = columnArray.length;
		var v_array = new Array(rowMax);
		for(var ri = 0; ri < rowMax; ri++){
			v_array[ri] = new Array(colMax);
			for(var ci = 0; ci < colMax; ci++){
				// 表示項目は活性・非活性操作対象
				if(dispArray[ri][ci] === true){
					var selectId = columnArray[ci].name + rowArray[ri].name + no + 'Select';
					var selObj = $tour_module_jq('#'+ selectId);
					var selSwitchDisabled = selObj.data('switch_disabled');
					// 選択項目とその他各入力項目の活性・非活性化パターンが異なる場合は非活性化
					if(selSwitchDisabled !== undefined && switchDisabledPatern !== selSwitchDisabled){
						if((htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType)
								|| (htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType)){
							selObj.closest('.item-num-set').addClass('disabled'); // TOP
							selObj.closest('.item-num-set').find('.m_nopTargetTabIndex').removeAttr('tabindex');
						}else{
							selObj.prop("disabled", true); // 2nd
						}
					}else{
						if((htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType)
								|| (htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType)){
							selObj.closest('.item-num-set').removeClass('disabled'); // TOP
							selObj.closest('.item-num-set').find('.m_nopTargetTabIndex').attr('tabindex','0');
						}else{
							selObj.prop("disabled", false); // 2nd
						}
					}
				}
			}
		}
	// 選択項目と同じ活性・非活性化パターンの各入力項目がすべて制御対象値未満の場合はすべての入力項目を活性化
	}else if(switchDisabledPatern !== undefined && selValue < switchValue){
		var resetFlg = true;
		parent.find(inputSelector).each(function(){
			var selSwitchDisabled = $tour_module_jq(this).data('switch_disabled');
			if(switchDisabledPatern !== selSwitchDisabled){
				// 異なるパターンの入力項目は無視
				return true;
			}
			var inputValue = parseInt($tour_module_jq(this).val()); // 2nd
			if(isNaN(inputValue)){
				inputValue = parseInt($tour_module_jq(this).find('li.mod-current').data('val')); // TOP
			}
			if(inputValue >= switchValue){
				resetFlg = false;
				return false;
			}
		});
		if(resetFlg){
			if((htmlType === m_tourModuleNumberOfPeople_FixedParams.inttour.top.htmlType)
					|| (htmlType === m_tourModuleNumberOfPeople_FixedParams.domtour.top.htmlType)){
				parent.find('.item-num-set').removeClass('disabled'); // TOP
				parent.find('.item-num-set .m_nopTargetTabIndex').attr('tabindex','0');
			}else{
				parent.find(inputSelector).prop("disabled", false); // 2nd
			}
		}
	}else{
		// 活性・非活性化対象外項目操作時は実施なし
	}

};

