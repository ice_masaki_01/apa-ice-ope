/*=====================================================
* Script Name: inttour_module_list_JA.js
* Description: 空席照会モジュール 海外ツアー共通 定義リスト（日本語）
* Version: 1.02
* Last Up Date: 2018/1/24
=====================================================*/
//--------------------------------------------------------------------検索遷移先
var m_inttourModuleDp_fromAction = {
	'URL':'/inttour/booking/csm/search/ISBP2000/init'
};
var m_inttourModulePkg_fromAction = {
	'URL':'/inttour/booking/csm/search/ISAP1000/init'
};
var m_inttourModulePkg_fromAction_coursecode = {
	'URL':'/inttour/booking/csm/search/ISAP1100/init'
};
var m_inttourModulePkg_fromAction_oldBE = {
	'URL':'/旧BE'
}

var m_inttourModuleOption_fromAction = {
	'URL':'/psz/fwd/jsp/login_cooperation/intTourVeltraLoginJa.jsp'
};

//--------------------------------------------------------------------ご国内線乗り継ぎの方へ(海外旅作)
var m_inttourModuleDp_domConnection = {
	'Text':'国内線乗り継ぎの方へ',
	'URL':'/travel/int_dp/info/fare/connection.html',
	'SiteCatalystId':'pc_jp_inttour_tabisaku_module_txt_connection',
	'SiteCatalystIdIntTop':'pc_jp_int_tabisaku_module_txt_connection'
};

//--------------------------------------------------------------------アップグレード特典対象インフォメーション
var m_inttourModuleDp_upgradeAwardInformation = {
	'Text':'ANA国際線アップグレード特典対象クラス',
	'URL':'/inttour/dp/point/#a1_othr'
};

//--------------------------------------------------------------------利用可能人数についてURL
var m_inttourModuleDp_help = {
	'Text':'利用可能人数について',
	'URL':'/inttour/dp/point/#a1_hotl',
	'SiteCatalystId':'pc_jp_inttour_tour_module_txt_person',
	'SiteCatalystIdIntTop':'pc_jp_int_tour_module_txt_person'
};

//--------------------------------------------------------------------ご国内線乗り継ぎの方へ(海外パッケージ)
var m_inttourModulePkg_domConnection = {
	'Text':'国内線乗り継ぎの方へ',
	'URL':'/inttour/info/001/',
	'SiteCatalystId':'pc_jp_inttour_tabisaku_module_txt_connection',
	'SiteCatalystIdIntTop':'pc_jp_int_tabisaku_module_txt_connection'
};

//--------------------------------------------------------------------現地ホテルから参加プランを選択可能なツアーインフォメーション
var m_inttourModulePkg_hotelPlanInfomation = {
	'Text':'現地ホテルから参加プランを選択可能なツアー（航空券を利用しない）',
	'URL':'/inttour/package/point/2/index.html#a2genchihotelkara'
};

//--------------------------------------------------------------------１都市滞在・２都市滞在ラジオボタン
var m_inttourModuleDp_stayCityCountList = [
	{value:'0', text:'1都市滞在'},
	{value:'1', text:'2都市滞在'}
];

//--------------------------------------------------------------------出発日を指定・出発期間を指定ラジオボタン
var m_inttourModulePkg_departureDateTypeList = [
	{value:'0', text:'出発日を指定'},
	{value:'1', text:'出発期間を指定'}
];

//--------------------------------------------------------------------フリーワードで探す・添乗員同行ツアー名で探すラジオボタン
var m_inttourModulePkg_NameTypeList = [
	{value:'0', text:'フリーワードで<br>探す',       spText:'全てのツアーから探す'},
	{value:'1', text:'添乗員同行<br>ツアー名で探す', spText:'添乗員同行ツアーのみで探す'}
];

//--------------------------------------------------------------------部屋数
var m_inttourModuleDp_numberOfRoomsList = [
	{value:'1', text:'1部屋'},
	{value:'2', text:'2部屋'},
	{value:'3', text:'3部屋'},
	{value:'4', text:'4部屋'},
	{value:'5', text:'5部屋'},
	{value:'6', text:'6部屋'},
	{value:'7', text:'7部屋'},
	{value:'8', text:'8部屋'},
];

//--------------------------------------------------------------------座席クラス

//指定しない
var m_inttourModuleCommon_FareList_Unspecified = '_NOSET';

//座席クラス配列作成処理
var createClassFareList = function(e){
    var result = [];
    
    for (var i = 0; i < e.length ; i++) {
        switch (e[i]) {
        case 'Y':
            result.push({value:'0', text:'エコノミークラス'});
            break;
        case 'P':
            result.push({value:'3', text:'プレミアムエコノミー'});
            break;
        case 'C':
            result.push({value:'1', text:'ビジネスクラス'});
            break;
        case '':
            result.push({value:'',  text:'指定なし'});
            break;
        default:
            break;
        }
    }
    
    return result;
};

//方面・国毎座席クラス紐付け
//海外旅作
var m_inttourModuleDp_classFareList = {};
//デフォルト
m_inttourModuleDp_classFareList['default'] = createClassFareList(['Y', 'P', 'C']);
//アメリカ・カナダ・中米
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='3'})[0].value] = createClassFareList(['Y', 'P', 'C']);
//ハワイ
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='1'})[0].value] = createClassFareList(['Y', 'P', 'C']);
//グアム・パラオ
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='2'})[0].value] = createClassFareList(['Y', 'C']);
//ヨーロッパ
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='6'})[0].value] = createClassFareList(['Y', 'P', 'C']);
//アジア
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='5'})[0].value] = createClassFareList(['Y', 'C']);
m_inttourModuleDp_classFareList['TH'] = createClassFareList(['Y', 'P', 'C']);   // タイ
m_inttourModuleDp_classFareList['IN'] = createClassFareList(['Y', 'P', 'C']);   // インド
m_inttourModuleDp_classFareList['SG'] = createClassFareList(['Y', 'P', 'C']);   // シンガポール
m_inttourModuleDp_classFareList['ID'] = createClassFareList(['Y', 'P', 'C']);   // インドネシア
m_inttourModuleDp_classFareList['MY'] = createClassFareList(['Y', 'P', 'C']);   // マレーシア

//中国
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='4'})[0].value] = createClassFareList(['Y', 'C']); 
//オセアニア
m_inttourModuleDp_classFareList[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='7'})[0].value] = createClassFareList(['Y', 'P', 'C']);

//海外パッケージ
//デフォルト
var m_inttourPkg_classFareList_DEFAULT = [
	{value:'0', text:'エコノミークラス'},
	{value:'3', text:'プレミアムエコノミー'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//アメリカ・カナダ・中南米
var m_inttourPkg_classFareList_AME = [
	{value:'0', text:'エコノミークラス'},
	{value:'3', text:'プレミアムエコノミー'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//ハワイ
var m_inttourPkg_classFareList_HWA = [
	{value:'0', text:'エコノミークラス'},
	{value:'3', text:'プレミアムエコノミー'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//ヨーロッパ
var m_inttourPkg_classFareList_EUR = [
	{value:'0', text:'エコノミークラス'},
	{value:'3', text:'プレミアムエコノミー'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//アジア(都市・リゾート)
var m_inttourPkg_classFareList_ASIA = [
	{value:'0', text:'エコノミークラス'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//中国
var m_inttourPkg_classFareList_CN = [
	{value:'0', text:'エコノミークラス'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//オーストラリア・ニュージーランド
var m_inttourPkg_classFareList_OC = [
	{value:'0', text:'エコノミークラス'},
	{value:'3', text:'プレミアムエコノミー'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];
//アフリカ・中近東
var m_inttourPkg_classFareList_AF = [
	{value:'0', text:'エコノミークラス'},
	{value:'3', text:'プレミアムエコノミー'},
	{value:'1', text:'ビジネスクラス'},
	{value:'', text:'指定なし'}
];

//国・エリア毎座席クラス紐付け
var m_inttourModulePkg_classFareList = {};
m_inttourModulePkg_classFareList['default'] = m_inttourPkg_classFareList_DEFAULT; // デフォルト
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='3'})[0].value] = m_inttourPkg_classFareList_AME;   // アメリカ・カナダ・中南米
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='1'})[0].value] = m_inttourPkg_classFareList_HWA;   // ハワイ
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='6'})[0].value] = m_inttourPkg_classFareList_EUR;   // ヨーロッパ
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='5'})[0].value] = m_inttourPkg_classFareList_ASIA;  // アジア(都市・リゾート)
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='5'})[0].value + m_inttourModuleCommon_FareList_Unspecified] 
= createClassFareList(['Y', 'P', 'C', '']);	// 指定しない
m_inttourModulePkg_classFareList['TH'] = createClassFareList(['Y', 'P', 'C', '']);   // タイ
m_inttourModulePkg_classFareList['IN'] = createClassFareList(['Y', 'P', 'C', '']);   // インド
m_inttourModulePkg_classFareList['SG'] = createClassFareList(['Y', 'P', 'C', '']);   // シンガポール
m_inttourModulePkg_classFareList['ID'] = createClassFareList(['Y', 'P', 'C', '']);   // インドネシア
m_inttourModulePkg_classFareList['MY'] = createClassFareList(['Y', 'P', 'C', '']);   // マレーシア


m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='4'})[0].value] = m_inttourPkg_classFareList_CN;    // 中国
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='7'})[0].value] = m_inttourPkg_classFareList_OC;    // オーストラリア・ニュージーランド
m_inttourModulePkg_classFareList[m_tourModuleAirportList_ArrAreaList['PD_814'].filter(function(e){return e.key==='9'})[0].value] = m_inttourPkg_classFareList_AF;    // アフリカ・中近東

//--------------------------------------------------------------------座席クラス（コースコード）
var m_inttourModulePkg_classFareListCourseCode = {
	//航空コード判定(6-8桁:7桁目で判定)
	'Y':'0',		//エコノミークラス
	'P':'3',		//プレミアムエコノミー
	'C':'1'			//ビジネスクラス
}
var m_inttourModulePkg_classFareListCourseCodeBasic = {
	//コース基本コード判定(1-5桁:3桁目で判定)
		'2':'0',		//エコノミークラス
		'3':'0',		//エコノミークラス
		'4':'0',		//エコノミークラス
		'5':'3',		//プレミアムエコノミー
		'6':'3',		//プレミアムエコノミー
		'7':'3',		//プレミアムエコノミー
		'A':'1',		//ビジネスクラス
		'B':'1',		//ビジネスクラス
		'C':'1',		//ビジネスクラス
		'D':'1',		//ビジネスクラス
		'E':'1',		//ビジネスクラス
		'F':'1',		//ビジネスクラス
		'G':'1',		//ビジネスクラス
		'H':'1',		//ビジネスクラス
		'I':'1',		//ビジネスクラス
		'J':'1',		//ビジネスクラス
		'K':'1',		//ビジネスクラス
		'L':'1',		//ビジネスクラス
		'M':'1',		//ビジネスクラス
		'N':'1',		//ビジネスクラス
		'O':'1',		//ビジネスクラス
		'P':'1',		//ビジネスクラス
		'Q':'1',		//ビジネスクラス
		'R':'1',		//ビジネスクラス
		'S':'1',		//ビジネスクラス
		'T':'1',		//ビジネスクラス
		'U':'1',		//ビジネスクラス
		'V':'1',		//ビジネスクラス
		'W':'1',		//ビジネスクラス
		'X':'1',		//ビジネスクラス
		'Y':'1',		//ビジネスクラス
		'Z':'1'			//ビジネスクラス
};

/*=====================================================
参加人数入力部品用文言定義（海外ツアー・日本語）
=====================================================*/

var m_inttourModuleNumberOfPeople_NPLP = new m_tourModuleNumberOfPeopleLanguageProperties();

//ヘッダー文言
m_inttourModuleNumberOfPeople_NPLP.HeadFormat = '人数選択（お申し込み上限8名様まで）';

//決定ボタン
m_inttourModuleNumberOfPeople_NPLP.SubmitButtonFormat = '決定';

//エラー文言
m_inttourModuleNumberOfPeople_NPLP.ErrorText = [
  '部屋割りをご確認ください',		// システムエラー
  '部屋割りをご確認ください',		// チェックエラー
];

//利用人数について
m_inttourModuleNumberOfPeople_NPLP.Help = {
  Text : '利用可能人数について',
  URL : m_inttourModuleDp_help.URL,
  SiteCatalystId : m_inttourModuleDp_help.SiteCatalystId
};

//クローズボタン
m_inttourModuleNumberOfPeople_NPLP.CloseText = '×';

//--------------------------------------------------------------------参加人数 列名/行
m_inttourModuleNumberOfPeople_NPLP.ColumnName = [
	{text:'大人（12歳以上）'},
	{text:'子供（2歳～11歳）', spanText:'ベッドあり'},
	{text:'子供（2歳～11歳）', spanText:'ベッドなし'},
	{text:'幼児（0歳～1歳）'},
	{} //合計列のため表示なし
];
m_inttourModuleNumberOfPeople_NPLP.RowName = [
	{text:'1部屋目'},
	{text:'2部屋目'},
	{text:'3部屋目'},
	{text:'4部屋目'},
	{text:'5部屋目'},
	{text:'6部屋目'},
	{text:'7部屋目'},
	{text:'8部屋目'}
];
//--------------------------------------------------------------------参加人数(旅作)
m_inttourModuleNumberOfPeople_NPLP.List = [
  [
   //大人（12歳以上）
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'},
   {value:'6', text:'6名'},
	],
  [
   //子供（2歳～11歳）ベッドあり
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //子供（2歳～11歳）ベッドなし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'}
	],
  [
   //幼児（0歳～1歳）
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'}
	],
	[] //合計列のため表示なし
];

/*=====================================================
  海外旅作参加人数エラーチェック用定義（海外ツアー共通・日本語）
=====================================================*/
// 許容最大人数
var m_inttourModuleDp_errorCheckNumberOfPeopleMax = {
	all : 8,			// 全部屋合計(大人+子供(ベッドあり))
	roomEach : 6,		// 1部屋毎合計(大人+子供(ベッドあり))
	noBed : 2			// ベッドなし人数合計(子供(ベッドなし)+幼児)
};

////////////////////////////////////////////////
// エリア毎許容人数
// isError true:エラー有り, false:エラーなし
var m_inttourModuleDp_errorCheckNumberOfPeopleArea = {};
//アメリカ・カナダ・中米
var m_inttourModuleDp_errorCheckNumberOfPeopleArea_AME = {
	// 大人+子供(ベッドあり)
	'0':{
		'0': false
	},
	'1':{
		// 子供(ベッドなし)+幼児
		'0': false,
		'1': false
	},
	'2':{
		'0': false,
		'1': false,
		'2': true
	},
	'3':{
		'0': false,
		'1': false,
		'2': true,
		'3': true
	},
	'4':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true
	},
	'5':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true
	},
	'6':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true,
		'6': true
	}
};
//ハワイ
var m_inttourModuleDp_errorCheckNumberOfPeopleArea_HWA = {
	// 大人+子供(ベッドあり)
	'0':{
		'0': false
	},
	'1':{
		// 子供(ベッドなし)+幼児
		'0': false,
		'1': false
	},
	'2':{
		'0': false,
		'1': false,
		'2': false
	},
	'3':{
		'0': false,
		'1': false,
		'2': true,
		'3': true
	},
	'4':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true
	},
	'5':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true
	},
	'6':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true,
		'6': true
	}
};
// グラム・パラオ
var m_inttourModuleDp_errorCheckNumberOfPeopleArea_GP = {
		// 大人+子供(ベッドあり)
		'0':{
			'0': false
		},
		'1':{
			// 子供(ベッドなし)+幼児
			'0': false,
			'1': false
		},
		'2':{
			'0': false,
			'1': false,
			'2': false
		},
		'3':{
			'0': false,
			'1': false,
			'2': true,
			'3': true
		},
		'4':{
			'0': false,
			'1': false,
			'2': false,
			'3': true,
			'4': true
		},
		'5':{
			'0': false,
			'1': false,
			'2': false,
			'3': true,
			'4': true,
			'5': true
		},
		'6':{
			'0': false,
			'1': false,
			'2': false,
			'3': true,
			'4': true,
			'5': true,
			'6': true
		}
	};
//ヨーロッパ
var m_inttourModuleDp_errorCheckNumberOfPeopleArea_EUR = {
	// 大人+子供(ベッドあり)
	'0':{
		'0': false
	},
	'1':{
		// 子供(ベッドなし)+幼児
		'0': false,
		'1': false
	},
	'2':{
		'0': false,
		'1': false,
		'2': true
	},
	'3':{
		'0': false,
		'1': false,
		'2': true,
		'3': true
	},
	'4':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true
	},
	'5':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true
	},
	'6':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true,
		'6': true
	}
};
// アジア
var m_inttourModuleDp_errorCheckNumberOfPeopleArea_ASIA = {
	// 大人+子供(ベッドあり)
	'0':{
		'0': false
	},
	'1':{
		// 子供(ベッドなし)+幼児
		'0': false,
		'1': false
	},
	'2':{
		'0': false,
		'1': false,
		'2': true
	},
	'3':{
		'0': false,
		'1': false,
		'2': true,
		'3': true
	},
	'4':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true
	},
	'5':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true
	},
	'6':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true,
		'6': true
	}
};
//中国
m_inttourModuleDp_errorCheckNumberOfPeopleArea_CN = {
	// 大人+子供(ベッドあり)
	'0':{
		'0': false
	},
	'1':{
		// 子供(ベッドなし)+幼児
		'0': false,
		'1': false
	},
	'2':{
		'0': false,
		'1': false,
		'2': true
	},
	'3':{
		'0': false,
		'1': false,
		'2': true,
		'3': true
	},
	'4':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true
	},
	'5':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true
	},
	'6':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true,
		'6': true
	}
};
//オセアニア
var m_inttourModuleDp_errorCheckNumberOfPeopleArea_OC = {
	// 大人+子供(ベッドあり)
	'0':{
		'0': false
	},
	'1':{
		// 子供(ベッドなし)+幼児
		'0': false,
		'1': false
	},
	'2':{
		'0': false,
		'1': false,
		'2': true
	},
	'3':{
		'0': false,
		'1': false,
		'2': true,
		'3': true
	},
	'4':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true
	},
	'5':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true
	},
	'6':{
		'0': false,
		'1': false,
		'2': false,
		'3': true,
		'4': true,
		'5': true,
		'6': true
	}
};
// エリア紐付け
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='3'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_AME;    // アメリカ
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='1'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_HWA;    // ハワイ
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='2'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_GP;     // グラム・パラオ
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='6'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_EUR;    // ヨーロッパ
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='5'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_ASIA;   // アジア
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='4'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_CN;     // 中国
m_inttourModuleDp_errorCheckNumberOfPeopleArea[m_tourModuleAirportList_ArrAreaList['PD_813'].filter(function(e){return e.key==='7'})[0].value] = m_inttourModuleDp_errorCheckNumberOfPeopleArea_OC;     // オセアニア


/*=====================================================
  サジェスト用定義（海外ツアー共通・日本語）
=====================================================*/
// サジェスト設定用定義
var m_inttourModulePkg_suggestSetting = {
	startingCharacterCount : 1		// サジェスト起動文字数
};


/*=====================================================
  サジェスト用文言定義（海外ツアー・日本語）
=====================================================*/
var m_inttourModuleTopPkg_strategiesLang = {
	dispLabel: '<%= course_basic_name_jpn.substring(0, 36) %>' //ラベルの書式フォーマット
}
var m_inttourModulePkg_strategiesLang = {
	type: 'ja',
	uniq: 'course_base_cd',
	label: '<%= course_basic_name_jpn %>', //ラベルの書式フォーマット
	dispLabel: '<%= course_basic_name_jpn.substring(0, 32) %>', //ラベルの書式フォーマット
	rule: function(text, obj) {
		var depApo = obj.target.depApo.val();
		var area = obj.target.area.val();
		var country = obj.target.country.val();
		var apo = obj.target.apo.val();
		var classFare = obj.target.classFare.val();
		var startingCharacterCount = m_inttourModulePkg_suggestSetting.startingCharacterCount;
		var startReg1 = new RegExp('^[^ -~｡-ﾟ]{' + startingCharacterCount + ',}.*$', 'g');
		var startReg2 = new RegExp('^[\x20-\x7E]{' + startingCharacterCount + ',}.*$', 'g');
		if (text.match(startReg1) || text.match(startReg2)) {
			var self = this;
			var t = text.replace(/(\(|\)|\[|\]|\\|\^|\+|\*|\?)/g, '\\$&');
			var r = new RegExp(t, 'i');

			// 出発地（都市）が一致する定義情報を絞込み
			var depCityFilterResult = _(this.dictionary).chain().filter(function(w) {
				if (!_.isEmpty(depApo) && !_.isEmpty(w['dept_airport'])) {
					if (depApo === w['dept_airport']) {
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			});
			// 目的地（地域・国・都市）が一致する定義情報を絞込み
			var cityFilterResult = depCityFilterResult.chain().filter(function(w) {
				if (!_.isEmpty(area) && !_.isEmpty(w['area_cd'])) {
					if (area === w['area_cd']) {
						if (!_.isEmpty(country) && !_.isEmpty(w['country_cd'])) {
							if ($tour_module_jq.inArray(country, w['country_cd']) > -1) {
								if (!_.isEmpty(apo) && !_.isEmpty(w['city_cd'])) {
									if ($tour_module_jq.inArray(apo, w['city_cd']) > -1) {
										return true;
									} else {
										return false;
									}
								} else {
									return true;
								}
							} else {
								return false;
							}
						} else {
							return true;
						}
					} else {
						return false;
					}
				} else {
					return true;
				}
			});
			// 座席クラスが一致する定義情報を絞込み
			var classFareFilterResult = cityFilterResult.chain().filter(function(w) {
				if (!_.isEmpty(classFare) && !_.isEmpty(w['cabin_class'])) {
					if (classFare === w['cabin_class']) {
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			});
			// 入力文字に一致する定義情報を絞込み
			var filterResult = classFareFilterResult.chain().filter(function(w) {
				if (w['course_basic_name_jpn'] && w['course_basic_name_jpn'].match(r)) {
					return true;
				} else {
					return false;
				}
			}).value();
			// 絞込んだ定義情報をpriority属性でソート
			this.sortDisplayList(filterResult);
			// ソート後の定義情報から最初の10件だけ取得
			filterResult = this.getFirstFewDatas(filterResult, 10);
			return filterResult;
		} else {
			return false;
		}
	},
	dictionary: m_inttourModulePkg_TourNameJson['data']
};


/*=====================================================
  空港一覧用文言定義（国際線WWS・日本語）
=====================================================*/
var m_tourModuleParts_airportListTableLanguageProperties = {
  modalTitle: {dep:'出発地を選択してください。',arr: '到着地を選択してください。'}
 ,majorAirport: '主要な空港'
 ,closeButton: {src: '/module/air-booking/image/common/d_btn_close.png' ,alt: 'closeAlt' }
};

/*=====================================================
カレンダー用文言定義
=====================================================*/

var m_tourModuleCalender_PCLP = new m_tourModulePopupCalendarLanguageProperties();

//ヘッダー文言
m_tourModuleCalender_PCLP.HeadFormat = '日付を選択してください。';

//月リスト
m_tourModuleCalender_PCLP.MonthList[0] = '&nbsp;<span>1</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[1] = '&nbsp;<span>2</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[2] = '&nbsp;<span>3</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[3] = '&nbsp;<span>4</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[4] = '&nbsp;<span>5</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[5] = '&nbsp;<span>6</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[6] = '&nbsp;<span>7</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[7] = '&nbsp;<span>8</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[8] = '&nbsp;<span>9</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[9] = '&nbsp;<span>10</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[10] = '&nbsp;<span>11</span>&nbsp;月';
m_tourModuleCalender_PCLP.MonthList[11] = '&nbsp;<span>12</span>&nbsp;月';

//曜日リスト
m_tourModuleCalender_PCLP.WeekList = new Array('日','月','火','水','木','金','土');

//前の月ボタン
m_tourModuleCalender_PCLP.PrevMonthFormat = '前へ';

//次の月ボタン
m_tourModuleCalender_PCLP.NextMonthFormat = '次へ';

//閉じるボタン
m_tourModuleCalender_PCLP.CloseButtonFormat = '閉じる';

//出発日の日付書式
m_tourModuleCalender_PCLP.FormatDate = function(baseDate) {
	var YYYY = baseDate.getFullYear();
	var MM = (baseDate.getMonth() + 1);
	var DD = baseDate.getDate();
	var W = this.WeekList[baseDate.getDay()]; // 曜日
	return YYYY + '年' + MM + '月' + DD + '日（' + W + '）';
};

//年月書式(HTML)
m_tourModuleCalender_PCLP.FormatYearMonth = function(baseDate) {
	return baseDate.getFullYear() + '年' + this.FormatMonth(baseDate);
};

//年月書式
m_tourModuleCalender_PCLP.FormatMonthText = function(baseDate) {
	return baseDate.getFullYear() + '年' + (baseDate.getMonth() + 1) + '月';
};

//日付加算
//yyyymmdd形式の年月日に指定した日数を加算してDate型のオブジェクトを返す
m_tourModuleCalender_PCLP.SubtractDate = function(targetDate, days) {
	var targetYear = parseInt(targetDate.substring(0, 4));
	var targetMonth = parseInt(targetDate.substring(4, 6));
	var targetDay = parseInt(targetDate.substring(6, 8));
	var resultDate = new Date(targetYear, targetMonth - 1, targetDay + days);

	return resultDate;
};

//yyyymmdd形式の年月日をDate型のオブジェクトに変換して返す
m_tourModuleCalender_PCLP.GetDateObject = function(targetDate) {
	var targetYear = parseInt(targetDate.substring(0, 4));
	var targetMonth = parseInt(targetDate.substring(4, 6));
	var targetDay = parseInt(targetDate.substring(6, 8));
	var resultDate = new Date(targetYear, targetMonth - 1, targetDay);

	return resultDate;
};

//Date型オブジェクトをYYYYMMDD形式の文字列で返す
m_tourModuleCalender_PCLP.GetDateText = function(targetDate) {
	var YYYY = targetDate.getFullYear();
	var MM = ('0' + (targetDate.getMonth() + 1)).slice(-2); // 2桁にゼロ埋めした月
	var DD = ('0' + targetDate.getDate()).slice(-2); // 2桁にゼロ埋めした日

	return YYYY + MM + DD;
};

//デフォルト日付を取得
m_tourModuleCalender_PCLP.calcDefDate = function(options) {
	var defDate;
	var defDateText;
	var midDaysFromStartDate = m_tourModuleCommon_getParam(m_tourModuleCalender_siteType, options, 'midDaysFromStartDate');
	var retDaysFromStartDate = m_tourModuleDateUtil.prototype.calcRetDaysFromStartDate(options);
	var currentDate = m_tourModuleDateUtil.prototype.calcCurrentDate(options.boundaryTime);

	if (options.startDateClass) {
		var startDateClass = options.parent.find('.' + options.startDateClass)
		var startDateVal;
		if (startDateClass.length > 0) {
			startDateVal = startDateClass.val();
			var startDateText = startDateVal.substring(0, 4) + '/'
				+ startDateVal.substring(4, 6) + '/' + startDateVal.substring(6, 8);
			if (m_tourModuleCalendar_isValidDate(startDateText)) {
				startDate = new Date(startDateText);
			} else {
				startDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + options.defSelectDay);
			}
		} else {
			startDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + options.defSelectDay);
		}
		if (options.middleDateClass) {
			// 帰り
			defDate = new Date(startDate.getFullYear(), startDate.getMonth(),
				startDate.getDate() + retDaysFromStartDate);
			
			//移動日の考慮
			var moveDate =options.targetObject.parents('form').find('.m_moveDate');
			if(undefined != moveDate.val()){
				var moveNextDate = m_tourModuleCalender_PCLP.SubtractDate(moveDate.val(), 1);
				if(defDate.getTime()>moveNextDate.getTime()){
					//移動日の翌日が選択可能期間外だった場合はデフォルトの初期値を設定
				}else{
					//移動日の翌日が選択可能期間だった場合は移動日の翌日を設定
					defDate = moveNextDate;
				}
			}else{
				//移動日が取得できない場合は移動日を考慮しない
			}
			
		} else if (options.retDateClass) {
			// 移動日
			defDate = new Date(startDate.getFullYear(), startDate.getMonth(),
				startDate.getDate() + midDaysFromStartDate);
		} else {
			// 移動日無し最終日
			defDate = new Date(startDate.getFullYear(), startDate.getMonth(),
				startDate.getDate() + retDaysFromStartDate);
		}
	} else {
		defDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + options.defSelectDay);
	}
	return defDate;
};

/*=====================================================
  カレンダー部品　エリア別設定情報定義（海外ツアー・旅作・日本語）
=====================================================*/
// 1.カレンダー表示月制御日
var m_inttourModuleDp_dispDays = 300;

// 2.カレンダー選択可能制御日
var m_inttourModuleDp_selectableDays = 300;

// 3.カレンダー初期選択日
var m_inttourModuleDp_defaultSelectedFromCurrent = 30;

// 4.受付終了日数
var m_inttourModuleDp_receiptDisableDays = {
	// ハワイ
	'01':17,
	// グァム・パラオ
	'02':18,
	// アメリカ・カナダ・中米
	'03':6,
	// 中国
	'04':6,
	// アジア
	'05':6,
	// ヨーロッパ
	'06':6,
	// オセアニア
	'07':6,
	// デフォルト
	'default':18
};

// 5.カレンダー受付終了判別起点時間
var m_inttourModuleDp_currentDateBaseTime = '17:00';

// 6.受付可能最大旅行日数（帰り）
var m_inttourModuleDp_maxTravelDays = {
	// ハワイ
	'01':28,
	// グァム・パラオ
	'02':10,
	// アメリカ・カナダ・中米
	'03':28,
	// 中国
	'04':28,
	// アジア
	'05':28,
	// ヨーロッパ
	'06':28,
	// オセアニア
	'07':28,
	// デフォルト
	'default':10
};

// 7.移動日の選択期間
var m_inttourModuleDp_moveDaysFromDepartureDate = 1;

// 8.帰り選択日（行きの出発日起点）
var m_inttourModuleDp_returnDaysFromDepartureDate = {
	'area':{
		// ヨーロッパ
		'06':3
	},
	'country':{
	},
	'city':{
		// メキシコシティ
		'MEX':3
	},
	// デフォルト
	'default':2
};


/*=====================================================
  カレンダー部品　エリア別設定情報定義（海外ツアー・パッケージツアー・日本語）
=====================================================*/
// 1.カレンダー表示月制御日
var m_inttourModulePkg_dispDays = 300;

// 2.カレンダー選択可能制御日
var m_inttourModulePkg_selectableDays = 300;

// 3.カレンダー初期選択日
var m_inttourModulePkg_defaultSelectedFromCurrent = 60;

// 4.受付終了日数
var m_inttourModulePkg_receiptDisableDays = 9;

// 5.カレンダー受付終了判別起点時間
var m_inttourModulePkg_currentDateBaseTime = '24:00';

// 6.帰り選択日（行きの出発日起点）
var m_inttourModulePkg_returnDaysFromDepartureDate = 2;

/*=====================================================
  カレンダー部品　エリア別設定情報定義（海外ツアー・オプション・日本語）
=====================================================*/
// 1.カレンダー表示月制御日
var m_inttourModuleOption_dispDays = 364;

// 2.カレンダー選択可能制御日
var m_inttourModuleOption_selectableDays = 364;

// 3.カレンダー初期選択日
var m_inttourModuleOption_defaultSelectedFromCurrent = 1;

// 4.受付終了日数
var m_inttourModuleOption_receiptDisableDays = 0;

// 5.カレンダー受付終了判別起点時間
var m_inttourModuleOption_currentDateBaseTime = '24:00';


