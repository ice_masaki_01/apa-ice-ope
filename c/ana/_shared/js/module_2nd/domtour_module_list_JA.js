/*=====================================================
* Script Name: domtour_module_list_JA.js
* Description: 空席照会モジュール 国内ツアー共通 定義リスト（日本語）
* Version: 1.00
* Last Up Date: 2017/10/13
=====================================================*/

//--------------------------------------------------------------------検索遷移先
var m_domtourModuleDp_fromAction = {
	'URL':'/domtour/booking/csm/search/DSCP0390/init'
};
var m_domtourModulePkg_fromAction = {
	'URL':'/domtour/booking/csm/search/DSBP0010/init'
};
var m_domtourModulePkg_fromAction_Course = {
	'URL':'/domtour/booking/csm/search/DSBP0020/init'
};
var m_domtourModuleHotel_fromAction = {
	'URL':'/domtour/booking/csm/search/DSDP0010/init'
};
var m_domtourModuleCar_fromAction = {
	'URL':'/domtour/booking/csm/search/DSDP0150/init'
};
var m_domtourModuleTraffic_fromAction = {
	1:'/domtour/booking/csm/search/DSDP0110/init',
	2:'/domtour/booking/csm/search/DSDP0120/init',
	3:'/domtour/booking/csm/search/DSDP0140/init',
	4:'/domtour/booking/csm/search/DSDP0130/init',
	5:'/domtour/booking/csm/search/DSDP0100/init',
	6:'/domtour/booking/csm/search/DSDP0080/init',
};
var m_domtourModuleBulk_fromAction = {
	'URL':'/domtour/booking/csm/search/DSAP0010/init'
};

var m_domtourModuleHotelBulk_fromAction = {
	'URL':'/domtour/booking/csm/search/DSGP0010/init'
};

//--------------------------------------------------------------------検索ボタン
var m_domtourModuleCommon_searchButton = {
	'Text':'検索する',
	'SiteCatalystId_dp':'pc_jp_looks_domtour_tabisaku_module_btn_serch',
	'SiteCatalystId_domdp':'pc_jp_looks_domtour_tabisaku_module_btn_serch',
	'SiteCatalystId_pkg':'pc_jp_looks_domtour_tour_module_btn_serch',
	'SiteCatalystId_hotel_min':'pc_jp_looks_domtour_hotel_module_btn_serch_min',
	'SiteCatalystId_hotel':'pc_jp_looks_domtour_hotel_module_btn_serch',
	'SiteCatalystId_car':'pc_jp_looks_domtour_rentalcar_module_btn_serch',
	'SiteCatalystId_traffic':'#',
};

//--------------------------------------------------------------------「旅作」 フライト
var m_domtourModuleDp_flight = {
	'Text':'フライト'
};
//--------------------------------------------------------------------「旅作」 行き
var m_domtourModuleDp_dep = {
	'Text':'行き'
};
//--------------------------------------------------------------------「旅作」 帰り
var m_domtourModuleDp_arr = {
	'Text':'帰り'
};
//--------------------------------------------------------------------「旅作」 フライト 行き
var m_domtourModuleDp_depFlight = {
	'Text':'フライト　行き'
};

//--------------------------------------------------------------------「旅作」 フライト 行き 搭乗日
var m_domtourModuleDp_depFlightDepDate = {
	'Text':'搭乗日',
	'BalloonTitle':'搭乗日'
};

//--------------------------------------------------------------------「旅作」 フライト 行き 出発地
var m_domtourModuleDp_depFlightDepApo = {
	'Text':'出発地'
};

//--------------------------------------------------------------------「旅作」 フライト 行き 到着地
var m_domtourModuleDp_depFlightArrApo = {
	'Text':'到着地'
};

//--------------------------------------------------------------------「旅作」 フライト 帰り
var m_domtourModuleDp_arrFlight = {
	'Text':'フライト　帰り'
};

//--------------------------------------------------------------------「旅作」 フライト 帰り 搭乗日
var m_domtourModuleDp_arrFlightDepDate = {
	'Text':'搭乗日',
	'BalloonTitle':'搭乗日'
};

//--------------------------------------------------------------------「旅作」 フライト 帰り 出発地
var m_domtourModuleDp_arrFlightDepApo = {
	'Text':'出発地'
};

//--------------------------------------------------------------------「旅作」 フライト 帰り 到着地
var m_domtourModuleDp_arrFlightArrApo = {
	'Text':'到着地'
};

//--------------------------------------------------------------------「旅作」 参加人数
var m_domtourModuleDp_numberOfPeople = {
	'Text':'参加人数'
};

//--------------------------------------------------------------------「旅作」 大人
var m_domtourModuleDp_adult = {
	'Text':'大人'
};

//--------------------------------------------------------------------「旅作」 子供・幼児
var m_domtourModuleDp_child = {
	'Text':'子供・幼児'
};

//--------------------------------------------------------------------「旅作」 宿泊
var m_domtourModuleDp_stay = {
	'Text':'宿泊'
};

//--------------------------------------------------------------------「旅作」 宿泊地
var m_domtourModuleDp_placeOfStay = {
	'Text':'宿泊地'
};

//--------------------------------------------------------------------「旅作」 チェックイン/チェックアウト
var m_domtourModuleDp_check = {
	'Text1':'チェック',
	'Text2':'イン',
	'Text3':'アウト',
	'BalloonTitle1':'チェックイン',
	'BalloonTitle2':'チェックアウト'
};

//--------------------------------------------------------------------「旅作」 レンタカー
var m_domtourModuleDp_car = {
	'Text1':'レンタカー',
	'Text2':'レンタカーも合せて予約する',
	'URL':'https://www.ana.co.jp/be/travel/domtour/con_012.html'
};
//--------------------------------------------------------------------「パッケージ」 新旧モジュール切り替えラベル
var m_domtourModulePkg_moduleTabText = {
	'Text1':'2018年',
	'Text2':'10月31日以前',
	'Text3':'11月1日以降',
	'Text4':'ご出発の方',
};
//--------------------------------------------------------------------「パッケージ」 出発日
var m_domtourModulePkg_depDate = {
	'Text':'出発日',
	'BalloonTitle':'出発日'
};

//--------------------------------------------------------------------「パッケージ」 出発地
var m_domtourModulePkg_depApo = {
	'Text':'出発地'
};

//--------------------------------------------------------------------「パッケージ」 目的地
var m_domtourModulePkg_arrApo = {
	'Text':'目的地'
};

//--------------------------------------------------------------------「パッケージ」 旅行日数
var m_domtourModulePkg_travelOfDays = {
	'Text':'旅行日数'
};

//--------------------------------------------------------------------「パッケージ」 1部屋あたりの利用人数
var m_domtourModulePkg_roomOfPeopleLabel = {
	'Text':'1部屋あたりの利用人数'
};

//--------------------------------------------------------------------「パッケージ」  大人
var m_domtourModulePkg_adult = {
	'Text':'大人'
};

//--------------------------------------------------------------------「パッケージ」  子供・幼児
var m_domtourModulePkg_child = {
	'Text':'子供・幼児'
};

//--------------------------------------------------------------------「ホテル」 チェックイン/チェックアウト
var m_domtourModuleHotel_check = {
	'Text1':'チェック',
	'Text2':'イン',
	'Text3':'アウト',
	'BalloonTitle1':'チェックイン',
	'BalloonTitle2':'チェックアウト'
};

//--------------------------------------------------------------------「ホテル」 宿泊地
var m_domtourModuleHotel_placeOfStay = {
	'Text':'宿泊地'
};

//--------------------------------------------------------------------「ホテル」 1部屋あたりの利用人数
var m_domtourModuleHotel_roomOfPeopleLabel = {
	'Text':'1部屋あたりの利用人数'
};

//--------------------------------------------------------------------「ホテル」  大人
var m_domtourModuleHotel_adult = {
	'Text':'大人'
};

//--------------------------------------------------------------------「ホテル」  子供・幼児
var m_domtourModuleHotel_child = {
	'Text':'子供・幼児',
	'Caution':'※大人2名以上で選択可'
};

//--------------------------------------------------------------------「ホテル」  こだわり条件
var m_domtourModuleHotel_addCondition = {
	'Text1':'こだわり',
	'Text2':'条件'
};


//--------------------------------------------------------------------「レンタカー」  配車日時
var m_domtourModuleCar_firstDateCar = {
	'Text':'配車日時',
	'BalloonTitle':'配車日'
		
};

//--------------------------------------------------------------------「レンタカー」  返車日時
var m_domtourModuleCar_lastDateCar = {
	'Text':'返車日時',
	'BalloonTitle':'返車日'
};

//--------------------------------------------------------------------「レンタカー」  配車場所
var m_domtourModuleCar_firstPlaceCar = {
	'Text':'配車場所',
	'SubText1':'空港エリア',
	'SubText2':'市内エリア'
};

//--------------------------------------------------------------------「レンタカー」  返車場所
var m_domtourModuleCar_lastPlaceCar = {
	'Text':'返車場所',
	'SubText1':'貸出店舗',
	'SubText2':'空港営業所',
	'SubText3':'市内営業所'
};

//--------------------------------------------------------------------「レンタカー」  車両クラスから検索
var m_domtourModuleCar_carClassLabel = {
	'Text1':'車両クラス',
	'Text2':''
};

//--------------------------------------------------------------------「観光」  利用日
var m_domtourModuleTraffic_trafficDate = {
	'Text':'利用日',
	'BalloonTitle':'利用日'
};

//--------------------------------------------------------------------「観光」  目的地
var m_domtourModuleTraffic_trafficPlace = {
	'Text':'目的地'
};

//--------------------------------------------------------------------「観光」  目的
var m_domtourModuleTraffic_trafficPurpose = {
	'Text':'目的'
};

//--------------------------------------------------------------------「ホテル(3社一括)」 ご利用日
var m_domtourModuleHotelBulk_useDays = {
		'Text':'ご利用日',
		'BalloonTitle':'ご利用日',
};

//--------------------------------------------------------------------「ホテル(3社一括)」 泊数
var m_domtourModuleHotelBulk_stayDays = {
		'Text':'泊数'
};

//--------------------------------------------------------------------「ホテル(3社一括)」 宿泊地
var m_domtourModuleHotelBulk_placeOfStay = {
	'Text':'宿泊地'
};

//--------------------------------------------------------------------「ホテル(3社一括)」 1部屋あたりの利用人数
var m_domtourModuleHotelBulk_roomOfPeopleLabel = {
	'Text':'1部屋あたりの利用人数'
};

//--------------------------------------------------------------------「ホテル(3社一括)」  大人
var m_domtourModuleHotelBulk_people = {
	'Text':'1部屋あたりの利用人数'
};

//--------------------------------------------------------------------「ホテル(3社一括)」  こだわり条件
var m_domtourModuleHotelBulk_addCondition = {
	'Text1':'こだわり',
	'Text2':'条件'
};

//--------------------------------------------------------------------旅行日数 パッケージ
m_domtourModulePkg_travelOfDaysList = [
	{text:'2日',value:'2'},
	{text:'3日',value:'3'},
	{text:'4日',value:'4'},
	{text:'5日',value:'5'},
	{text:'6日',value:'6'},
	{text:'7日',value:'7'},
	{text:'8日',value:'8'},
	{text:'9日',value:'9'},
	{text:'10日',value:'10'},
	{text:'11日',value:'11'},
	{text:'12日',value:'12'},
	{text:'13日',value:'13'},
	{text:'14日',value:'14'}
];

//--------------------------------------------------------------------旅行日数 ツアー一括
m_domtourModuleBulk_travelOfDaysList = [
	{text:'2日',value:'1'},
	{text:'3日',value:'2'},
	{text:'4日',value:'3'},
	{text:'5日',value:'4'},
	{text:'6日',value:'5'},
	{text:'7日',value:'6'},
	{text:'8日',value:'7'},
	{text:'9日',value:'8'},
	{text:'10日',value:'9'},
	{text:'11日',value:'10'},
	{text:'12日',value:'11'},
	{text:'13日',value:'12'},
	{text:'14日',value:'13'}
];

//--------------------------------------------------------------------「ホテル(3社一括)」 泊数
m_domtourModuleHotelBulk_stayDaysList = [
	{text:'1泊',value:'1'},
	{text:'2泊',value:'2'},
	{text:'3泊',value:'3'},
	{text:'4泊',value:'4'},
	{text:'5泊',value:'5'},
	{text:'6泊',value:'6'},
	{text:'7泊',value:'7'},
	{text:'8泊',value:'8'},
	{text:'9泊',value:'9'},
	{text:'10泊',value:'10'},
	{text:'11泊',value:'11'},
	{text:'12泊',value:'12'},
	{text:'13泊',value:'13'}
];

//--------------------------------------------------------------------大人 旅作
m_domtourModuleDp_PeoplesPulldownList = [
	{text:'1名(12歳以上)',value:'1'},
	{text:'2名(12歳以上)',value:'2'},
	{text:'3名(12歳以上)',value:'3'},
	{text:'4名(12歳以上)',value:'4'},
	{text:'5名(12歳以上)',value:'5'},
	{text:'6名(12歳以上)',value:'6'}
];

//--------------------------------------------------------------------大人 パッケージ
m_domtourModulePkg_PeoplesPulldownList = [
	{text:'1名(12歳以上)',value:'1'},
	{text:'2名(12歳以上)',value:'2'},
	{text:'3名(12歳以上)',value:'3'},
	{text:'4名(12歳以上)',value:'4'},
	{text:'5名(12歳以上)',value:'5'},
	{text:'6名(12歳以上)',value:'6'},
	{text:'7名(12歳以上)',value:'7'},
	{text:'8名(12歳以上)',value:'8'},
	{text:'9名(12歳以上)',value:'9'}
];

//--------------------------------------------------------------------大人 ホテル
m_domtourModuleHotel_PeoplesPulldownList = [
	{text:'1名(12歳以上)',value:'1'},
	{text:'2名(12歳以上)',value:'2'},
	{text:'3名(12歳以上)',value:'3'},
	{text:'4名(12歳以上)',value:'4'},
	{text:'5名(12歳以上)',value:'5'},
	{text:'6名(12歳以上)',value:'6'}
];

//--------------------------------------------------------------------人数 ホテル
m_domModuleHotel_PeoplesPulldownList = [
	{text:'1名1室',value:'1'},
	{text:'2名1室',value:'2'},
	{text:'3名1室',value:'3'},
	{text:'4名1室',value:'4'},
	{text:'5名1室',value:'5'},
	{text:'6名1室～',value:'6'}
];

//--------------------------------------------------------------------人数 ツアー一括
m_domtourModuleBulk_PeoplesPulldownList = [
	{text:'1名1室',value:'1'},
	{text:'2名1室',value:'2'},
	{text:'3名1室',value:'3'},
	{text:'4名1室',value:'4'},
	{text:'5名1室',value:'5'},
	{text:'6名1室',value:'6'}
];

//--------------------------------------------------------------------目的
m_domtourModule_PurposePulldownList = [
	{text:'観光・体験・レジャー',value:'1'},
	{text:'ゴルフ',value:'2'},
	{text:'食事',value:'3'},
	{text:'JRフリーパス',value:'4'},
	{text:'バス・鉄道・船舶',value:'5'},
	{text:'タクシー',value:'6'}
];

//--------------------------------------------------------------------時間 レンタカー
m_domtourModule_TimePulldownList = [
	{text:'00:00',value:'0000'},
	{text:'00:15',value:'0015'},
	{text:'00:30',value:'0030'},
	{text:'00:45',value:'0045'},
	{text:'01:00',value:'0100'},
	{text:'01:15',value:'0115'},
	{text:'01:30',value:'0130'},
	{text:'01:45',value:'0145'},
	{text:'02:00',value:'0200'},
	{text:'02:15',value:'0215'},
	{text:'02:30',value:'0230'},
	{text:'02:45',value:'0245'},
	{text:'03:00',value:'0300'},
	{text:'03:15',value:'0315'},
	{text:'03:30',value:'0330'},
	{text:'03:45',value:'0345'},
	{text:'04:00',value:'0400'},
	{text:'04:15',value:'0415'},
	{text:'04:30',value:'0430'},
	{text:'04:45',value:'0445'},
	{text:'05:00',value:'0500'},
	{text:'05:15',value:'0515'},
	{text:'05:30',value:'0530'},
	{text:'05:45',value:'0545'},
	{text:'06:00',value:'0600'},
	{text:'06:15',value:'0615'},
	{text:'06:30',value:'0630'},
	{text:'06:45',value:'0645'},
	{text:'07:00',value:'0700'},
	{text:'07:15',value:'0715'},
	{text:'07:30',value:'0730'},
	{text:'07:45',value:'0745'},
	{text:'08:00',value:'0800'},
	{text:'08:15',value:'0815'},
	{text:'08:30',value:'0830'},
	{text:'08:45',value:'0845'},
	{text:'09:00',value:'0900'},
	{text:'09:15',value:'0915'},
	{text:'09:30',value:'0930'},
	{text:'09:45',value:'0945'},
	{text:'10:00',value:'1000'},
	{text:'10:15',value:'1015'},
	{text:'10:30',value:'1030'},
	{text:'10:45',value:'1045'},
	{text:'11:00',value:'1100'},
	{text:'11:15',value:'1115'},
	{text:'11:30',value:'1130'},
	{text:'11:45',value:'1145'},
	{text:'12:00',value:'1200'},
	{text:'12:15',value:'1215'},
	{text:'12:30',value:'1230'},
	{text:'12:45',value:'1245'},
	{text:'13:00',value:'1300'},
	{text:'13:15',value:'1315'},
	{text:'13:30',value:'1330'},
	{text:'13:45',value:'1345'},
	{text:'14:00',value:'1400'},
	{text:'14:15',value:'1415'},
	{text:'14:30',value:'1430'},
	{text:'14:45',value:'1445'},
	{text:'15:00',value:'1500'},
	{text:'15:15',value:'1515'},
	{text:'15:30',value:'1530'},
	{text:'15:45',value:'1545'},
	{text:'16:00',value:'1600'},
	{text:'16:15',value:'1615'},
	{text:'16:30',value:'1630'},
	{text:'16:45',value:'1645'},
	{text:'17:00',value:'1700'},
	{text:'17:15',value:'1715'},
	{text:'17:30',value:'1730'},
	{text:'17:45',value:'1745'},
	{text:'18:00',value:'1800'},
	{text:'18:15',value:'1815'},
	{text:'18:30',value:'1830'},
	{text:'18:45',value:'1845'},
	{text:'19:00',value:'1900'},
	{text:'19:15',value:'1915'},
	{text:'19:30',value:'1930'},
	{text:'19:45',value:'1945'},
	{text:'20:00',value:'2000'},
	{text:'20:15',value:'2015'},
	{text:'20:30',value:'2030'},
	{text:'20:45',value:'2045'},
	{text:'21:00',value:'2100'},
	{text:'21:15',value:'2115'},
	{text:'21:30',value:'2130'},
	{text:'21:45',value:'2145'},
	{text:'22:00',value:'2200'},
	{text:'22:15',value:'2215'},
	{text:'22:30',value:'2230'},
	{text:'22:45',value:'2245'},
	{text:'23:00',value:'2300'},
	{text:'23:15',value:'2315'},
	{text:'23:30',value:'2330'},
	{text:'23:45',value:'2345'}

];

//--------------------------------------------------------------------車両クラス
m_domtourModule_CarClassList = [
	{text:'乗用車',id:'dom_car_class1'},
	{text:'ミニバン・ワゴン',id:'dom_car_class2'},
	{text:'エコカー',id:'dom_car_class3'},
	{text:'軽自動車',id:'dom_car_class4'}
];

//--------------------------------------------------------------------車両クラス送信対応パラメータ
m_domtourModule_CarClassParamList = {
	dom_car_class1:'S,SA,A,B,HC,HD',
	dom_car_class2:'WS,WH,TW,WA,WB,WC',
	dom_car_class3:'E,EA,EB,EV,EW',
	dom_car_class4:'K'
};

m_domtourModule_CarClassInverseParamList = {
		S:'dom_car_class1',
		WS:'dom_car_class2',
		E:'dom_car_class3',
		K:'dom_car_class4'
	};
//--------------------------------------------------------------------ホテル こだわり条件
m_domtourModule_HotelOptionList = [
	{text:'朝食あり',id:'BREAKFAST'},
	{text:'食事なし',id:'NOMEAL'},
	{text:'禁煙ルーム',id:'NONSMOKING'},
	{text:'プラスマイルあり',id:'MILE'}
];

//--------------------------------------------------------------------ホテル(3社一括) こだわり条件
m_domtourModule_HotelBulkOptionList = [
	{text:'朝食あり',id:'BREAKFAST'},
	{text:'食事なし',id:'NOMEAL'},
	{text:'禁煙ルーム',id:'NONSMOKING'},
	{text:'プラスマイルあり',id:'MILE'}
];

/*=====================================================
カレンダー部品設定（国内旅作 行き 搭乗日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleDp_DepDate_defSelectDay = 7;
//データ送信用のClass名
var m_domtourModuleDp_DepDate_dateValueClass = 'm_domDpDepDate';
//データ送信用のname名
var m_domtourModuleDp_DepDate_dateValueName = 'goDeptDt';
//プレースホルダ表示テキスト
var m_domtourModuleDp_DepDate_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleDp_DepDate_inputClass = 'm_domDpDepDateText';
//必須チェックデザイン設定
var m_domtourModuleDp_DepDate_isRequired = false;
//活性非活性設定
var m_domtourModuleDp_DepDate_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleDp_DepDate_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleDp_DepDate_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleDp_DepDate_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleDp_DepDate_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleDp_DepDate_selectableMonths = 12;
//祝日判定
var m_domtourModuleDp_DepDate_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleDp_DepDate_boundaryTime = '17:00';
//部品表示位置
var m_domtourModuleDp_DepDate_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内旅作 帰り 搭乗日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleDp_arrDate_defSelectDay = 8;
//データ送信用のClass名
var m_domtourModuleDp_arrDate_dateValueClass = 'm_domDpRetDate';
//データ送信用のname名
var m_domtourModuleDp_arrDate_dateValueName = 'rtnDeptDt';
//プレースホルダ表示テキスト
var m_domtourModuleDp_arrDate_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleDp_arrDate_inputClass = 'm_domDpRetDateText';
//必須チェックデザイン設定
var m_domtourModuleDp_arrDate_isRequired = false;
//活性非活性設定
var m_domtourModuleDp_arrDate_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleDp_arrDate_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleDp_arrDate_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleDp_arrDate_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleDp_arrDate_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleDp_arrDate_selectableMonths = 12;
//祝日判定
var m_domtourModuleDp_arrDate_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleDp_arrDate_boundaryTime = '17:00';
//旅行期間FromのClass名
var m_domtourModuleDp_arrDate_startDateClass = 'm_domDpDepDate';
//受付可能最大旅行日数
var m_domtourModuleDp_arrDate_maxTravelDays = 13;
//FROM日との連動用設定
var m_domtourModuleDp_arrDate_returnDaysFromDepartureDate = 1;
//部品表示位置
var m_domtourModuleDp_arrDate_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内旅作 チェックイン日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleDp_checkin_defSelectDay = 7;
//データ送信用のClass名
var m_domtourModuleDp_checkin_dateValueClass = 'm_domDpCheInDate';
//データ送信用のname名
var m_domtourModuleDp_checkin_dateValueName = 'checkInDt';
//プレースホルダ表示テキスト
var m_domtourModuleDp_checkin_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleDp_checkin_inputClass = 'm_domDpCheInDateText';
//必須チェックデザイン設定
var m_domtourModuleDp_checkin_isRequired = false;
//活性非活性設定
var m_domtourModuleDp_checkin_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleDp_checkin_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleDp_checkin_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleDp_checkin_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleDp_checkin_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleDp_checkin_selectableMonths = 12;
//祝日判定
var m_domtourModuleDp_checkin_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleDp_checkin_boundaryTime = '17:00';
//受付可能最大旅行日数
var m_domtourModuleDp_checkin_maxTravelDays = -1;
//受付終了日付を個別にクラス指定する場合の変数設定
var m_domtourModuleDp_checkin_maxTravelDateClass = 'm_domDpRetDate';
//受付開始日付を個別にクラス指定する場合の変数設定
var m_domtourModuleDp_checkin_minTravelDateClass = 'm_domDpDepDate';
//部品表示位置
var m_domtourModuleDp_checkin_defPartsPosition_Top = 'right';
/*=====================================================
カレンダー部品設定（国内旅作 チェックアウト日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleDp_checkout_defSelectDay = 8;
//データ送信用のClass名
var m_domtourModuleDp_checkout_dateValueClass = 'm_domDpCheOutDate';
//データ送信用のname名
var m_domtourModuleDp_checkout_dateValueName = 'checkOutDt';
//プレースホルダ表示テキスト
var m_domtourModuleDp_checkout_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleDp_checkout_inputClass = 'm_domDpCheOutDateText';
//必須チェックデザイン設定
var m_domtourModuleDp_checkout_isRequired = false;
//活性非活性設定
var m_domtourModuleDp_checkout_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleDp_checkout_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleDp_checkout_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleDp_checkout_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleDp_checkout_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleDp_checkout_selectableMonths = 12;
//祝日判定
var m_domtourModuleDp_checkout_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleDp_checkout_boundaryTime = '17:00';
//旅行期間FromのClass名
var m_domtourModuleDp_checkout_startDateClass = 'm_domDpCheInDate';
//FROM日との連動用設定
var m_domtourModuleDp_checkout_returnDaysFromDepartureDate = 1;
//受付可能最大旅行日数
var m_domtourModuleDp_checkout_maxTravelDays = 0;
//受付終了日付を個別にクラス指定する場合の変数設定
var m_domtourModuleDp_checkout_maxTravelDateClass = 'm_domDpRetDate';
//部品表示位置
var m_domtourModuleDp_checkout_defPartsPosition_Top = 'right';
/*=====================================================
カレンダー部品設定（国内パッケージ 出発日（行き先から探す））
=====================================================*/
//デフォルト選択日付
var m_domtourModulePkg_depDate_defSelectDay = 8;
//データ送信用のClass名
var m_domtourModulePkg_depDate_dateValueClass = 'm_domPackDepDate';
//データ送信用のname名
var m_domtourModulePkg_depDate_dateValueName = 'deptDt';
//プレースホルダ表示テキスト
var m_domtourModulePkg_depDate_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModulePkg_depDate_inputClass = 'm_domPackDepDateText';
//必須チェックデザイン設定
var m_domtourModulePkg_depDate_isRequired = false;
//活性非活性設定
var m_domtourModulePkg_depDate_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModulePkg_depDate_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModulePkg_depDate_prevNextInterval = 1;
//受付終了日付
var m_domtourModulePkg_depDate_receiptDisableDays = 7;
//カレンダー表示月制御（月単位指定）
var m_domtourModulePkg_depDate_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModulePkg_depDate_selectableMonths = 12;
//祝日判定
var m_domtourModulePkg_depDate_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModulePkg_depDate_boundaryTime = '15:00';
//部品表示位置
var m_domtourModulePkg_depDate_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内パッケージ 出発日（商品コードから探す））
=====================================================*/
//デフォルト選択日付
var m_domtourModulePkg_depDateCourseCode_defSelectDay = 8;
//データ送信用のClass名
var m_domtourModulePkg_depDateCourseCode_dateValueClass = 'm_domCourseDepDate';
//データ送信用のname名
var m_domtourModulePkg_depDateCourseCode_dateValueName = 'deptDt';
//プレースホルダ表示テキスト
var m_domtourModulePkg_depDateCourseCode_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModulePkg_depDateCourseCode_inputClass = 'm_domCourseDepDateText';
//必須チェックデザイン設定
var m_domtourModulePkg_depDateCourseCode_isRequired = false;
//活性非活性設定
var m_domtourModulePkg_depDateCourseCode_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModulePkg_depDateCourseCode_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModulePkg_depDateCourseCode_prevNextInterval = 1;
//受付終了日付
var m_domtourModulePkg_depDateCourseCode_receiptDisableDays = 7;
//カレンダー表示月制御（月単位指定）
var m_domtourModulePkg_depDateCourseCode_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModulePkg_depDateCourseCode_selectableMonths = 12;
//祝日判定
var m_domtourModulePkg_depDateCourseCode_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModulePkg_depDateCourseCode_boundaryTime = '15:00';

/*=====================================================
カレンダー部品設定（国内ホテル チェックイン）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleHotel_checkin_defSelectDay = 1;
//データ送信用のClass名
var m_domtourModuleHotel_checkin_dateValueClass = 'm_domHotelCheInDate';
//データ送信用のname名
var m_domtourModuleHotel_checkin_dateValueName = 'checkInDt';
//プレースホルダ表示テキスト
var m_domtourModuleHotel_checkin_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleHotel_checkin_inputClass = 'm_domHotelCheInDateText';
//必須チェックデザイン設定
var m_domtourModuleHotel_checkin_isRequired = false;
//活性非活性設定
var m_domtourModuleHotel_checkin_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleHotel_checkin_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleHotel_checkin_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleHotel_checkin_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleHotel_checkin_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleHotel_checkin_selectableMonths = 12;
//祝日判定
var m_domtourModuleHotel_checkin_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleHotel_checkin_boundaryTime = '0:00';
//部品表示位置
var m_domtourModuleHotel_checkin_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内ホテル チェックアウト）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleHotel_checkout_defSelectDay = 2;
//データ送信用のClass名
var m_domtourModuleHotel_checkout_dateValueClass = 'm_domHotelCheOutDate';
//データ送信用のname名
var m_domtourModuleHotel_checkout_dateValueName = 'checkOutDt';
//プレースホルダ表示テキスト
var m_domtourModuleHotel_checkout_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleHotel_checkout_inputClass = 'm_domHotelCheOutDateText';
//必須チェックデザイン設定
var m_domtourModuleHotel_checkout_isRequired = false;
//活性非活性設定
var m_domtourModuleHotel_checkout_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleHotel_checkout_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleHotel_checkout_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleHotel_checkout_receiptDisableDays = 1;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleHotel_checkout_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleHotel_checkout_selectableMonths = 12;
//祝日判定
var m_domtourModuleHotel_checkout_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleHotel_checkout_boundaryTime = '0:00';
//旅行期間FromのClass名
var m_domtourModuleHotel_checkout_startDateClass = 'm_domHotelCheInDate';
//FROM日との差分日数指定
var m_domtourModuleHotel_checkout_daysFromStartDate = 1;
//FROM日との連動用設定
var m_domtourModuleHotel_checkout_returnDaysFromDepartureDate = 1;
//受付可能最大旅行日数
var m_domtourModuleHotel_checkout_maxTravelDays = 13;
//部品表示位置
var m_domtourModuleHotel_checkout_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内レンタカー 配車日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleCar_firstDay_defSelectDay = 2;
//データ送信用のClass名
var m_domtourModuleCar_firstDay_dateValueClass = 'm_domCarFirstDate';
//データ送信用のname名
var m_domtourModuleCar_firstDay_dateValueName = 'allocDt';
//プレースホルダ表示テキスト
var m_domtourModuleCar_firstDay_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleCar_firstDay_inputClass = 'm_domCarFirstDateText';
//必須チェックデザイン設定
var m_domtourModuleCar_firstDay_isRequired = false;
//活性非活性設定
var m_domtourModuleCar_firstDay_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleCar_firstDay_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleCar_firstDay_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleCar_firstDay_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleCar_firstDay_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleCar_firstDay_selectableMonths = 12;
//祝日判定
var m_domtourModuleCar_firstDay_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleCar_firstDay_boundaryTime = '0:00';
//部品表示位置
var m_domtourModuleCar_firstDay_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内レンタカー 返車日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleCar_lastDay_defSelectDay = 4;
//データ送信用のClass名
var m_domtourModuleCar_lastDay_dateValueClass = 'm_domCarLastDate';
//データ送信用のname名
var m_domtourModuleCar_lastDay_dateValueName = 'brbkDt';
//プレースホルダ表示テキスト
var m_domtourModuleCar_lastDay_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleCar_lastDay_inputClass = 'm_domCarLastDateText';
//必須チェックデザイン設定
var m_domtourModuleCar_lastDay_isRequired = false;
//活性非活性設定
var m_domtourModuleCar_lastDay_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleCar_lastDay_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleCar_lastDay_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleCar_lastDay_receiptDisableDays = 1;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleCar_lastDay_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleCar_lastDay_selectableMonths = 12;
//祝日判定
var m_domtourModuleCar_lastDay_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleCar_lastDay_boundaryTime = '0:00';
//旅行期間FromのClass名
var m_domtourModuleCar_lastDay_startDateClass = 'm_domCarFirstDate';
//FROM日との差分日数指定
var m_domtourModuleCar_lastDay_daysFromStartDate = 2;
//FROM日との連動用設定
var m_domtourModuleCar_lastDay_returnDaysFromDepartureDate = 2;
//受付可能最大旅行日数
var m_domtourModuleCar_lastDay_maxTravelDays = 13;
//部品表示位置
var m_domtourModuleCar_lastDay_defPartsPosition_Top = 'left';
//配車日と同日選択可能フラグ
var m_domtourModuleCar_lastDay_sameDayFlag = true;
/*=====================================================
カレンダー部品設定（国内観光 利用日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleTraffic_defSelectDay = 7;
//データ送信用のClass名
var m_domtourModuleTraffic_dateValueClass = 'm_domTrafficDate';
//データ送信用のname名
var m_domtourModuleTraffic_dateValueName = 'useDt';
//プレースホルダ表示テキスト
var m_domtourModuleTraffic_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleTraffic_inputClass = 'm_domTrafficDateText';
//必須チェックデザイン設定
var m_domtourModuleTraffic_isRequired = false;
//活性非活性設定
var m_domtourModuleTraffic_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleTraffic_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleTraffic_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleTraffic_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleCommon_defDispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleCommon_defSelectableMonths = 12;
//祝日判定
var m_domtourModuleTraffic_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleTraffic_boundaryTime = '0:00';
//部品表示位置
var m_domtourModuleTraffic_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内ツアー一括 出発日）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleBulk_defSelectDay = 7;
//データ送信用のClass名
var m_domtourModuleBulk_dateValueClass = 'm_domBulkDepDate';
//データ送信用のname名
var m_domtourModuleBulk_dateValueName = 'deptDt';
//プレースホルダ表示テキスト
var m_domtourModuleBulk_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleBulk_inputClass = 'm_domBulkDepDateText';
//必須チェックデザイン設定
var m_domtourModuleBulk_isRequired = false;
//活性非活性設定
var m_domtourModuleBulk_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleBulk_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleBulk_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleBulk_receiptDisableDays = 1;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleCommon_defDispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleCommon_defSelectableMonths = 12;
//祝日判定
var m_domtourModuleBulk_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleBulk_boundaryTime = '17:00';
/*=====================================================
カレンダー部品設定（国内ホテル(3社一括) チェックイン）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleHotelBulk_checkin_defSelectDay = 1;
//データ送信用のClass名
var m_domtourModuleHotelBulk_checkin_dateValueClass = 'm_domHotelBulkCheInDate';
//データ送信用のname名
var m_domtourModuleHotelBulk_checkin_dateValueName = 'useDt';
//プレースホルダ表示テキスト
var m_domtourModuleHotelBulk_checkin_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleHotelBulk_checkin_inputClass = 'm_domHotelBulkCheInDateText';
//必須チェックデザイン設定
var m_domtourModuleHotelBulk_checkin_isRequired = false;
//活性非活性設定
var m_domtourModuleHotelBulk_checkin_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleHotelBulk_checkin_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleHotelBulk_checkin_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleHotelBulk_checkin_receiptDisableDays = 0;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleHotelBulk_checkin_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleHotelBulk_checkin_selectableMonths = 12;
//祝日判定
var m_domtourModuleHotelBulk_checkin_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleHotelBulk_checkin_boundaryTime = '0:00';
//部品表示位置
var m_domtourModuleHotelBulk_checkin_defPartsPosition_Top = 'left';
/*=====================================================
カレンダー部品設定（国内ホテル(3社一括) チェックアウト）
=====================================================*/
//デフォルト選択日付
var m_domtourModuleHotelBulk_checkout_defSelectDay = 2;
//データ送信用のClass名
var m_domtourModuleHotelBulk_checkout_dateValueClass = 'm_domHotelBulkCheOutDate';
//データ送信用のname名
var m_domtourModuleHotelBulk_checkout_dateValueName = 'hiddenCheckOutDt';
//プレースホルダ表示テキスト
var m_domtourModuleHotelBulk_checkout_inputPlaceholder = '日付を選択してください';
//表示用のClass名
var m_domtourModuleHotelBulk_checkout_inputClass = 'm_domHotelBulkCheOutDateText';
//必須チェックデザイン設定
var m_domtourModuleHotelBulk_checkout_isRequired = false;
//活性非活性設定
var m_domtourModuleHotelBulk_checkout_isDisabled = false;
//○月すべて　ON/OFF
var m_domtourModuleHotelBulk_checkout_isDispAllOfMonth = false;
//ページめくり設定
var m_domtourModuleHotelBulk_checkout_prevNextInterval = 1;
//受付終了日付
var m_domtourModuleHotelBulk_checkout_receiptDisableDays = 1;
//カレンダー表示月制御（月単位指定）
var m_domtourModuleHotelBulk_checkout_dispMonths = 12;
//カレンダー部品デフォルト選択可能期間設定
var m_domtourModuleHotelBulk_checkout_selectableMonths = 12;
//祝日判定
var m_domtourModuleHotelBulk_checkout_isDispHoliday = true;
//カレンダー受付終了判別起点時間
var m_domtourModuleHotelBulk_checkout_boundaryTime = '0:00';
//旅行期間FromのClass名
var m_domtourModuleHotelBulk_checkout_startDateClass = 'm_domHotelBulkCheInDate';
//FROM日との差分日数指定
var m_domtourModuleHotelBulk_checkout_daysFromStartDate = 1;
//FROM日との連動用設定
var m_domtourModuleHotelBulk_checkout_returnDaysFromDepartureDate = 1;
//受付可能最大旅行日数
var m_domtourModuleHotelBulk_checkout_maxTravelDays = 13;
//部品表示位置
var m_domtourModuleHotelBulk_checkout_defPartsPosition_Top = 'left';


/*=====================================================
参加人数入力部品用文言定義（国内ツアー旅作・日本語）
=====================================================*/

var m_domtourModuleNumberOfPeopleDp_NPLP = new m_tourModuleNumberOfPeopleLanguageProperties();

//ヘッダー文言
m_domtourModuleNumberOfPeopleDp_NPLP.HeadFormat = '子供・幼児人数';

//決定ボタン
m_domtourModuleNumberOfPeopleDp_NPLP.SubmitButtonFormat = '決定';

//クローズボタン
m_domtourModuleNumberOfPeopleDp_NPLP.CloseText = '×';

//--------------------------------------------------------------------参加人数 列名/行
m_domtourModuleNumberOfPeopleDp_NPLP.ColumnName = [
	{text:'食事あり'},
	{text:'食事なし'},
	{text:'食事・', text2:'寝具あり'},
	{text:'食事のみ'},
	{text:'寝具のみ'},
	{text:'食事・', text2:'寝具なし'},
	{text:''} // タイトル非表示
	
];
m_domtourModuleNumberOfPeopleDp_NPLP.RowName = [
	{text:'9～11歳'},
	{text:'6～8歳'},
	{text:'3～5歳'},
	{text:'0～2歳'}
];
//--------------------------------------------------------------------参加人数
m_domtourModuleNumberOfPeopleDp_NPLP.List = [
  [
   //食事あり
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //食事なし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //食事・寝具あり
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //食事のみ
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //寝具のみ
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //食事・寝具なし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //タイトルなし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'},
   {value:'6', text:'6名'}
	]
];

/*=====================================================
参加人数入力部品用文言定義（国内ツアーパッケージ・日本語）
=====================================================*/

var m_domtourModuleNumberOfPeoplePkg_NPLP = new m_tourModuleNumberOfPeopleLanguageProperties();

//ヘッダー文言
m_domtourModuleNumberOfPeoplePkg_NPLP.HeadFormat = '子供・幼児人数';

//決定ボタン
m_domtourModuleNumberOfPeoplePkg_NPLP.SubmitButtonFormat = '決定';

//クローズボタン
m_domtourModuleNumberOfPeoplePkg_NPLP.CloseText = '×';

//--------------------------------------------------------------------参加人数 列名/行
m_domtourModuleNumberOfPeoplePkg_NPLP.ColumnName = [
	{text:'食事・', text2:'寝具あり'},
	{text:'食事・', text2:'寝具なし'},
	{text:'0～2歳'} // SP用タイトル
	
];
m_domtourModuleNumberOfPeoplePkg_NPLP.RowName = [
	{text:'6～11歳'},
	{text:'3～5歳'},
	{text:'0～2歳'}
];
//--------------------------------------------------------------------参加人数
m_domtourModuleNumberOfPeoplePkg_NPLP.List = [
  [
   //食事・寝具あり
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'},
   {value:'6', text:'6名'},
   {value:'7', text:'7名'},
   {value:'8', text:'8名'}
	],
  [
   //食事・寝具なし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'},
   {value:'6', text:'6名'},
   {value:'7', text:'7名'},
   {value:'8', text:'8名'}
	],
  [
   //タイトルなし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'},
   {value:'6', text:'6名'},
   {value:'7', text:'7名'},
   {value:'8', text:'8名'},
   {value:'9', text:'9名'}
	]
];

/*=====================================================
参加人数入力部品用文言定義（国内ツアーホテル・日本語）
=====================================================*/

var m_domtourModuleNumberOfPeopleHotel_NPLP = new m_tourModuleNumberOfPeopleLanguageProperties();

//ヘッダー文言
m_domtourModuleNumberOfPeopleHotel_NPLP.HeadFormat = '子供・幼児人数';

//決定ボタン
m_domtourModuleNumberOfPeopleHotel_NPLP.SubmitButtonFormat = '決定';

//クローズボタン
m_domtourModuleNumberOfPeopleHotel_NPLP.CloseText = '×';

//--------------------------------------------------------------------参加人数 列名/行
m_domtourModuleNumberOfPeopleHotel_NPLP.ColumnName = [
	{text:'食事・', text2:'寝具あり'},
	{text:'食事のみ'},
	{text:'寝具のみ'},
	{text:'食事・', text2:'寝具なし'},
	{text:[ // 行毎にタイトル変更
	      {text:'9～11歳'}, // 9～11歳
	      {text:'6～8歳'},  // 6～8歳
	      {text:''}         // 0～5歳
	  ]
	} // SP用タイトル	
];
m_domtourModuleNumberOfPeopleHotel_NPLP.RowName = [
	{text:'9～11歳'},
	{text:'6～8歳'},
	{text:'0～5歳'}
];
//--------------------------------------------------------------------参加人数
m_domtourModuleNumberOfPeopleHotel_NPLP.List = [
  [
   //食事・寝具あり
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //食事のみ
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //寝具のみ
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //食事・寝具なし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	],
  [
   //タイトルなし
   {value:'0', text:'0名'},
   {value:'1', text:'1名'},
   {value:'2', text:'2名'},
   {value:'3', text:'3名'},
   {value:'4', text:'4名'},
   {value:'5', text:'5名'}
	]
];

/*=====================================================
参加人数入力部品用パラメータ（国内ツアー旅作・日本語）
=====================================================*/
var m_domtourModuleNumberOfPeopleDp_optionParams = {
	initNum :[
	  // 食事あり,食事なし,食事・寝具あり,食事のみ,寝具のみ,食事・寝具なし,幼児
	  [ 0, 0, 0, 0, 0, 0, 0 ],	// 9～11歳
	  [ 0, 0, 0, 0, 0, 0, 0 ],	// 6～8歳
	  [ 0, 0, 0, 0, 0, 0, 0 ],	// 3～5歳
	  [ 0, 0, 0, 0, 0, 0, 0 ]	// 0～2歳
	]
	,column : [
		{ name : 'sel1', sum : true , meal : true  , dispSum : true},
		{ name : 'sel2', sum : true , meal : false , dispSum : true},
		{ name : 'sel3', sum : true , meal : true  , dispSum : true},
		{ name : 'sel4', sum : true , meal : true  , dispSum : true},
		{ name : 'sel5', sum : true , dispSum : true},
		{ name : 'sel6', sum : true , dispSum : true},
		{ name : 'sel7', sum : true , dispSum : true}
	]
	,row : [
		{ name : 'Child1' },
		{ name : 'Child2' },
		{ name : 'Child3' },
		{ name : 'Child4' }
	]
	,dispMatrix : [
		[ true,  true,  false, false, false, false, false ],
		[ true,  true,  false, false, false, false, false ],
		[ false, false, true,  true,  true,  true,  false ],
		[ false, false, false, false, false, false, true ]
	]
	,textBoxTemplate : '<%= value %>名（0歳～11歳）'
	,textBoxTemplate_Top : '<%= value %>名（0歳～11歳）'
	,ctrlDisabled : 1
};
//--------------------------------------------------------------------送信パラメータ変換MAP
var m_domtourModuleNumberOfPeopleDp_requestParamsMap = {
		name:[
		      'sel1Child1',
		      'sel2Child1',
		      'sel1Child2',
		      'sel2Child2',
		      'sel3Child3',
		      'sel4Child3',
		      'sel5Child3',
		      'sel6Child3',
		      'sel7Child4'
		    ],
		param:[
		       'childADpCnt',
		       'childIDpCnt',
		       'childCDpCnt',
		       'childJDpCnt',
		       'childEDpCnt',
		       'childFDpCnt',
		       'childGDpCnt',
		       'childHDpCnt',
		       'infantCnt',
		],
		meal:[
		      true,
		      false,
		      true,
		      false,
		      true,
		      true,
		      undefined,
		      undefined,
		      undefined
		]
}

/*=====================================================
参加人数入力部品用パラメータ（国内ツアーパッケージ・日本語）
=====================================================*/
var m_domtourModuleNumberOfPeoplePkg_optionParams = {
	initNum :[
	  // 食事・寝具あり,食事・寝具なし,幼児
	  [ 0, 0, 0 ],	// 6～11歳
	  [ 0, 0, 0 ],	// 3～5歳
	  [ 0, 0, 0 ]	// 0～2歳
	]
	,column : [
		{ name : 'sel1', sum : true , dispSum : true },
		{ name : 'sel2', sum : true , dispSum : true },
		{ name : 'sel3', sum : true , dispSum : true }
	]
	,row : [
		{ name : 'Child1' },
		{ name : 'Child2' },
		{ name : 'Child3', nolegend : true }
	]
	,dispMatrix : [
		[ true,  true,  false ],
		[ true,  true,  false ],
		[ false, false, true  ]
	]
	,textBoxTemplate : '<%= value %>名（0歳―11歳）'
	,textBoxTemplate_Top : '<%= value %>名（0歳～11歳）'
};
//--------------------------------------------------------------------送信パラメータ変換MAP
var m_domtourModuleNumberOfPeoplePkg_requestParamsMap = {
		name:[
		      'sel1Child1',
		      'sel2Child1',
		      'sel1Child2',
		      'sel2Child2',
		      'sel3Child3'
		    ],
		param:[
		       'childAPkgCnt',
		       'childBPkgCnt',
		       'childCPkgCnt',
		       'childDPkgCnt',
		       'infantCnt'
		]
}
/*=====================================================
参加人数入力部品用パラメータ（国内ツアーホテル・日本語）
=====================================================*/
var m_domtourModuleNumberOfPeopleHotel_optionParams = {
	initNum :[
	    // 食事・寝具あり,食事のみ,寝具のみ,食事・寝具なし,6～11歳
		[ 0, 0, 0, 0, 0 ],	// 9～11歳
		[ 0, 0, 0, 0, 0 ],	// 6～8歳
		[ 0, 0, 0, 0, 0 ]	// 0～5歳
	]
	,column : [
		{ name : 'sel1', sum : true , dispSum : true},
		{ name : 'sel2', sum : true , dispSum : true},
		{ name : 'sel3', sum : true , dispSum : true},
		{ name : 'sel4', sum : true , dispSum : true},
		{ name : 'sel5', sum : true , dispSum : true}
	]
	,row : [
		{ name : 'Child1', nolegend : true },
		{ name : 'Child2', nolegend : true },
		{ name : 'Child3' }
	]
	,dispMatrix : [
		[ false, false, false, false, true  ],
		[ false, false, false, false, true  ],
		[ true,  true,  true,  true,  false ]
	]
	,textBoxTemplate : '<%= value %>名（0歳―11歳）'
	,textBoxTemplate_Top : '<%= value %>名（0歳～11歳）'
};

//--------------------------------------------------------------------送信パラメータ変換MAP
var m_domtourModuleNumberOfPeopleHotel_requestParamsMap = {
		name:[
		      'sel5Child1',
		      'sel5Child2',
		      'sel1Child3',
		      'sel2Child3',
		      'sel3Child3',
		      'sel4Child3'
		    ],
		param:[
		       'childAHtlCnt',
		       'childBHtlCnt',
		       'childCHtlCnt',
		       'childDHtlCnt',
		       'childEHtlCnt',
		       'childFHtlCnt'
		]
}
//--------------------------------------------------------------------レンタカー返車場所送信不要パラメータMAP
var m_domtourModuleCar_unnecessaryParamsMap = {
		1:[
		   'brbkOffice',
		   'brbkAirpOffice',
		   'brbkAirpCd',
		   'brbkDistrictCd',
		   'brbkRegionCd',
		   'brbkAreaCd',
		   ],
		2:[
		   'brbkOffice',
		   'brbkAirpOffice',
		   'brbkDistrictCd',
		   'brbkRegionCd',
		   'brbkAreaCd',
		   ],
		3:[
		   'brbkOffice',
		   'brbkAirpOffice',
		   'brbkAirpCd',
		   ],
}
//--------------------------------------------------------------------レンタカー返車場所送信パラメータMAP
var m_domtourModuleCar_officeParamsMap = {
		2:[
		   'brbkAirpOfficeCoptCd',
		   'brbkAirpOfficeFaclCd',
		   'brbkAirpOfficeAbdAreaCd',
		   'brbkAirpOfficeNo',
		   ],
		3:[
		   'brbkOfficeCoptCd',
		   'brbkOfficeFaclCd',
		   'brbkOfficeAbdAreaCd',
		   'brbkOfficeNo',
		   ],
}