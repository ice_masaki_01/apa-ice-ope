/*=====================================================
* Script Name: tour_module_airportListParts.js
* Description: 空席照会モジュール ツアー共通 空港一覧 共通処理
* Version: 1.1
* Last Up Date: 2017/10/31
=====================================================*/

var m_tourModuleAirportList_defaultAirportListTableProperties = {
		airportListTableLanguageProperties : {
			 modalTitle: {dep:'出発地を選択してください。',arr: '到着地を選択してください。'}
			,majorAirport: '主要な空港'
			,closeButton: {src: '/module/air-booking/image/common/d_btn_close.png' ,alt: 'closeAlt' }}};

var m_tourModuleAirportList_baseHtml_1 = [
	'<!-- \n',
	'  *****************************************************\n',
	'  ******************* select-airport ******************\n',
	'  *****************************************************\n',
	'  select-ap_arr or select-ap_dep\n',
	'-->\n',
	'\n',
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-scroll_col2 balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">出発地</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'    <div class="mod-balloon-cont_inner">\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_parent">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_child is-scroll">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <p tabindex="0" data-val="-2" class="mod-back_btn js-mod-back_btn">戻る</p>\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'  </div><!-- / .mod-balloon-cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_1_TOP = [
	'<!-- \n',
	'  *****************************************************\n',
	'  ******************* select-airport ******************\n',
	'  *****************************************************\n',
	'  select-ap_arr or select-ap_dep\n',
	'-->\n',
	'\n',
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-scroll_col2 balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">出発地</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'    <div class="mod-balloon-cont_inner">\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_parent">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_child is-scroll">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'  </div><!-- / .mod-balloon-cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_2 = [
	'<!-- \n',
	'  *****************************************************\n',
	'  ******************* select-airport ******************\n',
	'  *****************************************************\n',
	'  select-ap_arr or select-ap_dep\n',
	'-->\n',
	'\n',
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-scroll_col2 select-apo_horizon airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">到着地<span class="mod-balloon_ttl_stay_1">（1都市目）</span></h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_parent">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_child">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <p tabindex="0" data-val="-2" class="mod-back_btn js-mod-back_btn">戻る</p>\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'\n',
	'\n',
	'\n',
	'  </div><!-- / .mod-balloon-set -->\n',
	'  </div><!-- / .mod-balloon_cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_2_TOP = [
	'<!-- \n',
	'  *****************************************************\n',
	'  ******************* select-airport ******************\n',
	'  *****************************************************\n',
	'  select-ap_arr or select-ap_dep\n',
	'-->\n',
	'\n',
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-scroll_col2 select-apo_horizon airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">到着地<span class="mod-balloon_ttl_stay_1">（1都市目）</span></h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_parent">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_child">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'\n',
	'\n',
	'\n',
	'  </div><!-- / .mod-balloon-set -->\n',
	'  </div><!-- / .mod-balloon_cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_3 = [
	'<!-- \n',
	'  *****************************************************\n',
	'  ******************* select-airport ******************\n',
	'  *****************************************************\n',
	'  select-ap_arr or select-ap_dep\n',
	'-->\n',
	'\n',
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-single balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">到着地（2都市目）</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_single">\n',
	'  <div class="mod-balloon-set_box">\n',
	'  <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'\n',
	'  </div><!-- / .mod-balloon-set_box_inner -->\n',
	'</div><!-- / .mod-balloon-set -->\n',
	'</div><!-- / .mod-balloon-set_cover -->\n',
	'</div><!-- / .mod-balloon-set_box -->\n',
	'</div><!-- / .mod-balloon_cont_inner -->\n',
	'</div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_3_scroll = [
	'<!-- \n',
	'  *****************************************************\n',
	'  ******************* select-airport ******************\n',
	'  *****************************************************\n',
	'  select-ap_arr or select-ap_dep\n',
	'-->\n',
	'\n',
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-single balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">到着地（2都市目）</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_single is-scroll">\n',
	'  <div class="mod-balloon-set_box">\n',
	'  <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'\n',
	'  </div><!-- / .mod-balloon-set_box_inner -->\n',
	'</div><!-- / .mod-balloon-set -->\n',
	'<div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'</div><!-- / .mod-balloon-set_cover -->\n',
	'</div><!-- / .mod-balloon-set_box -->\n',
	'</div><!-- / .mod-balloon_cont_inner -->\n',
	'</div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_4 = [
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-single balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">国際線出発地</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'  <div class="mod-balloon-set_cover mod-balloon-set_single">\n',
	'  <div class="mod-balloon-set_box">\n',
	'  <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'\n',
	'\n',
	'\n',
	'\n',
	'  </div><!-- / .mod-balloon-set_box_inner -->\n',
	'  </div><!-- / .mod-balloon-set -->\n',
	'  </div><!-- / .mod-balloon-set_cover -->\n',
	'  </div><!-- / .mod-balloon-set_box -->\n',
	'  </div><!-- / .mod-balloon_cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_4_scroll = [
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-single balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl"></h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'  <div class="mod-balloon-set_cover mod-balloon-set_single is-scroll">\n',
	'  <div class="mod-balloon-set_box">\n',
	'  <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'\n',
	'\n',
	'\n',
	'\n',
	'  </div><!-- / .mod-balloon-set_box_inner -->\n',
	'  </div><!-- / .mod-balloon-set -->\n',
	'  <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'  </div><!-- / .mod-balloon-set_cover -->\n',
	'  </div><!-- / .mod-balloon-set_box -->\n',
	'  </div><!-- / .mod-balloon_cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_5 = [
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-scroll_col3 balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">市内営業所</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'    <div class="mod-balloon-cont_inner">\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_parent">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_child">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <p tabindex="0" data-val="-2" class="mod-back_btn js-mod-back_btn">戻る</p>\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_countryList mod-list-single">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_gchild">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <p tabindex="0" data-val="-2" class="mod-back_btn js-mod-back_btn">戻る</p>\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'  </div><!-- / .mod-balloon-cont_inner -->\n',
	'\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_5_TOP = [
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-scroll_column mod-scroll_col3 balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow"></i>\n',
	'<div class="modal-balloon_inner">\n',
	'<div class="modal-balloon_contents">\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">市内営業所</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'    <div class="mod-balloon-cont_inner">\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_parent">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_child">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_countryList mod-list-single">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'\n',
	'    <div class="mod-balloon-set_cover mod-balloon-set_gchild">\n',
	'    <div class="mod-balloon-set_box">\n',
	'    <div class="mod-balloon-set_box_inner mod-balloon-set_box_cityList mod-list-single">\n',
	'    </div><!-- / .mod-balloon-set_box_inner -->\n',
	'    </div><!-- / .mod-balloon-set_box -->\n',
	'    <div class="mod-balloon-scroll_over"><a class="js-mod-balloon-scroll_over" href="javascript:void(0);"><span>scroll</span></a></div>\n',
	'    </div><!-- / .mod-balloon-set_cover -->\n',
	'\n',
	'  </div><!-- / .mod-balloon-cont_inner -->\n',
	'\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div><!-- / .modal-balloon -->\n'
].join('');

var m_tourModuleAirportList_baseHtml_6 = [
	'<div id="airport_balloon_parts_%UNIQUE_KEY%" class="modal-balloon module-travel_parts mod-single balloon-set_left airport_balloon_parts">\n',
	'<div class="modal-balloon_cover">\n',
	'<i class="modal-balloon_arrow" style="top: 547px; left: 108px;"></i>\n',
	'<div class="modal-balloon_inner" style="right: auto; left: 0px; top: 559px;">\n',
	'<div class="modal-balloon_contents">\n',
	'\n',
	'  <div class="mod-balloon_title">\n',
	'    <h2 class="mod-balloon_ttl">目的地</h2>\n',
	'    <a href="javascript:void(0);" class="mod-balloon_close js-mod-balloon_close" tabindex="0">×</a>\n',
	'  </div>\n',
	'\n',
	'  <div class="mod-balloon_cont_cover">\n',
	'  <div class="mod-balloon-cont_inner">\n',
	'  <div class="mod-balloon-set">\n',
	'  <div class="mod-balloon-set_cover mod-balloon-set_single">\n',
	'  <div class="mod-balloon-set_box">\n',
	'\n',
	'	  <p class="error-re_display">\n',
	'	  エリアを途中で変更する場合は一度入力した内容がリセットされます。<br>\n',
	'	  エリア変更後は項目を再入力してください。\n',
	'	  </p>\n',
	'    <ul class="mod-list-single">\n',
	'    </ul>\n',
	'\n',
	'  </div><!-- / .mod-balloon-set -->\n',
	'  </div><!-- / .mod-balloon-set_cover -->\n',
	'  </div><!-- / .mod-balloon-set_box -->\n',
	'  </div><!-- / .mod-balloon_cont_inner -->\n',
	'  </div><!-- / .mod-balloon_cont_cover -->\n',
	'\n',
	'</div><!-- / .modal-balloon_contents -->\n',
	'</div><!-- / .modal-balloon_inner -->\n',
	'</div><!-- / .modal-balloon_cover -->\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_mainApoListTpl = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<% if(!_.isEmpty(onlineAirports)) { %>\n',
	'    <dl class="d_majorAirport m_majorAirport">\n',
	'        <dd>\n',
	'            <ul class="mod-list-single">\n',
	'                <% _.chain(onlineAirports).each(function(airportlist) { %>\n',
	'                    <% _.each(airportlist.letters, function(letter) { %>\n',
	'                        <li id="apo_<%= letter %>" data-area="<%= airportlist.area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListDp[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'                    <% }) %>\n',
	'                <% }) %>\n',
	'            </ul>\n',
	'        </dd>\n',
	'    </dl>\n',
	'<% } %>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTplDp = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListDp[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');
m_tourModuleAirportList_cityListTplPkg = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListPkg[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');


m_tourModuleAirportList_mainApoListTpl_domAirportList1 = [
'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
'<% if(!_.isEmpty(onlineAirports)) { %>\n',
'    <dl class="d_majorAirport m_majorAirport">\n',
'        <dd>\n',
'            <ul class="mod-list-single">\n',
'                <% _.chain(onlineAirports).each(function(airportlist) { %>\n',
'                    <% _.each(airportlist.letters, function(letter) { %>\n',
'                        <li id="apo_<%= letter %>" data-area="<%= airportlist.area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_domAirportList1_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
'                    <% }) %>\n',
'                <% }) %>\n',
'            </ul>\n',
'        </dd>\n',
'    </dl>\n',
'<% } %>\n',
'</div>\n'
].join('');
m_tourModuleAirportList_cityListTpl_domAirportList1 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domAirportList1_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domDirectionDistrictRegionList1 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domDirectionDistrictRegionList2 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domDirectionDistrictRegionList3 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domDirectionDistrictList1 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictList1_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_countryListTpl = [
	'<div data-area="<%= area %>" data-cid="<%= cid %>" class="m_COUNTRYlist <%= area + "_apos" %> <%= "cont_" + area %>" style="display: none;">\n',
	'<% if(!_.isEmpty(countries)) { %>\n',
	'<ul>\n',
	'    <% _.chain(countries).hsplit(3).each(function(country) { %>\n',
	'        <% _.each(country, function(letter) { %>\n',
	'<li data-val="<%= cidVal %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListPkg[letter["cid"]]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'<% } else { %>\n',
	'<% } %>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_countryListTpl_domDirectionDistrictRegionList1 = [
	'<div data-area="<%= area %>" data-cid="<%= cid %>" class="m_COUNTRYlist <%= area + "_apos" %> <%= "cont_" + area %>" style="display: none;">\n',
	'<% if(!_.isEmpty(countries)) { %>\n',
	'<ul>\n',
	'    <% _.chain(countries).hsplit(3).each(function(country) { %>\n',
	'        <% _.each(country, function(letter) { %>\n',
	'<li data-val="<%= cidVal %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[letter["cid"]]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'<% } else { %>\n',
	'<% } %>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_countryListTpl_domDirectionDistrictRegionList2 = [
'<div data-area="<%= area %>" data-cid="<%= cid %>" class="m_COUNTRYlist <%= area + "_apos" %> <%= "cont_" + area %>" style="display: none;">\n',
'<% if(!_.isEmpty(countries)) { %>\n',
'<ul>\n',
'    <% _.chain(countries).hsplit(3).each(function(country) { %>\n',
'        <% _.each(country, function(letter) { %>\n',
'<li data-val="<%= cidVal %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[letter["cid"]]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
'        <% }) %>\n',
'    <% }) %>\n',
'</ul>\n',
'<% } else { %>\n',
'<% } %>\n',
'</div>\n'
].join('');

m_tourModuleAirportList_countryListTpl_domDirectionDistrictRegionList3 = [
'<div data-area="<%= area %>" data-cid="<%= cid %>" class="m_COUNTRYlist <%= area + "_apos" %> <%= "cont_" + area %>" style="display: none;">\n',
'<% if(!_.isEmpty(countries)) { %>\n',
'<ul>\n',
'    <% _.chain(countries).hsplit(3).each(function(country) { %>\n',
'        <% _.each(country, function(letter) { %>\n',
'<li data-val="<%= cidVal %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[letter["cid"]]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
'        <% }) %>\n',
'    <% }) %>\n',
'</ul>\n',
'<% } else { %>\n',
'<% } %>\n',
'</div>\n'
].join('');

m_tourModuleAirportList_countryTpl = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<% if(!_.isEmpty(onlineAirports)) { %>\n',
	'    <dl class="d_majorAirport m_majorAirport">\n',
	'        <dt class="mod-app_ttl">主要な空港</dt><dd>\n',
	'        <ul class="mod-list-horizon">\n',
	'        <% _.chain(onlineAirports).hsplit(3).each(function(airportlist) { %>\n',
	'            <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListDp[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'            <% }) %>\n',
	'        <% }) %>\n',
	'        </ul></dd></dl>\n',
	'<% } %>\n',
	'<% if(!_.isEmpty(areaname)) { %>\n',
	'    <p class="d_airportIndex mod-app_ttl" id="<%= cid + "_anchor" %>"><%- areaname.replace(/(◆|<|>|●|\\*)/g, "") %></p>\n',
	'<% } %>\n',
	'    <ul class="mod-list-horizon">\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'        <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="***" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListDp[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'    </ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domAirportList2 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domAirportList2_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domAirportList3 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domAirportList3_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domSalesOffice1 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domSalesOffice1_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domSalesOffice2 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domSalesOffice2_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_domPkgArrList1 = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_domPkgArrList1_ApoKeyList[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_inttourOptionArea = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListInttourOptionArea[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

m_tourModuleAirportList_cityListTpl_inttourOptionTheme = [
	'<div data-cid="<%= cid %>" class="m_APlist <%= area + "_apos" %>" style="display: none;">\n',
	'<ul>\n',
	'    <% _.chain(airports).hsplit(3).each(function(airportlist) { %>\n',
	'        <% _.each(airportlist, function(letter) { %>\n',
	'            <li id="apo_<%= letter %>" data-area="<%= area %>" data-apo="<%= letter %>"  href="#" data-val="<%= letter %>" data-cat="0" tabindex="0"><%= m_tourModuleAirportList_ApoKeyListInttourOptionTheme[letter]["text"].replace(/(◆|<|>|●|\\*)/g, "") %></li>\n',
	'        <% }) %>\n',
	'    <% }) %>\n',
	'</ul>\n',
	'</div>\n'
].join('');

$tour_module_jq.fn.extend({
	//よく使う存在確認用の関数拡張を自前で実装。
	m_tourModuleAirportList_exists : function() {
		return Boolean(this.length > 0)
	},

	m_tourModuleAirportList_isFuncLayout : function(layoutType) {
		// レイアウトタイプより機能・リストを判定
		var layoutInfoTable = {
			'dp'  : ['1', '2', '3', '8', '10'],											// 'dp':海外ツアー旅作
			'pkg' : ['4', '5', '6', '9'],												// 'pkg':海外ツアーパッケージ
			'optionArea' : ['101'],														// 'optionArea':オプション 行き先
			'optionTheme' : ['102'],													// 'optionPlan':オプション プラン
			'domAirportList1' : ['11', '12', '13', '14', '31', '32', '33', '34'],		// 'domAirportList1':国内ツアー空港リスト①
			'domAirportList2' : ['16', '18', '27', '36'],								// 'domAirportList2':国内ツアー空港リスト②
			'domAirportList3' : ['20', '21', '40', '41'],								// 'domAirportList3':国内ツアー空港リスト③
			'domDirectionDistrictRegionList1' : ['15', '19', '35', '39', '28', '48'],	// 'domDirectionDistrictRegionList1':方面・地区・地域リスト①
			'domDirectionDistrictRegionList2' : ['22', '42'],							// 'domDirectionDistrictRegionList2':方面・地区・地域リスト②
			'domDirectionDistrictRegionList3' : ['23', '43'],							// 'domDirectionDistrictRegionList3':方面・地区・地域リスト③
			'domDirectionDistrictList1' : ['26', '46'],									// 'domDirectionDistrictRegionList3':目的地(方面・地区)リスト①
			'domSalesOffice1' : ['24', '44'],											// 'domSalesOffice1':営業所リスト①
			'domSalesOffice2' : ['25', '45'],											// 'domSalesOffice2':営業所リスト②
			'domPkgArrList1' : ['17', '37']												// 'domPkgArrList1':目的地(方面)リスト①
		}
		var result = '';
		$tour_module_jq.each(layoutInfoTable, function(eei, eev){
			var findData = eev.filter(function(fe) {
				return fe == layoutType
			});
			if(!_.isEmpty(findData)){
				result = eei;
				return false;
			}
		});
		return result;
	},
	
	m_tourModuleAirportList_isDomTourList : function(layoutType) {
		// true:国内ツアー, false:海外ツアー
		var layoutInfoTable = {
			'true' : ['11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '31', '32', '33', '34', '35', '36', '37', '39', '40', '41', '42', '43', '44', '45', '46', '48'],
			'false'  : ['1', '2', '3', '8', '4', '5', '6', '9']
		}
		var result = '';
		$tour_module_jq.each(layoutInfoTable, function(eei, eev){
			var findData = eev.filter(function(fe) {
				return fe == layoutType
			});
			if(!_.isEmpty(findData)){
				result = eei;
				return false;
			}
		});
		return result;
	},
	
	m_tourModuleAirportList_getLayoutInfo : function(layoutType) {
		var layoutInfo;
		switch (layoutType) {
		case '1' :
			// 旅作　出発地表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_812'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_1'};
			break;
		case '2' :
			// 旅作　到着地・旅作　到着地（1都市目）表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_813'], 'baseHtml':m_tourModuleAirportList_baseHtml_2, 'renderFunc':'AiportListWindow.renderAreaApoWindow_2'};
			break;
		case '3' :
			// 旅作　到着地（2都市目）表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_813'], 'baseHtml':m_tourModuleAirportList_baseHtml_3_scroll, 'renderFunc':'AiportListWindow.renderAreaApoWindow_3'};
			break;
		case '4' :
			// パッケージツアー　出発地表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_811'], 'baseHtml':m_tourModuleAirportList_baseHtml_4, 'renderFunc':'AiportListWindow.renderAreaApoWindow_4'};
			break;
		case '5' :
			// パッケージツアー　目的地表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_814'], 'baseHtml':m_tourModuleAirportList_baseHtml_5, 'renderFunc':'AiportListWindow.renderAreaApoWindow_5'};
			break;
		case '6' :
			// パッケージツアー　目的地（過渡期時）表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_814'], 'baseHtml':m_tourModuleAirportList_baseHtml_6, 'renderFunc':'AiportListWindow.renderAreaApoWindow_6'};
			break;
		case '101' :
			// 海外ツアーオプション 行き先表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_inttourOptionAreaAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_816'], 'baseHtml':m_tourModuleAirportList_baseHtml_4_scroll, 'renderFunc':'AiportListWindow.renderAreaApoWindow_101'};
			break;
		case '102' :
			// 海外ツアーオプション プラン表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_inttourOptionThemeAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_817'], 'baseHtml':m_tourModuleAirportList_baseHtml_4_scroll, 'renderFunc':'AiportListWindow.renderAreaApoWindow_102'};
			break;
		case '8' :
			// TOP旅作　出発地表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_812'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_1'};
			break;
		case '9' :
			// TOPパッケージツアー　目的地表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_814'], 'baseHtml':m_tourModuleAirportList_baseHtml_5_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_5'};
			break;
		case '10' :
			// TOP旅作　到着地・旅作　到着地（1都市目）表示用
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_813'], 'baseHtml':m_tourModuleAirportList_baseHtml_2_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_2'};
			break;
		case '11' :
			// 国内ツアー旅作 フライト行き出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_11'};
			break;
		case '12' :
			// 国内ツアー旅作 フライト行き到着地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_2, 'renderFunc':'AiportListWindow.renderAreaApoWindow_12'};
			break;
		case '13' :
			// 国内ツアー旅作 フライト帰り出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_13'};
			break;
		case '14' :
			// 国内ツアー旅作 フライト帰り到着地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_3, 'renderFunc':'AiportListWindow.renderAreaApoWindow_14'};
			break;
		case '15' :
			// 国内ツアー旅作 宿泊
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_833'], 'baseHtml':m_tourModuleAirportList_baseHtml_5, 'renderFunc':'AiportListWindow.renderAreaApoWindow_15'};
			break;
		case '16' :
			// 国内パッケージ 行き先から探す 出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_832'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_16'};
			break;
		case '17' :
			// 国内パッケージ 行き先から探す 目的地
			layoutInfo = {'areaList':m_tourModuleAirportList_domPkgArrList1, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_835'], 'baseHtml':m_tourModuleAirportList_baseHtml_4, 'renderFunc':'AiportListWindow.renderAreaApoWindow_17'};
			break;
		case '18' :
			// 国内パッケージ 商品コードから探す 出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_832'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_18'};
			break;
		case '19' :
			// 国内ツアーホテル 宿泊地
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_833'], 'baseHtml':m_tourModuleAirportList_baseHtml_5, 'renderFunc':'AiportListWindow.renderAreaApoWindow_19'};
			break;
		case '20' :
			// 国内ツアーレンタカー 配車場所 空港エリア
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList3_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_837'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_20'};
			break;
		case '21' :
			// 国内ツアーレンタカー 返車場所 空港営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList3_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_837'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_21'};
			break;
		case '22' :
			// 国内ツアーレンタカー 配車場所 市内エリア
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_838'], 'baseHtml':m_tourModuleAirportList_baseHtml_5, 'renderFunc':'AiportListWindow.renderAreaApoWindow_22'};
			break;
		case '23' :
			// 国内ツアーレンタカー 返車場所 市内営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList3_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_839'], 'baseHtml':m_tourModuleAirportList_baseHtml_5, 'renderFunc':'AiportListWindow.renderAreaApoWindow_23'};
			break;
		case '24' :
			// 国内ツアーレンタカー 返車場所 空港営業所・営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domSalesOffice1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_840'], 'baseHtml':m_tourModuleAirportList_baseHtml_3, 'renderFunc':'AiportListWindow.renderAreaApoWindow_24'};
			break;
		case '25' :
			// 国内ツアーレンタカー 返車場所 市内営業所・営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domSalesOffice2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_841'], 'baseHtml':m_tourModuleAirportList_baseHtml_3, 'renderFunc':'AiportListWindow.renderAreaApoWindow_25'};
			break;
		case '26' :
			// 国内観光 目的地
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_836'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_26'};
			break;
		case '27' :
			// 国内ツアー一括 出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_832'], 'baseHtml':m_tourModuleAirportList_baseHtml_1, 'renderFunc':'AiportListWindow.renderAreaApoWindow_27'};
			break;
		case '28' :
			// 国内ツアーホテル(3社一括) 宿泊地
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_833'], 'baseHtml':m_tourModuleAirportList_baseHtml_5, 'renderFunc':'AiportListWindow.renderAreaApoWindow_28'};
			break;
		case '31' :
			// TOP国内ツアー旅作 フライト行き出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_11'};
			break;
		case '32' :
			// TOP国内ツアー旅作 フライト行き到着地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_12'};
			break;
		case '33' :
			// TOP国内ツアー旅作 フライト帰り出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_13'};
			break;
		case '34' :
			// TOP国内ツアー旅作 フライト帰り到着地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_831'], 'baseHtml':m_tourModuleAirportList_baseHtml_3, 'renderFunc':'AiportListWindow.renderAreaApoWindow_14'};
			break;
		case '35' :
			// TOP国内ツアー旅作 宿泊
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_833'], 'baseHtml':m_tourModuleAirportList_baseHtml_5_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_15'};
			break;
		case '36' :
			// TOP国内パッケージ 行き先から探す 出発地
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_832'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_16'};
			break;
		case '37' :
			// TOP国内パッケージ 行き先から探す 目的地
			layoutInfo = {'areaList':m_tourModuleAirportList_domPkgArrList1, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_835'], 'baseHtml':m_tourModuleAirportList_baseHtml_4, 'renderFunc':'AiportListWindow.renderAreaApoWindow_17'};
			break;
		case '39' :
			// TOP国内ツアーホテル 宿泊地
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_833'], 'baseHtml':m_tourModuleAirportList_baseHtml_5_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_19'};
			break;
		case '40' :
			// TOP国内ツアーレンタカー 配車場所 空港エリア
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList3_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_837'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_20'};
			break;
		case '41' :
			// TOP国内ツアーレンタカー 返車場所 空港営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domAirportList3_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_837'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_21'};
			break;
		case '42' :
			// TOP国内ツアーレンタカー 配車場所 市内エリア
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_838'], 'baseHtml':m_tourModuleAirportList_baseHtml_5_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_22'};
			break;
		case '43' :
			// TOP国内ツアーレンタカー 返車場所 市内営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList3_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_839'], 'baseHtml':m_tourModuleAirportList_baseHtml_5_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_23'};
			break;
		case '44' :
			// TOP国内ツアーレンタカー 返車場所 空港営業所・営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domSalesOffice1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_840'], 'baseHtml':m_tourModuleAirportList_baseHtml_3, 'renderFunc':'AiportListWindow.renderAreaApoWindow_24'};
			break;
		case '45' :
			// TOP国内ツアーレンタカー 返車場所 市内営業所・営業所
			layoutInfo = {'areaList':m_tourModuleAirportList_domSalesOffice2_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_841'], 'baseHtml':m_tourModuleAirportList_baseHtml_3, 'renderFunc':'AiportListWindow.renderAreaApoWindow_25'};
			break;
		case '46' :
			// TOP国内TOP観光 目的地
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_836'], 'baseHtml':m_tourModuleAirportList_baseHtml_1_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_26'};
			break;
		case '48' :
			// TOP国内ツアーホテル 宿泊地
			layoutInfo = {'areaList':m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList, 'apoRelList':m_tourModuleAirportList_domApoRelList['PD_833'], 'baseHtml':m_tourModuleAirportList_baseHtml_5_TOP, 'renderFunc':'AiportListWindow.renderAreaApoWindow_28'};
			break;
		default :
			layoutInfo = {'areaList':m_tourModuleAirportList_DepAreaList, 'apoRelList':m_tourModuleAirportList_ApoRelList['PD_813'], 'baseHtml':m_tourModuleAirportList_baseHtml_2, 'renderFunc':'AiportListWindow.renderAreaApoWindow_2'};
			break;
		}

		return layoutInfo;
	},

	m_tourModuleAirportList_getMultiAirportValue : function(airport, layoutType) {
		var result = '';
		var funcType = this.m_tourModuleAirportList_isFuncLayout(layoutType);
		var multiValue = undefined;
		if(!_.isEmpty(airport)){
			if(funcType === 'domAirportList1'){
				multiValue = m_tourModuleAirportList_domAirportList1_ApoKeyList[airport]['multi'];
			}
		}
		if(!_.isEmpty(multiValue)){
			result = multiValue;
		}else{
			result = '';
		}
		return result;
	},

	m_tourModuleAirportList_getAreaFromCountry : function(country, layoutType) {
		var airportRelList = this.m_tourModuleAirportList_getLayoutInfo(layoutType)['apoRelList'];

		// 入力パラメーターの空港Letterから所属するエリアを取得
		var currentArea = _.chain(airportRelList).pairs().find(function(o) {
			return _.chain(o[1]).pluck('cid').find(function(q) {
				return 'cid-' + country === q;
			}).value();
		}).first().value();

		return currentArea;
	},

	m_tourModuleAirportList_getAreaCountryFromAirport : function(letter, layoutType) {
		var airportRelList = this.m_tourModuleAirportList_getLayoutInfo(layoutType)['apoRelList'];

		var areaCountry = {'area':'', 'country':''};

		// 入力パラメーターの空港Letterから所属するエリアを取得
		var currentArea = _.chain(airportRelList).pairs().find(function(o) {
			return _.chain(o[1]).pluck('airports').flatten().find(function(q) {
				return letter == q;
			}).value();
		}).first().value();

		if (currentArea) {
			for (var ii = 0, iiLength = airportRelList[currentArea].length; ii < iiLength; ii++) {
				var currentAirports =  airportRelList[currentArea][ii];
				if (currentAirports.airportCount === '0') { continue; }
				var currentCid = currentAirports['cid'];

				var airports = _.select(currentAirports.airports, function(c, i) {
					return c !== 'NOP' && i !== 0 && c === letter;
				});

				if (!_.isEmpty(airports)) {
					areaCountry['area'] = currentArea;
					var isDomTourFlg = this.m_tourModuleAirportList_isDomTourList(layoutType);
					if(isDomTourFlg != undefined && isDomTourFlg === 'true'){
						areaCountry['country'] = currentCid.substring(4, 7); // 国内ツアー
					}else{
						areaCountry['country'] = currentCid.substring(4, 6); // 海外ツアー
					}
					return areaCountry;
				}
			}
		}

		return areaCountry;
	},

	m_tourModuleAirportList_getSelectedName : function(area, country, airport, layoutType) {
		var airportRelList = this.m_tourModuleAirportList_getLayoutInfo(layoutType)['apoRelList'];
		var selectedName;
		var funcType = this.m_tourModuleAirportList_isFuncLayout(layoutType);
		if (!_.isEmpty(airport)) {
			if(funcType === 'dp'){
				selectedName = m_tourModuleAirportList_ApoKeyListDp[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'pkg'){
				selectedName = m_tourModuleAirportList_ApoKeyListPkg[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'optionArea'){
				selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionArea[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'optionTheme'){
				selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionTheme[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domAirportList1'){
				selectedName = m_tourModuleAirportList_domAirportList1_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domAirportList2'){
				selectedName = m_tourModuleAirportList_domAirportList2_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domAirportList3'){
				selectedName = m_tourModuleAirportList_domAirportList3_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictRegionList1'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictRegionList2'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictRegionList3'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictList1'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictList1_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domSalesOffice1'){
				selectedName = m_tourModuleAirportList_domSalesOffice1_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domSalesOffice2'){
				selectedName = m_tourModuleAirportList_domSalesOffice2_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domPkgArrList1'){
				selectedName = m_tourModuleAirportList_domPkgArrList1_ApoKeyList[airport]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}
		} else if (!_.isEmpty(country)) {
			if(funcType === 'dp'){
				selectedName = m_tourModuleAirportList_ApoKeyListDp['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'pkg'){
				selectedName = m_tourModuleAirportList_ApoKeyListPkg['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'optionArea'){
				selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionArea['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'optionTheme'){
				selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionTheme['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domAirportList1'){
				selectedName = m_tourModuleAirportList_domAirportList1_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domAirportList2'){
				selectedName = m_tourModuleAirportList_domAirportList2_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domAirportList3'){
				selectedName = m_tourModuleAirportList_domAirportList3_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictRegionList1'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictRegionList2'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictRegionList3'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domDirectionDistrictList1'){
				selectedName = m_tourModuleAirportList_domDirectionDistrictList1_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domSalesOffice1'){
				selectedName = m_tourModuleAirportList_domSalesOffice1_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domSalesOffice2'){
				selectedName = m_tourModuleAirportList_domSalesOffice2_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}else if(funcType === 'domPkgArrList1'){
				selectedName = m_tourModuleAirportList_domPkgArrList1_ApoKeyList['cid-' + country]['text'].replace(/(◆|<|>|●|\\*)/g, '');
			}
		} else if (!_.isEmpty(area)) {
			if(m_inttourModuleCommon_isTransitionPeriod()){
				if($tour_module_jq.inArray(area, m_inttourTransitionPeriodOptions.transitionToReliefPageAreas) >= 0){
					selectedName = '';
				}else{
					var areaId = airportRelList[area][0].cid;
					if(funcType === 'dp'){
						selectedName = m_tourModuleAirportList_ApoKeyListDp[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'pkg'){
						selectedName = m_tourModuleAirportList_ApoKeyListPkg[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'optionArea'){
						selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionArea[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'optionTheme'){
						selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionTheme[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domAirportList1'){
						selectedName = m_tourModuleAirportList_domAirportList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domAirportList2'){
						selectedName = m_tourModuleAirportList_domAirportList2_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domAirportList3'){
						selectedName = m_tourModuleAirportList_domAirportList3_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domDirectionDistrictRegionList1'){
						selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domDirectionDistrictRegionList2'){
						selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domDirectionDistrictRegionList3'){
						selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domDirectionDistrictList1'){
						selectedName = m_tourModuleAirportList_domDirectionDistrictList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domSalesOffice1'){
						selectedName = m_tourModuleAirportList_domSalesOffice1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domSalesOffice2'){
						selectedName = m_tourModuleAirportList_domSalesOffice2_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}else if(funcType === 'domPkgArrList1'){
						selectedName = m_tourModuleAirportList_domPkgArrList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					}
				}
			}else{
				var areaId = airportRelList[area][0].cid;
				if(funcType === 'dp'){
					selectedName = m_tourModuleAirportList_ApoKeyListDp[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'pkg'){
					selectedName = m_tourModuleAirportList_ApoKeyListPkg[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'optionArea'){
					selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionArea[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'optionTheme'){
					selectedName = m_tourModuleAirportList_ApoKeyListInttourOptionTheme[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domAirportList1'){
					selectedName = m_tourModuleAirportList_domAirportList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domAirportList2'){
					selectedName = m_tourModuleAirportList_domAirportList2_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domAirportList3'){
					selectedName = m_tourModuleAirportList_domAirportList3_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domDirectionDistrictRegionList1'){
					selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domDirectionDistrictRegionList2'){
					selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domDirectionDistrictRegionList3'){
					selectedName = m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domDirectionDistrictList1'){
					selectedName = m_tourModuleAirportList_domDirectionDistrictList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domSalesOffice1'){
					selectedName = m_tourModuleAirportList_domSalesOffice1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domSalesOffice2'){
					selectedName = m_tourModuleAirportList_domSalesOffice2_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}else if(funcType === 'domPkgArrList1'){
					selectedName = m_tourModuleAirportList_domPkgArrList1_ApoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
				}
			}
		} else {
			selectedName = '';
		}

		return selectedName;
	},

	m_tourModuleAirportList_AirportList : function (options) {
		//パラメータチェック
		if ( !$tour_module_jq(options.targetObject).m_tourModuleAirportList_exists() ) {
			console.log('targetAirport was not found in the argument!!');
			return this;
		}

		// 過渡期チェック
		if (m_inttourModuleCommon_isTransitionPeriod()) {
			if (options.layoutType === '5') {
				options.layoutType = '6';
			}
		}

		var tempAltp = {};
		$tour_module_jq.extend(true,tempAltp,m_tourModuleAirportList_defaultAirportListTableProperties);
		$tour_module_jq.extend(true,tempAltp,options);
		options = tempAltp;

		options.m_targetObject = $tour_module_jq(options.targetObject);
		options.filterAirportClass = options.filterAirportClass;
		options.targetAreaClass = options.targetAreaClass;
		options.targetCountryClass = options.targetCountryClass;
		options.targetAirportClass = options.targetAirportClass;
		options.isRequired = options.isRequired;
		options.isDisabled = options.isDisabled;
		options.layoutType = options.layoutType;
		options.uniqueKey = options.uniqueKey;
		options.stayCityCount = options.stayCityCount;

		//選択ボタンクリック
		$tour_module_jq(this).find('.d_locationIcon').unbind('click.selected keydown');//古いイベントが重複していたら削除
		$tour_module_jq(this).find('.d_locationIcon').bind('click.selected keydown',options, function(e){
			if(e.which == 1 || e.which == 13 || e.isTrigger == 3){ // 右クリックまたはエンターのみ
				$tour_module_jq('.airport_balloon_parts').remove();
				$tour_module_jq('.is-balloon_open').removeClass('is-balloon_open');
				options.scrollTop = $tour_module_jq(window).scrollTop();

				// タイプ別吹き出し枠の追加
				var baseHtml = '';
				switch (options.layoutType) {
				case '1' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '2' :
					baseHtml = m_tourModuleAirportList_baseHtml_2;
					break;
				case '3' :
					baseHtml = m_tourModuleAirportList_baseHtml_3_scroll;
					break;
				case '4' :
					baseHtml = m_tourModuleAirportList_baseHtml_4;
					break;
				case '5' :
					baseHtml = m_tourModuleAirportList_baseHtml_5;
					break;
				case '6' :
					baseHtml = m_tourModuleAirportList_baseHtml_6;
					break;
				case '8' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '9' :
					baseHtml = m_tourModuleAirportList_baseHtml_5_TOP;
					break;
				case '10' :
					baseHtml = m_tourModuleAirportList_baseHtml_2_TOP;
					break;
				case '101' :
					baseHtml = m_tourModuleAirportList_baseHtml_4_scroll;
					break;
				case '102' :
					baseHtml = m_tourModuleAirportList_baseHtml_4_scroll;
					break;
				case '11' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '12' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '13' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '14' :
					baseHtml = m_tourModuleAirportList_baseHtml_3;
					break;
				case '15' :
					baseHtml = m_tourModuleAirportList_baseHtml_5;
					break;
				case '16' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '17' :
					baseHtml = m_tourModuleAirportList_baseHtml_4;
					break;
				case '18' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '19' :
					baseHtml = m_tourModuleAirportList_baseHtml_5;
					break;
				case '20' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '21' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '22' :
					baseHtml = m_tourModuleAirportList_baseHtml_5;
					break;
				case '23' :
					baseHtml = m_tourModuleAirportList_baseHtml_5;
					break;
				case '24' :
					baseHtml = m_tourModuleAirportList_baseHtml_3;
					break;
				case '25' :
					baseHtml = m_tourModuleAirportList_baseHtml_3;
					break;
				case '26' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '27' :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				case '28' :
					baseHtml = m_tourModuleAirportList_baseHtml_5;
					break;
				case '31' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '32' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '33' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '34' :
					baseHtml = m_tourModuleAirportList_baseHtml_3;
					break;
				case '35' :
					baseHtml = m_tourModuleAirportList_baseHtml_5_TOP;
					break;
				case '36' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '37' :
					baseHtml = m_tourModuleAirportList_baseHtml_4;
					break;
				case '39' :
					baseHtml = m_tourModuleAirportList_baseHtml_5_TOP;
					break;
				case '40' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '41' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '42' :
					baseHtml = m_tourModuleAirportList_baseHtml_5_TOP;
					break;
				case '43' :
					baseHtml = m_tourModuleAirportList_baseHtml_5_TOP;
					break;
				case '44' :
					baseHtml = m_tourModuleAirportList_baseHtml_3;
					break;
				case '45' :
					baseHtml = m_tourModuleAirportList_baseHtml_3;
					break;
				case '46' :
					baseHtml = m_tourModuleAirportList_baseHtml_1_TOP;
					break;
				case '48' :
					baseHtml = m_tourModuleAirportList_baseHtml_5_TOP;
					break;
				default :
					baseHtml = m_tourModuleAirportList_baseHtml_1;
					break;
				}

				// 吹き出しのID部分を指定文字列に置換
				baseHtml = baseHtml.replace('%UNIQUE_KEY%', options.uniqueKey);
				$tour_module_jq('body').append(baseHtml);
				var targetBaloon = $tour_module_jq('#' + $tour_module_jq(this).attr('data-balloon'));

				//出発・到着を判別
				if (e.data.m_targetObject.find(this).m_tourModuleAirportList_exists()) {
					options.targetDiv = e.data.m_targetObject;
					if (options.layoutType === '2' || options.layoutType === '10') {
						var parentForm = options.targetObject.parents('form');
						var stay = parentForm.find('span.m_city_stay_inline_block_02').css('display');
						var $titleArea = targetBaloon.find('.mod-balloon_title');
						if(stay !== undefined && stay == 'none'){
							// 1都市滞在時は1都市目文言を削除
							$titleArea.find('.mod-balloon_ttl_stay_1').css('display', 'none');
						}else if(stay !== undefined && stay == 'inline-block'){
							// 2都市滞在時は1都市目文言を付与
							$titleArea.find('.mod-balloon_ttl_stay_1').css('display', 'inline-block');
						}
					} else {
						targetBaloon.find('.mod-balloon_ttl').text(options.balloonTitle);
					}
				}

				//一旦クリア
				AiportListWindow.clearWindow();

				//再描画
				var tempId = '';
				var targetArea = options.targetDiv.find(options.targetAreaClass).val();
				var targetCountry = options.targetDiv.find(options.targetCountryClass).val();
				var targetApo = options.targetDiv.find(options.targetAirportClass).val();
				if(options.isInitSelectOtherInputObj != undefined){
					// 他入力項目による初期選択の設定
					if(options.layoutType == '15' || options.layoutType == '35'){
						// 国内ツアー旅作宿泊地
						var relApo = $tour_module_jq(options.isInitSelectOtherInputObj).val();
						if(!_.isEmpty(relApo)){
							targetArea = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoRelAreaList[relApo].area;
						}else{
							targetArea = '';
						}
						targetCountry = '';
						targetApo = '';
					}
				}

				if (options.targetDiv == options.m_targetObject) {

					// タイプ別renderメソッドの呼び出し
					switch (options.layoutType) {
					case '1' :
						AiportListWindow.renderAreaApoWindow_1(options);
						break;
					case '2' :
					case '10' :
						if (options.stayCityCount) {
							var selectedStayCityCount = options.stayCityCount.find('input[name=m_stay]:checked');
							if (selectedStayCityCount && selectedStayCityCount.val() === '1') {
								AiportListWindow.renderAreaApoWindow_7(options);
								break;
							}
						}
						AiportListWindow.renderAreaApoWindow_2(options);
						break;
					case '3' :
						AiportListWindow.renderAreaApoWindow_3(options);
						break;
					case '4' :
						AiportListWindow.renderAreaApoWindow_4(options);
						break;
					case '5' :
						AiportListWindow.renderAreaApoWindow_5(options);
						break;
					case '6' :
						AiportListWindow.renderAreaApoWindow_6(options);
						break;
					case '7' :
						AiportListWindow.renderAreaApoWindow_7(options);
						break;
					case '8' :
						AiportListWindow.renderAreaApoWindow_1(options);
						break;
					case '9' :
						AiportListWindow.renderAreaApoWindow_5(options);
						break;
					case '101' :
						AiportListWindow.renderAreaApoWindow_101(options);
						break;
					case '102' :
						AiportListWindow.renderAreaApoWindow_102(options);
						break;
					case '11' :
						AiportListWindow.renderAreaApoWindow_11(options);
						break;
					case '12' :
						AiportListWindow.renderAreaApoWindow_12(options);
						break;
					case '13' :
						AiportListWindow.renderAreaApoWindow_13(options);
						break;
					case '14' :
						AiportListWindow.renderAreaApoWindow_14(options);
						break;
					case '15' :
						AiportListWindow.renderAreaApoWindow_15(options);
						break;
					case '16' :
						AiportListWindow.renderAreaApoWindow_16(options);
						break;
					case '17' :
						AiportListWindow.renderAreaApoWindow_17(options);
						break;
					case '18' :
						AiportListWindow.renderAreaApoWindow_18(options);
						break;
					case '19' :
						AiportListWindow.renderAreaApoWindow_19(options);
						break;
					case '20' :
						AiportListWindow.renderAreaApoWindow_20(options);
						break;
					case '21' :
						AiportListWindow.renderAreaApoWindow_21(options);
						break;
					case '22' :
						AiportListWindow.renderAreaApoWindow_22(options);
						break;
					case '23' :
						AiportListWindow.renderAreaApoWindow_23(options);
						break;
					case '24' :
						AiportListWindow.renderAreaApoWindow_24(options);
						break;
					case '25' :
						AiportListWindow.renderAreaApoWindow_25(options);
						break;
					case '26' :
						AiportListWindow.renderAreaApoWindow_26(options);
						break;
					case '27' :
						AiportListWindow.renderAreaApoWindow_27(options);
						break;
					case '28' :
						AiportListWindow.renderAreaApoWindow_28(options);
						break;
					case '31' :
						AiportListWindow.renderAreaApoWindow_11(options);
						break;
					case '32' :
						AiportListWindow.renderAreaApoWindow_12(options);
						break;
					case '33' :
						AiportListWindow.renderAreaApoWindow_13(options);
						break;
					case '34' :
						AiportListWindow.renderAreaApoWindow_14(options);
						break;
					case '35' :
						AiportListWindow.renderAreaApoWindow_15(options);
						break;
					case '36' :
						AiportListWindow.renderAreaApoWindow_16(options);
						break;
					case '37' :
						AiportListWindow.renderAreaApoWindow_17(options);
						break;
					case '39' :
						AiportListWindow.renderAreaApoWindow_19(options);
						break;
					case '40' :
						AiportListWindow.renderAreaApoWindow_20(options);
						break;
					case '41' :
						AiportListWindow.renderAreaApoWindow_21(options);
						break;
					case '42' :
						AiportListWindow.renderAreaApoWindow_22(options);
						break;
					case '43' :
						AiportListWindow.renderAreaApoWindow_23(options);
						break;
					case '44' :
						AiportListWindow.renderAreaApoWindow_24(options);
						break;
					case '45' :
						AiportListWindow.renderAreaApoWindow_25(options);
						break;
					case '46' :
						AiportListWindow.renderAreaApoWindow_26(options);
						break;
					case '48' :
						AiportListWindow.renderAreaApoWindow_28(options);
						break;
					default :
						AiportListWindow.renderAreaApoWindow_1(options);
						break;
					}

					tempId = options.targetDiv.find(options.targetAreaClass);
				}

				// フォーカスをクリア
				targetBaloon.find('.mod-balloon-set_box').find('li').removeClass('current');
				var targetObj;
				//フォーム値により、初期エリアの選択
				if (_.isEmpty(tempId)) {
					targetObj = targetBaloon.find('.mod-balloon-set_box li:first');
				} else if (!targetArea || _.isEmpty(targetArea)) {
					if (!targetBaloon.find('.mod-balloon-set_box li:first').attr('id')) {
						targetObj = targetBaloon.find('.mod-balloon-set_box li:first');
					}
				} else {
					targetObj = targetBaloon.find('.mod-balloon-set_box').find('[data-val = ' + targetArea + ']');
				}

				if (targetObj && targetObj.length > 0) {
					var id = targetObj.attr('data-val');
					targetArea = id;
					var modBalloonContInner = targetObj.parents('.mod-balloon-cont_inner');
					var currentAreaCountrys = modBalloonContInner.find('.mod-balloon-set_box_countryList').find('.' + id + '_apos');

					targetBaloon.find('.mod-balloon-set_box_inner > div').hide();
					targetBaloon.find('.mod-balloon-set_parent').find('li').removeClass('current');
					modBalloonContInner.find('.mod-balloon-set_box_countryList').find('li').removeClass('current');
					modBalloonContInner.find('.mod-balloon-set_box_cityList').find('li').removeClass('current');
					targetObj.addClass('current');

					currentAreaCountrys.show();

					var currentCountryCities;
					var countryList = targetBaloon.find('.m_COUNTRYlist li');
					if (countryList && countryList.length > 0) {
						// 国一覧の選択状態初期設定
						targetBaloon.find('.m_COUNTRYlist li').removeClass('current');
						var targetCid;
						if (_.isEmpty(tempId)) {
							targetBaloon.find('.mod-balloon-set_box_countryList').find('.' + targetArea + '_apos:first li').addClass('current');
							targetCid = targetBaloon.find('.mod-balloon-set_box_countryList').find('.' + targetArea + '_apos:first').attr('data-cid');
						} else if (!targetCountry || _.isEmpty(targetCountry)) {
							targetBaloon.find('.mod-balloon-set_box_countryList').find('.' + targetArea + '_apos:first li').addClass('current');
							targetCid = targetBaloon.find('.mod-balloon-set_box_countryList').find('.' + targetArea + '_apos:first').attr('data-cid');
						} else {
							targetCid = 'cid-' + targetCountry;
							targetBaloon.find('.mod-balloon-set_box_countryList').find('[data-cid=' + targetCid + '] li').addClass('current');
						}

						currentCountryCities = modBalloonContInner.find('.mod-balloon-set_box_cityList').find('[data-cid=' + targetCid + ']');
					} else {
						currentCountryCities = modBalloonContInner.find('.mod-balloon-set_box_cityList').find('.' + targetArea + '_apos');
					}
					currentCountryCities.show();
				}

				// 都市一覧の選択状態初期設定
				if (!_.isEmpty(targetApo)) {
					var targetCity = targetBaloon.find('.mod-balloon-set_box_cityList').find('[data-apo = ' + targetApo + ']');
					targetCity.addClass('current');
				} else {
					if (!_.isEmpty(targetCountry)) {
						targetBaloon.find('.mod-balloon-set_box_cityList').find('[data-cid=' + targetCid + '] li:first').addClass('current');
					} else {
						targetBaloon.find('.mod-balloon-set_box_cityList li:first').addClass('current');
					}
				}

				targetBaloon.find('.mod-balloon-set_box_inner').scrollTop(0);

				var selectItems = targetBaloon.find('li');
				$tour_module_jq.each(selectItems, function() {
					// 各liタグをエンターキー操作時、クリックイベントに変換
					$tour_module_jq(this).on('keydown',function(event){
						if(event.keyCode === 13){ // エンターキー

							// クリックイベントを発火
							$tour_module_jq(this).trigger('click');

							/*
							 * 地域または国に対してEnterキーを押下した場合は隣のエリアの先頭要素にフォーカスを当てる処理 
							 */
							var balloonContInner = $tour_module_jq(this).parents('.mod-balloon-cont_inner');
							// 地域のli要素でEnterキー押下された場合に国または都市にフォーカスを当てる
							if($tour_module_jq(this).hasClass('mod-area-catlist_btn')) {
								// 吹き出し上に表示中の国一覧を探す
								var displayingCountries = balloonContInner.find('.mod-balloon-set_box_countryList li:visible');
								if (displayingCountries.length > 0) {
									displayingCountries.first().focus();
								} else {
									// 吹き出し上に表示中の国一覧が無ければ都市一覧を探す
									var displayingAirports = balloonContInner.find('.m_majorAirport li:visible, .m_APlist li:visible');
									if (displayingAirports.length > 0) {
										displayingAirports.first().focus();
									}
								}
							}

							// 国のli要素でEnterキー押下された場合に都市にフォーカスを当てる
							if ($tour_module_jq(this).parents('.mod-balloon-set_box_countryList').length > 0) {
								// 吹き出し上に表示中の都市一覧を探す
								var displayingAirports = balloonContInner.find('.m_majorAirport li:visible, .m_APlist li:visible');
								if (displayingAirports.length > 0) {
									displayingAirports.first().focus();
								}
							}
						}
					});
				});
			}

			return true;
		});

		_.mixin({
			hsplit: function(array, cnt) {
				return _.times(cnt, function(i) {
					s =_.range(0, i);
					s = _.reduce(s, function(memo, num) {
						return memo + (num < Math.floor(_.size(array) % cnt) ?
								Math.ceil(_.size(array) / cnt) : Math.floor(_.size(array) / cnt));
					}, 0)
					t = (i < Math.floor(_.size(array) % cnt) ?
							Math.ceil(_.size(array) / cnt) : Math.floor(_.size(array) / cnt));

					return array.slice(s, s + t);
				});
			}
		});
		
		// 都市滞在選択切り替え時のイベント登録
		if(options.stayCityCount){
			options.stayCityCount.on('change', 'input[name=m_stay]', options, function(e){
				
				if(options.intTopFlg && typeof e.eventPhase === "undefined"){
					//国内線初期表示時に呼び出された場合はイベントスキップ
				}else{
					if($tour_module_jq(this).val() === '1'){
						// 1都市滞在⇒2都市滞在に変更時、2都市目を持たない到着地(1都市目)選択中の場合クリアする
						var beforeAirport = e.data.m_targetObject.find(e.data.targetAirportClass).val();
						if(!_.isEmpty(beforeAirport)){
							var findFlg = false;
							$tour_module_jq.each(m_tourModuleAirportList_ApoRelList['PD_815'], function(ee1i, ee1v){
								$tour_module_jq.each(ee1v, function(ee2i, ee2v){
									var existsAirport = ee2v['airports'].filter(function(fe) {
										return fe === beforeAirport
									});
									if(existsAirport.length > 0){
										findFlg = true;
										return false;
									}
								});
								if(findFlg){
									return false;
								}
							});
							// 2都市目を持たない1都市目の場合、1都市目の入力をクリア
							if(!findFlg){
								var eo = $tour_module_jq.Event('onChangeAirport', {
									moduleRootBox:e.data.targetObject,
									beforeArea:e.data.m_targetObject.find(e.data.targetAreaClass).val(),
									beforeCountry:e.data.m_targetObject.find(e.data.targetCountryClass).val(),
									beforeAirport:e.data.m_targetObject.find(e.data.targetAirportClass).val(),
									afterArea:'',
									afterCountry:'',
									afterAirport:''
								});
								var beforeArea = e.data.m_targetObject.find(e.data.targetAreaClass).val();
								// 選択クリア
								e.data.m_targetObject.find(e.data.targetAreaClass).val('');
								e.data.m_targetObject.find(e.data.targetCountryClass).val('');
								e.data.m_targetObject.find(e.data.targetAirportClass).val('');
								if(e.data.m_targetObject.find("span" + e.data.targetInputClass).length !== 0){
									e.data.m_targetObject.find("span" + e.data.targetInputClass).text(options.inputPlaceholder);
								}else{
									e.data.m_targetObject.find(e.data.targetInputClass).val('');
								}
								// 変更後イベント発火
								e.data.m_targetObject.find(e.data.targetAirportClass).trigger(eo);
								if (!_.isEmpty(beforeArea)) {
									e.data.m_targetObject.find(e.data.targetAirportClass).trigger('onChangeParentArea');
								}
							}
						}
					}
				}
			});
		}
		

		var AiportListWindow = {
			templates : {
				areaLink: _.template('<li data-val="<%= area %>" class="mod-area-catlist_btn" tabindex="0"><%= text %></li>'),
				areaALink: _.template('<li data-val="<%= area %>" class="mod-area-catlist_btn transition-to-relief" tabindex="0"><a href=""><%= text %></a></li>'),
				country: _.template(m_tourModuleAirportList_countryTpl),
				mainApoList: _.template(m_tourModuleAirportList_mainApoListTpl),
				countryList: _.template(m_tourModuleAirportList_countryListTpl),
				cityListDp: _.template(m_tourModuleAirportList_cityListTplDp),
				cityListPkg: _.template(m_tourModuleAirportList_cityListTplPkg),
				cityListInttourOptionArea: _.template(m_tourModuleAirportList_cityListTpl_inttourOptionArea),
				cityListInttourOptionTheme: _.template(m_tourModuleAirportList_cityListTpl_inttourOptionTheme),
				cityListDomAirportList1: _.template(m_tourModuleAirportList_cityListTpl_domAirportList1),
				mainApoListDomAirportList1: _.template(m_tourModuleAirportList_mainApoListTpl_domAirportList1),
				cityListDomAirportList2: _.template(m_tourModuleAirportList_cityListTpl_domAirportList2),
				cityListDomAirportList3: _.template(m_tourModuleAirportList_cityListTpl_domAirportList3),
				cityListDomDirectionDistrictRegionList1: _.template(m_tourModuleAirportList_cityListTpl_domDirectionDistrictRegionList1),
				cityListDomDirectionDistrictRegionList2: _.template(m_tourModuleAirportList_cityListTpl_domDirectionDistrictRegionList2),
				cityListDomDirectionDistrictRegionList3: _.template(m_tourModuleAirportList_cityListTpl_domDirectionDistrictRegionList3),
				countryListDomDirectionDistrictRegionList1: _.template(m_tourModuleAirportList_countryListTpl_domDirectionDistrictRegionList1),
				countryListDomDirectionDistrictRegionList2: _.template(m_tourModuleAirportList_countryListTpl_domDirectionDistrictRegionList2),
				countryListDomDirectionDistrictRegionList3: _.template(m_tourModuleAirportList_countryListTpl_domDirectionDistrictRegionList3),
				cityListDomDirectionDistrictList1: _.template(m_tourModuleAirportList_cityListTpl_domDirectionDistrictList1),
				cityListDomSalesOffice1: _.template(m_tourModuleAirportList_cityListTpl_domSalesOffice1),
				cityListDomSalesOffice2: _.template(m_tourModuleAirportList_cityListTpl_domSalesOffice2),
				cityListTpl_domPkgArrList1: _.template(m_tourModuleAirportList_cityListTpl_domPkgArrList1)
				
			},

			getDepArea: function(area) {
				return _.find(m_tourModuleAirportList_DepAreaList, function(e) { return area === e.value });
			},

			// 旅作　出発地表示用
			renderAreaApoWindow_1 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_DepAreaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_1(depAreaList, m_tourModuleAirportList_ApoRelList['PD_812'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListDp);
			},

			// 旅作　到着地表示用
			renderAreaApoWindow_2 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_ArrAreaList['PD_813']).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_2(depAreaList, m_tourModuleAirportList_ApoRelList['PD_813'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListDp);
			},

			// 旅作　到着地（2都市目）表示用
			renderAreaApoWindow_3 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_ArrAreaList['PD_813']).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_3(depAreaList, m_tourModuleAirportList_ApoRelList['PD_813'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListDp);
			},

			// パッケージツアー　出発地表示用
			renderAreaApoWindow_4 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_DepAreaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_4(depAreaList, m_tourModuleAirportList_ApoRelList['PD_811'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListPkg);
			},

			// パッケージツアー　目的地表示用
			renderAreaApoWindow_5 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_ArrAreaList['PD_814']).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_5(depAreaList, m_tourModuleAirportList_ApoRelList['PD_814'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListPkg);
			},

			// パッケージツアー　目的地（過渡期時）表示用
			renderAreaApoWindow_6 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_ArrAreaList['PD_814']).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_6(depAreaList, m_tourModuleAirportList_ApoRelList['PD_814'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListPkg);
			},

			// 旅作　到着地（1都市目）表示用
			renderAreaApoWindow_7 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_ArrAreaList['PD_815']).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_2(depAreaList, m_tourModuleAirportList_ApoRelList['PD_815'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListDp);
			},
			
			// 海外ツアー　オプション 行き先表示用
			renderAreaApoWindow_101 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_inttourOptionAreaAreaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_101(depAreaList, m_tourModuleAirportList_ApoRelList['PD_816'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListInttourOptionArea);
			},

			// 海外ツアー　オプション プラン表示用
			renderAreaApoWindow_102 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_inttourOptionThemeAreaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_102(depAreaList, m_tourModuleAirportList_ApoRelList['PD_817'], options);
				this.setClickHandler(m_tourModuleAirportList_ApoKeyListInttourOptionTheme);
			},
			
			// 国内ツアー旅作　フライト行き出発地表示用
			renderAreaApoWindow_11 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_11(depAreaList, m_tourModuleAirportList_domApoRelList['PD_831'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList1_ApoKeyList);
			},
			// 国内ツアー旅作　フライト行き到着地表示用
			renderAreaApoWindow_12 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_12(depAreaList, m_tourModuleAirportList_domApoRelList['PD_831'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList1_ApoKeyList);
			},
			// 国内ツアー旅作　フライト帰り出発地表示用
			renderAreaApoWindow_13 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_11(depAreaList, m_tourModuleAirportList_domApoRelList['PD_831'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList1_ApoKeyList);
			},
			// 国内ツアー旅作　フライト帰り到着地表示用
			renderAreaApoWindow_14 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_13(depAreaList, m_tourModuleAirportList_domApoRelList['PD_831'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList1_ApoKeyList);
			},
			// 国内ツアー旅作　宿泊地表示用
			renderAreaApoWindow_15 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_14(depAreaList, m_tourModuleAirportList_domApoRelList['PD_833'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
			},
			// 国内パッケージ　行き先から探す 出発地表示用
			renderAreaApoWindow_16 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList2_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_15(depAreaList, m_tourModuleAirportList_domApoRelList['PD_832'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList2_ApoKeyList);
			},
			// 国内パッケージ　行き先から探す 目的地表示用
			renderAreaApoWindow_17 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domPkgArrList1).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_16(depAreaList, m_tourModuleAirportList_domApoRelList['PD_835'], options);
				this.setClickHandler(m_tourModuleAirportList_domPkgArrList1_ApoKeyList);
			},
			// 国内パッケージ　商品コードから探す 出発地表示用
			renderAreaApoWindow_18 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList2_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_15(depAreaList, m_tourModuleAirportList_domApoRelList['PD_832'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList2_ApoKeyList);
			},
			// 国内ツアーホテル　宿泊地表示用
			renderAreaApoWindow_19 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_17(depAreaList, m_tourModuleAirportList_domApoRelList['PD_833'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
			},
			// 国内ツアーレンタカー　配車場所 空港エリア表示用
			renderAreaApoWindow_20 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList3_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_18(depAreaList, m_tourModuleAirportList_domApoRelList['PD_837'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList3_ApoKeyList);
			},
			// 国内ツアーレンタカー　返車場所 空港営業所表示用
			renderAreaApoWindow_21 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList3_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_18(depAreaList, m_tourModuleAirportList_domApoRelList['PD_837'], options);
				this.setClickHandler(m_tourModuleAirportList_domAirportList3_ApoKeyList);
			},
			// 国内ツアーレンタカー　返車場所 市内営業所
			renderAreaApoWindow_22 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictRegionList2_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_19(depAreaList, m_tourModuleAirportList_domApoRelList['PD_838'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList);
			},
			// 国内ツアーレンタカー　返車場所 市内営業所
			renderAreaApoWindow_23 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictRegionList3_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_20(depAreaList, m_tourModuleAirportList_domApoRelList['PD_839'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList);
			},
			// 国内ツアーレンタカー　返車場所 空港営業所・営業所表示用
			renderAreaApoWindow_24 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domSalesOffice1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_21(depAreaList, m_tourModuleAirportList_domApoRelList['PD_840'], options);
				this.setClickHandler(m_tourModuleAirportList_domSalesOffice1_ApoKeyList);
			},
			// 国内ツアーレンタカー　返車場所 市内営業所・営業所表示用
			renderAreaApoWindow_25 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictRegionList3_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_22(depAreaList, m_tourModuleAirportList_domApoRelList['PD_841'], options);
				this.setClickHandler(m_tourModuleAirportList_domSalesOffice2_ApoKeyList);
			},
			// 国内観光　目的地表示用
			renderAreaApoWindow_26 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_23(depAreaList, m_tourModuleAirportList_domApoRelList['PD_836'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictList1_ApoKeyList);
			},
			// ツアー一括　出発地表示用
			renderAreaApoWindow_27 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domAirportList2_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_24(depAreaList, m_tourModuleAirportList_domApoRelList['PD_832'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
			},
			// 国内ツアーホテル　宿泊地表示用
			renderAreaApoWindow_28 : function(options) {
				var depAreaList = _.chain(m_tourModuleAirportList_domDirectionDistrictRegionList1_areaList).filter(function(e) {
					return e.value !== 'NOP'
				}).uniq().value()
				this.renderAreaWindow_25(depAreaList, m_tourModuleAirportList_domApoRelList['PD_833'], options);
				this.setClickHandler(m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
			},

			renderAreaWindow_1 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 主要な空港選択肢を追加
				targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
					area: '00',
					text: '主要な空港'
				}));

				var onlineAirports = [];
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDp;
						var list = _.chain(m_tourModuleAirportList_IndexInfo)
							.map(function(v, k) { return [k, v.text] }).value();
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});
						//オンライン空港(主要空港)があれば入れる
						var _onlineAirports = _.findWhere(apoReList[currentArea],{airportCount: '0'});
						if (_onlineAirports && iiLength > 1 && ii == 1) {
							var eachOnlineAirports = {area : currentArea, letters :
								_.chain(_onlineAirports.airports).select(function(c, i) {
									return c !== 'NOP' && i !== 0;//区切りとラベルは除外
								}).value()};
							onlineAirports.push(eachOnlineAirports);
						}
						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: list
						}));
					}
				}
				var onlineAirportsHtml = [];
				template = this.templates.mainApoList;
				onlineAirportsHtml.push(template({
					cid: apoReList['01'][0].cid,
					area: '00',
					areaname: '主要な空港',
					onlineAirports: onlineAirports
				}));
				tempHtml = $tour_module_jq(onlineAirportsHtml.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},

			renderAreaWindow_2 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					var sortFunc = this.sortAirports
					$tour_module_jq.each(apoReList[currentArea], function() {
						sortFunc(this.airports, m_tourModuleAirportList_ApoKeyListDp);
					});
					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_ApoKeyListDp);

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.country;
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});
						//オンライン空港(主要空港)があれば入れる
						var _onlineAirports = _.findWhere(apoReList[currentArea],{airportCount: '0'});
						if (_onlineAirports && iiLength > 1 && ii == 1) {
							onlineAirports = 
								_.chain(_onlineAirports.airports).select(function(c, i) {
								return c !== 'NOP' && i !== 0;//区切りとラベルは除外
							}).value();
						} else if (currentArea === '2'){ //*Hawai hard coded
							onlineAirports = [airports.shift()];
						} else {
							onlineAirports = [];
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid.match(/\b(cid-)/g);});
						if (_countries.length > 1) {
							if (ii == 1) {
								countries = _countries;
							} else {
								countries = [];
							}
						} else {
							countries = [];
						}
						areaname = m_tourModuleAirportList_ApoKeyListDp[currentAirports.cid]['text'];
						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: onlineAirports,
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));
					}
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				return tempHtml.html();
			},

			renderAreaWindow_3 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 選択中の空港Letterから所属するエリアを取得
				var filterAirport = options.targetObject.parents('form').find(options.filterAirportClass).val();
				var filterArea = _.chain(m_tourModuleAirportList_ApoRelList['PD_813']).pairs().find(function(o) {
					return _.chain(o[1]).pluck('airports').flatten().find(function(q) {
						return filterAirport == q;
					}).value();
				}).first().value();

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }
					currentArea = areaList[i]['value'];

					if (currentArea !== filterArea) { continue; }

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDp;
						var list = _.chain(m_tourModuleAirportList_IndexInfo)
							.map(function(v, k) { return [k, v.text] }).value();
						airports = _.select(currentAirports.airports, function(c, i) {

							var result = false;

							//区切りとラベルは除外
							if (c !== 'NOP' && i !== 0) {
								$tour_module_jq.each(m_tourModuleAirportList_SecondStayRelList, function(){
									if (c === this['city_cd'] && filterAirport === this['first_stay_city_cd']) {
										result = true;
									}
								});
							}
							return result;
						});

						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid.match(/\b(cid-)/g);});
							if (_countries.length > 1) {
								if (ii == 1) {
									countries = _countries;
								} else {
									countries = [];
								}
								areaname = m_tourModuleAirportList_ApoKeyListDp[currentAirports.cid]['text'];
							} else {
								countries = [];
								areaname = '';
							}
							html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: list
						}));
					}
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				return tempHtml.html();
			},

			renderAreaWindow_4 : function(areaList, apoReList, options) {
				var currentArea = '00';
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var template = html = [];
				for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
					var currentAirports =  apoReList[currentArea][ii];

					if (currentAirports.airportCount === '0') { continue; }
					template =  this.templates.cityListPkg;
					var list = _.chain(m_tourModuleAirportList_IndexInfo)
						.map(function(v, k) { return [k, v.text] }).value();
					airports = _.select(currentAirports.airports, function(c, i) {
						return c !== 'NOP' && i !== 0 //区切りとラベルは除外
					});
					//地域に2つ以上の国が存在する場合の処理
					var _countries = _.filter(apoReList[currentArea],function(x) {
						return x.cid.match(/\b(cid-)/g);});
						if (_countries.length > 1) {
							if (ii == 1) {
								countries = _countries;
							} else {
								countries = [];
							}
							areaname = m_tourModuleAirportList_ApoKeyListPkg[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}
						html.push(template({
						cid: currentAirports.cid,
						city: currentAirports.cid,
						area: currentArea,
						areaname: areaname,
						countries: countries,
						onlineAirports: [],
						airports: airports,
						double: (iiLength > 2),
						list: list
					}));
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				return tempHtml.html();
			},

			renderAreaWindow_5 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 出発地に選択中の空港Letterを取得
				var filterAirport = options.targetObject.parents('form').find(options.filterAirportClass).val();

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					var sortFunc = this.sortAirports
					$tour_module_jq.each(apoReList[currentArea], function() {
						sortFunc(this.airports, m_tourModuleAirportList_ApoKeyListPkg);
					});
					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_ApoKeyListPkg);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];
						var isIdx = false;

						var airports = [];
						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = _.select(currentAirports.airports, function(c, i) {
							var result = false;
							//区切りとラベルは除外
							if (c !== 'NOP') {
								$tour_module_jq.each(m_tourModuleAirportList_TargetCityRelList[filterAirport], function(){
									var targetCity = String(this);
									if (c === targetCity || i === 0) {
										result = true;
									}
								});
							}
							return result;
						});

						if (filteredAirports.length < 2) { continue; }

						countryListTemplate =  this.templates.countryList;
						if (isIdx) {
							var list = _.chain(m_tourModuleAirportList_IndexInfo)
									.map(function(v, k) { return [k, v.text] }).value();
							template =  this.templates.cityListPkg;
							var airportsObj = _.chain(filteredAirports)
									.select(function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							})
							.groupBy(function(tlettr) { return m_tourModuleAirportList_ApoKeyListPkg[tlettr]['apoIndex']})
							.value();
							var airportsKeys = Object.keys(airportsObj);
							$tour_module_jq.each(airportsKeys, function(){
								var airportsItems = airportsObj[this];
								airports = airports.concat(airportsItems);
							});
						} else {
							template =  this.templates.cityListPkg;
							airports = _.select(filteredAirports, function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							});
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid === filteredAirports[0];
						});
						var countries = [];
						if (_countries.length > 0) {
							if (!filteredAirports[0].match(/\b(cid-0)/g) && filteredAirports.length < 3) {continue;}
							countries.push(_countries[0]);
							areaname = m_tourModuleAirportList_ApoKeyListPkg[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}

						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));

						$tour_module_jq.each(countries, function(){
							var cidVal;
							if (this.cid.match(/\b(cid-0)/g)) {
								cidVal = "";
							} else {
								cidVal = this.cid;
							}
							countryListHtml.push(countryListTemplate({
								cid: this.cid,
								cidVal: cidVal,
								area: currentArea,
								areaname: areaname,
								countries: [this],
								onlineAirports: [],
								airports: [],
								double: (iiLength > 2),
								list: []
							}));
						});

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-0)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}

					if (isExistAirport) {
						targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
							area: currentArea,
							text: areaList[i]['text']
						}));
					}
				}
				tempHtml = $tour_module_jq(countryListHtml.join(''));
				targetBaloon.find('.mod-balloon-set_box_countryList').append(tempHtml); // Country
				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_cityList').append(tempHtml); // City
				return tempHtml.html();
			},

			renderAreaWindow_6 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 出発地に選択中の空港Letterを取得
				var filterAirport = options.targetObject.parents('form').find(options.filterAirportClass).val();

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }
					currentArea = areaList[i]['value'];

					var sortFunc = this.sortAirports
					$tour_module_jq.each(apoReList[currentArea], function() {
						sortFunc(this.airports, m_tourModuleAirportList_ApoKeyListPkg);
					});
					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_ApoKeyListPkg);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = _.select(currentAirports.airports, function(c, i) {
							var result = false;
							//区切りとラベルは除外
							if (c !== 'NOP') {
								$tour_module_jq.each(m_tourModuleAirportList_TargetCityRelList[filterAirport], function(){
									var targetCity = String(this);
									if (c === targetCity || i === 0) {
										result = true;
									}
								});
							}
							return result;
						});
						if (filteredAirports.length < 2) { continue; }

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-0)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}
					if (isExistAirport) {
					// 過渡期チェック
						if (m_inttourModuleCommon_isTransitionPeriod() &&
							$tour_module_jq.inArray(currentArea, m_inttourTransitionPeriodOptions.transitionToReliefPageAreas) >= 0) {
							targetBaloon.find('.mod-balloon-set_single ul').append(this.templates.areaALink({
								area: currentArea,
								text: areaList[i]['text']
							}));
						} else {
							targetBaloon.find('.mod-balloon-set_single ul').append(this.templates.areaLink({
								area: currentArea,
								text: areaList[i]['text']
							}));
						}
					}
				}
			},

			renderAreaWindow_101 : function(areaList, apoReList, options) {
				var currentArea = '00';
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var template = html = [];
				for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
					var currentAirports =  apoReList[currentArea][ii];

					if (currentAirports.airportCount === '0') { continue; }
					template =  this.templates.cityListInttourOptionArea;
					var list = _.chain(m_tourModuleAirportList_IndexInfo)
						.map(function(v, k) { return [k, v.text] }).value();
					airports = _.select(currentAirports.airports, function(c, i) {
						return c !== 'NOP' && i !== 0 //区切りとラベルは除外
					});
					//地域に2つ以上の国が存在する場合の処理
					var _countries = _.filter(apoReList[currentArea],function(x) {
						return x.cid.match(/\b(cid-)/g);});
						if (_countries.length > 1) {
							if (ii == 1) {
								countries = _countries;
							} else {
								countries = [];
							}
							areaname = m_tourModuleAirportList_ApoKeyListInttourOptionArea[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}
						html.push(template({
						cid: currentAirports.cid,
						city: currentAirports.cid,
						area: currentArea,
						areaname: areaname,
						countries: countries,
						onlineAirports: [],
						airports: airports,
						double: (iiLength > 2),
						list: list
					}));
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				return tempHtml.html();
			},

			renderAreaWindow_102 : function(areaList, apoReList, options) {
				var currentArea = '00';
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var template = html = [];
				for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
					var currentAirports =  apoReList[currentArea][ii];

					if (currentAirports.airportCount === '0') { continue; }
					template =  this.templates.cityListInttourOptionTheme;
					var list = _.chain(m_tourModuleAirportList_IndexInfo)
						.map(function(v, k) { return [k, v.text] }).value();
					airports = _.select(currentAirports.airports, function(c, i) {
						return c !== 'NOP' && i !== 0 //区切りとラベルは除外
					});
					//地域に2つ以上の国が存在する場合の処理
					var _countries = _.filter(apoReList[currentArea],function(x) {
						return x.cid.match(/\b(cid-)/g);});
						if (_countries.length > 1) {
							if (ii == 1) {
								countries = _countries;
							} else {
								countries = [];
							}
							areaname = m_tourModuleAirportList_ApoKeyListInttourOptionTheme[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}
						html.push(template({
						cid: currentAirports.cid,
						city: currentAirports.cid,
						area: currentArea,
						areaname: areaname,
						countries: countries,
						onlineAirports: [],
						airports: airports,
						double: (iiLength > 2),
						list: list
					}));
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				return tempHtml.html();
			},

			renderAreaWindow_11 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 主要な空港選択肢を追加
				targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
					area: '00',
					text: '主要な空港'
				}));

				var onlineAirports = [];

				var onlineAreaList = $tour_module_jq.extend(true, [], areaList);
				onlineAreaList = _.chain(onlineAreaList).select(function(c, i) {
					return !_.isEmpty(c.mainIndex); // 主要な空港用インデックスが空のデータは除外
				}).value();
				var sortFunc = this.sortAirportsOnlineAirportArea;
				sortFunc(onlineAreaList);
				for (var i = 0, iLength = onlineAreaList.length; i < iLength; i++) {
					if (onlineAreaList[i]['value'] === '0') { continue; }
					currentArea = onlineAreaList[i]['value'];
					//オンライン空港(主要空港)があれば入れる
					var _onlineAirports = _.findWhere(apoReList[currentArea],{airportCount: '0'});
					if (_onlineAirports && apoReList[currentArea].length > 1) {
						var eachOnlineAirports = {area : currentArea, letters :
							_.chain(_onlineAirports.airports).select(function(c, i) {
								return c !== 'NOP' && i !== 0;//区切りとラベルは除外
							}).value()};
						onlineAirports.push(eachOnlineAirports);
					}
				}

				// 地域毎の空港一覧を追加
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomAirportList1;
						var list = _.chain(m_tourModuleAirportList_domIndexInfo)
							.map(function(v, k) { return [k, v.text] }).value();
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});
						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: list
						}));
					}
				}
				var onlineAirportsHtml = [];
				template = this.templates.mainApoListDomAirportList1;
				onlineAirportsHtml.push(template({
					cid: apoReList['01'][0].cid,
					area: '00',
					areaname: '主要な空港',
					onlineAirports: onlineAirports
				}));
				tempHtml = $tour_module_jq(onlineAirportsHtml.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},
			renderAreaWindow_12 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 主要な空港選択肢を追加
				targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
					area: '00',
					text: '主要な空港'
				}));

				var onlineAirports = [];

				var onlineAreaList = $tour_module_jq.extend(true, [], areaList);
				onlineAreaList = _.chain(onlineAreaList).select(function(c, i) {
					return !_.isEmpty(c.mainIndex); // 主要な空港用インデックスが空のデータは除外
				}).value();
				var sortFunc = this.sortAirportsOnlineAirportArea;
				sortFunc(onlineAreaList);
				for (var i = 0, iLength = onlineAreaList.length; i < iLength; i++) {
					if (onlineAreaList[i]['value'] === '0') { continue; }
					currentArea = onlineAreaList[i]['value'];
					//オンライン空港(主要空港)があれば入れる
					var _onlineAirports = _.findWhere(apoReList[currentArea],{airportCount: '0'});
					if (_onlineAirports && apoReList[currentArea].length > 1) {
						var eachOnlineAirports = {area : currentArea, letters :
							_.chain(_onlineAirports.airports).select(function(c, i) {
								return c !== 'NOP' && i !== 0;//区切りとラベルは除外
							}).value()};
						onlineAirports.push(eachOnlineAirports);
					}
				}

				// 地域毎の空港一覧を追加
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomAirportList1;
						var list = _.chain(m_tourModuleAirportList_domIndexInfo)
							.map(function(v, k) { return [k, v.text] }).value();
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});
						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: list
						}));
					}
				}
				var onlineAirportsHtml = [];
				template = this.templates.mainApoListDomAirportList1;
				onlineAirportsHtml.push(template({
					cid: apoReList['01'][0].cid,
					area: '00',
					areaname: '主要な空港',
					onlineAirports: onlineAirports
				}));
				tempHtml = $tour_module_jq(onlineAirportsHtml.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},
			renderAreaWindow_13 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				// 選択中の空港Letterから所属するマルチ空港を取得
				var filterAirport = options.targetObject.parents('form').find(options.filterAirportClass).val();
				var filterArea = _.chain(m_tourModuleAirportList_domApoRelList['PD_831']).pairs().find(function(o) {
					return _.chain(o[1]).pluck('airports').flatten().find(function(q) {
						return filterAirport == q;
					}).value();
				}).first().value();

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }
					currentArea = areaList[i]['value'];

					if (currentArea !== filterArea) { continue; }

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomAirportList1;
						var list = _.chain(m_tourModuleAirportList_IndexInfo)
							.map(function(v, k) { return [k, v.text] }).value();
						// マルチ空港取得
						multiNumeber = m_tourModuleAirportList_domAirportList1_ApoKeyList[filterAirport].multi;
						if(multiNumeber === undefined || multiNumeber <= 0) { continue; }
						airports = m_tourModuleAirportList_domAirportList1_multiAirportList[multiNumeber];

						//2つ以上の空港が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid.match(/\b(cid-)/g);});
							if (_countries.length > 1) {
								if (ii == 1) {
									countries = _countries;
								} else {
									countries = [];
								}
								areaname = m_tourModuleAirportList_domAirportList1_ApoKeyList[currentAirports.cid]['text'];
							} else {
								countries = [];
								areaname = '';
							}
							html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: list
						}));
					}
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				return tempHtml.html();
			},
			
			renderAreaWindow_14 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

//					var sortFunc = this.sortAirports
//					$tour_module_jq.each(apoReList[currentArea], function() {
//						sortFunc(this.airports, m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
//					});
//					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];
						var isIdx = false;

						var airports = [];
						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = currentAirports.airports; // 絞込みなし

						if (filteredAirports.length < 2) { continue; }

						countryListTemplate =  this.templates.countryListDomDirectionDistrictRegionList1;
						if (isIdx) {
							var list = _.chain(m_tourModuleAirportList_IndexInfo)
									.map(function(v, k) { return [k, v.text] }).value();
							template =  this.templates.cityListDomDirectionDistrictRegionList1;
							var airportsObj = _.chain(filteredAirports)
									.select(function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							})
							.groupBy(function(tlettr) { return m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[tlettr]['apoIndex']})
							.value();
							var airportsKeys = Object.keys(airportsObj);
							$tour_module_jq.each(airportsKeys, function(){
								var airportsItems = airportsObj[this];
								airports = airports.concat(airportsItems);
							});
						} else {
							template =  this.templates.cityListDomDirectionDistrictRegionList1;
							airports = _.select(filteredAirports, function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							});
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid === filteredAirports[0];
						});
						var countries = [];
						if (_countries.length > 0) {
							if (!filteredAirports[0].match(/\b(cid-S)/g) && filteredAirports.length < 3) {continue;}
							countries.push(_countries[0]);
							areaname = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}

						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));

						$tour_module_jq.each(countries, function(){
							var cidVal;
							if (this.cid.match(/\b(cid-S)/g)) {
								cidVal = "";
							} else {
								cidVal = this.cid;
							}
							countryListHtml.push(countryListTemplate({
								cid: this.cid,
								cidVal: cidVal,
								area: currentArea,
								areaname: areaname,
								countries: [this],
								onlineAirports: [],
								airports: [],
								double: (iiLength > 2),
								list: []
							}));
						});

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-S)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}

					if (isExistAirport) {
						targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
							area: currentArea,
							text: areaList[i]['text']
						}));
					}
				}
				tempHtml = $tour_module_jq(countryListHtml.join(''));
				targetBaloon.find('.mod-balloon-set_box_countryList').append(tempHtml); // Country
				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_cityList').append(tempHtml); // City
				return tempHtml.html();
			},
			
			renderAreaWindow_15 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var onlineAirports = [];
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomAirportList2;
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});

						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));
					}
				}

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},

			renderAreaWindow_16 : function(areaList, apoReList, options) {
				var currentArea = '00';
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var template = html = [];
				for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
					var currentAirports =  apoReList[currentArea][ii];

					if (currentAirports.airportCount === '0') { continue; }
					template =  this.templates.cityListTpl_domPkgArrList1;
					var list = _.chain(m_tourModuleAirportList_IndexInfo)
						.map(function(v, k) { return [k, v.text] }).value();
					airports = _.select(currentAirports.airports, function(c, i) {
						return c !== 'NOP' && i !== 0 //区切りとラベルは除外
					});
					//地域に2つ以上の国が存在する場合の処理
					var _countries = _.filter(apoReList[currentArea],function(x) {
						return x.cid.match(/\b(cid-)/g);});
						if (_countries.length > 1) {
							if (ii == 1) {
								countries = _countries;
							} else {
								countries = [];
							}
							areaname = m_tourModuleAirportList_domPkgArrList1_ApoKeyList[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}
						html.push(template({
						cid: currentAirports.cid,
						city: currentAirports.cid,
						area: currentArea,
						areaname: areaname,
						countries: countries,
						onlineAirports: [],
						airports: airports,
						double: (iiLength > 2),
						list: list
					}));
				}
				tempHtml = $tour_module_jq(html.join(''));
				tempHtml.find('.m_majorAirport dt').text(options.m_tourModuleParts_airportListTableLanguageProperties.majorAirport);
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				return tempHtml.html();
			},
			renderAreaWindow_17 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

//					var sortFunc = this.sortAirports
//					$tour_module_jq.each(apoReList[currentArea], function() {
//						sortFunc(this.airports, m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
//					});
//					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];
						var isIdx = false;

						var airports = [];
						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = currentAirports.airports; // 絞込みなし

						if (filteredAirports.length < 2) { continue; }

						countryListTemplate =  this.templates.countryListDomDirectionDistrictRegionList1;
						if (isIdx) {
							var list = _.chain(m_tourModuleAirportList_IndexInfo)
									.map(function(v, k) { return [k, v.text] }).value();
							template =  this.templates.cityListDomDirectionDistrictRegionList1;
							var airportsObj = _.chain(filteredAirports)
									.select(function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							})
							.groupBy(function(tlettr) { return m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[tlettr]['apoIndex']})
							.value();
							var airportsKeys = Object.keys(airportsObj);
							$tour_module_jq.each(airportsKeys, function(){
								var airportsItems = airportsObj[this];
								airports = airports.concat(airportsItems);
							});
						} else {
							template =  this.templates.cityListDomDirectionDistrictRegionList1;
							airports = _.select(filteredAirports, function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							});
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid === filteredAirports[0];
						});
						var countries = [];
						if (_countries.length > 0) {
							if (!filteredAirports[0].match(/\b(cid-S)/g) && filteredAirports.length < 3) {continue;}
							countries.push(_countries[0]);
							areaname = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}

						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));

						$tour_module_jq.each(countries, function(){
							var cidVal;
							if (this.cid.match(/\b(cid-S)/g)) {
								cidVal = "";
							} else {
								cidVal = this.cid;
							}
							countryListHtml.push(countryListTemplate({
								cid: this.cid,
								cidVal: cidVal,
								area: currentArea,
								areaname: areaname,
								countries: [this],
								onlineAirports: [],
								airports: [],
								double: (iiLength > 2),
								list: []
							}));
						});

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-S)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}

					if (isExistAirport) {
						targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
							area: currentArea,
							text: areaList[i]['text']
						}));
					}
				}
				tempHtml = $tour_module_jq(countryListHtml.join(''));
				targetBaloon.find('.mod-balloon-set_box_countryList').append(tempHtml); // Country
				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_cityList').append(tempHtml); // City
				return tempHtml.html();
			},
			renderAreaWindow_18 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var onlineAirports = [];
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomAirportList3;
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});

						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));
					}
				}

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},
			
			renderAreaWindow_19 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

//					var sortFunc = this.sortAirports
//					$tour_module_jq.each(apoReList[currentArea], function() {
//						sortFunc(this.airports, m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList);
//					});
//					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];
						var isIdx = false;

						var airports = [];
						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = currentAirports.airports; // 絞込みなし

						if (filteredAirports.length < 2) { continue; }

						countryListTemplate =  this.templates.countryListDomDirectionDistrictRegionList2;
						if (isIdx) {
							var list = _.chain(m_tourModuleAirportList_IndexInfo)
									.map(function(v, k) { return [k, v.text] }).value();
							template =  this.templates.cityListDomDirectionDistrictRegionList2;
							var airportsObj = _.chain(filteredAirports)
									.select(function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							})
							.groupBy(function(tlettr) { return m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[tlettr]['apoIndex']})
							.value();
							var airportsKeys = Object.keys(airportsObj);
							$tour_module_jq.each(airportsKeys, function(){
								var airportsItems = airportsObj[this];
								airports = airports.concat(airportsItems);
							});
						} else {
							template =  this.templates.cityListDomDirectionDistrictRegionList2;
							airports = _.select(filteredAirports, function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							});
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid === filteredAirports[0];
						});
						var countries = [];
						if (_countries.length > 0) {
							if (!filteredAirports[0].match(/\b(cid-S)/g) && filteredAirports.length < 3) {continue;}
							countries.push(_countries[0]);
							areaname = m_tourModuleAirportList_domDirectionDistrictRegionList2_ApoKeyList[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}

						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));

						$tour_module_jq.each(countries, function(){
							var cidVal;
							if (this.cid.match(/\b(cid-S)/g)) {
								cidVal = "";
							} else {
								cidVal = this.cid;
							}
							countryListHtml.push(countryListTemplate({
								cid: this.cid,
								cidVal: cidVal,
								area: currentArea,
								areaname: areaname,
								countries: [this],
								onlineAirports: [],
								airports: [],
								double: (iiLength > 2),
								list: []
							}));
						});

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-S)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}

					if (isExistAirport) {
						targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
							area: currentArea,
							text: areaList[i]['text']
						}));
					}
				}
				tempHtml = $tour_module_jq(countryListHtml.join(''));
				targetBaloon.find('.mod-balloon-set_box_countryList').append(tempHtml); // Country
				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_cityList').append(tempHtml); // City
				return tempHtml.html();
			},
			
			renderAreaWindow_20 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

//					var sortFunc = this.sortAirports
//					$tour_module_jq.each(apoReList[currentArea], function() {
//						sortFunc(this.airports, m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList);
//					});
//					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];
						var isIdx = false;

						var airports = [];
						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = currentAirports.airports; // 絞込みなし

						if (filteredAirports.length < 2) { continue; }

						countryListTemplate =  this.templates.countryListDomDirectionDistrictRegionList3;
						if (isIdx) {
							var list = _.chain(m_tourModuleAirportList_IndexInfo)
									.map(function(v, k) { return [k, v.text] }).value();
							template =  this.templates.cityListDomDirectionDistrictRegionList3;
							var airportsObj = _.chain(filteredAirports)
									.select(function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							})
							.groupBy(function(tlettr) { return m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[tlettr]['apoIndex']})
							.value();
							var airportsKeys = Object.keys(airportsObj);
							$tour_module_jq.each(airportsKeys, function(){
								var airportsItems = airportsObj[this];
								airports = airports.concat(airportsItems);
							});
						} else {
							template =  this.templates.cityListDomDirectionDistrictRegionList3;
							airports = _.select(filteredAirports, function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							});
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid === filteredAirports[0];
						});
						var countries = [];
						if (_countries.length > 0) {
							if (!filteredAirports[0].match(/\b(cid-S)/g) && filteredAirports.length < 3) {continue;}
							countries.push(_countries[0]);
							areaname = m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}

						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));

						$tour_module_jq.each(countries, function(){
							var cidVal;
							if (this.cid.match(/\b(cid-S)/g)) {
								cidVal = "";
							} else {
								cidVal = this.cid;
							}
							countryListHtml.push(countryListTemplate({
								cid: this.cid,
								cidVal: cidVal,
								area: currentArea,
								areaname: areaname,
								countries: [this],
								onlineAirports: [],
								airports: [],
								double: (iiLength > 2),
								list: []
							}));
						});

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-S)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}

					if (isExistAirport) {
						targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
							area: currentArea,
							text: areaList[i]['text']
						}));
					}
				}
				tempHtml = $tour_module_jq(countryListHtml.join(''));
				targetBaloon.find('.mod-balloon-set_box_countryList').append(tempHtml); // Country
				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_cityList').append(tempHtml); // City
				return tempHtml.html();
			},
			
			renderAreaWindow_21 : function(areaList, apoReList, options) {
				// 変数初期化
				var template = html = [];	// 追記用HTML
				
				// 吹き出し対象取得
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));
				
				// 親リストから絞込みコードを取得
				var filterAirport = options.targetObject.parents('form').find(options.filterAirportClass).val();
				
				// 絞込みコードが所属する地域コードを親リストから取得する
				var currentArea = _.chain(m_tourModuleAirportList_domApoRelList['PD_837']).pairs().find(function(o) {
					return _.chain(o[1]).pluck('airports').flatten().find(function(q) {
						return filterAirport == q;
					}).value();
				}).first().value();
				
				// リレーションデータを取得(添え字0は親データなので添え字1から取得)
				var currentAirports =  apoReList[currentArea][1];
				
				// 吹き出し追加用HTMLレイアウト取得
				template =  this.templates.cityListDomSalesOffice1;
				
				// 営業所情報取得
				var officeInfo = m_tourModuleAirportList_domAirportList3_ApoKeyList[filterAirport].office;
				
				// 営業所情報取得できた場合は、リストに追加
				if(officeInfo !== undefined) { 
					// 子リストに表示する内容を取得
					airports = m_tourModuleAirportList_domAirportList3_officeInfo[officeInfo];

					// 追記用HTMLに追加
					html.push(template({
						cid: currentAirports.cid,
						area: currentArea,
						airports: airports
					}));

				}
				// 追記用HTMLを配列から文字列に変更
				tempHtml = $tour_module_jq(html.join(''));
				
				// 吹き出しを表示する所定の場所に追記
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				
				return tempHtml.html();
			},
			
			renderAreaWindow_22 : function(areaList, apoReList, options) {
				// 変数初期化
				var template = html = [];	// 追記用HTML
				
				// 吹き出し対象取得
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));
				
				// 親リストから絞込みコードを取得
				var filterAirport = options.targetObject.parents('form').find(options.filterAirportClass).val();
				
				// 絞込みコードが所属する地域コードを親リストから取得する
				var currentArea = _.chain(m_tourModuleAirportList_domApoRelList['PD_839']).pairs().find(function(o) {
					return _.chain(o[1]).pluck('airports').flatten().find(function(q) {
						return filterAirport == q;
					}).value();
				}).first().value();
				
				// リレーションデータのを取得(添え字0は親データなので添え字1から取得)
				var currentAirports =  apoReList[currentArea][1];
				
				// 吹き出し追加用HTMLレイアウト取得
				template =  this.templates.cityListDomSalesOffice2;
				
				// 営業所情報取得
				var officeInfo = m_tourModuleAirportList_domDirectionDistrictRegionList3_ApoKeyList[filterAirport].office;
				
				// 営業所情報取得できた場合は、リストに追加
				if(officeInfo !== undefined) { 
					// 子リストに表示する内容を取得
					airports = m_tourModuleAirportList_domDirectionDistrictRegionList3_officeInfo[officeInfo];

					// 追記用HTMLに追加
					html.push(template({
						cid: currentAirports.cid,
						area: currentArea,
						airports: airports
					}));

				}
				// 追記用HTMLを配列から文字列に変更
				tempHtml = $tour_module_jq(html.join(''));
				
				// 吹き出しを表示する所定の場所に追記
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City
				targetBaloon.find('.mod-balloon-set_box_inner').find('.m_APlist').show();
				
				return tempHtml.html();
			},
			
			renderAreaWindow_23 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var onlineAirports = [];
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomDirectionDistrictList1;
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});

						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));
					}
				}

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},

			renderAreaWindow_24 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				var onlineAirports = [];
				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

					targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
						area: currentArea,
						text: areaList[i]['text']
					}));

					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];

						if (currentAirports.airportCount === '0') { continue; }
						template =  this.templates.cityListDomAirportList2;
						airports = _.select(currentAirports.airports, function(c, i) {
							return c !== 'NOP' && i !== 0 //区切りとラベルは除外
						});

						html.push(template({
							cid: currentAirports.cid,
							city: currentAirports.cid,
							area: currentArea,
							areaname: '',
							countries: [],
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));
					}
				}

				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_inner').append(tempHtml); // City

				return tempHtml.html();
			},

			renderAreaWindow_25 : function(areaList, apoReList, options) {
				var currentArea = '';
				var template = html = [];
				var countryListTemplete = countryListHtml = [];
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				for (var i = 0, iLength = areaList.length; i < iLength; i++) {
					if (areaList[i]['value'] === '0') { continue; }

					currentArea = areaList[i]['value'];

//					var sortFunc = this.sortAirports
//					$tour_module_jq.each(apoReList[currentArea], function() {
//						sortFunc(this.airports, m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);
//					});
//					this.sortCountries(apoReList[currentArea], m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList);

					var isExistAirport = false;
					for (var ii = 0, iiLength = apoReList[currentArea].length; ii < iiLength; ii++) {
						var currentAirports =  apoReList[currentArea][ii];
						var isIdx = false;

						var airports = [];
						if (currentAirports.airportCount === '0') { continue; }
						var filteredAirports = currentAirports.airports; // 絞込みなし

						if (filteredAirports.length < 2) { continue; }

						countryListTemplate =  this.templates.countryListDomDirectionDistrictRegionList1;
						if (isIdx) {
							var list = _.chain(m_tourModuleAirportList_IndexInfo)
									.map(function(v, k) { return [k, v.text] }).value();
							template =  this.templates.cityListDomDirectionDistrictRegionList1;
							var airportsObj = _.chain(filteredAirports)
									.select(function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							})
							.groupBy(function(tlettr) { return m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[tlettr]['apoIndex']})
							.value();
							var airportsKeys = Object.keys(airportsObj);
							$tour_module_jq.each(airportsKeys, function(){
								var airportsItems = airportsObj[this];
								airports = airports.concat(airportsItems);
							});
						} else {
							template =  this.templates.cityListDomDirectionDistrictRegionList1;
							airports = _.select(filteredAirports, function(c, i) {
								return c !== 'NOP' && i !== 0 //区切りとラベルは除外
							});
						}
						//地域に2つ以上の国が存在する場合の処理
						var _countries = _.filter(apoReList[currentArea],function(x) {
							return x.cid === filteredAirports[0];
						});
						var countries = [];
						if (_countries.length > 0) {
							if (!filteredAirports[0].match(/\b(cid-S)/g) && filteredAirports.length < 3) {continue;}
							countries.push(_countries[0]);
							areaname = m_tourModuleAirportList_domDirectionDistrictRegionList1_ApoKeyList[currentAirports.cid]['text'];
						} else {
							countries = [];
							areaname = '';
						}

						html.push(template({
							cid: currentAirports.cid,
							area: currentArea,
							areaname: areaname,
							countries: countries,
							onlineAirports: [],
							airports: airports,
							double: (iiLength > 2),
							list: []
						}));

						$tour_module_jq.each(countries, function(){
							var cidVal;
							if (this.cid.match(/\b(cid-S)/g)) {
								cidVal = "";
							} else {
								cidVal = this.cid;
							}
							countryListHtml.push(countryListTemplate({
								cid: this.cid,
								cidVal: cidVal,
								area: currentArea,
								areaname: areaname,
								countries: [this],
								onlineAirports: [],
								airports: [],
								double: (iiLength > 2),
								list: []
							}));
						});

						// 出発地に選ばれた都市で選択可能地域を絞り込む
						if (filteredAirports[0].match(/\b(cid-S)/g) || filteredAirports.length < 3) {continue;}

						isExistAirport = true;
					}

					if (isExistAirport) {
						targetBaloon.find('.mod-balloon-set_parent ul').append(this.templates.areaLink({
							area: currentArea,
							text: areaList[i]['text']
						}));
					}
				}
				tempHtml = $tour_module_jq(countryListHtml.join(''));
				targetBaloon.find('.mod-balloon-set_box_countryList').append(tempHtml); // Country
				tempHtml = $tour_module_jq(html.join(''));
				targetBaloon.find('.mod-balloon-set_box_cityList').append(tempHtml); // City
				return tempHtml.html();
			},
			
			clearWindow : function() {
				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));
				targetBaloon.find('.mod-balloon-set_box ul, .mod-balloon-set_box_inner').empty();
			},

			sortCountries : function(countries, apoKeyList) {
				countries.sort(function(item01,item02){
					if (item01.cid.match(/\b(IDX-)/g)) {
						return -1;
					}
					if (item02.cid.match(/\b(IDX-)/g)) {
						return 1;
					}
					var name01 = apoKeyList[item01.cid]['kana'].replace(/(◆|<|>|●|\\*)/g, '');
					if (!name01) name01 = apoKeyList[item01.cid]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					var name02 = apoKeyList[item02.cid]['kana'].replace(/(◆|<|>|●|\\*)/g, '');
					if (!name02) name02 = apoKeyList[item02.cid]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					if( name01 < name02 ) return -1;
					if( name01 > name02 ) return 1;
					return 0;
				});
				return countries;
			},

			sortAirports : function(airports, apoKeyList) {
				airports.sort(function(item01, item02) {
					if (item01.match(/\b(IDX-)/g)) {
						return -1;
					}
					if (item02.match(/\b(IDX-)/g)) {
						return 1;
					}
					if (item01.match(/\b(cid-)/g)) {
						return -1;
					}
					if (item02.match(/\b(cid-)/g)) {
						return 1;
					}
					var name01 = apoKeyList[item01]['kana'].replace(/(◆|<|>|●|\\*)/g, '');
					if (!name01) name01 = apoKeyList[item01]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					var name02 = apoKeyList[item02]['kana'].replace(/(◆|<|>|●|\\*)/g, '');
					if (!name02) name02 = apoKeyList[item02]['text'].replace(/(◆|<|>|●|\\*)/g, '');
					if( name01 < name02 ) return -1;
					if( name01 > name02 ) return 1;
					return 0;
				});
				return airports;
			},
			
			sortAirportsOnlineAirportArea : function(airportArea) {
				airportArea.sort(function(apArea01, apArea02) {
					var result = 0;
					var areaIndex01 = apArea01.mainIndex;
					var areaIndex02 = apArea02.mainIndex;
					if(!_.isEmpty(areaIndex01) && !_.isEmpty(areaIndex02)){
						if(areaIndex01 < areaIndex02){
							result = -1;
						}else if(areaIndex01 > areaIndex02){
							result = 1;
						}
					}
					return result;
				});
				return airportArea;
			},

			setClickHandler : function(apoKeyList) {

				var targetBaloon = $tour_module_jq('#' + options.targetObject.find('.d_locationIcon').attr('data-balloon'));

				//Left index click 地域クリック時のイベントハンドラ
				targetBaloon.find('.mod-area-catlist_btn').on('click', function(e) {
					var target;
					var id = $tour_module_jq(this).attr('data-val');
					var modBalloonContInner = $tour_module_jq(this).parents('.mod-balloon-cont_inner');
					var currentAreaCountrys = modBalloonContInner.find('.mod-balloon-set_box_countryList').find('.' + id + '_apos');
					var isIndexd = _.find(currentAreaCountrys, function(e) {
						return $tour_module_jq(e).data('cid') == 'cid-US' }) ? true : false;

					// 過渡期チェック
					if (m_inttourModuleCommon_isTransitionPeriod() &&
						$tour_module_jq(this).hasClass('transition-to-relief')) {
						window.open(m_tourModuleCommon_ASW_DOMAIN + m_inttourTransitionPeriodOptions.transitionToReliefURL, '_blank');
					}

					// 地域のみの吹き出し時
					if (targetBaloon.find('.mod-balloon-set_box_inner').find('li').length === 0) {
						var data = $tour_module_jq(this).data();
						if (options.targetDiv == options.m_targetObject) {
							var eo = $tour_module_jq.Event('onChangeAirport', {
								moduleRootBox:options.targetObject,
								beforeArea:options.m_targetObject.find(options.targetAreaClass).val(),
								beforeCountry:options.m_targetObject.find(options.targetCountryClass).val(),
								beforeAirport:options.m_targetObject.find(options.targetAirportClass).val(),
								afterArea:data.val,
								afterCountry:'',
								afterAirport:''});

							var beforeArea = options.m_targetObject.find(options.targetAreaClass).val();
							options.m_targetObject.find(options.targetAreaClass).val(data.val);
							options.m_targetObject.find(options.targetCountryClass).val('');
							options.m_targetObject.find(options.targetAirportClass).val('');
							if(options.m_targetObject.find("span" + options.targetInputClass).length !== 0){
								options.m_targetObject.find("span" + options.targetInputClass).text('');
							}else{
								options.m_targetObject.find(options.targetInputClass).val('');
							}
							var areaId = m_tourModuleAirportList_ApoRelList['PD_814'][data.val][0].cid;
							var areaName = apoKeyList[areaId]['text'].replace(/(◆|<|>|●|\\*)/g, '');
							if(options.m_targetObject.find("span" + options.targetInputClass).length !== 0){
								options.m_targetObject.find("span" + options.targetInputClass).text(areaName);
							}else{
								if (!(m_inttourModuleCommon_isTransitionPeriod() &&
										$tour_module_jq(this).hasClass('transition-to-relief'))) {
									options.m_targetObject.find(options.targetInputClass).val(areaName);
								}
							}
							options.m_targetObject.find(options.targetAirportClass).trigger(eo);
							if (beforeArea !== data.val) {
								options.m_targetObject.find(options.targetAirportClass).trigger('onChangeParentArea');
							}
						}
						return true;
					}

					targetBaloon.find('.mod-balloon-set_box_inner > div').hide();
					targetBaloon.find('.mod-balloon-set_parent').find('li').removeClass('current');
					modBalloonContInner.find('.mod-balloon-set_box_countryList').find('li').removeClass('current');
					modBalloonContInner.find('.mod-balloon-set_box_cityList').find('li').removeClass('current');
					$tour_module_jq(this).addClass('current');

					currentAreaCountrys.show();
					var targetCid = currentAreaCountrys.first().find('li:first').attr('data-val');
					var currentCountryCities;
					if (targetCid) {
						// 国が選択されている場合は指定の国配下の都市一覧を取得
						currentCountryCities = modBalloonContInner.find('.mod-balloon-set_box_cityList').find('[data-cid=' + targetCid + ']');
					} else {
						// 国が選択されていない場合は吹き出しに国自体のデータが存在するか存在しないかによって処理を切り替え
						if (currentAreaCountrys.length > 0) {
							// 国データが存在する場合は選択地域配下の先頭の国データ配下の都市一覧を取得
							currentCountryCities = modBalloonContInner.find('.mod-balloon-set_box_cityList').find('.' + id + '_apos:first');
						} else {
							// 国データが存在しない場合は選択地域配下のすべての都市一覧を取得
							currentCountryCities = modBalloonContInner.find('.mod-balloon-set_box_cityList').find('.' + id + '_apos');
						}
					}
					currentCountryCities.show();

					if ($tour_module_jq(this).parents('.is-sp').length === 0) {
						currentAreaCountrys.first().find('li:first').addClass('current');
						currentCountryCities.find('li:first').addClass('current');
					}

					// スクロールボタン表示判定
					var $airportListContainer = targetBaloon.find('.mod-balloon-set_child');
					m_tourModuleAirportList_setAreaSelectScroll($airportListContainer);

					targetBaloon.find('.mod-balloon-set_box_inner').scrollTop(0);
					var $airportBox = $airportListContainer.find('div.mod-balloon-set_box');
					if($airportBox.scrollTop() != 0){
						$airportBox.scrollTop(0);
					}
					
					// 目的地の場合は都市スクロールボタンを削除
					var arrvCheck = targetBaloon.find('.mod-balloon-set_gchild').length;
					if(arrvCheck > 0){
						var $cityListContainer = targetBaloon.find('.mod-balloon-set_gchild');
						m_tourModuleAirportList_setAreaSelectScroll($cityListContainer);
					}

					return true;
				});

				//Country Click
				targetBaloon.find('.m_COUNTRYlist li').on('click', function(e) {
					var data = $tour_module_jq(this).data();
					var dataVal = data.val;
					var area = $tour_module_jq(this).parents('.m_COUNTRYlist').attr('data-area');
					var cid = $tour_module_jq(this).parents('.m_COUNTRYlist').attr('data-cid');

					if (_.isEmpty(data.val)) {
						var eo = $tour_module_jq.Event('onChangeAirport', {
							moduleRootBox:options.targetObject,
							beforeArea:options.m_targetObject.find(options.targetAreaClass).val(),
							beforeCountry:options.m_targetObject.find(options.targetCountryClass).val(),
							beforeAirport:options.m_targetObject.find(options.targetAirportClass).val(),
							afterArea:area,
							afterCountry:'',
							afterAirport:''});

						var beforeArea = options.m_targetObject.find(options.targetAreaClass).val();
						options.m_targetObject.find(options.targetAreaClass).val(area);
						options.m_targetObject.find(options.targetCountryClass).val('');
						options.m_targetObject.find(options.targetAirportClass).val('');

						var selectedName = options.targetObject.m_tourModuleAirportList_getSelectedName(area, '', '', options.layoutType);
						var spanObj = options.m_targetObject.find("span" + options.targetInputClass);
						if(spanObj.length !== 0){
							spanObj.text(selectedName);
						}else{
							options.m_targetObject.find(options.targetInputClass).val(selectedName);
						}

						var inputArea = options.m_targetObject.find(options.targetAreaClass).val();
						var inputCountry = options.m_targetObject.find(options.targetCountryClass).val();
						var inputApo = options.m_targetObject.find(options.targetAirportClass).val();

						options.m_targetObject.find(options.targetAirportClass).trigger(eo);
						if (beforeArea !== area) {
							options.m_targetObject.find(options.targetAirportClass).trigger('onChangeParentArea');
						}

						if(options.m_targetObject.find("span" + options.targetInputClass).length !== 0){
							options.m_targetObject.find(options.targetInputClass).parent().focus();
							options.m_targetObject.find(options.targetInputClass).parent().blur();
						}else{
							options.m_targetObject.find(options.targetInputClass).focus();
							options.m_targetObject.find(options.targetInputClass).blur();
						}
					}

					targetBaloon.find('.mod-balloon-set_box_cityList > div').hide();
					$tour_module_jq(this).parents('.mod-balloon-set_box_countryList').find('li').removeClass('current');
					$tour_module_jq(this).addClass('current');
					targetBaloon.find('.mod-balloon-set_box_cityList').find('[data-cid=' + cid + ']').show();
					targetBaloon.find('.mod-balloon-set_box_cityList').find('[data-cid=' + cid + '] li:first').addClass('current');
					
					// スクロールボタン表示判定
					var $cityListContainer = targetBaloon.find('.mod-balloon-set_gchild');
					m_tourModuleAirportList_setAreaSelectScroll($cityListContainer);
					var $cityBox = $cityListContainer.find('.mod-balloon-set_box');
					if($cityBox.scrollTop() != 0){
						$cityBox.scrollTop(0);
					}

					return true;
				});

				//Top index click
				targetBaloon.find('#airport-tab-area a').on('click', function(e) {
					e.preventDefault();
					var data = $tour_module_jq(this).data();
					$tour_module_jq('div.' + data.target + '_apos').show().find('.m_cityOfCountry').hide();
					$tour_module_jq('#airport-tab-area li').removeClass('d_selected');
					$tour_module_jq(this).parent('li').addClass('d_selected');
					switch (data.index) {
					case 'ALL':
						$tour_module_jq('div.' + data.target + '_apos:not([data-cid=cid-' + data.index + '])').show();
					$tour_module_jq('div.' + data.target + '_apos').show().find(':hidden').show();
						break;
					case 'MX':

						$tour_module_jq('div.' + data.target + '_apos:not([data-cid=cid-' + data.index + ']) p.m_titledot').hide();

						$tour_module_jq('div.' + data.target + '_apos:not([data-cid=cid-' + data.target + '])').show();
						$tour_module_jq('div.' + data.target + '_apos:is([data-cid=cid-' + data.target + '])', 'p.m_titledot').show();
						break;
					default:
						$tour_module_jq('div.' + data.index + '_anchor').show().parent().find('p.m_titledot, .online-airports').hide();
						$tour_module_jq('.' + data.target + '_apos > p:visible').parent().hide()
						break;
					}
					return false;
				});

				//Airport Click
				targetBaloon.find('.m_majorAirport li, .m_APlist li').on('click', function(e) {
					// 選択オブジェクトのデータを取得
					var data = $tour_module_jq(this).data();
					var countryId;
					var countryCd;
					var cityLetter;
					if (data.apo === '000') {
						// 指定しないを選択した場合の国（CID）を取得
						countryId = $tour_module_jq(this).closest('div').attr('data-cid');
						// 指定しないを選択した場合の都市レターを取得
						cityLetter = '';
					} else {
						// 選択オブジェクトから親ノードをさかのぼり、所属している各国ノード配下の都市を取得（主要な空港データからは取得しないように）
						var targetCity = $tour_module_jq(this).parents('.mod-balloon-set_box_cityList').find('.' + data.area + '_apos').find('[data-val=' + data.apo + ']').eq(-1);
						// 指定しない以外を選択した場合の国（CID）を取得
						countryId = targetCity.closest('div').attr('data-cid');
						// 指定しない以外を選択した場合の都市レターを取得
						cityLetter = data.apo;
					}

					// 国（CID）から国コードを取得
					var isDomTourFlg = options.targetObject.m_tourModuleAirportList_isDomTourList(options.layoutType);
					if(isDomTourFlg != undefined && isDomTourFlg === 'true'){
						// 国内ツアー
						if (countryId.match(/\b(cid-S)/g)) {
							countryCd = '';
						} else {
							countryCd = countryId.substring(4, 7);
						}
					}else{
						// 海外ツアー
						if (countryId.match(/\b(cid-0)/g)) {
							countryCd = '';
						} else {
							countryCd = countryId.substring(4, 6);
						}
					}

					if (options.targetDiv == options.m_targetObject) {
						var eo = $tour_module_jq.Event('onChangeAirport', {
							moduleRootBox:options.targetObject,
							beforeArea:options.m_targetObject.find(options.targetAreaClass).val(),
							beforeCountry:options.m_targetObject.find(options.targetCountryClass).val(),
							beforeAirport:options.m_targetObject.find(options.targetAirportClass).val(),
							afterArea:data.area,
							afterCountry:data.countryCd,
							afterAirport:data.apo});

						var beforeArea = options.m_targetObject.find(options.targetAreaClass).val();
						options.m_targetObject.find(options.targetAreaClass).val(data.area);
						options.m_targetObject.find(options.targetCountryClass).val(countryCd);
						options.m_targetObject.find(options.targetAirportClass).val(cityLetter);

						var inputArea = options.m_targetObject.find(options.targetAreaClass).val();
						var inputCountry = options.m_targetObject.find(options.targetCountryClass).val();
						var inputApo = options.m_targetObject.find(options.targetAirportClass).val();
						var selectedName = options.targetObject.m_tourModuleAirportList_getSelectedName(inputArea, inputCountry, inputApo, options.layoutType);
						if(options.m_targetObject.find("span" + options.targetInputClass).length !== 0){
							options.m_targetObject.find("span" + options.targetInputClass).text(selectedName);
						}else{
							options.m_targetObject.find(options.targetInputClass).val(selectedName);
						}

						options.m_targetObject.find(options.targetAirportClass).trigger(eo);
						if (beforeArea !== data.area) {
							options.m_targetObject.find(options.targetAirportClass).trigger('onChangeParentArea');
						}

						if(options.m_targetObject.find("span" + options.targetInputClass).length !== 0){
							options.m_targetObject.find(options.targetInputClass).parent().focus();
							options.m_targetObject.find(options.targetInputClass).parent().blur();
						}else{
							options.m_targetObject.find(options.targetInputClass).focus();
							options.m_targetObject.find(options.targetInputClass).blur();
						}
					}
					return true;
				});

				//アンカークリック
				targetBaloon.find('.m_airportAncer a').on('click',function(e) {
					var targetAnchor = "";
					if (typeof $tour_module_jq(this).attr('data-index') !== 'undefined') {
						targetAnchor = '#' + $tour_module_jq(this).attr('data-index') + '_anchor';
					} else if(typeof $tour_module_jq(this).attr('data-area') !== 'undefined') {
						targetAnchor = '#' + $tour_module_jq(this).attr('data-area') + '_anchor';
					} else {
						targetAnchor = $tour_module_jq(this).attr('href');
					}
					var target = $tour_module_jq(targetAnchor);
					var position = 0;
					if (!target) {
						var prevIdxAnchor = $tour_module_jq(this).parent().prev();
						if (prevIdxAnchor) {
							position = $tour_module_jq(this).parent().prev().find('a').trigger('click')
						}
					} else if($tour_module_jq(targetAnchor).length == 0) {
						var prevIdxAnchor = $tour_module_jq(this).parent().prev();
						if (prevIdxAnchor) {
							position = $tour_module_jq(this).parent().prev().find('a').trigger('click')
						}
					} else {
						position = $tour_module_jq(targetAnchor).position().top + $tour_module_jq(this).parents('.mod-balloon-set_box').scrollTop();
						$tour_module_jq(this).parents('.mod-balloon-set_box').animate({scrollTop:position}, 400, 'swing');
					}
					return false;
				});
			}
		};
	}
});

// BOXリンク
$tour_module_jq(function() {
	$tour_module_jq.boxLink = function(options) {
		var o = $tour_module_jq.extend({
			targetBox: 'linkBox',
			hover: 'hover'
		}, options);

		if ((/(iPhone)|(iPad)/i.test(navigator.userAgent)) || (/(android)/i.test(navigator.userAgent))) {
			$tour_module_jq('.' + o.targetBox).bind('touchstart', function() {
				$tour_module_jq(this).addClass(o.hover);
			});
			$tour_module_jq('.' + o.targetBox).bind('touchend', function() {
				$tour_module_jq(this).removeClass(o.hover);
			});
		} else {
			$tour_module_jq('.' + o.targetBox).hover(function() {
				$tour_module_jq(this).addClass(o.hover);
			}, function() {
				$tour_module_jq(this).removeClass(o.hover);
			});
		}

		$tour_module_jq('.' + o.targetBox).click(function() {
			var boxUrl = $tour_module_jq(this).find('a').attr('href');
			if ($tour_module_jq(this).find('a').attr('target') == '_blank') {
				window.open(boxUrl);
			} else window.location = boxUrl;
			return false;
		});
		$tour_module_jq(window).unload(function() {
			$tour_module_jq(this).removeClass(o.hover);
		});
	};
	$tour_module_jq(function() {
		$tour_module_jq.boxLink();
	});
});

//変更されたエリア内に都市が9個以上ある場合はスクロールボタンを表示する
function m_tourModuleAirportList_setAreaSelectScroll($listContainer){
	// エリアウィンドウの高さとデータ表示欄の高さを比較
	var containerHeight = $listContainer.height();
	var listHeight = $listContainer.find('.mod-balloon-set_box_inner').height();
	
	if(listHeight - containerHeight > 0){
		// モーダルの画面に収まらない場合はスクロールボタン表示
		$listContainer.addClass('is-scroll');
		$listContainer.find('.mod-balloon-scroll_over').show();
	}else{
		if($listContainer.hasClass('is-scroll')){
			$listContainer.removeClass('is-scroll');
			$listContainer.find('.mod-balloon-scroll_over').hide();
		}
	}
	// スクロールボタン非表示判定を削除
	if($listContainer.hasClass('is-scroll-disabled')){
		$listContainer.removeClass('is-scroll-disabled');
	}
}
