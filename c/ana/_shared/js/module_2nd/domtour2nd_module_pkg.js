/*=====================================================
* Script Name: domtour2nd_module_pkg.js
* Description: 空席照会モジュール 国内ツアー2nd パッケージ
* Version: 1.00
* Last Up Date: 2017/10/11
=====================================================*/

var m_domtour2ndModulePkg_siteType = { site : 'domtour2nd', module : 'pack' };

var m_domtour2ndModuleCar_usePartsCount_ModuleRadio = 1;		//新旧モジュール入れ替えラジオ、配置数カウンタ

//初期値設定
var m_domtour2ndModulePkg_defaults = {
	m_defModuleMode:'1',			//新旧モジュール切り替えラジオ用
	m_defDateDep:'',				//出発日用
	m_defAirportDep:'001',			//出発地用
	m_defAreaArr:'',				//目的地用
	m_defNumberOfDays:'',			//旅行日数用
	m_defNumberOfAdult:'2',			//大人用
	m_defNumberOfChild:m_domtourModuleNumberOfPeoplePkg_optionParams.initNum,   //参加人数（子供幼児）用
	m_defCourseDateDep:'',			//出発日用（コースコード）
	m_defCourseAirportDep:'001',	//出発地用（コースコード）
	m_defCourseCode:'',				//コースコード用（コースコード）
	m_defTabNoDispCls:''					//遷移先表示状態パラメータ
};

// defaults値保持のため、shallow copy
var m_domtour2ndModulePkg_optionParam = $tour_module_jq.extend(m_domtour2ndModulePkg_optionParam, m_domtour2ndModulePkg_defaults);

/**
 * 国内ツアーパッケージ2ndモジュールパラメータ設定処理
 */
function m_domtour2ndModulePkg_setParams(options){
	m_domtour2ndModulePkg_optionParam = $tour_module_jq.extend(m_domtour2ndModulePkg_optionParam, options);
};

/**
 * 国内ツアーパッケージ2ndモジュールHTML埋め込み処理
 */
function m_domtour2ndModulePkg_secondModuleDisplay(options) {
	////////////////
	// 初期化設定
	////////////////

	// 国内ツアーパッケージの各項目不正値チェック処理
	options = $tour_module_jq.extend(true, options, m_domtourModulePkg_incorrectValue(options, m_domtour2ndModulePkg_siteType, m_domtour2ndModulePkg_defaults));
	
	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defModulePosition');
	
	//各項目設定値
	//新旧モジュール切り替えラジオ
	var uniqueNum = m_tour2ndModulePkg_returnCount_ModuleRadio();
	//行き先から探す
	var tourConditionLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_tourConditionLabel, 'Text');
	//出発日
	var depDateText = m_tourModuleCommon_getListValue(m_domtourModulePkg_depDate, 'Text');
	//出発地
	var depApoText = m_tourModuleCommon_getListValue(m_domtourModulePkg_depApo, 'Text');
	//目的地
	var arrApoText = m_tourModuleCommon_getListValue(m_domtourModulePkg_arrApo, 'Text');
	//旅行日数
	var travelOfDaysText = m_tourModuleCommon_getListValue(m_domtourModulePkg_travelOfDays, 'Text');
	//1部屋あたりの利用人数
	var roomOfPeopleLabelText = m_tourModuleCommon_getListValue(m_domtourModulePkg_roomOfPeopleLabel, 'Text');
	//大人
	var adultText = m_tourModuleCommon_getListValue(m_domtourModulePkg_adult, 'Text');
	//子供・幼児
	var childText = m_tourModuleCommon_getListValue(m_domtourModulePkg_child, 'Text');
	var numberOfPeoplePartsId1 = m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.pkg;
	var numberOfPeopleCount1 = m_tourModuleCommon_zeroPadding(m_tourModuleNumberOfPeople_usePartsCount); // 参加人数ID番号設定
	var numberOfPeopleTextId1 = m_tourModuleNumberOfPeople_defaultParams.textId;
	//商品コードから探す
	var courseCodeConditionLabelText = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCodeConditionLabel, 'Text');
	//コースコード
	var courseCodeText1 = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCode, 'Text1');
	var courseCodeText2 = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCode, 'Text2');
	var courseCodePlaceholder = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCode, 'placeholder');
	//商品コードの確認方法
	var courseCodeInfoText = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCodeInfo, 'Text');
	var courseCodeInfoURL = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCodeInfo, 'URL');
	var courseCodeInfoImg = m_tourModuleCommon_getListValue(m_domtour2ndModulePkg_courseCodeInfo, 'Img');
	//検索ボタン
	var actionURL = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModulePkg_fromAction, 'URL');
	var actionURLcourseCode = m_tourModuleCommon_ASW_DOMAIN + m_tourModuleCommon_getListValue(m_domtourModulePkg_fromAction_Course, 'URL');
	var searchButtonText = m_tourModuleCommon_getListValue(m_domtourModuleCommon_searchButton, 'Text');
	
	// formMethod設定
	var formMethod = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defFormMethod');
	
	//新旧モジュール切り替え
	var moduleTabText1 = m_tourModuleCommon_getListValue(m_domtourModulePkg_moduleTabText, 'Text1');
	var moduleTabText2 = m_tourModuleCommon_getListValue(m_domtourModulePkg_moduleTabText, 'Text2');
	var moduleTabText3 = m_tourModuleCommon_getListValue(m_domtourModulePkg_moduleTabText, 'Text3');
	var moduleTabText4 = m_tourModuleCommon_getListValue(m_domtourModulePkg_moduleTabText, 'Text4');

	/////////////////////
	// 画面レイアウト
	/////////////////////
	var html = '';

	html += '<div class="mod-content_wrap">\n';
	html += '<div class="js-mod-tab">\n';
	
	//上部切り替えタブ 10月31日以前(旧モジュール) or 11月1日以降(新モジュール)
	html += '<ul class="mod-radio-group_wrap tabstyle">\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_pkgmod" type="radio" id="dom_pkg_module1_' + uniqueNum + '" value="1" name="m_dom_pkg_module_' + uniqueNum + '">\n';
	html += '<label tabindex="0" for="dom_pkg_module1_' + uniqueNum + '">'+moduleTabText1+'<em class="large">'+moduleTabText2+'</em>'+moduleTabText4+'</label>\n';
	html += '</li>\n';
	html += '<li class="ico-radio">\n';
	html += '<input class="item-radio js-mod-tab_btn m_js-mod-tab_btn_pkgmod" type="radio" id="dom_pkg_module2_' + uniqueNum + '" value="0" name="m_dom_pkg_module_' + uniqueNum + '">\n';
	html += '<label tabindex="0" for="dom_pkg_module2_' + uniqueNum + '">'+moduleTabText1+'<em class="large">'+moduleTabText3+'</em>'+moduleTabText4+'</label>\n';
	html += '</li>\n';
	html += '</ul>\n';
	
	//10月31日以前(旧モジュール)
	html += '<div class="m_dom_pkg_module01 js-mod-tab_contents mod-tab_contents"> \n';
	html += '</div><!-- / .js-mod-tab_contents mod-tab_contents -->\n';
	
	
	//11月1日以降(新モジュール)
	html += '<div class="m_dom_pkg_module02 js-mod-tab_contents mod-tab_contents">\n';
	html += '<div class="mod-content_cover">\n';
	
	//行き先から探す
	html += '<div class="mod-content_box dom-pkg_search">\n';
	html += '<form class="form-domPkg" id="form-domPkg" method="'+formMethod+'" action="' + actionURL + '" accept-charset="UTF-8">\n';
	html += '<p class="mod-title line">' + tourConditionLabelText + '</p>\n';
	html += '<div class="mod-content_inner_cover col2">\n';
	
	
	//1カラム目 出発日 出発地 目的地 旅行日数
	html += '<div class="mod-content_inner_box">\n';
	
	//出発日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depDateText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depDate_tourCondition">\n';
	//出発日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//出発地
	html += '<fieldset class="mod-input_box m_dom_pkg_dep_box">\n';
	html += '<legend><span class="mod-input_head">' + depApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depApoArea_tourCondition">\n';
	//出発地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//目的地
	html += '<fieldset class="mod-input_box m_dom_pkg_arr_box">\n';
	html += '<legend><span class="mod-input_head">' + arrApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_arrArea">\n';
	//目的地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//旅行日数
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + travelOfDaysText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_travelOfDays">\n';
	//旅行日数部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	html += '</div><!-- / .mod-content_inner_box -->\n';
	//1カラム目 END
	
	
	//2カラム目 1部屋あたりの利用人数（大人、子供・幼児）
	html += '<div class="mod-content_inner_box mod-btn_box">\n';
	html += '<p class="mod-title small">' + roomOfPeopleLabelText + '</p>\n';
	
	//大人
	html += '<fieldset class="mod-input_box column">\n';
	html += '<legend><span class="mod-input_head">' + adultText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_adult">\n';
	//大人部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//子供・幼児
	html += '<fieldset class="mod-input_box column">\n';
	html += '<legend><span class="mod-input_head">' + childText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_child">\n';
	html += '<input type="text" readonly name="" id="' + numberOfPeopleTextId1 + numberOfPeopleCount1 + '" value="" class="ico_select mod-input mod-icon js-mod-balloon m_numberOfPeople" data-balloon="'+ numberOfPeoplePartsId1 + numberOfPeopleCount1 +'" data-control-point="'+ dataControlPointValue +'" tabindex="0">\n';
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//検索ボタン（行き先から探す）
	html += '<div class="btn-search-area btm">\n';
	html += '<div class="btn btn-search m_btn-search-dest" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</div><!-- / .mod-content_box -->\n';
	//2カラム目 END
	
	
	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</form>\n';
	html += '</div><!-- / .mod-content_box -->\n';
	//行き先から探す END
	
	
	//商品コードから探す（3カラム目）
	html += '<div class="mod-content_box dom-pkg_code mod-btn_box">\n';
	html += '<form class="form-domPkgCourse" id="form-domPkgCourse" method="'+formMethod+'" action="' + actionURLcourseCode + '" accept-charset="UTF-8">\n';
	html += '<p class="mod-title line">' + courseCodeConditionLabelText + '</p>\n';
	
	//出発日
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depDateText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depDate_courseCodeCondition">\n';
	//出発日部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//出発地
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + depApoText + '</span></legend>\n';
	html += '<div class="mod-input_parts m_depApoArea_courseCodeCondition">\n';
	//出発地部品埋め込み
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//コースコード
	html += '<fieldset class="mod-input_box">\n';
	html += '<legend><span class="mod-input_head">' + courseCodeText1 + '<span class="ib">' + courseCodeText2 + '</span></span></legend>\n';
	html += '<div class="mod-input_parts">\n';
	html += '<input type="text" name="cmdtCd" id="dom_pkg_code" value="" class="mod-pgtour-code mod-input m_courseCode" placeholder="' + courseCodePlaceholder + '">\n';
	html += '</div>\n';
	html += '</fieldset>\n';
	
	//商品コードの確認方法　0501削除
	
	//検索ボタン
	html += '<div class="btn-search-area btm">\n';
	html += '<div class="btn btn-search m_btn-search-course" tabindex="0">' + searchButtonText + '</div>\n';
	html += '</div>\n';
	
	html += '</form>\n';
	html += '</div><!-- / .mod-content_box -->\n';
	//商品コードから探す END
	
	html += '</div><!-- / .mod-content_cover -->\n';
	html += '</div><!-- / .js-mod-tab_contents mod-tab_contents -->\n';
	//11月1日以降(新モジュール) END
	
	html += '</div><!-- / .js-mod-tab -->\n';
	html += '</div>\n';

	html += '\n';

	this.append(html);
	
	//初期選択制御
	var isRadioCheck = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defModuleMode');
	if(isRadioCheck === '1'){
		this.find('#dom_pkg_module1_' + uniqueNum  + '').attr('checked', 'checked');
		this.find('.m_dom_pkg_module01').addClass('is-active');
	}else if(isRadioCheck === '0'){
		this.find('#dom_pkg_module2_' + uniqueNum  + '').attr('checked', 'checked');
		this.find('.m_dom_pkg_module02').addClass('is-active');
	}

	m_domtour2ndModulePkg_setDefVal(this, options);
	
};

function m_domtour2ndModulePkg_setDefVal(target, options){

	////////////////
	// 初期値取得
	////////////////

	// モジュール設置位置による噴出し表示位置設定
	var dataControlPointValue = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defModulePosition');
	
	
	////////////////////
	// 各入力項目初期化
	////////////////////
	
	//パラメータ設定を取得
	//出発日 行き先から探す
	var defDateDep = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defDateDep');
	//出発地 行き先から探す
	var defAirportDep = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defAirportDep');
	//目的地 行き先から探す
	var defAreaArr = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defAreaArr');
	//旅行日数
	var defNumberOfDays = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defNumberOfDays');
	//大人
	var defNumberOfAdult = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defNumberOfAdult');
	// 子供(参加人数部品)
	var defNumberOfPeopleSelectInit = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defNumberOfChild');
	var numberOfPeopleCount1 = m_tourModuleCommon_zeroPadding(m_tourModuleNumberOfPeople_usePartsCount);
	//出発日 商品コードから探す
	var defCourseDateDep = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defCourseDateDep');
	//出発地 商品コードから探す
	var defCourseAirportDep = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defCourseAirportDep');
	//コースコード
	 var defCourseCode = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defCourseCode');
	// 再検索パラメータ
	var kartKeepValue = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defKartKeep');
	//遷移先表示状態パラメータ
	var tabNoDispCls = m_tourModuleCommon_getParam(m_domtour2ndModulePkg_siteType, options, 'm_defTabNoDispCls');
	
	//出発日（行き先から探す）
	target.find('.m_depDate_tourCondition').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModulePkg_depDate, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defDateDep,
		defSelectDay : m_domtourModulePkg_depDate_defSelectDay,
		dateValueClass : m_domtourModulePkg_depDate_dateValueClass,
		dateValueName : m_domtourModulePkg_depDate_dateValueName,
		inputPlaceholder : m_domtourModulePkg_depDate_inputPlaceholder,
		inputClass : m_domtourModulePkg_depDate_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 0),
		isRequired : m_domtourModulePkg_depDate_isRequired,
		isDisabled : m_domtourModulePkg_depDate_isDisabled,
		isDispAllOfMonth : m_domtourModulePkg_depDate_isDispAllOfMonth,
		prevNextInterval : m_domtourModulePkg_depDate_prevNextInterval,
		receiptDisableDays : m_domtourModulePkg_depDate_receiptDisableDays,
		dispMonths : m_domtourModulePkg_depDate_dispMonths,
		selectableMonths : m_domtourModulePkg_depDate_selectableMonths,
		isDispHoliday : m_domtourModulePkg_depDate_isDispHoliday,
		boundaryTime : m_domtourModulePkg_depDate_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//出発地（行き先から探す）
	target.find('.m_depApoArea_tourCondition').tourCommonModule('createAirportInput', {
		parent:this,
		defAirportCode:defAirportDep,
		defPartsPosition:dataControlPointValue,
		balloonTitle:m_domtour2ndModulePkg_depApo['Text'],
		areaClass:'m_hiddenPkgDepArea',
		countryClass:'m_hiddenPkgDepCountry',
		apoCodeClass:'m_hiddenPkgDepApo',
		apoCodeName:'deptAreaCd',
		inputPlaceholder:m_domtour2ndModulePkg_depApo['Placeholder'],
		inputClass:'m_domPkgDepText',
		isRequired:false,
		isDisabled:false,
		layoutType:'16',
		uniqueKey:m_tourCommonModuleParts_getPartsCount('airport', 0),
		apoType:'0'
	});
	
	//目的地
	target.find('.m_arrArea').tourCommonModule('createAirportInput', {
		parent : this,
		defAirportCode : defAreaArr,
		defPartsPosition : dataControlPointValue,
		balloonTitle : m_domtour2ndModulePkg_arrApo['Text'],
		areaClass : 'm_packArrArea',
		countryClass : 'm_packArrCountry',
		apoCodeClass : 'm_packArrApo',
		apoCodeName : 'districtCd',
		inputPlaceholder :  m_domtour2ndModulePkg_arrApo['Placeholder'],
		inputClass : 'm_domPkgArrText',
		isRequired : false,
		isDisabled : false,
		layoutType : '17',
		uniqueKey : m_tourCommonModuleParts_getPartsCount('airport', 1),
		apoType : '1'
	});
	
	//旅行日数
	target.find('.m_travelOfDays').tourCommonModule('createTravelOfDays', {
		site : m_domtour2ndModulePkg_siteType.site,
		module : m_domtour2ndModulePkg_siteType.module,
		defSelect: defNumberOfDays,
		inputClass:'m_dom_pkg_days',
		inputId:'dom_pkg_days',
		inputName:'travelDaysCnt'
	});
	
	//大人
	target.find('.m_adult').tourCommonModule('createPeoplesPull', {
		site : m_domtour2ndModulePkg_siteType.site,
		module : m_domtour2ndModulePkg_siteType.module,
		defSelect: defNumberOfAdult,
		inputClass:'',
		inputId:'dom_pkg_member',
		inputName:'adultCnt'
	});
	
	//子供・幼児
	target.find('.m_child').tourCommonModule('createNumberOfPeople', {
		id: m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.pkg,
		className: m_tourModuleNumberOfPeople_FixedParams.domtour.sec.id.pkg,
		htmlType : m_tourModuleNumberOfPeople_FixedParams.domtour.sec.htmlType,
		moduleNo : numberOfPeopleCount1,
		NPLP : m_domtourModuleNumberOfPeoplePkg_NPLP,
		initNum : defNumberOfPeopleSelectInit,
		clearNum : defNumberOfPeopleSelectInit,
		column : m_domtourModuleNumberOfPeoplePkg_optionParams.column,
		row : m_domtourModuleNumberOfPeoplePkg_optionParams.row,
		dispMatrix : m_domtourModuleNumberOfPeoplePkg_optionParams.dispMatrix,
		textBoxTemplate : m_domtourModuleNumberOfPeoplePkg_optionParams.textBoxTemplate
	});
	
	//出発日（商品コードから探す）
	target.find('.m_depDate_courseCodeCondition').tourCommonModule('createDateInput', {
		parent : target,
		balloonTitle : m_tourModuleCommon_getListValue(m_domtourModulePkg_depDate, 'BalloonTitle'),
		defPartsPosition : dataControlPointValue,
		defDate : defCourseDateDep,
		defSelectDay : m_domtourModulePkg_depDateCourseCode_defSelectDay,
		dateValueClass : m_domtourModulePkg_depDateCourseCode_dateValueClass,
		dateValueName : m_domtourModulePkg_depDateCourseCode_dateValueName,
		inputPlaceholder : m_domtourModulePkg_depDateCourseCode_inputPlaceholder,
		inputClass : m_domtourModulePkg_depDateCourseCode_inputClass,
		uniqueKey : m_tourCommonModuleParts_getPartsCount('calendar', 1),
		isRequired : m_domtourModulePkg_depDateCourseCode_isRequired,
		isDisabled : m_domtourModulePkg_depDateCourseCode_isDisabled,
		isDispAllOfMonth : m_domtourModulePkg_depDateCourseCode_isDispAllOfMonth,
		prevNextInterval : m_domtourModulePkg_depDateCourseCode_prevNextInterval,
		receiptDisableDays : m_domtourModulePkg_depDateCourseCode_receiptDisableDays,
		dispMonths : m_domtourModulePkg_depDateCourseCode_dispMonths,
		selectableMonths : m_domtourModulePkg_depDateCourseCode_selectableMonths,
		isDispHoliday : m_domtourModulePkg_depDateCourseCode_isDispHoliday,
		boundaryTime : m_domtourModulePkg_depDateCourseCode_boundaryTime,
		spWidth : m_tour2ndModuleCommon_SP_WIDTH
	});
	
	//出発地（商品コードから探す）
	target.find('.m_depApoArea_courseCodeCondition').tourCommonModule('createAirportInput', {
		parent:this,
		defAirportCode:defCourseAirportDep,
		defPartsPosition:dataControlPointValue,
		balloonTitle:m_domtour2ndModulePkg_depApo['Text'],
		areaClass:'m_hiddenPkgCourseDepArea',
		countryClass:'m_hiddenPkgCourseDepCountry',
		apoCodeClass:'m_hiddenPkgCourseDepApo',
		apoCodeName:'deptAreaCd',
		inputPlaceholder:m_domtour2ndModulePkg_depApo['Placeholder'],
		inputClass:'m_domPkgCourseDepText',
		isRequired:false,
		isDisabled:false,
		layoutType:'18',
		uniqueKey:m_tourCommonModuleParts_getPartsCount('airport', 2),
		apoType:'0'
	});
	
	// パッケージツアー「コースコードから探す」コースコードの初期値設定
	if (!_.isEmpty(defCourseCode)) {
		target.find('.m_courseCode').attr('value', defCourseCode);
	}
	
	//新旧モジュール切り替えイベント
	$tour_module_jq(document).on('click keydown', '.m_js-mod-tab_btn_pkgmod', function (e) {
		var $modBox = $tour_module_jq(this).closest('.module-cont');
		var $modTabBox = $tour_module_jq(this).closest('.js-mod-tab');
		var $modTabBtn = $modTabBox.find('.js-mod-tab_btn');
		var $modTabContents = $modTabBox.children('.js-mod-tab_contents');
		var btnIndex = $modTabBtn.index(this);
		$modTabContents.removeClass('is-active');
		$modTabContents.eq(btnIndex).addClass('is-active');
	});
	
	// 検索ボタン（行き先から探す）押下イベント登録：サブミット実行
	target.find('.m_btn-search-dest').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			$tour_module_jq(this).domtourCommonModule('submitSearchPkgDest', {
				targetModule : target
			});
		}
	});
	
	// 検索ボタン（商品コードから探す）押下イベント登録：サブミット実行
	target.find('.m_btn-search-course').on('click keydown', function(e){
		var whichValue = e.which;
		if(whichValue == 1 || whichValue == 13){
			$tour_module_jq(this).domtourCommonModule('submitSearchPkgCourse', {
			});
		}
	});
	
	// 再検索パラメータのhidden生成
	if(!_.isEmpty(kartKeepValue)){
		target.tourCommonModule('createInputHidden', {
			site : 'domtour',
			inputName:'KartKeep',
			inputValue:kartKeepValue
		});
	}
	
	// 遷移先表示状態パラメータのhidden生成
	target.tourCommonModule('createInputHidden', {
		site : 'domtour',
		inputName:'tabNoDispCls',
		inputValue:tabNoDispCls
	});
	
	// 部品配置数カウントアップ
	m_tourCommonModuleParts_setPartsCount('airport', 3);
	m_tourCommonModuleParts_setPartsCount('calendar', 2);
};

//モジュール切り替えラジオ配置数カウント
function m_tour2ndModulePkg_returnCount_ModuleRadio(){
	
	var returnCount = m_domtour2ndModuleCar_usePartsCount_ModuleRadio;
	m_domtour2ndModuleCar_usePartsCount_ModuleRadio++;
	return m_tourModuleCommon_zeroPadding(returnCount);
}

$tour_module_jq.fn.domtour2ndModulePkg = function(method) {
	var methods = {
		'privateSetParams' : m_domtour2ndModulePkg_setParams,
		'privateSecondModuleDisplay' : m_domtour2ndModulePkg_secondModuleDisplay
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.domtour2ndModulePkg');
	}
};
