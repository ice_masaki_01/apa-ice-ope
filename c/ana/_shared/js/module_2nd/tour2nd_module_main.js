/*=====================================================
* Script Name: tour2nd_module_main.js
* Description: 空席照会モジュール ツアー2ndメイン処理
* Version: 1.00
* Last Up Date: 2017/09/29
=====================================================*/

// ここからコントローラ処理
var m_tour2ndModuleMain_siteType = { site : 'tour2nd', module : 'Main' };

var m_tour2ndModuleMain_AREA_CODE_JP = '01';

//初期値設定
var m_tour2ndModuleMain_defaults_common = {
	m_defModuleType:'H',					// デザイン 'V':縦版,'H':横版
	m_defModulePosition:'C',				// モジュール設定位置 'L':左,'C':中央,'R':右
	m_defIsDispTabDomInt:true,				// 国内/国際タブ表示スイッチ
	m_defIsDispTabFuncDom:true,				// 国内旅行タブ表示スイッチ
	m_defIsDispTabFuncInt:true,				// 国際旅行タブ表示スイッチ
	m_defIsDispDomDp:true,					// 国内旅作タブ表示スイッチ
	m_defIsDispDomPkg:true,					// 国内パッケージタブ表示スイッチ
	m_defIsDispDomHotel:true,				// 国内ホテルタブ表示スイッチ
	m_defIsDispDomHotelBulk:true,			// 国内ホテル（３社一括）タブの表示スイッチ
	m_defIsDispDomCar:true,					// 国内レンタカータブ表示スイッチ
	m_defIsDispDomTraffic:true,				// 国内観光タブ表示スイッチ
	m_defIsDispDomBulk:true,				// 国内ツアー一括タブ表示スイッチ
	m_defIsDispIntDp:true,					// 海外旅作タブ表示スイッチ
	m_defIsDispIntPkg:true,					// 海外パッケージタブ表示スイッチ
	m_defIsDispIntOption:true,				// 海外オプションタブ表示スイッチ
	m_defCoopSiteCode:'',					// 提携サイトコード
	m_defFormMethod:'get',					// formMethod切り替え get/post
	m_defKartKeep:''						// 再検索パラメータ
};

// defaults値保持のため、shallow copy
var m_tour2ndModuleMain_optionParam_common = $tour_module_jq.extend(m_tour2ndModuleMain_optionParam_common, m_tour2ndModuleMain_defaults_common);

function m_tour2ndModuleMain_setParams(options){
	m_tour2ndModuleMain_optionParam_common = $tour_module_jq.extend(m_tour2ndModuleMain_optionParam_common, options.common);

	var defIsDispTabDomInt = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispTabDomInt');
	// フラグ調整
	// 国内/国際タブ表示スイッチがtrueの場合は国内旅行タブと国際旅行タブを両方trueにする
	if(defIsDispTabDomInt){
		m_tour2ndModuleMain_optionParam_common.m_defIsDispTabFuncDom = true;
		m_tour2ndModuleMain_optionParam_common.m_defIsDispTabFuncInt = true;
	}
	//国内/国際タブ
	var defIsDispTabFuncDom = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispTabFuncDom');
	var defIsDispTabFuncInt = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispTabFuncInt');
	//国際
	var defIsDispIntTabisaku = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispIntDp');
	var defIsDispIntPackTour = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispIntPkg');
	var defIsDispIntOption = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispIntOption');
	//国内
	var defIsDispDomDp = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomDp');
	var defIsDispDomPkg = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomPkg');
	var defIsDispDomHotel = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomHotel');
	var defIsDispDomHotelBulk = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomHotelBulk');
	var defIsDispDomCar = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomCar');
	var defIsDispDomTraffic = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomTraffic');
	var defIsDispDomBulk = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomBulk');
	
	// 吹き出し位置パラメータ設定
	var defModulePosition = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defModulePosition');
	if(defModulePosition === 'R'){
		m_tour2ndModuleMain_optionParam_common['m_defModulePosition'] = 'right';
	}else if(defModulePosition === 'L'){
		m_tour2ndModuleMain_optionParam_common['m_defModulePosition'] = 'left';
	}else{
		m_tour2ndModuleMain_optionParam_common['m_defModulePosition'] = 'center';
	}
	
	//////////////////////////
	// 他モジュールの初期化
	//////////////////////////

	// 国内/国際タブ
	if(defIsDispTabDomInt){
		var optionsTabSelDomInt = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.tabDomInt);
		this.tour2ndModuleTab('privateSetParams', optionsTabSelDomInt);
	}
	//国内旅作
	if(defIsDispDomDp){
		var optionsDomTabiskau = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourDp);
		this.domtour2ndModuleDp('privateSetParams', optionsDomTabiskau);
	}
	//国内パッケージ
	if(defIsDispDomPkg){
		var optionsDomPack = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourPkg);
		this.domtour2ndModulePkg('privateSetParams', optionsDomPack);
	}
	
	//国内ホテル
	if(defIsDispDomHotel){
		var optionsDomHotel = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourHotel);
		this.domtour2ndModuleHotel('privateSetParams', optionsDomHotel);
	}
	
	//国内ホテル(３社一括)
	if(defIsDispDomHotelBulk){
		var optionsDomHotelBulk = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourHotelBulk);
		this.domtour2ndModuleHotelBulk('privateSetParams', optionsDomHotelBulk);
	}
	
	//国内レンタカー
	if(defIsDispDomCar){
		var optionsDomCar = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourCar);
		this.domtour2ndModuleCar('privateSetParams', optionsDomCar);
	}
	
	//国内観光
	if(defIsDispDomTraffic){
		var optionsDomTraffic = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourTraffic);
		this.domtour2ndModuleTraffic('privateSetParams', optionsDomTraffic);
	}
	
	//国内ツアー
	if(defIsDispDomBulk){
		var optionsDomBulk = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourBulk);
		this.domtour2ndModuleBulk('privateSetParams', optionsDomBulk);
	}
	// 海外旅作
	if(defIsDispIntTabisaku){
		var optionsIntTabiskau = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.inttourDp);
		this.inttour2ndModuleDp('privateSetParams', optionsIntTabiskau);
	}
	// 海外パッケージ
	if(defIsDispIntPackTour){
		var optionsIntPack = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.inttourPkg);
		this.inttour2ndModulePkg('privateSetParams', optionsIntPack);
	}
	// 海外オプション
	if(defIsDispIntOption){
		var optionsIntOption = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.inttourOption);
		this.inttour2ndModuleOption('privateSetParams', optionsIntOption);
	}
	// 国内旅行タブ
	if(defIsDispTabDomInt || defIsDispTabFuncDom){
		var optionsTabSelFuncDom = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.tabFuncDom);
		this.domtour2ndModuleTab('privateSetParams', optionsTabSelFuncDom);
	}
	// 国際旅行タブ
	if(defIsDispTabDomInt || defIsDispTabFuncInt){
		var optionsTabSelFuncInt = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.tabFuncInt);
		this.inttour2ndModuleTab('privateSetParams', optionsTabSelFuncInt);
	}
};

// 指定文字列の配列内存在チェック
function m_tour2ndModuleMain_getStrArrayMatchAll(targetStr, arrayStr){
	if($tour_module_jq.inArray(targetStr, arrayStr) != -1){
		return true;
	}
	return false;
};

/**
 * 国際線2ndモジュールHTML埋め込み処理
 */
function m_tour2ndModuleMain_secondModuleDisplay(options) {

	// 縦or横版制御
	var defModuleType = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defModuleType');
	if(defModuleType === 'V'){
		defModuleType = 'vertical';
	}else{
		defModuleType = 'horizon';
	}
	
	// テキスト設定
	// 特になし

	// 画面レイアウト
	var html = '';
	html += '<div class="res-contents module-travel_cover">\n';
	html += '<div class="module-travel_inner module-travel_parts '+ defModuleType +'">\n';
	html += '<div class="mod-travel_select js-mod-travel-tab">\n';
	// 国内/国際タブが追加される箇所
	html += '<div class="mod-travel_select_cover">\n';
	html += '<!-- 国内旅行 /////////// -->\n';
	html += '<div class="mod_domtour js-mod-travel-tab_contents mod-tab_contents is-active">\n';
	
	
	html += '<div class="js-mod-list-tab">\n';
	// 国内旅行タブが追加される箇所
	html += '<!-- 航空券+宿泊「旅作」 /////////// -->\n';
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-tabisaku is-active">\n';
	// 国内旅作モジュールが追加される箇所
	html += '</div><!-- /////////// 航空券+宿泊「旅作」 -->\n';
	
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-pgtour is-active">\n';
	// 国内パッケージツアーが追加される箇所
	html += '</div><!-- / .module-dom-pgtour -->\n';
	
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-hotel is-active">\n';
	// 国内ホテルが追加される箇所
	html += '</div><!-- / .module-dom-hotel -->\n';
	
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-hotel3 mod-hotel_sales is-active">\n';
	// 国内ホテル(３社一括)が追加される箇所
	html += '</div><!-- / .module-dom-hotel3 -->\n';
	
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-car is-active">\n';
	// 国内レンタカーが追加される箇所
	html += '</div><!-- / .module-dom-car -->\n';
	
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-traffic is-active">\n';
	// 国内観光・体験・現地交通が追加される箇所
	html += '</div><!-- / .module-dom-traffic -->\n';
	
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-dom-bulk is-active">\n';
	// 国内ツアーが追加される箇所
	html += '</div><!-- / .module-dom-bulk -->\n';
	
	html += '</div><!-- / .js-mod-list-tab -->\n';
	
	
	html += '</div><!-- /////////// 国内旅行 -->\n';
	html += '\n';
	html += '<!-- 海外旅行 /////////// -->\n';
	html += '<div class="mod_inttour js-mod-travel-tab_contents mod-tab_contents is-active">\n';
	html += '<div class="js-mod-list-tab">\n';
	// 国際旅行タブが追加される箇所
	html += '<!-- 航空券+宿泊「旅作」 /////////// -->\n';
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-int-tabisaku is-active">\n';
	// 海外旅作モジュールが追加される箇所
	html += '</div><!-- /////////// 航空券+宿泊「旅作」 -->\n';
	html += '<!-- パッケージツアー /////////// -->\n';
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-int-pgtour is-active">\n';
	// 海外パッケージモジュールが追加される箇所
	html += '</div><!-- /////////// #js-mod_domtour パッケージツアー -->\n';
	html += '<!-- オプション /////////// -->\n';
	html += '<div class="js-mod-list-tab_contents mod-tab_contents module-int-option is-active">\n';
	// 海外オプションモジュールが追加される箇所
	html += '</div><!-- /////////// #js-mod_domtour オプション -->\n';
	html += '</div><!-- / .js-mod_tab -->\n';
	html += '</div><!-- /////////// #js-mod_inttour 海外旅行 -->\n';
	html += '\n';
	html += '\n';
	html += '<button class="mod-shutter_btn js-mod-shutter_btn"><span>開く</span></button>';
	html += '</div><!-- / .mod-travel_select_cover -->\n';
	html += '</div><!-- / .mod-travel_select js-mod_tab -->\n';
	html += '</div>\n';
	html += '<!-- //#module-travel_parts -->\n';
	html += '\n';
	html += '</div><!-- module-travel_cover -->\n';

	this.append(html);

	var calHtml = '';
	this.after(calHtml);
	m_inttour2ndModuleMain_setDefVal(this, options);
};

function m_inttour2ndModuleMain_setDefVal(target, options){
	
	//国内/国際タブ
	var defIsDispTabDomInt = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispTabDomInt');
	var defIsDispTabFuncDom = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispTabFuncDom');
	var defIsDispTabFuncInt = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispTabFuncInt');
	//国際
	var defIsDispIntTabisaku = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispIntDp');
	var defIsDispIntPackTour = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispIntPkg');
	var defIsDispIntOption = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispIntOption');
	//国内
	var defIsDispDomDp = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomDp');
	var defIsDispDomPkg = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomPkg');
	var defIsDispDomHotel = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomHotel');
	var defIsDispDomHotelBulk = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomHotelBulk');
	var defIsDispDomCar = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomCar');
	var defIsDispDomTraffic = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomTraffic');
	var defIsDispDomBulk = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, options, 'm_defIsDispDomBulk');
	
	//国内ツアータブ・海外ツアータブ初期表示制御
	var activeTab = '';
	if(defIsDispTabFuncDom && !defIsDispTabFuncInt){
		target.find('.mod_domtour').addClass('is-active');
	}else if(!defIsDispTabFuncDom && defIsDispTabFuncInt){
		target.find('.mod_inttour').addClass('is-active');
	}
	
	// AdobeTarget設定、optionsの初期値を更新
	if(m_tourModuleCommon_setTargetValue_params !== undefined){
		options = $tour_module_jq.extend(true,options,m_tourModuleCommon_setTargetValue_params);
	}

	// 再検索パラメータ設定
	var optionsParam = undefined;
	if(defIsDispIntTabisaku || defIsDispIntPackTour){
		// 海外ツアー
		optionsParam = target.tourCommonModule('setResearchValue', m_inttour2ndModuleCommon_setDefValue);
	}
	if(defIsDispDomDp || defIsDispDomPkg || defIsDispDomHotel || defIsDispDomHotelBulk || defIsDispDomCar || defIsDispDomTraffic || defIsDispDomBulk){
		// 国内ツアー
		var optionsParamDom = target.tourCommonModule('setResearchValue', m_domtour2ndModuleCommon_setDefValue);
		if (!_.isEmpty(optionsParamDom) && optionsParam === undefined) {
			optionsParam = {};
		}
		$tour_module_jq.extend(optionsParam, optionsParamDom);
	}
	
	if(optionsParam!==undefined){
		
		var moduleFlg = true;	//タブ判定 true:国内 false:海外
		
		// 海外モジュール判定
		// 海外旅行タブの各モジュール一意の項目を設定(仮)
		// [旅作,パッケージツアー,パッケージツアー(コースコード)]の順
		// [旅作,パッケージ,ホテル,レンタカー,観光,ツアー一括]
		var module_judge = ['outwardDeptName','freeword','courseNo'];
		for(i=0;i<module_judge.length;i++){
			var judgeKey = module_judge[i];
			
			if(m_tourModuleCommon_setResearchValue_params[judgeKey]!==undefined){
				var tabFuncInt = {tabFuncInt : {'m_defInitTab' : '1'}}
				
				var tabDomInt = {tabDomInt : {'m_defInitTab' : '2'}}
				optionsParam = $tour_module_jq.extend(true,optionsParam,tabDomInt);
				moduleFlg = false;
				
				if(i==0){
					//海外タブ初期選択設定
					tabFuncInt = {tabFuncInt : {'m_defInitTab' : '1'}}
					// 旅作の場合は都市滞在の状態確認と移動日
					if(m_tourModuleCommon_setResearchValue_params['secondDestCode']!==undefined){
						var inttourDp = {inttourDp : {'m_defStayCityCount' : '1'}}
						optionsParam = $tour_module_jq.extend(true,optionsParam,inttourDp);
						
						// 移動日
						if(m_tourModuleCommon_setResearchValue_params['transferDeptDate'] !== undefined && m_tourModuleCommon_setResearchValue_params['transferDeptDate'] != ""){
							var transferDeptDate = m_tourModuleCommon_setResearchValue_params['transferDeptDate'].substr(0,4) + "/";
							transferDeptDate += m_tourModuleCommon_setResearchValue_params['transferDeptDate'].substr(4,2) + "/";
							transferDeptDate += m_tourModuleCommon_setResearchValue_params['transferDeptDate'].substr(6);
							optionsParam['inttourDp']['m_defDateMove'] = transferDeptDate;
						}
					}
					// 行き、帰り
					if(m_tourModuleCommon_setResearchValue_params['outwardDeptDate'] !== undefined && m_tourModuleCommon_setResearchValue_params['outwardDeptDate'] != ""){
						var outwardDeptDate = m_tourModuleCommon_setResearchValue_params['outwardDeptDate'].substr(0,4) + "/";
						outwardDeptDate += m_tourModuleCommon_setResearchValue_params['outwardDeptDate'].substr(4,2) + "/";
						outwardDeptDate += m_tourModuleCommon_setResearchValue_params['outwardDeptDate'].substr(6);
						optionsParam['inttourDp']['m_defDateDep'] = outwardDeptDate;
					}
					if(m_tourModuleCommon_setResearchValue_params['returnDeptDate'] !== undefined && m_tourModuleCommon_setResearchValue_params['returnDeptDate'] != ""){
						var returnDeptDate = m_tourModuleCommon_setResearchValue_params['returnDeptDate'].substr(0,4) + "/";
						returnDeptDate += m_tourModuleCommon_setResearchValue_params['returnDeptDate'].substr(4,2) + "/";
						returnDeptDate += m_tourModuleCommon_setResearchValue_params['returnDeptDate'].substr(6);
						optionsParam['inttourDp']['m_defDateRet'] = returnDeptDate;
					}
					
					// 別タブのパラメータを削除
					optionsParam['inttourPkg'] = undefined;
					optionsParam['domtourDp'] = undefined;
					optionsParam['domtourPkg'] = undefined;
					optionsParam['domtourHotel'] = undefined;
					optionsParam['domtourHotelBulk'] = undefined;
					optionsParam['domtourCar'] = undefined;
					optionsParam['domtourTraffic'] = undefined;
					optionsParam['domtourBulk'] = undefined;
				}else if(i==1){
					//海外タブ初期選択設定
					tabFuncInt = {tabFuncInt : {'m_defInitTab' : '2'}}
					// パッケージツアーの場合は出発日の状態確認
					if(m_tourModuleCommon_setResearchValue_params['deptEndYearMonth']!==undefined && m_tourModuleCommon_setResearchValue_params['deptEndYearMonth'] != ""){
						// 出発日、期間
						var DateDep = "";
						if(m_tourModuleCommon_setResearchValue_params['deptYearMonth']!==undefined && m_tourModuleCommon_setResearchValue_params['deptYearMonth'] != ""){
							DateDep = m_tourModuleCommon_setResearchValue_params['deptYearMonth'].substr(0,4) + "/";
							DateDep += m_tourModuleCommon_setResearchValue_params['deptYearMonth'].substr(4,2) + "/";
							DateDep += m_tourModuleCommon_setResearchValue_params['deptDay'];
						}
						var DateDepEnd = m_tourModuleCommon_setResearchValue_params['deptEndYearMonth'].substr(0,4) + "/";
						DateDepEnd += m_tourModuleCommon_setResearchValue_params['deptEndYearMonth'].substr(4,2) + "/";
						DateDepEnd += m_tourModuleCommon_setResearchValue_params['deptEndDay'];

						// 出発期間ラジオ選択、出発日期間を設定
						var inttourPkg = {inttourPkg : {'m_defDepartureDateType' : '1', 'm_defDateDep' : DateDep, 'm_defDateDepEnd' : DateDepEnd}}
						
					}else{
						if(m_tourModuleCommon_setResearchValue_params['deptYearMonth']!==undefined && m_tourModuleCommon_setResearchValue_params['deptYearMonth'] != ""){
							var DateDep = m_tourModuleCommon_setResearchValue_params['deptYearMonth'].substr(0,4) + "/";
							DateDep += m_tourModuleCommon_setResearchValue_params['deptYearMonth'].substr(4,2) + "/";
							if(m_tourModuleCommon_setResearchValue_params['deptDay'] !== undefined && m_tourModuleCommon_setResearchValue_params['deptDay'] != ""){
								DateDep += m_tourModuleCommon_setResearchValue_params['deptDay'];
							}else{
								// 「月すべて」が選択された場合は末尾「/」削除
								DateDep = DateDep.slice(0, -1);
							}
							var inttourPkg = {inttourPkg : {'m_defDepartureDateType' : '0', 'm_defDateDep' : DateDep}}
						}else{
							var inttourPkg = {inttourPkg : {'m_defDepartureDateType' : '0'}}
						}
					}
					
					optionsParam = $tour_module_jq.extend(true,optionsParam,inttourPkg);
					// 別タブのパラメータを削除
					optionsParam['inttourDp'] = undefined;
					optionsParam['domtourDp'] = undefined;
					optionsParam['domtourPkg'] = undefined;
					optionsParam['domtourHotel'] = undefined;
					optionsParam['domtourHotelBulk'] = undefined;
					optionsParam['domtourCar'] = undefined;
					optionsParam['domtourTraffic'] = undefined;
					optionsParam['domtourBulk'] = undefined;
				}else if(i==2){
					//海外タブ初期選択設定
					tabFuncInt = {tabFuncInt : {'m_defInitTab' : '2'}}
					// パッケージツアー(コースコード)の場合は出発日(コースコード)の指定
					if(m_tourModuleCommon_setResearchValue_params['deptYearMonth']!==undefined && m_tourModuleCommon_setResearchValue_params['deptYearMonth'] != ""){
						var DateDepCourse = m_tourModuleCommon_setResearchValue_params['deptYearMonth'].substr(0,4) + "/";
						DateDepCourse += m_tourModuleCommon_setResearchValue_params['deptYearMonth'].substr(4,2) + "/";
						if(m_tourModuleCommon_setResearchValue_params['deptDay'] !== undefined && m_tourModuleCommon_setResearchValue_params['deptDay'] != ""){
							DateDepCourse += m_tourModuleCommon_setResearchValue_params['deptDay'];
						}else{
							// 「月すべて」が選択された場合は末尾「/」削除
							DateDepCourse = DateDepCourse.slice(0, -1);
						}
						var inttourPkg = {inttourPkg : {'m_defCourseDateDep' : DateDepCourse}}
						optionsParam = $tour_module_jq.extend(true,optionsParam,inttourPkg);
					}
					// 「行き先から探す」出発日を初期値に設定
					optionsParam['inttourPkg']['m_defDateDep'] = m_inttour2ndModulePkg_defaults['m_defDateDep'];
					
					// 別タブのパラメータを削除
					optionsParam['inttourDp'] = undefined;
					optionsParam['domtourDp'] = undefined;
					optionsParam['domtourPkg'] = undefined;
					optionsParam['domtourHotel'] = undefined;
					optionsParam['domtourHotelBulk'] = undefined;
					optionsParam['domtourCar'] = undefined;
					optionsParam['domtourTraffic'] = undefined;
					optionsParam['domtourBulk'] = undefined;
				}
				
				optionsParam = $tour_module_jq.extend(true,optionsParam,tabFuncInt);
				
				break;
			}
		}
		
		if(moduleFlg){
			// 旅作、PKG(行き先)、Pkg(商品)、ホテル(ホテル・三社一括ホテル)、レンタカー、観光、ツアー一括
			var domModule_judge = ['goDeptDt','travelDaysCnt','cmdtCd','checkInDt','allocDt','optCls','stayDaysCnt'];
			
			for(i=0;i<domModule_judge.length;i++){
				var judgeKey = domModule_judge[i];
				
				//国内モジュールタブ選択
				var tabFuncDomval = 1;
				
				if(m_tourModuleCommon_setResearchValue_params[judgeKey]!==undefined){
					//国内の場合
					
					//国内海外タブ選択
					var tabDomInt = {tabDomInt : {'m_defInitTab' : '1'}}
					optionsParam = $tour_module_jq.extend(true,optionsParam,tabDomInt);
					
					
					if(i==0){
						//旅作の場合
						
						//日付項目"/"付与
						//搭乗日　行き
						if(m_tourModuleCommon_setResearchValue_params['goDeptDt'] !== undefined && m_tourModuleCommon_setResearchValue_params['goDeptDt'] != ""){
							var goDeptDt = m_tourModuleCommon_setResearchValue_params['goDeptDt'].substr(0,4) + "/";
							goDeptDt += m_tourModuleCommon_setResearchValue_params['goDeptDt'].substr(4,2) + "/";
							goDeptDt += m_tourModuleCommon_setResearchValue_params['goDeptDt'].substr(6);
							optionsParam['domtourDp']['m_defDepFlightDateDep'] = goDeptDt;
						}
						//搭乗日　帰り
						if(m_tourModuleCommon_setResearchValue_params['rtnDeptDt'] !== undefined && m_tourModuleCommon_setResearchValue_params['rtnDeptDt'] != ""){
							var rtnDeptDt = m_tourModuleCommon_setResearchValue_params['rtnDeptDt'].substr(0,4) + "/";
							rtnDeptDt += m_tourModuleCommon_setResearchValue_params['rtnDeptDt'].substr(4,2) + "/";
							rtnDeptDt += m_tourModuleCommon_setResearchValue_params['rtnDeptDt'].substr(6);
							optionsParam['domtourDp']['m_defArrFlightDateDep'] = rtnDeptDt;
						}
						//チェックイン
						if(m_tourModuleCommon_setResearchValue_params['checkInDt'] !== undefined && m_tourModuleCommon_setResearchValue_params['checkInDt'] != ""){
							var checkInDt = m_tourModuleCommon_setResearchValue_params['checkInDt'].substr(0,4) + "/";
							checkInDt += m_tourModuleCommon_setResearchValue_params['checkInDt'].substr(4,2) + "/";
							checkInDt += m_tourModuleCommon_setResearchValue_params['checkInDt'].substr(6);
							optionsParam['domtourDp']['m_defCheckin'] = checkInDt;
						}
						//チェックアウト
						if(m_tourModuleCommon_setResearchValue_params['checkOutDt'] !== undefined && m_tourModuleCommon_setResearchValue_params['checkOutDt'] != ""){
							var checkOutDt = m_tourModuleCommon_setResearchValue_params['checkOutDt'].substr(0,4) + "/";
							checkOutDt += m_tourModuleCommon_setResearchValue_params['checkOutDt'].substr(4,2) + "/";
							checkOutDt += m_tourModuleCommon_setResearchValue_params['checkOutDt'].substr(6);
							optionsParam['domtourDp']['m_defCheckout'] = checkOutDt;
						}
						
						// 別タブのパラメータを削除
						optionsParam['inttourDp'] = undefined;
						optionsParam['inttourPkg'] = undefined;
						optionsParam['domtourPkg'] = undefined;
						optionsParam['domtourHotel'] = undefined;
						optionsParam['domtourHotelBulk'] = undefined;
						optionsParam['domtourCar'] = undefined;
						optionsParam['domtourTraffic'] = undefined;
						optionsParam['domtourBulk'] = undefined;
						
						tabFuncDomval = 1; //国内モジュールタブ選択
						
					}else if(i==1){
						//パッケージ（行き先）の場合
						//出発日
						if(optionsParam['domtourPkg']['m_defDateDep'] !== undefined && optionsParam['domtourPkg']['m_defDateDep'] != ""){
							var m_defDateDep = optionsParam['domtourPkg']['m_defDateDep'].substr(0,4) + "/";
							m_defDateDep += optionsParam['domtourPkg']['m_defDateDep'].substr(4,2) + "/";
							m_defDateDep += optionsParam['domtourPkg']['m_defDateDep'].substr(6);
							optionsParam['domtourPkg']['m_defDateDep'] = m_defDateDep;
						}
						
						// 別タブのパラメータを削除
						optionsParam['inttourDp'] = undefined;
						optionsParam['inttourPkg'] = undefined;
						optionsParam['domtourDp'] = undefined;
						optionsParam['domtourHotel'] = undefined;
						optionsParam['domtourHotelBulk'] = undefined;
						optionsParam['domtourCar'] = undefined;
						optionsParam['domtourTraffic'] = undefined;
						optionsParam['domtourBulk'] = undefined;
						
						tabFuncDomval = 2; //国内モジュールタブ選択
					}else if(i==2){
						//パッケージ（商品）の場合
						//出発日(商品コード検索)
						if(optionsParam['domtourPkg']['m_defCourseDateDep'] !== undefined && optionsParam['domtourPkg']['m_defCourseDateDep'] != ""){
							var m_defCourseDateDep = optionsParam['domtourPkg']['m_defCourseDateDep'].substr(0,4) + "/";
							m_defCourseDateDep += optionsParam['domtourPkg']['m_defCourseDateDep'].substr(4,2) + "/";
							m_defCourseDateDep += optionsParam['domtourPkg']['m_defCourseDateDep'].substr(6);
							optionsParam['domtourPkg']['m_defCourseDateDep'] = m_defCourseDateDep;
						}
						
						// 別タブのパラメータを削除
						optionsParam['inttourDp'] = undefined;
						optionsParam['inttourPkg'] = undefined;
						optionsParam['domtourDp'] = undefined;
						optionsParam['domtourHotel'] = undefined;
						optionsParam['domtourHotelBulk'] = undefined;
						optionsParam['domtourCar'] = undefined;
						optionsParam['domtourTraffic'] = undefined;
						optionsParam['domtourBulk'] = undefined;
						
						tabFuncDomval = 2; //国内モジュールタブ選択
					}else if(i==3){
						//ホテルの場合
						//チェックイン
						if(optionsParam['domtourHotel']['m_defCheckin'] !== undefined && optionsParam['domtourHotel']['m_defCheckin'] != ""){
							var m_defCheckin = optionsParam['domtourHotel']['m_defCheckin'].substr(0,4) + "/";
							m_defCheckin += optionsParam['domtourHotel']['m_defCheckin'].substr(4,2) + "/";
							m_defCheckin += optionsParam['domtourHotel']['m_defCheckin'].substr(6);
							optionsParam['domtourHotel']['m_defCheckin'] = m_defCheckin;
						}
						
						//チェックアウト
						if(optionsParam['domtourHotel']['m_defCheckout'] !== undefined && optionsParam['domtourHotel']['m_defCheckout'] != ""){
							var m_defCheckout = optionsParam['domtourHotel']['m_defCheckout'].substr(0,4) + "/";
							m_defCheckout += optionsParam['domtourHotel']['m_defCheckout'].substr(4,2) + "/";
							m_defCheckout += optionsParam['domtourHotel']['m_defCheckout'].substr(6);
							optionsParam['domtourHotel']['m_defCheckout'] = m_defCheckout;
						}
						
						//こだわり条件
						if(optionsParam['domtourHotel']['m_defAddConditionChecked'] !== undefined && optionsParam['domtourHotel']['m_defAddConditionChecked'] != ""){
							var addConditionParams = optionsParam['domtourHotel']['m_defAddConditionChecked'].split(',');
							var addConditionValues=[];
							for(i=0;i < addConditionParams.length;i++){
								addConditionValues[i]=addConditionParams[i];
							}
							optionsParam['domtourHotel']['m_defAddConditionChecked'] = addConditionValues;
						}
						
						// 別タブのパラメータを削除
						optionsParam['inttourDp'] = undefined;
						optionsParam['inttourPkg'] = undefined;
						optionsParam['domtourDp'] = undefined;
						optionsParam['domtourHotelBulk'] = undefined;
						optionsParam['domtourPkg'] = undefined;
						optionsParam['domtourCar'] = undefined;
						optionsParam['domtourTraffic'] = undefined;
						optionsParam['domtourBulk'] = undefined;
						
						tabFuncDomval = 3; //国内モジュールタブ選択
					}else if(i==4){
						//レンタカーの場合
						//配車日
						if(optionsParam['domtourCar']['m_defDateFirstCar'] !== undefined && optionsParam['domtourCar']['m_defDateFirstCar'] != ""){
							var m_defDateFirstCar = optionsParam['domtourCar']['m_defDateFirstCar'].substr(0,4) + "/";
							m_defDateFirstCar += optionsParam['domtourCar']['m_defDateFirstCar'].substr(4,2) + "/";
							m_defDateFirstCar += optionsParam['domtourCar']['m_defDateFirstCar'].substr(6);
							optionsParam['domtourCar']['m_defDateFirstCar'] = m_defDateFirstCar;
						}
						
						//返車日
						if(optionsParam['domtourCar']['m_defDateLastCar'] !== undefined && optionsParam['domtourCar']['m_defDateLastCar'] != ""){
							var m_defDateLastCar = optionsParam['domtourCar']['m_defDateLastCar'].substr(0,4) + "/";
							m_defDateLastCar += optionsParam['domtourCar']['m_defDateLastCar'].substr(4,2) + "/";
							m_defDateLastCar += optionsParam['domtourCar']['m_defDateLastCar'].substr(6);
							optionsParam['domtourCar']['m_defDateLastCar'] = m_defDateLastCar;
						}
						
						//車両クラス
						if(optionsParam['domtourCar']['m_defCarClassChecked'] !== undefined && optionsParam['domtourCar']['m_defCarClassChecked'] != ""){
							var carClassParams = optionsParam['domtourCar']['m_defCarClassChecked'].split(',');
							var carClassValues=[];
							var j=0
							for(i=0;i < carClassParams.length;i++){
								if(m_domtourModule_CarClassInverseParamList[carClassParams[i]]!== undefined){
									carClassValues[j] = m_domtourModule_CarClassInverseParamList[carClassParams[i]] ;
									j++;
								}
							}
							optionsParam['domtourCar']['m_defCarClassChecked'] = carClassValues;
						}
						
						// 別タブのパラメータを削除
						optionsParam['inttourDp'] = undefined;
						optionsParam['inttourPkg'] = undefined;
						optionsParam['domtourDp'] = undefined;
						optionsParam['domtourPkg'] = undefined;
						optionsParam['domtourHotel'] = undefined;
						optionsParam['domtourHotelBulk'] = undefined;
						optionsParam['domtourTraffic'] = undefined;
						optionsParam['domtourBulk'] = undefined;
						
						tabFuncDomval = 5; //国内モジュールタブ選択
					}else if(i==5){
						//観光の場合
						//利用日
						if(optionsParam['domtourTraffic']['m_defDateTraffic'] !== undefined && optionsParam['domtourTraffic']['m_defDateTraffic'] != ""){
							var m_defDateTraffic = optionsParam['domtourTraffic']['m_defDateTraffic'].substr(0,4) + "/";
							m_defDateTraffic += optionsParam['domtourTraffic']['m_defDateTraffic'].substr(4,2) + "/";
							m_defDateTraffic += optionsParam['domtourTraffic']['m_defDateTraffic'].substr(6);
							optionsParam['domtourTraffic']['m_defDateTraffic'] = m_defDateTraffic;
						}
						
						// 別タブのパラメータを削除
						optionsParam['inttourDp'] = undefined;
						optionsParam['inttourPkg'] = undefined;
						optionsParam['domtourDp'] = undefined;
						optionsParam['domtourPkg'] = undefined;
						optionsParam['domtourHotel'] = undefined;
						optionsParam['domtourHotelBulk'] = undefined;
						optionsParam['domtourCar'] = undefined;
						optionsParam['domtourBulk'] = undefined;
						
						tabFuncDomval = 6; //国内モジュールタブ選択
					}else if(i==6){
						if(m_tourModuleCommon_setResearchValue_params['criteriaCondId']!==undefined){
							//三社ホテルの場合
							//チェックイン
							if(optionsParam['domtourHotelBulk']['m_defCheckin'] !== undefined && optionsParam['domtourHotelBulk']['m_defCheckin'] != ""){
								var m_defCheckin = optionsParam['domtourHotelBulk']['m_defCheckin'].substr(0,4) + "/";
								m_defCheckin += optionsParam['domtourHotelBulk']['m_defCheckin'].substr(4,2) + "/";
								m_defCheckin += optionsParam['domtourHotelBulk']['m_defCheckin'].substr(6);
								optionsParam['domtourHotelBulk']['m_defCheckin'] = m_defCheckin;
							}
							
							//こだわり条件
							if(optionsParam['domtourHotelBulk']['m_defAddConditionChecked'] !== undefined && optionsParam['domtourHotelBulk']['m_defAddConditionChecked'] != ""){
								var addConditionParams = optionsParam['domtourHotelBulk']['m_defAddConditionChecked'].split(',');
								var addConditionValues=[];
								for(i=0;i < addConditionParams.length;i++){
									addConditionValues[i]=addConditionParams[i];
								}
								optionsParam['domtourHotelBulk']['m_defAddConditionChecked'] = addConditionValues;
							}
							
							// 別タブのパラメータを削除
							optionsParam['inttourDp'] = undefined;
							optionsParam['inttourPkg'] = undefined;
							optionsParam['domtourDp'] = undefined;
							optionsParam['domtourHotel'] = undefined;
							optionsParam['domtourPkg'] = undefined;
							optionsParam['domtourCar'] = undefined;
							optionsParam['domtourTraffic'] = undefined;
							optionsParam['domtourBulk'] = undefined;
							
							tabFuncDomval = 4; //国内モジュールタブ選択
						}else{
							//ツアー一括の場合
							//利用日
							if(optionsParam['domtourBulk']['m_defDateDep'] !== undefined && optionsParam['domtourBulk']['m_defDateDep'] != ""){
								var m_defDateDep = optionsParam['domtourBulk']['m_defDateDep'].substr(0,4) + "/";
								m_defDateDep += optionsParam['domtourBulk']['m_defDateDep'].substr(4,2) + "/";
								m_defDateDep += optionsParam['domtourBulk']['m_defDateDep'].substr(6);
								optionsParam['domtourBulk']['m_defDateDep'] = m_defDateDep;
							}
							
							// 別タブのパラメータを削除
							optionsParam['inttourDp'] = undefined;
							optionsParam['inttourPkg'] = undefined;
							optionsParam['domtourDp'] = undefined;
							optionsParam['domtourPkg'] = undefined;
							optionsParam['domtourHotel'] = undefined;
							optionsParam['domtourHotelBulk'] = undefined;
							optionsParam['domtourCar'] = undefined;
							optionsParam['domtourTraffic'] = undefined;
							
							tabFuncDomval = 7; //国内モジュールタブ選択
						}
					}

					//国内モジュールタブ選択
					var tabFuncDom = {tabFuncDom : {'m_defInitTab' : tabFuncDomval}}
					optionsParam = $tour_module_jq.extend(true,optionsParam,tabFuncDom);
					
					break;
				}
			}
		}
		
		
		// optionsの初期値を更新
		options = $tour_module_jq.extend(true,options,optionsParam);
	}
	
	///////////////////
	// html記述追加
	///////////////////
	// 国内/国際タブ
	if(defIsDispTabDomInt){
		var optionsTabSelDomInt = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.tabDomInt);
		target.find('.mod-travel_select').tour2ndModuleTab('privateSecondModuleDisplay', optionsTabSelDomInt);
	}
	//国内旅作
	if(defIsDispDomDp){
		var optionsDomTabiskau = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourDp);
		target.find('.module-dom-tabisaku').domtour2ndModuleDp('privateSecondModuleDisplay', optionsDomTabiskau);
	}
	//国内パッケージ
	if(defIsDispDomPkg){
		var optionsDomPack = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourPkg);
		target.find('.module-dom-pgtour').domtour2ndModulePkg('privateSecondModuleDisplay', optionsDomPack);
	}
	
	//国内ホテル
	if(defIsDispDomHotel){
		var optionsDomHotel = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourHotel);
		target.find('.module-dom-hotel').domtour2ndModuleHotel('privateSecondModuleDisplay', optionsDomHotel);
	}
	
	//国内ホテル(３社一括)
	if(defIsDispDomHotelBulk){
		var optionsDomHotelBulk = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourHotelBulk);
		target.find('.module-dom-hotel3').domtour2ndModuleHotelBulk('privateSecondModuleDisplay', optionsDomHotelBulk);
	}
	
	//国内レンタカー
	if(defIsDispDomCar){
		var optionsDomCar = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourCar);
		target.find('.module-dom-car').domtour2ndModuleCar('privateSecondModuleDisplay', optionsDomCar);
	}
	
	//国内観光
	if(defIsDispDomTraffic){
		var optionsDomTraffic = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourTraffic);
		target.find('.module-dom-traffic').domtour2ndModuleTraffic('privateSecondModuleDisplay', optionsDomTraffic);
	}
	
	//国内ツアー
	if(defIsDispDomBulk){
		var optionsDomBulk = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.domtourBulk);
		target.find('.module-dom-bulk').domtour2ndModuleBulk('privateSecondModuleDisplay', optionsDomBulk);
	}
	// 海外旅作
	if(defIsDispIntTabisaku){
		var optionsIntTabiskau = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.inttourDp);
		target.find('.module-int-tabisaku').inttour2ndModuleDp('privateSecondModuleDisplay', optionsIntTabiskau);
	}
	// 海外パッケージ
	if(defIsDispIntPackTour){
		var optionsIntPack = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.inttourPkg);
		target.find('.module-int-pgtour').inttour2ndModulePkg('privateSecondModuleDisplay', optionsIntPack);
	}
	// 海外オプション
	if(defIsDispIntOption){
		var optionsIntOption = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.inttourOption);
		target.find('.module-int-option').inttour2ndModuleOption('privateSecondModuleDisplay', optionsIntOption);
	}
	// 国内旅行タブ
	if(defIsDispTabFuncDom){
		var optionsTabSelFuncDom = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.tabFuncDom);
		target.find('.mod_domtour').children('.js-mod-list-tab').domtour2ndModuleTab('privateSecondModuleDisplay', optionsTabSelFuncDom);
	}
	// 国際旅行タブ
	if(defIsDispTabFuncInt){
		var optionsTabSelFuncInt = $tour_module_jq.extend({}, m_tour2ndModuleMain_optionParam_common, options.tabFuncInt);
		target.find('.mod_inttour').children('.js-mod-list-tab').inttour2ndModuleTab('privateSecondModuleDisplay', optionsTabSelFuncInt);
	}

	// SP時シャッター
	function moduleShutterOpen () {
		var $moduleBox = $tour_module_jq(this).closest('.module-travel_cover');
		$moduleBox.find('.mod-shutter_box').removeClass('mod-shutter_box');
		$moduleBox.find('.js-mod-shutter_btn').hide();
	}
	// シャッター開くボタン
	$tour_module_jq(document).on('click.shutterOpen', '.js-mod-shutter_btn', function (e) {
		moduleShutterOpen.call(this);
	});
	// 触ったらシャッターが開く要素
	$tour_module_jq(document).on('click.shutterOpen', '.js-mod-travel-tab_btn.m_inttourTab, .module-travel_cover input, .module-travel_cover .mod-type_select a', function (e) {
		moduleShutterOpen.call(this);
	});

	// 国内、海外初期表示タブ取得
	var domintTabIndex = m_tourModuleCommon_getParam({ site : 'tour2nd',    module : 'tab' }, options, 'm_defInitTab');
	
	// 国内旅作の表示状態取得
	var domTabActive = target.find('.module-dom-tabisaku.is-active')[0];
	
	// 国内旅作以外が初期表示選択状態の場合、シャッター開示処理（単独表示または海外ツアー初期表示の場合、国内旅作見えなくてもactive扱いなので表示フラグでも判断）
	if (!( "1" === domintTabIndex && domTabActive && defIsDispDomDp)) {
		moduleShutterOpen.call(target.find('.js-mod-shutter_btn'));
	}
};

// 外部設置向けURL変換 相対から絶対パスに変更
function m_tour2ndModuleURL_change(option){
	// 海外ツアー2nd
	var optionsIntTabiskau = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, {}, 'm_defIsDispIntDp');
	var optionsIntPack = m_tourModuleCommon_getParam(m_tour2ndModuleMain_siteType, {}, 'm_defIsDispIntPkg');
	
	if(optionsIntTabiskau){
		m_tour2ndModuleDp_changeURL();
	}
	if(optionsIntPack){
		m_tour2ndModulePkg_changeURL();
	}
}
//URL変更 END
/////////////////////////////////////

// 外部公開メソッドの定義
$tour_module_jq.fn.tour2ndModule = function(method) {
	var methods = {
		'setParams' : m_tour2ndModuleMain_setParams,
		'secondModuleDisplay' : m_tour2ndModuleMain_secondModuleDisplay,
		'changeURL' : m_tour2ndModuleURL_change
	};

	if (methods[method]) {
		  return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
	} else {
		$tour_module_jq.error('Method ' +  method + ' does not exist on $tour_module_jq.inttour2ndModule');
	}
};

